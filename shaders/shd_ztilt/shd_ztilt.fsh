/// FRAGMENT SHADER ///
/*
	Обрабатывает каждый пиксель спрайта индивидуально (параллельно)
*/
varying vec2 v_vTexcoord;	// [0-1] - (u,v) координаты одного пикселя на странице текстуры
varying vec4 v_vColour;		// [0-1] - (r,g,b,a) image_blend + image_alpha после модификации вершинным шейдером
uniform float atest;

void main()
{
	vec4 sprite_col = texture2D( gm_BaseTexture, v_vTexcoord ); // захватить цвет спрайта в этом пикселе
	if (atest==1.0 && sprite_col.a < 1.0) { discard; }
	gl_FragColor = sprite_col * v_vColour;
}