/// VERTEX SHADER ///
/*
	Обрабатывает 6 вершин на спрайт!
	1-3  4-6    Full image (2 triangles)
	**	  *  =>	**
	*	 **		**

	=== z-наклон спрайтов ===
Определите верхние вершины, используйте альфа-канал (image_alpha) объекта / спрайта, чтобы определить
сколько пикселей поднять на Z (глубину), тем самым наклоняя спрайт.

*/

// === Vertex Format === //		// это формат вершины по умолчанию, который использует каждый спрайт / объект / фон / плитка в GM!
attribute vec3 in_Position;		// (x,y,z)		[как видно в комнате редактора]		sprite (x,y,depth) - углы не спрайтового происхождения!
attribute vec4 in_Colour;		// (r,g,b,a)	[0-1]								image_blend(r,g,b) + image_alpha(a) converted from [0-255] to [0-1] range
attribute vec2 in_TextureCoord; // (u,v)		[0-1]								координаты страницы текстуры спрайта

// === Define Output to Fragment === //
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    // === Init Vars === //
	vec4 col = in_Colour;
	vec4 object_space_pos = vec4( in_Position, 1.0);
	
	// === Z-tilting === //
	float top = 1.0-mod( col.b * 255.0, 2.0);		// верхние вершины? [0, 1]
	object_space_pos.z-=255.0*col.a*top;			// поднимите Z верхних вершин в направлении камеры image_alpha > 0
	//object_space_pos.y+=col.a/10.0;					// !ПО ЖЕЛАНИЮ! помогает с zfighting
	/*	если 2 спрайта перекрываются ровно по z, происходит драка (мерцание), слегка изменяя положение y
		мы можем облегчить эту проблему по большей части. Так как в этом случае мы используем альфа, это работает только для спрайтов
		разных размеров, что до сих пор решает большинство неприятностей!	*/
	
	// === UNNECCESARY === //: добавляет небольшой цветовой сдвиг в верхние вершины. Только для иллюстрации.
	// if (top==1.0) { col.rgb = vec3(1.0,0.5,0.5); }
	
	// === To Fragment === //
	gl_Position = gm_Matrices[MATRIX_WORLD_VIEW_PROJECTION] * object_space_pos;
    v_vColour = vec4(col.rgb, 1.0); // передать цвет с полной альфа на фрагмент
    v_vTexcoord = in_TextureCoord;		
}