t_scene_info = [
	[scr_cutscene_instance_destroy, obj_hero],
	[scr_cutscene_play_sound, snd_voice1, 10, false],
	
	[scr_cutscene_wait, .5],
	
	[scr_cutscene_instance_create, 400, 320, "Instances", obj_hero],
	[scr_cutscene_play_sound, snd_voice2, 10, false],
	
	[scr_cutscene_wait, .5],
	
	[scr_cutscene_move_character, obj_hero, -32, 0, true, 1],
];