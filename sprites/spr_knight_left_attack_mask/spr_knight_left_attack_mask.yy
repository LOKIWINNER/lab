{
    "id": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_left_attack_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 106,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbf33b77-2f2e-4bd9-9aca-603a820d578c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "b28e458e-ee9c-4960-b29a-2d10dbb60369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf33b77-2f2e-4bd9-9aca-603a820d578c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90aaea2-6c76-441f-b76a-70c18040da15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf33b77-2f2e-4bd9-9aca-603a820d578c",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "f6b57312-5a6b-4f06-a9a1-98041e514908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "1c1eb1c1-cf26-4d35-9b90-2ddd9972146c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6b57312-5a6b-4f06-a9a1-98041e514908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11573c4e-9957-46ec-be1c-78f9d1d12de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6b57312-5a6b-4f06-a9a1-98041e514908",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "a763c3ef-c99c-4b66-bf9e-71d6828878bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "7fdc41c8-5a42-4315-86eb-fd57aa1576e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a763c3ef-c99c-4b66-bf9e-71d6828878bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d65e2a-af4f-4c9d-80cf-e4e70ba167dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a763c3ef-c99c-4b66-bf9e-71d6828878bb",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "07a6481d-0142-4e9b-b1bd-7021958fdddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "73997fa8-48ef-4f6a-aa9e-3f18af0ff478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a6481d-0142-4e9b-b1bd-7021958fdddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79e72213-8283-421f-aa39-38deba30a3e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a6481d-0142-4e9b-b1bd-7021958fdddc",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "2e3af755-612f-44ac-a9e2-c8512e0bd195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "5c3dfa39-dc4b-4646-a2e7-d8de2213e7c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e3af755-612f-44ac-a9e2-c8512e0bd195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0656cb2b-bc5d-4d6d-88ba-5444cf9f5700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e3af755-612f-44ac-a9e2-c8512e0bd195",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "00097f95-7ea2-4ade-8959-bfd1d0da7025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "ce50dac6-0171-4c87-89b1-44df48ed621d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00097f95-7ea2-4ade-8959-bfd1d0da7025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a452f2a-dd15-44d9-a53b-f43f07ed1ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00097f95-7ea2-4ade-8959-bfd1d0da7025",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "e19e1d99-c611-4b0b-9b6e-a409d375d948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "1c50c391-6899-4458-a47f-70a5ca793e69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19e1d99-c611-4b0b-9b6e-a409d375d948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "609543cc-03ed-47df-acb6-c9e5ce33d935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19e1d99-c611-4b0b-9b6e-a409d375d948",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "67ae2695-0968-4c49-866c-d7bbffad6cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "9325b942-ab1f-43bb-bfd0-4222cf3c40ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67ae2695-0968-4c49-866c-d7bbffad6cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49520afa-7e75-44f7-b7da-b0e862b7144a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67ae2695-0968-4c49-866c-d7bbffad6cf2",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "628a37f5-19ea-4ad1-9584-6fb3dcbf3fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "a772194c-770e-4c7f-a304-d57de6d02b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "628a37f5-19ea-4ad1-9584-6fb3dcbf3fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87502fa-40e4-4e5f-be80-a34cd050a4c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "628a37f5-19ea-4ad1-9584-6fb3dcbf3fb0",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        },
        {
            "id": "3af6ca9c-fde7-4adb-a480-403f6e099908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "compositeImage": {
                "id": "13ac7cf4-cecf-403f-845d-76ac24f44bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af6ca9c-fde7-4adb-a480-403f6e099908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50c45db-30a8-4115-aff9-05e3984dc4ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af6ca9c-fde7-4adb-a480-403f6e099908",
                    "LayerId": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1eade1ab-c1ed-4b20-b5ba-0d8691af1c47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2260d97-7c6b-47d0-87e1-8822e5d1f522",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 63,
    "yorig": 67
}