{
    "id": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_press_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 14,
    "bbox_right": 45,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d854100-501c-432d-8f74-df7a28ebfa16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "compositeImage": {
                "id": "b9e76651-1da9-43f4-ad2f-32a843bac703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d854100-501c-432d-8f74-df7a28ebfa16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b4691e5-63da-4740-abaa-e6b4056a1bf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d854100-501c-432d-8f74-df7a28ebfa16",
                    "LayerId": "5fae3fa5-cb35-4399-b45b-be0354480f0d"
                }
            ]
        },
        {
            "id": "b42e4009-cd52-41a4-8236-187e74d04015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "compositeImage": {
                "id": "5fdb7e27-7aa5-4af4-9b66-6dbe7b4d5820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b42e4009-cd52-41a4-8236-187e74d04015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca2c088-984e-40dc-9076-334c7e0f7b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b42e4009-cd52-41a4-8236-187e74d04015",
                    "LayerId": "5fae3fa5-cb35-4399-b45b-be0354480f0d"
                }
            ]
        },
        {
            "id": "4bdce7d2-8f59-4635-a3d9-e1f4b88967ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "compositeImage": {
                "id": "84b1c101-fc03-49e9-8e4b-f613a8611a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bdce7d2-8f59-4635-a3d9-e1f4b88967ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e6b12dd-5759-4485-9904-cf08ce10e2d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bdce7d2-8f59-4635-a3d9-e1f4b88967ba",
                    "LayerId": "5fae3fa5-cb35-4399-b45b-be0354480f0d"
                }
            ]
        },
        {
            "id": "1df74195-de20-4c50-b330-bd5b9f4ecdd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "compositeImage": {
                "id": "e99f892a-b376-4e21-b4a0-916bae1cc8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df74195-de20-4c50-b330-bd5b9f4ecdd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ad0cc5-75a7-41af-8489-4fff712ebe58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df74195-de20-4c50-b330-bd5b9f4ecdd4",
                    "LayerId": "5fae3fa5-cb35-4399-b45b-be0354480f0d"
                }
            ]
        },
        {
            "id": "32d87843-138a-431c-a97c-edce78170e1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "compositeImage": {
                "id": "6c1248b0-30cf-4de5-9f71-a540c170fea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32d87843-138a-431c-a97c-edce78170e1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "746bb7b9-abe0-482b-ba8a-403f4c176036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d87843-138a-431c-a97c-edce78170e1d",
                    "LayerId": "5fae3fa5-cb35-4399-b45b-be0354480f0d"
                }
            ]
        },
        {
            "id": "d2417148-07ac-4ed9-9706-6cb27b72b7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "compositeImage": {
                "id": "6189e1cf-dd6b-4916-b9be-b6c0af09e8e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2417148-07ac-4ed9-9706-6cb27b72b7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e5593d6-6694-41e0-bc67-268c539067fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2417148-07ac-4ed9-9706-6cb27b72b7a7",
                    "LayerId": "5fae3fa5-cb35-4399-b45b-be0354480f0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "5fae3fa5-cb35-4399-b45b-be0354480f0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 15
}