{
    "id": "066153ff-0faf-460f-918b-b2d554310665",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 2,
    "bbox_right": 509,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eaf51ea8-c258-45a5-9299-e5eade691cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "066153ff-0faf-460f-918b-b2d554310665",
            "compositeImage": {
                "id": "09ecd82e-4a51-4aca-b05a-00f3e4c1dd53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf51ea8-c258-45a5-9299-e5eade691cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35b8a8a0-ff9d-4113-8b65-059401265fe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf51ea8-c258-45a5-9299-e5eade691cbc",
                    "LayerId": "72c24fb5-fa59-4dd0-9e5f-2d0803e29130"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "72c24fb5-fa59-4dd0-9e5f-2d0803e29130",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "066153ff-0faf-460f-918b-b2d554310665",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}