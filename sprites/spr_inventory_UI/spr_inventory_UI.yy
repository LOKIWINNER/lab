{
    "id": "b5f5ceb2-3bfd-4bcd-9b3e-7632d492de5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bcd1e1e-dfee-4262-a863-78c3cccf4f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5f5ceb2-3bfd-4bcd-9b3e-7632d492de5f",
            "compositeImage": {
                "id": "6f70f384-47b4-4310-9878-78c4606b0e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bcd1e1e-dfee-4262-a863-78c3cccf4f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c26ecd-71fb-4658-afd3-57aa3934ae67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bcd1e1e-dfee-4262-a863-78c3cccf4f06",
                    "LayerId": "79f53a4a-7fa5-47ff-9f89-b5c628942bc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "79f53a4a-7fa5-47ff-9f89-b5c628942bc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5f5ceb2-3bfd-4bcd-9b3e-7632d492de5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}