{
    "id": "70a4fae1-196f-43c7-84e4-d9e8cea4708d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45b4afb4-92db-4501-a3e0-454831cf698f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70a4fae1-196f-43c7-84e4-d9e8cea4708d",
            "compositeImage": {
                "id": "3a3548cf-a881-4aab-b9ee-69a19e38d727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b4afb4-92db-4501-a3e0-454831cf698f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c83d3678-4f54-4367-9362-e15ce7dc77f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b4afb4-92db-4501-a3e0-454831cf698f",
                    "LayerId": "be14fd31-bbca-451f-bd76-2dab509add53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "be14fd31-bbca-451f-bd76-2dab509add53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70a4fae1-196f-43c7-84e4-d9e8cea4708d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}