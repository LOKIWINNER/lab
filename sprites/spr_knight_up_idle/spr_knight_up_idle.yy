{
    "id": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_up_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 26,
    "bbox_right": 98,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7362c633-7f0f-4db8-811c-53a97b44b7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "457e8dae-9ff2-41a7-9307-3e3262a061e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7362c633-7f0f-4db8-811c-53a97b44b7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ffe7ce6-fc86-4d3b-8fa2-af420f55e02e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7362c633-7f0f-4db8-811c-53a97b44b7ff",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "f23f1a5a-24a5-4bb8-868e-455265dd5e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "9f342fc3-b2bd-4035-9ef1-52850af145eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f23f1a5a-24a5-4bb8-868e-455265dd5e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa74808b-3666-4fdc-b137-84cedd11381c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f23f1a5a-24a5-4bb8-868e-455265dd5e16",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "1f051d1f-4744-44d1-9159-dde7a4de1e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "10001a7e-9fc3-4e6f-99b1-e5e6ff824021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f051d1f-4744-44d1-9159-dde7a4de1e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4322be3-5196-4139-9c0a-d99aa45ebbb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f051d1f-4744-44d1-9159-dde7a4de1e66",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "1d64df6b-5b52-40c8-8d10-e459937de32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "3749fc34-0025-4c9e-9138-f8006d9436ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d64df6b-5b52-40c8-8d10-e459937de32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "556bbd44-1a60-4e12-8b22-0c48d919a55b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d64df6b-5b52-40c8-8d10-e459937de32c",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "40d9757d-bf7d-4d09-8d3e-a172ea996594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "5a7c06da-04be-48f1-a713-09e4a403a9c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d9757d-bf7d-4d09-8d3e-a172ea996594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4712fba-8907-473b-9903-18bb232db0a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d9757d-bf7d-4d09-8d3e-a172ea996594",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "8f77117d-8840-4701-ba9e-aab2754d619b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "30018446-26d7-47fb-9000-dad664b191da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f77117d-8840-4701-ba9e-aab2754d619b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a794a1e-2926-4597-9773-e7d65127b5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f77117d-8840-4701-ba9e-aab2754d619b",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "e303126c-b8c3-40db-ad68-d18b111f840d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "ccdc4012-0829-4e18-8a17-f0dc3d603f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e303126c-b8c3-40db-ad68-d18b111f840d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be1161df-ffff-45de-aea3-0fc63fc9373e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e303126c-b8c3-40db-ad68-d18b111f840d",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        },
        {
            "id": "a9628ea8-363f-44a5-9d57-2a491a7f922c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "compositeImage": {
                "id": "a8e96c16-f939-4a7c-9e60-afba2ef52be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9628ea8-363f-44a5-9d57-2a491a7f922c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "100e74c2-670b-4b8a-badf-84e76b0e0bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9628ea8-363f-44a5-9d57-2a491a7f922c",
                    "LayerId": "7147074b-9e05-476d-b757-1f1f47749b9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7147074b-9e05-476d-b757-1f1f47749b9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bfde3e0-9747-4f1f-ba31-2c7d00e2205a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 48,
    "yorig": 66
}