{
    "id": "c77327a4-1c94-4136-b702-5c4c0fcd8591",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cdd2d81-94c2-4aef-b5a5-123c49fe39e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c77327a4-1c94-4136-b702-5c4c0fcd8591",
            "compositeImage": {
                "id": "bd05f1ec-e319-4843-91d9-9791b7f05b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cdd2d81-94c2-4aef-b5a5-123c49fe39e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97f6d776-a27f-429c-9288-5c26d7d8814e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cdd2d81-94c2-4aef-b5a5-123c49fe39e1",
                    "LayerId": "41cb1151-4465-4500-9992-e4b3efb4e330"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "41cb1151-4465-4500-9992-e4b3efb4e330",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c77327a4-1c94-4136-b702-5c4c0fcd8591",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}