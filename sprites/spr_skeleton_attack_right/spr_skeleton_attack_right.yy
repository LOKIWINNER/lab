{
    "id": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa19c99e-0fbb-4e13-82ca-a886cf452e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "84ff9a26-76d6-4d1c-9cf5-5e2a90853cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa19c99e-0fbb-4e13-82ca-a886cf452e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6acd86e5-cba4-46e2-95e5-48f9b969b54f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa19c99e-0fbb-4e13-82ca-a886cf452e02",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        },
        {
            "id": "6990a729-ccd1-4db4-86f8-8ff1b7683451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "801f44b7-983f-4a55-9409-645e130b89d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6990a729-ccd1-4db4-86f8-8ff1b7683451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f60f59d-16b9-40c9-bf3f-b2876220fb9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6990a729-ccd1-4db4-86f8-8ff1b7683451",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        },
        {
            "id": "09d517db-ede4-492d-a5ae-eadf1abe40c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "fc309cc0-67b9-4ce2-9806-e45bb32eb402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09d517db-ede4-492d-a5ae-eadf1abe40c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bffa8f43-7c92-4593-b16e-e912c89f6877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09d517db-ede4-492d-a5ae-eadf1abe40c7",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        },
        {
            "id": "a329e972-067c-443c-8000-544cd92a285c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "89809f69-9e8d-46a5-8d55-ee25ea8c63d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a329e972-067c-443c-8000-544cd92a285c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf62c85-a26d-4b66-8acf-aae2f755fea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a329e972-067c-443c-8000-544cd92a285c",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        },
        {
            "id": "6255a4dd-4353-4b49-bb76-fbccbb5273eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "3c045503-3ed2-448e-b86a-f15866deed13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6255a4dd-4353-4b49-bb76-fbccbb5273eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d4a0f3-708d-431d-9775-7fcf750a6ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6255a4dd-4353-4b49-bb76-fbccbb5273eb",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        },
        {
            "id": "0fe1d084-2af3-407b-9090-4b5045f2bb23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "8cfba892-84b8-4cc4-9244-a150a967799d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe1d084-2af3-407b-9090-4b5045f2bb23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e269698-602f-4c25-b2ae-10d026c88072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe1d084-2af3-407b-9090-4b5045f2bb23",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        },
        {
            "id": "50e68d62-6f40-41af-8129-3f649c60b7f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "compositeImage": {
                "id": "78d6fedb-6454-4539-b73b-fe984a07dfdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50e68d62-6f40-41af-8129-3f649c60b7f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a87aca50-5a83-4aff-9117-90d7583657aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50e68d62-6f40-41af-8129-3f649c60b7f3",
                    "LayerId": "5749e25a-dfff-4387-8a0a-99841034a28e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5749e25a-dfff-4387-8a0a-99841034a28e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8312d4a-453c-4f7f-9d73-f01b1490df12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}