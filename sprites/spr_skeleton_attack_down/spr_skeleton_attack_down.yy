{
    "id": "9e23bf7e-9377-4324-8af8-d28127830477",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb0c9406-47c7-44b1-89b9-345c2d8cd18b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "be2db4c4-e448-43db-a0f8-cfe3f5f4c23f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb0c9406-47c7-44b1-89b9-345c2d8cd18b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d598acd1-33b5-4ef0-a20a-e6ed6916276f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb0c9406-47c7-44b1-89b9-345c2d8cd18b",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        },
        {
            "id": "76a17dde-17f7-47c1-aa34-315928d9e535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "bb77decc-0397-48be-8b24-bfba53724722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a17dde-17f7-47c1-aa34-315928d9e535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b010791-5998-4291-9a20-db77600379ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a17dde-17f7-47c1-aa34-315928d9e535",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        },
        {
            "id": "79d381bf-0a45-4e59-868e-33ccf884f637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "a27d983c-dde8-4c05-b139-028721959f2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d381bf-0a45-4e59-868e-33ccf884f637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eaff61c-987d-482f-b672-bd387e4fb2d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d381bf-0a45-4e59-868e-33ccf884f637",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        },
        {
            "id": "a4868549-02f4-416d-943c-1ac5dd1f9c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "8f8f48d0-ae64-4130-9b4d-ff5ae8f59be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4868549-02f4-416d-943c-1ac5dd1f9c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb62cbc-d4a2-476a-8460-8ee27346fd6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4868549-02f4-416d-943c-1ac5dd1f9c4f",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        },
        {
            "id": "03d6ba7e-84c9-436b-a15c-36aac34821e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "db4918bc-3424-41a1-86c4-c76aa7098701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03d6ba7e-84c9-436b-a15c-36aac34821e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f6da48-d5fc-4074-9e3c-26d1a55fb170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03d6ba7e-84c9-436b-a15c-36aac34821e3",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        },
        {
            "id": "82b3c184-2693-452f-bb87-63862af43bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "4d84f6b8-77de-4392-92f8-843579251b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b3c184-2693-452f-bb87-63862af43bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b339f9a9-22b2-40e1-a219-67b737bc4f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b3c184-2693-452f-bb87-63862af43bc1",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        },
        {
            "id": "8844003c-8edc-4978-8bd0-7da37ab9810b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "compositeImage": {
                "id": "620105ea-1716-4694-a39c-700e1e17c972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8844003c-8edc-4978-8bd0-7da37ab9810b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39c2de64-95d5-4d47-9efe-b4d041a619cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8844003c-8edc-4978-8bd0-7da37ab9810b",
                    "LayerId": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "63d3d14b-ee5f-4915-8f82-8f4eefa950e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e23bf7e-9377-4324-8af8-d28127830477",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}