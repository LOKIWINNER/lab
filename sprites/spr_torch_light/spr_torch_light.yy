{
    "id": "48f98564-836d-470d-826f-3f79becd6588",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torch_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 172,
    "bbox_left": 19,
    "bbox_right": 172,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "509fbd58-88a1-4034-b335-01c3cf4d43be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48f98564-836d-470d-826f-3f79becd6588",
            "compositeImage": {
                "id": "00a0654a-79a1-4573-a370-919acaed2866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "509fbd58-88a1-4034-b335-01c3cf4d43be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa275a36-1bd3-4900-8ff3-de99a2f4f531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509fbd58-88a1-4034-b335-01c3cf4d43be",
                    "LayerId": "f4a4d3df-a711-4c29-b91a-77e15b12b307"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "f4a4d3df-a711-4c29-b91a-77e15b12b307",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48f98564-836d-470d-826f-3f79becd6588",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}