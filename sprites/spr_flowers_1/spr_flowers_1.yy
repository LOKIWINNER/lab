{
    "id": "fda15aea-80c9-4f9e-b397-120b52507d62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flowers_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 28,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "027829fb-23f3-4f60-b3a9-ecdad54b4259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fda15aea-80c9-4f9e-b397-120b52507d62",
            "compositeImage": {
                "id": "6bba66ac-16a6-44c2-a6aa-2a051aa95391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027829fb-23f3-4f60-b3a9-ecdad54b4259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad90e649-23e0-4521-97b2-2e46bc2cea6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027829fb-23f3-4f60-b3a9-ecdad54b4259",
                    "LayerId": "fa793207-2479-4a58-97d4-9588c0a98fbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa793207-2479-4a58-97d4-9588c0a98fbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fda15aea-80c9-4f9e-b397-120b52507d62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}