{
    "id": "27a18b49-b518-44d7-9f94-74039f66577b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_attack_mask_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6b3a783-8f13-4907-aa11-62c832f3dbc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "0739de2a-580f-45e5-a582-997c49343bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b3a783-8f13-4907-aa11-62c832f3dbc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77351b23-1736-4677-92b1-72f2f3afbb4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b3a783-8f13-4907-aa11-62c832f3dbc7",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "ea070992-769f-4325-979a-17b3df7835d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "51121d01-0c8e-4250-ad09-882ca6f8caba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea070992-769f-4325-979a-17b3df7835d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adb3e03d-17f4-43ce-8ce8-40de1fc137de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea070992-769f-4325-979a-17b3df7835d4",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "a6077f70-f558-490f-873c-d36550c499ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "499d94dd-9075-4d75-9460-9f3b3e2b4390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6077f70-f558-490f-873c-d36550c499ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6599d43-8a7c-43e1-a88d-a2c0be0d3c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6077f70-f558-490f-873c-d36550c499ff",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "063de9b5-9831-433e-b8da-e39b233b5d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "09d5eb97-3d6b-465c-9e1b-4d346beb140f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "063de9b5-9831-433e-b8da-e39b233b5d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f31542dd-cfca-4d20-9bd4-c367a0210245",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "063de9b5-9831-433e-b8da-e39b233b5d4d",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "1d45402d-d821-4f01-b362-9a12f09dd345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "ecd1b9c5-ec57-482a-94b4-7748793e17f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d45402d-d821-4f01-b362-9a12f09dd345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998af5ad-9df1-445d-bfc1-993c5dfae376",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d45402d-d821-4f01-b362-9a12f09dd345",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "a2f4a1d9-bea1-477b-8e6a-cc9fe484039e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "2742c9e5-2b12-40ce-9dca-4059a95feae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f4a1d9-bea1-477b-8e6a-cc9fe484039e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50f0e575-d597-4bb1-8fac-73d6b18150dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f4a1d9-bea1-477b-8e6a-cc9fe484039e",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "94d97560-eec0-4869-9534-2f2b4ff034dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "fa558565-a52e-4da8-b1fc-044442e09032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d97560-eec0-4869-9534-2f2b4ff034dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52271009-14d8-4c75-a5cc-c6f364c37ed9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d97560-eec0-4869-9534-2f2b4ff034dd",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "330a993f-27e7-4559-a147-7ef0261b2781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "815fbae5-7a6d-4fb2-bbe4-2cdc461fe487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330a993f-27e7-4559-a147-7ef0261b2781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d7981f-b8e7-48a1-998a-353aeb9c80f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330a993f-27e7-4559-a147-7ef0261b2781",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "534aa802-9cba-4253-ae1e-a5cab63342cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "630001bf-4b3f-4cb4-aa9e-7144a254fcee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534aa802-9cba-4253-ae1e-a5cab63342cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b4a4d94-c2c7-4226-a6bb-45cac184dd85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534aa802-9cba-4253-ae1e-a5cab63342cd",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        },
        {
            "id": "e9ad1450-bd18-45fa-84d0-418f9969614f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "compositeImage": {
                "id": "44a31783-0d8c-4197-af27-2367ff5d6c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ad1450-bd18-45fa-84d0-418f9969614f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a424c84-14ef-4689-9f27-a682bb6a635e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ad1450-bd18-45fa-84d0-418f9969614f",
                    "LayerId": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1f6cdf1d-ac8f-4479-962c-59e9fbc58930",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27a18b49-b518-44d7-9f94-74039f66577b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}