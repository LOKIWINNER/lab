{
    "id": "df158df0-ed9a-45e4-807b-7977e31e074e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_up_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 28,
    "bbox_right": 99,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be735971-f9dd-4854-87c7-a744cc802b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "833fce65-ec9e-42af-958f-098b43d71a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be735971-f9dd-4854-87c7-a744cc802b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "240d0966-6867-431b-95d9-cde69b71a7ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be735971-f9dd-4854-87c7-a744cc802b06",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "fec2fd58-9cae-436b-ae8b-a54bf3aed022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "892c0b50-de11-413b-bb4a-1789106caf72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fec2fd58-9cae-436b-ae8b-a54bf3aed022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2387e18f-6660-4793-8df7-6fa11161b532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fec2fd58-9cae-436b-ae8b-a54bf3aed022",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "854fe381-bce1-4d08-b4da-bf83f919d583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "a51d5a27-6db7-4ffe-b3dc-0718d0c228fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854fe381-bce1-4d08-b4da-bf83f919d583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b1c55e7-dadc-41f3-859b-0489ea73a06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854fe381-bce1-4d08-b4da-bf83f919d583",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "6efcd277-8fb1-4442-aa79-6cb039f9e015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "96bc3f7d-9611-4bff-991a-dbf2d5936766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6efcd277-8fb1-4442-aa79-6cb039f9e015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f5939f-36d3-4b7d-9691-cdf53e7457f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6efcd277-8fb1-4442-aa79-6cb039f9e015",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "4c6c9e62-a883-4386-bc4f-4b59ab7c01a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "2931eb06-c42e-4f0a-8afa-40a9e4ba3825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c6c9e62-a883-4386-bc4f-4b59ab7c01a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53b1b5b4-bff9-41e8-8f06-acbc52b21874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c6c9e62-a883-4386-bc4f-4b59ab7c01a3",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "93c12ea2-6abc-4181-bb4c-6d6f2b17fb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "cd0a8535-44d1-4684-8bb6-f7d0feab18c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c12ea2-6abc-4181-bb4c-6d6f2b17fb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd5c18d4-047a-483f-bd2e-ca3a55a1024e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c12ea2-6abc-4181-bb4c-6d6f2b17fb2c",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "511d84b3-70c0-42bc-86af-24fc964ba82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "df47a923-d110-49a7-aad5-557b8d768193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511d84b3-70c0-42bc-86af-24fc964ba82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa45734-e477-49b7-844d-01d42d8d2f8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511d84b3-70c0-42bc-86af-24fc964ba82d",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "d2cc3508-ce23-487d-8eaf-e2a219cf2056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "b6e964cf-4803-4b2c-8451-25fab4a3f5d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2cc3508-ce23-487d-8eaf-e2a219cf2056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c06e9e-8ae0-428a-9b47-5272e5f0b4dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2cc3508-ce23-487d-8eaf-e2a219cf2056",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "6e2beebb-4124-4d78-8cc9-fffbecf9e0e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "95a84b84-ea97-4aaa-a847-0bba5cf0564a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e2beebb-4124-4d78-8cc9-fffbecf9e0e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b88a674-e49e-4842-9910-a6c87c94a355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e2beebb-4124-4d78-8cc9-fffbecf9e0e8",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "c74cce96-19f6-4204-b448-d3fff2cf1a28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "34bb4806-ac40-415a-a269-a1dc5b076842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74cce96-19f6-4204-b448-d3fff2cf1a28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7caaa144-4760-4935-8f4f-bbbd53adbad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74cce96-19f6-4204-b448-d3fff2cf1a28",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "ef858ef1-e978-4ce5-842c-4d72e11cea89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "ad8ee201-a014-405f-ad2e-43539fd6eb97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef858ef1-e978-4ce5-842c-4d72e11cea89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56e4441-8bf3-4742-9b8b-864075ff0f26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef858ef1-e978-4ce5-842c-4d72e11cea89",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "348645f6-8aa6-4f5d-8f44-2c8dc3adfd2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "bf9ea13e-b952-47a9-b4db-f8ac76d5b55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "348645f6-8aa6-4f5d-8f44-2c8dc3adfd2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3400b04-357a-478f-8caf-405fd50f253c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "348645f6-8aa6-4f5d-8f44-2c8dc3adfd2f",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "ae64a916-f164-46d6-991b-14224bfcf660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "c7bd4fd7-2a75-49e6-9db5-0c0eec1dc997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae64a916-f164-46d6-991b-14224bfcf660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d0ac03c-dccc-4c5d-b656-5e4e484c349f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae64a916-f164-46d6-991b-14224bfcf660",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "4889b0e3-38cd-462b-a01c-7f9adad5a6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "0f232c5e-456c-41f8-ac25-667bc4ef8d5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4889b0e3-38cd-462b-a01c-7f9adad5a6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "653ea39d-41cb-4d3b-90ef-e3ca9ef979c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4889b0e3-38cd-462b-a01c-7f9adad5a6f7",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "ad773db2-11c4-49ed-90bf-127121450f4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "0c6189d2-e665-4f59-aa5c-e0c3c8dd2b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad773db2-11c4-49ed-90bf-127121450f4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c05322-6689-4737-997a-514419075f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad773db2-11c4-49ed-90bf-127121450f4f",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "adbe965f-d215-4d2f-88b3-975a82c516b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "0b5fa974-b097-4aad-af0e-cb4796ea1e88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adbe965f-d215-4d2f-88b3-975a82c516b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d352a5e-8200-4b26-83b2-c1110a821744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adbe965f-d215-4d2f-88b3-975a82c516b8",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "f9e54d33-2c26-47f8-8191-5d679051762d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "7129aac1-8a5c-4547-b460-dbef1c419dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9e54d33-2c26-47f8-8191-5d679051762d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07ca8b8f-6b41-45cb-aef0-cc70ad05f3f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9e54d33-2c26-47f8-8191-5d679051762d",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "df3952c1-e6a8-44a4-a1b3-50fb967c003a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "83b78828-f084-4330-8262-b5f72387936d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3952c1-e6a8-44a4-a1b3-50fb967c003a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8dd22b-e0f1-479f-9fde-726da411868d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3952c1-e6a8-44a4-a1b3-50fb967c003a",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "1200337a-c103-4785-8d99-5efbff3b203b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "a3d744a7-9d16-4c6f-af13-a751701cc069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1200337a-c103-4785-8d99-5efbff3b203b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "514e9dcb-a027-4b57-8974-dd074ad63c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1200337a-c103-4785-8d99-5efbff3b203b",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        },
        {
            "id": "84b91b38-1fc6-4c19-b082-a0e843d8e9ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "compositeImage": {
                "id": "22a0ee3b-8a8d-48ea-b0f2-7c74fa9776fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b91b38-1fc6-4c19-b082-a0e843d8e9ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8576334-a2ab-44ab-a815-5e23f0cb8f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b91b38-1fc6-4c19-b082-a0e843d8e9ff",
                    "LayerId": "67b70118-5839-436c-9bee-63aaf1455c19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "67b70118-5839-436c-9bee-63aaf1455c19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df158df0-ed9a-45e4-807b-7977e31e074e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 48,
    "yorig": 66
}