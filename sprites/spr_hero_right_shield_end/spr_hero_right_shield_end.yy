{
    "id": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_shield_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 49,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fceb6bd4-c2e2-47f9-9254-21e29523af2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "f2db62d1-2e76-41b6-a5c8-17602f9ff887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fceb6bd4-c2e2-47f9-9254-21e29523af2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2106951-e7f5-4976-afff-d9ae52c7702d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fceb6bd4-c2e2-47f9-9254-21e29523af2e",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "1139e44e-59fe-49da-bccc-4ca3c46be891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "58058a6e-b588-42ab-bfec-6e42a56461df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1139e44e-59fe-49da-bccc-4ca3c46be891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7120dcfc-7237-4bd3-929b-0cb6d1bca717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1139e44e-59fe-49da-bccc-4ca3c46be891",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "b37877fa-328d-4bc2-8bfe-ee6cc1bea262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "c0df0a8b-a801-4d6c-bc9d-616c3af007c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b37877fa-328d-4bc2-8bfe-ee6cc1bea262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c8f09da-837a-4795-84a2-dc9cc3267449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b37877fa-328d-4bc2-8bfe-ee6cc1bea262",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "dc6fa411-8f5c-4881-8424-5e5421c57283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "fdfec3c9-26db-4e18-aeda-add91d6ddb62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc6fa411-8f5c-4881-8424-5e5421c57283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f044564-4d93-4f2f-932c-f0631090e7ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc6fa411-8f5c-4881-8424-5e5421c57283",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "d85c55f3-7315-455b-b8db-d0fb176816d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "3f48c07d-3d90-4e81-b9a1-86c935e257d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d85c55f3-7315-455b-b8db-d0fb176816d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7a3a886-161a-44e2-b15e-99213c0363d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d85c55f3-7315-455b-b8db-d0fb176816d3",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "a7e93517-3a0c-4899-b48b-4328ba963a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "fd717fcc-3072-411f-9039-50050d179a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e93517-3a0c-4899-b48b-4328ba963a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aac1893-4b37-411b-942e-65744df03b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e93517-3a0c-4899-b48b-4328ba963a12",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "5a74450d-84d4-427e-b716-2c7a69e65545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "b9f1d050-d36f-4bea-95d1-abf4a912014c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a74450d-84d4-427e-b716-2c7a69e65545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29118952-3b0b-46c6-80b9-f6786066d01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a74450d-84d4-427e-b716-2c7a69e65545",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        },
        {
            "id": "f44ccaca-e3f8-46be-a7a8-20ffa46fffd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "compositeImage": {
                "id": "62836230-c05e-4405-afa1-6251f734259a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44ccaca-e3f8-46be-a7a8-20ffa46fffd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf18943a-ef6a-40be-8237-e41c97367dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44ccaca-e3f8-46be-a7a8-20ffa46fffd7",
                    "LayerId": "9c0b148c-30e0-494f-ab1c-23fca84844da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c0b148c-30e0-494f-ab1c-23fca84844da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6d5bcb-d6ac-4329-8d5d-91337570189e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 34,
    "yorig": 36
}