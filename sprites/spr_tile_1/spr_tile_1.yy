{
    "id": "8aa96ca6-eab3-444c-b868-04c3acd0a188",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 96,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "645ad120-62fa-4a73-bf83-0b73dbf7b143",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8aa96ca6-eab3-444c-b868-04c3acd0a188",
            "compositeImage": {
                "id": "88c901d4-f441-48f0-a004-c8cc9c391f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "645ad120-62fa-4a73-bf83-0b73dbf7b143",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b063c4e-c810-4448-9beb-ba0cb6c39b10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "645ad120-62fa-4a73-bf83-0b73dbf7b143",
                    "LayerId": "b2b2793c-0c43-4e16-9bd6-ec75f9c93d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b2b2793c-0c43-4e16-9bd6-ec75f9c93d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8aa96ca6-eab3-444c-b868-04c3acd0a188",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}