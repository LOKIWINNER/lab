{
    "id": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_attack_mask_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ef39811-c121-492a-a464-223c6b5e3f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "e7e619e8-e15b-4c1e-b847-f4f7473e5f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef39811-c121-492a-a464-223c6b5e3f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5118cac7-0953-49d3-bccb-696766374d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef39811-c121-492a-a464-223c6b5e3f19",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "9e5b0bf6-28c3-46ad-81bc-1c2fd841b76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "19645cde-8cfe-4af3-ae8f-804f4df6531d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e5b0bf6-28c3-46ad-81bc-1c2fd841b76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "108333b0-2d9b-4fc9-9c41-79dca7f79e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e5b0bf6-28c3-46ad-81bc-1c2fd841b76f",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "dd8d9dab-bb4b-4975-9c2a-1c457784b561",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "b16ca421-c228-4e88-bf74-c30ec91cedff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8d9dab-bb4b-4975-9c2a-1c457784b561",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1eea52b-1fe3-46fe-935e-125b4050d82f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8d9dab-bb4b-4975-9c2a-1c457784b561",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "13fdd690-c3d8-4ec1-832a-ff57a393e2d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "c3ed445b-39b6-4c09-baa5-a0520e9e3bf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13fdd690-c3d8-4ec1-832a-ff57a393e2d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b0f525d-e500-4c07-913e-ec3f3ddb14f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fdd690-c3d8-4ec1-832a-ff57a393e2d2",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "1662aee5-e4cb-4bd9-9957-26a05c9cfa8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "95bbb729-36cc-4544-8713-efcbfa157a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1662aee5-e4cb-4bd9-9957-26a05c9cfa8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383b4607-e5c9-4a8f-b5f1-55b7f127c895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1662aee5-e4cb-4bd9-9957-26a05c9cfa8e",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "96aad38b-4e41-470b-9024-74e6f2ba6776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "59d8eb4c-7e1d-4390-9979-a82fdd5d7b98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96aad38b-4e41-470b-9024-74e6f2ba6776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f0d0211-46d7-49c2-8fc1-19841b494de4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96aad38b-4e41-470b-9024-74e6f2ba6776",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "56b8e6c3-7f38-4e3d-bbea-7967c30ed6ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "a889b60f-9f83-46ed-8e2f-cca219f4913d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56b8e6c3-7f38-4e3d-bbea-7967c30ed6ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8955c68-23fe-4fb7-abee-2373223159fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56b8e6c3-7f38-4e3d-bbea-7967c30ed6ce",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "5e7b00de-2746-453d-af2e-8e99a5cc4eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "0688049a-54a5-46ff-8e1c-63bdc2eb634a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7b00de-2746-453d-af2e-8e99a5cc4eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed6bf6d-fad7-48d7-8561-3f2f1799f1e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7b00de-2746-453d-af2e-8e99a5cc4eef",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "45473d53-fc60-4a73-8a95-dd4e3fbf2afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "de33e638-8817-48e0-810f-af8700218d86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45473d53-fc60-4a73-8a95-dd4e3fbf2afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bcba2f6-caad-4809-88b0-5b2e7e1fc511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45473d53-fc60-4a73-8a95-dd4e3fbf2afc",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        },
        {
            "id": "ad2aa4e8-7ba1-4954-b672-4d23189fecf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "compositeImage": {
                "id": "188bdb33-ab11-4dd8-b818-f4b64147c4e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2aa4e8-7ba1-4954-b672-4d23189fecf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddcccfa-d44a-4ef6-8d51-67d88eed8e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2aa4e8-7ba1-4954-b672-4d23189fecf9",
                    "LayerId": "ff3bcadd-9114-4534-af75-54562c3f2d1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff3bcadd-9114-4534-af75-54562c3f2d1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0436d0a-4ad0-46ce-8233-606a2fd7af30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}