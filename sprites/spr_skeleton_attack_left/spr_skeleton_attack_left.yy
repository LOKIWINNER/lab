{
    "id": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_attack_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbe3b936-f93e-44a7-957a-1460a7a1fd18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "fec8d676-fc1e-4cee-a33c-da116005388a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbe3b936-f93e-44a7-957a-1460a7a1fd18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b440ae61-b404-4a43-9b2f-4c37f048afbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbe3b936-f93e-44a7-957a-1460a7a1fd18",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        },
        {
            "id": "d1045cde-2200-49fc-b91d-214a2c0e5843",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "27a5b4f9-ad8e-47cf-b47f-932a613c6798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1045cde-2200-49fc-b91d-214a2c0e5843",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6396d6f2-7231-4891-83b3-d6acf9fea9d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1045cde-2200-49fc-b91d-214a2c0e5843",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        },
        {
            "id": "d4c54cb4-d7a1-48c6-a3c5-c11fca9ed0a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "6545c5e2-ef15-40b8-a4af-b7417d329673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c54cb4-d7a1-48c6-a3c5-c11fca9ed0a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13dc883d-5899-4ac0-aad2-45387a58cec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c54cb4-d7a1-48c6-a3c5-c11fca9ed0a7",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        },
        {
            "id": "c70d1728-a2ec-4b09-936b-06d9e69afa49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "60a3f4d3-addb-42e3-9c09-1dbdeb5f78a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c70d1728-a2ec-4b09-936b-06d9e69afa49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c18071c4-49ad-4c7b-a66d-f79d8de7f4a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c70d1728-a2ec-4b09-936b-06d9e69afa49",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        },
        {
            "id": "0138d04f-0166-408a-9618-e9432132da85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "2e7acdce-3c3b-4bdf-9eb9-2fb7be6d4308",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0138d04f-0166-408a-9618-e9432132da85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b244f296-efd7-474e-8e65-6060c9e69f6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0138d04f-0166-408a-9618-e9432132da85",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        },
        {
            "id": "67fbea67-fa6b-437f-ba7a-6f363befd2b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "18a54185-c21a-4e2f-bbd0-dd538fb6f96a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67fbea67-fa6b-437f-ba7a-6f363befd2b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fcb1b22-4201-4ef0-adcc-3cc9601bebb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67fbea67-fa6b-437f-ba7a-6f363befd2b9",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        },
        {
            "id": "f6021f5e-1143-4efc-aa47-1ebd0abef2b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "compositeImage": {
                "id": "1c9894f5-b396-4770-a9c4-7ba02eb712fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6021f5e-1143-4efc-aa47-1ebd0abef2b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e487987-df3d-4667-8684-057ca4c66ee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6021f5e-1143-4efc-aa47-1ebd0abef2b6",
                    "LayerId": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dbf32d41-9086-4b68-9a9e-4e9f29a7e83f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d3ec1ff-359c-4dde-b3ac-05045ab7b9f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}