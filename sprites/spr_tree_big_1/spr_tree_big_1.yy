{
    "id": "3c59cf4f-f129-4ac6-93e5-4671ce661cab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_big_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 292,
    "bbox_left": 82,
    "bbox_right": 176,
    "bbox_top": 244,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e79bf9d6-944f-440c-87e3-294371168dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c59cf4f-f129-4ac6-93e5-4671ce661cab",
            "compositeImage": {
                "id": "9eb5b70b-ecc6-485d-a183-abd7387e033d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79bf9d6-944f-440c-87e3-294371168dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd0df9b-f411-45ca-859e-7c05192e83db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79bf9d6-944f-440c-87e3-294371168dd6",
                    "LayerId": "6af1cc1c-671f-4518-96e3-8a49c222ab09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "6af1cc1c-671f-4518-96e3-8a49c222ab09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c59cf4f-f129-4ac6-93e5-4671ce661cab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 125,
    "yorig": 262
}