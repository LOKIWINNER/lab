{
    "id": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_shield_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 19,
    "bbox_right": 40,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e068c6ea-4d6c-4c4d-ae60-2aed6c7067a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "e63eb275-413b-4fd7-8f99-507112065b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e068c6ea-4d6c-4c4d-ae60-2aed6c7067a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ccd3e20-f4b2-4d9e-98e8-c6b4add7cc74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e068c6ea-4d6c-4c4d-ae60-2aed6c7067a4",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "8ea5c3cd-cb4d-4433-afab-d11b6bde0641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "bc3a182f-7f42-4753-b1dd-54730a79357a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea5c3cd-cb4d-4433-afab-d11b6bde0641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e7fc4ef-2943-4d9a-93ea-55bf612f8011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea5c3cd-cb4d-4433-afab-d11b6bde0641",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "aaf1fd5b-29a7-4667-827a-facfe1e0bb16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "3c522082-8f1b-4e6f-a000-82bf1090f4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf1fd5b-29a7-4667-827a-facfe1e0bb16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52a2129-ce0f-4bdd-9147-34eb124ee3c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf1fd5b-29a7-4667-827a-facfe1e0bb16",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "5564697b-752b-4eaf-8557-ddd85956d86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "749a1058-cae3-4212-a84b-70f1b78e5a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5564697b-752b-4eaf-8557-ddd85956d86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9004cd6e-7539-406d-b581-5547c9f879eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5564697b-752b-4eaf-8557-ddd85956d86e",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "019511a5-ff32-4411-8e24-70ec2cc7101b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "0efa40ff-17cb-4157-966a-07113beb099f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019511a5-ff32-4411-8e24-70ec2cc7101b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf88bb52-e04b-4e47-a8f7-58ab1eedeb21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019511a5-ff32-4411-8e24-70ec2cc7101b",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "55592250-b07b-4db7-b34b-b6691bc24b9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "196d62c3-ad9d-4a3a-b3a3-49383cc7819e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55592250-b07b-4db7-b34b-b6691bc24b9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb45b284-cdd1-478d-af63-60a5ed732cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55592250-b07b-4db7-b34b-b6691bc24b9c",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "8b8d1988-9991-458d-9889-908ed71358fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "b940f1a6-7c2a-46e1-8762-23acd7a5cbbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b8d1988-9991-458d-9889-908ed71358fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e4304b0-2e6c-4b8f-9b45-4c07627629e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b8d1988-9991-458d-9889-908ed71358fe",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "012b0fc7-0c85-4196-b06a-1e7f06d224de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "8a59b531-4a20-4a80-917d-2647a1ef4edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "012b0fc7-0c85-4196-b06a-1e7f06d224de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "590c24f6-2c88-4859-bbbf-1617e760062f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "012b0fc7-0c85-4196-b06a-1e7f06d224de",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "ab07bde9-b713-4ff5-92b9-c56939960839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "51e13e8f-2682-4eed-ad1b-4c11e272952a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab07bde9-b713-4ff5-92b9-c56939960839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd59f4bf-1942-4ead-a940-497a183da6ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab07bde9-b713-4ff5-92b9-c56939960839",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "a9dbd4c2-fdf0-41de-9a93-b7ed8a2eaf99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "4f51a0e1-515e-4a8b-8299-665ede9b92d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9dbd4c2-fdf0-41de-9a93-b7ed8a2eaf99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "879f9a78-3940-448f-8978-e00d1dcdbd10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9dbd4c2-fdf0-41de-9a93-b7ed8a2eaf99",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "3b8f7bbc-e0a6-446c-8edf-b5a7658c840e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "f41befee-9d8a-40fb-910a-b1a288d86e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8f7bbc-e0a6-446c-8edf-b5a7658c840e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd4656c-d002-4464-8cef-b3ec73052c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8f7bbc-e0a6-446c-8edf-b5a7658c840e",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "98fa4fa2-5354-4b33-90f2-55fc480b4cce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "7f0f6994-39f4-461d-a997-4bfc612485fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fa4fa2-5354-4b33-90f2-55fc480b4cce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eac70b1-6707-45eb-b320-8c19c5b8cec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fa4fa2-5354-4b33-90f2-55fc480b4cce",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "d8e08242-a43f-4485-933f-cad6d1e82d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "57c996b3-949b-4698-bdcb-40d4fd09a0cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e08242-a43f-4485-933f-cad6d1e82d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e081cc-9a8b-4558-a903-b2cab90f899d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e08242-a43f-4485-933f-cad6d1e82d3a",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "f4277b6c-0162-4fe9-b2f3-be096ad445fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "131bf8d1-bc91-497c-b143-90272fd51aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4277b6c-0162-4fe9-b2f3-be096ad445fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880fee4e-6f92-4fa9-80e9-80802e11adb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4277b6c-0162-4fe9-b2f3-be096ad445fd",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        },
        {
            "id": "bdd1e52e-bfe5-4672-9653-037387fbf76e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "compositeImage": {
                "id": "d2383a07-57d0-49e3-b5d3-fcf3e7692ad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd1e52e-bfe5-4672-9653-037387fbf76e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c2c645-f98e-4a12-95f0-bce57a3a76b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd1e52e-bfe5-4672-9653-037387fbf76e",
                    "LayerId": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "02a4a7e1-6478-411d-9795-6dd5f8f6fd59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a2b7849-f919-4acf-af00-c9ce0f3f578e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}