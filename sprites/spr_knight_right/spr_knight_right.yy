{
    "id": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 7,
    "bbox_right": 127,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a04ab80-e690-4c6a-b284-7a7751987915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "86d18127-c08a-4ed7-8ed0-c0a9a6dabce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a04ab80-e690-4c6a-b284-7a7751987915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db130ee-bcfa-4ac7-b489-7ad77d799595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a04ab80-e690-4c6a-b284-7a7751987915",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "384029ea-dfff-4357-84c1-00d5314e5b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7451333e-72da-41f0-9357-2b538017c13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "384029ea-dfff-4357-84c1-00d5314e5b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80a6751a-6e65-4ab3-88b3-504446393510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "384029ea-dfff-4357-84c1-00d5314e5b28",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "83914b29-7482-40e8-9865-662d53607023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "b1adacc2-ad3e-4faa-bc04-9ef8be2f19ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83914b29-7482-40e8-9865-662d53607023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdecfea1-9057-424e-a8bf-7a43022a6c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83914b29-7482-40e8-9865-662d53607023",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "97e36781-812a-4816-aef0-3ffe7310643c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "d91f9243-d460-425a-a808-75b3c0e25a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e36781-812a-4816-aef0-3ffe7310643c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b4360b-1b33-4835-8094-2798f0c6a5c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e36781-812a-4816-aef0-3ffe7310643c",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "70924184-2e28-49e9-8645-bd56ce0c9308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "cc8c45a5-5b57-4cdd-b5f7-832fe66b6fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70924184-2e28-49e9-8645-bd56ce0c9308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1cb6384-f628-4f6e-8764-824b016cdb65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70924184-2e28-49e9-8645-bd56ce0c9308",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "18f55e2b-1b81-4066-b5b4-987b7b102139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "b487fc25-2f03-4650-be88-9cc6ecc4e16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f55e2b-1b81-4066-b5b4-987b7b102139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76d2431b-0e2e-4bcd-aa2a-9235c689a6d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f55e2b-1b81-4066-b5b4-987b7b102139",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "6c31cafb-1bb2-4220-b14c-2f75783a6fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "8f48b586-124a-4eea-bea8-41c91da4a8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c31cafb-1bb2-4220-b14c-2f75783a6fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a73418d-27b5-4eee-a3bb-da9589cd35b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c31cafb-1bb2-4220-b14c-2f75783a6fc8",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "25c3dd84-22b7-4641-b9a3-28255943cea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "80c9478c-6c75-46cc-9c24-3c20b06fe2c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c3dd84-22b7-4641-b9a3-28255943cea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c3ab6bc-eb88-4960-952a-19d798cc65f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c3dd84-22b7-4641-b9a3-28255943cea6",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "64f5fe1f-136f-421e-b14f-b14937565fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "938bc08f-cb4e-4f30-ab92-f0f049da89cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64f5fe1f-136f-421e-b14f-b14937565fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd2a70b5-60d3-4588-bea7-e68ab495d4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64f5fe1f-136f-421e-b14f-b14937565fef",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "785e23a9-ff5e-456a-b1af-2a0964b1adf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "250ace65-3016-4b01-b22d-facbe298cf0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "785e23a9-ff5e-456a-b1af-2a0964b1adf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc549888-05bc-4b95-b6a6-af6dd1710555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "785e23a9-ff5e-456a-b1af-2a0964b1adf7",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "6637bcb1-5c7c-4f07-8c3d-9d02dc490401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "fba2c14f-8ccb-409e-91ee-8d3cb3d7ea7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6637bcb1-5c7c-4f07-8c3d-9d02dc490401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0e8521-f75f-4385-9b8c-50adc5203267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6637bcb1-5c7c-4f07-8c3d-9d02dc490401",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "15b101af-ff55-4cae-9159-5de14003e8cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "69eb55a8-6baa-4f40-9495-1593af3b8594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b101af-ff55-4cae-9159-5de14003e8cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a014be6d-71bf-4b50-aacd-932a77f2a595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b101af-ff55-4cae-9159-5de14003e8cf",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "ff5ee990-ad8d-4e08-a680-23fa1eb31ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "ef176547-7d77-4278-b872-7649f9dd1e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5ee990-ad8d-4e08-a680-23fa1eb31ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed6615c-893f-4b69-9294-ef3ccec2ef03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5ee990-ad8d-4e08-a680-23fa1eb31ced",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "67bee9c7-f88a-43b2-a781-bd960e3ed0e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "a0afdd4d-cf8a-4b7b-a825-791e070c2bd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67bee9c7-f88a-43b2-a781-bd960e3ed0e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e190aad9-890b-4a10-8514-ef0e6b9c5e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67bee9c7-f88a-43b2-a781-bd960e3ed0e3",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "a1d3046f-6422-4411-b8e0-256285b01eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "2a649f03-1ed8-47f6-a94e-5003175cd5c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d3046f-6422-4411-b8e0-256285b01eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68f89c16-57ff-4876-99a2-99b2d63a13be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d3046f-6422-4411-b8e0-256285b01eda",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "a55e9bbb-4e69-4e0b-8cd4-e9f9a9cf0d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "d3baac26-9434-4372-a30d-ba4acd061ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55e9bbb-4e69-4e0b-8cd4-e9f9a9cf0d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7a0b0a0-c735-4d12-87a4-137296576758",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55e9bbb-4e69-4e0b-8cd4-e9f9a9cf0d88",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f179a155-41f8-43d8-9a8d-0514a4a058fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "b88cccbb-347d-4549-ad49-44eed782b22f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f179a155-41f8-43d8-9a8d-0514a4a058fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68469425-ba43-456d-b6bd-3dba36ba75bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f179a155-41f8-43d8-9a8d-0514a4a058fd",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "5a65fc8d-74b0-41d5-a167-08ffb626818f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "836b7207-c5ea-4d21-8900-aafabc4ad1b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a65fc8d-74b0-41d5-a167-08ffb626818f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d7d36e-6495-40c3-99e0-9a7c7e364ff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a65fc8d-74b0-41d5-a167-08ffb626818f",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "db6e2e35-2c28-4cef-9de9-344cb56dac5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "02806004-4fe9-447a-b215-0e66565bfc24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6e2e35-2c28-4cef-9de9-344cb56dac5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd45d4ff-496a-48e1-84d3-e64a7d7a9bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6e2e35-2c28-4cef-9de9-344cb56dac5b",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f6f9139b-f85b-4eb3-97f7-22027247e500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "e9c63e16-98ea-463d-82f6-24c66f2234f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f9139b-f85b-4eb3-97f7-22027247e500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20dd56b7-aeb8-4a32-8356-5dbd2abf51a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f9139b-f85b-4eb3-97f7-22027247e500",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "cc83dc92-a808-44f2-a842-8dc330d36072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "512d7a8b-5ea2-4b3a-83f7-36ab095304a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc83dc92-a808-44f2-a842-8dc330d36072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66311bfe-55a8-4529-b284-c3615bb7d8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc83dc92-a808-44f2-a842-8dc330d36072",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "4fec64dc-c997-4792-b617-70fd94aa9889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "b19a139c-fe74-4fb1-82f4-ce0f26f8c1c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fec64dc-c997-4792-b617-70fd94aa9889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8333d66-45a8-46d4-b720-dd8ac0780793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fec64dc-c997-4792-b617-70fd94aa9889",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "01887a98-450d-4800-ba0b-5973ce7cf30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "109c7463-d262-4e10-a63a-a0ef44a24664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01887a98-450d-4800-ba0b-5973ce7cf30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b93aeaf7-363e-482c-9dc1-0a49844a7da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01887a98-450d-4800-ba0b-5973ce7cf30e",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "1124654b-4c11-4aa0-a785-8704ef6dedf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "5864a651-3d33-4513-ad36-8099f01f90a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1124654b-4c11-4aa0-a785-8704ef6dedf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ee62f74-c6db-41c7-99b3-07e389d3f3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1124654b-4c11-4aa0-a785-8704ef6dedf3",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "fad80a1d-9c17-47c9-bd0d-44b7337a752b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "fea05ae7-bdde-4df5-96bc-fd7b673b778f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fad80a1d-9c17-47c9-bd0d-44b7337a752b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5fb5912-7f1d-45d0-89f4-0c02726bc344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fad80a1d-9c17-47c9-bd0d-44b7337a752b",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "63a00c73-fcee-4ce3-9194-a82b48710205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "cb6859e2-f45f-4065-93ec-5dd1a459d62f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63a00c73-fcee-4ce3-9194-a82b48710205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208a89ed-fd54-4e39-b622-149c9b802ce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63a00c73-fcee-4ce3-9194-a82b48710205",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "8997e25d-5d4a-440d-b167-2ac59426b38d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "9e6ee12c-1f55-4705-8006-3a16a38d125e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8997e25d-5d4a-440d-b167-2ac59426b38d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e63e9a7a-bb44-4104-b058-2a2dcabf7df6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8997e25d-5d4a-440d-b167-2ac59426b38d",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f03daf01-8ca2-4698-acf1-f37de0af6899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "f25c02d4-1bf1-4dc1-8b3b-4231c574e5c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03daf01-8ca2-4698-acf1-f37de0af6899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafde321-0aa2-4722-8dad-bce88db9e172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03daf01-8ca2-4698-acf1-f37de0af6899",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "3d7ffa97-6925-490e-9374-9871d1d2fa38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "25f805d1-6271-4221-bab0-58a1d0e97087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d7ffa97-6925-490e-9374-9871d1d2fa38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea1898e-08ca-4345-85f5-90cf7d97688d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d7ffa97-6925-490e-9374-9871d1d2fa38",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "cc2a82e3-7290-4786-aa83-dadaa5c8b88d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7bd1c0de-e496-4fdc-99fe-2f342a7173b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc2a82e3-7290-4786-aa83-dadaa5c8b88d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5492583-f5e5-413f-9e5f-e86ed96e5eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc2a82e3-7290-4786-aa83-dadaa5c8b88d",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "1df9bf52-69b9-4600-aea1-bbd291751f0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "79aad061-ffea-42db-9b5f-e1a2931af95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df9bf52-69b9-4600-aea1-bbd291751f0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "403661e4-9543-4d1d-9777-c20fa7fa3737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df9bf52-69b9-4600-aea1-bbd291751f0c",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "12a932d2-0cdf-4d58-902e-ff02ceaddbc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "71a0f439-b2f4-470e-bf96-27ee18c6a248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12a932d2-0cdf-4d58-902e-ff02ceaddbc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d688e472-4e26-4b35-bf7c-b11944958a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12a932d2-0cdf-4d58-902e-ff02ceaddbc8",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e203c4fd-1496-4b51-889f-cf026ea597b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "71bff757-2a42-4d47-ac69-04d40ff8ce2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e203c4fd-1496-4b51-889f-cf026ea597b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d1a867-9f56-4acf-b311-ffded899388a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e203c4fd-1496-4b51-889f-cf026ea597b4",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "866864f1-96b5-4c91-b19a-e8682c46ca2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "3b2577e6-2571-4f04-af2b-3cbce1c837c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866864f1-96b5-4c91-b19a-e8682c46ca2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b239d172-b2f5-4fab-881d-40fee13a649c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866864f1-96b5-4c91-b19a-e8682c46ca2e",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "9eb48f30-527b-49d0-a91f-d608576f2346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "69ff1131-4f96-46be-9acc-17f018bb95ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb48f30-527b-49d0-a91f-d608576f2346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116a134f-ecfa-41a6-b6d3-38e91b8f2dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb48f30-527b-49d0-a91f-d608576f2346",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "4f93f651-0af4-4726-8050-ec8cc76b8d14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "da039a81-5aaa-453e-9558-3b6ce1c0b748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f93f651-0af4-4726-8050-ec8cc76b8d14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e7118c-86c4-40a4-aac1-cef5e43eb682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f93f651-0af4-4726-8050-ec8cc76b8d14",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "88f54923-2ec3-4b16-89a0-0fbb864b1893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "8e06d67d-d360-4b25-a88f-18f10fd4aabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f54923-2ec3-4b16-89a0-0fbb864b1893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f10047-6fc6-4cee-a1a1-25227c3afff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f54923-2ec3-4b16-89a0-0fbb864b1893",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "c40d94de-4b32-4dcf-8359-a8da9dfec874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "f6a2dc66-2914-489c-bb89-8666914505ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40d94de-4b32-4dcf-8359-a8da9dfec874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39145695-38f2-4deb-ba88-bba6dc78a1fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40d94de-4b32-4dcf-8359-a8da9dfec874",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "eef607a4-84bd-415f-987e-eed98f3a88b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "faedcc71-c928-4ef8-a867-caa2c3c3d5ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef607a4-84bd-415f-987e-eed98f3a88b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50129a4-353d-4998-8afc-3bd050f9ba46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef607a4-84bd-415f-987e-eed98f3a88b9",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "1d424a3a-e3f5-4f7a-aa0a-0dcd81648bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "4800b4ef-e7dd-4e99-b3c9-35306650cb16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d424a3a-e3f5-4f7a-aa0a-0dcd81648bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaca3dfa-4d5c-4fa4-b8b9-c5c2bb3c6691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d424a3a-e3f5-4f7a-aa0a-0dcd81648bee",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "eddb4ca2-6d4b-466d-a609-8cf64416f377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "bbf7164c-44bd-4e9b-aed1-8b8bf29a02df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eddb4ca2-6d4b-466d-a609-8cf64416f377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfae68c0-3afe-4622-982c-711d18955bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eddb4ca2-6d4b-466d-a609-8cf64416f377",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "bbd0a6f4-8282-4618-a082-f448313e09aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "05b0545b-af39-40b6-bad2-326510f74f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd0a6f4-8282-4618-a082-f448313e09aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d842754-7205-42c1-8783-c99ce2caefd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd0a6f4-8282-4618-a082-f448313e09aa",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "fa804546-7bf1-406b-8b3b-fbddd8e56c02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "b11efb23-4c5b-4dc1-a08b-ba7fc7dfe61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa804546-7bf1-406b-8b3b-fbddd8e56c02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "181ef494-9390-4a97-8954-ee47d75789ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa804546-7bf1-406b-8b3b-fbddd8e56c02",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "6022183a-c10b-479b-9f8b-283086836284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "74bd2211-27c6-4d86-8058-0306d9494ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6022183a-c10b-479b-9f8b-283086836284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f3ea47-691c-4064-a76c-65b63f73e8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6022183a-c10b-479b-9f8b-283086836284",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "18fccaa9-dc90-47e0-939f-24201e0b0d2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "e538fbc9-8b68-44e6-885b-94bf6876618d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18fccaa9-dc90-47e0-939f-24201e0b0d2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2495967e-8a77-45b0-a71a-db9dee64dbdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18fccaa9-dc90-47e0-939f-24201e0b0d2c",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "4386cec2-bd31-42ef-bdc7-52691e66e41b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "3f8db609-341b-41f0-b846-d0ca2f7f7e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4386cec2-bd31-42ef-bdc7-52691e66e41b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d6f817-a5b5-4827-8033-7f642dc3ce0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4386cec2-bd31-42ef-bdc7-52691e66e41b",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "7bced462-153f-4f2a-8cc4-6fe3071469f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "64afd523-84f5-4e8e-b968-a844ab75e068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bced462-153f-4f2a-8cc4-6fe3071469f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da6f092-c61d-4100-95b8-bcc24444e899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bced462-153f-4f2a-8cc4-6fe3071469f1",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "6a7b88c9-3e03-4801-a600-fd1968446dc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "2230c2d7-2e49-41dd-8bab-e18d41e57146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7b88c9-3e03-4801-a600-fd1968446dc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78e9baf2-e5af-4280-8849-f1d699b15cc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7b88c9-3e03-4801-a600-fd1968446dc5",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "132e78ca-cd85-4b52-bf7e-aa03c860de87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "1a547662-b4f8-43f7-963b-37a24ed160bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132e78ca-cd85-4b52-bf7e-aa03c860de87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9490a5cb-0846-428a-aaa4-ed0cf31da87d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132e78ca-cd85-4b52-bf7e-aa03c860de87",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "9bdd1c88-21c2-4f3a-8158-afed78069920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "4052e12b-1c97-4ab8-ac3f-3e877bfa626c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bdd1c88-21c2-4f3a-8158-afed78069920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "298a5aa9-aae5-4c47-9a31-efb1ab61006c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bdd1c88-21c2-4f3a-8158-afed78069920",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "788f1673-b5bc-4f99-8c8c-80ff77a930e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "b0e717ad-6eda-4dc4-a6d6-44eb3c2bd376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "788f1673-b5bc-4f99-8c8c-80ff77a930e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86152505-41e1-42bf-a2f9-7db4d8695fb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "788f1673-b5bc-4f99-8c8c-80ff77a930e3",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "cf40f394-6a7d-48bc-926d-67f5e63e2df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "a32c82da-abe4-40b2-9943-09088f32fbd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf40f394-6a7d-48bc-926d-67f5e63e2df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c262c6fb-8b97-4338-9cb1-080f017a4dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf40f394-6a7d-48bc-926d-67f5e63e2df4",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "c90dc28a-44c9-4622-8e05-d8c8c814bfc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "d4539fe3-b30d-4a54-8fae-abbc1c32506c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c90dc28a-44c9-4622-8e05-d8c8c814bfc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8ef6af0-d02b-442b-96de-06b4487e4a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c90dc28a-44c9-4622-8e05-d8c8c814bfc9",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "dee06511-cf6a-4593-8fd7-d41a000434e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7a154838-02e2-4279-9f3e-b41a6ee6562f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee06511-cf6a-4593-8fd7-d41a000434e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66d9ebe1-03b4-49a5-8aba-951da9a7dc5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee06511-cf6a-4593-8fd7-d41a000434e2",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "29fe0a5e-72a2-448e-a63e-b2d1e2dd43d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "806d727e-f92a-4735-903f-6a0fb2173cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29fe0a5e-72a2-448e-a63e-b2d1e2dd43d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d01fccd-ec04-4b71-a421-73848d625eb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29fe0a5e-72a2-448e-a63e-b2d1e2dd43d8",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "a854c6c3-b5ba-4384-bd20-5ed2a9508be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "78d0ab5d-597d-4c74-90b3-d13c0a7dda8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a854c6c3-b5ba-4384-bd20-5ed2a9508be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df98b7c9-1e04-4a7f-afbd-0f1091677b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a854c6c3-b5ba-4384-bd20-5ed2a9508be9",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e92328e3-5d5d-47f3-8240-886ba787b395",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "6af520e2-b772-4d55-bc14-e85c4caa2194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e92328e3-5d5d-47f3-8240-886ba787b395",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bffca15a-9ec3-41bd-bf99-0ee31b72b54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e92328e3-5d5d-47f3-8240-886ba787b395",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "66256f38-28d9-4d70-a61a-56c6522a430f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "d76e65e6-809e-43de-bcad-a16b16c44ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66256f38-28d9-4d70-a61a-56c6522a430f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cacd8d1d-9427-457e-8b96-a2be0edb1e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66256f38-28d9-4d70-a61a-56c6522a430f",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "fa1a5090-e065-4cb9-aa46-367f9d6c2ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "61f74aab-77ca-4b74-a453-e51930170a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1a5090-e065-4cb9-aa46-367f9d6c2ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e411b6b-39ab-4afd-8d15-2dfc4cf47f94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1a5090-e065-4cb9-aa46-367f9d6c2ded",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "95130b7d-8d35-410e-92c3-a818b1b6bafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "4457fed8-5aee-45fa-84a0-dc196386b2bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95130b7d-8d35-410e-92c3-a818b1b6bafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86a79c9-38f5-46c3-b76a-11d55125b442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95130b7d-8d35-410e-92c3-a818b1b6bafa",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "af28292e-ec91-42c9-8a71-cda2274a455c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "2ddf44a0-1053-4b6d-addc-481eeb6cb660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af28292e-ec91-42c9-8a71-cda2274a455c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "570878d6-e667-479c-98aa-f49d18cb864b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af28292e-ec91-42c9-8a71-cda2274a455c",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f322226b-d7c4-4153-bf2b-2a54764a5429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "28187b08-2284-41f5-be46-3485445d1d99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f322226b-d7c4-4153-bf2b-2a54764a5429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da49dee-0c90-490b-b9f6-96d4f62040ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f322226b-d7c4-4153-bf2b-2a54764a5429",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "a2290890-cf0f-463f-93a0-9add59fd0339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "bbf79968-f5fe-47ab-ab01-6b141696fa19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2290890-cf0f-463f-93a0-9add59fd0339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a9f0cb-89df-4675-b014-7fc55e964a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2290890-cf0f-463f-93a0-9add59fd0339",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "c03b7522-389b-4ce9-9493-979d76cb9698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "c1f985c6-5e61-49f9-bc81-c06807e8f59e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c03b7522-389b-4ce9-9493-979d76cb9698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebd7f554-1ea4-420f-931b-1b56313098e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c03b7522-389b-4ce9-9493-979d76cb9698",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "76ac1e3b-8711-47c2-b02c-57d2472e4582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "9f3f7841-4408-46f8-b36e-e7d2222d1d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ac1e3b-8711-47c2-b02c-57d2472e4582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9ce99e-9976-4d50-a550-7cc733552eeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ac1e3b-8711-47c2-b02c-57d2472e4582",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f1d0ccf0-77e3-49dd-9946-10d6584ae043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "38f69c67-8131-499d-8a1e-f6e3092a3120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d0ccf0-77e3-49dd-9946-10d6584ae043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9f0fdfc-03b7-4d4a-88d3-14f2b6d6540c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d0ccf0-77e3-49dd-9946-10d6584ae043",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f620db69-d6d8-4766-83f1-2756c32b96e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "75c8b78f-3b92-4c32-a133-7462f6ce3c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f620db69-d6d8-4766-83f1-2756c32b96e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb06816d-af20-44f2-9cc3-3482ad375d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f620db69-d6d8-4766-83f1-2756c32b96e6",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "2b299030-9321-4f01-9a23-d7963a1d0449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "2f3a10f3-d14b-4d87-b0a2-46238f52001d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b299030-9321-4f01-9a23-d7963a1d0449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da709a2c-9b11-42c1-8e2f-a84444d7b438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b299030-9321-4f01-9a23-d7963a1d0449",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "a4c0e15b-4df2-4892-b393-5294dd16744d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "3edcc25b-983a-4346-81e4-c4e7ef0858b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c0e15b-4df2-4892-b393-5294dd16744d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14ecfe1c-3908-469a-a977-2e42f1d02004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c0e15b-4df2-4892-b393-5294dd16744d",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "127b741f-555d-455f-885c-92132a770975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7481e3e2-8236-4dcc-9121-54a8401b0454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "127b741f-555d-455f-885c-92132a770975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da23fe15-539a-42a9-95dd-2b2bb12a9da8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "127b741f-555d-455f-885c-92132a770975",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "4c7d2d76-f6d7-4573-ba97-708b99534359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "f9ed92c3-07a3-4a8b-a806-340fd09770e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c7d2d76-f6d7-4573-ba97-708b99534359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e9950c-7a5d-4893-8e91-250a0a6de8cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c7d2d76-f6d7-4573-ba97-708b99534359",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f8fd10e5-6dae-43dd-a39a-9d3fb9d93394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "a8c6ba83-a57c-4f5e-8064-c695f5dbdd6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8fd10e5-6dae-43dd-a39a-9d3fb9d93394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c0e79ba-3ed3-40dc-a56d-de4c5509bf07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8fd10e5-6dae-43dd-a39a-9d3fb9d93394",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "87043261-bbf7-4d34-88b4-1483d6eed0a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "1b526850-eba5-46fd-86dc-560b745a20e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87043261-bbf7-4d34-88b4-1483d6eed0a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c98661-b996-4ded-bf7d-9bbfb012b925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87043261-bbf7-4d34-88b4-1483d6eed0a7",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "d25e71d1-26b4-43ef-821e-763df286d097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "dad13f50-322a-4918-af9f-373061f2b189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d25e71d1-26b4-43ef-821e-763df286d097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34326a1f-b530-4b07-9376-00aa74af0d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d25e71d1-26b4-43ef-821e-763df286d097",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "59c26cc7-7c4d-45c9-a9fc-7820ffbdba83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "5e2fcc2b-9554-4501-b3c7-98dc8d525f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c26cc7-7c4d-45c9-a9fc-7820ffbdba83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf0e6eeb-0a53-48ac-8bc6-26a2908b3a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c26cc7-7c4d-45c9-a9fc-7820ffbdba83",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "c3388b83-d662-40e5-967e-b8fb15574d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "1c6b1b66-8408-438a-9a81-df0430ba31f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3388b83-d662-40e5-967e-b8fb15574d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106fcedf-191f-4cc5-80f7-99d54f7e7c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3388b83-d662-40e5-967e-b8fb15574d5e",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e14c9412-bf11-46a9-aad1-7f0813647be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "1e05988f-cc7c-472f-a57f-f86955634ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e14c9412-bf11-46a9-aad1-7f0813647be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f41c04b4-ff69-4965-b94e-6b8caa05bd49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e14c9412-bf11-46a9-aad1-7f0813647be8",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "80aca94b-d01a-42ed-b400-7b5e2bf2cf11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "f8a9eacb-ec3b-4cef-a2ec-e6375e3e39e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80aca94b-d01a-42ed-b400-7b5e2bf2cf11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aff6e76-fcf9-4bb7-9e66-4cdf8afa9d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80aca94b-d01a-42ed-b400-7b5e2bf2cf11",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "106e563b-ffed-48c9-a6d2-ab3bbe19421c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "c120e4f3-fc99-42d5-a99a-81e7eebf0085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "106e563b-ffed-48c9-a6d2-ab3bbe19421c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "605b80e3-a22d-4a31-8809-c255d3cc221f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "106e563b-ffed-48c9-a6d2-ab3bbe19421c",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e7a9bfb8-c191-447d-8fc9-f08a20759ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "e17baf28-73a5-4543-ba74-498b9bf0821e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a9bfb8-c191-447d-8fc9-f08a20759ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09b71b1-5103-44fe-9b17-8b3f27135bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a9bfb8-c191-447d-8fc9-f08a20759ddb",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "3930c3b9-3f79-4add-88c1-3ef8f20b0ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "89db89ab-cfd7-42c5-8cd4-487863cb8577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3930c3b9-3f79-4add-88c1-3ef8f20b0ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00429bd-d4a8-4f20-9460-61dcde4f87f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3930c3b9-3f79-4add-88c1-3ef8f20b0ad8",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "91edee84-34b2-4b8d-a843-887774731bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "ee75c303-2814-434b-a466-98c325be2b17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91edee84-34b2-4b8d-a843-887774731bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02299cbe-27c9-4bb2-ab28-9da6dc333f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91edee84-34b2-4b8d-a843-887774731bd6",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "af6efe17-8769-45ed-8d91-1b1c6745e828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "a29aedb6-0020-4854-bcfa-537294fd9b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6efe17-8769-45ed-8d91-1b1c6745e828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c90efb98-dd41-42ca-862f-ca446994357a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6efe17-8769-45ed-8d91-1b1c6745e828",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "fa2a6507-8287-4afd-90e6-c42a218b5ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "4f1b4a07-e472-485c-8634-d7be318bb9fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa2a6507-8287-4afd-90e6-c42a218b5ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "449c2335-7c46-4c7f-93e6-8f7802fe327c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa2a6507-8287-4afd-90e6-c42a218b5ef4",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "78eb2b36-7b04-4073-93c4-e90b8270108c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "80110e57-3fd2-45c5-bd94-54a18cbc7503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78eb2b36-7b04-4073-93c4-e90b8270108c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62554cf6-d8d0-4ecc-a936-82ff019a3f8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78eb2b36-7b04-4073-93c4-e90b8270108c",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "bb66f46c-1055-429a-83ef-981a3fc31215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "5f106656-7f76-4220-a95a-72a4fd8c0c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb66f46c-1055-429a-83ef-981a3fc31215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d234e640-6c0a-4fc1-9800-2bed9adcf5a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb66f46c-1055-429a-83ef-981a3fc31215",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "2c9f2aad-3615-497b-83b9-c74e0f689617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "1314b54f-59d4-4789-9d48-23a81f8b8e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9f2aad-3615-497b-83b9-c74e0f689617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f0cac26-cd19-4da4-8ee7-ad82551ea245",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9f2aad-3615-497b-83b9-c74e0f689617",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "40e0d987-68d5-4581-ae30-d6a1d3aa68b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "054a68e1-09f8-47a4-a3fc-3659b6bb51e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40e0d987-68d5-4581-ae30-d6a1d3aa68b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e9b389-ac69-45e2-a22a-4d8645acf8b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40e0d987-68d5-4581-ae30-d6a1d3aa68b7",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "b32f2ab9-d721-4b66-bc09-bab3c1c0538e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "0a7d5dc2-2a71-4804-8feb-deeea73479e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b32f2ab9-d721-4b66-bc09-bab3c1c0538e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85d12c01-90ef-46b9-a6ec-8045064e0da9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b32f2ab9-d721-4b66-bc09-bab3c1c0538e",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "bdb50052-3a57-4ac5-9ca6-41f9cc01ff33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "8d23d2fc-718d-470f-b20f-7a496a951140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdb50052-3a57-4ac5-9ca6-41f9cc01ff33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4f6fbe-430b-4219-90e7-d3532a6e35b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdb50052-3a57-4ac5-9ca6-41f9cc01ff33",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "64a76230-978d-4662-babb-34b2abdb9751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "170025d5-0be0-47d6-9c0a-274d5ceb974c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a76230-978d-4662-babb-34b2abdb9751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a63fe46b-1f25-4431-91c3-c624417f2235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a76230-978d-4662-babb-34b2abdb9751",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e2ae20ba-9cef-4bdc-98e4-a344e4a7f8d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "dfb09a24-5781-4f86-bf4b-b1bc3bb9a00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2ae20ba-9cef-4bdc-98e4-a344e4a7f8d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0d5392-d551-4269-8755-f26e5ef7c87d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2ae20ba-9cef-4bdc-98e4-a344e4a7f8d1",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "bbc6eefb-35a6-4e1a-889e-a0a5b8f0b363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "48afefd8-106a-486b-bbf2-c10d692571b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc6eefb-35a6-4e1a-889e-a0a5b8f0b363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68effd02-18d6-414c-b74f-f8b848720333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc6eefb-35a6-4e1a-889e-a0a5b8f0b363",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "464181ac-1d5a-4e09-8c98-2bd00c75c9a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "5504f6d2-61ec-44cb-9e81-2d6c5bfb718f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464181ac-1d5a-4e09-8c98-2bd00c75c9a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4864454-7f2e-4180-b3b7-42fbbc7956cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464181ac-1d5a-4e09-8c98-2bd00c75c9a7",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "5fcd52e8-be98-4e86-b3cf-0bc4c0b1a584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "0c41f393-c22b-43cd-9486-34017d194690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fcd52e8-be98-4e86-b3cf-0bc4c0b1a584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a302f5-870c-4a31-a984-8ee5654eb674",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fcd52e8-be98-4e86-b3cf-0bc4c0b1a584",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "8d526c5d-5bcd-458d-9589-8dea5976d2cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7845966c-3c42-4936-bf34-269432a4f479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d526c5d-5bcd-458d-9589-8dea5976d2cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d46f7c4c-7245-4db7-9999-b30dace271a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d526c5d-5bcd-458d-9589-8dea5976d2cb",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "62aca3a3-cdf9-4631-97ec-6abf45d0b2f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "4ff9e6d6-ae6b-4922-aade-a36cd089a27e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62aca3a3-cdf9-4631-97ec-6abf45d0b2f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16696011-60b3-4a5d-b8bf-06ba3b4a0633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62aca3a3-cdf9-4631-97ec-6abf45d0b2f9",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "a693a147-063a-40a6-a579-adc65b200639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "425afd36-6a46-44cb-a9c6-81d5d1cf1015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a693a147-063a-40a6-a579-adc65b200639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5b41278-52c0-4d11-8862-8ea96f13bb60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a693a147-063a-40a6-a579-adc65b200639",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "0b47fd2d-a31d-4739-b142-e55ff2673717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "6e05ca4a-b45e-4035-922e-3a4b02add918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b47fd2d-a31d-4739-b142-e55ff2673717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cceafbaf-5081-430b-abcc-63c59b2595fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b47fd2d-a31d-4739-b142-e55ff2673717",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "96bbc0bb-919b-4a73-a438-4e3303eaee54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "15a75e54-bd7c-4612-80e6-9147abc8ff4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96bbc0bb-919b-4a73-a438-4e3303eaee54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "390d96f4-9451-40fa-964a-cb66f2ab468f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96bbc0bb-919b-4a73-a438-4e3303eaee54",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "c74a24d9-2d09-4c33-9fb1-b2e567eec18f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "270e2d7e-793a-4e96-8466-6dd380e39be1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74a24d9-2d09-4c33-9fb1-b2e567eec18f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd1fce11-c607-4b58-8481-3f7accbe8aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74a24d9-2d09-4c33-9fb1-b2e567eec18f",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "b68381aa-3c3b-4b2c-9045-a46966778e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "ab87c192-093c-4141-afd7-11aaecad15d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68381aa-3c3b-4b2c-9045-a46966778e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a389dfb9-2339-4d65-ad35-aad23c87aea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68381aa-3c3b-4b2c-9045-a46966778e06",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f1395052-efe2-434a-83f0-403310108594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "48e3023e-31a0-4fd6-a802-897f0423d844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1395052-efe2-434a-83f0-403310108594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88aca8e3-422d-439a-9f36-4916c7981916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1395052-efe2-434a-83f0-403310108594",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "3125d54b-4d64-4339-a7b8-8cf0584f72b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "bc902368-c1bb-4474-b9d2-ef64c2f24662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3125d54b-4d64-4339-a7b8-8cf0584f72b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f6365f-a654-4398-81dd-d29c91b8ab9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3125d54b-4d64-4339-a7b8-8cf0584f72b4",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e3b62f17-9c9c-4d44-bb76-2741a9440e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "eda3be73-4315-486c-a62a-06d0bd0dd5e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b62f17-9c9c-4d44-bb76-2741a9440e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08b0f94c-5be5-45fa-9fcf-760481652bf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b62f17-9c9c-4d44-bb76-2741a9440e26",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "246f75c7-9484-4390-b90d-01f289e0bf24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "20d6ab7b-35a1-4063-a98e-895632c0bf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246f75c7-9484-4390-b90d-01f289e0bf24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e57061de-f79d-42ff-ae04-742d5b6007e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246f75c7-9484-4390-b90d-01f289e0bf24",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "600d64c0-5e39-40aa-bf33-713ef98fe2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "9d82e5a7-a510-47d0-a755-2e150331feec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "600d64c0-5e39-40aa-bf33-713ef98fe2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac602f6-27e1-4f79-9a21-d7b0fb085bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600d64c0-5e39-40aa-bf33-713ef98fe2fc",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "25f34295-35e7-40c6-b88d-7745acccf651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "a8c78d1d-bef6-4bdb-b7f9-c2b7db2e965a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f34295-35e7-40c6-b88d-7745acccf651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a02a6c91-5183-413f-9d48-87f6dbec5e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f34295-35e7-40c6-b88d-7745acccf651",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "d007156d-d80d-4193-819b-238e7204c02f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "bdc019b7-f8a3-4b3d-a1e3-e154ac14de26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d007156d-d80d-4193-819b-238e7204c02f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d9177e-b62c-4e2b-9676-5a8620292043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d007156d-d80d-4193-819b-238e7204c02f",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "fcee3a06-2880-4a10-868d-b6045769eef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "9e53236f-bead-40cf-b930-0e79181991bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcee3a06-2880-4a10-868d-b6045769eef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7b8c20-aad7-46cd-afee-da52bb2a77f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcee3a06-2880-4a10-868d-b6045769eef0",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f0204603-4443-4b64-8adf-f4752105dc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "24e58f4e-59eb-4632-bb5e-552cfc1c5eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0204603-4443-4b64-8adf-f4752105dc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6824fa2a-30f0-40d3-9eff-a4aa5cafaea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0204603-4443-4b64-8adf-f4752105dc9f",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "f6568083-d29e-47df-ac18-1b1d32ae78dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7f88be54-31d8-410c-b07c-8be76ba1c9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6568083-d29e-47df-ac18-1b1d32ae78dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "525736f3-1b8f-42b2-8e1b-330f88f8f1b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6568083-d29e-47df-ac18-1b1d32ae78dc",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "1f99e98d-9a0a-48e4-8c3b-309eda3726f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "7da924a6-9cc4-4dd5-984e-5433db399cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f99e98d-9a0a-48e4-8c3b-309eda3726f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "067a988d-66a4-49a4-a37b-8ddb466a5031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f99e98d-9a0a-48e4-8c3b-309eda3726f1",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "13d295df-4dcd-4b2f-af2a-eee0731e10aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "a399ed92-84b7-4cfa-8117-ab342a14c50a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d295df-4dcd-4b2f-af2a-eee0731e10aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96d24d21-13f7-4b65-9b56-58f0f9675c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d295df-4dcd-4b2f-af2a-eee0731e10aa",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "8d74beb1-ae42-4226-910a-2cb2c6e67585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "572bc115-2a1a-4879-8dd0-c653beb33725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d74beb1-ae42-4226-910a-2cb2c6e67585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed5454e-9f52-4c7e-9bf5-7e441c1040c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d74beb1-ae42-4226-910a-2cb2c6e67585",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "e5c89129-424e-4bde-80f6-1e40d0ad2d45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "8df4eb26-166e-42b7-a9db-8eeaf037b4ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c89129-424e-4bde-80f6-1e40d0ad2d45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8d1280-1afd-4a6f-9cbd-59c4932ea8e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c89129-424e-4bde-80f6-1e40d0ad2d45",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "2cfe9800-e95f-467d-9afc-c0a3f6c14a91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "360ef6bd-8733-41b5-8d43-a2a53edeb30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cfe9800-e95f-467d-9afc-c0a3f6c14a91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3335f42e-d3be-4609-a2b9-79ab37e1761e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cfe9800-e95f-467d-9afc-c0a3f6c14a91",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "6cc45731-dc42-4ed3-9b78-3724fc0ffc3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "8981480a-c24b-4014-9109-b51fd483dd81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc45731-dc42-4ed3-9b78-3724fc0ffc3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08457bf4-06cd-46ff-ae62-1081bbefa19f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc45731-dc42-4ed3-9b78-3724fc0ffc3e",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "c3b67ce9-104a-4ceb-a4ce-3d2410ba34da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "89d8200f-f5c6-443c-bcd0-2ecb29fbfdba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b67ce9-104a-4ceb-a4ce-3d2410ba34da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "655f2a98-7d32-4e9e-acbe-6967c5440aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b67ce9-104a-4ceb-a4ce-3d2410ba34da",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "8abc0925-2aac-4029-8add-ec7aab9a56af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "bdc5e679-a557-4644-9b39-ae901f2969d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8abc0925-2aac-4029-8add-ec7aab9a56af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f79c9ff4-7e7d-4a70-8d81-aea270d39d8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8abc0925-2aac-4029-8add-ec7aab9a56af",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        },
        {
            "id": "0a387e20-a26a-41c5-9f9b-b3da4a14f410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "compositeImage": {
                "id": "914d79b3-44d4-46c9-ab16-6359cde24bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a387e20-a26a-41c5-9f9b-b3da4a14f410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a532c20e-2313-4d3c-ad19-95b5dd23ae0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a387e20-a26a-41c5-9f9b-b3da4a14f410",
                    "LayerId": "4ac58b34-931c-4b5e-9d7e-dafc569880ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4ac58b34-931c-4b5e-9d7e-dafc569880ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba753367-c12a-492e-a2b6-6c72a4f55e6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 63,
    "yorig": 67
}