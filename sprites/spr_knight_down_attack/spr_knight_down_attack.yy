{
    "id": "108acb45-5b09-4838-b042-fd4d03607c11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_down_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 38,
    "bbox_right": 65,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86711fd3-01b2-4928-ad8d-adec90ae9334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "eb594173-95c8-4772-b47e-5b38aa6be40c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86711fd3-01b2-4928-ad8d-adec90ae9334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f4dbfa8-8cf8-43b4-9119-16c4d86c5dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86711fd3-01b2-4928-ad8d-adec90ae9334",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "69acfa30-c46c-4bec-a8e9-fefb5561f2cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "a269aec4-4c91-408e-91d3-ac50987eb65a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69acfa30-c46c-4bec-a8e9-fefb5561f2cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d140cfc2-69f5-42c0-b26f-add0394b1866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69acfa30-c46c-4bec-a8e9-fefb5561f2cb",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "2066b27d-7333-414a-9906-56b17758e2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "35684adf-8a6b-401f-8661-421503bb041f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2066b27d-7333-414a-9906-56b17758e2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72959208-c5d6-400a-84b4-5a23adae81cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2066b27d-7333-414a-9906-56b17758e2f4",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "0ff01077-bb88-44af-876a-317e24fdb79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "a64d1f1b-508f-4bf9-af04-dc1b59a47fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff01077-bb88-44af-876a-317e24fdb79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e731eb3-b96b-4a72-bcd1-055071434196",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff01077-bb88-44af-876a-317e24fdb79b",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "a36f99c6-745d-47ee-b661-ba91cefeb931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "d9058100-6625-4078-9ebb-c37a91e646ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36f99c6-745d-47ee-b661-ba91cefeb931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c670d6-2a5b-4763-997e-a35b69cb4fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36f99c6-745d-47ee-b661-ba91cefeb931",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "4ac967ac-4052-475c-a251-cf104c5c76ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "b1a01d8b-007f-49fe-a4fd-6c10cf773e7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac967ac-4052-475c-a251-cf104c5c76ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413e0dc6-ce08-4dba-a753-bf9e9a8e17e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac967ac-4052-475c-a251-cf104c5c76ed",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "5b25ca80-e25a-43cd-8c5e-bf30b05e78d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "641e7ea0-19f1-4220-b7fb-af7acd3e2470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b25ca80-e25a-43cd-8c5e-bf30b05e78d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b51a5c8-6d6e-4814-9d9e-d8d80d0acb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b25ca80-e25a-43cd-8c5e-bf30b05e78d1",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "f9fcfd65-e918-4c19-bbf1-5de7e5ecdb7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "1e82a752-9e00-45e6-be3f-99767fd13903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9fcfd65-e918-4c19-bbf1-5de7e5ecdb7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b7ba5e-53bc-4962-9517-a714e8a19474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9fcfd65-e918-4c19-bbf1-5de7e5ecdb7b",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "0f7936d8-9dc9-456c-9a95-b1d63eb7dc54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "dcbec851-f240-45cc-9dd1-facd358d13fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7936d8-9dc9-456c-9a95-b1d63eb7dc54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "690ba4e0-1eb0-426b-9148-bb64b3f7e1b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7936d8-9dc9-456c-9a95-b1d63eb7dc54",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        },
        {
            "id": "3ed4ca08-7c30-4aaa-832c-4a5f79a8258a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "compositeImage": {
                "id": "02c68609-4ab7-46d3-bd45-5dfb08ae13eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ed4ca08-7c30-4aaa-832c-4a5f79a8258a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e3b1aa4-f566-4b2e-b5ca-cb17224be068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ed4ca08-7c30-4aaa-832c-4a5f79a8258a",
                    "LayerId": "cfb1daba-3f23-4d1b-adab-d58024e9d474"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cfb1daba-3f23-4d1b-adab-d58024e9d474",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "108acb45-5b09-4838-b042-fd4d03607c11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 56,
    "yorig": 67
}