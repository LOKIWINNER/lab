{
    "id": "85d7b2b1-f88d-44f8-ba34-b0ee0a1d7c91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_attack_hold_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 21,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cdcade4-d507-4641-84dc-96e146ac66d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85d7b2b1-f88d-44f8-ba34-b0ee0a1d7c91",
            "compositeImage": {
                "id": "a2a2e61e-8de9-4562-aefd-e8cb69e121a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdcade4-d507-4641-84dc-96e146ac66d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb0704b8-7da7-49ca-87f2-a25039d106e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdcade4-d507-4641-84dc-96e146ac66d7",
                    "LayerId": "45b20407-ec73-4918-af97-c4ee1931071a"
                }
            ]
        },
        {
            "id": "0624e57a-e4fd-4b33-9ef1-0dc351ded2be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85d7b2b1-f88d-44f8-ba34-b0ee0a1d7c91",
            "compositeImage": {
                "id": "ffde5b37-52f4-4a04-b71a-801f04f43d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0624e57a-e4fd-4b33-9ef1-0dc351ded2be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706c468d-5f89-4ea2-97f6-e361e6291a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0624e57a-e4fd-4b33-9ef1-0dc351ded2be",
                    "LayerId": "45b20407-ec73-4918-af97-c4ee1931071a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "45b20407-ec73-4918-af97-c4ee1931071a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85d7b2b1-f88d-44f8-ba34-b0ee0a1d7c91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}