{
    "id": "6c048194-c062-498c-b621-a84793e334f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_attack_mask_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ac3dbef-ded8-4a4a-82ec-4532afe4ecc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "c99640ee-5071-4662-8ae3-4d254be50664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac3dbef-ded8-4a4a-82ec-4532afe4ecc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99d78aa-9028-408b-8cae-96f66fbc4267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac3dbef-ded8-4a4a-82ec-4532afe4ecc7",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "dc13a6ab-9efe-4885-bfb8-189f2f126eaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "d983633a-a14f-4bf8-8da8-4081428c8f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc13a6ab-9efe-4885-bfb8-189f2f126eaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdf83ccd-7d0a-462b-a0b3-84609b0f9051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc13a6ab-9efe-4885-bfb8-189f2f126eaa",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "220347be-5224-4a11-b2fe-fb76e961f2b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "83474528-06df-4083-8707-4b4ea1f0640f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220347be-5224-4a11-b2fe-fb76e961f2b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d5cc9cc-8ffa-4efc-92ff-6980ea72fbac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220347be-5224-4a11-b2fe-fb76e961f2b4",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "29eaac7b-6b10-4fa4-a6e4-74fef83e2373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "db3cad15-a7d1-425e-917f-278a667cf417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29eaac7b-6b10-4fa4-a6e4-74fef83e2373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eca235e-71f0-42b3-b6c0-eeb700ff385f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29eaac7b-6b10-4fa4-a6e4-74fef83e2373",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "48851839-c611-4992-9d67-6ed6fd2ce089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "6c43e2ad-cf50-46a8-82bc-c9150f28dfcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48851839-c611-4992-9d67-6ed6fd2ce089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa51018-6c30-4e7e-a93d-b687ec529cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48851839-c611-4992-9d67-6ed6fd2ce089",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "93dea9d7-515d-4965-be63-9a92fe54fa71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "06d0a80a-2401-494f-8598-99e8505c83cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93dea9d7-515d-4965-be63-9a92fe54fa71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a2dce5d-2c66-4eea-8c2b-1e307d08173d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93dea9d7-515d-4965-be63-9a92fe54fa71",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "377282be-7016-452a-a016-f7da17859b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "0b06f76d-0dd8-4ecb-8da2-2f79886bd90e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377282be-7016-452a-a016-f7da17859b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2fbf2f-df38-4e77-a6dd-cadff5994a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377282be-7016-452a-a016-f7da17859b20",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "9fcf3c68-b90b-4e2e-bf3d-ac3838bb8e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "f4733145-c437-493c-8ad5-ca6b1c7497bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fcf3c68-b90b-4e2e-bf3d-ac3838bb8e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be65f916-af6e-4e0c-86f0-9933979e0dd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fcf3c68-b90b-4e2e-bf3d-ac3838bb8e91",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "c14f35ed-a4ab-4a99-a5d7-1c5767c5d20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "cfa2efc3-b7eb-4d85-9637-ce1c35c85854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c14f35ed-a4ab-4a99-a5d7-1c5767c5d20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac8f75e-29a2-45fa-9c75-078fca958c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c14f35ed-a4ab-4a99-a5d7-1c5767c5d20a",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        },
        {
            "id": "d0780a15-7a04-4f17-ad7f-434f28424bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "compositeImage": {
                "id": "11898107-dd90-4de4-a249-81c9170d49d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0780a15-7a04-4f17-ad7f-434f28424bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2472c1b-5637-46e0-8952-97b2c654d042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0780a15-7a04-4f17-ad7f-434f28424bcc",
                    "LayerId": "d8786925-2964-4c54-8601-2fbb0df924a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d8786925-2964-4c54-8601-2fbb0df924a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c048194-c062-498c-b621-a84793e334f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}