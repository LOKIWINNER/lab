{
    "id": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_right_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 28,
    "bbox_right": 125,
    "bbox_top": 53,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63930547-54de-467f-b07c-a491d41b0686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "760d2e5a-5044-4217-af7f-29cada073231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63930547-54de-467f-b07c-a491d41b0686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261400ee-bdee-4881-b88d-68ece6566318",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63930547-54de-467f-b07c-a491d41b0686",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "74bac125-6bd9-4d35-8914-12f42300d0e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "d92ee728-1570-4463-a360-945c931081cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74bac125-6bd9-4d35-8914-12f42300d0e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24153ae5-dced-4df0-b529-1116a06eaf30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74bac125-6bd9-4d35-8914-12f42300d0e9",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "7dc01118-3f59-4751-ae6f-51b4df567fb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "835eae8f-3fab-4368-99e8-ab1100a5a9c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc01118-3f59-4751-ae6f-51b4df567fb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ae94d8-a6f2-42ae-a7ed-37903174a94f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc01118-3f59-4751-ae6f-51b4df567fb7",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "d2f0e954-0a78-4500-9b1b-9838fb9d7501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "caec82b0-d091-4ef3-8d30-ee64c386e4ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2f0e954-0a78-4500-9b1b-9838fb9d7501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "777f6831-8eab-4b23-aea9-8287932c4919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2f0e954-0a78-4500-9b1b-9838fb9d7501",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "fc2fb346-eee1-4ed7-9d6b-21b691ef91de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "b1f51d7b-100e-41e8-a0a8-dc417ab840c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2fb346-eee1-4ed7-9d6b-21b691ef91de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65bd3711-4a4e-499f-b9f6-08490a7e8ef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2fb346-eee1-4ed7-9d6b-21b691ef91de",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "7b64c5a1-4c69-4934-ab84-987ca5c3749e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "8f5c31b8-a5ff-462f-829e-2265665a1b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b64c5a1-4c69-4934-ab84-987ca5c3749e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "190af6d4-9ba6-4293-ac92-dd2adf10474a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b64c5a1-4c69-4934-ab84-987ca5c3749e",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "cfe302f3-e497-489a-9dd2-1f789c6a2b4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "b60b635b-0210-4623-b000-2804b9ebd39d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfe302f3-e497-489a-9dd2-1f789c6a2b4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a00c72a-c55e-4ccd-85c4-d6ed614f8a37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfe302f3-e497-489a-9dd2-1f789c6a2b4d",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "fa6656b4-b486-418f-9ee5-5c06da1a69db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "8657fcf1-4bb1-44bd-9ad6-6c944ebabb5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6656b4-b486-418f-9ee5-5c06da1a69db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e90d52b-03f4-4758-9aef-1253cd002bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6656b4-b486-418f-9ee5-5c06da1a69db",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "e2f14482-f5d6-4fe2-bdbf-9d2722d68027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "526a9129-8908-4bf2-9c37-23685a30994e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f14482-f5d6-4fe2-bdbf-9d2722d68027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea412145-2ffe-4a93-adbb-78a2a847a0f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f14482-f5d6-4fe2-bdbf-9d2722d68027",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        },
        {
            "id": "9b966b9a-f882-4b89-8ee0-4b25a60bb407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "compositeImage": {
                "id": "aeffb07f-e508-41b1-82ae-a7da307c58ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b966b9a-f882-4b89-8ee0-4b25a60bb407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7b9bfc-47c2-48c5-9d85-162906ec58dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b966b9a-f882-4b89-8ee0-4b25a60bb407",
                    "LayerId": "a4621e61-63ee-44ea-a090-16e1218c3c81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a4621e61-63ee-44ea-a090-16e1218c3c81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3245663f-8ec3-41cb-a6c6-fb385e8f18f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 63,
    "yorig": 67
}