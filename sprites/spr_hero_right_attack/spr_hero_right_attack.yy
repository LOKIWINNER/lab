{
    "id": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 22,
    "bbox_right": 63,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "433817ff-2843-4a5e-8156-94f4a97db4da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "05f562a3-4f8e-4bfb-b332-a2534924acdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "433817ff-2843-4a5e-8156-94f4a97db4da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f10bbd1-a157-47d0-8989-29580ad2eb18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "433817ff-2843-4a5e-8156-94f4a97db4da",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "dc5223e4-fe62-4370-9331-a473adeb3d76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "d1d7e0df-ae93-45b6-bb29-57186a6fa4e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5223e4-fe62-4370-9331-a473adeb3d76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6bc8184-8fc2-4ac8-a63e-ed401d2cdea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5223e4-fe62-4370-9331-a473adeb3d76",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "939c77b3-e15b-4cd0-b0f3-c962b7ba65c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "02dfadaa-495a-4d8b-b731-d88cf9060639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "939c77b3-e15b-4cd0-b0f3-c962b7ba65c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66848726-4739-44e4-84c7-4e20fec879bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "939c77b3-e15b-4cd0-b0f3-c962b7ba65c1",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "676efd07-6001-4f70-a684-b12086bfa167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "f6bfff41-8a54-4367-9270-10c41c974764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676efd07-6001-4f70-a684-b12086bfa167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29207191-1131-401e-9720-5adefabdbd4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676efd07-6001-4f70-a684-b12086bfa167",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "4d31d9f0-57fd-4abd-94e9-be138d4d2f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "adefdab5-47a8-43fa-9733-7bf8bcf2c47a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d31d9f0-57fd-4abd-94e9-be138d4d2f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6df666b7-342d-4cf6-bad4-eb3a993cffbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d31d9f0-57fd-4abd-94e9-be138d4d2f3d",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "2372b8f4-d4ec-4970-a050-cc78adf79fb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "948d0e3b-c7ae-48a3-88f3-17546a51d9d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2372b8f4-d4ec-4970-a050-cc78adf79fb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b8fd26-a15d-4d39-b37f-08f08a9b3714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2372b8f4-d4ec-4970-a050-cc78adf79fb2",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "070dac08-8be2-44e6-b0d0-7ffda3e228d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "d5379f29-eab3-47d6-9645-afbbe008dc35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070dac08-8be2-44e6-b0d0-7ffda3e228d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d29689-41f3-42cc-b954-708587ecee09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070dac08-8be2-44e6-b0d0-7ffda3e228d3",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "0cd01742-6311-40c6-a01b-20ed16bfe53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "407323a5-4308-482f-8134-c225a6f08e3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd01742-6311-40c6-a01b-20ed16bfe53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ffc64e-19c9-4dda-b0dc-3530849328c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd01742-6311-40c6-a01b-20ed16bfe53d",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "9d406f48-5d3c-4b97-92f9-20954f76d8c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "5d48e7e0-e09b-45c8-ae59-af55f4090218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d406f48-5d3c-4b97-92f9-20954f76d8c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48bcda30-783d-441d-bf6e-90107e52cd68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d406f48-5d3c-4b97-92f9-20954f76d8c9",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        },
        {
            "id": "6f1c067e-1d08-41cc-be0f-7f8d081cfce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "compositeImage": {
                "id": "77eaa1d3-bfa3-468a-a1d5-828dea5795b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f1c067e-1d08-41cc-be0f-7f8d081cfce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce128e9c-2312-4b6b-84fb-e91796c01647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f1c067e-1d08-41cc-be0f-7f8d081cfce9",
                    "LayerId": "cad0a905-51c7-4611-8af1-42861c19802e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cad0a905-51c7-4611-8af1-42861c19802e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cde0f9b9-3117-4efa-aad7-d6519a614f71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}