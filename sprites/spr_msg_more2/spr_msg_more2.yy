{
    "id": "587895f5-f883-4ede-aba1-cc25668b1e21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09ec3552-7afb-4d2a-885f-26d31902c369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "587895f5-f883-4ede-aba1-cc25668b1e21",
            "compositeImage": {
                "id": "3485d222-7f47-4bf2-85ee-6e32a5ed4a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ec3552-7afb-4d2a-885f-26d31902c369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a96410e-685c-4b32-a09d-bfd83557816d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ec3552-7afb-4d2a-885f-26d31902c369",
                    "LayerId": "bec88f49-3e6e-443a-a556-0b1e0c6c51ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bec88f49-3e6e-443a-a556-0b1e0c6c51ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "587895f5-f883-4ede-aba1-cc25668b1e21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}