{
    "id": "0fbd3ccc-f69a-4265-b21c-f825adf40deb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_floor_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "175cb811-228d-4c8b-b2d3-96a8e0c5d4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fbd3ccc-f69a-4265-b21c-f825adf40deb",
            "compositeImage": {
                "id": "0271b37f-7c78-4703-ac0b-ea55cd3e3124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "175cb811-228d-4c8b-b2d3-96a8e0c5d4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc6c3a1-ca9b-4317-9eb7-de4b6c779e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "175cb811-228d-4c8b-b2d3-96a8e0c5d4c4",
                    "LayerId": "ad749ca4-2733-455d-92ee-6f810738a220"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad749ca4-2733-455d-92ee-6f810738a220",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fbd3ccc-f69a-4265-b21c-f825adf40deb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}