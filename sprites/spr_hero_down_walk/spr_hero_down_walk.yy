{
    "id": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 19,
    "bbox_right": 45,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e9d0a19-06d9-4bfb-b56d-e9745ed49726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "e7674561-3ced-440a-b461-00e89cb13aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e9d0a19-06d9-4bfb-b56d-e9745ed49726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0959eff-7b19-4c22-bb9a-066c72612168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e9d0a19-06d9-4bfb-b56d-e9745ed49726",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "3d93adaf-1331-4db6-b77f-705afb1e3d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "6f7ae704-3361-4b0a-b80d-29adb926e514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d93adaf-1331-4db6-b77f-705afb1e3d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21907e22-98b7-44a6-84b6-1c54bea6b596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d93adaf-1331-4db6-b77f-705afb1e3d72",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "c5710be6-2cfb-4f57-a6bc-4d90e09b3612",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "cd5ef81b-6f5a-4663-a300-608e078cc5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5710be6-2cfb-4f57-a6bc-4d90e09b3612",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c814459f-3c66-4450-acf8-da7665220f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5710be6-2cfb-4f57-a6bc-4d90e09b3612",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "31441213-5bbb-4b5c-9888-abe76f3b5929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "2ac97317-6b12-4ace-993b-2420edcb438d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31441213-5bbb-4b5c-9888-abe76f3b5929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "059041ca-16b3-4cee-9a1f-b51307af6ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31441213-5bbb-4b5c-9888-abe76f3b5929",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "aaf52c73-471c-42a8-9dc5-7bbb8a9e0529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "b7a8c195-97ed-4f9d-a58d-ab715475d164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf52c73-471c-42a8-9dc5-7bbb8a9e0529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5fd8c7d-31f0-4bd2-88c3-ba9627968e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf52c73-471c-42a8-9dc5-7bbb8a9e0529",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "faa3fe85-6227-464e-8854-d19e1b719aca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "f1939bc4-cdec-4e80-b3f0-a6b489832aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa3fe85-6227-464e-8854-d19e1b719aca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a3717b-4fdf-4872-94cb-5662ccc4370e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa3fe85-6227-464e-8854-d19e1b719aca",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "b6eb7eba-db22-4b62-ac9f-f04c1e8719e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "7f7ec0c4-fab0-4bde-9421-bc14a941933e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6eb7eba-db22-4b62-ac9f-f04c1e8719e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208b86fc-ada2-4909-9a03-bd2b0f3b73e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6eb7eba-db22-4b62-ac9f-f04c1e8719e9",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "5245abbe-5e0b-48e2-ba80-2ee677ae02ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "b3e98670-6aad-4bfa-b578-2db332db870d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5245abbe-5e0b-48e2-ba80-2ee677ae02ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0af3b0f-965f-4643-a7a9-b6084aa238df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5245abbe-5e0b-48e2-ba80-2ee677ae02ce",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "9f93b6fe-44e4-41bb-9ecd-e638b0b650f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "1c7e5b72-b7ea-44d7-862c-19f73635ee4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f93b6fe-44e4-41bb-9ecd-e638b0b650f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e81066b-4ed2-46fe-80d1-9910a97da2ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f93b6fe-44e4-41bb-9ecd-e638b0b650f8",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "61959a54-ee8e-41a1-b744-0fcd60575cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "6c419344-d61d-4154-b3d4-e7d4e9aa4b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61959a54-ee8e-41a1-b744-0fcd60575cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32565b2-6623-427a-a106-a64ef6e0ebb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61959a54-ee8e-41a1-b744-0fcd60575cdf",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "43d49f67-7e5b-4bce-98b2-6530b51c928f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "a52c6ce5-53ea-495b-a6f3-7775e032106e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d49f67-7e5b-4bce-98b2-6530b51c928f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7058de9e-ab63-4a30-a566-f10133306593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d49f67-7e5b-4bce-98b2-6530b51c928f",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "223f06c6-64ae-4f3a-9337-9df6e503487d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "4e5fdb67-1ecd-4366-97fd-838a71f6d41c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "223f06c6-64ae-4f3a-9337-9df6e503487d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0f3d71f-03f0-4e60-9e92-7c2a47134502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "223f06c6-64ae-4f3a-9337-9df6e503487d",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "8f94d936-5179-4de2-a073-2b3c5b0133f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "b80e1ba0-690f-49ed-bc04-abb85788be5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f94d936-5179-4de2-a073-2b3c5b0133f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb223f0f-0ef3-4832-98b7-c82ef160ec3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f94d936-5179-4de2-a073-2b3c5b0133f6",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "6faadfdf-d28b-4d11-bac8-ac085fcc7f17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "cea76e62-0d1d-4f69-a77a-fa644aa7752b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6faadfdf-d28b-4d11-bac8-ac085fcc7f17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d8c3026-6914-4a39-b8e5-1ba428d698f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6faadfdf-d28b-4d11-bac8-ac085fcc7f17",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "041d4906-3de2-4bcb-a11e-1070d1110fda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "441fdae6-4cf0-4108-93be-eb3b7a164616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "041d4906-3de2-4bcb-a11e-1070d1110fda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b92186af-3b10-4242-898b-196fc19d234d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "041d4906-3de2-4bcb-a11e-1070d1110fda",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "d1f7fd20-d304-4ca1-acd3-69a077190186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "37882c67-c30b-4e1d-a9fb-10f9453a8b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f7fd20-d304-4ca1-acd3-69a077190186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac67b570-93f2-4554-b9b9-f6d61c33cf53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f7fd20-d304-4ca1-acd3-69a077190186",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "a4876937-fe55-4801-9e8b-04a512d85835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "cd805c0f-328a-4f0e-9ecf-b42ff5725940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4876937-fe55-4801-9e8b-04a512d85835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73e876c-5b70-4eaa-9482-524492c85ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4876937-fe55-4801-9e8b-04a512d85835",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "73809122-4de4-4665-88d2-37b0c3913fd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "0e6ee887-2531-4d88-81af-4f8f089d07ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73809122-4de4-4665-88d2-37b0c3913fd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116478b0-9e47-47aa-be83-69676dbb84df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73809122-4de4-4665-88d2-37b0c3913fd7",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "da285b3a-5517-4bd0-83d6-fe05284b097a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "5a901082-5c1f-4355-ad3e-ef7220e50202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da285b3a-5517-4bd0-83d6-fe05284b097a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b72127c-e539-463f-bd8e-fd74a2274d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da285b3a-5517-4bd0-83d6-fe05284b097a",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        },
        {
            "id": "54a9d5a3-b61d-4ddc-8746-0d892e29d512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "compositeImage": {
                "id": "a57b75c2-39ed-4a93-b50c-4d47b44c102a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a9d5a3-b61d-4ddc-8746-0d892e29d512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "438d9363-ecf7-4d8f-bf3f-5eb60700516e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a9d5a3-b61d-4ddc-8746-0d892e29d512",
                    "LayerId": "03829fde-bb14-4ebd-af9f-b3258a9afbe9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "03829fde-bb14-4ebd-af9f-b3258a9afbe9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa46df71-087e-4e2a-bcec-a1310ef7d715",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}