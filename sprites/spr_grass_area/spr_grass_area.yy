{
    "id": "77b56da0-9e60-4e85-a88d-2516e13f01ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass_area",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14e5c4b0-2d31-485b-a675-09945c81954b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b56da0-9e60-4e85-a88d-2516e13f01ee",
            "compositeImage": {
                "id": "3933f99f-28ac-429d-a22c-4ec39d8ac9bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14e5c4b0-2d31-485b-a675-09945c81954b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a04b7e14-55e8-4062-a481-04ef3bcc514b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14e5c4b0-2d31-485b-a675-09945c81954b",
                    "LayerId": "32e807a5-03de-438a-a839-34e33bbe4fda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "32e807a5-03de-438a-a839-34e33bbe4fda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b56da0-9e60-4e85-a88d-2516e13f01ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}