{
    "id": "561c82d9-7485-416d-85df-4c3f32cc8100",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barrel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e98b4e14-355a-4c51-9e8d-520d1d03e390",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "561c82d9-7485-416d-85df-4c3f32cc8100",
            "compositeImage": {
                "id": "19d53a85-de39-40e3-b49a-c3044096ef31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98b4e14-355a-4c51-9e8d-520d1d03e390",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf70bbe-6888-4f1f-b87b-3c834688638a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98b4e14-355a-4c51-9e8d-520d1d03e390",
                    "LayerId": "d0923e01-3d9a-4807-8367-6f6d3a8d5234"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0923e01-3d9a-4807-8367-6f6d3a8d5234",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "561c82d9-7485-416d-85df-4c3f32cc8100",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}