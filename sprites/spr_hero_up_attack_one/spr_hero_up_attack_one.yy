{
    "id": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_attack_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64a7a834-83f1-4ea2-93c0-bcda67b550ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "5f3e00a5-b40c-4b78-a7c2-bfb6bbd33928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a7a834-83f1-4ea2-93c0-bcda67b550ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c0d3fa6-1dcb-41b0-a2d7-93643e4ef704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a7a834-83f1-4ea2-93c0-bcda67b550ae",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "dde469d3-9040-4d28-a8d8-c0e3154ec041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "485fc2c5-38bd-40c7-a2ed-0233af64c9d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dde469d3-9040-4d28-a8d8-c0e3154ec041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27513449-0e81-467c-8442-026a5f063948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dde469d3-9040-4d28-a8d8-c0e3154ec041",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "fca84ab5-f6c4-4736-bb84-a630fc658550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "214418d6-f6ad-4897-8f6e-cd87cd08285f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fca84ab5-f6c4-4736-bb84-a630fc658550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "837ff23f-7d79-43cf-9dfd-966d81b35712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fca84ab5-f6c4-4736-bb84-a630fc658550",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "81ee5b7b-894a-4706-9bf6-ca7bcf992d64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "492d1fbf-74ba-4c0f-84f0-357553b23a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ee5b7b-894a-4706-9bf6-ca7bcf992d64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37338f23-a592-4a44-a804-222ad13c376e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ee5b7b-894a-4706-9bf6-ca7bcf992d64",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "4fd11194-fb1a-4435-837a-df5d84490df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "d7cbdfb2-8d8d-4c58-aa81-1ebd76fa46e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd11194-fb1a-4435-837a-df5d84490df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "665a2673-e7d5-4880-8b9c-8b78806a9660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd11194-fb1a-4435-837a-df5d84490df0",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "4fd13cb5-ad24-43b5-b82b-129105bdb0df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "702476ba-ca37-4d7a-9f25-516e54b21c14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd13cb5-ad24-43b5-b82b-129105bdb0df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d84df44-d4dd-4693-9bf6-407c461c4c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd13cb5-ad24-43b5-b82b-129105bdb0df",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "21142489-ee19-42af-8b32-9bc850358cb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "55959d19-cc9f-4820-a6e3-ac026e345ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21142489-ee19-42af-8b32-9bc850358cb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ee6b53-ab75-443a-8f3b-5c36bfc5134e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21142489-ee19-42af-8b32-9bc850358cb4",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        },
        {
            "id": "8b34b8ff-cd7d-4679-aa94-869540b84cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "compositeImage": {
                "id": "4a5858ed-dd77-4ee8-9bc6-12b538e648e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b34b8ff-cd7d-4679-aa94-869540b84cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14e40e8-eee5-4a45-9f49-6127a8abc33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b34b8ff-cd7d-4679-aa94-869540b84cd3",
                    "LayerId": "5b222463-d385-49a5-be2e-2c950887535e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5b222463-d385-49a5-be2e-2c950887535e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d39da40d-2ae3-4cc5-9b03-c0d698d18f92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}