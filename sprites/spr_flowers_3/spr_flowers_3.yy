{
    "id": "daaecf9d-035b-40fa-869a-e8a6faf77391",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flowers_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dde9e3c-5af1-4608-a5f7-7972fa726464",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaecf9d-035b-40fa-869a-e8a6faf77391",
            "compositeImage": {
                "id": "8a680cae-98c5-4f27-891d-db1992070fc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dde9e3c-5af1-4608-a5f7-7972fa726464",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7a42d1-df59-4f7a-b56e-84b757deaaea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dde9e3c-5af1-4608-a5f7-7972fa726464",
                    "LayerId": "c3da83f6-4cf4-4781-9ad3-aa243bea406a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c3da83f6-4cf4-4781-9ad3-aa243bea406a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daaecf9d-035b-40fa-869a-e8a6faf77391",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}