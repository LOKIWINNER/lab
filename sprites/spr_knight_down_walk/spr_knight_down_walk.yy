{
    "id": "17317750-b0fe-47c4-8ae4-34505287afe5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_down_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 3,
    "bbox_right": 77,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19929a13-09d4-4ebd-bbd8-a744178fa7fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "99544c6b-3629-4368-8868-41f3ff88bbf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19929a13-09d4-4ebd-bbd8-a744178fa7fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1eec663-0d50-4044-a123-c1121d0666ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19929a13-09d4-4ebd-bbd8-a744178fa7fd",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "9a5b226c-282c-4e14-a7ed-9715c5fd2d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "b08eca3e-9ae8-4030-a637-fc9e0dc80db9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a5b226c-282c-4e14-a7ed-9715c5fd2d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee29a8bb-6d8a-45f7-a6c2-efe1fe529c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a5b226c-282c-4e14-a7ed-9715c5fd2d50",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "22293edb-e3c6-42db-b3fa-6b5ccaef3c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "58040d19-f2b5-4fc4-a6c2-d43ad90d40f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22293edb-e3c6-42db-b3fa-6b5ccaef3c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30732eba-4db6-44c2-95bd-43360c8814a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22293edb-e3c6-42db-b3fa-6b5ccaef3c04",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "73ca567f-6811-48d2-976f-4b5f5dc79a34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "35ab5d32-21b4-4351-a073-c1b98029bec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ca567f-6811-48d2-976f-4b5f5dc79a34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69481bdc-b47e-4d1c-9874-e049d18410cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ca567f-6811-48d2-976f-4b5f5dc79a34",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "b2e88250-b9ac-498d-b018-fc09172af4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "5bdda4d4-13e2-4c4b-9347-318a99337dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e88250-b9ac-498d-b018-fc09172af4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfba7c79-ca2f-4a6c-b048-052fa65ee819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e88250-b9ac-498d-b018-fc09172af4d3",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "11ac5527-4a43-4184-8d96-be54cbca82ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "a5c52759-7dd2-45f8-8305-cf3812c33062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11ac5527-4a43-4184-8d96-be54cbca82ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3eb381-d6fd-4e0b-a1d3-c80d0c1e8cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11ac5527-4a43-4184-8d96-be54cbca82ff",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "09c01eca-11d9-4ddd-be60-355d8f4ea17e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "368afa33-ffde-4037-b627-14093fb3accb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09c01eca-11d9-4ddd-be60-355d8f4ea17e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc2965c-6a6b-4e42-a504-fe477c94bc81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09c01eca-11d9-4ddd-be60-355d8f4ea17e",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "1af02138-9404-4bd3-9f50-c7511066ff54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "19f9f847-e249-4f13-9bd2-9d64576cf32a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af02138-9404-4bd3-9f50-c7511066ff54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d105f5b9-809d-46c1-b82a-85d777337145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af02138-9404-4bd3-9f50-c7511066ff54",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "07b26a01-a09a-4df7-96fe-ced217695b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "22f1e462-259d-4862-b192-f4ff3bcff02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07b26a01-a09a-4df7-96fe-ced217695b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a2791df-3dbe-4b7e-bf34-f22675091bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07b26a01-a09a-4df7-96fe-ced217695b62",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "3390c427-3411-4c12-9e70-f86bc9b0b5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "989fbb03-398d-458a-b926-aa45b3e9fd7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3390c427-3411-4c12-9e70-f86bc9b0b5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc5335c6-1a8f-480e-a5d7-e59d4ee4c77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3390c427-3411-4c12-9e70-f86bc9b0b5d9",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "1ec97bc2-098a-4430-b495-ee29f65bfaaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "a90a01ef-1e44-4805-a7a2-c27a2280b2e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec97bc2-098a-4430-b495-ee29f65bfaaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcebfe28-f46b-4f56-b60c-71d6bdea9972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec97bc2-098a-4430-b495-ee29f65bfaaf",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "e1008a3b-c15f-457d-97a8-8b89a4784e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "da021bff-512b-4b00-9887-4b1619733de7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1008a3b-c15f-457d-97a8-8b89a4784e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3955efae-8d33-4401-af64-86ed130bfa30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1008a3b-c15f-457d-97a8-8b89a4784e45",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "093250db-02f8-4fce-9ee3-273c4314537d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "2a1f92ba-b1a8-4b4b-becb-de57e461e4c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093250db-02f8-4fce-9ee3-273c4314537d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b5fd4ad-9659-421e-9a96-6e24d05c1796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093250db-02f8-4fce-9ee3-273c4314537d",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "ba7125fe-a788-4ba1-9fb8-4f86c8248e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "66af875f-8d65-4c91-97ad-02062fe1bc99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba7125fe-a788-4ba1-9fb8-4f86c8248e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5802552-00dc-4452-92be-5e7a22a54f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba7125fe-a788-4ba1-9fb8-4f86c8248e1a",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "534253b7-daaa-4b34-ac9a-34281ed3765c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "bd3ca81d-6e0f-4071-8801-dc5645ef4970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "534253b7-daaa-4b34-ac9a-34281ed3765c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edfbd317-e9f5-4407-98e4-58e6c38124b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "534253b7-daaa-4b34-ac9a-34281ed3765c",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "564e0bf1-b1bb-4d9e-8951-d95b5b6b80c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "def97f2b-093b-4faa-8c2f-36e20efd9385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "564e0bf1-b1bb-4d9e-8951-d95b5b6b80c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff660129-c3ca-4a97-8363-344107590045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "564e0bf1-b1bb-4d9e-8951-d95b5b6b80c3",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "5c130c35-7de9-4be7-9102-e97f801dbd9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "7e20cf79-c06c-4792-bf27-a68cb52ade5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c130c35-7de9-4be7-9102-e97f801dbd9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca34ee8e-070d-4ed7-8002-67276c131a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c130c35-7de9-4be7-9102-e97f801dbd9c",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "10f129dd-fc0d-4d94-81cf-0075da4a89a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "b8748636-f080-4237-9987-44ab6f26ffc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f129dd-fc0d-4d94-81cf-0075da4a89a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac837e74-de4d-4c91-8613-0cbb71ec6038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f129dd-fc0d-4d94-81cf-0075da4a89a6",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "0bc75f64-c484-46b2-90b0-0f7dea3e1aed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "c2571c83-6fc9-411b-ba2b-f42e34a603b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc75f64-c484-46b2-90b0-0f7dea3e1aed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34178f03-e819-44a2-8a04-415e0355f6b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc75f64-c484-46b2-90b0-0f7dea3e1aed",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        },
        {
            "id": "86f3ab44-beda-4a40-8902-e24e604f69b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "compositeImage": {
                "id": "77409efc-6a20-452f-b64b-4039399a0be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f3ab44-beda-4a40-8902-e24e604f69b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e11990-ab33-4858-a63d-708fe857e046",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f3ab44-beda-4a40-8902-e24e604f69b0",
                    "LayerId": "65937823-f8e2-4e7c-a394-6c2787fc2431"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "65937823-f8e2-4e7c-a394-6c2787fc2431",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17317750-b0fe-47c4-8ae4-34505287afe5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 56,
    "yorig": 67
}