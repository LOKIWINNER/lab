{
    "id": "909f1664-555e-47ad-a7e6-d784274d090f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_attack_hold_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 37,
    "bbox_right": 82,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "837d8f24-caed-49de-baeb-7c90ee8eafb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "909f1664-555e-47ad-a7e6-d784274d090f",
            "compositeImage": {
                "id": "348dc946-bb7f-4a73-aa87-1db13daa31be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "837d8f24-caed-49de-baeb-7c90ee8eafb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d1a749-0f3e-4157-a615-56b9dfd0cc21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "837d8f24-caed-49de-baeb-7c90ee8eafb2",
                    "LayerId": "531a595e-bf4c-4de4-a7a2-584730a9cc77"
                }
            ]
        },
        {
            "id": "0f0e1de0-4d04-4b17-98e2-6ca3eb443f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "909f1664-555e-47ad-a7e6-d784274d090f",
            "compositeImage": {
                "id": "92f701d6-b76c-41e1-ab0b-29b73f89f470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f0e1de0-4d04-4b17-98e2-6ca3eb443f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf9ee11-be95-4f50-aeec-dbc8cf834273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f0e1de0-4d04-4b17-98e2-6ca3eb443f29",
                    "LayerId": "531a595e-bf4c-4de4-a7a2-584730a9cc77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 134,
    "layers": [
        {
            "id": "531a595e-bf4c-4de4-a7a2-584730a9cc77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "909f1664-555e-47ad-a7e6-d784274d090f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}