{
    "id": "09da240a-c227-4e81-a84f-8453618f50be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "silver_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bcc8400-280e-4cfa-92ab-b63441989ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "f00ef848-130d-4206-b4ec-a5a6bc3080f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bcc8400-280e-4cfa-92ab-b63441989ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68c4dff9-849f-4737-845e-2d5804975c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bcc8400-280e-4cfa-92ab-b63441989ef4",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "d8338e27-764a-45b3-9990-724e5edf429a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "8c74bcb4-bb43-4680-988c-11a5f696396e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8338e27-764a-45b3-9990-724e5edf429a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d2e17c-103d-4d10-8bcd-71b27cc1ee05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8338e27-764a-45b3-9990-724e5edf429a",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "4b791b44-fd8d-4428-a3a5-a4538eceffe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "b15bc982-7f81-42d1-9a40-c63411e129c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b791b44-fd8d-4428-a3a5-a4538eceffe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26effad6-d6f8-41db-a8b3-4fcaae97ec7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b791b44-fd8d-4428-a3a5-a4538eceffe6",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "59c8856f-7ffd-422c-8130-113e5e341f58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "6c551e28-99da-41f2-b11c-500b90e84738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59c8856f-7ffd-422c-8130-113e5e341f58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1794b3fa-e163-4a04-83d9-2e3e4bb1110d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59c8856f-7ffd-422c-8130-113e5e341f58",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "306e1fbc-6bf7-4b6c-9ab2-fd62cd102a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "77301809-224a-45ec-a262-542f8d014595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306e1fbc-6bf7-4b6c-9ab2-fd62cd102a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2047f9e0-ade0-4595-be6f-978c85735545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306e1fbc-6bf7-4b6c-9ab2-fd62cd102a36",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "b4170acb-9e75-4520-80cb-48124e833117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "e7515b53-1822-4c98-924f-204f0c877194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4170acb-9e75-4520-80cb-48124e833117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "965b2066-60a7-447f-8909-bb8aac6c341b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4170acb-9e75-4520-80cb-48124e833117",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "bac2f42f-086c-4e10-b7cc-9e8d5c523313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "15b30de9-49ad-40c5-8b72-cd06fd983037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac2f42f-086c-4e10-b7cc-9e8d5c523313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d82e0d-080d-4c9a-9494-23ce2f887f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac2f42f-086c-4e10-b7cc-9e8d5c523313",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        },
        {
            "id": "85b04d14-760f-4709-9caa-b805feeffd30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "compositeImage": {
                "id": "f1ada7b7-24f2-4463-ad09-1cf57f2aa99f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b04d14-760f-4709-9caa-b805feeffd30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b14123-7d72-4b59-9474-e377c053a668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b04d14-760f-4709-9caa-b805feeffd30",
                    "LayerId": "5139782b-517c-4ee1-9c33-a67f5e082ea4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5139782b-517c-4ee1-9c33-a67f5e082ea4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09da240a-c227-4e81-a84f-8453618f50be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 28
}