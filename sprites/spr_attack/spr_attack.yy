{
    "id": "f52e5662-c6fb-451a-b947-af5578c54cb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fd7605c-30cd-4770-8eed-4e1932072553",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f52e5662-c6fb-451a-b947-af5578c54cb8",
            "compositeImage": {
                "id": "29c2406b-d229-429d-8f9a-1720bb0946a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd7605c-30cd-4770-8eed-4e1932072553",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8500fa3-c26e-4ff5-912f-81cdc33e92a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd7605c-30cd-4770-8eed-4e1932072553",
                    "LayerId": "b686d15d-c707-4c9d-a404-9eaa45e79941"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "b686d15d-c707-4c9d-a404-9eaa45e79941",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f52e5662-c6fb-451a-b947-af5578c54cb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 16
}