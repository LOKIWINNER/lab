{
    "id": "3f8d9e05-07ca-47eb-923b-14b026150167",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 20,
    "bbox_right": 43,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c940d23-532e-4c99-9429-33bbde2e7ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f8d9e05-07ca-47eb-923b-14b026150167",
            "compositeImage": {
                "id": "b476a06a-580c-4fae-ab78-3614f73f19af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c940d23-532e-4c99-9429-33bbde2e7ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f907de4-a018-4755-9212-f58a87a14a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c940d23-532e-4c99-9429-33bbde2e7ca2",
                    "LayerId": "53979ad0-54d1-49fe-a856-1de4094ac364"
                }
            ]
        }
    ],
    "gridX": 2,
    "gridY": 2,
    "height": 64,
    "layers": [
        {
            "id": "53979ad0-54d1-49fe-a856-1de4094ac364",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f8d9e05-07ca-47eb-923b-14b026150167",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}