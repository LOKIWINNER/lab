{
    "id": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 7,
    "bbox_right": 57,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd337e14-3b8b-4243-bfb1-e51fef8dd0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "ede9f425-574c-48e4-bb9b-9e65b8e8712e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd337e14-3b8b-4243-bfb1-e51fef8dd0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af25558f-a56f-4a42-adbf-b0ab7894d81c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd337e14-3b8b-4243-bfb1-e51fef8dd0d2",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "6583231c-4630-4722-86ea-7ae5e6ed6d29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "0963d2e0-5d4f-49c1-8a6a-fec6635ff763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6583231c-4630-4722-86ea-7ae5e6ed6d29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b833369-8441-401d-b927-52947295b4ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6583231c-4630-4722-86ea-7ae5e6ed6d29",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "e79977a0-60d5-4e90-a747-61f09fa2f651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "8ada4aea-a9d8-44bc-b65f-c2a0a3277e88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e79977a0-60d5-4e90-a747-61f09fa2f651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f61cfb-6146-4a5a-8b7c-eb46fe0192cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e79977a0-60d5-4e90-a747-61f09fa2f651",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "63c11f70-de47-46c4-9d13-865489d70d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "cc9e41cb-bc53-42c1-9290-37d8b6195641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c11f70-de47-46c4-9d13-865489d70d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2668a0b0-e8bd-444c-b98f-a06cbb65a3cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c11f70-de47-46c4-9d13-865489d70d87",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "dd392788-9c84-48b3-b1c5-fac1ac9d2b3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "9f86960e-e9bc-4b1f-80df-3db6c8c9d152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd392788-9c84-48b3-b1c5-fac1ac9d2b3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7742213-b404-47cf-9605-3ae668276ece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd392788-9c84-48b3-b1c5-fac1ac9d2b3c",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "2ba2b960-965e-4830-b1ea-99695755e53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "4e91dc75-a2eb-4729-9d9f-03c45c0d1c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba2b960-965e-4830-b1ea-99695755e53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb4592c-049b-4d4a-8e43-25b4a11bb544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba2b960-965e-4830-b1ea-99695755e53d",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "5eb44210-334d-4416-9ce5-ec3e4298c021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "1ceeb9bd-12b9-46a9-942d-a8643c167f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb44210-334d-4416-9ce5-ec3e4298c021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d32759-8ea5-42e4-b8ce-629b1d14900b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb44210-334d-4416-9ce5-ec3e4298c021",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        },
        {
            "id": "0b970c44-191e-42d9-bd3a-2b78ba4db610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "compositeImage": {
                "id": "c4988861-70dd-4831-8680-87833ac028d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b970c44-191e-42d9-bd3a-2b78ba4db610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43aca2f3-e6ac-4182-95c0-77bfcf82124f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b970c44-191e-42d9-bd3a-2b78ba4db610",
                    "LayerId": "fec4e51b-12f1-4f46-8baa-43569b702826"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fec4e51b-12f1-4f46-8baa-43569b702826",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b9a9bb7-e294-43a5-9a03-5c2368426e81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}