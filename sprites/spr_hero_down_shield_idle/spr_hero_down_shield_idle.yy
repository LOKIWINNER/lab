{
    "id": "bccaf0b7-758a-4762-bb58-dcaae5aa4f9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_shield_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 20,
    "bbox_right": 41,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af3ca658-560a-4518-8e0f-35a47e9f5cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bccaf0b7-758a-4762-bb58-dcaae5aa4f9f",
            "compositeImage": {
                "id": "b6a1fbb1-02ee-4ae8-b228-60c4f413e429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3ca658-560a-4518-8e0f-35a47e9f5cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "427a99d1-2035-4368-ac91-4ec0d6ae6972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3ca658-560a-4518-8e0f-35a47e9f5cd1",
                    "LayerId": "014e2620-cf63-4d90-b8e1-27baecfe38f3"
                }
            ]
        },
        {
            "id": "5e7618f5-4152-4f3b-bcd2-e506ca66d859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bccaf0b7-758a-4762-bb58-dcaae5aa4f9f",
            "compositeImage": {
                "id": "b0c39f72-6474-4eb4-a5ef-8f15982760ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7618f5-4152-4f3b-bcd2-e506ca66d859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e3e115e-3312-4218-9384-715eff067993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7618f5-4152-4f3b-bcd2-e506ca66d859",
                    "LayerId": "014e2620-cf63-4d90-b8e1-27baecfe38f3"
                }
            ]
        },
        {
            "id": "95d31a27-0dd7-487d-84f2-4600e31c3113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bccaf0b7-758a-4762-bb58-dcaae5aa4f9f",
            "compositeImage": {
                "id": "11f0f2f8-db42-468a-8877-6e5f0cdaece6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d31a27-0dd7-487d-84f2-4600e31c3113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88af779d-0649-4427-b4cd-c23ee867619a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d31a27-0dd7-487d-84f2-4600e31c3113",
                    "LayerId": "014e2620-cf63-4d90-b8e1-27baecfe38f3"
                }
            ]
        },
        {
            "id": "2236cc8e-0a06-47be-be88-b56ec286f91f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bccaf0b7-758a-4762-bb58-dcaae5aa4f9f",
            "compositeImage": {
                "id": "6b406344-6466-4dbd-90e2-e2ca51166dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2236cc8e-0a06-47be-be88-b56ec286f91f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8068429d-207d-4df3-97f7-9ced2916db72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2236cc8e-0a06-47be-be88-b56ec286f91f",
                    "LayerId": "014e2620-cf63-4d90-b8e1-27baecfe38f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "014e2620-cf63-4d90-b8e1-27baecfe38f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bccaf0b7-758a-4762-bb58-dcaae5aa4f9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 34
}