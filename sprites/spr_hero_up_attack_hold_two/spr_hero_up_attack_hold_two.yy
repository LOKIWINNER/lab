{
    "id": "192f97f3-a416-44eb-9ed1-374fd0d79fe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_attack_hold_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 17,
    "bbox_right": 41,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc82f2ea-c592-4c45-aa12-e41cbbd28148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "192f97f3-a416-44eb-9ed1-374fd0d79fe3",
            "compositeImage": {
                "id": "45776f71-04c9-4f72-a8bf-f92f92780c16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc82f2ea-c592-4c45-aa12-e41cbbd28148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3b3c76f-7ad4-4865-8569-9a28aee27a8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc82f2ea-c592-4c45-aa12-e41cbbd28148",
                    "LayerId": "ca43f962-49dd-4e87-bd55-3596ff992cef"
                }
            ]
        },
        {
            "id": "bf00b204-04c0-4628-91c1-ef01cd665aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "192f97f3-a416-44eb-9ed1-374fd0d79fe3",
            "compositeImage": {
                "id": "baa41a69-150c-421b-a61e-acf54343130b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf00b204-04c0-4628-91c1-ef01cd665aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2da560-e105-4ec2-a776-10ce710e2935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf00b204-04c0-4628-91c1-ef01cd665aa4",
                    "LayerId": "ca43f962-49dd-4e87-bd55-3596ff992cef"
                }
            ]
        },
        {
            "id": "c1d04a6b-171a-4d0c-b68a-e80e681f9a77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "192f97f3-a416-44eb-9ed1-374fd0d79fe3",
            "compositeImage": {
                "id": "cf064380-50c7-4182-97f6-101029ccfa47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d04a6b-171a-4d0c-b68a-e80e681f9a77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48aacbc-25c0-4197-97f8-6938d84aaafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d04a6b-171a-4d0c-b68a-e80e681f9a77",
                    "LayerId": "ca43f962-49dd-4e87-bd55-3596ff992cef"
                }
            ]
        },
        {
            "id": "ba2ec568-51ab-47ff-8470-88295615002b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "192f97f3-a416-44eb-9ed1-374fd0d79fe3",
            "compositeImage": {
                "id": "d2d42212-6279-491f-87c3-8873f8cd5f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2ec568-51ab-47ff-8470-88295615002b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1af0d38f-56bd-4824-8bdd-0e9bdec11bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2ec568-51ab-47ff-8470-88295615002b",
                    "LayerId": "ca43f962-49dd-4e87-bd55-3596ff992cef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ca43f962-49dd-4e87-bd55-3596ff992cef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "192f97f3-a416-44eb-9ed1-374fd0d79fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}