{
    "id": "14d10c94-f236-44ec-a00a-a1be07202d09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_attack_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 40,
    "bbox_right": 63,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6995543-a38f-4514-857d-05e58e6ec9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "88bf3b10-39fa-4859-97ce-cc85c0d5108c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6995543-a38f-4514-857d-05e58e6ec9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea78a238-5221-4597-b949-5ebcfce1f484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6995543-a38f-4514-857d-05e58e6ec9e7",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "4ebd5dd3-153b-44bb-91e7-8a1333280f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "8fd9db14-ee2c-4547-b6a3-8156505baf19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ebd5dd3-153b-44bb-91e7-8a1333280f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02e6b77e-cb78-46fe-924d-830a77f17256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ebd5dd3-153b-44bb-91e7-8a1333280f2c",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "95bb024e-5153-4e31-ab90-be3f517417ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "b7d5ecdc-4e27-441a-8b46-19a6c202df41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95bb024e-5153-4e31-ab90-be3f517417ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09e1bed1-ce73-42b1-b731-035fe0d45335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95bb024e-5153-4e31-ab90-be3f517417ea",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "c4bc19f9-8269-4271-a504-76a77bc5cda9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "6cee1fbb-9cf7-4c7c-8830-d0763193dcc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4bc19f9-8269-4271-a504-76a77bc5cda9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82886b43-4e01-4de1-9d2b-83e548de6b97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4bc19f9-8269-4271-a504-76a77bc5cda9",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "bea796dc-f0b3-4b20-9a99-b2bde40b9566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "c9aa57d4-aa65-44a2-a71e-715f77ae6597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bea796dc-f0b3-4b20-9a99-b2bde40b9566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a54ce856-4f17-4963-84cc-15fc2fd10772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea796dc-f0b3-4b20-9a99-b2bde40b9566",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "65d5a481-9389-448c-8b80-8248aa3d85c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "b7ff14e1-d1f5-4b6a-a336-1f1e0479ee41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d5a481-9389-448c-8b80-8248aa3d85c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da33705e-37d0-4444-ab99-9908706c24e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d5a481-9389-448c-8b80-8248aa3d85c7",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "1a5e9292-3279-4dfa-b257-d8cd702e637e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "f94969c2-b9fe-4130-90ca-00bc79926ba8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a5e9292-3279-4dfa-b257-d8cd702e637e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34070373-e780-40dc-9471-fc174344fc3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a5e9292-3279-4dfa-b257-d8cd702e637e",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "7b5f00a4-7ae0-4e05-9418-adebe7545267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "54ecb44c-d393-4e7f-ba1b-8cc9092340b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b5f00a4-7ae0-4e05-9418-adebe7545267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84b9c278-97bd-4a7c-8d2a-16ead282ed9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b5f00a4-7ae0-4e05-9418-adebe7545267",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "f9f5cf94-aa85-43d1-a341-3975d73a42f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "321d4b00-f2d2-4301-838c-31ced398b258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f5cf94-aa85-43d1-a341-3975d73a42f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d01627c-7d3f-4e96-b9c5-f150d74f4fe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f5cf94-aa85-43d1-a341-3975d73a42f1",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "03f6ec52-31d7-4bf3-8711-a0bab1657b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "4259288f-ae2d-41bc-b5a5-acceb6e0dbed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f6ec52-31d7-4bf3-8711-a0bab1657b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ac11a5-b59b-46c7-a9a5-07c8b292d6e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f6ec52-31d7-4bf3-8711-a0bab1657b3f",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        },
        {
            "id": "f5cc61f7-f658-4f05-a3ff-1c50f577da5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "compositeImage": {
                "id": "da076a50-3795-4ed2-9ddd-05b95cbbb11c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5cc61f7-f658-4f05-a3ff-1c50f577da5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26aa7501-52ae-4ed0-9402-55b121868e14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5cc61f7-f658-4f05-a3ff-1c50f577da5d",
                    "LayerId": "83bfff1e-589a-4063-907e-e5c6dec609b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "83bfff1e-589a-4063-907e-e5c6dec609b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14d10c94-f236-44ec-a00a-a1be07202d09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}