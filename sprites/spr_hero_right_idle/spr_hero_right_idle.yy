{
    "id": "b77d9a43-7467-47d2-9bf8-9def6d388412",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 24,
    "bbox_right": 47,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bae6d32d-4094-4d6f-9c68-b6ead781be1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "3c1ef547-1c46-444e-8698-896acb88687d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae6d32d-4094-4d6f-9c68-b6ead781be1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0557844b-8d35-494c-888e-7344af7115a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae6d32d-4094-4d6f-9c68-b6ead781be1e",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "4124e4c6-e247-415d-99cd-d2e2873ef85a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "4cf8bc09-480a-4b1d-a5c3-e04507133ba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4124e4c6-e247-415d-99cd-d2e2873ef85a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7308525-850c-4466-9def-f3fe68790ef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4124e4c6-e247-415d-99cd-d2e2873ef85a",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "bf95a2ef-000a-4ab7-95c1-257232bf0e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "b68b9b1b-bd48-4041-a469-b9f460c8e12f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf95a2ef-000a-4ab7-95c1-257232bf0e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c72a358-9b4f-4c90-8534-89ba4a7d9ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf95a2ef-000a-4ab7-95c1-257232bf0e62",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "cf78050b-ee62-41d7-a255-2c5ac2c2949f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "8baa5bf6-b284-4b8d-8f45-0015aceb20fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf78050b-ee62-41d7-a255-2c5ac2c2949f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e45f7c67-8497-4887-8a03-047153d1a8d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf78050b-ee62-41d7-a255-2c5ac2c2949f",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "9135e29a-b27c-45d4-b9a9-18a474774a2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "934cbe47-c691-4d8e-a353-ad38fafb3022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9135e29a-b27c-45d4-b9a9-18a474774a2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24720097-81e2-4b17-9101-f04e2de62645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9135e29a-b27c-45d4-b9a9-18a474774a2b",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "657c3e49-09fe-4fdc-8aad-a6795f06bd79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "0401692b-8a90-4323-8814-3640675646bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "657c3e49-09fe-4fdc-8aad-a6795f06bd79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31fa7da9-3229-4aab-b0e1-20e50d6d1d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "657c3e49-09fe-4fdc-8aad-a6795f06bd79",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "513f8a10-7072-40ca-b98b-c5142a75c1e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "b575ed66-df8c-4f77-a56c-b05b76913ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "513f8a10-7072-40ca-b98b-c5142a75c1e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956f49bc-0b00-4b5f-95d7-f608ed0e47ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "513f8a10-7072-40ca-b98b-c5142a75c1e5",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        },
        {
            "id": "117dfb19-b4f8-4e64-95a2-a0489423b45d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "compositeImage": {
                "id": "9c7a796e-f648-4439-9780-dacf940132d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "117dfb19-b4f8-4e64-95a2-a0489423b45d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e66a9c0-ef31-4e7f-b5ac-bcee901daaa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "117dfb19-b4f8-4e64-95a2-a0489423b45d",
                    "LayerId": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be9d4f14-b9a4-42b3-93f3-7d47ad85844b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b77d9a43-7467-47d2-9bf8-9def6d388412",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}