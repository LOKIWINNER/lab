{
    "id": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_magicball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de8d1925-b61e-433c-8a7c-b8cb44099223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "09c68af6-2873-4a53-b867-02f6b14e7f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de8d1925-b61e-433c-8a7c-b8cb44099223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5033d896-c647-41b0-8764-1855effbbaf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de8d1925-b61e-433c-8a7c-b8cb44099223",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "1f5b1e85-c4e5-443e-b26f-6d4c0df1a8d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "1cc0feaa-0377-4de8-83de-fa16caec1230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5b1e85-c4e5-443e-b26f-6d4c0df1a8d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8a522e-a604-43f8-b1a1-0397925cc3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5b1e85-c4e5-443e-b26f-6d4c0df1a8d1",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "007ac6fe-3f6e-42c5-8177-99e3bfc39f0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "25b4fc41-bcb4-42d8-b56c-9c04ee465c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007ac6fe-3f6e-42c5-8177-99e3bfc39f0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dfdf61f-39ae-4a1a-bb64-db558e5a670a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007ac6fe-3f6e-42c5-8177-99e3bfc39f0b",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "e7bb229d-f3f1-4854-920c-947f2e24728d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "85b80a30-166f-4073-8aab-28840703de0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7bb229d-f3f1-4854-920c-947f2e24728d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e24157-b786-40b9-b417-dc3eb0d4fb71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7bb229d-f3f1-4854-920c-947f2e24728d",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "0ee21105-d54a-400c-a547-848ce7b41f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "7c798dd0-a66b-479a-977a-8072a89f285c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee21105-d54a-400c-a547-848ce7b41f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32724e90-ea14-46ef-8c6a-76fa50155c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee21105-d54a-400c-a547-848ce7b41f90",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "4743423d-ec10-4441-863f-7fefc0840965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "89001a29-3e2d-4383-a787-d8628fd0f79f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4743423d-ec10-4441-863f-7fefc0840965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515fed10-56a4-4921-a847-2283a669e55f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4743423d-ec10-4441-863f-7fefc0840965",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "2cf2b211-bec2-4ecb-bb34-90d03c84fa2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "52743a42-7070-479c-b68d-cdefac93c472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf2b211-bec2-4ecb-bb34-90d03c84fa2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d3bcfbc-acb7-46fc-9fae-0dd01f945e38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf2b211-bec2-4ecb-bb34-90d03c84fa2c",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        },
        {
            "id": "823dfb54-1cd1-4b73-ba77-35c24a242403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "compositeImage": {
                "id": "ac5c40a9-c81c-454a-9308-6e3dac5a6e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823dfb54-1cd1-4b73-ba77-35c24a242403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86d37819-a3b1-4f61-b249-ab053531f5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823dfb54-1cd1-4b73-ba77-35c24a242403",
                    "LayerId": "ba150661-dbb9-44e3-8778-7deb1017d1cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba150661-dbb9-44e3-8778-7deb1017d1cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}