{
    "id": "5a732c26-f6fa-4965-9729-284a2a30a8ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c11738a-95f4-4f28-a4e2-53e3370742f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a732c26-f6fa-4965-9729-284a2a30a8ff",
            "compositeImage": {
                "id": "c93b3431-b494-4126-a59d-8871d3ef527d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c11738a-95f4-4f28-a4e2-53e3370742f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6924bdb2-18dd-4e41-b84e-559b9a1d00c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c11738a-95f4-4f28-a4e2-53e3370742f8",
                    "LayerId": "fba47bfa-3f52-4b7f-b5b4-b8bc6a831d13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fba47bfa-3f52-4b7f-b5b4-b8bc6a831d13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a732c26-f6fa-4965-9729-284a2a30a8ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 62,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}