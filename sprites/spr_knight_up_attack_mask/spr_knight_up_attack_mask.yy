{
    "id": "54218a50-793d-44cc-a636-7084f2c95856",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_up_attack_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7d94c7e-a002-4dec-b2fa-937a1b90db71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "93065404-3df1-48c8-8fe7-b5fcfa0901a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d94c7e-a002-4dec-b2fa-937a1b90db71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03eb0a25-f8f3-4642-b5e9-c964cdb18080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d94c7e-a002-4dec-b2fa-937a1b90db71",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "0638d17f-a267-4707-afbf-7304245d67b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "c34e4328-5104-44bc-9be6-f7f5607df9eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0638d17f-a267-4707-afbf-7304245d67b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e49c681b-bd21-46be-8acb-9b97179928e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0638d17f-a267-4707-afbf-7304245d67b1",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "12861822-0ec1-402c-94eb-b824468690ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "baed8b09-67ff-4e62-8211-8765938d95b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12861822-0ec1-402c-94eb-b824468690ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0f7c5f1-c62c-454a-8d3a-660a99b5082b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12861822-0ec1-402c-94eb-b824468690ee",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "ed9ab0df-fd6e-4e77-84d0-b0206fffc80c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "3ae95db3-dd69-4ad0-8d18-2a8123145c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9ab0df-fd6e-4e77-84d0-b0206fffc80c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a33b910-401e-48f1-baf9-5437c42fd7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9ab0df-fd6e-4e77-84d0-b0206fffc80c",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "88edc0f3-1267-4963-aaed-b37d5981b63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "8cdb6e02-a761-48ce-bcdc-cbfdda41bbd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88edc0f3-1267-4963-aaed-b37d5981b63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53fba877-ca9e-4243-8b71-f9a136ce31cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88edc0f3-1267-4963-aaed-b37d5981b63d",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "d3703d3d-5ce0-4dee-995d-b6cafe2e1fbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "e892652c-f40a-48f9-9863-82505d3b4c62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3703d3d-5ce0-4dee-995d-b6cafe2e1fbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a293ab3-7638-480f-ae73-fbe6d50ef67a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3703d3d-5ce0-4dee-995d-b6cafe2e1fbc",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "41e27fe8-2353-44e3-bfa7-b53ca4284655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "e960d850-ad4f-4f2e-8d55-fc946a33f7e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41e27fe8-2353-44e3-bfa7-b53ca4284655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f84e2b-9933-4b07-809c-14db1cfdaffa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41e27fe8-2353-44e3-bfa7-b53ca4284655",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "ae996b7d-8772-4eba-b8ff-aee680a03474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "476132ee-21e4-4bdf-8e12-60fd324cac33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae996b7d-8772-4eba-b8ff-aee680a03474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b117f69-3f74-4d28-b433-65f22a592d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae996b7d-8772-4eba-b8ff-aee680a03474",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "e1896c23-0ff6-48f4-84eb-b1074d3dabc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "c2f7bbd5-d296-4ef5-8b65-413d6f5d7f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1896c23-0ff6-48f4-84eb-b1074d3dabc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ccff9e-b615-4c1a-8fd9-efbc647df1fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1896c23-0ff6-48f4-84eb-b1074d3dabc8",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        },
        {
            "id": "3c62492d-8a17-4ed6-a654-8ad0598b7df5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "compositeImage": {
                "id": "20c24275-b486-41ba-9815-baa412bb9001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c62492d-8a17-4ed6-a654-8ad0598b7df5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce19239d-6cbf-43dd-bb30-a10e74f531f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c62492d-8a17-4ed6-a654-8ad0598b7df5",
                    "LayerId": "ed4202e8-ae6c-461d-907c-2b41f753e394"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ed4202e8-ae6c-461d-907c-2b41f753e394",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54218a50-793d-44cc-a636-7084f2c95856",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 48,
    "yorig": 66
}