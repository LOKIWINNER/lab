{
    "id": "788cbcfd-3264-43b4-9a79-3371556e4e5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d43c2443-5699-442d-9e41-fa2f0c86886b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "788cbcfd-3264-43b4-9a79-3371556e4e5c",
            "compositeImage": {
                "id": "4580eca8-6d0f-4efc-a11a-8289f43a653c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43c2443-5699-442d-9e41-fa2f0c86886b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baed3678-6b8c-4500-9258-63a13898b8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43c2443-5699-442d-9e41-fa2f0c86886b",
                    "LayerId": "16bd4840-152a-4c66-88bd-4f1521615376"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "16bd4840-152a-4c66-88bd-4f1521615376",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "788cbcfd-3264-43b4-9a79-3371556e4e5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}