{
    "id": "1c611b56-fd0d-4436-a5b4-881267c6b610",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a63437c8-d6bb-43b9-8e5c-8fc33470d374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c611b56-fd0d-4436-a5b4-881267c6b610",
            "compositeImage": {
                "id": "298ecbad-f133-4b91-b908-3dbede3dd888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a63437c8-d6bb-43b9-8e5c-8fc33470d374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92069509-fd3c-4a14-8cad-03b887ab6a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a63437c8-d6bb-43b9-8e5c-8fc33470d374",
                    "LayerId": "e8267202-d37f-4492-b8c2-87bffcad3b04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e8267202-d37f-4492-b8c2-87bffcad3b04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c611b56-fd0d-4436-a5b4-881267c6b610",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}