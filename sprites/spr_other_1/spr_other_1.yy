{
    "id": "f1f7c498-5783-4607-a952-4ffafa80e752",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_other_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "998aa28d-211e-463c-8392-f09d163d2657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1f7c498-5783-4607-a952-4ffafa80e752",
            "compositeImage": {
                "id": "05e67946-3f56-42f6-b82a-f8003398280e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "998aa28d-211e-463c-8392-f09d163d2657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54ca46d-443a-43ab-a399-6c0e2f4953ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "998aa28d-211e-463c-8392-f09d163d2657",
                    "LayerId": "1a81791a-6a6d-449e-9c66-1fa722894b0c"
                }
            ]
        },
        {
            "id": "ffcabc89-d8c0-43b2-9533-4d39d04e7f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1f7c498-5783-4607-a952-4ffafa80e752",
            "compositeImage": {
                "id": "8324f793-574c-4d09-aacb-94ec1dc97ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffcabc89-d8c0-43b2-9533-4d39d04e7f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84592c9e-956f-47f4-b78c-5ceb21877fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffcabc89-d8c0-43b2-9533-4d39d04e7f23",
                    "LayerId": "1a81791a-6a6d-449e-9c66-1fa722894b0c"
                }
            ]
        },
        {
            "id": "dcf44a61-ba61-41f1-aa2a-29664a9430a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1f7c498-5783-4607-a952-4ffafa80e752",
            "compositeImage": {
                "id": "d9fd2732-ef6c-440f-a602-1bbcb5dd24a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf44a61-ba61-41f1-aa2a-29664a9430a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b084c3eb-f429-4b07-99e4-23f194d32427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf44a61-ba61-41f1-aa2a-29664a9430a0",
                    "LayerId": "1a81791a-6a6d-449e-9c66-1fa722894b0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1a81791a-6a6d-449e-9c66-1fa722894b0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1f7c498-5783-4607-a952-4ffafa80e752",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}