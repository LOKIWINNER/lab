{
    "id": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_shield_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 18,
    "bbox_right": 59,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b65d2fea-6e1e-4227-8d09-a2796b29273a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "c1f8038e-bc00-4686-88a8-a852c3e4f1ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65d2fea-6e1e-4227-8d09-a2796b29273a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373784f9-e327-45b2-b267-d8fec481b401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65d2fea-6e1e-4227-8d09-a2796b29273a",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        },
        {
            "id": "469560be-7488-4227-9402-98e3d1b61858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "f3acdcdc-2d8c-4a94-a795-fc51f895e834",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "469560be-7488-4227-9402-98e3d1b61858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8747fd2-a823-48f0-b160-7739cffd697a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "469560be-7488-4227-9402-98e3d1b61858",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        },
        {
            "id": "2b63dd86-af6d-492b-b720-9b5f0c9254be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "77bc586e-2a8c-44f2-8e00-6a26b2e0a828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b63dd86-af6d-492b-b720-9b5f0c9254be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e30857-071a-4f62-8435-9367a1645436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b63dd86-af6d-492b-b720-9b5f0c9254be",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        },
        {
            "id": "c18926b2-2966-4171-99b6-12748c668ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "bfea3795-002c-4a3f-b057-d497f79f34f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c18926b2-2966-4171-99b6-12748c668ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5496c59e-b13f-49cf-b5dc-136cc7659449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c18926b2-2966-4171-99b6-12748c668ee8",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        },
        {
            "id": "43058904-a148-4553-a3bb-f005c1ce6a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "9a67dbc7-a62d-4c45-aa19-2800d70d19cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43058904-a148-4553-a3bb-f005c1ce6a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2141889-3d47-4e42-91d6-fea600014d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43058904-a148-4553-a3bb-f005c1ce6a61",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        },
        {
            "id": "1dfd624e-db9c-4c97-b133-034eef1ee6eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "0ca0c472-76a1-48a1-a130-6968482e2315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfd624e-db9c-4c97-b133-034eef1ee6eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6126fd26-7f25-4fed-93d7-50d718c03029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfd624e-db9c-4c97-b133-034eef1ee6eb",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        },
        {
            "id": "f033d99d-7148-49af-91c9-3663efa37c3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "compositeImage": {
                "id": "94673ce5-aa66-4194-997d-cdc05304efaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f033d99d-7148-49af-91c9-3663efa37c3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "424a9290-45b6-4194-af0b-0ddf1c1b2600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f033d99d-7148-49af-91c9-3663efa37c3c",
                    "LayerId": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e0cfc99-a4c2-4c32-bcb9-52a8c9313241",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7922777-fbfa-43b9-a954-f1b4d903a09f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}