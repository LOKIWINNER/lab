{
    "id": "d6e9373e-9817-417f-b5f2-60225b70c301",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWalls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6ad4946-1248-4452-bf0d-15859fe5c41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "ff2e1b4f-4442-4c56-bd27-cbc2afc33b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ad4946-1248-4452-bf0d-15859fe5c41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb8bb3c-6966-4cc0-b28a-61be17d53edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ad4946-1248-4452-bf0d-15859fe5c41c",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "5e2bd968-20c1-4924-9289-eaf3db1e83aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "a3c057ac-8712-40dc-aefb-b300425cb797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2bd968-20c1-4924-9289-eaf3db1e83aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40c17d43-8a08-4f12-9d28-fa736cb18600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2bd968-20c1-4924-9289-eaf3db1e83aa",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "e5e5674d-aa95-4f00-96fd-12dcf7cec7f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "e3f14598-bb24-42da-bea9-0db1a0d03d11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e5674d-aa95-4f00-96fd-12dcf7cec7f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2944e15-a79d-4f87-a027-71fff7a66f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e5674d-aa95-4f00-96fd-12dcf7cec7f2",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "f751b14e-d0fc-41ac-baae-6f0d6ae0639a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "e2d73399-1bd3-4238-976c-dcaa0d4cb05f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f751b14e-d0fc-41ac-baae-6f0d6ae0639a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7717a334-abbd-4347-aa47-cae8bd58ae6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f751b14e-d0fc-41ac-baae-6f0d6ae0639a",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "30ef54f9-b4c4-4971-bf37-97b7b03d0d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "eee36834-c79a-4662-9309-a389557a5b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ef54f9-b4c4-4971-bf37-97b7b03d0d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d5fba0-1e3b-4f6b-976a-f788541f1b50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ef54f9-b4c4-4971-bf37-97b7b03d0d1d",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "744f6b03-36ee-457d-b984-1e0d42df0a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "685d2d35-4caa-4593-ae22-7f40ba98f701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "744f6b03-36ee-457d-b984-1e0d42df0a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3310987c-3de0-42ef-9928-a3770988b5dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "744f6b03-36ee-457d-b984-1e0d42df0a99",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "156e02d6-be5c-478a-9bee-55991bdfe328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "c57eaff5-ce36-41f3-aaef-6bcee860727d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "156e02d6-be5c-478a-9bee-55991bdfe328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e99d1cd-a4d1-490a-93b3-5cb2a80d859d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "156e02d6-be5c-478a-9bee-55991bdfe328",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "325b4f35-5d00-4c98-a3f2-698d810a49c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "6a576335-321a-404f-ab92-4e4e1908b7ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "325b4f35-5d00-4c98-a3f2-698d810a49c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "763b6f32-2738-4de2-a124-63fc51f43a08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "325b4f35-5d00-4c98-a3f2-698d810a49c4",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "92e19abf-f085-4814-a951-ad21a5cc6bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "29c133fb-6fae-4598-ae4a-f9112e21dd2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e19abf-f085-4814-a951-ad21a5cc6bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0018a6c5-bdcc-48e1-b654-b0b67d1b2165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e19abf-f085-4814-a951-ad21a5cc6bc1",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "2e8b95b1-69b2-4a02-b07f-790630a1b633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "1992f3fc-608b-48ea-8a0b-c9b277214099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8b95b1-69b2-4a02-b07f-790630a1b633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0c5ff2-5f92-4d5c-b6a6-a08701eb96ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8b95b1-69b2-4a02-b07f-790630a1b633",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "8da0ef5c-29a4-42df-ae55-211d4b4dd45d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "f98f8d67-3e89-4376-8e9e-8d31db7d4a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da0ef5c-29a4-42df-ae55-211d4b4dd45d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6035f13-235f-4d93-b932-c41434d82d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da0ef5c-29a4-42df-ae55-211d4b4dd45d",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "dafb7885-6f0b-4d1e-beec-2af873913a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "4fda057a-0d54-4c13-8948-923a65f4ab5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dafb7885-6f0b-4d1e-beec-2af873913a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcfdd85a-27b5-4cab-8765-acaaf3504c92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dafb7885-6f0b-4d1e-beec-2af873913a2d",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "5de1acbc-9823-48dd-8b71-11467096f016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "a34a84f3-e118-4bb6-89f0-14c002e9c1b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de1acbc-9823-48dd-8b71-11467096f016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "730ed678-7d04-4986-ba60-fc438dab7bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de1acbc-9823-48dd-8b71-11467096f016",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "470db050-bf09-4eb1-8e98-3c3f0e6f4a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "a1229461-d1d2-4790-9aee-5b0502b202e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "470db050-bf09-4eb1-8e98-3c3f0e6f4a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9bc642e-f277-469f-8a2d-2155b3e36aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "470db050-bf09-4eb1-8e98-3c3f0e6f4a39",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "9144c5f5-b392-4e5e-adfd-d2c9d3aa2e11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "84a6d235-09c7-4fde-bf1d-2dc197ffa6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9144c5f5-b392-4e5e-adfd-d2c9d3aa2e11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c67a780f-9fd2-40a4-85b0-a6b676cdb252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9144c5f5-b392-4e5e-adfd-d2c9d3aa2e11",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        },
        {
            "id": "eb30bbd4-ff48-4627-9bbf-34987d860e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "compositeImage": {
                "id": "ba211fe3-5b97-494b-b0d3-0310d485251d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb30bbd4-ff48-4627-9bbf-34987d860e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21255259-7422-4c76-914b-e3386746cda9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb30bbd4-ff48-4627-9bbf-34987d860e02",
                    "LayerId": "082a3a53-37de-4f6d-9912-5548c8f528fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "082a3a53-37de-4f6d-9912-5548c8f528fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6e9373e-9817-417f-b5f2-60225b70c301",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 4,
    "yorig": 0
}