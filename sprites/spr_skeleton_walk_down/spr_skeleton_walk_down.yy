{
    "id": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 51,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca99d64e-1a5f-4330-9934-d07e58e45a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "d58baeec-bc70-452b-adfe-f1acd485ccf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca99d64e-1a5f-4330-9934-d07e58e45a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1d634c-f07c-4616-bc39-7ab6883d77b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca99d64e-1a5f-4330-9934-d07e58e45a05",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "84399ed9-9fb1-4dd3-87e9-7ed159f3eec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "478f7022-c03c-449e-8ee4-b204a5695926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84399ed9-9fb1-4dd3-87e9-7ed159f3eec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a39642b7-4a67-4da0-a1f1-a1c5d4dd59d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84399ed9-9fb1-4dd3-87e9-7ed159f3eec8",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "f9040336-e649-4902-a324-b2b7d3c92bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "7f374310-2c3a-4a30-b293-13cd44d5bcd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9040336-e649-4902-a324-b2b7d3c92bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e07ef0d1-fb65-47bf-a49b-9c8fdb5970ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9040336-e649-4902-a324-b2b7d3c92bc5",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "3fc13d72-f82e-4e82-9cdd-c1cb45013c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "727bfe28-4848-4f68-acec-7600edc40f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc13d72-f82e-4e82-9cdd-c1cb45013c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa110fed-03db-40e2-a070-e4179c6550f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc13d72-f82e-4e82-9cdd-c1cb45013c19",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "c7430c7e-a695-4007-b4d8-d06a27114d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "21f5de61-a623-4649-8de4-29e2d0922890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7430c7e-a695-4007-b4d8-d06a27114d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bbb05b1-1fb4-46d2-bb57-a0cb2759a03f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7430c7e-a695-4007-b4d8-d06a27114d9d",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "b77462bb-e52e-46fe-92ed-2f3a5c746ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "a8f40550-b91b-470a-9744-33823b87818b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77462bb-e52e-46fe-92ed-2f3a5c746ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b94838-7a21-4dda-8632-e78efe9a92d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77462bb-e52e-46fe-92ed-2f3a5c746ffe",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "d140bcec-8f18-4f5e-a356-b6736937fe77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "88053ea8-cc95-4cf4-ab4e-dc3e72e65fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d140bcec-8f18-4f5e-a356-b6736937fe77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50768f2d-c4e5-4c17-9b51-58977a4cf7cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d140bcec-8f18-4f5e-a356-b6736937fe77",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        },
        {
            "id": "77942df6-e4ba-4c10-8e1b-0696234d4e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "compositeImage": {
                "id": "78df4a0b-1b3e-4460-b32d-5bf6ed00e58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77942df6-e4ba-4c10-8e1b-0696234d4e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5afdca-6e5b-441c-9559-dd56a825cfc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77942df6-e4ba-4c10-8e1b-0696234d4e3c",
                    "LayerId": "9e812258-d12e-4582-b355-049f6a51d1ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e812258-d12e-4582-b355-049f6a51d1ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28d26614-80c5-4ae7-a87b-7648bdf3587d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}