{
    "id": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_attack_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 9,
    "bbox_right": 53,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "311acccd-e8c0-4199-b654-7b8314bacd18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "c1ad9141-97f2-4046-836a-3135038aacc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311acccd-e8c0-4199-b654-7b8314bacd18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876377bf-0c83-424c-9927-db462deee806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311acccd-e8c0-4199-b654-7b8314bacd18",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "f5ea7e8d-1dcd-44ed-a4d4-03e16d4c94a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "07813e11-7480-4796-9884-cd98f2e64823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ea7e8d-1dcd-44ed-a4d4-03e16d4c94a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8524e29-c393-4e24-8d89-c40bd35597e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ea7e8d-1dcd-44ed-a4d4-03e16d4c94a9",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "bb48ac0b-0898-4625-91b0-f7c16713ca13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "86782311-e25e-4d29-90e8-ecbd5d7bd6b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb48ac0b-0898-4625-91b0-f7c16713ca13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb6af91-4777-4a65-9f00-353d42ff256e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb48ac0b-0898-4625-91b0-f7c16713ca13",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "c0e4e1c7-d61a-4703-b2a3-eee5e19bcad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "a25ddc21-1c34-4909-af9a-eba87ec2262c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0e4e1c7-d61a-4703-b2a3-eee5e19bcad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7f50d0-bae0-4f1e-a5ba-e78d80bc223a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0e4e1c7-d61a-4703-b2a3-eee5e19bcad4",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "3e6f778a-79ec-42f1-a8dc-7508231c0a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "783a4aca-bfc1-4024-904b-98b762b31e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6f778a-79ec-42f1-a8dc-7508231c0a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4f7eef9-c698-46e5-9a3e-5e11dd3ddfe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6f778a-79ec-42f1-a8dc-7508231c0a2a",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "eba5b37b-9fbf-4914-b152-dd8ee9c4d214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "75332c1b-03e4-4490-a4ce-f79074127478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba5b37b-9fbf-4914-b152-dd8ee9c4d214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac63bafa-e670-40e0-89e1-bad6af0221fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba5b37b-9fbf-4914-b152-dd8ee9c4d214",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "239de887-4a00-48c4-833e-3db8db64a37c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "62bb457a-6370-4f92-80fa-b2921540c60c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "239de887-4a00-48c4-833e-3db8db64a37c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e1944e-6d0f-4bcc-80ba-0674b91fc8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239de887-4a00-48c4-833e-3db8db64a37c",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        },
        {
            "id": "a684c265-9b16-4c41-9fad-8c67d1580a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "compositeImage": {
                "id": "8bfb419e-d345-4614-9075-6fa7ea86f07b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a684c265-9b16-4c41-9fad-8c67d1580a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98fb308-8362-4212-9e1d-02bb7c6b70d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a684c265-9b16-4c41-9fad-8c67d1580a96",
                    "LayerId": "c5eeff2f-b810-4f78-8fdc-92524432d4b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c5eeff2f-b810-4f78-8fdc-92524432d4b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5318ef2d-acca-4d23-bced-ef4034a6be8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}