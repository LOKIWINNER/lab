{
    "id": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 4,
    "bbox_right": 54,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c0baa42-3b23-438e-9711-3089448d22c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "971231f7-5b18-48ac-b695-1a27d38b6121",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c0baa42-3b23-438e-9711-3089448d22c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c06b3e3e-636f-4ced-9f5d-2b271330c8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c0baa42-3b23-438e-9711-3089448d22c4",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        },
        {
            "id": "fadfd585-0d78-48c8-8dcf-a56ebd7aa31f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "3987cbc5-8c72-48a7-b5c2-f3e80f0f2bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fadfd585-0d78-48c8-8dcf-a56ebd7aa31f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08be2d5-48f2-4603-b840-19a1458fb91c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fadfd585-0d78-48c8-8dcf-a56ebd7aa31f",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        },
        {
            "id": "6f23b685-3854-4338-b1e3-6de097ff1b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "93789ba2-7926-40e5-9499-784c9f0fe5fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f23b685-3854-4338-b1e3-6de097ff1b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "032552e2-85b7-40c2-92b8-ae6ef572f5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f23b685-3854-4338-b1e3-6de097ff1b1e",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        },
        {
            "id": "ab451594-038a-40ce-beef-874acd890b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "f7c4164f-f086-49cb-a4b7-16184015c773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab451594-038a-40ce-beef-874acd890b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3722030-26fc-4c63-924b-7fdb93b97a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab451594-038a-40ce-beef-874acd890b6e",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        },
        {
            "id": "30e8c4c7-abfe-42fa-a576-9bab77c67337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "4b2bbf9a-88e7-42b1-b350-31af1d185e77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e8c4c7-abfe-42fa-a576-9bab77c67337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0a4d8d0-fce9-4d72-a13c-db10724da7a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e8c4c7-abfe-42fa-a576-9bab77c67337",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        },
        {
            "id": "9e50c2f7-75bf-416e-bb11-04eb41ba1da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "045ded99-4e4e-49b9-a21a-9498660111ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e50c2f7-75bf-416e-bb11-04eb41ba1da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3e11888-3124-4a55-a1a8-95aeaa4ad051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e50c2f7-75bf-416e-bb11-04eb41ba1da6",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        },
        {
            "id": "b4aed399-0073-4e20-b76c-7fd96a8c970b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "compositeImage": {
                "id": "1d439034-154b-4eb9-a0a2-39fb2a982f2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4aed399-0073-4e20-b76c-7fd96a8c970b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ee3ef7-974e-4aa1-b0b9-fbd9b97f2084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4aed399-0073-4e20-b76c-7fd96a8c970b",
                    "LayerId": "0ba655a4-d8c7-4805-9b66-333725a7c126"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0ba655a4-d8c7-4805-9b66-333725a7c126",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93ad7f41-3e1c-4b43-a2cc-a21e172916a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}