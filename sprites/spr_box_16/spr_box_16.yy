{
    "id": "df15591f-f68a-44b7-ab33-666a8bb8e667",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box_16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "853175f4-9d9f-4652-8a12-4e7f23c8b283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df15591f-f68a-44b7-ab33-666a8bb8e667",
            "compositeImage": {
                "id": "3471723a-4e41-41e4-8781-5ae43a4fbcc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853175f4-9d9f-4652-8a12-4e7f23c8b283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a1ea9ee-443e-4f02-a393-db8669ee6977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853175f4-9d9f-4652-8a12-4e7f23c8b283",
                    "LayerId": "3422ee2e-7f63-41c2-ba4e-2f97fe643ef9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3422ee2e-7f63-41c2-ba4e-2f97fe643ef9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df15591f-f68a-44b7-ab33-666a8bb8e667",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 16
}