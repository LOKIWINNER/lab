{
    "id": "1e845dea-6b72-484e-82f0-6c20b7ff36f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_maze",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb645a61-46b5-4b73-a5d8-4b99e6d5b2ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e845dea-6b72-484e-82f0-6c20b7ff36f2",
            "compositeImage": {
                "id": "6fe7f566-cb22-443d-8b18-3ef57b20dee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb645a61-46b5-4b73-a5d8-4b99e6d5b2ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2549645d-30b5-4b21-8efb-39d7fc9a2197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb645a61-46b5-4b73-a5d8-4b99e6d5b2ce",
                    "LayerId": "33263f3a-fa7b-4555-b70d-8c4f28efe80f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "33263f3a-fa7b-4555-b70d-8c4f28efe80f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e845dea-6b72-484e-82f0-6c20b7ff36f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": -126,
    "yorig": 87
}