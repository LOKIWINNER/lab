{
    "id": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_shield_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51cfb934-fc57-496f-a586-5f232a5a060d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "54936437-f699-4fe6-8108-28e06af6884f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51cfb934-fc57-496f-a586-5f232a5a060d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ce8b7f-1298-4141-a2ab-fab37933242f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51cfb934-fc57-496f-a586-5f232a5a060d",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "93586537-b04a-4e56-819c-929f9193db02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "1e6fc9de-fee8-4908-9074-20ed1171ce9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93586537-b04a-4e56-819c-929f9193db02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7c5d0f4-0c00-4a6b-8db7-a5a8d55b9d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93586537-b04a-4e56-819c-929f9193db02",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "7d46fc52-3e72-44cd-b864-81c77f8eb83f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "ec3b1dc1-18be-4a48-a1a1-d4683d8154cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d46fc52-3e72-44cd-b864-81c77f8eb83f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c67617d-98ff-4c53-b498-58acd2632d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d46fc52-3e72-44cd-b864-81c77f8eb83f",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "6e278dff-dfd1-4925-a431-185d537a8f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "948f2c34-1a5b-4afd-bb67-949c8e2abf62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e278dff-dfd1-4925-a431-185d537a8f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87162d82-78e4-4e29-8b18-0e1d21dfc1eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e278dff-dfd1-4925-a431-185d537a8f39",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "245fce36-d764-44d8-91d2-12f060f76a2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "4012c96e-eef5-4da7-b7dd-eefd1e611432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245fce36-d764-44d8-91d2-12f060f76a2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a1cef87-568a-44c8-a6de-264053be2431",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245fce36-d764-44d8-91d2-12f060f76a2f",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "6ab58ccb-b7d9-444d-87e5-3b5d638c6bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "19bb42c5-ecf8-4f43-b062-a3696fa70403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ab58ccb-b7d9-444d-87e5-3b5d638c6bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47dca96f-9797-452c-bb19-93740680e396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ab58ccb-b7d9-444d-87e5-3b5d638c6bb3",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "f4cefb2d-a68c-47f7-9764-c071d57bed43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "f37c424c-e81c-416a-9ffe-e3a259ab3805",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4cefb2d-a68c-47f7-9764-c071d57bed43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf968ab-6526-4af9-abbd-1f3ed754b421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4cefb2d-a68c-47f7-9764-c071d57bed43",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "966f7047-d1a2-4d4a-b271-14611c4c3623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "ca4c6d74-7f20-46d1-ab91-c1e6232ddd0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "966f7047-d1a2-4d4a-b271-14611c4c3623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fed081d-b044-4f64-8874-ac5258981b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966f7047-d1a2-4d4a-b271-14611c4c3623",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "29c8b39f-78c9-4753-a250-d22dceffaf16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "c94e5dae-fb69-402c-9407-ac2d0144fc8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c8b39f-78c9-4753-a250-d22dceffaf16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06d2c933-9e32-459d-85d6-90ec9eafebe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c8b39f-78c9-4753-a250-d22dceffaf16",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "cc0b6839-d925-44da-bfe5-18f84f5b50ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "5f55b31d-2d14-4069-98f0-caf5a4479f4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0b6839-d925-44da-bfe5-18f84f5b50ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d9751f-eb02-4c4f-883f-43c2e84338fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0b6839-d925-44da-bfe5-18f84f5b50ca",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "7401a4a3-a469-475f-9486-856bdd313c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "82a950b4-b35c-4484-b6e1-0a9e738deda0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7401a4a3-a469-475f-9486-856bdd313c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b90be2-6335-4571-a0b2-dfc0e7cee326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7401a4a3-a469-475f-9486-856bdd313c35",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "b7c03acb-b2c6-422c-95b2-7f507bd06d01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "f6ed16d5-7705-4796-b209-b5ffa6aa7679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c03acb-b2c6-422c-95b2-7f507bd06d01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705506ca-ffa1-41d6-a2f6-990a9f34edcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c03acb-b2c6-422c-95b2-7f507bd06d01",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "4a748f65-fd85-4481-b145-1fb005b18284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "2b4dae7b-124f-48af-b0ae-ae7816710fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a748f65-fd85-4481-b145-1fb005b18284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a49db5b-b6fe-4d62-82bb-72d45bc4da03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a748f65-fd85-4481-b145-1fb005b18284",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "971aaefb-7a14-4d0d-8801-9b3de8359a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "364ccc0f-a6d8-41cd-b796-3ad62d4e5371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "971aaefb-7a14-4d0d-8801-9b3de8359a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84e9a0f3-e332-41c2-b23e-65aa0923a60e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "971aaefb-7a14-4d0d-8801-9b3de8359a4d",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        },
        {
            "id": "21555b84-a2f8-4e5e-b9a3-08cb37ff09bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "compositeImage": {
                "id": "a18988f9-05cf-42f9-a139-1aedfe4ce622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21555b84-a2f8-4e5e-b9a3-08cb37ff09bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445b6ffd-5256-415f-be27-ff9097c8b266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21555b84-a2f8-4e5e-b9a3-08cb37ff09bd",
                    "LayerId": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cb8ce0c3-e4e7-4d41-a82b-6bc076a8ad79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcf2fd5b-5e97-42e9-8c3b-a34588052515",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}