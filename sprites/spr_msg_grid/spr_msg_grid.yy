{
    "id": "58da1cb3-da6b-4b76-88b2-65e9c1cd8cce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb62d2dd-e1fc-41b3-bec1-0bfaa030cfc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58da1cb3-da6b-4b76-88b2-65e9c1cd8cce",
            "compositeImage": {
                "id": "7bb95746-82c8-4265-b8de-f907405490a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb62d2dd-e1fc-41b3-bec1-0bfaa030cfc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906df762-e7dc-483d-8875-d86f56359d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb62d2dd-e1fc-41b3-bec1-0bfaa030cfc9",
                    "LayerId": "34c9bc32-a9f5-4258-a5f1-bffc18783f87"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 48,
    "layers": [
        {
            "id": "34c9bc32-a9f5-4258-a5f1-bffc18783f87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58da1cb3-da6b-4b76-88b2-65e9c1cd8cce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 62,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}