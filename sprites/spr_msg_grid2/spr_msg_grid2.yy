{
    "id": "8777d0df-9b7f-4a27-a7a8-4ac6f754974b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 17,
    "bbox_right": 78,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "464762db-62a8-4fcf-b953-6a0de3ce63a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8777d0df-9b7f-4a27-a7a8-4ac6f754974b",
            "compositeImage": {
                "id": "83ccf764-6477-48ff-85fe-0c2d6ff3fab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "464762db-62a8-4fcf-b953-6a0de3ce63a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e25df92-865b-48cd-b526-f496306c97aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "464762db-62a8-4fcf-b953-6a0de3ce63a0",
                    "LayerId": "eb9d2c8e-25db-417c-8a1c-7512f633eb17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "eb9d2c8e-25db-417c-8a1c-7512f633eb17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8777d0df-9b7f-4a27-a7a8-4ac6f754974b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}