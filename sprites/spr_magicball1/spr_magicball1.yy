{
    "id": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_magicball1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 5,
    "bbox_right": 52,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f715a48d-59b9-43a1-ad1d-f9b49beac034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "82027531-6139-497c-a7c3-52bc151b2b53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f715a48d-59b9-43a1-ad1d-f9b49beac034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a496f4-2ed5-4504-aa58-2a8c51012afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f715a48d-59b9-43a1-ad1d-f9b49beac034",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "f66ceaa1-a101-4811-b91c-a59b5e7efd31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "a21074f2-a288-4da5-aabd-75d0dba10a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f66ceaa1-a101-4811-b91c-a59b5e7efd31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddec9cf2-f84c-4309-b2ff-28ce7142ca58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66ceaa1-a101-4811-b91c-a59b5e7efd31",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "368963d7-ee8a-467b-b5e7-e572ffe5bfb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "dfad615e-179a-4abd-bfd5-49453d20430c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368963d7-ee8a-467b-b5e7-e572ffe5bfb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4be69f5-decd-4ef5-a72a-c30efa96169b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368963d7-ee8a-467b-b5e7-e572ffe5bfb3",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "067dab55-6d00-401f-b4e9-93ef091dfb64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "c23cb25d-50c1-4bd6-8f4f-3276172d7adf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "067dab55-6d00-401f-b4e9-93ef091dfb64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038f6bc2-b695-48a2-ab29-6620691ff342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "067dab55-6d00-401f-b4e9-93ef091dfb64",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "244d4218-028e-4e2c-9760-ea50646c010b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "c16489d6-3013-4159-a984-021935560997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244d4218-028e-4e2c-9760-ea50646c010b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc82d8a-d1eb-4fa5-afef-e201211db81d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244d4218-028e-4e2c-9760-ea50646c010b",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "3444f61e-ae7f-4968-ac43-dcb7ae77a1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "8e0a0d99-c90c-42a9-900d-6d4a1cc2eb74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3444f61e-ae7f-4968-ac43-dcb7ae77a1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ed4a17-16ec-4265-8bb9-abb364996da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3444f61e-ae7f-4968-ac43-dcb7ae77a1c0",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "9044915a-8dca-41fb-81bd-7803eab46f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "212ad309-5b6c-4491-befa-7e0769c366b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9044915a-8dca-41fb-81bd-7803eab46f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0127a05f-b282-4d49-927d-2d53f476d862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9044915a-8dca-41fb-81bd-7803eab46f21",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        },
        {
            "id": "97fbbf3f-ceb1-499c-990f-a72c41310129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "compositeImage": {
                "id": "caf2af48-3759-4782-8208-651fdfe3dabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fbbf3f-ceb1-499c-990f-a72c41310129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2052a39e-ce3e-48ce-af0f-165840f0d8ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fbbf3f-ceb1-499c-990f-a72c41310129",
                    "LayerId": "3e1c0399-54a0-420d-a62a-6afd5e8db700"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3e1c0399-54a0-420d-a62a-6afd5e8db700",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ede24212-86a2-4ff4-80fb-be6cbfbf775c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}