{
    "id": "829dacbb-728f-459c-adce-70c0ff49eb8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite45",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5343,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "284bd818-ee8a-43ab-be7a-1928e49f5568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "829dacbb-728f-459c-adce-70c0ff49eb8c",
            "compositeImage": {
                "id": "0aaa37f0-8ded-4df1-b5b8-e642d0c88072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "284bd818-ee8a-43ab-be7a-1928e49f5568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491865a3-bb28-46e5-8d0c-e40a2238d611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "284bd818-ee8a-43ab-be7a-1928e49f5568",
                    "LayerId": "d4dc0e4c-ae13-44d1-abab-764381060f99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6032,
    "layers": [
        {
            "id": "d4dc0e4c-ae13-44d1-abab-764381060f99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "829dacbb-728f-459c-adce-70c0ff49eb8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}