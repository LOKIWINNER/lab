{
    "id": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_right_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 42,
    "bbox_right": 101,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de5062e3-7f68-47d7-aeb9-1b6cae92dccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "f2898216-989c-4c5d-b022-b80d39357fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de5062e3-7f68-47d7-aeb9-1b6cae92dccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f1c155-3946-4062-9318-4efe72d9f47c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de5062e3-7f68-47d7-aeb9-1b6cae92dccb",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "4d2a95dc-a8ce-4085-9f3d-04724caa8253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "fdffd0be-f802-48f2-827d-1b1ff079c2fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d2a95dc-a8ce-4085-9f3d-04724caa8253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e6cfe2-7740-4a9e-acb9-906b76a02781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d2a95dc-a8ce-4085-9f3d-04724caa8253",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "0b8186cf-7d65-47c1-8819-e171eac6e5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "d7265abd-b3ec-4ce7-9dc9-af7e64d01a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8186cf-7d65-47c1-8819-e171eac6e5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83603224-dcea-4051-bd2b-aa6900dba9f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8186cf-7d65-47c1-8819-e171eac6e5fc",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "5bff6268-00ca-4ecd-9d18-efe198331514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "b364c6a6-39e7-412c-ae6b-42eb5837d621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bff6268-00ca-4ecd-9d18-efe198331514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b971e63-ed5e-4668-9a6c-93f665fe53c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bff6268-00ca-4ecd-9d18-efe198331514",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "cf3fd65c-081e-4f5a-bb5f-46ebcbed2f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "721c96ae-155e-43b5-ae60-97a98e1be69a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3fd65c-081e-4f5a-bb5f-46ebcbed2f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8a55ba-d57a-4b16-9186-875e33c9c05a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3fd65c-081e-4f5a-bb5f-46ebcbed2f92",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "28048490-44aa-481b-996f-c078f5e93b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "ecadcc30-1c03-48d3-9847-204d41b81870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28048490-44aa-481b-996f-c078f5e93b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "347d5cf5-7110-4ecb-bf4e-1be7c4909d7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28048490-44aa-481b-996f-c078f5e93b5c",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "3780de63-4ace-4ee9-83b6-8a0958af4ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "f07bfe92-cbe9-4e46-aac5-ae9095a22b10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3780de63-4ace-4ee9-83b6-8a0958af4ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0343895-0c26-4310-9479-2b3ed29de577",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3780de63-4ace-4ee9-83b6-8a0958af4ad3",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "c6a07c26-8ae4-4a91-a42a-091bcdaf1557",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "98ae843f-a659-4f17-8fa1-532333b6ff2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a07c26-8ae4-4a91-a42a-091bcdaf1557",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2aedbda-44e2-444d-a825-d37a05b43e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a07c26-8ae4-4a91-a42a-091bcdaf1557",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "8743d785-7ec2-462c-a5c0-c95e72951584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "5e891654-f1d5-46b9-97cc-c6587c50c900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8743d785-7ec2-462c-a5c0-c95e72951584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c8dc8c-d8d2-4d71-9a09-547f18866e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8743d785-7ec2-462c-a5c0-c95e72951584",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "5ebce514-5b41-4da0-9516-98f96a1232f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "3fb4b764-a306-400b-b660-18465cc412fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ebce514-5b41-4da0-9516-98f96a1232f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ffcc0b-5741-488a-9492-80ec8491b00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ebce514-5b41-4da0-9516-98f96a1232f1",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "fc2178ff-9c94-4fae-bcf8-e23a2b5b34fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "12af4a22-1abe-4824-b601-1f05adface32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2178ff-9c94-4fae-bcf8-e23a2b5b34fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d51ad7ac-ad72-460a-8995-32b104201016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2178ff-9c94-4fae-bcf8-e23a2b5b34fd",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "c60244b0-efa7-4f74-9f66-8d3f5cca2409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "c78a18e0-5607-44e0-823c-47ad46fb5c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c60244b0-efa7-4f74-9f66-8d3f5cca2409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f3ac00-e590-4611-8a52-9a4502599e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c60244b0-efa7-4f74-9f66-8d3f5cca2409",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "4276d521-abb7-46ed-9a79-04b0ad0b9756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "3c508788-9ccc-4f31-bdeb-e59cb3f4d6ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4276d521-abb7-46ed-9a79-04b0ad0b9756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9703c442-302a-41ca-b00e-1e91df28e773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4276d521-abb7-46ed-9a79-04b0ad0b9756",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "62b0516f-dc71-426a-a9fe-b04a2b21331b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "dbb45e79-8a07-4e9c-8432-4d1ff1fbcf64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b0516f-dc71-426a-a9fe-b04a2b21331b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785fc269-c431-40da-b83d-e174d62a72fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b0516f-dc71-426a-a9fe-b04a2b21331b",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "59d1ede5-353c-4358-b503-b65d81bdd3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "41edff12-83a1-4fea-bfa4-afafb52d329c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59d1ede5-353c-4358-b503-b65d81bdd3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baa78248-d427-48e5-bdce-b8ffa81899c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59d1ede5-353c-4358-b503-b65d81bdd3a2",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "7eae6c71-a61c-4d11-898e-558c7876f3e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "e3133130-089d-4370-bfce-69a1ee36ade6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eae6c71-a61c-4d11-898e-558c7876f3e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6e59a2-0455-4c95-8b94-3a7790f83231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eae6c71-a61c-4d11-898e-558c7876f3e7",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "eb3e6eab-09d4-4f3c-86ab-d4578b0e0d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "ad1aa008-a492-4e7a-8438-ff70731d66bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3e6eab-09d4-4f3c-86ab-d4578b0e0d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "515feff9-2be3-4a96-a9c2-d3acac6ad8f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3e6eab-09d4-4f3c-86ab-d4578b0e0d0d",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "f7ffaaa5-e255-4f37-8fb5-696ed91f1305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "c75eac1d-5dd1-443e-82a4-4012f58ab9de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ffaaa5-e255-4f37-8fb5-696ed91f1305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59d6771e-e16d-41a8-bbb3-2597dbdef415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ffaaa5-e255-4f37-8fb5-696ed91f1305",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "e9f2b88b-eac9-4d05-9e94-1efce2de055f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "4321c907-9cca-44bf-90f5-f708aefee7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f2b88b-eac9-4d05-9e94-1efce2de055f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826d1d9c-2183-4d87-bde8-41898e751237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f2b88b-eac9-4d05-9e94-1efce2de055f",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        },
        {
            "id": "c87cb000-70db-47ca-8584-2f93faa1eccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "compositeImage": {
                "id": "71cdb838-7c6b-4353-b90c-a41924b73fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87cb000-70db-47ca-8584-2f93faa1eccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fadddc3-4d40-40fd-bd4b-c7ac47e6448b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87cb000-70db-47ca-8584-2f93faa1eccb",
                    "LayerId": "44ad2818-46c3-454b-af21-e94f6a3829ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "44ad2818-46c3-454b-af21-e94f6a3829ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c761e0c7-9d8c-4dbc-b317-aca9fa40f3a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 63,
    "yorig": 67
}