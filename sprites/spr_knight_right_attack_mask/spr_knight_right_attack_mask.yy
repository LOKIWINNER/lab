{
    "id": "c790f09a-0b49-4596-9d98-390bc5ef4749",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_right_attack_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "548548f3-dc62-4930-82b9-93afbd83f96d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "412853cd-9167-4a12-af7f-1e41cde60454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "548548f3-dc62-4930-82b9-93afbd83f96d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6e44ea-dc6f-4fd2-8153-710583965202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "548548f3-dc62-4930-82b9-93afbd83f96d",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "84397dc3-440e-46a4-a7b9-eeaf4c9ef14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "3429c781-1633-4b27-aa50-963b09f8e8b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84397dc3-440e-46a4-a7b9-eeaf4c9ef14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83274c36-e4ae-49e2-b33e-83d418bda49c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84397dc3-440e-46a4-a7b9-eeaf4c9ef14e",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "ef94d9c2-ba8d-4ab6-b6df-fcc537789c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "eee13675-8432-4662-9e27-bba07206f27a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef94d9c2-ba8d-4ab6-b6df-fcc537789c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34dd36ae-a8e3-4e06-bb94-df325db0ba94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef94d9c2-ba8d-4ab6-b6df-fcc537789c21",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "47b88299-a9be-4dad-ac06-e825009f6b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "376de4f7-20c2-4784-8911-86c09ea59016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b88299-a9be-4dad-ac06-e825009f6b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3cbd1cf-384a-4e9e-8645-228b00bdbed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b88299-a9be-4dad-ac06-e825009f6b78",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "b00d03a1-676f-47fc-b431-f6f960042ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "81a2e09d-f857-4dac-b570-d6a6fa8fe7c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b00d03a1-676f-47fc-b431-f6f960042ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a2862b-fda4-42e9-ba65-3dc614277626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b00d03a1-676f-47fc-b431-f6f960042ad6",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "96902184-d4c4-4204-8d1f-e3886980217d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "e10abb89-a944-46ce-b2f8-38540fdf18ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96902184-d4c4-4204-8d1f-e3886980217d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f428a267-6350-4d2e-a0a7-40819f60b3c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96902184-d4c4-4204-8d1f-e3886980217d",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "cb8682d6-8db3-41cd-83ae-262210657801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "b8b5c602-aab5-4897-bf47-719a336da214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8682d6-8db3-41cd-83ae-262210657801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2448ec46-762d-4f13-9ed3-40b28b5c2c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8682d6-8db3-41cd-83ae-262210657801",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "1911177d-a76b-40dc-88f9-1b21d678259c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "d7fe6933-cf28-4b16-8713-bc48fb888a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1911177d-a76b-40dc-88f9-1b21d678259c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a9c1c5-8171-4d65-a42f-1e279e1a9173",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1911177d-a76b-40dc-88f9-1b21d678259c",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "baa5ecee-f0b5-4415-a6ed-b8dc7e0fb9ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "36a59218-dc2b-4e17-9ea8-51b9552fe065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa5ecee-f0b5-4415-a6ed-b8dc7e0fb9ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62faa45a-87cd-49dd-8c1a-9beabca6b5fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa5ecee-f0b5-4415-a6ed-b8dc7e0fb9ba",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        },
        {
            "id": "02472627-69a6-4e40-b1cb-c44686e3e224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "compositeImage": {
                "id": "27515593-ae84-46cf-b084-cb011c04f47b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02472627-69a6-4e40-b1cb-c44686e3e224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec38b3a1-f6d1-43d2-bc50-63ce29b0286b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02472627-69a6-4e40-b1cb-c44686e3e224",
                    "LayerId": "1416c1f8-21ed-45dc-b410-ef68317b3953"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1416c1f8-21ed-45dc-b410-ef68317b3953",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c790f09a-0b49-4596-9d98-390bc5ef4749",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 63,
    "yorig": 67
}