{
    "id": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireball1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 7,
    "bbox_right": 60,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0668e63-82bb-4d94-b548-27d3c83fbbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
            "compositeImage": {
                "id": "419af500-9e28-45d8-b859-c1c76f6bb0cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0668e63-82bb-4d94-b548-27d3c83fbbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f46fdb-c594-4b4b-b70c-7cd7a310bce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0668e63-82bb-4d94-b548-27d3c83fbbea",
                    "LayerId": "3807d635-9bbc-4eb7-8cae-f88cc3db7962"
                }
            ]
        },
        {
            "id": "844af20e-3670-4423-a124-bc1cd04de777",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
            "compositeImage": {
                "id": "dcb43591-0737-40df-ba3e-6c3507e21a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "844af20e-3670-4423-a124-bc1cd04de777",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b73c19e-7f25-4d98-94e8-40059df33285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "844af20e-3670-4423-a124-bc1cd04de777",
                    "LayerId": "3807d635-9bbc-4eb7-8cae-f88cc3db7962"
                }
            ]
        },
        {
            "id": "ab277a7c-42a6-4141-a2f9-94ea0e305d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
            "compositeImage": {
                "id": "53403f1c-e67e-490d-9498-d6780bf1ab0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab277a7c-42a6-4141-a2f9-94ea0e305d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d16b99a-4833-4f83-b04c-f8e7e3255083",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab277a7c-42a6-4141-a2f9-94ea0e305d43",
                    "LayerId": "3807d635-9bbc-4eb7-8cae-f88cc3db7962"
                }
            ]
        },
        {
            "id": "bb448178-210e-49e6-9ca0-361570691cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
            "compositeImage": {
                "id": "7ac3fa7c-f319-4877-9bf0-da6754c106ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb448178-210e-49e6-9ca0-361570691cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b82575-9d6f-4016-9c9a-3dad30066a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb448178-210e-49e6-9ca0-361570691cd6",
                    "LayerId": "3807d635-9bbc-4eb7-8cae-f88cc3db7962"
                }
            ]
        },
        {
            "id": "5db60592-9c3f-4266-8245-3919b1edaec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
            "compositeImage": {
                "id": "0589206d-5f42-4ac0-9b43-c7ed23c9b3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db60592-9c3f-4266-8245-3919b1edaec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58384e5c-edb0-4a63-a491-db54e60933e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db60592-9c3f-4266-8245-3919b1edaec8",
                    "LayerId": "3807d635-9bbc-4eb7-8cae-f88cc3db7962"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3807d635-9bbc-4eb7-8cae-f88cc3db7962",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e036fa51-3041-48c8-b74b-1f9487bba7ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}