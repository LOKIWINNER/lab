{
    "id": "488bf0a8-2942-4144-a7ea-0aab30119075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba58a164-0893-4914-90bc-38a164fee67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "488bf0a8-2942-4144-a7ea-0aab30119075",
            "compositeImage": {
                "id": "acfb76e2-2f91-417f-98b8-b7d695388e1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba58a164-0893-4914-90bc-38a164fee67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95827c76-d316-45e1-9f6c-1e119e8b1c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba58a164-0893-4914-90bc-38a164fee67e",
                    "LayerId": "999a5938-b8fb-4914-95c7-e7e735327093"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "999a5938-b8fb-4914-95c7-e7e735327093",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "488bf0a8-2942-4144-a7ea-0aab30119075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 0,
    "yorig": 0
}