{
    "id": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 24,
    "bbox_right": 48,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8178a5ba-5f79-4dc5-adb6-6b9d65f0294f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "a534ac68-ce60-4b2c-99d7-9ad3246f0c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8178a5ba-5f79-4dc5-adb6-6b9d65f0294f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51618110-5410-4f5e-954b-e1a99b3eacf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8178a5ba-5f79-4dc5-adb6-6b9d65f0294f",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "d4be8d32-2a6b-4401-aee6-df07ec1bb6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "3bb13690-494d-44dd-9ec7-aad24ea5c8fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4be8d32-2a6b-4401-aee6-df07ec1bb6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9b4499-4362-40c4-b33d-9d316e96493b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4be8d32-2a6b-4401-aee6-df07ec1bb6e7",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "7f64d12c-f5ac-499c-af90-524150bca15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "fb48a66a-9491-4a9b-b5b5-cc08a574bc59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f64d12c-f5ac-499c-af90-524150bca15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b31077e0-6341-482c-a293-8e42bc65ff27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f64d12c-f5ac-499c-af90-524150bca15f",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "75b3ebf0-b08a-4740-b281-8d36893589f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "2783048e-5db4-4ecb-8834-588149e82532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b3ebf0-b08a-4740-b281-8d36893589f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ede68d-7715-4c09-946d-5c1a08f68607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b3ebf0-b08a-4740-b281-8d36893589f4",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "beba16be-6120-4559-819a-2685afd01c0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "1fd300b2-c467-4577-9b78-aebab520255b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beba16be-6120-4559-819a-2685afd01c0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7561e73e-3127-417b-a04a-f158868791d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beba16be-6120-4559-819a-2685afd01c0c",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "6a77adf1-9153-42aa-9419-8e19d25ea73c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "f2b8caa3-b977-4919-b780-58ebe1e7b401",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a77adf1-9153-42aa-9419-8e19d25ea73c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccdd5781-0749-4de3-be65-02bebdaaf9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a77adf1-9153-42aa-9419-8e19d25ea73c",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "9f0f8b05-57e4-432c-8cc9-20f834c60c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "05cf84f3-dc41-4fe1-be61-d03dc1f27496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f0f8b05-57e4-432c-8cc9-20f834c60c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a207d4-7791-4d3a-b712-7fd8f1f9fd60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f0f8b05-57e4-432c-8cc9-20f834c60c4c",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "5640ed91-7e4d-4acd-a630-ccb6e0e1c171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "aa819ede-d517-47b0-bb29-ab774f65e2d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5640ed91-7e4d-4acd-a630-ccb6e0e1c171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172d05aa-de19-4b17-8cb1-ab4317320f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5640ed91-7e4d-4acd-a630-ccb6e0e1c171",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "3cd8c2a8-8879-45de-b7b7-336b5d6589d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "4643a0e5-47ed-4fae-ab2d-89fc328eed23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd8c2a8-8879-45de-b7b7-336b5d6589d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0b4d74-ce68-4fa8-9c27-dc106d4fabe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd8c2a8-8879-45de-b7b7-336b5d6589d7",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "911c309a-7088-4691-88a0-a2970b5560dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "9a2724fb-cbd3-4856-ae4b-3b7fad84af4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "911c309a-7088-4691-88a0-a2970b5560dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba2fc75-8553-49ac-97c6-08e55d89ec18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "911c309a-7088-4691-88a0-a2970b5560dd",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "2033b5b2-c0a8-4e0d-b0a5-677047106137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "cc98695b-0667-4426-b957-8f15a8dcb79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2033b5b2-c0a8-4e0d-b0a5-677047106137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1542d25-802d-4c48-a8e7-3a4908cdc92f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2033b5b2-c0a8-4e0d-b0a5-677047106137",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "77e5f91c-080d-42c4-85de-835c6a4ea876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "0d05f919-9b4c-42bf-92e2-c37f2aaa49ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e5f91c-080d-42c4-85de-835c6a4ea876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec0a3938-9517-49df-a130-18ad209ae2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e5f91c-080d-42c4-85de-835c6a4ea876",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "5208eb6b-a6f4-4b5c-928f-86ea0ec0d1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "b2e55d26-84e7-4706-81a4-b37fb01a5a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5208eb6b-a6f4-4b5c-928f-86ea0ec0d1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a52636f9-ce6a-425d-8999-10811d78daa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5208eb6b-a6f4-4b5c-928f-86ea0ec0d1f9",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "88b1ce8b-2a0b-4f60-a276-7dc5f2917c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "6509f59c-850f-42a3-a0dc-21b940ee0926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88b1ce8b-2a0b-4f60-a276-7dc5f2917c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c1e5809-d829-46f9-be27-e77d08e5e254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88b1ce8b-2a0b-4f60-a276-7dc5f2917c56",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "89a46d1b-68d3-428d-8d44-0880590b31f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "6a580b69-566c-4510-b749-78852cdceccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a46d1b-68d3-428d-8d44-0880590b31f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91eeddca-3e87-41d4-86dc-605bc1d62a25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a46d1b-68d3-428d-8d44-0880590b31f2",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "394e5ce5-46b7-462a-81a3-c8511ebfa79e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "1a954f95-6efb-4a33-8828-c19a4799d30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "394e5ce5-46b7-462a-81a3-c8511ebfa79e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "155c8d6e-2de2-451a-bace-cadd68283929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "394e5ce5-46b7-462a-81a3-c8511ebfa79e",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "155c2a55-9253-4bc5-8f91-97e81b68c59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "394b40c0-a21d-4440-8aea-f5927cd3f02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "155c2a55-9253-4bc5-8f91-97e81b68c59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725847fc-a43e-4d1c-87a1-5d918cad9aa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "155c2a55-9253-4bc5-8f91-97e81b68c59b",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "0b62bffe-dcb3-40af-b047-62fab7a61902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "ff2ab228-3777-45a2-8b36-02e01c7230ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b62bffe-dcb3-40af-b047-62fab7a61902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d316c0-ff18-4952-ad9f-e477e85ac9b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b62bffe-dcb3-40af-b047-62fab7a61902",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "e3d7010d-e095-4e63-9e56-a20af9d84f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "b96dfa68-2079-4b81-8bd6-08b394bd7a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3d7010d-e095-4e63-9e56-a20af9d84f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c582e98-a8f1-4660-ae88-58faa662fb86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3d7010d-e095-4e63-9e56-a20af9d84f87",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        },
        {
            "id": "1f89810b-e6e4-48c4-835d-f39493373d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "compositeImage": {
                "id": "8decda77-3922-4ed7-b33b-b821982a587a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f89810b-e6e4-48c4-835d-f39493373d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "321c31bf-4466-4ed0-9a5c-290b9b9b1d09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f89810b-e6e4-48c4-835d-f39493373d68",
                    "LayerId": "b5589716-04ca-4c32-919a-00b05e70fb30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b5589716-04ca-4c32-919a-00b05e70fb30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10955cbc-3e94-46d2-a9bc-d22bc8b49331",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}