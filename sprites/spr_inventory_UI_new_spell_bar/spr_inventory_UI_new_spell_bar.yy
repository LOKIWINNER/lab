{
    "id": "30a0ee41-ad2b-466c-9fca-6328f6b3d532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new_spell_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 623,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a3062ed-1ae0-4706-86c5-97c610fa306d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30a0ee41-ad2b-466c-9fca-6328f6b3d532",
            "compositeImage": {
                "id": "ca0e991d-9a7b-484e-a4c2-5894f5226c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a3062ed-1ae0-4706-86c5-97c610fa306d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f23daa4-83f9-4b41-baa4-481091995d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a3062ed-1ae0-4706-86c5-97c610fa306d",
                    "LayerId": "c9971e1a-7428-4486-a16b-9a33b01450c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "c9971e1a-7428-4486-a16b-9a33b01450c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30a0ee41-ad2b-466c-9fca-6328f6b3d532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 624,
    "xorig": 0,
    "yorig": 0
}