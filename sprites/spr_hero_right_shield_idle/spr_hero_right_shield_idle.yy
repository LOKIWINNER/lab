{
    "id": "7685e287-8791-4e6a-b1cb-e93e9c545603",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_shield_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 22,
    "bbox_right": 48,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dc285db-d2fb-4824-a90b-70bb806d04db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7685e287-8791-4e6a-b1cb-e93e9c545603",
            "compositeImage": {
                "id": "2dd3290d-3551-4127-9a25-543d4046bac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dc285db-d2fb-4824-a90b-70bb806d04db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a99500-9f5f-44cb-bf4c-a28ee7565644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dc285db-d2fb-4824-a90b-70bb806d04db",
                    "LayerId": "f7807a55-568a-4f23-bf73-4d3a77bac461"
                }
            ]
        },
        {
            "id": "5ca2d34c-38d3-4c0f-baac-a64262d92268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7685e287-8791-4e6a-b1cb-e93e9c545603",
            "compositeImage": {
                "id": "a965f7af-39f4-4025-a002-411a7aeab8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca2d34c-38d3-4c0f-baac-a64262d92268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa0a8c5-124d-4084-b0ac-83c455c7c3bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca2d34c-38d3-4c0f-baac-a64262d92268",
                    "LayerId": "f7807a55-568a-4f23-bf73-4d3a77bac461"
                }
            ]
        },
        {
            "id": "cfab6761-45db-4011-9c6d-6ef126920a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7685e287-8791-4e6a-b1cb-e93e9c545603",
            "compositeImage": {
                "id": "9056d5af-2ce1-4ce0-bfcf-23c7be456d6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfab6761-45db-4011-9c6d-6ef126920a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2847c02-ec0d-4596-80d0-86abd0c0b01a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfab6761-45db-4011-9c6d-6ef126920a7a",
                    "LayerId": "f7807a55-568a-4f23-bf73-4d3a77bac461"
                }
            ]
        },
        {
            "id": "2b3fd7e9-1db8-4c05-9751-ff007e60fa6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7685e287-8791-4e6a-b1cb-e93e9c545603",
            "compositeImage": {
                "id": "66ff8271-a82c-4cf7-85fa-a1de839a553a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b3fd7e9-1db8-4c05-9751-ff007e60fa6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c374cf28-b688-4edc-b49e-221df7a2b873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b3fd7e9-1db8-4c05-9751-ff007e60fa6d",
                    "LayerId": "f7807a55-568a-4f23-bf73-4d3a77bac461"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f7807a55-568a-4f23-bf73-4d3a77bac461",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7685e287-8791-4e6a-b1cb-e93e9c545603",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}