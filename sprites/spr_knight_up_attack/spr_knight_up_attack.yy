{
    "id": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_up_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 36,
    "bbox_right": 62,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4e9410b-2b95-461b-a67a-32143ff9a486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "a27cfc1a-da43-440c-bcdf-f649226b8369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4e9410b-2b95-461b-a67a-32143ff9a486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d0eec2f-575a-4e0a-857a-a2a1ce2991e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4e9410b-2b95-461b-a67a-32143ff9a486",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "24cd55b2-0c76-4507-b425-fb3c21bdb45f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "68ffcb61-0927-4111-9834-35e613c0f4dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24cd55b2-0c76-4507-b425-fb3c21bdb45f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224060ae-0327-4a21-a800-49a6482da0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24cd55b2-0c76-4507-b425-fb3c21bdb45f",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "94682b33-99ae-4e56-a180-a215d63bae68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "dbb1fa5d-cf65-41a4-bf6a-8d40fc24b3f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94682b33-99ae-4e56-a180-a215d63bae68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74978248-c051-416f-8798-75882e76147e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94682b33-99ae-4e56-a180-a215d63bae68",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "d10738fa-ce81-4efb-aa4d-bdfa51d51f5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "44ef1c43-ff8e-45f8-b15e-246b2ea9aedc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d10738fa-ce81-4efb-aa4d-bdfa51d51f5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4526e90b-cf5b-446a-a477-a07d579fdacc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d10738fa-ce81-4efb-aa4d-bdfa51d51f5d",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "475b9fc7-c9b8-43e8-94b5-b3e012bfead3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "1ee5cc39-8f03-48e6-b592-5acdfb4b6134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475b9fc7-c9b8-43e8-94b5-b3e012bfead3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "003298f9-e133-4bb6-b4fd-ab2e03417d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475b9fc7-c9b8-43e8-94b5-b3e012bfead3",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "342bca51-afcc-477f-93d1-e0e5ac777ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "19e32f96-3960-4fab-a3d7-6a4c0e92358d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "342bca51-afcc-477f-93d1-e0e5ac777ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e68bd338-aa21-4a01-9ef7-95709a8202da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "342bca51-afcc-477f-93d1-e0e5ac777ca3",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "fc9e60e8-f9c1-43da-83de-63842d321187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "3e33cf11-80d1-4b07-9c5e-5cf6d61e013e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc9e60e8-f9c1-43da-83de-63842d321187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5157f13-60a4-4e0a-b6ef-58676ae85756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc9e60e8-f9c1-43da-83de-63842d321187",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "fae0d608-3aa3-498e-8334-4429cbb914bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "4580e69d-7699-4a61-8f18-5fe5046166bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae0d608-3aa3-498e-8334-4429cbb914bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9beb923a-35f3-4d03-a8ad-a67876a61116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae0d608-3aa3-498e-8334-4429cbb914bf",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "78952bab-68a6-4e8c-af56-0b1b47aa897f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "5a46ff4a-e260-4b19-88b7-614e1bdf58c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78952bab-68a6-4e8c-af56-0b1b47aa897f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def0bb2d-cbbf-4b06-8c78-4f88ee63dadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78952bab-68a6-4e8c-af56-0b1b47aa897f",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        },
        {
            "id": "a846e833-b8f7-4322-b8e5-b202a18a22e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "compositeImage": {
                "id": "40f961e8-4b2c-4e1e-926e-f959d0130fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a846e833-b8f7-4322-b8e5-b202a18a22e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c36a62-efc4-42dc-976c-471fcb7a38ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a846e833-b8f7-4322-b8e5-b202a18a22e8",
                    "LayerId": "f42df100-bd29-4aed-93bb-0fd6a421a847"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f42df100-bd29-4aed-93bb-0fd6a421a847",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "384eb6e2-213e-4c66-9d47-8c38a4e0235c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 48,
    "yorig": 66
}