{
    "id": "69be2ce2-60c0-44fe-819c-87c935e0c325",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_right_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 41,
    "bbox_right": 98,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bdcb46c-9dc4-4bd5-94e3-318a6b5a0c10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "6661f826-031c-4cad-b078-a4d877a64078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bdcb46c-9dc4-4bd5-94e3-318a6b5a0c10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8fdb16-c018-4b00-9d76-1be340f0e573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bdcb46c-9dc4-4bd5-94e3-318a6b5a0c10",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "cb5e2abf-0dd9-493e-a544-84f0d39273c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "3f9caf6b-219c-4c3d-a43c-793dbeadc15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb5e2abf-0dd9-493e-a544-84f0d39273c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdc379c9-68dc-45a7-b98e-aa4a930189ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb5e2abf-0dd9-493e-a544-84f0d39273c2",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "416cfb69-d118-430c-bd1a-0bcdf4ea27e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "094589bd-c662-4a73-b1dc-332553e6c2c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416cfb69-d118-430c-bd1a-0bcdf4ea27e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a2ba97-850d-486c-8feb-6afb782413c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416cfb69-d118-430c-bd1a-0bcdf4ea27e2",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "55105648-fcdf-460c-87b0-7ec8ccfdd10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "9f459ebc-5ab1-425b-90f8-c0d5cd27739f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55105648-fcdf-460c-87b0-7ec8ccfdd10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2889eb-31a1-4773-a3bb-dc792fb13912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55105648-fcdf-460c-87b0-7ec8ccfdd10c",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "95b01724-c63f-47f6-90ea-d3aa9edfa05a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "f597d389-6734-41b0-8361-ceeb3c004724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b01724-c63f-47f6-90ea-d3aa9edfa05a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e31a822-f4fc-4ffb-ba57-c3672b9b7141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b01724-c63f-47f6-90ea-d3aa9edfa05a",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "583afcc8-6249-4dbf-a38e-8672614fb519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "5b5eaf31-53be-412b-bdaf-d772ec2c4dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "583afcc8-6249-4dbf-a38e-8672614fb519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb07d97-9a81-456b-b5e3-263d65d4c924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "583afcc8-6249-4dbf-a38e-8672614fb519",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "4bee79ec-1cf7-4dc9-9828-a61fb35efcec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "0e020b9a-d6f4-41b6-aeb0-007bd2bc49be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bee79ec-1cf7-4dc9-9828-a61fb35efcec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f52cd5f5-cbb8-4e50-a787-73fa952cbad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bee79ec-1cf7-4dc9-9828-a61fb35efcec",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        },
        {
            "id": "01c74d9a-710a-43f0-8801-99413e00be6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "compositeImage": {
                "id": "0fa62a2a-2182-4f2e-9794-889332748661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c74d9a-710a-43f0-8801-99413e00be6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea4b5236-2881-4c9c-820e-ddcb7898d27b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c74d9a-710a-43f0-8801-99413e00be6b",
                    "LayerId": "9510ef97-cb6d-40b8-8008-82b4d49792a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9510ef97-cb6d-40b8-8008-82b4d49792a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 131,
    "xorig": 63,
    "yorig": 67
}