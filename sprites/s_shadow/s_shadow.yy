{
    "id": "920d7698-f1be-4295-8e6c-915914d77c69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6d65454-7491-465a-ba7c-25b29a6cc132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "920d7698-f1be-4295-8e6c-915914d77c69",
            "compositeImage": {
                "id": "3b10192d-e224-4cb3-b98d-4283dcd26116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d65454-7491-465a-ba7c-25b29a6cc132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99999156-61db-4ded-8fc7-4932019763c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d65454-7491-465a-ba7c-25b29a6cc132",
                    "LayerId": "3224ad7c-f94a-4944-a044-5cd6cfe2e1e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3224ad7c-f94a-4944-a044-5cd6cfe2e1e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "920d7698-f1be-4295-8e6c-915914d77c69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}