{
    "id": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 85,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22fb5f3a-cf66-435d-be29-9b81c863fb57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "ffd7f31c-4172-4cda-8208-23f633de49ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22fb5f3a-cf66-435d-be29-9b81c863fb57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6435fd3-e714-4fca-929b-33eee4b79084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22fb5f3a-cf66-435d-be29-9b81c863fb57",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "c6ec2cdb-8255-4d01-9821-fab4df5bafeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "95ea8b1d-d387-45d5-8a80-1e879b577851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ec2cdb-8255-4d01-9821-fab4df5bafeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57d73928-e1b3-4739-a355-203cb95d3ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ec2cdb-8255-4d01-9821-fab4df5bafeb",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "5b1ddf45-0d52-4eb0-a4e8-57aee38a1cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4f8a60f6-2ba4-41d9-876e-11c02c14964a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b1ddf45-0d52-4eb0-a4e8-57aee38a1cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d2b225-feed-40b8-84bd-eca42fe7d627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b1ddf45-0d52-4eb0-a4e8-57aee38a1cb0",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "fc76ab8f-05ae-4220-a19e-9ffbba067e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "caa746b9-7b2d-47b8-b10f-c1798976b376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc76ab8f-05ae-4220-a19e-9ffbba067e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f74e18-209a-4ec8-bd20-e418cff94ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc76ab8f-05ae-4220-a19e-9ffbba067e27",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "654680dc-a1ae-48a2-8744-f690130d97fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "a9ca9c57-6536-4b3b-86a5-e493a00055d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "654680dc-a1ae-48a2-8744-f690130d97fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7819093-fd21-4e1e-9923-9b88f1357f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654680dc-a1ae-48a2-8744-f690130d97fd",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "bc4aad29-46e8-4541-89d0-368b8c67e5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "72e584ad-c022-4dd1-984f-c654c4f619bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc4aad29-46e8-4541-89d0-368b8c67e5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a73c56-f01a-472c-a561-b37f4972869f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc4aad29-46e8-4541-89d0-368b8c67e5b0",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4c8e5deb-3a2d-47ca-9167-643e1f8e4d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "cd35876a-96d2-432e-9e10-98c2a4cc6d2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c8e5deb-3a2d-47ca-9167-643e1f8e4d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ebdcce-6bf8-4deb-9969-a82b997db774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c8e5deb-3a2d-47ca-9167-643e1f8e4d91",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "0e1a7c62-9438-42ef-a6c5-fc2e2dc7199e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "9554e798-c3b3-4f59-8b8c-4a3357ebb924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e1a7c62-9438-42ef-a6c5-fc2e2dc7199e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3534c1bd-517c-4169-a30f-3446866897c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e1a7c62-9438-42ef-a6c5-fc2e2dc7199e",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "c72ec27f-0e8c-43ae-8ca5-e98e4d7a5cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "dc473ef1-9f59-4cbc-b3b6-eb7d54f576f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c72ec27f-0e8c-43ae-8ca5-e98e4d7a5cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee11c388-63c2-4cd4-8792-9acc2832bd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c72ec27f-0e8c-43ae-8ca5-e98e4d7a5cde",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "53ba070a-b120-4530-b6e1-be9446015ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "cab18d1b-725c-4885-9d92-bdc8e2f1704e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53ba070a-b120-4530-b6e1-be9446015ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35788b6f-9786-419c-8f5d-7a5be6d9a946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53ba070a-b120-4530-b6e1-be9446015ec9",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "24b7d638-3704-4784-8479-d76afb8e1ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "017682cf-430b-4d1c-a88d-bcea061005cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24b7d638-3704-4784-8479-d76afb8e1ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0137e6b-7af9-4cf7-b49c-2297bf3a426f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24b7d638-3704-4784-8479-d76afb8e1ebd",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "0f9dc0ca-bde4-49e8-a9cc-e353befc8871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "dc17bede-8696-410e-beb6-1f94e3c467d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f9dc0ca-bde4-49e8-a9cc-e353befc8871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2269621-ab76-4b15-86df-90caba0cd008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f9dc0ca-bde4-49e8-a9cc-e353befc8871",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "72142c5b-63b9-4628-8394-cb9ad2cfe404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "b1b1a6c3-4d78-4b2f-aa87-1d5533a9b2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72142c5b-63b9-4628-8394-cb9ad2cfe404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6a542b-1b18-4d01-881a-ae95a580b9e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72142c5b-63b9-4628-8394-cb9ad2cfe404",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "d43bcde4-4749-45be-91a4-baf331c584d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "104a504c-db34-43f0-bc5f-2470b6e02598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43bcde4-4749-45be-91a4-baf331c584d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1300202-3b65-4cc9-82b9-90ee885c4006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43bcde4-4749-45be-91a4-baf331c584d1",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "24957311-c43e-4dee-8f80-c7d17e281875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "07d523b8-ce7d-484f-af40-01baf32ecacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24957311-c43e-4dee-8f80-c7d17e281875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b29c966d-2f27-4fae-b739-37c086cd6dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24957311-c43e-4dee-8f80-c7d17e281875",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ee3003f7-fd0d-4b05-a2d5-805fc2170caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "1371e9cf-5a26-4fbd-9562-0a411de7e820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3003f7-fd0d-4b05-a2d5-805fc2170caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91aa4f88-77e8-42e3-aacb-55f26d8f2ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3003f7-fd0d-4b05-a2d5-805fc2170caa",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "cbb6d9b0-6703-4d05-b810-fdcdb53d16dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "266131bb-4bca-4cbd-b94a-bf94686191ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb6d9b0-6703-4d05-b810-fdcdb53d16dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6a0c8ff-1570-4d34-9256-838e50e89bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb6d9b0-6703-4d05-b810-fdcdb53d16dc",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7993ea6e-b46d-4984-9d81-18620d298651",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "1d70f306-238d-4924-aa59-08ff35359d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7993ea6e-b46d-4984-9d81-18620d298651",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf6862f-6e33-45b1-afa7-f611721002f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7993ea6e-b46d-4984-9d81-18620d298651",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "cfecb262-0c5d-4e1e-9d91-3ad8b7851e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4ccb423f-bc0f-490a-9b0f-cb53b183d414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfecb262-0c5d-4e1e-9d91-3ad8b7851e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac47c872-ef50-48de-871e-2867d7ab4448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfecb262-0c5d-4e1e-9d91-3ad8b7851e72",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ae9caa3b-01ad-4a6d-beaa-535f7ae515b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "9f87ed43-660e-4709-83f1-f4f84f399c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9caa3b-01ad-4a6d-beaa-535f7ae515b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e49a635-1a37-4fcb-a14a-241b36142ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9caa3b-01ad-4a6d-beaa-535f7ae515b6",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "b52e9086-c2de-4c40-b6ea-bde79615193b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "6e9d1e74-9e19-48c7-849e-abbfc2a19bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52e9086-c2de-4c40-b6ea-bde79615193b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28f5c518-bc5b-49af-9e22-e51e59adf6b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52e9086-c2de-4c40-b6ea-bde79615193b",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "de8b22dd-8691-4306-aff7-43a4874b0792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5734db27-2bd3-4654-a4d0-62039e68454f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de8b22dd-8691-4306-aff7-43a4874b0792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23fc78c-e8cb-412e-b7ac-96d454db08ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de8b22dd-8691-4306-aff7-43a4874b0792",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "2a161b2d-442c-4b87-8de9-2e8f965876c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "d6855be3-041e-4fdb-867a-eda53aa00f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a161b2d-442c-4b87-8de9-2e8f965876c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f688a061-7fd5-4746-9641-a49b7f4bb5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a161b2d-442c-4b87-8de9-2e8f965876c8",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "df3b8ba8-5420-4130-9fac-785d873f95cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "da5bb57e-1ce9-46ec-a544-5a41da6d3341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df3b8ba8-5420-4130-9fac-785d873f95cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee186907-92f3-4bfc-b550-1cfaeb3177b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df3b8ba8-5420-4130-9fac-785d873f95cc",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "de9428e1-a743-4e46-b5bd-d0c780fa2c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "10c21e98-d0b3-4efd-ab76-e0ea33e6ce17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de9428e1-a743-4e46-b5bd-d0c780fa2c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55cc9f4-2dde-4204-9352-5f1789bff008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de9428e1-a743-4e46-b5bd-d0c780fa2c94",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "23b56cac-ebc6-4608-acb9-71e9118c6ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "3b681f12-d8ed-4bf7-98ec-b37ef642ca6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b56cac-ebc6-4608-acb9-71e9118c6ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5cb331-1c20-48dd-beef-d0df59d0e237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b56cac-ebc6-4608-acb9-71e9118c6ed4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "f2e1cf73-d7e6-4a98-a4f2-25635df4a1ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "6d4c2c05-21a5-420f-915c-333c49b9d52f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e1cf73-d7e6-4a98-a4f2-25635df4a1ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949b164a-6584-4475-870d-150e822fb098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e1cf73-d7e6-4a98-a4f2-25635df4a1ff",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "c1d6143a-cceb-494d-bc22-ec7deddedd34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "1b49ede3-a2bb-4222-b3d7-4694278f6892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d6143a-cceb-494d-bc22-ec7deddedd34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408067d3-3f96-441e-a8de-621066aa72e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d6143a-cceb-494d-bc22-ec7deddedd34",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "eeea0c78-f575-497f-b468-e5dc7d0f3b20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "dcd35093-d400-481e-9f92-c5116322a78f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeea0c78-f575-497f-b468-e5dc7d0f3b20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f342872a-e1e4-427f-bbe6-35e4b259a186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeea0c78-f575-497f-b468-e5dc7d0f3b20",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "325d1c99-cfd2-44af-a99c-c8615414e0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "8da00b66-5b55-4ecf-bb08-ab0c52c89dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "325d1c99-cfd2-44af-a99c-c8615414e0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eb61733-7334-441c-801a-19e91bb93089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "325d1c99-cfd2-44af-a99c-c8615414e0e6",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "b2a8c3ab-db89-4c40-bd00-f59bd96989bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "310e1bf2-69fc-4541-9c6a-8e51b6b1bbf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a8c3ab-db89-4c40-bd00-f59bd96989bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98464ffa-943b-4503-901f-ac928c199ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a8c3ab-db89-4c40-bd00-f59bd96989bd",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "45d415e7-daac-4c9f-b957-b1e53363d895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "557c3ec5-c22c-4264-90f7-804c425f9f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45d415e7-daac-4c9f-b957-b1e53363d895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd735df4-661b-4186-a867-26636d236a6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45d415e7-daac-4c9f-b957-b1e53363d895",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "29e5988c-6e9d-43e5-a5a2-45d91c414747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "3c156559-f73b-4ab7-bc0a-4d6009b49cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e5988c-6e9d-43e5-a5a2-45d91c414747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0582e1a-cac2-45bf-a6f3-0adc6031971e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e5988c-6e9d-43e5-a5a2-45d91c414747",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "59e9da51-a71f-4178-91cd-b374313c8a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "9e801370-b543-46e8-b649-9cd3da9e455e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59e9da51-a71f-4178-91cd-b374313c8a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49de0e45-abd6-40ab-930d-5c8586c4d0ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59e9da51-a71f-4178-91cd-b374313c8a12",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "5c04739d-906c-4cc2-87af-307baf16a236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "56130bf5-80b6-4ea3-beec-4120119a8794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c04739d-906c-4cc2-87af-307baf16a236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe441a7-d450-4a63-a6a8-39ea5733950d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c04739d-906c-4cc2-87af-307baf16a236",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "f58a0cdf-40ec-4426-b83d-840885633fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4f8bcf83-4190-433e-b44d-9b7743282f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f58a0cdf-40ec-4426-b83d-840885633fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d01b1c7-eac8-4df9-a65b-63bb1d499928",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f58a0cdf-40ec-4426-b83d-840885633fd6",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4629d56c-301c-4943-92f0-0dce028279ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "2baa0dac-5e8f-4773-9142-d92dc7cd557c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4629d56c-301c-4943-92f0-0dce028279ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e121055-51f7-41df-a6a4-82eadf277ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4629d56c-301c-4943-92f0-0dce028279ba",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "167d6997-627c-47d3-80b7-4ad3d481dca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5cc5f783-bf07-4afd-a097-cfa5f621624f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "167d6997-627c-47d3-80b7-4ad3d481dca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5692f50-11df-47c8-b62d-babcf9387096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "167d6997-627c-47d3-80b7-4ad3d481dca7",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "377a8657-c5bb-4be3-b451-bfaa56bc95b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "717e2147-b213-42de-b458-02e191cc4c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377a8657-c5bb-4be3-b451-bfaa56bc95b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "839c0b45-2cfb-4697-b45e-2ad4d5645acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377a8657-c5bb-4be3-b451-bfaa56bc95b6",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "8e29d6a1-a052-43b9-8ee3-00c195753b0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "e827089d-7cf3-4e6e-bf85-c75dab3a4452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e29d6a1-a052-43b9-8ee3-00c195753b0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe99b143-0c12-4ccc-b2da-76a7d33d6bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e29d6a1-a052-43b9-8ee3-00c195753b0c",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4dec7a79-6164-4b57-a91b-33fef7e6ef90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "b99c5829-bdf2-4a9a-a3fb-00e659104a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dec7a79-6164-4b57-a91b-33fef7e6ef90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee77843-7ca8-41ec-b7c1-62eb56cd0ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dec7a79-6164-4b57-a91b-33fef7e6ef90",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "258ff6ce-5e29-4beb-8d91-e5dab0210d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "463eb418-e41b-44da-b29b-f508b9b22c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "258ff6ce-5e29-4beb-8d91-e5dab0210d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20d28e30-d962-44fd-b5fd-e64bedb1a103",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "258ff6ce-5e29-4beb-8d91-e5dab0210d1a",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4c600b27-d777-4e52-b7d9-4e374382aea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "366e2313-eeda-4f91-bf66-f2553ee2fea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c600b27-d777-4e52-b7d9-4e374382aea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d77071ce-cc03-4ae1-a0e1-cc4306a19de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c600b27-d777-4e52-b7d9-4e374382aea4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "b0cfd0ba-907b-4fc4-a1d8-e6902231c100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4be68b0f-a0c9-4512-9970-f2647f53fd8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cfd0ba-907b-4fc4-a1d8-e6902231c100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36365799-17c8-462f-9536-eb2cfa6aa9d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cfd0ba-907b-4fc4-a1d8-e6902231c100",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "9aae9268-c624-46a3-8e21-feabf5c01c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "fc4dba5d-6f3f-44a1-a1e8-9d116f985d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aae9268-c624-46a3-8e21-feabf5c01c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5eb50e2-aefd-40ae-93d8-82a23dd04859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aae9268-c624-46a3-8e21-feabf5c01c51",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "39eb5631-0724-426e-8ecb-076f1bb7d1a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "48ea77e5-786d-4426-8383-ae3e846eb015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39eb5631-0724-426e-8ecb-076f1bb7d1a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08483d5d-79b8-43f2-968c-73625754560b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39eb5631-0724-426e-8ecb-076f1bb7d1a5",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "06023d12-f839-4681-b6ae-1be0e0818178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "ce725c14-40a2-4c9e-8cf1-faafd1a687d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06023d12-f839-4681-b6ae-1be0e0818178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70456feb-e118-4204-b655-f3f536faddae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06023d12-f839-4681-b6ae-1be0e0818178",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "9dfe6c1c-19f9-4514-a091-49599aed294d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5e1aa6db-cbe2-4759-93c5-2b50c5d3948b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dfe6c1c-19f9-4514-a091-49599aed294d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b6bcc7-92ae-42ad-8350-bd898744b076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dfe6c1c-19f9-4514-a091-49599aed294d",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "97af0029-95b4-4b91-93fa-fe6f5e0a2b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "9472b65d-7fb9-4197-8362-323682f55693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97af0029-95b4-4b91-93fa-fe6f5e0a2b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fa1c2e-4fe9-41fd-906c-b2b8d74855b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97af0029-95b4-4b91-93fa-fe6f5e0a2b22",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "5c4d2738-7917-4b3e-a6c2-836ab9e5008f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "262b63f1-6e6d-43e1-8f3e-467badc4f4ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c4d2738-7917-4b3e-a6c2-836ab9e5008f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f8fd43-04d0-4c16-84a8-cbef86e4c039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c4d2738-7917-4b3e-a6c2-836ab9e5008f",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "beaf3456-d1be-4a80-8a74-5b35fd32e264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "803b9996-aab6-4801-8c53-c0beed02079f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beaf3456-d1be-4a80-8a74-5b35fd32e264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44fbabb4-d1df-484c-bf3b-459371fa9bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beaf3456-d1be-4a80-8a74-5b35fd32e264",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "67c306c2-3f73-4689-b14d-1aa3ce57a845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "6707e1e2-d5b7-40e1-ab6d-78f9caff91af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67c306c2-3f73-4689-b14d-1aa3ce57a845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54da8f8-b40e-45e9-8d17-49fd16a1da95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67c306c2-3f73-4689-b14d-1aa3ce57a845",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4fd05bdd-c8e5-4172-9edc-52b688622c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "7adc06c9-4b60-4025-a0c4-55ee5564903d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd05bdd-c8e5-4172-9edc-52b688622c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9621116-fb8d-4589-a2ff-a6054e0aa825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd05bdd-c8e5-4172-9edc-52b688622c17",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ca88c480-ad61-4238-b1a4-9f14dc8ea3ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "689d8dd5-5292-4184-ab56-3e0a9262b842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca88c480-ad61-4238-b1a4-9f14dc8ea3ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7cd3b5e-266c-46f8-99fb-0193669b0e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca88c480-ad61-4238-b1a4-9f14dc8ea3ee",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "f6642fc9-e5e1-4a86-8a1b-8c78bde4f1df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "2dbff480-7bb6-470f-a4af-a8c77eaeb193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6642fc9-e5e1-4a86-8a1b-8c78bde4f1df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ebd5a2-0651-4b9f-a94c-238ef8df6a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6642fc9-e5e1-4a86-8a1b-8c78bde4f1df",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7bd48f4f-7ffb-417f-a1a6-3306e33e37ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "329290a6-0a6e-4e9c-86cd-d8325737eeaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bd48f4f-7ffb-417f-a1a6-3306e33e37ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45aa860-4082-441c-a157-eeab9d2b6b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bd48f4f-7ffb-417f-a1a6-3306e33e37ca",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ec2ba0c9-9640-47df-aa9d-40ada21f88ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "c23b7c1b-cfb7-4d3f-b6ff-3c215b0061ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec2ba0c9-9640-47df-aa9d-40ada21f88ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6219528-d377-4348-b2e9-eda6356ec3cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec2ba0c9-9640-47df-aa9d-40ada21f88ba",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "79938ad3-1ee3-45d7-a701-3d4aa39283f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5ba24c38-bc31-4f91-8c0a-318efa1a168f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79938ad3-1ee3-45d7-a701-3d4aa39283f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca491b4-6fec-423b-9dab-4f5b740735e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79938ad3-1ee3-45d7-a701-3d4aa39283f9",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ee6a0a46-a878-45e3-9da1-83b3d04fbe45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "fa76dc1e-bdb5-46c0-81ef-d000ae3c8d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee6a0a46-a878-45e3-9da1-83b3d04fbe45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "303beb73-bb0e-4ee6-8c20-25a8c84e459b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee6a0a46-a878-45e3-9da1-83b3d04fbe45",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "5ff428f0-83be-496f-9baa-6e91686b468b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "d03a1f29-c6cf-44e1-ba5b-d5cfde657d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff428f0-83be-496f-9baa-6e91686b468b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f64d2af-cb3a-439d-9628-6df807147cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff428f0-83be-496f-9baa-6e91686b468b",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "e71a2432-0739-4683-8061-21aa2f4fb3c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "484262b1-9784-4aa5-a477-a1f473fbdefb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71a2432-0739-4683-8061-21aa2f4fb3c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc04767a-c6c5-46e7-9bf8-3a4b4d29d9bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71a2432-0739-4683-8061-21aa2f4fb3c4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "2788007c-a55e-4173-9ff2-81305f442209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "a20d65f7-faa4-4aad-ba52-b1336babb589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2788007c-a55e-4173-9ff2-81305f442209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efac2669-0cf4-4737-bc71-d6be0135b481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2788007c-a55e-4173-9ff2-81305f442209",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "584ad72d-d5fa-4af7-97ab-544bbec4a914",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4eb18ca6-6862-4021-90cc-ea014a3e9261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "584ad72d-d5fa-4af7-97ab-544bbec4a914",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71dbd71a-46ac-42de-9645-e9bdce4a5cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "584ad72d-d5fa-4af7-97ab-544bbec4a914",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "c22c0117-7de5-4fcd-a34c-1f0ed3c9eaf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "b8c96879-6664-4b64-9aef-be8e65e09b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c22c0117-7de5-4fcd-a34c-1f0ed3c9eaf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12bbf579-0a3a-43b8-a96e-e4cee2cd0364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c22c0117-7de5-4fcd-a34c-1f0ed3c9eaf5",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "83684363-3c5b-48cc-ba29-9e27e2e63bdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "1d8f81d5-8085-4d7f-a94f-4603c9246dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83684363-3c5b-48cc-ba29-9e27e2e63bdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc905493-f75f-4233-8258-3543ea6449b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83684363-3c5b-48cc-ba29-9e27e2e63bdc",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7b7bdb8a-4852-4809-aaf8-9f576586fc13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5f3bb89b-0bf9-458f-8603-a63de8905ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7bdb8a-4852-4809-aaf8-9f576586fc13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eefb091-7725-43dc-86e7-deb0d5f5269d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7bdb8a-4852-4809-aaf8-9f576586fc13",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "6f99316a-4cd4-496a-94e9-efeeb2ef3e98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "f8af2748-88e5-4be7-aa27-6939a9310e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f99316a-4cd4-496a-94e9-efeeb2ef3e98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c64100-5a7b-4b03-8b5f-a3f48594999f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f99316a-4cd4-496a-94e9-efeeb2ef3e98",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "6bcc116d-9a5e-4226-8741-eece2a34ea2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "ff10f5cd-7b36-4278-b02e-4b950f3558c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bcc116d-9a5e-4226-8741-eece2a34ea2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb43a8c4-e57e-4fed-a6be-019a14468a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bcc116d-9a5e-4226-8741-eece2a34ea2f",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ddaf8746-8427-410c-97c5-866bf860468c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "036b899e-d6aa-4901-85a0-46a8191d0e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddaf8746-8427-410c-97c5-866bf860468c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6abd33c9-0155-4ad3-8782-74e18751e97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddaf8746-8427-410c-97c5-866bf860468c",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "cf9f519a-d154-4988-9bb3-22988f616b65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "1aeaea24-e3ae-45aa-8705-3a5f606920cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9f519a-d154-4988-9bb3-22988f616b65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e6a8c2-f438-426a-8e87-8211e9df7949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9f519a-d154-4988-9bb3-22988f616b65",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "71748426-a64c-4a9a-b20a-7f5ef4a2648b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "eb7c30c0-9bca-4f0d-b041-80b442ce272d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71748426-a64c-4a9a-b20a-7f5ef4a2648b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7594d618-67ea-42a4-bbab-25521388f56f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71748426-a64c-4a9a-b20a-7f5ef4a2648b",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "b90e67d1-2e29-4874-bb1d-d09f52af8268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "effa408b-42bb-41b8-9339-5c28ed477f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90e67d1-2e29-4874-bb1d-d09f52af8268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24780380-7abb-4683-accd-cc7de73fd2c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90e67d1-2e29-4874-bb1d-d09f52af8268",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "76fd28a0-11ca-4c7f-b1ed-1f995710d1b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "c93f7c1b-af20-4bee-95b2-e13505f51474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fd28a0-11ca-4c7f-b1ed-1f995710d1b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe0860e-bbd0-4a0d-82df-74967e4cc172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fd28a0-11ca-4c7f-b1ed-1f995710d1b3",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "56adf978-8402-4e1d-8fb9-9a6b2b9b3119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "bbbc9061-db7a-48ab-96ff-b2ee1a0e3f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56adf978-8402-4e1d-8fb9-9a6b2b9b3119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7bf01d3-6642-47f4-9471-aefdb26639d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56adf978-8402-4e1d-8fb9-9a6b2b9b3119",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7966a43f-1611-4caa-8eb9-37b469e3b3d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4a33d4e8-5a55-4d06-8fb7-1a1d2f90d048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7966a43f-1611-4caa-8eb9-37b469e3b3d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705446fd-7d23-41e4-9819-436f93783c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7966a43f-1611-4caa-8eb9-37b469e3b3d4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "49225a1e-7e4e-4097-adac-1eea99f3a9d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "b67c8b76-afca-48c4-97dd-f1c202f9aca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49225a1e-7e4e-4097-adac-1eea99f3a9d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "453ad289-12fc-49b0-b684-44e679aaf715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49225a1e-7e4e-4097-adac-1eea99f3a9d3",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "2e46b8d8-b2ab-408a-9a27-dcab500ef9b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "891eda4d-252a-4067-a4fc-e9170e22cfcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e46b8d8-b2ab-408a-9a27-dcab500ef9b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87e939f6-1be8-451f-b6d2-14e0cd1bc732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e46b8d8-b2ab-408a-9a27-dcab500ef9b4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "f4bc6fbc-ef98-4325-b5e8-50a5a69e53ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "c8459fdb-475d-4bb9-8af4-4545a84b5bcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4bc6fbc-ef98-4325-b5e8-50a5a69e53ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad8c79d-0f80-4659-b58c-859f7fea8dc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4bc6fbc-ef98-4325-b5e8-50a5a69e53ec",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "3e691a73-6399-46b8-a6b9-b17d20435274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "ab034858-fe6b-49ef-85aa-ca45010a5d7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e691a73-6399-46b8-a6b9-b17d20435274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ddb71d-6efd-4ce8-b27b-f01637e4c528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e691a73-6399-46b8-a6b9-b17d20435274",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4fb5cf4f-edd4-40ae-ae37-ca073440047a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "23f1c035-beca-4d0c-9933-4385be4d31b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb5cf4f-edd4-40ae-ae37-ca073440047a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c880a8-2d86-44f2-8fa1-f211c20f11cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb5cf4f-edd4-40ae-ae37-ca073440047a",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "fc2bad70-6f14-47e5-9a88-af747c2b0f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "29f7dddc-7321-49e2-8061-55278827238c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2bad70-6f14-47e5-9a88-af747c2b0f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5ff441b-49cc-427f-91dd-e3d7f7e34a17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2bad70-6f14-47e5-9a88-af747c2b0f28",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "79bab84a-d69a-4d66-8ffb-46cfdff0b8c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "a680aa2d-e038-4929-8c75-9733ede42f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79bab84a-d69a-4d66-8ffb-46cfdff0b8c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82a846e-5512-4516-900b-318bb1c8b8d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79bab84a-d69a-4d66-8ffb-46cfdff0b8c8",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "fa777ec4-b019-4305-bb4f-b52e7736e9fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "8ced44e0-3994-4317-881e-99c1a9dafb1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa777ec4-b019-4305-bb4f-b52e7736e9fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d50329-732e-45ae-be0a-d609af45e00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa777ec4-b019-4305-bb4f-b52e7736e9fe",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7c0f8d7b-93ea-4e2f-8c46-cf9f2f9d7e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "7d608950-afc9-4abb-a34d-2dabeab78f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0f8d7b-93ea-4e2f-8c46-cf9f2f9d7e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae48e736-8f7b-4c76-bab7-5c71b617469a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0f8d7b-93ea-4e2f-8c46-cf9f2f9d7e9e",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "b506102e-9502-45e1-9e76-944a076701dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5f581460-12a2-4f39-a235-f1c76ade6430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b506102e-9502-45e1-9e76-944a076701dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494bd91f-00fc-4d21-8170-41c670b56bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b506102e-9502-45e1-9e76-944a076701dd",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "62754f85-fcbc-4659-9d6e-80eb2f10890f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "7f874e12-8135-453b-9f38-ae736a65dd39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62754f85-fcbc-4659-9d6e-80eb2f10890f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4da9aab-f766-42b5-8ebc-d4eb6d8bd8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62754f85-fcbc-4659-9d6e-80eb2f10890f",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "31ed5b62-65a4-436c-a6db-dcfd2e6a5f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "7100d1c0-d43c-4742-b3d9-ec480e4bc975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ed5b62-65a4-436c-a6db-dcfd2e6a5f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7f7b14-042c-469b-bc5a-628fde1e9756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ed5b62-65a4-436c-a6db-dcfd2e6a5f95",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "60a8b39d-311b-4420-99a7-3d530feb7437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "77ea5bc4-fbbb-45f6-a5a1-023342c66e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a8b39d-311b-4420-99a7-3d530feb7437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2381834a-0d8e-41aa-ae9a-2264c84c1d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a8b39d-311b-4420-99a7-3d530feb7437",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "21240554-1b8e-4172-9217-ad4e09a40569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "0f716254-c820-4fb4-a9e7-40a0496d3656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21240554-1b8e-4172-9217-ad4e09a40569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa146e4-b0d7-42a3-94e8-a1b63405118e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21240554-1b8e-4172-9217-ad4e09a40569",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "3ddb9c57-e1a6-4c60-ac6f-7b5df7eeb444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "03302030-ecc2-475e-8c96-04ecf6bb46f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ddb9c57-e1a6-4c60-ac6f-7b5df7eeb444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceebb1ba-04f9-4bd2-b6db-815adf42eccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ddb9c57-e1a6-4c60-ac6f-7b5df7eeb444",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "21f58a23-2923-4a68-a297-2ad04ab73f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4f6c498a-d47e-4694-a3ec-97331270efb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f58a23-2923-4a68-a297-2ad04ab73f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff3146c-8d09-4361-8493-cc2d941d21a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f58a23-2923-4a68-a297-2ad04ab73f4d",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "f7d6546d-20b8-4507-bc8b-8f2aaa0a38b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "f665b272-aacd-4b1c-99ef-584a11d0b3e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7d6546d-20b8-4507-bc8b-8f2aaa0a38b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f10192-07cb-47a9-a29c-e734831e7dc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d6546d-20b8-4507-bc8b-8f2aaa0a38b3",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7b9d1640-6adc-44cd-a753-f7d6ef692fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "5855f1de-4520-4f6b-acb8-9e49eb2610c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b9d1640-6adc-44cd-a753-f7d6ef692fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "993692ae-7079-4225-ab7a-54e31bcd8f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b9d1640-6adc-44cd-a753-f7d6ef692fce",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "d12e0f00-84ae-4bb6-b0cd-5c05edacab0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "21a2f679-61cd-47e6-b8c4-a5ca4f839a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d12e0f00-84ae-4bb6-b0cd-5c05edacab0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9596617f-a347-40d9-b93f-a0a11f8e4e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12e0f00-84ae-4bb6-b0cd-5c05edacab0c",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "500ea810-68df-48f9-9327-3e2df2c250bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "957700f8-9b87-43bd-a07f-656444b79745",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "500ea810-68df-48f9-9327-3e2df2c250bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60f9eeef-4c09-49c9-8d4c-098c8a803dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "500ea810-68df-48f9-9327-3e2df2c250bb",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "685f194a-fb6b-4f70-898b-e246c4259cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "a60b17ab-844d-47d7-84e6-ea7805e0ea16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685f194a-fb6b-4f70-898b-e246c4259cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138eb6d4-0bfc-4cdf-8138-5800b5c6e90e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685f194a-fb6b-4f70-898b-e246c4259cc3",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "85f662e0-d473-4216-bc72-418f5251b979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "526e051f-ece9-43c4-b37b-effbdba6ef66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f662e0-d473-4216-bc72-418f5251b979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9e3baeb-49db-4f9e-acff-f792c78e9109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f662e0-d473-4216-bc72-418f5251b979",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "19e0e4da-af5f-4561-bb06-2bdb6bb6c6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "2a9d90d0-0f13-4c6f-8c29-f7c5f0080c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19e0e4da-af5f-4561-bb06-2bdb6bb6c6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffedd4b0-1107-4ac5-80e5-0ad0f2b4a98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19e0e4da-af5f-4561-bb06-2bdb6bb6c6f7",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "20cdbb7b-8d19-420c-96f5-a00020c2f65f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "8d135b3c-1fb5-45f6-84c3-9b3e2a2f0b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20cdbb7b-8d19-420c-96f5-a00020c2f65f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfd5efc-a272-465b-a833-d92021eb740a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20cdbb7b-8d19-420c-96f5-a00020c2f65f",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "82e83b4d-e84b-4e10-9028-e38a47b6bec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "d0841cd2-dace-42d5-af1a-84f5bd18c683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e83b4d-e84b-4e10-9028-e38a47b6bec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d877759b-f024-4107-b714-0d7273137692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e83b4d-e84b-4e10-9028-e38a47b6bec5",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "d520b6af-4a08-4f42-b878-fa40fde3c3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "ce06022d-6f67-4fe0-95b4-88e552185516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d520b6af-4a08-4f42-b878-fa40fde3c3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3459b7a3-9a36-4ebe-bf11-be949ab049b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d520b6af-4a08-4f42-b878-fa40fde3c3cf",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7fa7bdad-e67f-4bf1-9c52-18929b42faee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "54ed69f1-318d-40c0-b7b3-21356c9bd4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa7bdad-e67f-4bf1-9c52-18929b42faee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf1afbc-c2fc-4750-8184-c3fe78e88d01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa7bdad-e67f-4bf1-9c52-18929b42faee",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "9198d44c-bf0f-4783-90ca-fe80545c3725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "a3aae1d0-45a2-4012-b6a5-6d0b2e94a23c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9198d44c-bf0f-4783-90ca-fe80545c3725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45905233-939f-4d6b-8a9c-af934adeb6f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9198d44c-bf0f-4783-90ca-fe80545c3725",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "7d23b815-c771-41f8-9be5-fa7abfea7b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "d74f7715-bf97-4ba8-a589-bf3084a74e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d23b815-c771-41f8-9be5-fa7abfea7b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b93098c-c5d2-47d1-9c9e-23c5cbb821df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d23b815-c771-41f8-9be5-fa7abfea7b12",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "6e37982c-54ed-4623-8eb6-9f5c7122e6c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "e9b4320a-ca60-48f0-8a11-56806ba3f450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e37982c-54ed-4623-8eb6-9f5c7122e6c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969007bd-e677-47eb-a5e6-e239550b4d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e37982c-54ed-4623-8eb6-9f5c7122e6c8",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "9475b591-70ad-4a92-9d44-065a847cb7a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4d1865e4-ff02-4c6c-b124-f84fa8daf400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9475b591-70ad-4a92-9d44-065a847cb7a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "588cbda9-6be7-4413-91fc-7b37e83e1a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9475b591-70ad-4a92-9d44-065a847cb7a2",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "09d7588e-0d56-4d42-861b-ef3af8218ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "3b4dfbf9-8b19-4594-92fb-5e2a7df5cd49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09d7588e-0d56-4d42-861b-ef3af8218ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ee479b-91fe-464c-a25b-e2ba93515543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09d7588e-0d56-4d42-861b-ef3af8218ef4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "f6a5fd44-61a2-425b-9348-c244187a1092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4e8d8313-a53a-496b-ba9e-15fcc449a641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a5fd44-61a2-425b-9348-c244187a1092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62df4800-0aab-4e7c-a1c8-a63a5af17a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a5fd44-61a2-425b-9348-c244187a1092",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "b57abd51-174d-470c-bf09-cb40b28f4ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "19063a54-06ed-46ea-abb4-3536e0ab06f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b57abd51-174d-470c-bf09-cb40b28f4ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa62cab-f58a-4e28-aa44-07c14f493c40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b57abd51-174d-470c-bf09-cb40b28f4ae6",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "0b1b0c06-5ace-44e3-b49b-7819677190b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "7889917f-32a3-4661-830d-2910b2f0d6bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b1b0c06-5ace-44e3-b49b-7819677190b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98becbb7-8c03-470f-8ccb-209c6580aacd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b1b0c06-5ace-44e3-b49b-7819677190b9",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "e0eb1e58-01b8-4038-a426-07ca344fb29d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "70c7bcde-a7e9-44b0-b2c2-5d0e7626a24e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0eb1e58-01b8-4038-a426-07ca344fb29d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6933696-db6a-4045-b92e-ac156b6d2252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0eb1e58-01b8-4038-a426-07ca344fb29d",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "1e57169c-86ef-45bc-b8a1-0f7dd500ec16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "1536e3fc-e2dd-4993-9266-5f4bc9ef5b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e57169c-86ef-45bc-b8a1-0f7dd500ec16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f02536b-f039-4f27-8c23-2f404246534e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e57169c-86ef-45bc-b8a1-0f7dd500ec16",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "eb86595e-0025-46ba-9402-d6778d41efae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "094ce40c-1d62-4b08-b721-afddf28c4e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb86595e-0025-46ba-9402-d6778d41efae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b33c56-5f9f-46a3-9ab4-113d6d8ac132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb86595e-0025-46ba-9402-d6778d41efae",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "165fe596-3ddc-4fb3-9fdf-2b33ac73b717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "69b9f152-6ccb-4915-b201-377b7bdb6e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165fe596-3ddc-4fb3-9fdf-2b33ac73b717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23e45428-0607-472e-b3d9-da465e90b5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165fe596-3ddc-4fb3-9fdf-2b33ac73b717",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "82c14928-2041-4d1a-bfbc-5ba90ad5cf64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "4fa940ce-df32-431a-a5a2-e51b577704dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c14928-2041-4d1a-bfbc-5ba90ad5cf64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ab347b-727a-4921-b82d-a8a9cc1a09f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c14928-2041-4d1a-bfbc-5ba90ad5cf64",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "cdd04b35-75e7-4507-afd2-99b4b79d53d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "8c317c68-8f56-4e93-8b6c-df7c85fbdb0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd04b35-75e7-4507-afd2-99b4b79d53d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6045e7c5-8d3b-4ae9-875b-ec503ce48a7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd04b35-75e7-4507-afd2-99b4b79d53d4",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "4d885a21-faef-4c65-b0c8-6b5d0d3f4497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "751ecec0-c5ee-4f30-9766-63c53720a893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d885a21-faef-4c65-b0c8-6b5d0d3f4497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4babe5f5-3d12-4af7-9642-aa93be7a416c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d885a21-faef-4c65-b0c8-6b5d0d3f4497",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ae693410-0119-42b7-8977-a218109725c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "26895565-4228-4939-8814-ffa372edd4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae693410-0119-42b7-8977-a218109725c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1156197-af1c-409e-b4ce-861fbd85f280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae693410-0119-42b7-8977-a218109725c1",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "ed259e78-2d04-4e7e-a099-2a93ec8c1f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "56922eca-3311-487f-9c3c-c78a0fcacae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed259e78-2d04-4e7e-a099-2a93ec8c1f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27ce5e20-33e5-4050-bd5a-eef459c59705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed259e78-2d04-4e7e-a099-2a93ec8c1f8b",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "a35b983a-5ab4-407d-a60b-6522637a5445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "9b0370ab-14ad-4d33-9b31-bfc5833e6e11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35b983a-5ab4-407d-a60b-6522637a5445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac2884ef-6739-42dc-b106-9a19561e60c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35b983a-5ab4-407d-a60b-6522637a5445",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        },
        {
            "id": "c6075c2d-f831-43ed-8f46-3a2363ae72e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "compositeImage": {
                "id": "c506b6a4-98bc-43b8-ade6-d59fd4f871bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6075c2d-f831-43ed-8f46-3a2363ae72e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c61adc3d-ceb3-4f05-94bc-f102c261ca94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6075c2d-f831-43ed-8f46-3a2363ae72e2",
                    "LayerId": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "17d5c41b-a583-4ee5-9514-0882d9d8dfb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53c5e2fe-2103-4e39-90d6-3ef8ce887aca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 56,
    "yorig": 67
}