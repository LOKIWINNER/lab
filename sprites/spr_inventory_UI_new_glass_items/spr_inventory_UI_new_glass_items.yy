{
    "id": "bbb0339a-693c-4433-bf1d-5687fdfeba5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new_glass_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc291b7a-93e7-429a-8651-c31c555610ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbb0339a-693c-4433-bf1d-5687fdfeba5c",
            "compositeImage": {
                "id": "0608d1ea-8d3f-4f2e-b834-3d47519b6ae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc291b7a-93e7-429a-8651-c31c555610ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ef10d6-0900-4270-a7ad-09bcb226e11f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc291b7a-93e7-429a-8651-c31c555610ac",
                    "LayerId": "40474a81-a810-4645-a690-b9ea1812d14c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "40474a81-a810-4645-a690-b9ea1812d14c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbb0339a-693c-4433-bf1d-5687fdfeba5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}