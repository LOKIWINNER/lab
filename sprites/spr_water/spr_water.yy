{
    "id": "3e7bb6f7-6eee-4dfd-be78-d802fd05c38b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c622b01-87b0-4a67-acf1-62fa7f19a080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e7bb6f7-6eee-4dfd-be78-d802fd05c38b",
            "compositeImage": {
                "id": "fad5c7bb-1278-4ce0-a330-f7ccc1e65ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c622b01-87b0-4a67-acf1-62fa7f19a080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "853335e8-ac88-4b84-8402-ad3f80009dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c622b01-87b0-4a67-acf1-62fa7f19a080",
                    "LayerId": "add8c6f2-79db-4dd9-b6cb-28b2e69f73bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "add8c6f2-79db-4dd9-b6cb-28b2e69f73bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e7bb6f7-6eee-4dfd-be78-d802fd05c38b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}