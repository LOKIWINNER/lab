{
    "id": "8f3c2d8f-af21-4c62-b89b-28f082ce4005",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_other_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1f77202-3bbc-475a-a559-69af924f776b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f3c2d8f-af21-4c62-b89b-28f082ce4005",
            "compositeImage": {
                "id": "c66d9e2d-383a-4ba0-8baf-b0e18c98973e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1f77202-3bbc-475a-a559-69af924f776b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a04f16b-a715-4781-a192-186e1ab23a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1f77202-3bbc-475a-a559-69af924f776b",
                    "LayerId": "ef5a2718-9b66-42cd-9f07-4f5defa29f68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ef5a2718-9b66-42cd-9f07-4f5defa29f68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f3c2d8f-af21-4c62-b89b-28f082ce4005",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}