{
    "id": "a20ba7f0-7fdf-42af-aacd-ad3cb023be9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flowers_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79617c38-7880-4afd-8d9d-24276ffc6f00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a20ba7f0-7fdf-42af-aacd-ad3cb023be9c",
            "compositeImage": {
                "id": "c17277ac-35e2-4b18-88c6-7ec10b224434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79617c38-7880-4afd-8d9d-24276ffc6f00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94eb5742-e50d-459e-a11d-16df0ade9457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79617c38-7880-4afd-8d9d-24276ffc6f00",
                    "LayerId": "b65e45ae-3e31-4c78-878d-1c765567f763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b65e45ae-3e31-4c78-878d-1c765567f763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a20ba7f0-7fdf-42af-aacd-ad3cb023be9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -245,
    "yorig": -119
}