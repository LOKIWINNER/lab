{
    "id": "9f637081-c3b5-4c83-9cc5-f27fd0cf1630",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new_elem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ac2d390-3359-482b-aee1-74d525055280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f637081-c3b5-4c83-9cc5-f27fd0cf1630",
            "compositeImage": {
                "id": "c5c6ac42-0df6-495d-bb6a-335a22242328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac2d390-3359-482b-aee1-74d525055280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c4a27c-5168-4270-9115-6c8cebfd6d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac2d390-3359-482b-aee1-74d525055280",
                    "LayerId": "0f0c489a-00af-43c3-8bde-2eb910ad11ff"
                }
            ]
        },
        {
            "id": "5fde625e-0f18-4d94-9532-44f37f6eba87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f637081-c3b5-4c83-9cc5-f27fd0cf1630",
            "compositeImage": {
                "id": "a9e72b13-d26c-480a-b652-b0201e6bbf44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fde625e-0f18-4d94-9532-44f37f6eba87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c103f1-78c8-4f56-8e8c-e10492fb7ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fde625e-0f18-4d94-9532-44f37f6eba87",
                    "LayerId": "0f0c489a-00af-43c3-8bde-2eb910ad11ff"
                }
            ]
        },
        {
            "id": "f1e7a0b9-1bf3-4bd9-9c09-f9a81cf7676d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f637081-c3b5-4c83-9cc5-f27fd0cf1630",
            "compositeImage": {
                "id": "f837aea2-95ed-4d77-8286-3639f50881ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1e7a0b9-1bf3-4bd9-9c09-f9a81cf7676d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a696c08-e051-4d17-8fbf-d8ba1cb1a62c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1e7a0b9-1bf3-4bd9-9c09-f9a81cf7676d",
                    "LayerId": "0f0c489a-00af-43c3-8bde-2eb910ad11ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0f0c489a-00af-43c3-8bde-2eb910ad11ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f637081-c3b5-4c83-9cc5-f27fd0cf1630",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}