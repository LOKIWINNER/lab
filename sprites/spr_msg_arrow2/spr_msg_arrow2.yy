{
    "id": "f417dd1b-5e5d-4d5c-b81f-3f08dc517dbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac1d36e3-e467-4f24-9f67-f5e1b5adc896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f417dd1b-5e5d-4d5c-b81f-3f08dc517dbb",
            "compositeImage": {
                "id": "46eef82d-4cd2-4180-a6f4-61a3621e04d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac1d36e3-e467-4f24-9f67-f5e1b5adc896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b097df98-da45-4a33-b4ed-ed7a2dd699b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac1d36e3-e467-4f24-9f67-f5e1b5adc896",
                    "LayerId": "8f02fe91-aee7-497b-af0d-46f3b4d38ca6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f02fe91-aee7-497b-af0d-46f3b4d38ca6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f417dd1b-5e5d-4d5c-b81f-3f08dc517dbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 23
}