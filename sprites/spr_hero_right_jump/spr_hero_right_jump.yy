{
    "id": "64a50da9-9a51-4b96-b7ee-18361c22e10a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 25,
    "bbox_right": 46,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3abb08b0-ae72-4a47-8cab-d15aa81dbe9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a50da9-9a51-4b96-b7ee-18361c22e10a",
            "compositeImage": {
                "id": "be184500-12a4-451d-ae54-353fdd43a047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3abb08b0-ae72-4a47-8cab-d15aa81dbe9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98125ceb-3f01-4b85-b132-03014c3a7965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3abb08b0-ae72-4a47-8cab-d15aa81dbe9e",
                    "LayerId": "a5130366-94dd-4c31-a24e-373d558f8c0a"
                }
            ]
        },
        {
            "id": "e5e38c8f-b1e8-45c7-b4de-ce51e4d3c6a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a50da9-9a51-4b96-b7ee-18361c22e10a",
            "compositeImage": {
                "id": "035e695b-2123-4a4d-9fed-0156b6a11f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e38c8f-b1e8-45c7-b4de-ce51e4d3c6a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c7c6005-8ebe-49b8-bfc4-fdac7548c742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e38c8f-b1e8-45c7-b4de-ce51e4d3c6a2",
                    "LayerId": "a5130366-94dd-4c31-a24e-373d558f8c0a"
                }
            ]
        },
        {
            "id": "c1cfbf4f-9ba2-4704-a4a3-77b4dd3a4c62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a50da9-9a51-4b96-b7ee-18361c22e10a",
            "compositeImage": {
                "id": "40a8b187-bf2b-47fb-9c26-0e42e0572537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1cfbf4f-9ba2-4704-a4a3-77b4dd3a4c62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "337ab3fe-189a-42c6-8ea4-3a93c6d35ac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1cfbf4f-9ba2-4704-a4a3-77b4dd3a4c62",
                    "LayerId": "a5130366-94dd-4c31-a24e-373d558f8c0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a5130366-94dd-4c31-a24e-373d558f8c0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64a50da9-9a51-4b96-b7ee-18361c22e10a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}