{
    "id": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94385940-72a2-4a5a-b4a6-702cb223d13a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "0f36459f-6c2a-4bc0-a59b-f319908b83a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94385940-72a2-4a5a-b4a6-702cb223d13a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f86bc5db-bc64-4b9c-ada4-6800b82f3f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94385940-72a2-4a5a-b4a6-702cb223d13a",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "82770f28-072d-4e55-8c8c-b1eb2ff02f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c8825f2a-8574-4845-a9a1-67bd26943caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82770f28-072d-4e55-8c8c-b1eb2ff02f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcbdb57f-1ff3-4a81-a06a-628ff2ca0982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82770f28-072d-4e55-8c8c-b1eb2ff02f77",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "401cd68f-ddf9-4493-96aa-8864f53123e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "51982052-8aa9-42d3-b6c5-72b19ff01a28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401cd68f-ddf9-4493-96aa-8864f53123e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0531196-0c5c-43b2-acfc-a8f4782727d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401cd68f-ddf9-4493-96aa-8864f53123e7",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e4357b12-e9fa-455e-ba39-13ada8bca96d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "9f929d95-9224-4076-996b-2387ca06f0c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4357b12-e9fa-455e-ba39-13ada8bca96d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b93bf02-8c5b-4b17-916e-e05ca4cd2ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4357b12-e9fa-455e-ba39-13ada8bca96d",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "d36ae566-6d64-431f-88dc-8c5b8c869e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "07e4870f-bf6f-4945-9e39-cdc6f78d3bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d36ae566-6d64-431f-88dc-8c5b8c869e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1fa1e2-6388-40fd-b2ff-ad3b2f9f0a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d36ae566-6d64-431f-88dc-8c5b8c869e69",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "2726317e-4dec-4572-b449-1e78afc33308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "642731fb-c921-4d5b-b2f5-b6e0896cf4d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2726317e-4dec-4572-b449-1e78afc33308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4435be-a3f9-4926-8ab3-ace321828ed0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2726317e-4dec-4572-b449-1e78afc33308",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "29ca7636-f3bb-4b5f-bc5e-67df30420f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c912f073-82d5-49bd-ae40-c590eb0e71d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ca7636-f3bb-4b5f-bc5e-67df30420f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6faf09-f260-46ce-8301-0bc3115069d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ca7636-f3bb-4b5f-bc5e-67df30420f28",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "1a36386a-f70d-4a91-9478-f9dfdd27a108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "034f0e39-f783-4ce0-a7ad-fa61e38e2a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a36386a-f70d-4a91-9478-f9dfdd27a108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b68d55d-ff12-46fd-bfac-8c1548ca93a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a36386a-f70d-4a91-9478-f9dfdd27a108",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "fbef449b-e3d4-4fd6-8c80-9cde3868a188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "6e398ec8-3ae2-4d7b-90b6-3263cd8bc047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbef449b-e3d4-4fd6-8c80-9cde3868a188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2077976f-96ff-49de-9c57-66f48ebaab81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbef449b-e3d4-4fd6-8c80-9cde3868a188",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "c087d8b3-fa30-4998-b8e2-d3e18e68ed77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "16efec17-1911-4dd7-b81c-4d08765cf4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c087d8b3-fa30-4998-b8e2-d3e18e68ed77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16bab3d7-9bba-4a00-9f1c-e5763297ce78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c087d8b3-fa30-4998-b8e2-d3e18e68ed77",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "03faa9e6-9e74-427b-a96f-c96ca4a8ba97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "02f326af-db22-4dcd-a5bc-87da5bd66027",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03faa9e6-9e74-427b-a96f-c96ca4a8ba97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d4059d-c313-4cc8-8088-0eb88391896b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03faa9e6-9e74-427b-a96f-c96ca4a8ba97",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b8302f6d-3dcd-47d3-a8e5-5f67fa130fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d0077ab5-4e3f-422f-bb19-789c4193b530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8302f6d-3dcd-47d3-a8e5-5f67fa130fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7291de7b-70bc-4d31-a250-c2ecb97eaab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8302f6d-3dcd-47d3-a8e5-5f67fa130fbe",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "95569582-01ec-4d6a-a9cd-9d626c9b6199",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "30a36dc7-156f-4463-9f7f-0765808380d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95569582-01ec-4d6a-a9cd-9d626c9b6199",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d774e7d5-31a0-432a-bd77-2ddead6025d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95569582-01ec-4d6a-a9cd-9d626c9b6199",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "f990dc0d-3fcc-44cd-96ac-f38a428a379f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "ba4a4213-8e6e-4c10-aa11-c1a0ab76eef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f990dc0d-3fcc-44cd-96ac-f38a428a379f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ee5e11-1bdc-4db3-b656-d5e49dfe43fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f990dc0d-3fcc-44cd-96ac-f38a428a379f",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "6f231c65-d235-4279-9577-fcd7c70e0388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "480e6979-3a14-4e0d-b146-290752f837ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f231c65-d235-4279-9577-fcd7c70e0388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36239bdc-481f-4b57-8b44-e91f1d33489c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f231c65-d235-4279-9577-fcd7c70e0388",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "94db087e-9582-4881-be05-688c6901835a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "4217dab0-cd9a-43f9-b997-39e769f908b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94db087e-9582-4881-be05-688c6901835a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907d976c-55de-471c-bf8a-c24df045d455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94db087e-9582-4881-be05-688c6901835a",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "dd061441-d1eb-44fd-9529-01db9db9cb5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a233bd34-93bf-404c-8e32-6811754d48e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd061441-d1eb-44fd-9529-01db9db9cb5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9ad9493-a454-4069-b592-6b0bb55c1f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd061441-d1eb-44fd-9529-01db9db9cb5c",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e4b9dc80-9d34-4c95-b78c-a65548b82aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "6763d455-fbe0-4414-b127-3f2ac3a8cfb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b9dc80-9d34-4c95-b78c-a65548b82aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78be560c-ea12-4436-adcc-257ae03d849d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b9dc80-9d34-4c95-b78c-a65548b82aac",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "fc80c3f8-1223-495c-9772-abfd09761b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d5615ff1-28f7-4284-864d-3ff739018b9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc80c3f8-1223-495c-9772-abfd09761b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b1dbd7-6cf9-4f0f-b03a-a5b7bddd6ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc80c3f8-1223-495c-9772-abfd09761b86",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "d07e2434-fd39-4bc4-9802-43a78dc5d5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "376001ae-2c41-4791-aa2e-0674ed181510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07e2434-fd39-4bc4-9802-43a78dc5d5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2b4bce-9ce3-46b4-9ccc-954730c869f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07e2434-fd39-4bc4-9802-43a78dc5d5b0",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "37b11c48-338b-4bcd-8983-193230e6dd0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "b7ded843-65e0-47e1-95eb-d70c9145c895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b11c48-338b-4bcd-8983-193230e6dd0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acb2d4f2-ea35-4a66-b1ce-506f0b2355aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b11c48-338b-4bcd-8983-193230e6dd0c",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "ba8984b7-431e-4b28-b9d0-aef620555c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "6359f995-e572-4856-8fc6-054b453ebe8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8984b7-431e-4b28-b9d0-aef620555c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b7d829-26c0-407f-a32b-74f4304f5981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8984b7-431e-4b28-b9d0-aef620555c4e",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "62eb5b75-75c4-4bd4-b2aa-c1eb3c9068a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "f5a3094b-0354-433f-925a-77050aa86e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62eb5b75-75c4-4bd4-b2aa-c1eb3c9068a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d190a0-7514-4ad4-b655-6daed6f15126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62eb5b75-75c4-4bd4-b2aa-c1eb3c9068a2",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "1be6b90d-3eb2-46db-9b68-37d17370789c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a128b5ab-f343-4252-967a-ec43a181f8a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1be6b90d-3eb2-46db-9b68-37d17370789c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650bbe74-8d15-46bf-8aa6-3e47f5bf14e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1be6b90d-3eb2-46db-9b68-37d17370789c",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "9919569d-f784-4eb6-ad1f-73b711b0a86c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "e9ebccb9-a208-40dc-afb3-0f0101d73ff9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9919569d-f784-4eb6-ad1f-73b711b0a86c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc52508-1067-4800-93a7-49cf314d36ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9919569d-f784-4eb6-ad1f-73b711b0a86c",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e2b70955-3b4b-4bcb-bd34-7b82c08c1a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "2361ba13-6c6a-47b5-b2db-f032a225cd45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b70955-3b4b-4bcb-bd34-7b82c08c1a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13e00dc7-fd8b-42d9-9a70-7887c099a075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b70955-3b4b-4bcb-bd34-7b82c08c1a9d",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a3963836-b002-46ce-ac01-98596ac2eab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "b62e2f24-43df-45d8-a70e-aa1c91d94649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3963836-b002-46ce-ac01-98596ac2eab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a063aa-3e20-42cf-986d-337baa3d993a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3963836-b002-46ce-ac01-98596ac2eab6",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "8a282cf4-9f41-44fe-8118-1fce3fa80c91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a2d18d8a-8a7e-483c-a8c0-4bcf9bd28081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a282cf4-9f41-44fe-8118-1fce3fa80c91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d15ad790-eb0c-46d9-9b6b-19963e3dadfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a282cf4-9f41-44fe-8118-1fce3fa80c91",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b4148ce1-85bf-4b84-9e9a-7816928037c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "ab442d41-2079-4c72-aeed-40242ec32274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4148ce1-85bf-4b84-9e9a-7816928037c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc34b56-9fc2-4d73-92c0-08004afe7269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4148ce1-85bf-4b84-9e9a-7816928037c0",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "ac21a2c0-6b96-4387-970f-4bb6639a61e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "5ed25038-69fb-43e2-8a8e-abd2a27b6edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac21a2c0-6b96-4387-970f-4bb6639a61e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354ad0b2-4772-482b-a8db-49fe4deb7dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac21a2c0-6b96-4387-970f-4bb6639a61e2",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "afc69f1f-68e2-40fd-8988-92866e88d75d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "0d21b034-fd7d-4ab2-b627-ee9ef6d446b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afc69f1f-68e2-40fd-8988-92866e88d75d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb23929d-5d24-46bf-baba-961e4e336941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc69f1f-68e2-40fd-8988-92866e88d75d",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "9f909bc4-6afc-4e83-bbcb-4c4c2d37f7f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "6d5b860f-b96c-44d8-961c-8e22d3c2bbe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f909bc4-6afc-4e83-bbcb-4c4c2d37f7f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3fd8e77-8171-4e81-bf96-48b8c0b6bcad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f909bc4-6afc-4e83-bbcb-4c4c2d37f7f5",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "5ce253d2-6ff7-4926-a447-b22631e3ea30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "00b4076c-77c4-4c29-9594-a0b57014dd42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce253d2-6ff7-4926-a447-b22631e3ea30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a56a8dc-f5d7-4fd3-9da3-ff8e90d5d790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce253d2-6ff7-4926-a447-b22631e3ea30",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b8d1eda3-9975-4fd6-9b94-1c6d54b9a3e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "96e356d4-658a-4f69-907d-542ae087dfa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8d1eda3-9975-4fd6-9b94-1c6d54b9a3e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6426b68c-3dba-47c0-a99c-30c5d3d64f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d1eda3-9975-4fd6-9b94-1c6d54b9a3e6",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e44d0f75-606d-40f5-af50-37dcb5ec12c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d35d282b-89f4-46d4-9ecb-6b2c505da783",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e44d0f75-606d-40f5-af50-37dcb5ec12c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81860770-5e3c-47fd-9d68-3e6495cfa651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44d0f75-606d-40f5-af50-37dcb5ec12c0",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "21757368-a0ca-4214-bb81-a01239102228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d64420c8-62c4-4f92-ac01-02d77389ee5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21757368-a0ca-4214-bb81-a01239102228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c6f933-efc2-431d-8cbf-81563f3305ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21757368-a0ca-4214-bb81-a01239102228",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "4621ccd2-2eac-41b0-8ab6-31be6d1959d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c20fa67f-4c5e-447b-aea3-777e31db6109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4621ccd2-2eac-41b0-8ab6-31be6d1959d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af63820c-7533-4151-98e9-9335e4fcbf8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4621ccd2-2eac-41b0-8ab6-31be6d1959d8",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "9c105c60-9cb8-4b4b-b269-44b068fcbd89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "b74050a8-2888-4093-bc1d-8fcd3367bb5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c105c60-9cb8-4b4b-b269-44b068fcbd89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66dc11f4-9352-49d4-b967-f1e9d2401584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c105c60-9cb8-4b4b-b269-44b068fcbd89",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b5c51bf2-0855-4195-832b-37ccfaf10ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "59b2f725-960d-488e-baa5-e37edcbea709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5c51bf2-0855-4195-832b-37ccfaf10ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75467dda-ece2-42ad-a8ac-61b37537d30d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5c51bf2-0855-4195-832b-37ccfaf10ea3",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "db3dc672-bc3b-409a-bcf6-a0befd5a3287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "72dcb795-b0b2-4549-b838-5e499eaecde9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3dc672-bc3b-409a-bcf6-a0befd5a3287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311734b7-0464-4bc6-a1a9-d27e82ef8819",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3dc672-bc3b-409a-bcf6-a0befd5a3287",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a3fa2999-14ac-4cf6-9595-92af3cf7abe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "4513c700-1565-4484-b3b6-a2c4ef7841cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3fa2999-14ac-4cf6-9595-92af3cf7abe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6504e08b-2530-436d-b1f9-6d970df6a41d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3fa2999-14ac-4cf6-9595-92af3cf7abe8",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "50ae3ed5-ec8f-4101-afb6-277ee3b35eb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "4e5883c5-9b43-400b-807f-96f0166e06d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ae3ed5-ec8f-4101-afb6-277ee3b35eb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37e5bbd4-5b34-4c0a-a0d4-76b43284f68b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ae3ed5-ec8f-4101-afb6-277ee3b35eb5",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "40dec96c-a041-4434-aa46-b10d7c6972e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "168b2e30-86e4-41dd-b012-348c8b87e1d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40dec96c-a041-4434-aa46-b10d7c6972e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58322b94-8ede-4c10-b67f-63642054077c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40dec96c-a041-4434-aa46-b10d7c6972e5",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "c65184d5-fe9b-456e-9741-c363289bc885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a9bbe1ac-59fd-4282-8649-f16d5ed00041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c65184d5-fe9b-456e-9741-c363289bc885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985f2b97-4a9a-483d-a2c0-08767a7a2e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c65184d5-fe9b-456e-9741-c363289bc885",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "d42bb810-b20c-4b70-ac97-12603caa5a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "142e3a0f-5c44-421a-8421-5e9d40fa936f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d42bb810-b20c-4b70-ac97-12603caa5a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e17c004-6b03-40e4-b9a8-48951bd8a39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d42bb810-b20c-4b70-ac97-12603caa5a63",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "ac3c4c91-60a8-4046-9107-8890126fc5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "08dcc9d1-76fd-47cc-bd34-bc2097d820c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac3c4c91-60a8-4046-9107-8890126fc5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8725de-b3a0-464d-ba7f-29518f4c9d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac3c4c91-60a8-4046-9107-8890126fc5d7",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "423fd44e-71f0-46a4-aa15-9cad37f7376b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "fe47f9bf-df85-4e9a-9754-ab9c0cd652ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423fd44e-71f0-46a4-aa15-9cad37f7376b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3274aa91-f63b-4b1b-a7ed-c0dd791436aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423fd44e-71f0-46a4-aa15-9cad37f7376b",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "55576b9b-4f07-4a88-abda-8a4a7349fd70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "b393ef82-8b65-4b80-8c57-df217e398e37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55576b9b-4f07-4a88-abda-8a4a7349fd70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d68cc6b-befb-4cb3-b758-013982669cb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55576b9b-4f07-4a88-abda-8a4a7349fd70",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "8132cf85-816e-4356-b560-d2076c5075da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "43473286-41df-496b-aa88-e29b32ed5574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8132cf85-816e-4356-b560-d2076c5075da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce007cb-9d35-4815-a3a9-1ba7b102675c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8132cf85-816e-4356-b560-d2076c5075da",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "f79c56d9-375f-4897-ae0a-10e8b2980943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "5d5d4563-58ce-49e4-98ee-a4d9fa03b2e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f79c56d9-375f-4897-ae0a-10e8b2980943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45bd27e4-6c59-48b0-8128-880f74f1d7ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f79c56d9-375f-4897-ae0a-10e8b2980943",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "bb7e2dbb-3b8c-4989-aaac-990ce0b5ea47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "56e05c87-4126-43fc-91d7-2ae1dafbc77f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7e2dbb-3b8c-4989-aaac-990ce0b5ea47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f99b64-3081-4830-908f-3e22a409ba93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7e2dbb-3b8c-4989-aaac-990ce0b5ea47",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b31ccb32-f100-4dd2-931e-bdf1eeb9abef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "2e0f1fcb-bdae-43f8-a072-81662f60adb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31ccb32-f100-4dd2-931e-bdf1eeb9abef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb8c24e-1bf3-496f-b685-e0d1ba9367e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31ccb32-f100-4dd2-931e-bdf1eeb9abef",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "0803771d-8dd5-42bc-bff8-aa0ee3a269f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d8316ff9-467d-41de-b36d-e0fffd6468ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0803771d-8dd5-42bc-bff8-aa0ee3a269f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1532e4-cc3b-4a82-a2e5-60a440eae79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0803771d-8dd5-42bc-bff8-aa0ee3a269f7",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "89a3d861-afe3-4811-b7c0-61658e2aae1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "07a50bc4-ed35-46b0-af3e-32966b8b70dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a3d861-afe3-4811-b7c0-61658e2aae1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc04dcce-ede8-468f-b522-7fb6491c8d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a3d861-afe3-4811-b7c0-61658e2aae1c",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "ec514793-493c-4dfe-b667-49490cbcf3c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "fcecd53e-5f8a-40f0-b1b6-246ce5de20b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec514793-493c-4dfe-b667-49490cbcf3c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c54af88-bbc4-4c24-a532-01ae76fff1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec514793-493c-4dfe-b667-49490cbcf3c8",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "c69f0077-5037-43fb-b530-a1bd98f2b5de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "eec3f57a-d7ce-4872-9ee9-dcae4e522095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c69f0077-5037-43fb-b530-a1bd98f2b5de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02d80ac0-40c1-457d-b59d-d352aa2f5bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c69f0077-5037-43fb-b530-a1bd98f2b5de",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "3876e1f7-d641-491d-b415-e1e384b4e0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "5e19a35c-0103-443e-9174-4bd34261aaeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3876e1f7-d641-491d-b415-e1e384b4e0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da921571-2557-4b80-8da1-999f250d1849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3876e1f7-d641-491d-b415-e1e384b4e0e7",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b7675e77-54d5-4ec7-8de4-2472e6cb3992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "4d7454d9-5e54-460e-9f3b-9f0ef601e76f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7675e77-54d5-4ec7-8de4-2472e6cb3992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a787fc68-a547-4396-9d78-6db429a6d0ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7675e77-54d5-4ec7-8de4-2472e6cb3992",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a685c4ed-379b-4c09-ac46-3d67296a6fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "ce7d637a-f85d-4462-9eb4-f63b3180d080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a685c4ed-379b-4c09-ac46-3d67296a6fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552ca068-7c5e-4044-8f63-f2c990a643bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a685c4ed-379b-4c09-ac46-3d67296a6fc9",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "73d86af6-1415-48ef-a181-a3cea85ac591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "ef16581b-ccbf-4f20-a2e4-bea175495c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73d86af6-1415-48ef-a181-a3cea85ac591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19793147-8545-4d1b-b13c-9f1c273e2402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73d86af6-1415-48ef-a181-a3cea85ac591",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "c21ecb67-c027-461d-9ac0-cab3f86959d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c5229fc1-7c73-420e-9c3b-cef3956239dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c21ecb67-c027-461d-9ac0-cab3f86959d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82150706-a33c-4d75-9b24-d8a5b4ca6f3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21ecb67-c027-461d-9ac0-cab3f86959d8",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "394d8570-bcfa-4941-87a1-fe9b0295f7d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "f53580fb-15e8-43c3-9429-3d902aa3bcc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "394d8570-bcfa-4941-87a1-fe9b0295f7d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbb1b9ee-dd80-479a-8609-69aceddd0f7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "394d8570-bcfa-4941-87a1-fe9b0295f7d1",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "6b861be4-d504-48ed-9f4c-d930d72d7f63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "684f94c6-3ae8-4509-b867-9ec161a3e126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b861be4-d504-48ed-9f4c-d930d72d7f63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9e4bac-1faf-4545-b91a-f7e84ad366b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b861be4-d504-48ed-9f4c-d930d72d7f63",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "8fd8f886-dc11-4d20-92c8-9a3edeb778bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "77658e90-ad6e-48b0-8894-c76198c63004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd8f886-dc11-4d20-92c8-9a3edeb778bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f471bbe0-4780-4543-9b3d-b46d5c6b68c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd8f886-dc11-4d20-92c8-9a3edeb778bc",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "389177e1-7a1f-40eb-8354-c0c987c90587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "53b85446-91b9-4ab5-bb86-948f7b80d000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389177e1-7a1f-40eb-8354-c0c987c90587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f066f2-6bbd-4264-a088-ee8116864c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389177e1-7a1f-40eb-8354-c0c987c90587",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "61df091f-1464-4445-a4c9-3ac79ec86d92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "82967e71-50ae-4b44-a875-40ef9d254839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61df091f-1464-4445-a4c9-3ac79ec86d92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dacbf7e-8749-487f-8dcf-0f037394ea90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61df091f-1464-4445-a4c9-3ac79ec86d92",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "504c68fc-3669-463b-aa99-9874da8ec5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "3fa6eacc-dd10-4e09-836a-38947e991b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "504c68fc-3669-463b-aa99-9874da8ec5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4deb04b0-70f9-482b-9876-3885fdaeed15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "504c68fc-3669-463b-aa99-9874da8ec5fa",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "81678ca7-0c58-4940-accb-a737cca83228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "2a13ec4e-8569-47a8-9b78-f24e7cafdb7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81678ca7-0c58-4940-accb-a737cca83228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf7c4b3-5625-4393-b341-e70b7bd4a6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81678ca7-0c58-4940-accb-a737cca83228",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "1348e7f6-921d-4b15-8511-9fa21aedcb04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "67f47c96-43f9-433f-b64c-1caa9b4ad572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1348e7f6-921d-4b15-8511-9fa21aedcb04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8669c63b-365c-4205-aa0a-1eeff6c55a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1348e7f6-921d-4b15-8511-9fa21aedcb04",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "8c5f09bb-3ed8-49c2-b841-6fcbfd37faea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a05c2029-0db6-45c0-b01d-4aa0afabeb34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5f09bb-3ed8-49c2-b841-6fcbfd37faea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00707c7a-b118-49eb-b409-c19d71c52a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5f09bb-3ed8-49c2-b841-6fcbfd37faea",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "0941eeb5-3191-4a91-aa26-1713e86d899e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "8c053746-4663-4813-bdcc-1a2a36c5aec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0941eeb5-3191-4a91-aa26-1713e86d899e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98fe210a-9609-40fd-8c4e-b68d1f243eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0941eeb5-3191-4a91-aa26-1713e86d899e",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "efff78de-07ab-46d2-9986-513699e8815c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "242f9556-b8db-4d9a-9f18-3dc184f939a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efff78de-07ab-46d2-9986-513699e8815c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80af086e-9c7f-4771-97b3-441eded4206d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efff78de-07ab-46d2-9986-513699e8815c",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a97e9f5f-0f5d-4a1a-b7e9-6515bd881240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "9a7b82a5-cc49-47a2-8230-a7ffe28c7096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a97e9f5f-0f5d-4a1a-b7e9-6515bd881240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb19cbc-fd00-4eeb-b807-e6c3f406927b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a97e9f5f-0f5d-4a1a-b7e9-6515bd881240",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "5478b32a-0d9c-476c-95a6-1106d6b50000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "88c608d9-6e05-4457-a3b0-01a5485bcf84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5478b32a-0d9c-476c-95a6-1106d6b50000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bb66d0-9bde-4af7-9c7d-4dc8c3db9073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5478b32a-0d9c-476c-95a6-1106d6b50000",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "0fa070be-42aa-4ccf-bb01-cb380515e152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "b21ecc72-2cda-4676-909c-af2671922098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa070be-42aa-4ccf-bb01-cb380515e152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbea7412-e891-4d7b-a6c5-cd86ea668c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa070be-42aa-4ccf-bb01-cb380515e152",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "972a1531-cc7d-4662-b0be-15d6751116a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "97c9a744-bbae-4456-8078-5c9beb2ac293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972a1531-cc7d-4662-b0be-15d6751116a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "217f36f9-7abe-42dd-a5c9-d27e0ec4e9b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972a1531-cc7d-4662-b0be-15d6751116a2",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "5c5d13b8-41cf-4266-a661-acba106558c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "69e6773f-3b4b-4e96-b73c-1e7b18e16df4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5d13b8-41cf-4266-a661-acba106558c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "641de6d3-25e5-4e89-8ce9-8f204a07e914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5d13b8-41cf-4266-a661-acba106558c4",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "760d514d-9362-4ffc-8c61-f415210e474e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c80a7d50-31e8-4b10-a0f2-9e8fa867f581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760d514d-9362-4ffc-8c61-f415210e474e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a04d5e-31f8-4611-98f1-0219798d2d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760d514d-9362-4ffc-8c61-f415210e474e",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "5b9ce53d-11b0-4649-ba4c-2af57a86bcea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "543708d0-3f31-4890-9042-bd84387b531c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b9ce53d-11b0-4649-ba4c-2af57a86bcea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9efdf1d4-47a6-4834-875f-088c2d34f829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b9ce53d-11b0-4649-ba4c-2af57a86bcea",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "1b29d7d2-0b9d-4c3a-a4df-6dd856aca0c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a091236d-9a46-4a2a-9204-7518ab4c59da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b29d7d2-0b9d-4c3a-a4df-6dd856aca0c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2b02052-bfb6-4099-8008-c8e5483faff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b29d7d2-0b9d-4c3a-a4df-6dd856aca0c6",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "3b0f1ba0-75aa-47c6-89c9-d7aa4d04d06e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "732de63e-54f7-4920-8207-6a7f4e810ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b0f1ba0-75aa-47c6-89c9-d7aa4d04d06e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f015b348-819e-49eb-ab63-c1ee11df24d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b0f1ba0-75aa-47c6-89c9-d7aa4d04d06e",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "c2628186-7cc3-431b-9c0e-d8fbc3e7394f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "f4735528-b3fb-4cda-8640-6a3cbedde97c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2628186-7cc3-431b-9c0e-d8fbc3e7394f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efda103b-91c8-4c3e-ac32-6471ca876238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2628186-7cc3-431b-9c0e-d8fbc3e7394f",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "94b4d14f-33f8-4b6e-9bf4-f17ffd3302c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "6f0d4450-3ed1-471a-87b4-c339aaab89bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b4d14f-33f8-4b6e-9bf4-f17ffd3302c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1d09136-2563-42f9-b142-1f47ba865400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b4d14f-33f8-4b6e-9bf4-f17ffd3302c3",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "4a628f4a-179e-44e6-8fc2-f76a7ebc4564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a00e6578-72a6-4099-95d2-844b7a8f5ad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a628f4a-179e-44e6-8fc2-f76a7ebc4564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eea79e2-c260-4dfc-9334-f305952b3cd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a628f4a-179e-44e6-8fc2-f76a7ebc4564",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "eba0a17a-e840-4f7f-a357-213833cabe22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d62c37a3-3305-498f-b13a-687c31d9b94c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba0a17a-e840-4f7f-a357-213833cabe22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddee6929-4079-42a4-bc68-4146960642fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba0a17a-e840-4f7f-a357-213833cabe22",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a4e795f7-1321-44c8-a4d1-b69259038517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "5febabd5-9ec1-4410-b9b7-9533d87b7c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e795f7-1321-44c8-a4d1-b69259038517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77eda973-29b9-4f65-80f1-9ef1827e83e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e795f7-1321-44c8-a4d1-b69259038517",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "088a56b9-b7ac-456a-befe-cf4fb7b762fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a18f30d4-8ca6-4266-ad2b-728e416df766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088a56b9-b7ac-456a-befe-cf4fb7b762fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6652712-57ac-4a15-8793-4861011f45f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088a56b9-b7ac-456a-befe-cf4fb7b762fa",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a6a6ffa3-ef23-4510-8b81-1a8ef2a97e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "493629c2-3dfb-4b1d-8402-966ea53c39e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a6ffa3-ef23-4510-8b81-1a8ef2a97e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99c842d-b6f2-417d-8b99-656d9679c2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a6ffa3-ef23-4510-8b81-1a8ef2a97e73",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "21574a0f-8c9c-4a76-a629-d7adeb6d0134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "60ce42ec-dd48-436f-b95e-922080e3ef56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21574a0f-8c9c-4a76-a629-d7adeb6d0134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9550967f-bc5b-45ff-bbe3-b34cad312ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21574a0f-8c9c-4a76-a629-d7adeb6d0134",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "143e0897-61e9-428e-b40d-eb784c7fb46b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "3c4208f6-b5a4-4b77-b6cd-87a6ad1702ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143e0897-61e9-428e-b40d-eb784c7fb46b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79cbc004-0eb9-4baf-8f43-9c36ddd74c3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143e0897-61e9-428e-b40d-eb784c7fb46b",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "ea7051c8-c785-4ba2-a791-e512c934858f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a9b11607-a2f1-47e0-ba9d-350cd7d7aa62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea7051c8-c785-4ba2-a791-e512c934858f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65211b16-f187-45bb-a4ac-4b0607606ccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea7051c8-c785-4ba2-a791-e512c934858f",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "643551f6-4d5c-492f-b23d-5e0e618f2467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a8cf8eff-0c72-44e5-8e0e-791a11590c5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643551f6-4d5c-492f-b23d-5e0e618f2467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ff5f37-9ab2-4e23-aa8c-b209853cf445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643551f6-4d5c-492f-b23d-5e0e618f2467",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a3ac50b6-b74f-4804-84fb-d7ff368a5fe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c37c2ab4-b78d-46bb-9114-fd08062da0c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ac50b6-b74f-4804-84fb-d7ff368a5fe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0adcc01b-14c2-4f17-919b-1f425ba29c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ac50b6-b74f-4804-84fb-d7ff368a5fe8",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "fcd839bb-bf0a-472e-afaa-2867459f2be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d4d307ee-ea26-4cf2-b3af-8735773eb85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd839bb-bf0a-472e-afaa-2867459f2be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444016a2-5070-4f26-8911-a9d1e3848ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd839bb-bf0a-472e-afaa-2867459f2be2",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "d8141e65-f35c-4dcb-a6b0-985263dd1691",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "3fc132ba-b65a-41c9-91a8-d4a80ed98113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8141e65-f35c-4dcb-a6b0-985263dd1691",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c7af1d-95f2-45c2-a46b-db2c041a8380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8141e65-f35c-4dcb-a6b0-985263dd1691",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "1ee6b34b-51d3-4686-a00b-f454b881cd8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c4f2400f-ccd2-4de5-9094-8783bcde9fa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ee6b34b-51d3-4686-a00b-f454b881cd8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b65213-3c15-4251-a67f-e3b127de202d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ee6b34b-51d3-4686-a00b-f454b881cd8e",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "db8af33a-cb15-477e-ba6b-2f1042ce6a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "333a0479-a471-4487-9508-863da955bccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8af33a-cb15-477e-ba6b-2f1042ce6a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76aa62fd-a83e-4f08-95f3-bb0027b15d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8af33a-cb15-477e-ba6b-2f1042ce6a65",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a1cf717b-be2d-4a7e-811f-40602d41c153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "4b8c3e1d-4690-4d6c-b060-f5133b1a8f51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1cf717b-be2d-4a7e-811f-40602d41c153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d63b57f-b683-4270-ab01-971a837e3312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1cf717b-be2d-4a7e-811f-40602d41c153",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b9a861b8-9bc4-4c47-b733-b28b8b4625f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "e3636491-3952-464b-88e3-49940038cf0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9a861b8-9bc4-4c47-b733-b28b8b4625f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b70262e-b3d0-4b14-91c9-1316d5764729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9a861b8-9bc4-4c47-b733-b28b8b4625f9",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "bb402958-5aa4-4691-aede-a9ae9b0049c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "325735ea-a9c8-4dda-bf82-2561a135a128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb402958-5aa4-4691-aede-a9ae9b0049c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5172dc6-0036-454c-8411-3d5cc52aecb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb402958-5aa4-4691-aede-a9ae9b0049c9",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "9b4e5d7e-691b-42db-be29-8d8b87e258d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a493b583-f6d9-4ce3-a294-cb31fc4b9e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b4e5d7e-691b-42db-be29-8d8b87e258d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5db482-c9e2-4aa7-9da1-1a984065d96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b4e5d7e-691b-42db-be29-8d8b87e258d7",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "23d4a62d-2a2b-49a0-9dc5-0cf3486b0882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "6771f1d8-a8c5-4aad-91ec-435a54826253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d4a62d-2a2b-49a0-9dc5-0cf3486b0882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb97f92-705e-4625-bc29-60041a946efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d4a62d-2a2b-49a0-9dc5-0cf3486b0882",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e2aa69f6-3ede-432b-958c-622984f07915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "521e3a35-4d82-4fdd-ad61-b8ac9a69f590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2aa69f6-3ede-432b-958c-622984f07915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af806bc9-ae40-44c8-bdd1-3d04b7f3d1fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2aa69f6-3ede-432b-958c-622984f07915",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "b000daf9-5a8a-4b58-86cc-63cd650db776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "3afaf0cd-a8de-442e-9b31-18fcb1ed679b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b000daf9-5a8a-4b58-86cc-63cd650db776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ef536a7-f274-4859-ac29-ad3fa70a3899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b000daf9-5a8a-4b58-86cc-63cd650db776",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "4cf3113c-87a2-44c7-bf15-bb5f7a58d0ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "512c37e4-a1b4-4514-95a0-0d893018141c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf3113c-87a2-44c7-bf15-bb5f7a58d0ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c25a29-8832-4602-b002-da0a7130ebac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf3113c-87a2-44c7-bf15-bb5f7a58d0ff",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "1a0c32bb-c808-47c9-804b-97488bf96d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "c8083771-537e-4410-b964-15dbd4d499f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a0c32bb-c808-47c9-804b-97488bf96d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf6b4bf-a85e-4cd0-86f6-94604ca539a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a0c32bb-c808-47c9-804b-97488bf96d73",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "61acaf63-70fa-4dbb-8466-42bfbf7ab72a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "3429c46c-7069-4a6c-ab34-854d7e923696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61acaf63-70fa-4dbb-8466-42bfbf7ab72a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cbd7a4f-b20d-4493-a8fd-9b6685a7ee1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61acaf63-70fa-4dbb-8466-42bfbf7ab72a",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "c337fec7-ad79-4dda-9c9d-5e1892ac28fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "475fe4f6-0ddf-42f0-9976-e097afe87b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c337fec7-ad79-4dda-9c9d-5e1892ac28fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d2954b-2856-456f-9800-c0401d588a91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c337fec7-ad79-4dda-9c9d-5e1892ac28fd",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a179391a-3330-40db-9fbf-e9698d36f641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "06d601bf-b215-4bf6-923c-9516611c6bfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a179391a-3330-40db-9fbf-e9698d36f641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9f08a1d-226d-4e51-b603-f2ef89d83f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a179391a-3330-40db-9fbf-e9698d36f641",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e9a1ab0d-40b7-4cf7-abd6-ca4ddfdecba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "4ca839b1-0746-4175-8b99-60ce05e01a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a1ab0d-40b7-4cf7-abd6-ca4ddfdecba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceea7bf1-2e5c-4d6f-b663-468ab98b9f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a1ab0d-40b7-4cf7-abd6-ca4ddfdecba3",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "7b09e719-f88d-4a01-a9c7-4ea0ad1022b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d58dc16a-8fcd-4e76-8653-8d75a4ffb82a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b09e719-f88d-4a01-a9c7-4ea0ad1022b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2fef095-e02d-41c2-b312-4e97e9d1a8e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b09e719-f88d-4a01-a9c7-4ea0ad1022b6",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "92c84f6d-727f-49c2-895e-f4664c9c4445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "a0e709cb-0ddb-4e95-94c5-056927855ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c84f6d-727f-49c2-895e-f4664c9c4445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38cbb7c8-d055-4fa6-bc45-f13f64d4cc18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c84f6d-727f-49c2-895e-f4664c9c4445",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "e15b4bce-6ed1-4f97-b3b9-14439f94362b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "f4066694-5453-4432-8a05-6fc1304223f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15b4bce-6ed1-4f97-b3b9-14439f94362b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c2416c-5d5e-440d-b404-c36e9eee9269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15b4bce-6ed1-4f97-b3b9-14439f94362b",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "a7fe9259-e465-4c88-bab3-e39e909baa8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "8f4206f3-d186-4958-b0e9-4127de96846f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fe9259-e465-4c88-bab3-e39e909baa8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "009f822b-6b42-4606-aad8-fab14f5dd0a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fe9259-e465-4c88-bab3-e39e909baa8f",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "58e01758-b662-4ad8-b8c4-ac2baa67c372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "5b8cd7e1-2db4-4478-9a9a-8bb15dfb378b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e01758-b662-4ad8-b8c4-ac2baa67c372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea51d55-77b0-4652-89f5-4702f9d5c43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e01758-b662-4ad8-b8c4-ac2baa67c372",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "6817afa0-0df4-4e08-9d6c-9f59188382a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d169e726-664f-4d6e-9178-21f7b0dc6a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6817afa0-0df4-4e08-9d6c-9f59188382a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4febbc-f994-4b60-bb09-266bccfd9c1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6817afa0-0df4-4e08-9d6c-9f59188382a8",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "350fd5bb-f46d-4a2a-9aa7-f36f884f1b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "8744f756-1d8b-4015-8e64-60bfad1279af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350fd5bb-f46d-4a2a-9aa7-f36f884f1b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acfe8f66-4cc5-4af2-97c4-633817cb72e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350fd5bb-f46d-4a2a-9aa7-f36f884f1b45",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "8d63b24c-e9da-4356-9968-cb74d1f43989",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "d9fbee0a-aab9-4df9-ac7b-e4ef2548f3f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d63b24c-e9da-4356-9968-cb74d1f43989",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61989a1e-597c-42e2-b643-28d8f1c34810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d63b24c-e9da-4356-9968-cb74d1f43989",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "d301b942-5598-4e6b-8b73-2b1b8ae49af6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "cffa2dc6-8a6b-4851-a919-c3a917988520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d301b942-5598-4e6b-8b73-2b1b8ae49af6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f701e3-657d-4d85-be48-cb5445b49afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d301b942-5598-4e6b-8b73-2b1b8ae49af6",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "4dd5e77e-a3ba-42b0-9e0c-421b90a3b303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "7133657f-deb0-4cd9-a4bb-8a1d6e5d3bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd5e77e-a3ba-42b0-9e0c-421b90a3b303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11a6332a-5955-4bca-b3c0-9ce9b0b66d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd5e77e-a3ba-42b0-9e0c-421b90a3b303",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        },
        {
            "id": "54017971-b2a1-4eba-aaeb-982d0fa9788f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "compositeImage": {
                "id": "297962c7-a734-44fc-bdc3-f140fd878c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54017971-b2a1-4eba-aaeb-982d0fa9788f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c693b5-d73a-4738-8ae6-737c14e3268a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54017971-b2a1-4eba-aaeb-982d0fa9788f",
                    "LayerId": "1b6fc17c-7001-4d25-946e-5cd991de38ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1b6fc17c-7001-4d25-946e-5cd991de38ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1eebaee-7fd4-4ad7-a46d-172ea846bc27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}