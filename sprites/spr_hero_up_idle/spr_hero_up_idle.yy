{
    "id": "02084387-3c3a-47e1-b02a-e2aa7520f825",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 17,
    "bbox_right": 58,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "968093ef-0b8c-4f80-9db5-5038dc76495a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "f4850dd7-bff1-4fc4-8cb5-6d6306b3355f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968093ef-0b8c-4f80-9db5-5038dc76495a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad7d3a4-859a-4799-9817-7bc405f4a446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968093ef-0b8c-4f80-9db5-5038dc76495a",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "0de1721d-8847-4caa-997f-61e8467521fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "fb102d8e-6077-4d9f-bf48-62acb8883f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de1721d-8847-4caa-997f-61e8467521fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f87509de-9f88-422a-a41f-138e98d82592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de1721d-8847-4caa-997f-61e8467521fe",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "09ca3460-d3e5-4a06-9755-1a9e8dc90d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "10581ec4-4732-4aee-a53b-e2e91c1bd2ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ca3460-d3e5-4a06-9755-1a9e8dc90d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29ffdefe-7637-42fa-9178-b7bb4f2468a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ca3460-d3e5-4a06-9755-1a9e8dc90d1a",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "e69a0f26-aac8-48fe-a8f7-cda2aff98427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "a149c1a1-9e49-428f-a3ce-2b0c29813fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69a0f26-aac8-48fe-a8f7-cda2aff98427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6b82cc-9093-45d1-aee2-92839971e830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69a0f26-aac8-48fe-a8f7-cda2aff98427",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "99b743d7-8110-4725-8861-d5a1006d1ad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "408af65a-5149-465e-a195-b18bd6de142d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b743d7-8110-4725-8861-d5a1006d1ad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5e000c-e0d5-4d2a-9890-b0eff73305b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b743d7-8110-4725-8861-d5a1006d1ad5",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "a26ed2fb-88e7-424f-bfa3-a37b92bb5dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "1e0c4342-785e-44a2-b113-c9c5ca63bf89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26ed2fb-88e7-424f-bfa3-a37b92bb5dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f5d3c73-dc2a-491a-ba31-5650f4b0b468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26ed2fb-88e7-424f-bfa3-a37b92bb5dca",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "aca9806d-1c11-468d-8ee2-a03c22fbe681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "dde0e222-a96a-48c8-a295-f700917e9790",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca9806d-1c11-468d-8ee2-a03c22fbe681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84519f2c-29ca-497e-92a8-36f77af62a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca9806d-1c11-468d-8ee2-a03c22fbe681",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        },
        {
            "id": "23534f2c-6224-41fb-8c1c-fb4b61358336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "compositeImage": {
                "id": "c916f641-1678-4c1a-9ee8-6a52a5142059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23534f2c-6224-41fb-8c1c-fb4b61358336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df98b5d-ea1c-43e1-ab6e-f3f47805f6d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23534f2c-6224-41fb-8c1c-fb4b61358336",
                    "LayerId": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0bae0fc3-6b43-4851-9e86-ecfa55ef4d01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02084387-3c3a-47e1-b02a-e2aa7520f825",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}