{
    "id": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_sun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "330fb34b-b07a-45d7-baf6-8ff6889142a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "1f89112f-97d1-4cb6-8b9c-d5dc1183ce27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "330fb34b-b07a-45d7-baf6-8ff6889142a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a12108-9091-4c30-8136-269671bf226f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "330fb34b-b07a-45d7-baf6-8ff6889142a9",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "a5410291-9334-4669-bac9-45286505fc65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "daefd905-8092-4bd2-85db-b000192e1c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5410291-9334-4669-bac9-45286505fc65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "176295d2-700c-447a-9942-8729f3a8088b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5410291-9334-4669-bac9-45286505fc65",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "e70459f6-b3f3-464d-9841-90ca65f7d0b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "d78c03a7-2d6f-4183-91f2-1f4f97cc0e81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e70459f6-b3f3-464d-9841-90ca65f7d0b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802f65ba-613e-4a02-aa0a-c5b2128a63b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e70459f6-b3f3-464d-9841-90ca65f7d0b6",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "0390da67-9b87-4484-a5fc-3718271a2264",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "0155c471-8e41-46e8-a024-00db9fb42e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0390da67-9b87-4484-a5fc-3718271a2264",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2fa73d0-cdd0-493e-b139-1143c6f1ca5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0390da67-9b87-4484-a5fc-3718271a2264",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "3f3cdae7-9f60-41a1-a1e4-d37af394e1ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "0fd6dd43-2faf-4069-a8e2-efa459e98e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3cdae7-9f60-41a1-a1e4-d37af394e1ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a51110a-da1d-4ceb-aaeb-ae3d704ff79d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3cdae7-9f60-41a1-a1e4-d37af394e1ec",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "9fcb6f3e-6329-47f6-b720-63a62db17f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "0c809c0f-47a5-4923-9056-1029e75f6f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fcb6f3e-6329-47f6-b720-63a62db17f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac369ce-310d-4c24-93b2-27bb7b7bb827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fcb6f3e-6329-47f6-b720-63a62db17f50",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "818b0439-fead-4892-9f98-f9ad7dad98f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "fb235e80-1abe-4dd9-9387-b52fb0086f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "818b0439-fead-4892-9f98-f9ad7dad98f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aed67be-0ad3-4a31-bfc8-89f879b356ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "818b0439-fead-4892-9f98-f9ad7dad98f8",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "e5ca6ea1-5ae1-4e0e-860d-f53b392c3848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "0a6082dd-3037-44c5-bc10-51705339191a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5ca6ea1-5ae1-4e0e-860d-f53b392c3848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff12bac-bce8-433e-bb1b-011d78aea18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5ca6ea1-5ae1-4e0e-860d-f53b392c3848",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "b05cbd2a-21b1-454b-b536-8b63ee94b655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "1727f7ee-5834-46e3-82e1-cc48217f80c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b05cbd2a-21b1-454b-b536-8b63ee94b655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a607e8-e186-460c-abea-83589c154bda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b05cbd2a-21b1-454b-b536-8b63ee94b655",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "40ab6fe9-2af0-4882-a95c-cbdd06f9c4f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "a66572e6-0276-4726-af2d-8fdde77eff66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ab6fe9-2af0-4882-a95c-cbdd06f9c4f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "118a118d-7ea3-4ea7-ab62-5fa030064b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ab6fe9-2af0-4882-a95c-cbdd06f9c4f3",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "ad5e509e-5f2b-46e4-abb3-18167d1b89f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "9cb46912-6928-4670-b0de-9cc05e55d06c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad5e509e-5f2b-46e4-abb3-18167d1b89f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c03f44a-d75a-4e69-9d3a-def2260e333a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad5e509e-5f2b-46e4-abb3-18167d1b89f7",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "1801a5d3-6ccf-4f44-b299-4b2da1297b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "9c7e7370-8d07-433b-a1f6-1bc40eb884d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1801a5d3-6ccf-4f44-b299-4b2da1297b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5433cf1e-0c9f-4575-a57b-f99eceddc2d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1801a5d3-6ccf-4f44-b299-4b2da1297b0b",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "c4686d8d-4cfc-427c-bb27-07d70caf4854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "6cb70a60-b43e-4a51-835f-fa82bd6aaac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4686d8d-4cfc-427c-bb27-07d70caf4854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab080fd0-bfbf-47e6-8647-b2fe5d164764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4686d8d-4cfc-427c-bb27-07d70caf4854",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "e6f72053-bbc7-4811-8b3e-088900e22f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "f7c76e29-b384-4618-88d4-0ad1859aa0af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f72053-bbc7-4811-8b3e-088900e22f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036c2a8f-e98a-4768-a583-88355137a364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f72053-bbc7-4811-8b3e-088900e22f34",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "da9ee341-c24e-4b17-b2d8-c1fb8286c5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "a01bd723-538a-4fc6-aa95-e1e4c06670f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da9ee341-c24e-4b17-b2d8-c1fb8286c5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8154bfca-50ec-464c-a312-7d58cc768c05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da9ee341-c24e-4b17-b2d8-c1fb8286c5db",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "2314020d-a705-4b31-b51f-521ad5bab88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "ac135316-f212-4ecb-b8cf-eaec43856767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2314020d-a705-4b31-b51f-521ad5bab88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5c77bc-92c8-4250-8c75-ab0c76dbcd63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2314020d-a705-4b31-b51f-521ad5bab88b",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "e0d1aa33-612f-491a-87e0-af8ad18b30ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "45df1673-6854-45f6-9128-b9d324953e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d1aa33-612f-491a-87e0-af8ad18b30ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32455afb-cac0-467a-ab90-8b1fc8db6fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d1aa33-612f-491a-87e0-af8ad18b30ca",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "c15809f6-6161-4375-aeef-d85fa0cec707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "d7b38618-d5f9-4687-aad1-b554b1c9f2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15809f6-6161-4375-aeef-d85fa0cec707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b555bf26-3e89-4314-8e71-1722b4caf940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15809f6-6161-4375-aeef-d85fa0cec707",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "9d2420bc-b00f-46ac-a219-beb54208112c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "85b90562-684c-48e9-a51e-27c7234501fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d2420bc-b00f-46ac-a219-beb54208112c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78fea7a4-a1df-4a4d-84de-f810f74f05a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d2420bc-b00f-46ac-a219-beb54208112c",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "b35943d4-a436-49db-8584-8cec893a7760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "13f542bc-2a71-4a15-b6f4-316c7b6d35f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b35943d4-a436-49db-8584-8cec893a7760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d142d099-8322-42ec-8a76-3386af3e506b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b35943d4-a436-49db-8584-8cec893a7760",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "977d716f-c78f-4530-98bc-73786ffb4f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "7d5022b9-4dd5-4dea-8e2f-dcc927607693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "977d716f-c78f-4530-98bc-73786ffb4f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d7dc15-7318-4bed-b881-c51c7fe999f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "977d716f-c78f-4530-98bc-73786ffb4f4c",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "2fd24164-bf60-411f-a42e-e9b98d6aff01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "95e01119-8889-440b-996a-44199dd4617b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd24164-bf60-411f-a42e-e9b98d6aff01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c93bf3cb-ed33-47c2-b885-4c5fa8c719bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd24164-bf60-411f-a42e-e9b98d6aff01",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "a9e9e766-9e2e-4630-be7b-af3fccb37033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "60b568e0-3303-494a-bfc1-32e35587e00d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e9e766-9e2e-4630-be7b-af3fccb37033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd590f2f-f11e-47c9-9472-4fbc82212134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e9e766-9e2e-4630-be7b-af3fccb37033",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "4235df3a-e93f-461f-be82-406d8d22b98d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "8074fea2-9886-42e6-894f-09db4ed20c64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4235df3a-e93f-461f-be82-406d8d22b98d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb447994-dcf1-4e67-b4a2-c8cba2780515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4235df3a-e93f-461f-be82-406d8d22b98d",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "b68608e8-f53d-42ce-8f08-d4551961e2f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "58ee991e-0878-4707-be94-340a66eca543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68608e8-f53d-42ce-8f08-d4551961e2f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d6fb827-46d6-46b8-af56-dfa8aeb4af01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68608e8-f53d-42ce-8f08-d4551961e2f9",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "5b7ee4a8-ad85-4c2b-80c7-6aaadd750705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "ef59fcbf-6053-4ed5-b48c-a2bc4f29a48f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7ee4a8-ad85-4c2b-80c7-6aaadd750705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1d51e2-4e43-4b5a-85b2-821253d48d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7ee4a8-ad85-4c2b-80c7-6aaadd750705",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "0adbb8d6-2dd8-4a02-becd-01b9a8682a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "766e4036-34a9-40b1-bcb4-02ea9358c39a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0adbb8d6-2dd8-4a02-becd-01b9a8682a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dd2524f-a3b0-4986-8020-1ed2ff870930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0adbb8d6-2dd8-4a02-becd-01b9a8682a66",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "ce915051-8827-42b3-91fd-c163f1253587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "e8d893f4-9c0c-4138-9c30-c1be82579147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce915051-8827-42b3-91fd-c163f1253587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5f23e02-4c91-447f-852a-f09394ab5068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce915051-8827-42b3-91fd-c163f1253587",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        },
        {
            "id": "12e28df8-0f7d-4c0b-a1a4-b5939c74fe5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "compositeImage": {
                "id": "48f9aa12-3f76-4918-ba7d-89b55c7f8485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e28df8-0f7d-4c0b-a1a4-b5939c74fe5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312caf21-44c0-4b12-ba53-fb731d5a1e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e28df8-0f7d-4c0b-a1a4-b5939c74fe5a",
                    "LayerId": "b5260675-c883-4f04-ba5c-e1064253da5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "b5260675-c883-4f04-ba5c-e1064253da5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 70
}