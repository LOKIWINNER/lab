{
    "id": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_attack_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 9,
    "bbox_right": 53,
    "bbox_top": 37,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92e6a954-4fdd-4ef6-9aa5-870c687d0a6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "d24fdb3c-1d24-41bd-97e8-6dfaa5532b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e6a954-4fdd-4ef6-9aa5-870c687d0a6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47daa847-cc33-4f07-b7eb-455de9ba5e7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e6a954-4fdd-4ef6-9aa5-870c687d0a6b",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "33c9b6ce-55e3-4a29-aa59-33d00994bab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "8e98b2df-236f-4477-ab04-08c78da2c268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c9b6ce-55e3-4a29-aa59-33d00994bab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3011bb9d-c9cf-4f91-b384-ad36789745e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c9b6ce-55e3-4a29-aa59-33d00994bab2",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "bc66960e-d8b9-446b-ba98-9f1e6eed4a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "13bff277-e08e-4a82-a1be-dd6feb2cb8a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc66960e-d8b9-446b-ba98-9f1e6eed4a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882ae7d7-92ac-4845-9474-985c57ba2b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc66960e-d8b9-446b-ba98-9f1e6eed4a59",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "150a73cc-b205-419c-805e-c87b0052b440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "4ecc3e71-4603-4453-9dd3-e6029a5921fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150a73cc-b205-419c-805e-c87b0052b440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e215f833-b9fb-430a-aeed-021e9d0d6bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150a73cc-b205-419c-805e-c87b0052b440",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "a7704e09-f0f1-4764-8159-483d973d912a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "5129a566-fe23-4fe7-b3fc-a571edd9a658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7704e09-f0f1-4764-8159-483d973d912a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c315c23c-6b02-4367-b6c3-f1ca72968e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7704e09-f0f1-4764-8159-483d973d912a",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "430641e9-2a15-421a-9cd2-b1e6699ea782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "2f1efc8d-78a6-427f-b1be-3e17afc92b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430641e9-2a15-421a-9cd2-b1e6699ea782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3524b4b5-4cc4-4fe3-b1ab-03cfa764831f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430641e9-2a15-421a-9cd2-b1e6699ea782",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "36302781-5dbd-4a21-86a0-a2c623e86003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "d827d90c-a284-4eca-b9a5-d39258e13f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36302781-5dbd-4a21-86a0-a2c623e86003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61953b1d-8353-433c-961e-2ef6290b0e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36302781-5dbd-4a21-86a0-a2c623e86003",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "fcbe8d75-d997-4ba3-9627-bd4dbb5eed3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "3ea3611a-2dda-455d-9a82-a31261f1d5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcbe8d75-d997-4ba3-9627-bd4dbb5eed3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6649cc52-7c9d-4c05-a9a8-cd25b17f9c35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcbe8d75-d997-4ba3-9627-bd4dbb5eed3f",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "e33e8afa-1030-4229-a76e-5f8be8433633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "5d6ffee2-59f5-473f-b867-1ca016275201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e33e8afa-1030-4229-a76e-5f8be8433633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e9e36f9-01b3-43c1-b85e-6182acb9cde5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e33e8afa-1030-4229-a76e-5f8be8433633",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "3254afbb-317b-4940-97a6-dc8c9b5857d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "e4856060-8ba5-42fc-9490-8e45970f8587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3254afbb-317b-4940-97a6-dc8c9b5857d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b735f66-8e47-4b2c-956c-aa8cfa839863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3254afbb-317b-4940-97a6-dc8c9b5857d4",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        },
        {
            "id": "f4b43df2-8a4a-498f-beae-8deb9918c681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "compositeImage": {
                "id": "0b217f68-7378-4571-8e72-b5f6d565c8ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b43df2-8a4a-498f-beae-8deb9918c681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed65c703-2b41-4750-b19f-bea897d799ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b43df2-8a4a-498f-beae-8deb9918c681",
                    "LayerId": "36a0c244-6be5-4b11-83de-6d9c99d0a056"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "36a0c244-6be5-4b11-83de-6d9c99d0a056",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6d8b4d0-53d7-4d1c-bd70-41d69209ca45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}