{
    "id": "eb4f5670-0593-418a-80f5-148b90ce082b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_shield_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81b2380f-d700-404d-baec-2731ad69e11b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb4f5670-0593-418a-80f5-148b90ce082b",
            "compositeImage": {
                "id": "cdccf3d5-376b-475d-bcfd-f7cf187a5a96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b2380f-d700-404d-baec-2731ad69e11b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31bac76f-4f7b-4ab4-be4a-397937767b74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b2380f-d700-404d-baec-2731ad69e11b",
                    "LayerId": "f0bac042-5b6a-4739-8f6d-69a679961992"
                }
            ]
        },
        {
            "id": "41755a94-b662-4e83-85fd-8f0386044f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb4f5670-0593-418a-80f5-148b90ce082b",
            "compositeImage": {
                "id": "b26ccba9-341a-4742-b56f-6394b084afe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41755a94-b662-4e83-85fd-8f0386044f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b0e015-646c-4414-ac31-e428c7bbaad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41755a94-b662-4e83-85fd-8f0386044f3c",
                    "LayerId": "f0bac042-5b6a-4739-8f6d-69a679961992"
                }
            ]
        },
        {
            "id": "419bac0e-3326-45ea-87f3-1a276d28b3fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb4f5670-0593-418a-80f5-148b90ce082b",
            "compositeImage": {
                "id": "454ac9a4-5899-4e2b-aefd-40f5ee0aac9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419bac0e-3326-45ea-87f3-1a276d28b3fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "759c3de0-0cf6-43f0-b00e-4784cf50fe83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419bac0e-3326-45ea-87f3-1a276d28b3fc",
                    "LayerId": "f0bac042-5b6a-4739-8f6d-69a679961992"
                }
            ]
        },
        {
            "id": "652a6667-9111-49b4-bd72-3fb2cc3f50a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb4f5670-0593-418a-80f5-148b90ce082b",
            "compositeImage": {
                "id": "1e4cacd4-c871-4660-8723-a9c7cec00a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652a6667-9111-49b4-bd72-3fb2cc3f50a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dda9ec96-9615-40b4-9af9-26d54b1d9664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652a6667-9111-49b4-bd72-3fb2cc3f50a2",
                    "LayerId": "f0bac042-5b6a-4739-8f6d-69a679961992"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f0bac042-5b6a-4739-8f6d-69a679961992",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb4f5670-0593-418a-80f5-148b90ce082b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}