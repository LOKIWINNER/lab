{
    "id": "760a2ec9-677a-4983-a8e6-96a75c4af669",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new_glass_spell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 354,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7521cc6f-9e7f-4e34-aa30-3f463ae693e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "760a2ec9-677a-4983-a8e6-96a75c4af669",
            "compositeImage": {
                "id": "00c00b60-ad44-49f4-948a-0fefc26a87aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7521cc6f-9e7f-4e34-aa30-3f463ae693e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bac7ce0-c71f-46f9-bad5-0bcaac88b836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7521cc6f-9e7f-4e34-aa30-3f463ae693e8",
                    "LayerId": "84c813b7-1f03-41e4-90d5-98e52e886d0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "84c813b7-1f03-41e4-90d5-98e52e886d0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "760a2ec9-677a-4983-a8e6-96a75c4af669",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 355,
    "xorig": 0,
    "yorig": 0
}