{
    "id": "29caebcd-08c3-402b-a94d-09728f5fcdaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite51",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 485,
    "bbox_left": 16,
    "bbox_right": 527,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a193398-2ba0-4149-9a83-1ac31ccbaa6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29caebcd-08c3-402b-a94d-09728f5fcdaf",
            "compositeImage": {
                "id": "410e3a24-f09a-481c-a26b-fc51be6c65ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a193398-2ba0-4149-9a83-1ac31ccbaa6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f77be97-9965-4bff-a4cc-6799465a8796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a193398-2ba0-4149-9a83-1ac31ccbaa6e",
                    "LayerId": "dd6fadf5-73d9-446c-bd60-2a7f2ea55587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 495,
    "layers": [
        {
            "id": "dd6fadf5-73d9-446c-bd60-2a7f2ea55587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29caebcd-08c3-402b-a94d-09728f5fcdaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 545,
    "xorig": 0,
    "yorig": 0
}