{
    "id": "f939d94f-7e2c-45f7-ae6d-5547c5f9037e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_attack_hold_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 45,
    "bbox_right": 92,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48c24056-4c0b-48a4-8733-74c52c5b44a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f939d94f-7e2c-45f7-ae6d-5547c5f9037e",
            "compositeImage": {
                "id": "9b159dcc-8914-4122-a64b-7e93e8c44396",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48c24056-4c0b-48a4-8733-74c52c5b44a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cad4577-196f-4d8c-ac21-84133903292d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c24056-4c0b-48a4-8733-74c52c5b44a1",
                    "LayerId": "7c6466d7-1984-4230-a731-8f4eb4f77c4f"
                }
            ]
        },
        {
            "id": "62150ab8-3558-4836-9e67-aefacde7887f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f939d94f-7e2c-45f7-ae6d-5547c5f9037e",
            "compositeImage": {
                "id": "dbac1106-16e6-4c38-8ad4-196916724661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62150ab8-3558-4836-9e67-aefacde7887f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "293028bc-ce6e-4c88-816b-53989a3b21c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62150ab8-3558-4836-9e67-aefacde7887f",
                    "LayerId": "7c6466d7-1984-4230-a731-8f4eb4f77c4f"
                }
            ]
        },
        {
            "id": "836626f5-1d01-4c23-a473-6a0ef7dab8e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f939d94f-7e2c-45f7-ae6d-5547c5f9037e",
            "compositeImage": {
                "id": "e682ff6d-f661-418c-85e6-e8584b5198ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "836626f5-1d01-4c23-a473-6a0ef7dab8e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af6de6d2-44f3-40ec-b8f5-9fe2a12d91e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "836626f5-1d01-4c23-a473-6a0ef7dab8e6",
                    "LayerId": "7c6466d7-1984-4230-a731-8f4eb4f77c4f"
                }
            ]
        },
        {
            "id": "ba24cdc0-2aba-4d56-aec1-d943b6c6a368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f939d94f-7e2c-45f7-ae6d-5547c5f9037e",
            "compositeImage": {
                "id": "683867d7-5d03-4515-915e-69e77a7399db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba24cdc0-2aba-4d56-aec1-d943b6c6a368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "114a62b2-748e-4331-ab85-ac7f477d9b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba24cdc0-2aba-4d56-aec1-d943b6c6a368",
                    "LayerId": "7c6466d7-1984-4230-a731-8f4eb4f77c4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 134,
    "layers": [
        {
            "id": "7c6466d7-1984-4230-a731-8f4eb4f77c4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f939d94f-7e2c-45f7-ae6d-5547c5f9037e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}