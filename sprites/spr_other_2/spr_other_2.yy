{
    "id": "8477ef2f-aea8-44de-b219-c067b1de8a74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_other_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f34248f7-8f98-4bcc-9c73-be0a032e0bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8477ef2f-aea8-44de-b219-c067b1de8a74",
            "compositeImage": {
                "id": "124b110a-87b2-4974-ad0f-622315935b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34248f7-8f98-4bcc-9c73-be0a032e0bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35aa7f7a-d2d4-4b0b-ae37-d4e196d21d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34248f7-8f98-4bcc-9c73-be0a032e0bb8",
                    "LayerId": "516098a8-3ba0-46d5-ae73-3543cb19066b"
                }
            ]
        },
        {
            "id": "73e200de-9269-4530-a925-03f15f837f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8477ef2f-aea8-44de-b219-c067b1de8a74",
            "compositeImage": {
                "id": "0d632155-2c59-45e5-8309-a78a6ae91d31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e200de-9269-4530-a925-03f15f837f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1430bde-137d-4147-82f6-0c5bc2d67416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e200de-9269-4530-a925-03f15f837f24",
                    "LayerId": "516098a8-3ba0-46d5-ae73-3543cb19066b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "516098a8-3ba0-46d5-ae73-3543cb19066b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8477ef2f-aea8-44de-b219-c067b1de8a74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}