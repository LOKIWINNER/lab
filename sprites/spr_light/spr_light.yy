{
    "id": "cb768503-0476-48e4-b16a-4c16040dbcee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b88bf32-0107-42be-a0c0-11ecf943af4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb768503-0476-48e4-b16a-4c16040dbcee",
            "compositeImage": {
                "id": "446ded6b-14d0-48ab-95d6-ecd19a4ae2d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b88bf32-0107-42be-a0c0-11ecf943af4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7769b199-a27f-45d8-912c-6966b9d0b5d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b88bf32-0107-42be-a0c0-11ecf943af4a",
                    "LayerId": "9c00536a-639b-4159-b18b-d511780a6d4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "9c00536a-639b-4159-b18b-d511780a6d4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb768503-0476-48e4-b16a-4c16040dbcee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 200,
    "yorig": 200
}