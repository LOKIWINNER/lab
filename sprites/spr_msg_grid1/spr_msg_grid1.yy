{
    "id": "b0d86c2d-399f-4908-9bf5-83c7428fb10f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f25dc73e-93b0-4b14-b160-3d7a6daf9abb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d86c2d-399f-4908-9bf5-83c7428fb10f",
            "compositeImage": {
                "id": "bf014a77-de66-41c0-aa29-7dee7dcfbf04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25dc73e-93b0-4b14-b160-3d7a6daf9abb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23d6ddd5-29fa-4271-8691-7662efc3c370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25dc73e-93b0-4b14-b160-3d7a6daf9abb",
                    "LayerId": "6b7b3c63-1ccd-481f-af71-5a6a97c15182"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6b7b3c63-1ccd-481f-af71-5a6a97c15182",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d86c2d-399f-4908-9bf5-83c7428fb10f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}