{
    "id": "f743e21d-4e05-450a-b9ed-4f867008cf39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWalls1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef67ac7b-8d72-45b6-acb8-be9cdd7d9b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "697a0381-bf07-4277-8157-516f195b6a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef67ac7b-8d72-45b6-acb8-be9cdd7d9b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a18f8f7-46e5-4e6f-8aae-6db5aeb6d704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef67ac7b-8d72-45b6-acb8-be9cdd7d9b8e",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "51eade5c-f427-436b-84cb-32af79bb935f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "af8bf970-091c-4cdc-9dea-0a5500437448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51eade5c-f427-436b-84cb-32af79bb935f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc92396-c6dd-4cbf-ac32-9e446a7f2503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51eade5c-f427-436b-84cb-32af79bb935f",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "df219456-c6ae-4297-8ddc-d189ad4b4bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "66391d3b-4b0c-4d32-891c-15a47863115f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df219456-c6ae-4297-8ddc-d189ad4b4bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6141a6ac-8ac7-4a92-8fe9-9f78963725c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df219456-c6ae-4297-8ddc-d189ad4b4bfd",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "2e7732e4-9742-4a1e-961c-bcb1147e1f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "493b5de4-33e3-41eb-bee4-1c5a8adc26b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e7732e4-9742-4a1e-961c-bcb1147e1f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cf6a4b-a2e1-4f16-82a0-e069f5cb6988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e7732e4-9742-4a1e-961c-bcb1147e1f66",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "b0dd7c4b-a3f7-40b3-a8d7-7f1a5f913bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "4c0265f5-8662-4016-969a-d59a08c8b0d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dd7c4b-a3f7-40b3-a8d7-7f1a5f913bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0360a02b-8a88-4f14-be95-ed5bfd8c7d41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dd7c4b-a3f7-40b3-a8d7-7f1a5f913bd8",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "37b3acd8-cf50-4879-a02e-9cf38ca562c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "b6f7177f-e553-4af7-b56a-66790746fced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b3acd8-cf50-4879-a02e-9cf38ca562c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba9f9be-fb65-42e7-a441-0185f0295367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b3acd8-cf50-4879-a02e-9cf38ca562c2",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "835b6abd-0d99-4b18-9b87-1a3f034cf920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "b77073b3-c1c5-45ae-9863-80f2809d1649",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835b6abd-0d99-4b18-9b87-1a3f034cf920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2511151f-c556-455c-ae21-b2b7ed261e27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835b6abd-0d99-4b18-9b87-1a3f034cf920",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "41aeedae-6b9b-4e9d-85ef-ee3d4ff5a106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "dcb71fe0-47e7-4a99-8395-bb0e9db71dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41aeedae-6b9b-4e9d-85ef-ee3d4ff5a106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5414642e-475f-4885-b1dc-9395b477898c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41aeedae-6b9b-4e9d-85ef-ee3d4ff5a106",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "625b152a-dc43-4923-a072-5e54fa72f3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "f5a4d880-7293-4b63-97bf-9719ceb41e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "625b152a-dc43-4923-a072-5e54fa72f3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ffe8db-c5eb-4b9e-8900-60f1d8a222ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "625b152a-dc43-4923-a072-5e54fa72f3e0",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "36426046-03ef-45c9-bdfb-b40db611b8dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "edf97c13-7aa0-46cb-89ba-cea950c8a27f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36426046-03ef-45c9-bdfb-b40db611b8dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7a21e9f-7e30-4675-bad3-15f70e0bbea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36426046-03ef-45c9-bdfb-b40db611b8dc",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "04f7ff79-1308-4259-9168-2ed0d8012d5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "f8fd131f-7852-4252-bab2-87bdf882c9eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f7ff79-1308-4259-9168-2ed0d8012d5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36ee7ccf-bd15-45c5-aac1-04d02b7d28d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f7ff79-1308-4259-9168-2ed0d8012d5f",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "44d475ed-6bfe-410d-baeb-418473725086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "2b086d41-be57-4e1b-ae03-6e61bdcbc98f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d475ed-6bfe-410d-baeb-418473725086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece235c9-64e3-4e93-aa92-8c1e879e89ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d475ed-6bfe-410d-baeb-418473725086",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "df030dad-e1e2-4951-a229-fa5d37d428f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "ad9790ff-814d-400d-9791-704c4e04cc4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df030dad-e1e2-4951-a229-fa5d37d428f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f953bb-5106-4970-85a8-5353c7c5c51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df030dad-e1e2-4951-a229-fa5d37d428f9",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "6a5bc443-6051-46fa-a88f-1802cfd54a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "558793cc-fa98-4e35-81ff-e025507f0e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5bc443-6051-46fa-a88f-1802cfd54a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2730ae5d-7016-4662-b0f1-0efae9731da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5bc443-6051-46fa-a88f-1802cfd54a33",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "4a4b7993-e8bf-40a2-86f3-ba35ec6b25a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "fa395ada-888f-462d-a26b-482b97443f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4b7993-e8bf-40a2-86f3-ba35ec6b25a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91fc612d-dffc-4840-a76a-b9da24a37ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4b7993-e8bf-40a2-86f3-ba35ec6b25a6",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "0d088a63-7ab3-481f-aebb-144c93a8c794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "857c0dfd-06de-4426-8c41-f026ba44a555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d088a63-7ab3-481f-aebb-144c93a8c794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "000d3e0c-6474-4ca4-aea6-465dff36bfee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d088a63-7ab3-481f-aebb-144c93a8c794",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "2002f2c9-3071-4b32-8835-8507cafab106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "25b497c7-b7e0-4a6e-8747-cdc5fe3be2f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2002f2c9-3071-4b32-8835-8507cafab106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c0dfa2e-db90-4eae-86d8-595bf3831362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2002f2c9-3071-4b32-8835-8507cafab106",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "7ed59755-a3a4-40b1-8acd-4b7177df1980",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "554451c0-2cbf-42cb-8a7f-d131a52e7232",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ed59755-a3a4-40b1-8acd-4b7177df1980",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce89b76-4ded-4771-8b9c-3f496a31dc54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ed59755-a3a4-40b1-8acd-4b7177df1980",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "9a4f189d-e0c9-40ab-8d9b-6180a87c5839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "b0f642db-f824-4663-bfc3-5d4fe12a9c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a4f189d-e0c9-40ab-8d9b-6180a87c5839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f79769-c899-44e9-a405-186aebc63349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a4f189d-e0c9-40ab-8d9b-6180a87c5839",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        },
        {
            "id": "b903b2cc-e439-4b27-acc8-ae107064c6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "compositeImage": {
                "id": "f121ffbb-bad3-482c-be26-b0910e5a9bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b903b2cc-e439-4b27-acc8-ae107064c6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c47d3545-861f-492c-9fd0-ddae6555ea98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b903b2cc-e439-4b27-acc8-ae107064c6c7",
                    "LayerId": "d133238f-0988-457d-8ee1-d61a180acb7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d133238f-0988-457d-8ee1-d61a180acb7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f743e21d-4e05-450a-b9ed-4f867008cf39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}