{
    "id": "c43764b6-c49c-4c47-b583-b6ead5540957",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b809cfeb-4780-4e94-98f8-4f2ce4f22da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "6a087acf-48f0-4cfe-9ee9-baef74bff5d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b809cfeb-4780-4e94-98f8-4f2ce4f22da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08271f72-dac8-497b-863d-047cbc83660d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b809cfeb-4780-4e94-98f8-4f2ce4f22da5",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "c8743c4f-4dbc-42c2-aced-89e88ee126ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "8255180c-77dd-4057-ab09-f843b16fb256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8743c4f-4dbc-42c2-aced-89e88ee126ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da9273d1-dbdf-4ff2-8475-f8393d56dea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8743c4f-4dbc-42c2-aced-89e88ee126ef",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "b6968c38-89a7-42bf-860b-652e58d7a05b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "4a5ba7c7-6221-495c-a985-3c92cae708c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6968c38-89a7-42bf-860b-652e58d7a05b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e071aa89-eef8-41a9-bbc4-af7929f53686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6968c38-89a7-42bf-860b-652e58d7a05b",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "8b621159-2dc2-4b84-8d12-b5cd0a9eec61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "10093137-ea7f-4733-89bb-9d09d8944efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b621159-2dc2-4b84-8d12-b5cd0a9eec61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf8a3d25-410b-4808-b6bc-0aa182a86248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b621159-2dc2-4b84-8d12-b5cd0a9eec61",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "da04cf6d-a051-4f13-be3d-2e7589027c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "c071840c-73bd-4e05-aa56-9ba7f7206c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da04cf6d-a051-4f13-be3d-2e7589027c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a097cd-ee40-480f-8ba7-d41d5b6ffe21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da04cf6d-a051-4f13-be3d-2e7589027c81",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "9ec10325-d637-47f3-94eb-21a4ba23967e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "07d48dd1-8d43-4105-8065-8f1ef16acd7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec10325-d637-47f3-94eb-21a4ba23967e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e381b84-823d-4e0d-8da7-69347846eddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec10325-d637-47f3-94eb-21a4ba23967e",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "94734fde-ef99-4894-b035-b79056889ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "5ab95301-b64b-475d-912e-4544e1f9580f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94734fde-ef99-4894-b035-b79056889ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906c884e-88f3-4457-8450-afdac26a7e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94734fde-ef99-4894-b035-b79056889ce5",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "241966b4-e4fb-4e4d-aa61-0665a48b50d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "c3737701-addf-4f24-8151-c13ce43ff946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241966b4-e4fb-4e4d-aa61-0665a48b50d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e0438b3-9380-47d5-b5be-f23a173b5592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241966b4-e4fb-4e4d-aa61-0665a48b50d7",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "5460195a-445a-424d-8ab4-40a96e08cb3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "68c1eb87-db37-4bc2-9d1b-1e6c6242e2e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5460195a-445a-424d-8ab4-40a96e08cb3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "765e80d1-17ce-45c8-9b85-99a95c6bd58a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5460195a-445a-424d-8ab4-40a96e08cb3e",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        },
        {
            "id": "5ee75de7-bead-4bc4-889b-82829d0772e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "compositeImage": {
                "id": "cd0d7541-80e0-4ec4-87a6-d65efd601c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee75de7-bead-4bc4-889b-82829d0772e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "824f14ae-b071-4c73-959e-fd5440f53523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee75de7-bead-4bc4-889b-82829d0772e8",
                    "LayerId": "92b4b558-4403-4ca7-b309-31453cc0c48e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "92b4b558-4403-4ca7-b309-31453cc0c48e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c43764b6-c49c-4c47-b583-b6ead5540957",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}