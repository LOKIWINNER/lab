{
    "id": "db61d05a-275d-4079-af94-3a350b88c06d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_attack_hold_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 12,
    "bbox_right": 45,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c79cc88-6588-4f28-bad5-39a83d6144bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db61d05a-275d-4079-af94-3a350b88c06d",
            "compositeImage": {
                "id": "0d4ca046-e42d-4544-ba3c-c35b35685ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c79cc88-6588-4f28-bad5-39a83d6144bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1619a9a7-fdd4-4659-b4eb-e05d53849c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c79cc88-6588-4f28-bad5-39a83d6144bc",
                    "LayerId": "d9545efb-74af-41f9-8b62-ed20e977a1a4"
                }
            ]
        },
        {
            "id": "0fd11b7d-a3e7-4ac2-8722-39ee60a234b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db61d05a-275d-4079-af94-3a350b88c06d",
            "compositeImage": {
                "id": "63ae795e-29d8-4b6a-9c21-9f8c5aa569f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd11b7d-a3e7-4ac2-8722-39ee60a234b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f1a8fd-5480-43b8-82fd-138b6a46c253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd11b7d-a3e7-4ac2-8722-39ee60a234b5",
                    "LayerId": "d9545efb-74af-41f9-8b62-ed20e977a1a4"
                }
            ]
        },
        {
            "id": "88a0a013-8d78-4444-9ede-31fb7c662a61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db61d05a-275d-4079-af94-3a350b88c06d",
            "compositeImage": {
                "id": "282c450c-a759-4cc5-a3ae-2d0169380bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a0a013-8d78-4444-9ede-31fb7c662a61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e790adf-d02a-4777-aa6c-e9615b89a19c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a0a013-8d78-4444-9ede-31fb7c662a61",
                    "LayerId": "d9545efb-74af-41f9-8b62-ed20e977a1a4"
                }
            ]
        },
        {
            "id": "8dbac3ab-0226-4589-b50c-c976cd82d179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db61d05a-275d-4079-af94-3a350b88c06d",
            "compositeImage": {
                "id": "4385f08d-b17e-480a-bed1-6747a7453d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dbac3ab-0226-4589-b50c-c976cd82d179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd5b2525-9f58-42d2-b43d-2ef2164c0bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dbac3ab-0226-4589-b50c-c976cd82d179",
                    "LayerId": "d9545efb-74af-41f9-8b62-ed20e977a1a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d9545efb-74af-41f9-8b62-ed20e977a1a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db61d05a-275d-4079-af94-3a350b88c06d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 34,
    "yorig": 36
}