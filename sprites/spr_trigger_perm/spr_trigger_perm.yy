{
    "id": "ac4952a7-0700-48d1-8ce8-956d71503cb4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trigger_perm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d532ed5e-6fd2-4e49-99ca-e913f7c68b84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac4952a7-0700-48d1-8ce8-956d71503cb4",
            "compositeImage": {
                "id": "f6c866d1-7883-4d3a-9a11-3c6420bd7751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d532ed5e-6fd2-4e49-99ca-e913f7c68b84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb2a4bc-461d-4b28-8138-57e45a591f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d532ed5e-6fd2-4e49-99ca-e913f7c68b84",
                    "LayerId": "bf1a4814-27f1-4e6e-a5b6-eea153ac4228"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bf1a4814-27f1-4e6e-a5b6-eea153ac4228",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac4952a7-0700-48d1-8ce8-956d71503cb4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}