{
    "id": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 13,
    "bbox_right": 19,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "940730f0-1fdb-42e7-bbe1-5f5e176099e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "8a675db5-1742-48ed-8683-77445b32fca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940730f0-1fdb-42e7-bbe1-5f5e176099e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f72fe098-9842-4195-ad6a-d1838f830860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940730f0-1fdb-42e7-bbe1-5f5e176099e6",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "c0c03d2e-db9a-4317-a836-aebaa987f9c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "e71a7d12-408f-47f8-bcc5-8552921b4216",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c03d2e-db9a-4317-a836-aebaa987f9c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e504c36f-1058-4d46-b747-9edcabe159a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c03d2e-db9a-4317-a836-aebaa987f9c7",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "c956a92f-f742-4a91-b575-fcd65482b25d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "99ea6881-b2be-48fb-87ea-d271df59f94a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c956a92f-f742-4a91-b575-fcd65482b25d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e68cd4-2048-4f1e-98f0-69adbf12fc23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c956a92f-f742-4a91-b575-fcd65482b25d",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "96ca5b44-3bbe-44a8-a164-913fc71c7b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "7ac3ef44-bd9e-4095-9ec5-37c2502ad850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ca5b44-3bbe-44a8-a164-913fc71c7b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b593bf72-e3ed-4347-b5bb-36cf9946b144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ca5b44-3bbe-44a8-a164-913fc71c7b42",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "82b8fccb-15d0-4186-9c34-f3f1cf0fb20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "8e533b83-1b2f-4beb-a579-e34ed9c7d0e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b8fccb-15d0-4186-9c34-f3f1cf0fb20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc982312-d46e-46a0-9617-5d328924a49d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b8fccb-15d0-4186-9c34-f3f1cf0fb20a",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "85bc1c53-57e1-430a-9bab-abb411d2b2f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "8c30153b-9ec3-4c09-a95e-80f34e282db2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bc1c53-57e1-430a-9bab-abb411d2b2f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be681b9-b94e-4285-8836-0fdb5bf1085c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bc1c53-57e1-430a-9bab-abb411d2b2f0",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "443b1d5a-57af-4351-be7c-c3b39b270585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "02856804-6a07-4399-8b34-b09f7c32b0a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "443b1d5a-57af-4351-be7c-c3b39b270585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2679dc-4dcd-4396-9348-2bc32790a0f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "443b1d5a-57af-4351-be7c-c3b39b270585",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        },
        {
            "id": "96c788cc-a7e3-40ee-bde9-fcb56cb57f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "compositeImage": {
                "id": "9883efe7-ee59-48da-a36e-d0e4db29434f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c788cc-a7e3-40ee-bde9-fcb56cb57f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "138cd0f7-2761-4057-a92a-de01571b6e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c788cc-a7e3-40ee-bde9-fcb56cb57f1d",
                    "LayerId": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52bc57d1-fe25-41ed-b8be-fa5e68bbe6ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}