{
    "id": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 61,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39987145-a34a-46c6-bfa5-59986abab283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "eb995320-8053-4d06-8f77-0ec3d77f7c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39987145-a34a-46c6-bfa5-59986abab283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f3dc4a4-c45e-4b4c-9559-3b2c7a7aa33c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39987145-a34a-46c6-bfa5-59986abab283",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "2192c545-54ce-4524-8feb-da92bbf829cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "6dbd8fc4-3aa7-41d2-9d6a-345221837a38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2192c545-54ce-4524-8feb-da92bbf829cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42ff3f60-92ce-4582-8252-78b288cdf3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2192c545-54ce-4524-8feb-da92bbf829cc",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "e0e1e7c6-b592-4763-adc9-4cfa85d698c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "2559802d-9ccf-4096-8fde-641526a31b5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e1e7c6-b592-4763-adc9-4cfa85d698c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e372c480-9797-428e-b0d3-906b5f97997d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e1e7c6-b592-4763-adc9-4cfa85d698c0",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "6882bc18-e068-4161-bf3a-7a522d8c226e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "89278498-05c5-4829-a19f-0c20fb19ed2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6882bc18-e068-4161-bf3a-7a522d8c226e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "551b8daa-a9a9-4551-a32d-05380f841e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6882bc18-e068-4161-bf3a-7a522d8c226e",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "4d33c0d3-3918-4c09-bdbb-44e25230c4d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "89ec8e7e-b95e-469a-97bb-2880837e94d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d33c0d3-3918-4c09-bdbb-44e25230c4d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15ec40a-1963-42f9-89ff-78a1c5fd6b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d33c0d3-3918-4c09-bdbb-44e25230c4d2",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "c15bedea-45b4-45e5-8379-d604ba8d1962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "687cf9df-9299-454b-9e8a-0e9598705992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c15bedea-45b4-45e5-8379-d604ba8d1962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7abda5ed-a684-4b8f-b0e3-78c515b67be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c15bedea-45b4-45e5-8379-d604ba8d1962",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "00f71c7c-2c58-4cbf-a502-ecc0331abd19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "486e1292-db14-4578-a880-421d30f7bb2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f71c7c-2c58-4cbf-a502-ecc0331abd19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49265790-4746-410d-908e-5e0e72905474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f71c7c-2c58-4cbf-a502-ecc0331abd19",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        },
        {
            "id": "6b50892c-a002-415a-85ea-6782df17250c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "compositeImage": {
                "id": "eeb81ac8-7692-4414-b8c3-03266a30801b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b50892c-a002-415a-85ea-6782df17250c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73edbd50-1a4a-44de-b16e-15c2f0469630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b50892c-a002-415a-85ea-6782df17250c",
                    "LayerId": "42527a74-5ae3-4ab0-ad1d-488e64856a33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "42527a74-5ae3-4ab0-ad1d-488e64856a33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae1e6b49-552c-489e-8e8f-98789c29c5f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}