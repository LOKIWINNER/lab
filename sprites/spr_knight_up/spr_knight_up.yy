{
    "id": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 17,
    "bbox_right": 102,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f53a5d16-5159-4e2a-845c-0017374cc558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "4c280b75-ab23-48ee-8502-7baa7cc3f1bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53a5d16-5159-4e2a-845c-0017374cc558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b07570a8-cca2-423b-96e4-b5b7d91c97ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53a5d16-5159-4e2a-845c-0017374cc558",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "80ee4e51-be56-476a-98f2-61020d2626de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "d938c235-076c-4492-a877-d1a6c619c4c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80ee4e51-be56-476a-98f2-61020d2626de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b20306-34c1-4dc8-8124-d7f72db33b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80ee4e51-be56-476a-98f2-61020d2626de",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c9e2c2a8-5b66-4d6b-a798-b907f0bc83be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "e879747f-834f-4378-a9fa-8cef15d19912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e2c2a8-5b66-4d6b-a798-b907f0bc83be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0abef057-bd40-4e08-86c6-9c4419b38276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e2c2a8-5b66-4d6b-a798-b907f0bc83be",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "e015dfac-245a-4d6a-ac1b-90f4f678e386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "dbc25c6c-c76f-45b7-aabd-fb0602991712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e015dfac-245a-4d6a-ac1b-90f4f678e386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6159faf3-6568-4a67-acd0-25ec16e9196b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e015dfac-245a-4d6a-ac1b-90f4f678e386",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "8c8a4f8e-9f7b-4c04-9dca-f227b2dbab05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "69005f42-8056-44e0-929a-1e2594495ccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8a4f8e-9f7b-4c04-9dca-f227b2dbab05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e566ea3-c684-4a71-937a-c1ced5f1b2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8a4f8e-9f7b-4c04-9dca-f227b2dbab05",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "daf7944e-319a-4196-a65d-36098c57d45d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "e17da61d-3e50-461d-89a1-9600045c35ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf7944e-319a-4196-a65d-36098c57d45d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc09e319-e83c-434e-a665-2d6622e332a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf7944e-319a-4196-a65d-36098c57d45d",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "e5a1c33f-c9fa-4027-b527-15214c7c1fa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "38d189ad-1498-4f9a-a8aa-84ccd069054b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a1c33f-c9fa-4027-b527-15214c7c1fa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d09abc13-9b11-46fb-9770-09da94903ddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a1c33f-c9fa-4027-b527-15214c7c1fa1",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "b768f609-0d43-44c3-b591-be80ff38cf1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "37f9c9fc-4402-46f6-906a-08cc67eacad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b768f609-0d43-44c3-b591-be80ff38cf1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23515268-2007-4ba8-b673-27d97439fa6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b768f609-0d43-44c3-b591-be80ff38cf1d",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ccb3446d-fc3d-4053-bdeb-e129b3a90656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "cc9415ff-5652-493c-98dc-a3acd9fd7435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccb3446d-fc3d-4053-bdeb-e129b3a90656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92d808f-e9a0-4928-a2d1-03cf2e7ff7bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccb3446d-fc3d-4053-bdeb-e129b3a90656",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "2aa0058e-28f7-4aa4-a944-68a297538937",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "27860930-43ca-4b62-a0f7-17b67e269462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa0058e-28f7-4aa4-a944-68a297538937",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97ac415e-7461-467f-aaa6-b4e336a4cb73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa0058e-28f7-4aa4-a944-68a297538937",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "a17da8f7-ae1d-46dd-af78-5e32ab53d8b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "a0682d51-dc36-460c-a10b-c3bfcbbe0066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a17da8f7-ae1d-46dd-af78-5e32ab53d8b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9250c6-d12a-4c2e-8977-503a1e46c495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a17da8f7-ae1d-46dd-af78-5e32ab53d8b5",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "3b018fd8-28c9-4167-a769-b33137ed9185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "be0ea15b-e919-49b0-a518-87a999168493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b018fd8-28c9-4167-a769-b33137ed9185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34ecc5a2-e0a1-40c2-962b-3c769187b054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b018fd8-28c9-4167-a769-b33137ed9185",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "3f0a3467-40b4-4a15-9e76-d33b5ed9b17d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "903bc4c3-c252-4db2-bd43-8c42280cf9cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f0a3467-40b4-4a15-9e76-d33b5ed9b17d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "322acc8e-972a-4c63-aa84-43dbcc5cce50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f0a3467-40b4-4a15-9e76-d33b5ed9b17d",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "2c9f1c79-fc09-4f3f-8fea-715f7856458a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "ad77d3f1-c45b-4861-90a2-d46f07ac3050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c9f1c79-fc09-4f3f-8fea-715f7856458a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a69418-25d0-4f5d-8881-0cfb9472b194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c9f1c79-fc09-4f3f-8fea-715f7856458a",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "be0e921d-05f6-499b-b8a7-2482ef81ddf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "a7ef3d4e-7680-4e27-863d-0829668226c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be0e921d-05f6-499b-b8a7-2482ef81ddf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b15eeb0-24ca-4cbe-9357-25e19bca3037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be0e921d-05f6-499b-b8a7-2482ef81ddf3",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c957d5c7-e3e9-4840-9f7b-a6f5033b23bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "118529be-3bb7-45e9-9426-301677fed719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c957d5c7-e3e9-4840-9f7b-a6f5033b23bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "628db76f-9a27-4f16-b117-df21d0f63278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c957d5c7-e3e9-4840-9f7b-a6f5033b23bc",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "7613c453-7904-49e1-88eb-87082764a040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "6c95cac5-017d-44f8-b82e-201df45f91cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7613c453-7904-49e1-88eb-87082764a040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf99adb8-a7f6-46f7-a05f-1c8ed379b9eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7613c453-7904-49e1-88eb-87082764a040",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "23d2d8ce-8b97-471e-9724-96578be96b62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "df62ccd0-5ae8-4388-a793-be9ff8246a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d2d8ce-8b97-471e-9724-96578be96b62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3bd9be-f95a-4d23-a145-7574bfe6c750",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d2d8ce-8b97-471e-9724-96578be96b62",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "3e86e369-aea3-4d02-b047-dae8e5ffd72b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "25fcf61c-1e69-45bd-a7dd-c9ce711ad030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e86e369-aea3-4d02-b047-dae8e5ffd72b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "629b8bc6-c508-47c6-a8e3-606e91332702",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e86e369-aea3-4d02-b047-dae8e5ffd72b",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "8a8132ba-fd08-40a8-98eb-0be86d626ce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "1d25610d-5901-4d34-bc84-26d2195b68ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a8132ba-fd08-40a8-98eb-0be86d626ce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "377abcad-8a78-40eb-82f5-8da6c8d14669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a8132ba-fd08-40a8-98eb-0be86d626ce0",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "582cbd6c-bdef-4a1e-904c-4e4d77b42355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "6ef2d8c3-abca-4bed-938c-66c792c06d97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582cbd6c-bdef-4a1e-904c-4e4d77b42355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b95a4d6-6812-428d-8b46-af0b294eddec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582cbd6c-bdef-4a1e-904c-4e4d77b42355",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "7251d445-a7a9-482c-8c11-cb8e137e75d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "b0566054-4c53-4e6e-8eb7-631a201e1e0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7251d445-a7a9-482c-8c11-cb8e137e75d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1429908a-e5ff-4d74-b6a3-60b21a144717",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7251d445-a7a9-482c-8c11-cb8e137e75d3",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "a79bf2a9-2bea-4164-87ac-50a4bece21f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "c9566e74-0d16-4f9c-9319-6db6e2c6a142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79bf2a9-2bea-4164-87ac-50a4bece21f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93d7017c-1420-4ce8-ac5e-1d8097667d46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79bf2a9-2bea-4164-87ac-50a4bece21f6",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6147fca1-6dea-401c-b8fc-32e07f86b9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "3d8ddcc9-26d9-4e8d-ab9e-41d220f868ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6147fca1-6dea-401c-b8fc-32e07f86b9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "076d38ca-0aee-41a0-bf9d-a0213b8c4c51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6147fca1-6dea-401c-b8fc-32e07f86b9cc",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "2e34de1d-65f3-46bb-937d-79b70669021f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "cd07e1b4-f9dc-4046-8678-602abea9dd38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e34de1d-65f3-46bb-937d-79b70669021f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1446c859-64b8-46fd-97df-cfc64736c2e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e34de1d-65f3-46bb-937d-79b70669021f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "db101c43-df9f-4f7f-ae42-0813d497bfd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "68222f78-0e7a-4954-9576-771d55f6fb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db101c43-df9f-4f7f-ae42-0813d497bfd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c89f5293-6d39-46a5-b323-a8be37d0f17b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db101c43-df9f-4f7f-ae42-0813d497bfd9",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "1aba6e13-d44a-406b-b336-c79428b0255c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "16ab1c0e-c5ff-4a1a-ba5b-04b3e6f202cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aba6e13-d44a-406b-b336-c79428b0255c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0fc2987-c6a4-41b8-a920-de8578f0e020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aba6e13-d44a-406b-b336-c79428b0255c",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "0f4d734b-ead0-4211-b9bd-4534ce2805f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "b4be5de1-3a38-450a-b817-c0da9eff6212",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f4d734b-ead0-4211-b9bd-4534ce2805f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d87cf1a-8bb7-429a-b52c-9a7ec467223c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f4d734b-ead0-4211-b9bd-4534ce2805f0",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ab220b09-fb88-4c97-9403-0eed390005d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "33b7d673-a299-42ee-b12c-44412d8d5951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab220b09-fb88-4c97-9403-0eed390005d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167e8aa6-b0b9-4ef7-a2f4-0ebe04160cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab220b09-fb88-4c97-9403-0eed390005d0",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "d7e40f5b-2859-41d6-a2e9-0b261ccd40c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "69dcf52e-3be9-4c27-86bd-9de8b3a93d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7e40f5b-2859-41d6-a2e9-0b261ccd40c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c42fde9-ccce-4ce4-8392-f4f7723da15f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e40f5b-2859-41d6-a2e9-0b261ccd40c7",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "1704671e-d7cd-42c5-8b82-a2192209abea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "368c7560-0fed-471e-85af-14d7a2740137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1704671e-d7cd-42c5-8b82-a2192209abea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f87eb7-84ef-4b92-aeb6-c31530f6b43d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1704671e-d7cd-42c5-8b82-a2192209abea",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ffddc0e3-718c-4297-9fed-86d8258dec67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "152307cc-3e8a-443d-850c-e68bfdf39b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffddc0e3-718c-4297-9fed-86d8258dec67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "957ef87a-412b-4c9d-a66c-19c086d2725d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffddc0e3-718c-4297-9fed-86d8258dec67",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6b150d60-5d4f-4744-b8cf-146217cded5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "99567a43-0aa5-42b9-8d53-152f0c0746df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b150d60-5d4f-4744-b8cf-146217cded5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e28ebc-d0e2-4eb6-ac29-6863b5636d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b150d60-5d4f-4744-b8cf-146217cded5c",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "11a98b50-2b4b-4dc9-957f-20421ada4bde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "038483f7-b593-4d08-99ab-3ff49474158a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a98b50-2b4b-4dc9-957f-20421ada4bde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e18e09-3bd9-4f30-8464-dae326701a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a98b50-2b4b-4dc9-957f-20421ada4bde",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "b0fa3b1d-daec-4411-aeef-7cae69d7d8e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "6a99f89b-cb70-4f3a-a589-4e7c5fd92064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0fa3b1d-daec-4411-aeef-7cae69d7d8e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569d4b56-78a5-4089-872c-7578558dbabf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0fa3b1d-daec-4411-aeef-7cae69d7d8e5",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "baa427e4-5fd0-474a-a02f-320e7d2162d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "062283c2-9916-4ad5-b6d5-a21ab9a7eb64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa427e4-5fd0-474a-a02f-320e7d2162d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dcfe819-42ce-4cb7-bf2f-ab08a9b2b98e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa427e4-5fd0-474a-a02f-320e7d2162d4",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "7dc78d4b-c5a9-4c07-9aa9-783d4a7237af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "9066ddd2-a9a3-4077-9458-281220978b9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc78d4b-c5a9-4c07-9aa9-783d4a7237af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6552154-2a5d-40be-b20e-3873a391ef26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc78d4b-c5a9-4c07-9aa9-783d4a7237af",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "f5a6ca28-c07b-49b8-a7ed-75ffebcff958",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "49e7cc82-6738-4980-aeaa-4b7036df66d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5a6ca28-c07b-49b8-a7ed-75ffebcff958",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f804aae-c9d3-40fc-8a59-58978fbd6772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5a6ca28-c07b-49b8-a7ed-75ffebcff958",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6d1ee244-a7a7-4aae-b1e9-7ae0a97d21a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "62b76626-f91e-4966-aa2a-c7fa37b066d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d1ee244-a7a7-4aae-b1e9-7ae0a97d21a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b02dee13-1473-47fc-ad30-f40d6819a4ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d1ee244-a7a7-4aae-b1e9-7ae0a97d21a7",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ac36dcab-6cb8-4369-953b-56f857611cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "ae6831b7-d0aa-466c-8ac4-86b2c7c8e4df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac36dcab-6cb8-4369-953b-56f857611cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "579b71c3-2317-4941-9bcc-4563c72d3f02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac36dcab-6cb8-4369-953b-56f857611cf5",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "3486c6cb-ca01-457b-b1da-94391d6896cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "12a2b3af-c6fc-445e-a08b-704432fe43bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3486c6cb-ca01-457b-b1da-94391d6896cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b088d7f4-385a-4165-951d-aeff0a92badd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3486c6cb-ca01-457b-b1da-94391d6896cf",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "abb3c464-c477-491c-a05e-80363805628a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "395cb43d-6b5c-4e90-bb7d-32c7b3ef8590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb3c464-c477-491c-a05e-80363805628a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be46c251-9a78-444a-9425-f560c742b553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb3c464-c477-491c-a05e-80363805628a",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "4f5e4681-eae1-4760-9aab-2d6798be928e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "c6e86784-16d3-418b-9eb4-1b24451deba8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f5e4681-eae1-4760-9aab-2d6798be928e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c06e6d-3443-4fcd-9569-84ea7d4d5447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f5e4681-eae1-4760-9aab-2d6798be928e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ca0b04d9-a99d-4dd6-ba73-6bac54672c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "fd72f696-8e4c-456a-9e89-81294dd4c7e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca0b04d9-a99d-4dd6-ba73-6bac54672c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "286011a9-19c5-4b36-99da-a29844d1f4b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca0b04d9-a99d-4dd6-ba73-6bac54672c3f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "a0af7a8c-7a7c-41ad-bb10-3f638ffb1094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "6b7311b9-74b0-4fbe-b1c0-857dcc87795d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0af7a8c-7a7c-41ad-bb10-3f638ffb1094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4afdff4-4b6b-491a-a735-05da8b1c8de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0af7a8c-7a7c-41ad-bb10-3f638ffb1094",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6aab8c49-fcfc-4497-a20a-cbca77aca9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "7dd04b4f-bb66-4b88-b52f-63e7ed5d5cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aab8c49-fcfc-4497-a20a-cbca77aca9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f237517f-1e89-426a-a2a1-badc8e3c5104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aab8c49-fcfc-4497-a20a-cbca77aca9b5",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "1869e931-853c-4607-98f4-be1a4a35bc0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "53883266-e421-4309-a5a6-eb8ae9750c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1869e931-853c-4607-98f4-be1a4a35bc0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c74d7b69-8c88-4ea3-beb7-423c659b880c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1869e931-853c-4607-98f4-be1a4a35bc0d",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "70bcb288-1c72-4cf8-b7ee-1bd7ef079175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "583c1612-ee78-472e-a1e0-c64053fc9512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70bcb288-1c72-4cf8-b7ee-1bd7ef079175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0ce0db-5170-4ac3-83ff-e0b431e2efb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70bcb288-1c72-4cf8-b7ee-1bd7ef079175",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "dd84ba9f-d826-42db-86fb-d80c82e24809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "ec34daff-8e98-426d-bd19-bb4916033bc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd84ba9f-d826-42db-86fb-d80c82e24809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8594484c-8903-475a-b427-f0e2a9391fe7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd84ba9f-d826-42db-86fb-d80c82e24809",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "1dace213-0d0f-4faf-8b59-9ea6a314feef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "9de16da4-5ca0-4086-9903-7be8694aa315",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dace213-0d0f-4faf-8b59-9ea6a314feef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf8f9c5-339d-48f9-92f7-ff0c4505e119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dace213-0d0f-4faf-8b59-9ea6a314feef",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "9beffc5c-1ca7-4683-be54-16360f545926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "a3dc4b39-c250-4c10-b92b-0270fc376881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9beffc5c-1ca7-4683-be54-16360f545926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6caf6e-f8e2-4b7b-9382-c602cb7394db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9beffc5c-1ca7-4683-be54-16360f545926",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c291afd5-2322-4521-806e-67586931ca0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "49d8657e-d839-4147-9b86-94c24bfb5934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c291afd5-2322-4521-806e-67586931ca0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5078825-1507-47f2-921c-b2750013cc65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c291afd5-2322-4521-806e-67586931ca0e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "a8302379-af9e-4ba1-84ea-f31871d867ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "8fac3942-adba-43fc-904d-456c1b243cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8302379-af9e-4ba1-84ea-f31871d867ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357cc892-d8a8-4a24-9368-8c0e903fee8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8302379-af9e-4ba1-84ea-f31871d867ad",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "04f91eb6-69df-4b71-83c5-ee0810cab6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "b85e3b0e-8178-4286-a720-a2a0b3b76820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f91eb6-69df-4b71-83c5-ee0810cab6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53fabfbc-857b-44d0-bcba-e01fdaa4f49f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f91eb6-69df-4b71-83c5-ee0810cab6b2",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c166cfb8-a0b8-4db8-b91a-ab67e7bb7810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "57604c1e-ecc2-4ee2-8683-c45dca43d6d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c166cfb8-a0b8-4db8-b91a-ab67e7bb7810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19532ad0-3e0f-4c7e-9435-e5ee4cc8fe4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c166cfb8-a0b8-4db8-b91a-ab67e7bb7810",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "eaf48177-3f2c-4a58-9792-b620bc47378f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "f5d646ed-47bc-4ec7-a5a9-7687bdf916ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf48177-3f2c-4a58-9792-b620bc47378f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d7f1e54-eacd-4931-8c32-495a4fcedad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf48177-3f2c-4a58-9792-b620bc47378f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "b5916315-6ccf-4176-93c7-13a7e75a995e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "5dd21425-6797-4cf2-a565-064779500a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5916315-6ccf-4176-93c7-13a7e75a995e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82036a84-59cd-4483-a5b9-b1adb1c4a743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5916315-6ccf-4176-93c7-13a7e75a995e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "75f5d3e0-2037-4eb7-aa5a-d456700f8fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "f4964dff-d3a5-425b-aedb-8550a8536d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f5d3e0-2037-4eb7-aa5a-d456700f8fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb3cc4f7-b8d9-4fbd-a433-da56c02437b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f5d3e0-2037-4eb7-aa5a-d456700f8fc2",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "4258daa4-e3c4-403c-9b23-905f2fb7e310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "7b643d59-f86c-45e2-8632-f2a85a8a9a5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4258daa4-e3c4-403c-9b23-905f2fb7e310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d271ed0-cc58-4929-801f-9e8787b12188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4258daa4-e3c4-403c-9b23-905f2fb7e310",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "4049e964-4984-4052-bed3-4d302d9be1da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "ea9d83ec-1be4-4886-992f-3b26667952d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4049e964-4984-4052-bed3-4d302d9be1da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6013390b-c0b8-4574-b7c1-e37274722c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4049e964-4984-4052-bed3-4d302d9be1da",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "d1834786-77a5-4be7-9cf4-0cbddd98bfed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "1557259e-62eb-49d6-8fb1-f85d25681a53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1834786-77a5-4be7-9cf4-0cbddd98bfed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9406fc1-3ec3-4604-b5bb-a65058e3c514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1834786-77a5-4be7-9cf4-0cbddd98bfed",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "87128d9d-0b4a-4412-a3fc-4b220633f60b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "347f9a25-a610-4fac-a7aa-400c6b754480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87128d9d-0b4a-4412-a3fc-4b220633f60b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b528286a-4c2b-4216-bbac-85195ba5ec2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87128d9d-0b4a-4412-a3fc-4b220633f60b",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "93f26301-7fc1-47e3-b597-d2bfe73765ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "3b1d592e-5812-4bd1-a1c8-c6a38b874d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93f26301-7fc1-47e3-b597-d2bfe73765ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d32a3f-fa92-45c0-8aa0-49ce29106641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93f26301-7fc1-47e3-b597-d2bfe73765ff",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "9511ff81-fdab-4442-8be0-c2442083ab07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "b4d7f29d-9db2-45f0-bc4a-0f5a94c1d9e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9511ff81-fdab-4442-8be0-c2442083ab07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51c227b3-b842-455e-b51a-d9d3a0d65742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9511ff81-fdab-4442-8be0-c2442083ab07",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "bd002ca0-0c80-4110-b418-de73290f18d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "a6e79c3d-ec61-481e-ac6b-7d6403c0f66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd002ca0-0c80-4110-b418-de73290f18d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e198a6d-a70c-4c99-9029-e6328a7a4b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd002ca0-0c80-4110-b418-de73290f18d7",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "840da9b3-8c62-4e9c-ac0c-080e41a0cdb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "19259b67-689c-4a59-ad70-ccedd22716f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "840da9b3-8c62-4e9c-ac0c-080e41a0cdb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43d0755-76c8-4786-9db4-61e6abb001a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "840da9b3-8c62-4e9c-ac0c-080e41a0cdb8",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "daf66c73-67d6-455d-b4e8-78d0fa8bbe57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "ec8042a9-97dd-4bd0-96e3-f8df380829a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf66c73-67d6-455d-b4e8-78d0fa8bbe57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bee017-7964-490d-b84d-dc7820cdb2d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf66c73-67d6-455d-b4e8-78d0fa8bbe57",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "27ddae88-0cfd-47e0-8b4f-5c36ee0759ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "2b14de71-284a-4341-8029-e44c1ab3e04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ddae88-0cfd-47e0-8b4f-5c36ee0759ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b498ef-18a3-4e45-9388-22fcfef4314c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ddae88-0cfd-47e0-8b4f-5c36ee0759ad",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "f25af8f5-37e4-4896-8157-548e63549865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "82ccf59a-5b4d-429f-b3e3-5b9f122ef1a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25af8f5-37e4-4896-8157-548e63549865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46e9739-22ea-4fd1-90be-ff1fd2e09323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25af8f5-37e4-4896-8157-548e63549865",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ef0efda7-099a-444b-920a-fef74de647bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "4eed2776-2bde-4c9d-b2a2-d3e61fcd66a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0efda7-099a-444b-920a-fef74de647bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d6a1e4-ed89-49bc-8358-e621f23538b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0efda7-099a-444b-920a-fef74de647bc",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "f666149e-777b-44ec-8551-4e713b919e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "e6228163-5d9b-4014-acac-8376db357cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f666149e-777b-44ec-8551-4e713b919e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6102a65d-0f50-4e30-bdd7-0f28fc013857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f666149e-777b-44ec-8551-4e713b919e51",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "304d1b9e-feff-41b7-adc3-3902a2d23830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "e1d0619e-9ffb-48b8-8247-8a4308431b5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "304d1b9e-feff-41b7-adc3-3902a2d23830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7290d320-e81d-4c74-955c-9eb4b8181b5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "304d1b9e-feff-41b7-adc3-3902a2d23830",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "1ffc8f11-a374-43bc-90de-3b07e22908ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "589179aa-2838-49f8-bed4-7186886cbaca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ffc8f11-a374-43bc-90de-3b07e22908ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "310edabc-0806-40d4-aebc-df2d7ea23bed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ffc8f11-a374-43bc-90de-3b07e22908ca",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "8551dc65-d5dc-4c40-ad08-261e793a4b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "d19a8493-4a02-4bfc-9e80-16578df2d575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8551dc65-d5dc-4c40-ad08-261e793a4b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78eb10d8-ffc4-4f7d-944a-cf6ee57e46f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8551dc65-d5dc-4c40-ad08-261e793a4b1f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "2ac4dc8f-a889-48a8-a166-40a898b08d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "26ac340a-4efe-4bfe-bc4f-e565ba5306fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ac4dc8f-a889-48a8-a166-40a898b08d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc05e221-d16e-4c05-9455-9273cb006db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ac4dc8f-a889-48a8-a166-40a898b08d2f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "dd832182-68cd-40af-b064-3bfcc21b9c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "38b3232a-032e-4e8a-b63d-cd956c033777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd832182-68cd-40af-b064-3bfcc21b9c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f9e815-fb91-4b98-942c-95bd5bafa015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd832182-68cd-40af-b064-3bfcc21b9c2e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "8f6d0cdb-4922-4c3c-a93e-d35db93a03a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "688c328f-3c25-47d7-9688-d313691aad20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f6d0cdb-4922-4c3c-a93e-d35db93a03a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d2dd1f-8552-4706-8a3c-ce8560a89a7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f6d0cdb-4922-4c3c-a93e-d35db93a03a1",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "89179275-ec86-4eda-94d5-60edfb6ed4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "f0e6801a-ac72-4af1-bc9a-81e9d655ffc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89179275-ec86-4eda-94d5-60edfb6ed4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e847441a-601a-4d6a-b677-217a52171ae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89179275-ec86-4eda-94d5-60edfb6ed4c4",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "13e914ff-c39c-49dc-baa2-0ea5a042dd86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "1acc7443-b5bd-4b7e-9533-ef176696f931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e914ff-c39c-49dc-baa2-0ea5a042dd86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a804c979-2dc1-4fa4-aefe-2bdaef96cf3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e914ff-c39c-49dc-baa2-0ea5a042dd86",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "b49c85a0-51be-4d1d-be1b-0dce122ecdf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "28e9e751-b600-4428-9dc8-c0ecd9d1980d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b49c85a0-51be-4d1d-be1b-0dce122ecdf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eb8cb88-3bb4-4a71-a776-e3433a9b833e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b49c85a0-51be-4d1d-be1b-0dce122ecdf8",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "732578b8-7348-4ca9-ae2f-371fd8eb213f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "d0aa5d9e-98d5-4f38-8ac0-afce12e4dc37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "732578b8-7348-4ca9-ae2f-371fd8eb213f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9b5350a-ee19-450d-88a4-38ba518ae051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "732578b8-7348-4ca9-ae2f-371fd8eb213f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "999c566b-2b3d-4dc5-b37c-8ee6700e0eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "215e39fc-8dc9-42f0-afcf-ea9ddef96603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "999c566b-2b3d-4dc5-b37c-8ee6700e0eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea73a030-17fb-41be-aa06-738d82e01b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "999c566b-2b3d-4dc5-b37c-8ee6700e0eb6",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c1fabacb-ccdc-468e-ab91-3879ad473ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "3056b129-c933-44d4-83ad-ef139659b175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1fabacb-ccdc-468e-ab91-3879ad473ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a15f40c-70ca-4f2a-9d59-1f44f4d172f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1fabacb-ccdc-468e-ab91-3879ad473ce7",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "11d177b3-c497-4436-b08d-c29fb8caf764",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "753e822e-7d33-402f-99a2-398a523026db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11d177b3-c497-4436-b08d-c29fb8caf764",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87251edc-f48c-4005-a336-9121eddb58a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d177b3-c497-4436-b08d-c29fb8caf764",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6d5dafa9-62ff-427c-97c8-e6e39196b04f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "583997a3-52e1-4bd6-881f-104896e9c9ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d5dafa9-62ff-427c-97c8-e6e39196b04f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e9a113a-2b2a-4071-b7cc-950ba0068f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d5dafa9-62ff-427c-97c8-e6e39196b04f",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "bd206689-7871-405b-9d4e-f0e9e05a455e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "715c45a8-d4ea-496d-92f9-d028acc6e6e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd206689-7871-405b-9d4e-f0e9e05a455e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6529d9-810c-49de-818d-684e98b07949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd206689-7871-405b-9d4e-f0e9e05a455e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "5b5e6fe2-1dcd-434b-a0fe-07ced92be67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "c681be42-2ca5-4efd-ba22-5c966c9ec336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5e6fe2-1dcd-434b-a0fe-07ced92be67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecd0607d-4df1-4a00-a3db-07a14c17ac1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5e6fe2-1dcd-434b-a0fe-07ced92be67e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "edf0c692-036f-4142-a070-07ef0aa9d56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "1953a2ee-1f1a-4a41-b9f6-833cad75d84b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf0c692-036f-4142-a070-07ef0aa9d56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf4bc5d-7685-4a02-99ed-3b3df2fff9c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf0c692-036f-4142-a070-07ef0aa9d56e",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "cc060002-b1a6-46f8-b822-9c6b2b0d8d97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "5e210bae-ead0-4078-adad-4cda03a02c1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc060002-b1a6-46f8-b822-9c6b2b0d8d97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82329593-1b13-47fa-ae7f-32ce54be87ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc060002-b1a6-46f8-b822-9c6b2b0d8d97",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "a70b7d12-4e0f-4382-993d-c48a22d0a3b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "3ceabe48-20ce-40bd-98bd-1567c9f3b0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a70b7d12-4e0f-4382-993d-c48a22d0a3b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4c0de16-1b7d-4614-96c1-9cb71a205672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a70b7d12-4e0f-4382-993d-c48a22d0a3b2",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "fe52b6b7-6f10-40bc-ae54-4f4a0e679e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "06fbf9d3-e4f5-4373-8d53-82072e80565d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe52b6b7-6f10-40bc-ae54-4f4a0e679e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53131b20-244d-4cef-a36f-cb7b077f317e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe52b6b7-6f10-40bc-ae54-4f4a0e679e52",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c2044d11-48e7-4ec2-b5ab-d859051ea350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "10ad9cdb-26ad-499d-8c4a-492bf3df0195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2044d11-48e7-4ec2-b5ab-d859051ea350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8df777-b796-4dbd-9820-060c020a83f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2044d11-48e7-4ec2-b5ab-d859051ea350",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "d193f0a4-b353-4455-a417-f4304f143da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "32030c5b-735c-4733-8692-5916a928f989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d193f0a4-b353-4455-a417-f4304f143da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a88c47d-94bb-4ab2-bb02-c6983be80bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d193f0a4-b353-4455-a417-f4304f143da2",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "cb83a4bb-ec09-4464-ac1b-835b34d935f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "30265a5f-63b5-4bfe-bbf9-df16c1bb7135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb83a4bb-ec09-4464-ac1b-835b34d935f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad036ab-ce18-4ce8-ad52-3a6bcc0eafa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb83a4bb-ec09-4464-ac1b-835b34d935f6",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "7e693138-2c5a-438a-8f9f-f5b5d7d4ef22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "fab66a1b-2c8c-4602-92f9-6340c7385df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e693138-2c5a-438a-8f9f-f5b5d7d4ef22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c705bc7-910a-476d-a948-ab5d9fb1b067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e693138-2c5a-438a-8f9f-f5b5d7d4ef22",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "a54a80ff-2237-4ae0-82f8-d032d1006e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "814b2b6b-24d3-4bf0-b57a-f20e0cf204f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a54a80ff-2237-4ae0-82f8-d032d1006e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9809ecd1-e818-443e-8b97-478f2ffe4389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a54a80ff-2237-4ae0-82f8-d032d1006e8d",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "99f4b1c8-2c9d-4bb7-8a28-d1b3f1277262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "93435ead-a882-4ed2-8833-bd31f0b8aac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f4b1c8-2c9d-4bb7-8a28-d1b3f1277262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1adaf79-3026-4475-9fee-39ab1578b811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f4b1c8-2c9d-4bb7-8a28-d1b3f1277262",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "36a7b9d0-5eda-4dd4-b7b4-0a199bc78dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "d0fef9e4-6221-448c-a188-7e613bd171ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a7b9d0-5eda-4dd4-b7b4-0a199bc78dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ec62c70-fbf9-4756-8957-172b40b76ee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a7b9d0-5eda-4dd4-b7b4-0a199bc78dfd",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "85297c07-0926-4937-b5bc-7c19e7c2c1d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "977b8cea-2134-402a-9cf3-fc2ac8134fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85297c07-0926-4937-b5bc-7c19e7c2c1d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae5fa14-ea1d-4ccc-be9e-437c513d0126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85297c07-0926-4937-b5bc-7c19e7c2c1d7",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "c516ba30-6f91-4538-a80c-20658d0b1cb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "322222b2-f0c7-430f-9eb0-b78ee9a758c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c516ba30-6f91-4538-a80c-20658d0b1cb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21444099-8214-4210-a84f-1d6ffa1ba692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c516ba30-6f91-4538-a80c-20658d0b1cb6",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "917e6044-47a8-447f-8d06-52850b5217f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "6a1016df-4f24-46db-91f6-7be85bfea0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "917e6044-47a8-447f-8d06-52850b5217f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b899a4f6-f1ff-4e1c-8e82-e022cee22f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "917e6044-47a8-447f-8d06-52850b5217f3",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "d8022d8c-4cd7-4c6c-93a2-4ba1bf8c5f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "3c19966d-711e-4124-85b1-028f34a16f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8022d8c-4cd7-4c6c-93a2-4ba1bf8c5f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39455c37-f0c9-4b6e-a40e-607590bc3406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8022d8c-4cd7-4c6c-93a2-4ba1bf8c5f31",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ca6836cd-2b98-4173-b52b-34d0954a1468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "156cba9b-df74-4eb7-aba0-84f11ee83ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6836cd-2b98-4173-b52b-34d0954a1468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d93debd-cb67-434b-9b60-0e1ee34d7afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6836cd-2b98-4173-b52b-34d0954a1468",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "4e0b765a-50ed-4cbf-9873-c4fb8e9e6dd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "f73bfa4c-880b-42fb-b571-0b73f1a2a2cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0b765a-50ed-4cbf-9873-c4fb8e9e6dd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91a36c5-c952-4482-8504-d9832b273a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0b765a-50ed-4cbf-9873-c4fb8e9e6dd2",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "d57b4561-f771-4ba4-8b82-48b0dea9454d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "67926c95-ded0-4881-8272-77f25ab60ff7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d57b4561-f771-4ba4-8b82-48b0dea9454d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1fad6e-47ed-4180-8619-f45967ef4016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d57b4561-f771-4ba4-8b82-48b0dea9454d",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "08971af7-6f3d-4a7b-8b87-3abca9234857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "54e6c44e-aa4f-4259-95a5-63e2e17012b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08971af7-6f3d-4a7b-8b87-3abca9234857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4995a896-944a-4e26-b380-0e2b61e071bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08971af7-6f3d-4a7b-8b87-3abca9234857",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "be3e3c68-7c7a-4266-babc-9b2a27321e36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "aed20fc7-1ab8-4c11-88ff-6723bf893e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3e3c68-7c7a-4266-babc-9b2a27321e36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df39b072-fb37-4fa0-9081-b9bb0519556b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3e3c68-7c7a-4266-babc-9b2a27321e36",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6b563781-4633-448c-a41b-74df3a798533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "0eab0014-6c69-4f3d-b8e9-73507472e835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b563781-4633-448c-a41b-74df3a798533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb8bd7f-7ee0-47dc-a255-6b5fb19cefd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b563781-4633-448c-a41b-74df3a798533",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "f01793f2-9877-44af-8f06-9aafc9768710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "a5f9e858-73ea-43f4-9b44-b3a41c253375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f01793f2-9877-44af-8f06-9aafc9768710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de311636-421d-4fc9-8574-1b00b1369b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f01793f2-9877-44af-8f06-9aafc9768710",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "6e96dfdb-801f-4c7c-a076-804463cf9c3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "01d09d7f-1b1b-46a3-83e1-03f4e21df041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e96dfdb-801f-4c7c-a076-804463cf9c3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e491ed3-f5f7-4156-9d20-34ebe0574d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e96dfdb-801f-4c7c-a076-804463cf9c3c",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "32c620a5-443d-4b15-b71f-ab5d24e09ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "5629ea60-109a-4769-b085-0d8cc556a756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c620a5-443d-4b15-b71f-ab5d24e09ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b0f5b5-1388-4288-a1be-5d6d8068ce25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c620a5-443d-4b15-b71f-ab5d24e09ed1",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "0221b1f6-d0f4-4387-8068-9bdac3009e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "c0091d2c-14a4-42fb-b695-a1659beded10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0221b1f6-d0f4-4387-8068-9bdac3009e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede86b34-eb19-46cf-aabd-c55a6eedf21e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0221b1f6-d0f4-4387-8068-9bdac3009e65",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "f1ab160b-6a9d-4b2f-9185-ac3b4a0ba7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "ada2dbfe-3dd9-4eb8-85d1-9878d5bc8aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ab160b-6a9d-4b2f-9185-ac3b4a0ba7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fde11aa-42df-4bf8-9c5e-c164d7fdb774",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ab160b-6a9d-4b2f-9185-ac3b4a0ba7d6",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "e1765743-57d9-4bbe-b37e-796d5cda2291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "b165c95e-4763-4175-9154-b6f4c5a7f32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1765743-57d9-4bbe-b37e-796d5cda2291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843e50d8-4d2e-4858-bc03-5722d3e98611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1765743-57d9-4bbe-b37e-796d5cda2291",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "16b38a42-04bd-49f9-8735-a0e5972c77f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "141d8008-deff-470c-ade5-d283a5a73dcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16b38a42-04bd-49f9-8735-a0e5972c77f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0cf33f8-cb41-456b-b201-1f30c34a129b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16b38a42-04bd-49f9-8735-a0e5972c77f4",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "35e093a1-3f37-448f-96ad-4ec3eb966e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "544ba05d-8b13-46f2-a628-d68c167b3c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e093a1-3f37-448f-96ad-4ec3eb966e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa0f23c-9127-470c-80ac-142fa175af56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e093a1-3f37-448f-96ad-4ec3eb966e54",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "315ecb63-501c-4670-a4bb-d0ebb5203769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "4210f579-0702-405b-838d-d13fb265407f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "315ecb63-501c-4670-a4bb-d0ebb5203769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67797209-da56-487a-b235-d5c54a9531d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "315ecb63-501c-4670-a4bb-d0ebb5203769",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "d76a5675-9841-47a6-8c3d-b76816921a42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "6a96236c-e6a1-4923-a4b3-e884c071493f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76a5675-9841-47a6-8c3d-b76816921a42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2445be64-6ea0-4841-bf14-9d59b812cb7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76a5675-9841-47a6-8c3d-b76816921a42",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "64b1cf6b-25cc-4306-8694-a1a2effd9cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "468de100-c6f0-4418-8077-c8c4a8c471f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64b1cf6b-25cc-4306-8694-a1a2effd9cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0719030c-1798-4ddf-a3f8-2f5ce8e58e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64b1cf6b-25cc-4306-8694-a1a2effd9cc8",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "3ecbcbb8-826f-42a4-a40b-c665170faf84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "0868be91-4d2e-4866-9c55-9bd6e865d76d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ecbcbb8-826f-42a4-a40b-c665170faf84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50192e46-c79d-470d-b7ae-c6ff45d41984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ecbcbb8-826f-42a4-a40b-c665170faf84",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        },
        {
            "id": "ae96bc2b-f310-400e-afcf-7561fb8315ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "compositeImage": {
                "id": "d3cead0c-8b15-433c-8a92-18b9e7d15fbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae96bc2b-f310-400e-afcf-7561fb8315ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c5cb5c-2f92-4e67-8a11-badd3b6278e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae96bc2b-f310-400e-afcf-7561fb8315ad",
                    "LayerId": "13b7c570-8d38-4c2c-835c-8cf6201a7838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "13b7c570-8d38-4c2c-835c-8cf6201a7838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f57ae7f6-a785-4d83-b891-dd3a719b4809",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 104,
    "xorig": 48,
    "yorig": 66
}