{
    "id": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_down_attack_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dd8f622-f52e-45e8-a9b7-e6c35ffd492f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "beb42eb4-13e9-40e1-83fd-a4d9f976dc52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dd8f622-f52e-45e8-a9b7-e6c35ffd492f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47545289-49d1-46c1-ac82-4b5703dde749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dd8f622-f52e-45e8-a9b7-e6c35ffd492f",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "828980f9-82ca-47b1-a559-c6161bdb4a9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "732e9ab1-47f4-476c-9c2e-219fe0091635",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "828980f9-82ca-47b1-a559-c6161bdb4a9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "addb1878-52fa-4c74-ad81-931f32eda6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "828980f9-82ca-47b1-a559-c6161bdb4a9c",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "9fa569e2-1a2d-48fc-a4b7-d7ff4cb8072c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "011194ba-fa83-4508-87e7-aeaffb9e80a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa569e2-1a2d-48fc-a4b7-d7ff4cb8072c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f927af67-e5c2-40df-acc5-2c99a882f906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa569e2-1a2d-48fc-a4b7-d7ff4cb8072c",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "8d432a77-f680-41c6-8a11-9323cf217028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "112d08dc-efd1-47e2-926a-f20bf07bfe25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d432a77-f680-41c6-8a11-9323cf217028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed81989-57a8-41a3-bc3a-69304c0cba5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d432a77-f680-41c6-8a11-9323cf217028",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "1c13929e-5209-404a-9a7e-5a2e3610615c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "9d82b2d9-06cf-4277-81d6-69ccd2a459bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c13929e-5209-404a-9a7e-5a2e3610615c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b53fa83-6866-473b-b229-24c01ad1f5cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c13929e-5209-404a-9a7e-5a2e3610615c",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "30f0bbc8-b7a9-4580-849f-73cfbca3219a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "0992a750-2408-4370-a777-42b04484b390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f0bbc8-b7a9-4580-849f-73cfbca3219a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8e639b-c11b-41e5-8a39-d50ea5c030b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f0bbc8-b7a9-4580-849f-73cfbca3219a",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "ff72b44b-572e-420a-9c66-ab29ca7782f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "92f501e9-b5b8-4a02-9c57-32278d670910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff72b44b-572e-420a-9c66-ab29ca7782f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43894ac5-d3d0-4416-8c91-3b99fa66a325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff72b44b-572e-420a-9c66-ab29ca7782f9",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "b55a234e-3cfb-4b37-8173-df26f88c7f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "df6ab73c-99d6-44f2-8331-757a9a31e242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b55a234e-3cfb-4b37-8173-df26f88c7f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25cfa383-24f8-404c-8423-66982d6d6ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b55a234e-3cfb-4b37-8173-df26f88c7f44",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "e6284ace-a492-443d-b8eb-c2f5b0905f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "2901c4bb-8b7a-4f39-b303-6f851386e8cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6284ace-a492-443d-b8eb-c2f5b0905f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4a12c3-ff60-49e0-aa0c-94b19d6b7853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6284ace-a492-443d-b8eb-c2f5b0905f8c",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        },
        {
            "id": "391a8f02-a3b1-4cc7-ae87-a9608cafadf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "compositeImage": {
                "id": "b00f26a8-37c2-414e-a6c8-f1789a6ff3a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "391a8f02-a3b1-4cc7-ae87-a9608cafadf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3fcbb2-8b03-4396-9599-717529b2244c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "391a8f02-a3b1-4cc7-ae87-a9608cafadf1",
                    "LayerId": "96bbd555-8a68-4367-8599-e2150438382a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "96bbd555-8a68-4367-8599-e2150438382a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5f51759-9c04-4868-87c4-3d36b1f6e91e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 56,
    "yorig": 67
}