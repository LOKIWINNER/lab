{
    "id": "2f5ea1c8-5c8b-4f42-b7ab-7ba0ffb5ea4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 733,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bbdd126-d172-4641-b83d-3d8667616320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f5ea1c8-5c8b-4f42-b7ab-7ba0ffb5ea4a",
            "compositeImage": {
                "id": "820e39f8-82cc-4cc9-a033-fdb4e8f4816c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bbdd126-d172-4641-b83d-3d8667616320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df05b795-ce95-481b-9c4e-4a7755d722a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bbdd126-d172-4641-b83d-3d8667616320",
                    "LayerId": "1eb0bf86-3176-4ec4-85c7-c3dd5c994c84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 736,
    "layers": [
        {
            "id": "1eb0bf86-3176-4ec4-85c7-c3dd5c994c84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f5ea1c8-5c8b-4f42-b7ab-7ba0ffb5ea4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}