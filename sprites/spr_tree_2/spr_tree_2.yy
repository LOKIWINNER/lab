{
    "id": "8bfdeb6d-9647-4473-b5aa-b98aa93a1f82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 46,
    "bbox_right": 79,
    "bbox_top": 137,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f5b60a0-e736-4604-ab92-63fe9505d6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bfdeb6d-9647-4473-b5aa-b98aa93a1f82",
            "compositeImage": {
                "id": "1ef913ab-bb3c-4119-8423-98aa360c1cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5b60a0-e736-4604-ab92-63fe9505d6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2240b2-1790-4fe5-ada4-c45cdd92c24d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5b60a0-e736-4604-ab92-63fe9505d6f2",
                    "LayerId": "a8e6e6b1-86d4-4f7d-bfd4-526b9c3dc715"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "a8e6e6b1-86d4-4f7d-bfd4-526b9c3dc715",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bfdeb6d-9647-4473-b5aa-b98aa93a1f82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 147
}