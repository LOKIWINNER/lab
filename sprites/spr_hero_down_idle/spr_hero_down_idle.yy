{
    "id": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 4,
    "bbox_right": 46,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10113fe6-ed52-4d9d-b3f7-bca70e8c61ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "3eaffe44-8fbf-409b-af40-4b242a28b2a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10113fe6-ed52-4d9d-b3f7-bca70e8c61ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39af6963-21b6-4a01-9032-9cb9369dd8dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10113fe6-ed52-4d9d-b3f7-bca70e8c61ed",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "5eaed0bb-eb03-4905-8b1b-988dd196ce16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "5e83f8c6-39c5-4200-a083-f7bc64a51122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eaed0bb-eb03-4905-8b1b-988dd196ce16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "094c8319-4312-4ed8-b381-3d71ffe5a0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eaed0bb-eb03-4905-8b1b-988dd196ce16",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "1672707f-5fdb-4169-89f1-9a1778d35897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "506c5c57-ba73-4285-bb90-a2550a0186f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1672707f-5fdb-4169-89f1-9a1778d35897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6004f247-ab76-4459-8594-2f778191fecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1672707f-5fdb-4169-89f1-9a1778d35897",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "8320522a-c27a-4a2d-a4bc-c4477347f3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "bd704ae5-0bdd-4790-98c6-cd568997048c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8320522a-c27a-4a2d-a4bc-c4477347f3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8eceb62-60b4-40bd-9e84-453699f88b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8320522a-c27a-4a2d-a4bc-c4477347f3be",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "00b77a80-42a2-45f9-9c72-6170fa3af639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "6bb8d52b-8053-4857-9ce3-30853c9535f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b77a80-42a2-45f9-9c72-6170fa3af639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7400e8dc-eff3-439a-a8ce-80c0081ba89c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b77a80-42a2-45f9-9c72-6170fa3af639",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "d560b663-961f-4f36-a90b-d5142c7ea309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "2289afb4-a128-4715-9a50-a6a117ad0272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d560b663-961f-4f36-a90b-d5142c7ea309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8da850-15f9-46c1-89c7-d2a8ba102c04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d560b663-961f-4f36-a90b-d5142c7ea309",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "2cda8d64-8ee9-4ab0-9f87-e5d9d84b027c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "228c6ad4-79ac-4a2b-a5b7-2e0c8d3a6e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cda8d64-8ee9-4ab0-9f87-e5d9d84b027c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f585f8a-465d-4669-a7e3-1acd5b7d8f5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cda8d64-8ee9-4ab0-9f87-e5d9d84b027c",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        },
        {
            "id": "5943da00-8bc2-4761-899f-fa0d3b743697",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "compositeImage": {
                "id": "0b73d0f6-cbf8-4356-905d-99d2ae83b4f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5943da00-8bc2-4761-899f-fa0d3b743697",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e40523c4-d548-4ae2-aae9-9d48422fb55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5943da00-8bc2-4761-899f-fa0d3b743697",
                    "LayerId": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4bbc33e5-f995-4e68-8ab8-9bdb314daf50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}