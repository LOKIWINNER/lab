{
    "id": "014ad170-2112-438d-979e-0f3ca22c81a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "096187c1-4bb0-495d-824a-09fc0ac107b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "014ad170-2112-438d-979e-0f3ca22c81a4",
            "compositeImage": {
                "id": "adbd2a14-16d6-4d67-b9d6-ae7191a95e88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "096187c1-4bb0-495d-824a-09fc0ac107b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e890bfb-fb53-47b2-bc1f-2351a0064b15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "096187c1-4bb0-495d-824a-09fc0ac107b1",
                    "LayerId": "64d33fb4-a2a3-4d32-9376-4edab36e2965"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "64d33fb4-a2a3-4d32-9376-4edab36e2965",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "014ad170-2112-438d-979e-0f3ca22c81a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}