{
    "id": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_shield_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 21,
    "bbox_right": 49,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abe7a742-0d09-40ac-a7ad-9ebc33bbb204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "1b373e69-be4f-4274-ac87-76fe97fdae9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe7a742-0d09-40ac-a7ad-9ebc33bbb204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdb58155-8948-4dba-b9c6-a3263778a205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe7a742-0d09-40ac-a7ad-9ebc33bbb204",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "a8d835d4-f017-4a53-ab8e-e362e6a941e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "b1dfafd8-def8-4139-a429-a69ba6060054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8d835d4-f017-4a53-ab8e-e362e6a941e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf3ec96-b589-4573-910e-cc676e48c39b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8d835d4-f017-4a53-ab8e-e362e6a941e7",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "ffc51144-bf36-41c9-a7e4-7c2d7a546b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "5a252d35-c159-4d1b-819d-cc49a43cb996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc51144-bf36-41c9-a7e4-7c2d7a546b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e177cad-98ef-496a-87d1-df7c35f98873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc51144-bf36-41c9-a7e4-7c2d7a546b6e",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "db5ade54-35f4-42ce-8091-80353e74545c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "92c1d411-e983-477d-b933-ef1426d98650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db5ade54-35f4-42ce-8091-80353e74545c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "470445ef-bde1-43c9-a5a4-9394776f3b0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5ade54-35f4-42ce-8091-80353e74545c",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "35eef41a-c342-4b73-9c62-f32cd4fde647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "4a10fba9-186c-4e25-88be-6853238d8fc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35eef41a-c342-4b73-9c62-f32cd4fde647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863e8efa-e9ed-42e6-b075-afd4bbf0de80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35eef41a-c342-4b73-9c62-f32cd4fde647",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "440f4da2-5b22-4af1-af8f-b727ae06b08f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "1fe67cd9-9466-41d5-9c74-156d42a273de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "440f4da2-5b22-4af1-af8f-b727ae06b08f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71c965cb-0d93-4972-a474-1e0e49b791b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "440f4da2-5b22-4af1-af8f-b727ae06b08f",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "b47ea1fa-7a79-41db-928d-a432b2690162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "dfbfa6ca-bd76-4e80-813b-ad07eaf27143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b47ea1fa-7a79-41db-928d-a432b2690162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa767b7-2b1b-48e3-ac5f-d069faa40696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b47ea1fa-7a79-41db-928d-a432b2690162",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "92147872-4393-47f1-b62a-a37f7993b4f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "b5a90c50-7b26-4031-b697-7e3867c068d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92147872-4393-47f1-b62a-a37f7993b4f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "902c3763-06fe-49c3-9027-52e6a4d6c06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92147872-4393-47f1-b62a-a37f7993b4f9",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "07589545-0d8b-4bfd-931c-27e48a452441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "b5a4b6ff-d118-49c0-ab03-b1efc2aee193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07589545-0d8b-4bfd-931c-27e48a452441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0c09fa-ad67-4dbb-9ad0-4f844c13d228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07589545-0d8b-4bfd-931c-27e48a452441",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "44994ec3-b4e3-4cd2-99b0-845fa3c27bb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "f20a5382-2bb1-4639-b8bb-87b957bf0bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44994ec3-b4e3-4cd2-99b0-845fa3c27bb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac13e63a-d8d5-4f4a-888d-df4c8aff675a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44994ec3-b4e3-4cd2-99b0-845fa3c27bb0",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "ef21dddf-19ac-44b5-bf74-51c03cd06875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "733e1251-f08f-4bf1-a096-e069e61a92ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef21dddf-19ac-44b5-bf74-51c03cd06875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a173d6-82d0-4463-b49d-ec00e3872fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef21dddf-19ac-44b5-bf74-51c03cd06875",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "8c43104d-4334-437e-9638-05c2e208dd76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "e07b0737-85a4-444a-b5ab-73bdcbb78027",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c43104d-4334-437e-9638-05c2e208dd76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7025825a-28c1-4afb-8f4c-97284a07fb76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c43104d-4334-437e-9638-05c2e208dd76",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "d3a61e6f-7f61-466a-9604-c74bc6878151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "e8c06dcf-3aa5-481c-9284-22d8b2a44ef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a61e6f-7f61-466a-9604-c74bc6878151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eaf5743-f3df-4799-b8ba-91a7c3ae956d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a61e6f-7f61-466a-9604-c74bc6878151",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "e9fedb9a-0359-44fc-a81a-3924d44b8abb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "5fb93b4d-5f46-4d32-9feb-97ae2f1deefb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fedb9a-0359-44fc-a81a-3924d44b8abb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dac9280-c612-4667-a824-e901fbbe2c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fedb9a-0359-44fc-a81a-3924d44b8abb",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        },
        {
            "id": "ac760502-39f1-416d-b7af-d4ddaab9b359",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "compositeImage": {
                "id": "764e105f-f194-4178-9841-e25ec2cc2217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac760502-39f1-416d-b7af-d4ddaab9b359",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d28a9d52-6cf5-4115-8f04-3ddb336cefcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac760502-39f1-416d-b7af-d4ddaab9b359",
                    "LayerId": "b7569237-317a-402a-9ca6-b282a907e0b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b7569237-317a-402a-9ca6-b282a907e0b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbf89de9-e953-477f-a32d-a30ed7a5943e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 34,
    "yorig": 36
}