{
    "id": "bb88f3b2-424a-4146-9f4d-0109cc0bca8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 18,
    "bbox_right": 58,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7553afc3-f6cf-4a85-b875-4752c7f77e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb88f3b2-424a-4146-9f4d-0109cc0bca8f",
            "compositeImage": {
                "id": "fa193797-88cc-41f6-8b09-e56dac7eb853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7553afc3-f6cf-4a85-b875-4752c7f77e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9423b9d1-b35f-485f-8c79-cfcf858c6950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7553afc3-f6cf-4a85-b875-4752c7f77e69",
                    "LayerId": "9f97902b-b001-4259-836b-12d912e7ac68"
                }
            ]
        },
        {
            "id": "bd830e90-8eb6-40ab-a38e-b23bd8692f5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb88f3b2-424a-4146-9f4d-0109cc0bca8f",
            "compositeImage": {
                "id": "05a6a425-480b-4315-b5e3-aefc5c8250d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd830e90-8eb6-40ab-a38e-b23bd8692f5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "317590da-f096-485f-b66d-f8b5bd904e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd830e90-8eb6-40ab-a38e-b23bd8692f5c",
                    "LayerId": "9f97902b-b001-4259-836b-12d912e7ac68"
                }
            ]
        },
        {
            "id": "dfd94c34-0c6b-417d-af50-0e7e6fbf772e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb88f3b2-424a-4146-9f4d-0109cc0bca8f",
            "compositeImage": {
                "id": "e328d3fb-5905-4315-868e-0e7c3bac5e6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfd94c34-0c6b-417d-af50-0e7e6fbf772e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f6222d-cce7-4463-97b2-6d5123efd10e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfd94c34-0c6b-417d-af50-0e7e6fbf772e",
                    "LayerId": "9f97902b-b001-4259-836b-12d912e7ac68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9f97902b-b001-4259-836b-12d912e7ac68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb88f3b2-424a-4146-9f4d-0109cc0bca8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}