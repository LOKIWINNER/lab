{
    "id": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 3,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9ec021c-aa09-4dd5-91f7-2921451bd657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "1d7a8a9c-dbf3-4fc4-a07b-d5b1fbaf304e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ec021c-aa09-4dd5-91f7-2921451bd657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb1adf4-04da-40d6-b5b2-df30b1651069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ec021c-aa09-4dd5-91f7-2921451bd657",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "eb83ef8a-761e-489d-a08b-8dd3f7da7e8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "1064fc2b-67e6-4be1-9f50-37259f9be2be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb83ef8a-761e-489d-a08b-8dd3f7da7e8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7712b265-cbfa-4841-a319-db3521bc43d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb83ef8a-761e-489d-a08b-8dd3f7da7e8c",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "d4521b63-81d3-4197-b352-6fd0a2211615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "676125a6-bfef-47c6-8826-c8f3ad85fdae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4521b63-81d3-4197-b352-6fd0a2211615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50a426c0-b15f-45ab-b9d6-69a813b513ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4521b63-81d3-4197-b352-6fd0a2211615",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "92450d07-e82e-4c47-ad3b-6dd0ec9ce44c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "314b5972-b19b-4945-8523-c85eb198a5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92450d07-e82e-4c47-ad3b-6dd0ec9ce44c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73dcd296-16f5-4d21-8634-afc55bf12953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92450d07-e82e-4c47-ad3b-6dd0ec9ce44c",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "d4037dec-76d3-43f9-89ba-bc4e831b52b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "78f3c0d4-6e2e-4f74-8598-dddf74b9b271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4037dec-76d3-43f9-89ba-bc4e831b52b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638527e4-12d1-43c8-b96e-f40160cd9b60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4037dec-76d3-43f9-89ba-bc4e831b52b3",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "a89526ab-71a3-4e0b-922b-c41c1e4214cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "7e33c9f7-1ef3-47c0-917c-5e8035a3952a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a89526ab-71a3-4e0b-922b-c41c1e4214cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeceeb1f-5c74-4819-be03-2161303b5caa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a89526ab-71a3-4e0b-922b-c41c1e4214cb",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "8cad962f-110e-40ec-9ccd-f66332e89ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "8f7a2854-f0f8-4f9b-8981-80d60df6155a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cad962f-110e-40ec-9ccd-f66332e89ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eb248b1-2a4e-4dd6-88fa-487cb080cdb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cad962f-110e-40ec-9ccd-f66332e89ed2",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        },
        {
            "id": "9f69e983-0b8c-401a-944e-bbe216a5632e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "compositeImage": {
                "id": "5d1439bb-4694-442c-b959-beb3cb71612d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f69e983-0b8c-401a-944e-bbe216a5632e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32817cd0-a935-452f-8618-53fa280f7b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f69e983-0b8c-401a-944e-bbe216a5632e",
                    "LayerId": "d3f034f4-d247-49f8-8866-ecccd63286e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "d3f034f4-d247-49f8-8866-ecccd63286e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f2f92f8-2719-4a50-97cb-ac0f6244308b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 0,
    "yorig": 0
}