{
    "id": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_shield_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 18,
    "bbox_right": 59,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "359052f6-3637-4a0d-95c0-ff6f6deb3b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "fa42b6e5-7bb9-43ea-95e8-015c2e2a34aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "359052f6-3637-4a0d-95c0-ff6f6deb3b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6aba50a-2004-40c8-93e3-3b13fb3ea34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "359052f6-3637-4a0d-95c0-ff6f6deb3b61",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        },
        {
            "id": "2bb2c442-1f92-4e60-857f-c097205671fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "00a060eb-e1d6-48bd-ba45-be674597554e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bb2c442-1f92-4e60-857f-c097205671fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "620b663e-fd93-44b8-b5e8-787f4daf733b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bb2c442-1f92-4e60-857f-c097205671fc",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        },
        {
            "id": "9fc1b105-f225-498f-afa4-f86e8f192656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "7c756feb-c14d-48e8-974d-bf9b5c0b4bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc1b105-f225-498f-afa4-f86e8f192656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dad7505-8251-4c46-a039-5163f7398469",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc1b105-f225-498f-afa4-f86e8f192656",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        },
        {
            "id": "19b31922-93c7-4e5b-a286-0873bf8ad800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "c3d54316-7c90-4c82-b2f2-ecf51bb36d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b31922-93c7-4e5b-a286-0873bf8ad800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a73b70bb-dc57-4a75-9fc9-2aad7fef9d9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b31922-93c7-4e5b-a286-0873bf8ad800",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        },
        {
            "id": "a919e395-1c2a-424a-8b9f-103589be5353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "6f0253ca-fdd1-4f25-b0ea-988c7cd4944d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a919e395-1c2a-424a-8b9f-103589be5353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a65d9e-cf58-4e8b-9523-a05544d6ffab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a919e395-1c2a-424a-8b9f-103589be5353",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        },
        {
            "id": "c64fa9c8-7220-4cb5-9e30-cdad79803b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "2c1c7759-7ea0-4cd2-b191-f628667d0f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64fa9c8-7220-4cb5-9e30-cdad79803b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7d0718-40db-4fc4-b6bb-20f3f2fc9af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64fa9c8-7220-4cb5-9e30-cdad79803b8e",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        },
        {
            "id": "ddcf9422-3853-43b7-91c5-551daa301e79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "compositeImage": {
                "id": "d4e5a6f5-6f9d-4c14-b13b-61198155cc04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddcf9422-3853-43b7-91c5-551daa301e79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abbab858-9a59-4508-a6b5-034ba1fd364f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcf9422-3853-43b7-91c5-551daa301e79",
                    "LayerId": "6e0a825e-ac94-4078-bdb4-f64e70129177"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6e0a825e-ac94-4078-bdb4-f64e70129177",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eaed351a-d0b1-4dd5-97bd-bb43f4d5b17b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}