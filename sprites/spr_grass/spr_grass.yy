{
    "id": "1afa99b6-6221-4732-be61-ecad0a4d459f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1ca915c-c327-473e-83c1-e5d4402c425d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1afa99b6-6221-4732-be61-ecad0a4d459f",
            "compositeImage": {
                "id": "0c32a9eb-b733-48b4-b899-8a4921065afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ca915c-c327-473e-83c1-e5d4402c425d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5af5187-9344-46b6-a2c4-0ee8acf7e7c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ca915c-c327-473e-83c1-e5d4402c425d",
                    "LayerId": "c6ec3a1f-ee70-4eb0-8d32-876842687648"
                }
            ]
        },
        {
            "id": "a4bb1e84-370d-4642-813a-52ca920aba56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1afa99b6-6221-4732-be61-ecad0a4d459f",
            "compositeImage": {
                "id": "5a1b32c9-70bd-4126-92e7-ea1c5ac10363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4bb1e84-370d-4642-813a-52ca920aba56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd0e0313-5da7-4e93-9898-ed28f612da1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4bb1e84-370d-4642-813a-52ca920aba56",
                    "LayerId": "c6ec3a1f-ee70-4eb0-8d32-876842687648"
                }
            ]
        },
        {
            "id": "e184ea08-6b7e-429e-b3c5-9b7f868c2d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1afa99b6-6221-4732-be61-ecad0a4d459f",
            "compositeImage": {
                "id": "ef2da983-ba53-4b32-973c-59384a2cc11e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e184ea08-6b7e-429e-b3c5-9b7f868c2d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f214b71e-8a03-4c5b-80af-cf1d347df726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e184ea08-6b7e-429e-b3c5-9b7f868c2d5e",
                    "LayerId": "c6ec3a1f-ee70-4eb0-8d32-876842687648"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c6ec3a1f-ee70-4eb0-8d32-876842687648",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1afa99b6-6221-4732-be61-ecad0a4d459f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}