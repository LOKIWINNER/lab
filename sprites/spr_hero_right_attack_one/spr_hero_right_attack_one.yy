{
    "id": "60429774-4e44-44de-9208-3a90341afc10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_attack_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 40,
    "bbox_right": 63,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "611bb69f-8fee-436d-a6f1-6be8755d52a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "216f3a87-7dea-402f-bc7b-b772bfa4ec11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "611bb69f-8fee-436d-a6f1-6be8755d52a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb36495-8ace-4d16-940a-b39727e30a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611bb69f-8fee-436d-a6f1-6be8755d52a9",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "8cb702b1-fe24-46b2-8bad-ec03a4a7c7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "1a6f85cc-9adc-4e86-8e11-00e2048180ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb702b1-fe24-46b2-8bad-ec03a4a7c7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c11585f-dd2f-4021-af05-58e9ff6663dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb702b1-fe24-46b2-8bad-ec03a4a7c7ea",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "0f1a4fc6-bd82-4fdf-abc5-932e04c276ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "afe46201-6b1c-4dc4-99d8-dfa9f5a01940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f1a4fc6-bd82-4fdf-abc5-932e04c276ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a64ec5-6f6d-49e2-b8c5-bbe5d91b5513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f1a4fc6-bd82-4fdf-abc5-932e04c276ba",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "48250837-b6b7-4674-9c4b-1e44bd70c479",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "f7c8bc03-2339-49ba-956d-c3499b78195d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48250837-b6b7-4674-9c4b-1e44bd70c479",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a237025d-a123-43f6-9e40-01ed1bd7e484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48250837-b6b7-4674-9c4b-1e44bd70c479",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "062b0cbd-0670-493d-870d-4427228e68c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "a7d90163-bc06-4d9c-83a9-3cf0cdd5bcf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062b0cbd-0670-493d-870d-4427228e68c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33583642-5d7f-4c83-876b-2e92232859f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062b0cbd-0670-493d-870d-4427228e68c3",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "6059b999-96a2-481a-a479-e09383ebf253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "aa314075-81dd-4398-af14-e60c6b4ba3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6059b999-96a2-481a-a479-e09383ebf253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de60147e-b4d5-4ed2-b75f-751e9222ac70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6059b999-96a2-481a-a479-e09383ebf253",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "02aeb740-3377-4c7b-9985-be7c88dd97e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "e213d455-09d2-406a-8990-c8e5e1145739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02aeb740-3377-4c7b-9985-be7c88dd97e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04061206-bf5a-4f9a-9d03-9f5b0bd8fa61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02aeb740-3377-4c7b-9985-be7c88dd97e0",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        },
        {
            "id": "b52633ac-8881-4bdc-9ab8-b601d127f419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "compositeImage": {
                "id": "3684886c-b65f-4be0-a10e-0ba09ccc324e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52633ac-8881-4bdc-9ab8-b601d127f419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b54127a-2a8b-4ad3-a91e-6ec085cf7516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52633ac-8881-4bdc-9ab8-b601d127f419",
                    "LayerId": "dd751269-52a2-468f-a740-61cd02a912ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dd751269-52a2-468f-a740-61cd02a912ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60429774-4e44-44de-9208-3a90341afc10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}