{
    "id": "be6fdf71-1a16-432c-8846-98740d00d193",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 2,
    "bbox_right": 61,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03b7ddbf-2902-45e4-9ea1-ac1daf0d374e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "55b2ebbc-859e-4392-8c38-5825b8d198a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b7ddbf-2902-45e4-9ea1-ac1daf0d374e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "169ec4b4-e78d-4d40-bdc0-7cd7640abf03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b7ddbf-2902-45e4-9ea1-ac1daf0d374e",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c2908235-678f-48a1-902e-a62037c1ffdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "5a569ed9-0945-4956-a935-f6375aac97ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2908235-678f-48a1-902e-a62037c1ffdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "187ec88e-62fe-407e-b3ab-e2c775c05e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2908235-678f-48a1-902e-a62037c1ffdb",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c5302497-6748-45dc-85c5-5e65079c34ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "3fe1a6c2-6765-46f6-b402-3cc18bf8ebea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5302497-6748-45dc-85c5-5e65079c34ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acf5e6b-b85e-4796-8eee-47177a2a7673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5302497-6748-45dc-85c5-5e65079c34ce",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "bdda7c8b-a45e-4389-a9a7-4226030f31f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "360c5c35-73df-4e1c-8903-558eea5beede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdda7c8b-a45e-4389-a9a7-4226030f31f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "713ab484-277c-4e53-88eb-c7a6d0ab9b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdda7c8b-a45e-4389-a9a7-4226030f31f0",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "23e4239e-d39e-4dba-84d9-9a9510fd6816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "9c042b4c-30fa-4681-9200-70fab80f66a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23e4239e-d39e-4dba-84d9-9a9510fd6816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec912f1e-782b-4507-af0c-f836dd6302fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23e4239e-d39e-4dba-84d9-9a9510fd6816",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "d4709dd7-a906-4325-b95a-d034b97fc77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "0560ede4-dc4e-42bf-b8bf-27f64a63584e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4709dd7-a906-4325-b95a-d034b97fc77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d015d6-b0ff-4988-bd64-862f7177a2a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4709dd7-a906-4325-b95a-d034b97fc77d",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "683eb599-fc04-4f62-8577-cd4b7401a3a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "129f3bd3-0529-4692-9862-8e61117ca1a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "683eb599-fc04-4f62-8577-cd4b7401a3a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab709433-4b46-461a-b007-cf82a9e9dc36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "683eb599-fc04-4f62-8577-cd4b7401a3a2",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "3e18f404-36e2-47f1-ad7b-2102db3e4c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "647dd3f1-49ac-4ba4-8f3c-9bba1be12764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e18f404-36e2-47f1-ad7b-2102db3e4c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52b28332-0d65-48a7-ba71-d946ed1d2d77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e18f404-36e2-47f1-ad7b-2102db3e4c39",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "3c7a5278-34e4-4517-bd33-0216ef9b0d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "4a07eadb-e1fb-4430-9dac-c31da0498b3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7a5278-34e4-4517-bd33-0216ef9b0d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86430cb0-3f0b-44d9-b8ba-3d946408b022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7a5278-34e4-4517-bd33-0216ef9b0d50",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c94c868b-7f36-4d4f-aa53-765f46c5c751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f52761d9-3a10-4c5f-b387-713cbe9ef089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94c868b-7f36-4d4f-aa53-765f46c5c751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58aa9308-483c-4e0f-8ccc-67a470e8d9a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94c868b-7f36-4d4f-aa53-765f46c5c751",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2a267bf9-02d3-48ae-8f7c-90364d415921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "efc1330b-4002-489b-b52b-49877777be48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a267bf9-02d3-48ae-8f7c-90364d415921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "808d72c6-9fdb-4e42-8106-c2f4fdeacc01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a267bf9-02d3-48ae-8f7c-90364d415921",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "3a6ba9e5-5fb1-4d85-86f2-ae6b7bfd2c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "8ef6d860-9776-41c6-bb09-a2f9c10c46d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6ba9e5-5fb1-4d85-86f2-ae6b7bfd2c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b1a481-4572-4274-a988-ba79ec507b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6ba9e5-5fb1-4d85-86f2-ae6b7bfd2c87",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "37999e2d-ba0f-4f05-8f34-2044cf33f221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "c85a6b99-b553-45b7-ab3a-69650aa571fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37999e2d-ba0f-4f05-8f34-2044cf33f221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "145bee77-37ed-47d2-b1c3-2b18f29da363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37999e2d-ba0f-4f05-8f34-2044cf33f221",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "df46084a-1d65-4510-bd2a-1ec88c2a970f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "86cd8577-93b5-4e6d-beda-e221dcde2338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df46084a-1d65-4510-bd2a-1ec88c2a970f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47a715e-6978-406c-a594-a8f5450089f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df46084a-1d65-4510-bd2a-1ec88c2a970f",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "ab38faeb-edd3-4680-8481-fe26d7b905f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "a97fd9c4-b91f-49f4-ace1-b6594c714e57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab38faeb-edd3-4680-8481-fe26d7b905f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "316c7496-995f-4a8a-9c3e-84a0c6497eb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab38faeb-edd3-4680-8481-fe26d7b905f4",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "8668ea7f-5eab-4144-b238-7c34bbb0e02f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "24bf8e38-6bf0-4aa1-b2fc-95045ef3b641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8668ea7f-5eab-4144-b238-7c34bbb0e02f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b450c091-9c7f-4077-b8f2-df07f73a2129",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8668ea7f-5eab-4144-b238-7c34bbb0e02f",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "00404b4f-471c-43c9-8b66-1f0d20c21d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "62eccd40-90f7-467b-b380-2aff8dd75455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00404b4f-471c-43c9-8b66-1f0d20c21d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c65fdb-4119-4dd5-9c69-25cea05a58b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00404b4f-471c-43c9-8b66-1f0d20c21d9d",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "e3dd6c17-17c9-4c77-abdb-63b475a67928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "6a00f7ed-d57f-4bc0-98a8-220d5774dbfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3dd6c17-17c9-4c77-abdb-63b475a67928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a587319-d7fb-4ce2-af56-5692a8f4c890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3dd6c17-17c9-4c77-abdb-63b475a67928",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "39a767eb-d4b9-4f67-9410-7e4dfe577273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "4e667f9d-1772-4c59-8b75-6f35e7877f53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39a767eb-d4b9-4f67-9410-7e4dfe577273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11bbbf77-db02-4493-8075-cd360b45139f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39a767eb-d4b9-4f67-9410-7e4dfe577273",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "48de8afe-7df8-47a2-b337-b3040b81856e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "adaaac22-107c-4eca-afe0-27775e4ce4a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48de8afe-7df8-47a2-b337-b3040b81856e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1160e9ca-ef28-4e18-9ff2-6b1e46211d20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48de8afe-7df8-47a2-b337-b3040b81856e",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "13faca7c-ed59-402d-84bf-9277334a9e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "ed692430-6876-40fd-923a-bab5dd348ca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13faca7c-ed59-402d-84bf-9277334a9e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7bb0b2-ed62-493d-a458-6b83491e4d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13faca7c-ed59-402d-84bf-9277334a9e68",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "4dacb3ca-89a5-46cd-a6a8-55f59eea1784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "fff00b41-23f0-45a1-aaf2-b53762e5feea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dacb3ca-89a5-46cd-a6a8-55f59eea1784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "429376c5-a115-44ae-b292-4c1310ffda0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dacb3ca-89a5-46cd-a6a8-55f59eea1784",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "115d1f12-59ce-44d8-9bfd-1b1be3a2e4e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b87b94fe-f55f-4c0a-9266-c417953a42a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115d1f12-59ce-44d8-9bfd-1b1be3a2e4e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595142b3-3b65-4e88-a99f-99411883bb8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115d1f12-59ce-44d8-9bfd-1b1be3a2e4e5",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "7108a21b-58b5-4f9e-b080-9122bcf6519e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "97130107-fff0-4255-ad9a-3fd015a07e01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7108a21b-58b5-4f9e-b080-9122bcf6519e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8e7ed9c-9c23-4b78-a934-ef6699cc9e23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7108a21b-58b5-4f9e-b080-9122bcf6519e",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "bc68c008-9c21-4f66-a261-15af362b6638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "80d4651a-56e2-45b2-90a7-3563b0679593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc68c008-9c21-4f66-a261-15af362b6638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "611eecb5-8cc9-4686-9f22-9bc60b9df065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc68c008-9c21-4f66-a261-15af362b6638",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "9aa04ca2-64b5-49f9-a64a-f8eb506d602a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f91c1b12-bb64-431f-bdb9-f320330f5078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aa04ca2-64b5-49f9-a64a-f8eb506d602a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78983982-f6c6-4f5e-87fd-e7a8cdf54688",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aa04ca2-64b5-49f9-a64a-f8eb506d602a",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "acdc75c5-d725-4ee5-a1b2-cdf435d2799d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "8f3aa7dc-85bb-4e48-b372-e54e6f315d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acdc75c5-d725-4ee5-a1b2-cdf435d2799d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0825393a-69cc-46b8-b669-c0b52d8da925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acdc75c5-d725-4ee5-a1b2-cdf435d2799d",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "6efd5abc-00ce-473b-ac98-cb4bee75f1e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "384a5f92-4ed7-4228-9273-e2188d09e661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6efd5abc-00ce-473b-ac98-cb4bee75f1e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b38007-04db-4426-a18b-0a8b32ea3e79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6efd5abc-00ce-473b-ac98-cb4bee75f1e7",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "ab7f67c7-9298-4d74-95a9-e78b81d7b4dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "552ca128-d927-4c39-a811-3f0c572d2d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab7f67c7-9298-4d74-95a9-e78b81d7b4dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "174f202c-29de-457a-bbdf-f19dd73a975f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab7f67c7-9298-4d74-95a9-e78b81d7b4dc",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "61b6f2b2-be0a-4712-a6be-08ea84438b9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "82712ad7-a7f9-416d-a22f-cca8f4c11946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61b6f2b2-be0a-4712-a6be-08ea84438b9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e076a20-eaab-4b16-a4df-7f6561164f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61b6f2b2-be0a-4712-a6be-08ea84438b9f",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "f6e94539-78a8-46e9-a784-e78d02a03dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "6e318790-4ebe-40ff-b491-f0c621854f55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e94539-78a8-46e9-a784-e78d02a03dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36aed998-1819-400e-8476-91b03f0a0187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e94539-78a8-46e9-a784-e78d02a03dbc",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "a950a6d2-19fd-4f01-89b4-cd5de365edbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "bc630dad-6311-481c-bda0-ebf08f9b5345",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a950a6d2-19fd-4f01-89b4-cd5de365edbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1104aa7-2451-43e3-93c0-32c184b01080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a950a6d2-19fd-4f01-89b4-cd5de365edbd",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "7958ac4b-c466-4bda-9e13-af127e2b76f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "8896e249-5b62-4a79-922a-c309de8d3154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7958ac4b-c466-4bda-9e13-af127e2b76f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02253fd-f011-4cda-b10e-799233b479cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7958ac4b-c466-4bda-9e13-af127e2b76f2",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "7a7572fe-0327-4fe2-bd60-a592bed379ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "c9d9b9da-f176-46e6-8100-06795ff8b255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7572fe-0327-4fe2-bd60-a592bed379ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55dcc850-ae5b-4b2b-9822-2a40f0da09b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7572fe-0327-4fe2-bd60-a592bed379ed",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "b068a536-099c-4e99-a2e4-9679a89401b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "a3cf79cd-1c4c-4d7f-8c0c-7336941fd0b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b068a536-099c-4e99-a2e4-9679a89401b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8926db5e-8a5c-4b3b-9f58-d6f0f238feb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b068a536-099c-4e99-a2e4-9679a89401b8",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2d971727-3772-4bcc-80cc-fc8b144e4ff9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f28cba6f-646c-4d70-b837-cb7bd71bd34f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d971727-3772-4bcc-80cc-fc8b144e4ff9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333faf9f-b356-446a-b449-50c315b33ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d971727-3772-4bcc-80cc-fc8b144e4ff9",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "dd05dd66-257f-4a81-9c6b-13ad93fede43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "27ab13d7-92d4-4c10-8310-882ec7e44fc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd05dd66-257f-4a81-9c6b-13ad93fede43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c67ef69-fe5f-4000-8256-e737ec6b2af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd05dd66-257f-4a81-9c6b-13ad93fede43",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "831632eb-7338-4700-8810-b77cdefeabe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "5c2c0dc7-a114-47af-b259-879a0137949d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "831632eb-7338-4700-8810-b77cdefeabe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45165d23-31a0-45bc-b1a4-8f140a1f4975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "831632eb-7338-4700-8810-b77cdefeabe7",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "f06c588b-2d41-4eb3-ad87-e0381f2b569a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f7c26700-90b0-4255-876f-da2e6c90d905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06c588b-2d41-4eb3-ad87-e0381f2b569a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871aca37-bb1d-4b20-8b52-8a9d8dd3c447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06c588b-2d41-4eb3-ad87-e0381f2b569a",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "da0bbed8-4e07-4e9f-bd2e-b8e4767ec7eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "eedfbfe7-3fb5-4a02-9ac5-0cb04571325a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0bbed8-4e07-4e9f-bd2e-b8e4767ec7eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb4bb85-b08e-4ee3-a981-68bf2840b6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0bbed8-4e07-4e9f-bd2e-b8e4767ec7eb",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "e3114e6d-2491-48f6-8598-ad523bb964aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "238bcd08-a2f0-4863-acf7-260e52fede52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3114e6d-2491-48f6-8598-ad523bb964aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f14deff-af79-4b18-a025-bb7eb6763b5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3114e6d-2491-48f6-8598-ad523bb964aa",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "632bc028-0b5b-4177-8e59-4def057655a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f4662dcb-5d9d-400c-9a25-401eccbf7a66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "632bc028-0b5b-4177-8e59-4def057655a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d84866-9226-4bd4-ab74-ffa205ca8240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "632bc028-0b5b-4177-8e59-4def057655a5",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "76c9c04d-ce29-4d54-b0fb-a3b71eb291ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "08badc35-b7fa-456e-a3c4-715a3d463434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c9c04d-ce29-4d54-b0fb-a3b71eb291ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5a11ef8-94dd-4855-906b-dfed4d3703c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c9c04d-ce29-4d54-b0fb-a3b71eb291ef",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2394e4d9-d50d-4b54-849c-26044460faf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "85466496-72cc-49e2-a209-1e9b2bf3ba64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2394e4d9-d50d-4b54-849c-26044460faf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eebc503e-ddb4-47e1-ab3f-01426a974644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2394e4d9-d50d-4b54-849c-26044460faf1",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2d12a1ba-9133-4913-a605-ff21b730134d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b7a6dbad-b65f-4fc4-9000-01a3d3adde73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d12a1ba-9133-4913-a605-ff21b730134d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b3ff8f-dbfa-4675-83ee-0acae8439e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d12a1ba-9133-4913-a605-ff21b730134d",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "65f075cc-e7e4-4a28-9c54-5263e90e2684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "0e13209b-edc8-499a-8a32-86a8cf585aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f075cc-e7e4-4a28-9c54-5263e90e2684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac401524-6015-4d8a-ae0b-e394e8b71bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f075cc-e7e4-4a28-9c54-5263e90e2684",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "36952843-e6b5-4d5f-b215-f764c4db8279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "55198cd3-32b2-4066-9fec-67c523c395cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36952843-e6b5-4d5f-b215-f764c4db8279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0759db9-9d0f-4a19-8139-9f1210e86bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36952843-e6b5-4d5f-b215-f764c4db8279",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "5cc36970-49ff-4a13-a53f-7fa96630e81d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "756be30a-1d30-4d1b-9d9c-366bb75ce3f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cc36970-49ff-4a13-a53f-7fa96630e81d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7153a130-c932-4d92-b045-64364fe745a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cc36970-49ff-4a13-a53f-7fa96630e81d",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "19d2be44-f98a-4f7d-8f46-7d62b15e4a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "91f87860-c221-4649-bb0a-28b38744991a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19d2be44-f98a-4f7d-8f46-7d62b15e4a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ec0c9c-5e36-4e3c-b60a-1ef546ab6633",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19d2be44-f98a-4f7d-8f46-7d62b15e4a5a",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "5da7b1ec-e4e1-4047-a516-10320d4d1849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "1dbc1443-00f8-4a62-8c66-455037ab2277",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da7b1ec-e4e1-4047-a516-10320d4d1849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4612a984-739e-4c91-a03b-123d59619c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da7b1ec-e4e1-4047-a516-10320d4d1849",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "64a7d465-cb1c-4b23-be6c-d5cacca558aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "ed2a17ec-b0a7-4e93-8fc3-f01590ac7e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a7d465-cb1c-4b23-be6c-d5cacca558aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af06715e-34ed-4d28-802e-2c39ae918951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a7d465-cb1c-4b23-be6c-d5cacca558aa",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c20c10f3-e569-4c38-9170-c89a2f687bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "2f1561f7-8535-4083-896d-29a90c3a70b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c20c10f3-e569-4c38-9170-c89a2f687bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05a1debc-6bbe-4bb9-8220-07b32a4f1dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20c10f3-e569-4c38-9170-c89a2f687bcf",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "f011969a-c093-4416-bfbd-e984aa1abdd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "cf8f839d-5354-428d-ad22-07697bb3de1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f011969a-c093-4416-bfbd-e984aa1abdd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5147bb97-026a-4b6e-ae6f-e8631b84351c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f011969a-c093-4416-bfbd-e984aa1abdd3",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2aa13dd6-5c9b-4cce-9344-e67430187374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "7a1969cc-5063-40ac-a0e9-b8f272596f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa13dd6-5c9b-4cce-9344-e67430187374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ae150d-e804-436e-89b1-795a217bec56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa13dd6-5c9b-4cce-9344-e67430187374",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "fa9af558-5880-4771-9c76-928a69b24009",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "02b1e47a-08c3-4dbb-beb1-1c8e3030896d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa9af558-5880-4771-9c76-928a69b24009",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae77fb9-d670-4120-b27f-0041f5c3c512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa9af558-5880-4771-9c76-928a69b24009",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c6ca6570-f824-40d6-8bde-dc30b58bd772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "57d2881e-16a9-4715-ba2b-c94041b05a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ca6570-f824-40d6-8bde-dc30b58bd772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c310b887-fed5-4fb5-92c5-dd8409846c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ca6570-f824-40d6-8bde-dc30b58bd772",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "9bceb3e3-b072-41b5-a805-d405eb283d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "1813ff00-c118-431f-89ce-f7f7ca0cebac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bceb3e3-b072-41b5-a805-d405eb283d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "542c0877-1c9e-4486-866e-476ca7b035af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bceb3e3-b072-41b5-a805-d405eb283d80",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "56e0809a-31b3-44fa-9b88-b63eb41989f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "439c0767-f412-454a-9d51-2aac24876def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e0809a-31b3-44fa-9b88-b63eb41989f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f267a47-327a-4c2f-bbe6-bbc3f6395799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e0809a-31b3-44fa-9b88-b63eb41989f9",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "131c03c7-dc7c-42df-ba01-0555c192c0d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "3ce6822a-e01e-443a-8c11-f55823e41d73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "131c03c7-dc7c-42df-ba01-0555c192c0d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd37f45-7361-4786-87b4-2d5b5da9fa23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "131c03c7-dc7c-42df-ba01-0555c192c0d2",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "349b9fa5-95d4-491f-b7ca-4666ede1c527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b89bccf9-df30-4e93-a6ac-c895d46c7a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349b9fa5-95d4-491f-b7ca-4666ede1c527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd57e07a-bc7c-45b1-9ec1-9ed6871a156f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349b9fa5-95d4-491f-b7ca-4666ede1c527",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "cc9a871a-24eb-422d-8daf-2ced8c13860a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f60ae586-e57e-4d4b-9803-d1e99ca3defe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9a871a-24eb-422d-8daf-2ced8c13860a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f43628-4fe5-41c3-b071-a1bf2caf7256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9a871a-24eb-422d-8daf-2ced8c13860a",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "42647239-fed0-451c-95fa-be02221cbf29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "1f68e5a6-f1ed-4c29-afee-609abfec10e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42647239-fed0-451c-95fa-be02221cbf29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "024cbfce-d667-4ee5-b11a-31e2668db8fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42647239-fed0-451c-95fa-be02221cbf29",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "176beac1-e901-420e-ae29-7fa8977233bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "754f03ff-968e-4712-95e4-da72621f3735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176beac1-e901-420e-ae29-7fa8977233bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a576493f-e7bb-4f0b-9c48-7b4dc2e8ec5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176beac1-e901-420e-ae29-7fa8977233bc",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "0446eb2a-44ea-4a35-b93b-9e4e3e83de60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "038aa568-1dab-4125-ad3a-6fbe68cea5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0446eb2a-44ea-4a35-b93b-9e4e3e83de60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2005a932-389f-4e91-a1de-0cec400f5d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0446eb2a-44ea-4a35-b93b-9e4e3e83de60",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "7fc8391d-0939-46bd-b072-ac14bcf0bd70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "64126f91-cfb0-4866-8aeb-dd70161b108c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fc8391d-0939-46bd-b072-ac14bcf0bd70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eda46aa5-ffa5-4a26-9171-e9cdf09d679f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fc8391d-0939-46bd-b072-ac14bcf0bd70",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "02f0e0b0-1828-4dc7-8ed7-d5bc98e7c6ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "dd8afb8d-7e7f-4de8-88a8-d373847ce0af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f0e0b0-1828-4dc7-8ed7-d5bc98e7c6ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f4428f6-b320-43de-835c-94544859b3c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f0e0b0-1828-4dc7-8ed7-d5bc98e7c6ef",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c8dcb818-80ff-4343-9460-6d0499247d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "2267a20a-a7c8-4074-980a-e44eaf8996e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8dcb818-80ff-4343-9460-6d0499247d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92eaa712-5c64-424a-a96d-f44e520c88e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8dcb818-80ff-4343-9460-6d0499247d60",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "346813ec-c6f8-4293-8376-951069e7e022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "c172361e-2c12-4296-8518-1abd3d03d69b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "346813ec-c6f8-4293-8376-951069e7e022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "124c1477-7499-46fa-a629-72602558dfb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346813ec-c6f8-4293-8376-951069e7e022",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "142dc0a6-28b8-4a5e-84c2-3156e5c25762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b287d122-a0da-4c40-99e1-eeaf99856470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "142dc0a6-28b8-4a5e-84c2-3156e5c25762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cee668a-175b-4e1e-9cba-76195528aef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "142dc0a6-28b8-4a5e-84c2-3156e5c25762",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "1e311409-23e5-455e-a211-d125f17c80ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "d3c098f6-d00d-4307-b153-9a50198701ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e311409-23e5-455e-a211-d125f17c80ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d191202-fa5e-4594-b2c8-78625be48531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e311409-23e5-455e-a211-d125f17c80ee",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "b37733b9-78d6-4390-81b0-c5f7c4fd8e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "3f46804c-88cb-4edd-b411-8df00e621689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b37733b9-78d6-4390-81b0-c5f7c4fd8e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b36a83f-8542-440e-b8ef-8d158f9b26f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b37733b9-78d6-4390-81b0-c5f7c4fd8e8b",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2f2f4748-2cd7-4e36-a4fa-1ca2bc99f889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "cad35cef-8389-4656-be34-bda81e8f366b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2f4748-2cd7-4e36-a4fa-1ca2bc99f889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc2fcc5-51b4-4d4e-b347-954149915a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2f4748-2cd7-4e36-a4fa-1ca2bc99f889",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "cb08801e-3666-4f23-9bac-a3d5f2a92b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "e2fbb299-90f8-4c1e-ab0c-7a420d91ade9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb08801e-3666-4f23-9bac-a3d5f2a92b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68fa1f4a-688e-4701-975f-c1cff9133205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb08801e-3666-4f23-9bac-a3d5f2a92b27",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "949b31a5-a506-47c8-93d4-fb5d132d73f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "dafa74ef-4c7f-4dc7-adfb-818696ae3a4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949b31a5-a506-47c8-93d4-fb5d132d73f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be413b59-6652-49e1-9f2e-e7f9ef705f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949b31a5-a506-47c8-93d4-fb5d132d73f5",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "29468787-462a-453a-935b-875b48589dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "e52423a2-b2e1-4288-95e7-7a76e532243a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29468787-462a-453a-935b-875b48589dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab5cc92-a1b3-4288-ba7b-0bb5d39c3e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29468787-462a-453a-935b-875b48589dbb",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "685e10fe-504b-4e34-ac29-4df85e3b57fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "55a511fa-0129-48f0-85fd-9f3f8d88b445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685e10fe-504b-4e34-ac29-4df85e3b57fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac2e62f-78d9-4e29-aa32-6afcd463ad53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685e10fe-504b-4e34-ac29-4df85e3b57fa",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "b4cc43eb-65f0-4c32-bafb-44953b4716af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "7c5f00da-f429-4ea2-bd31-c697be246575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4cc43eb-65f0-4c32-bafb-44953b4716af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1546c4-2045-45c5-9a26-08569c089cba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4cc43eb-65f0-4c32-bafb-44953b4716af",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "114e1d5a-a021-4cc9-bbed-0263b4067d8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "3ddc8f1e-68be-44f8-8e8a-48554039ff55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114e1d5a-a021-4cc9-bbed-0263b4067d8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71681a6-a425-43e0-9d09-6afacf5381e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114e1d5a-a021-4cc9-bbed-0263b4067d8f",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "4988a354-3308-42a9-8e52-fca2c1d71feb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b10dfc06-1246-4dc9-b68e-e2ab2b811217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4988a354-3308-42a9-8e52-fca2c1d71feb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50a9d63a-d087-49e4-85d6-3c168a3e6d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4988a354-3308-42a9-8e52-fca2c1d71feb",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "b559e971-1f69-49d0-81f6-686052aac5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "2278f834-3cdb-432f-8c4e-65dc50311c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b559e971-1f69-49d0-81f6-686052aac5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1f48ae-280d-4044-b54c-d855fbe2550d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b559e971-1f69-49d0-81f6-686052aac5eb",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "573cdc19-1136-4ac9-91ff-5f8cc6a65abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "6f05f390-a31f-4556-9a71-30b4ec141f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573cdc19-1136-4ac9-91ff-5f8cc6a65abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ddd882b-a07c-48d4-a59d-6deffe52cd6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573cdc19-1136-4ac9-91ff-5f8cc6a65abd",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "c7aa910f-eba8-452b-ab8a-1a2170e2f9c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "1df81f7d-67d2-4359-95c1-2b5b347d21bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7aa910f-eba8-452b-ab8a-1a2170e2f9c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69922989-36ae-4813-b6b1-ace9503df63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7aa910f-eba8-452b-ab8a-1a2170e2f9c0",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "db9333a8-3b7a-4aaf-8af7-8069ffe1ac2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "ec036e1b-7387-487c-86d8-69bf8afe0569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db9333a8-3b7a-4aaf-8af7-8069ffe1ac2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c91d1796-8b5f-4c9d-9498-268686505108",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db9333a8-3b7a-4aaf-8af7-8069ffe1ac2b",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2edd3f5a-2d83-49f8-8acd-188ee2b8cf00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "29d4082d-3051-4457-9dee-d6da4d643300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2edd3f5a-2d83-49f8-8acd-188ee2b8cf00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42cb29c-bc1c-43f5-ac23-bc0f7fa4adb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2edd3f5a-2d83-49f8-8acd-188ee2b8cf00",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "a020b9a4-c8b3-465b-9282-6ce5f13d9acb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "fe3520e5-6c36-475b-9b2f-b9eff0329763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a020b9a4-c8b3-465b-9282-6ce5f13d9acb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d500db2f-078a-4693-bc4c-a9736ace55f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a020b9a4-c8b3-465b-9282-6ce5f13d9acb",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "bdeac121-6e7a-4829-816c-ed55eafbbf1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "56169044-eb1c-4343-9031-7fac20aff5fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdeac121-6e7a-4829-816c-ed55eafbbf1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fac853c-3aca-40a4-86e6-4ed0ee12d543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdeac121-6e7a-4829-816c-ed55eafbbf1f",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "a73a0fec-9933-4af3-a83d-f99a1c656c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "98d66ae1-b45a-4400-b647-d773d23a0692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a73a0fec-9933-4af3-a83d-f99a1c656c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb0edf5-2bff-4d80-843b-1adc4cc56b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a73a0fec-9933-4af3-a83d-f99a1c656c37",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "7fada596-6859-438d-b6f3-18bde351112a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "206d0ea9-4f16-43ba-b558-f00215804d2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fada596-6859-438d-b6f3-18bde351112a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1e224d-4ba2-4811-985e-301cdfd4a462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fada596-6859-438d-b6f3-18bde351112a",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "30b0be5a-0307-4a4a-b651-a741780492e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "6664775b-46e7-4b56-a8f4-b26163426c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b0be5a-0307-4a4a-b651-a741780492e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35900c3-e495-45ec-a761-b7cb5c4918f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b0be5a-0307-4a4a-b651-a741780492e1",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "adc4d188-c997-4759-a76c-e5d3e7ad84a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "5eb24690-9315-4cfd-ad09-4ca23a96bf02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adc4d188-c997-4759-a76c-e5d3e7ad84a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6728cb65-3424-4ea7-bd92-1c761604e770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adc4d188-c997-4759-a76c-e5d3e7ad84a0",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "8a22ec74-d149-45a1-bda2-f7ee411fc602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "c846a212-0dae-4420-abb7-b305c23e688f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a22ec74-d149-45a1-bda2-f7ee411fc602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f002f3-9dcd-4cc9-8342-1bfcdde8025d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a22ec74-d149-45a1-bda2-f7ee411fc602",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "00ac37f2-f9be-4760-ac63-e3928bc9b4ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "95cd8a99-2fa7-45a4-9968-dc590d6ccd3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00ac37f2-f9be-4760-ac63-e3928bc9b4ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612fef2f-0cca-4cd4-888a-a5c566ac9cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00ac37f2-f9be-4760-ac63-e3928bc9b4ed",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "1403bb09-5fb4-4188-9c92-7c4bdec76bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "7212c0ec-9ac6-4fc4-a69c-0fee1b5d888c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1403bb09-5fb4-4188-9c92-7c4bdec76bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08904d81-891a-406c-964f-35ec33d83a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1403bb09-5fb4-4188-9c92-7c4bdec76bcc",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "1d203f18-10bb-4c4a-8db8-0ef5dd883dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "d0d49d05-de6b-44ec-9960-23dbb66bb11b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d203f18-10bb-4c4a-8db8-0ef5dd883dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cb21576-28e2-4df3-a4a0-58d6b32d28e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d203f18-10bb-4c4a-8db8-0ef5dd883dc8",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "67a7e56c-5cbb-448e-8bd7-cfc18a8e621f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "4aea6157-d281-4772-ad92-2bbf3a4db13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a7e56c-5cbb-448e-8bd7-cfc18a8e621f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0382db92-9666-40db-a39f-aa8cd71b1668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a7e56c-5cbb-448e-8bd7-cfc18a8e621f",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "e44ec734-783f-402a-baa6-d16252140268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "9d997da3-891f-4109-9bd2-0498c7c9acab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e44ec734-783f-402a-baa6-d16252140268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba2580d-3f63-46f4-b74d-f0c369e301f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44ec734-783f-402a-baa6-d16252140268",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "33bf3ce9-5e89-46d7-b336-d2752cd3c92b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "821cef23-4b59-4b57-bf8f-8c9817aabd77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33bf3ce9-5e89-46d7-b336-d2752cd3c92b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4e67e9-89df-49a1-927b-b34bb16d7d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33bf3ce9-5e89-46d7-b336-d2752cd3c92b",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "481398b3-0a90-4161-bc21-fda065dbcf72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "a26a1867-72de-4fd1-a548-0146ea505bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "481398b3-0a90-4161-bc21-fda065dbcf72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc762fd2-d3a7-42a7-9285-c02205a4938f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481398b3-0a90-4161-bc21-fda065dbcf72",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "96989daf-fc84-4144-b356-1d16ea8b90a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "af8cf94a-e908-49df-a5ae-a6fb7c9acf6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96989daf-fc84-4144-b356-1d16ea8b90a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "695145f3-ed3f-4af9-aa20-d7f951d112ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96989daf-fc84-4144-b356-1d16ea8b90a0",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "e4303d85-36cb-4302-8b7c-1089b9405dd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "7f605fd2-e585-42e7-aefd-257283857b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4303d85-36cb-4302-8b7c-1089b9405dd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2f22bc1-537c-40cf-94bc-d7b4645ba62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4303d85-36cb-4302-8b7c-1089b9405dd9",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "30ca5281-f724-4481-8867-b4b982839615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "e72dd281-a3b6-4bc0-997f-bacb636e5172",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ca5281-f724-4481-8867-b4b982839615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad06f2db-a784-4277-a29c-0397ffb9a194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ca5281-f724-4481-8867-b4b982839615",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "4953c31c-2552-46a2-ac5e-adaa2c7e48df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "0b270ba1-8b65-4ad9-8214-fb108108fefd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4953c31c-2552-46a2-ac5e-adaa2c7e48df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ef828b-04e7-4f0f-8561-82c0b12f1228",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4953c31c-2552-46a2-ac5e-adaa2c7e48df",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "4d689698-f828-4262-a8ff-f4a64d630358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b0fa8042-c09b-4e09-b484-bcda42d112ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d689698-f828-4262-a8ff-f4a64d630358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37d7665-2240-4ec2-8d5f-fe92e59bee2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d689698-f828-4262-a8ff-f4a64d630358",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "9856f25d-908d-4906-af96-2c66050ecd5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b953b6f4-6f9c-4030-88e1-deb64ca53d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9856f25d-908d-4906-af96-2c66050ecd5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81f4b6dc-910b-40fa-a0ac-ae6179e6b7b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9856f25d-908d-4906-af96-2c66050ecd5e",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "dede3c5c-fe9a-4c87-804f-bde0c0ce73d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "71a59aa5-f4e2-4038-ab5e-3b501e699bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dede3c5c-fe9a-4c87-804f-bde0c0ce73d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b630f42-3bb2-49ec-be97-42646f67300d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dede3c5c-fe9a-4c87-804f-bde0c0ce73d7",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "73a02d98-be2c-4516-bee9-d60cf6c1e49d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "33428ec3-2bc3-469c-9c02-0b3b46d09e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a02d98-be2c-4516-bee9-d60cf6c1e49d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6071f6f-1c28-4ef5-84f9-d3dd9d8de765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a02d98-be2c-4516-bee9-d60cf6c1e49d",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "35008c00-14ab-48ee-919e-fb3bf5887834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "4001f06c-55f5-48d1-9084-7997fcbdacc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35008c00-14ab-48ee-919e-fb3bf5887834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c65b9b00-b7da-40e1-9352-8f5766982a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35008c00-14ab-48ee-919e-fb3bf5887834",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "2f1e584c-870f-413b-8204-8ae114416c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "8d23f648-9d52-4d3a-aa2c-a8cab67b0669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f1e584c-870f-413b-8204-8ae114416c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe1b306-725b-46f2-83d4-cc9603c4ef01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f1e584c-870f-413b-8204-8ae114416c1c",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "306e2da1-f734-440f-85c8-5b8162abc62b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "1032c1b6-e94c-4314-a5e9-16fc86f3101a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306e2da1-f734-440f-85c8-5b8162abc62b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cba5bb81-38c8-47c1-849f-e2a9960158c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306e2da1-f734-440f-85c8-5b8162abc62b",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "53017e5b-fd55-44c8-bd57-98e33b93eaf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "699a4224-cdb9-48ae-8e2b-f8f4ad30bf4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53017e5b-fd55-44c8-bd57-98e33b93eaf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a11829-ef3a-4c92-b1d9-d135d5dd081e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53017e5b-fd55-44c8-bd57-98e33b93eaf8",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "365268fc-a5b4-46a3-9726-10ee24c8b4ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "3e0d9cde-c068-49e3-aaac-e276b6690afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "365268fc-a5b4-46a3-9726-10ee24c8b4ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28aee3b1-f2db-4059-b469-37bcf22e8628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "365268fc-a5b4-46a3-9726-10ee24c8b4ec",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "230775d7-23ec-4572-a68d-6a1745bffebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "dbe87ceb-c242-4d09-86b0-80f0f43b1948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "230775d7-23ec-4572-a68d-6a1745bffebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e50bf3-e251-43c4-a1a4-45320463e723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "230775d7-23ec-4572-a68d-6a1745bffebe",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "8cb0862d-0053-453e-94aa-05ad6deb42f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "5059b19c-f7b8-4a6d-965c-29b286d09e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb0862d-0053-453e-94aa-05ad6deb42f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da9d859f-3d1c-4013-a4f3-dc438963e6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb0862d-0053-453e-94aa-05ad6deb42f8",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "7995897b-ea1f-46df-905c-cf01a22f8e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "fb7c3f6a-e627-4d0d-a7f4-2c67d51c45eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7995897b-ea1f-46df-905c-cf01a22f8e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d202ae-75e6-4716-854e-dd4a605b4b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7995897b-ea1f-46df-905c-cf01a22f8e58",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "9ee56b6c-2c2d-4182-92a4-f09a55b06492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "adce7671-3b68-4444-81f7-fdcafd98fa21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee56b6c-2c2d-4182-92a4-f09a55b06492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ffef5c-4d34-4a4d-a3d2-e357a54c5bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee56b6c-2c2d-4182-92a4-f09a55b06492",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "708cedad-20af-4215-aa11-b624682b69b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "bad6e382-f167-4b7c-a383-1ef3313b2873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708cedad-20af-4215-aa11-b624682b69b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8269327-7e31-4094-a43a-739c6fd6978d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708cedad-20af-4215-aa11-b624682b69b3",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "0fd0ffc7-c495-403d-ac3d-957fe28c5afd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "7365a00c-07fd-4899-824c-3456da0bfb57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd0ffc7-c495-403d-ac3d-957fe28c5afd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91894a3d-5b68-4961-9cfa-1e9aca9b22eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd0ffc7-c495-403d-ac3d-957fe28c5afd",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "653d416a-299b-45f4-9809-445d9fdf11e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "7476f6e5-2ae5-42e1-bfdc-d13b551b78f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "653d416a-299b-45f4-9809-445d9fdf11e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "157f1f66-f195-4435-bf03-696fdda2df6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "653d416a-299b-45f4-9809-445d9fdf11e3",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "ac22949a-2988-43f5-a459-fd1ec2f2ab75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "f8133132-663c-4b8f-9dbd-50a7b8293713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac22949a-2988-43f5-a459-fd1ec2f2ab75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee319da6-7817-4090-8b19-9bdd53ab920c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac22949a-2988-43f5-a459-fd1ec2f2ab75",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "ac0178bc-f0e3-47bd-a5e4-ad856cebc661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "1c7cb0a4-8828-4a16-9251-3ddfe25e04d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac0178bc-f0e3-47bd-a5e4-ad856cebc661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a20d01-e5e6-41b8-8d3c-cd6e1a9e1937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac0178bc-f0e3-47bd-a5e4-ad856cebc661",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        },
        {
            "id": "ab34d900-51ad-4459-b076-92f41b6149dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "compositeImage": {
                "id": "b17fa47f-c043-43c6-b6c6-d7061f93c3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab34d900-51ad-4459-b076-92f41b6149dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7240e497-d8d8-4329-b527-5484408f0d57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab34d900-51ad-4459-b076-92f41b6149dd",
                    "LayerId": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ebaaf5ac-4a87-4cf8-ae61-0f599c74be12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be6fdf71-1a16-432c-8846-98740d00d193",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}