{
    "id": "1346967b-3605-4239-a2da-77899b288e21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 423,
    "bbox_left": 0,
    "bbox_right": 273,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75253a5d-5c5a-44ac-aa0f-30dd04522de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1346967b-3605-4239-a2da-77899b288e21",
            "compositeImage": {
                "id": "c18d3839-ea38-495d-ae2d-30409ea9ae1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75253a5d-5c5a-44ac-aa0f-30dd04522de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5cab0c-9548-4021-9a83-6186118a9586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75253a5d-5c5a-44ac-aa0f-30dd04522de9",
                    "LayerId": "aadd080e-64e7-4014-a168-c61a2ca4297d"
                }
            ]
        },
        {
            "id": "1689d0b4-6d19-47e9-bdc8-7a0ee0af4547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1346967b-3605-4239-a2da-77899b288e21",
            "compositeImage": {
                "id": "c13d939e-25d5-4454-8c1c-47b4c696eff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1689d0b4-6d19-47e9-bdc8-7a0ee0af4547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4dbb355-a767-47a8-9c96-b6d1b4fd8880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1689d0b4-6d19-47e9-bdc8-7a0ee0af4547",
                    "LayerId": "aadd080e-64e7-4014-a168-c61a2ca4297d"
                }
            ]
        },
        {
            "id": "71b62661-683e-4f4b-aed9-08cf86fe6b7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1346967b-3605-4239-a2da-77899b288e21",
            "compositeImage": {
                "id": "337a89cb-516e-4c50-9100-ba6f807a1df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b62661-683e-4f4b-aed9-08cf86fe6b7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b38174b8-19b4-44a9-ace9-fa3c8a42db57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b62661-683e-4f4b-aed9-08cf86fe6b7c",
                    "LayerId": "aadd080e-64e7-4014-a168-c61a2ca4297d"
                }
            ]
        },
        {
            "id": "1c91fbbb-1578-48a3-a26a-1b30c3d4659a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1346967b-3605-4239-a2da-77899b288e21",
            "compositeImage": {
                "id": "54761cff-a313-4acf-9018-fb366bb7144b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c91fbbb-1578-48a3-a26a-1b30c3d4659a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38206a9e-c291-469c-ad2c-861bb934dc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c91fbbb-1578-48a3-a26a-1b30c3d4659a",
                    "LayerId": "aadd080e-64e7-4014-a168-c61a2ca4297d"
                }
            ]
        },
        {
            "id": "766fb2c7-03ec-4b5e-9f5b-fae31fe69507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1346967b-3605-4239-a2da-77899b288e21",
            "compositeImage": {
                "id": "9affce10-d76f-4b2e-8677-37b5f64cc55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766fb2c7-03ec-4b5e-9f5b-fae31fe69507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ddf609-f985-4cb5-9bac-2b6493d1272a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766fb2c7-03ec-4b5e-9f5b-fae31fe69507",
                    "LayerId": "aadd080e-64e7-4014-a168-c61a2ca4297d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 424,
    "layers": [
        {
            "id": "aadd080e-64e7-4014-a168-c61a2ca4297d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1346967b-3605-4239-a2da-77899b288e21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 274,
    "xorig": 0,
    "yorig": 0
}