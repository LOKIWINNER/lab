{
    "id": "0cfc22c5-bbe2-41d5-ba49-3c414dbabb20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fefc775c-b4d5-4b3c-8362-b9ec8cb14ca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cfc22c5-bbe2-41d5-ba49-3c414dbabb20",
            "compositeImage": {
                "id": "83514aef-897b-43e5-962d-4533d820af87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fefc775c-b4d5-4b3c-8362-b9ec8cb14ca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a735e7b1-24f6-47f5-887a-6a06030f8c00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fefc775c-b4d5-4b3c-8362-b9ec8cb14ca5",
                    "LayerId": "0dca3b77-4220-4e58-8870-9cbbae10b50a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0dca3b77-4220-4e58-8870-9cbbae10b50a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cfc22c5-bbe2-41d5-ba49-3c414dbabb20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}