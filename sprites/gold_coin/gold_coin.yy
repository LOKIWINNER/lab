{
    "id": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gold_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c2c561e-d32a-4502-8d9d-33874f5e0214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "b4abf6dc-b057-44cd-844b-fae284186c0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c2c561e-d32a-4502-8d9d-33874f5e0214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96d24271-2d4d-4ca7-90d7-9f3d97087244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c2c561e-d32a-4502-8d9d-33874f5e0214",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "f498fd9b-669c-49ca-b296-d7e051e7a64a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "31f828bb-0123-4fa0-9737-3d95aa8ce64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f498fd9b-669c-49ca-b296-d7e051e7a64a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9ff8ff-48d2-42b4-ae38-d833201cab55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f498fd9b-669c-49ca-b296-d7e051e7a64a",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "828f805e-56f6-4a6f-a05b-2daad6b733aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "3178e790-cf5b-48dc-b988-61c43e3a3565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "828f805e-56f6-4a6f-a05b-2daad6b733aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeeef7bd-6452-477b-bb5a-c0dee936f870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "828f805e-56f6-4a6f-a05b-2daad6b733aa",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "b8f48283-3279-4250-bee0-0302187de020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "49029a46-6ea3-4fd7-acae-9a8e2b3e3b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f48283-3279-4250-bee0-0302187de020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce4a454e-05b9-4b1a-b50d-5496698da1a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f48283-3279-4250-bee0-0302187de020",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "660a3533-da95-4b18-9757-183886338eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "81df6b27-ff69-46e4-82f6-6a2e3e74b0ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660a3533-da95-4b18-9757-183886338eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc11665-f08a-40ba-af06-cbaa3d3cb4da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660a3533-da95-4b18-9757-183886338eaf",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "86002817-94de-40cf-be84-5b696e88cc95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "dc2edab5-9740-46fc-8cac-f3818d292be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86002817-94de-40cf-be84-5b696e88cc95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb014801-59bc-4ea5-a79f-604d033db898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86002817-94de-40cf-be84-5b696e88cc95",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "65d533da-f75f-4b2b-b754-6e540a42cab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "02d32336-04eb-432a-8dee-97b148334d34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d533da-f75f-4b2b-b754-6e540a42cab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58a7394a-f895-4b33-96de-bf9c4f1af086",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d533da-f75f-4b2b-b754-6e540a42cab5",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        },
        {
            "id": "ebcd0d1d-d105-4160-88b9-359cb55fc977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "compositeImage": {
                "id": "3c83b593-2d01-422f-b5ca-724af94d6f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebcd0d1d-d105-4160-88b9-359cb55fc977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56a00c2a-7d78-41c3-b525-58884e5fa71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebcd0d1d-d105-4160-88b9-359cb55fc977",
                    "LayerId": "843db7b9-0514-41e6-9f60-4aefa4264655"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "843db7b9-0514-41e6-9f60-4aefa4264655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13f0a6fc-3fda-44e5-b162-0d3ec7b8bb39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 28
}