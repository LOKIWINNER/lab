{
    "id": "dfa2b25f-af7f-4bf7-8096-68cae28e35aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 20,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c650ea2-5de7-44e9-8e8b-b06cef1e0205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfa2b25f-af7f-4bf7-8096-68cae28e35aa",
            "compositeImage": {
                "id": "4cab058c-675a-47da-a381-8d3b6c475de1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c650ea2-5de7-44e9-8e8b-b06cef1e0205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4752c81-ddd3-4870-921e-c00f8e63febf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c650ea2-5de7-44e9-8e8b-b06cef1e0205",
                    "LayerId": "5a051e05-d70c-4da1-82da-6741cd4f13f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "5a051e05-d70c-4da1-82da-6741cd4f13f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfa2b25f-af7f-4bf7-8096-68cae28e35aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 11,
    "yorig": 11
}