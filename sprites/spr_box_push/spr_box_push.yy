{
    "id": "d6ed6990-c3a0-4ef1-9d2f-d17b1364f1e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box_push",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b67d5a8b-5425-4769-9ec2-db9f77dba489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6ed6990-c3a0-4ef1-9d2f-d17b1364f1e8",
            "compositeImage": {
                "id": "f65ab240-af2a-4c34-9adb-63ea137b09a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b67d5a8b-5425-4769-9ec2-db9f77dba489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f9ebd5-9195-4f31-9bba-42ec3ad13722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b67d5a8b-5425-4769-9ec2-db9f77dba489",
                    "LayerId": "661e908e-a97f-42a2-a5aa-f7fc6050da49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "661e908e-a97f-42a2-a5aa-f7fc6050da49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6ed6990-c3a0-4ef1-9d2f-d17b1364f1e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 16
}