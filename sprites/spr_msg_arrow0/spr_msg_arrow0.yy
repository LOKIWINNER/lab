{
    "id": "243b2433-3283-4f48-91fa-9552bdcd9b9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_arrow0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48a0975e-e090-41fa-99a7-3467ce6974c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243b2433-3283-4f48-91fa-9552bdcd9b9e",
            "compositeImage": {
                "id": "342450c7-8bee-40f6-92d2-921b2e75f322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a0975e-e090-41fa-99a7-3467ce6974c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87b22ea-3383-497f-8619-10c4ec19275e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a0975e-e090-41fa-99a7-3467ce6974c6",
                    "LayerId": "53cadb4a-c98f-4b78-8ad5-508c6d1bdd0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "53cadb4a-c98f-4b78-8ad5-508c6d1bdd0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243b2433-3283-4f48-91fa-9552bdcd9b9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}