{
    "id": "3d28870d-a5a1-42b1-ae3e-aa9bea268524",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box_32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03caf6a8-87f1-489c-8da5-5fba880c08d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d28870d-a5a1-42b1-ae3e-aa9bea268524",
            "compositeImage": {
                "id": "a4d6c676-0b9e-4b79-92b2-cbd11d5ef0a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03caf6a8-87f1-489c-8da5-5fba880c08d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd2b8832-5548-4137-9499-4bdce4037daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03caf6a8-87f1-489c-8da5-5fba880c08d1",
                    "LayerId": "e86bb89b-714a-4a06-bf48-7fa590ce5bd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e86bb89b-714a-4a06-bf48-7fa590ce5bd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d28870d-a5a1-42b1-ae3e-aa9bea268524",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 32
}