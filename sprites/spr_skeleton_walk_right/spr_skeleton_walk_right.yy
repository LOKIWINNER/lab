{
    "id": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee79e1cc-d938-47d6-b986-48ca29478d1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "477d850d-1ffc-4296-8db3-25d80db07bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee79e1cc-d938-47d6-b986-48ca29478d1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52da64e8-db26-4b13-a983-0934745c27ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee79e1cc-d938-47d6-b986-48ca29478d1d",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "9013913d-7a0b-4818-9e0c-5a77f54babe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "89120896-12e1-4b9f-8cbb-77c2f1f2943d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9013913d-7a0b-4818-9e0c-5a77f54babe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b71cc0-360a-4bc8-a1ed-2b2a0a86fd9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9013913d-7a0b-4818-9e0c-5a77f54babe9",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "b8245478-8618-4d38-a970-5eaed6b6d977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "ccf06fc5-a439-4d0e-b7a2-8650c7605f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8245478-8618-4d38-a970-5eaed6b6d977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "478834b4-9e62-48fe-b096-7ad8bf06bc85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8245478-8618-4d38-a970-5eaed6b6d977",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "2e78b7e6-1821-4a56-bd54-c9a68ef989af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "9f3ab6fa-cec8-45a2-824b-2829c8cc1902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e78b7e6-1821-4a56-bd54-c9a68ef989af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd508bd5-6acc-45c4-b87f-3fa7becb316e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e78b7e6-1821-4a56-bd54-c9a68ef989af",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "e6f1e748-0193-4082-b7cf-6639b3c913d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "e4680108-62cf-4c8e-aba1-b77be714ed98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f1e748-0193-4082-b7cf-6639b3c913d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "406f1477-efeb-4dfd-812d-58dd88720481",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f1e748-0193-4082-b7cf-6639b3c913d5",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "8d9e8fca-3ecf-41ff-a7f9-66a477ebc21e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "e4a0e35f-b71f-4aec-b5d5-a0ce076f263c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9e8fca-3ecf-41ff-a7f9-66a477ebc21e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2eab36-b716-4543-93bc-12d7a5990afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9e8fca-3ecf-41ff-a7f9-66a477ebc21e",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "0d08f1b4-bf5d-4a7d-bb69-7f32ad15009d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "82730b25-cf34-4566-81fc-6fda77da1043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d08f1b4-bf5d-4a7d-bb69-7f32ad15009d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "752096ca-40ca-4fec-aef8-4a2c8a6f675a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d08f1b4-bf5d-4a7d-bb69-7f32ad15009d",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        },
        {
            "id": "ebc16879-9cbb-4372-bcf4-f65267c1a48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "compositeImage": {
                "id": "cb4341f0-6d05-48d5-9870-f7cced455a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebc16879-9cbb-4372-bcf4-f65267c1a48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d9e81b-a689-42f9-bf38-f81eb3e28244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebc16879-9cbb-4372-bcf4-f65267c1a48c",
                    "LayerId": "9287c0a9-6255-41c0-84e9-97786e09d565"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9287c0a9-6255-41c0-84e9-97786e09d565",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e00998ad-2c13-4a6b-8da5-55c7ec57a513",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}