{
    "id": "cbb72197-0f22-41f8-b592-bea3b485c4b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "792d8185-6c96-4e5e-9511-fa7fada443a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbb72197-0f22-41f8-b592-bea3b485c4b9",
            "compositeImage": {
                "id": "d3a171a5-7507-4729-ac25-89af70bd003f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "792d8185-6c96-4e5e-9511-fa7fada443a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd235a18-75e8-4a5a-a2b5-7757c462e0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "792d8185-6c96-4e5e-9511-fa7fada443a0",
                    "LayerId": "e4d7c42e-7afd-451b-a9fb-ec810cdc2dc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "e4d7c42e-7afd-451b-a9fb-ec810cdc2dc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbb72197-0f22-41f8-b592-bea3b485c4b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 5
}