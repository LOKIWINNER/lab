{
    "id": "b5206d6e-d906-4e9a-9773-4a386f648451",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_UI_new_glass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99ec0a10-6aaa-4bf0-9710-9aa67b6beaf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5206d6e-d906-4e9a-9773-4a386f648451",
            "compositeImage": {
                "id": "2e7989ec-3537-4cb0-9088-a5b32720a102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ec0a10-6aaa-4bf0-9710-9aa67b6beaf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c7135c3-8eb3-4e24-ba57-ef44f787d4ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ec0a10-6aaa-4bf0-9710-9aa67b6beaf7",
                    "LayerId": "8c8ca07c-3e71-4dd7-a936-54ebf3ac8fa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "8c8ca07c-3e71-4dd7-a936-54ebf3ac8fa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5206d6e-d906-4e9a-9773-4a386f648451",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}