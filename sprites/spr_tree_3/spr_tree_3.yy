{
    "id": "ab82a4a9-d3cb-42ea-9997-d9a78492c68c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 45,
    "bbox_right": 81,
    "bbox_top": 138,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b8b216d-4edd-44c1-9905-2a6bd115a797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab82a4a9-d3cb-42ea-9997-d9a78492c68c",
            "compositeImage": {
                "id": "e5a53c18-c1e5-4a9d-8848-c9490b5faee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b8b216d-4edd-44c1-9905-2a6bd115a797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5279221b-7bf3-4585-8f8f-747d17e47906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b8b216d-4edd-44c1-9905-2a6bd115a797",
                    "LayerId": "f22d09e4-11fe-4054-a459-126635105ca4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "f22d09e4-11fe-4054-a459-126635105ca4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab82a4a9-d3cb-42ea-9997-d9a78492c68c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 147
}