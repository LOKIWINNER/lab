{
    "id": "476e0211-2bec-4c05-99bb-9b48fcb0c68b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_attack_hold_one",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 20,
    "bbox_right": 43,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ced7c3f4-ac0e-4e15-b8ff-c3d0f94b3aee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476e0211-2bec-4c05-99bb-9b48fcb0c68b",
            "compositeImage": {
                "id": "0160ceb5-2732-4cf6-814a-b7afa9e06540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced7c3f4-ac0e-4e15-b8ff-c3d0f94b3aee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c59445d-7716-4c9b-be16-2dcde441901a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced7c3f4-ac0e-4e15-b8ff-c3d0f94b3aee",
                    "LayerId": "8ca68f8d-1b9f-4672-bf66-5e0dd30b40cc"
                }
            ]
        },
        {
            "id": "8eb1776e-7dcc-47c6-a5df-b932cbc41418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476e0211-2bec-4c05-99bb-9b48fcb0c68b",
            "compositeImage": {
                "id": "75459eaf-9e58-4ee4-bf82-f56af6e298aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb1776e-7dcc-47c6-a5df-b932cbc41418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b250bde2-4ccd-4faf-8c97-03b6b6f1d097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb1776e-7dcc-47c6-a5df-b932cbc41418",
                    "LayerId": "8ca68f8d-1b9f-4672-bf66-5e0dd30b40cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ca68f8d-1b9f-4672-bf66-5e0dd30b40cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "476e0211-2bec-4c05-99bb-9b48fcb0c68b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}