{
    "id": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_attack_two",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e75cfe2-0f6d-473d-8e68-7d40690a430a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "7896f95f-11cf-421d-a863-31c547608662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e75cfe2-0f6d-473d-8e68-7d40690a430a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f602d38-a453-47b0-aa94-46933c85933b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e75cfe2-0f6d-473d-8e68-7d40690a430a",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "09af4a03-09de-4823-9569-588bfec258f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "c836dcd6-e829-4e36-8e47-36f28564de1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09af4a03-09de-4823-9569-588bfec258f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f8c612-7572-456b-9ed8-53027c9673f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09af4a03-09de-4823-9569-588bfec258f7",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "47bc2467-d4d4-4431-94eb-f4b70e72e196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "501b4984-d806-4851-bc22-db8a04cec271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47bc2467-d4d4-4431-94eb-f4b70e72e196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad0277e7-514d-4eff-b61e-4283a192dc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47bc2467-d4d4-4431-94eb-f4b70e72e196",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "fb3495b8-b65d-452c-b59c-891ede17ffe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "b53775fa-01b6-489c-aa27-044127851e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb3495b8-b65d-452c-b59c-891ede17ffe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d492255e-cfe7-448d-8b3b-dfb1800bc8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb3495b8-b65d-452c-b59c-891ede17ffe4",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "acc8762f-109d-4804-8d6a-8aedd3341f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "653a7eb9-77ff-475b-8144-abe919a0f61c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acc8762f-109d-4804-8d6a-8aedd3341f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2f9e2f-a54a-437f-acd8-f28ddb67a733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acc8762f-109d-4804-8d6a-8aedd3341f36",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "c13609b4-2299-4a02-8606-ab7bb2b96fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "c2c4f36e-5065-438d-a93d-2528dabb3384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13609b4-2299-4a02-8606-ab7bb2b96fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64a497dd-b176-4b42-ba37-65e935ac4575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13609b4-2299-4a02-8606-ab7bb2b96fc9",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "ac4ad089-712c-4c80-b6c9-fc135f2df5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "2626271f-e94d-48a3-bec1-ebde3730c6f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4ad089-712c-4c80-b6c9-fc135f2df5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6cbda8e-3a98-4e91-8218-738be38fab18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4ad089-712c-4c80-b6c9-fc135f2df5a2",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "699593c4-d022-4cff-9665-57deaf3accc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "f090dc2b-6a62-4e84-bfe4-81f7081b56c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699593c4-d022-4cff-9665-57deaf3accc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf7d766-d950-40d5-951c-a1b5d8914744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699593c4-d022-4cff-9665-57deaf3accc9",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "8dc71f14-4913-4a53-bf67-71bba4113c2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "e09828c3-cd5c-4140-ad5c-f9001fe534f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc71f14-4913-4a53-bf67-71bba4113c2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6513c808-1fa2-44eb-8832-b3c19bd1f6a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc71f14-4913-4a53-bf67-71bba4113c2b",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "3492a83d-2981-46b7-b99e-706847914a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "02ce0106-7a9e-475c-89ec-dec09494f876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3492a83d-2981-46b7-b99e-706847914a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b2fd08-d3db-4428-8402-685f21fd93ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3492a83d-2981-46b7-b99e-706847914a4f",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        },
        {
            "id": "aa86fcc2-7d73-484b-85fb-f05e2b104982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "compositeImage": {
                "id": "42c362e0-9461-4b44-9e93-2ebcb98a4350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa86fcc2-7d73-484b-85fb-f05e2b104982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a52f9f-4a47-4e32-a037-6d76cf9a3db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa86fcc2-7d73-484b-85fb-f05e2b104982",
                    "LayerId": "6cff963e-6996-459a-a797-66d625869221"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6cff963e-6996-459a-a797-66d625869221",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72fb1632-fd42-4ced-bfe3-94e19fbe1b64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}