{
    "id": "35125bca-09ee-4750-b78c-49920b5e005f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_attack_three",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 9,
    "bbox_right": 55,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "158805a2-ef69-4615-bf04-822907e640b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "f9e1a964-3d88-42a0-8b3d-8df1a0fec18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "158805a2-ef69-4615-bf04-822907e640b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090ef493-8322-4ea4-8c8f-e3f754c60de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "158805a2-ef69-4615-bf04-822907e640b8",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "13d9d055-0f58-46ce-968f-51f5c1d8cf1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "379955a3-821c-4952-98f6-461e589594e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d9d055-0f58-46ce-968f-51f5c1d8cf1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36e50e3-eeed-4ec8-b33d-cffe903fe9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d9d055-0f58-46ce-968f-51f5c1d8cf1a",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "6976e85a-d410-4c7e-8d3f-52377e63db4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "e3e11fff-5d4e-4289-b5a9-b263224acc23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6976e85a-d410-4c7e-8d3f-52377e63db4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8ae50c-42b3-4bf2-aa9e-05db85511484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6976e85a-d410-4c7e-8d3f-52377e63db4d",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "d1db7067-0e10-441e-94eb-b6b21a664291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "2e9a2821-94bb-443c-afd1-56588d48fe32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1db7067-0e10-441e-94eb-b6b21a664291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a783a21-d0ba-45ba-9a2e-85c09ec33df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1db7067-0e10-441e-94eb-b6b21a664291",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "0254dfa6-3e21-4bce-a4c4-9676f5a86487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "fb5eaa6e-5997-4b08-8623-f6ddc30701f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0254dfa6-3e21-4bce-a4c4-9676f5a86487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c709011-6f5f-4b5e-b317-8bcd054dc859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0254dfa6-3e21-4bce-a4c4-9676f5a86487",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "0c39c0ae-595f-4007-a20b-023725de3272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "36fde690-5ade-4854-bd52-9a11f2b190fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c39c0ae-595f-4007-a20b-023725de3272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb6ca88-e899-4753-a306-4bfc3998a817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c39c0ae-595f-4007-a20b-023725de3272",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "d457c59a-6163-4231-aab9-c54fca3f47b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "d7e0acd1-abca-4767-ad1d-11d7b7092f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d457c59a-6163-4231-aab9-c54fca3f47b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09732431-ec44-492c-a5a9-9db605921d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d457c59a-6163-4231-aab9-c54fca3f47b6",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "46174dd4-30df-4fd7-aab3-eb0a40e25203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "8dec2399-d925-4932-a1ba-f929476325f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46174dd4-30df-4fd7-aab3-eb0a40e25203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c248bb43-c2bf-4f4e-bc65-e9eb15ae29a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46174dd4-30df-4fd7-aab3-eb0a40e25203",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "f3e91648-4fa9-42c2-9b4a-3de9639c6b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "f8010330-1ce8-418b-b6bb-107132b1e10d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e91648-4fa9-42c2-9b4a-3de9639c6b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc74bc1-07f1-4fe7-a671-933820ee5880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e91648-4fa9-42c2-9b4a-3de9639c6b92",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "97559ed5-bc64-49c8-870a-66d185149582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "2e92641e-e2c7-460f-9247-fe0281147e6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97559ed5-bc64-49c8-870a-66d185149582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d0d128-7f10-48c9-8128-fd680f4beadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97559ed5-bc64-49c8-870a-66d185149582",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        },
        {
            "id": "04f14e0c-dd2c-45e5-9e08-0c6dbdc20985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "compositeImage": {
                "id": "102cfadb-7d18-40d8-a41b-1e8901a10808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f14e0c-dd2c-45e5-9e08-0c6dbdc20985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd41e595-4fa9-425d-bd56-beda3fc8bba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f14e0c-dd2c-45e5-9e08-0c6dbdc20985",
                    "LayerId": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6ffdc12b-ff8c-4e84-a79c-13ebc8c40bd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35125bca-09ee-4750-b78c-49920b5e005f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}