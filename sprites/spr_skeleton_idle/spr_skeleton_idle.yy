{
    "id": "21b543b2-c139-42af-8131-a23d8e6ecf61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeleton_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "252ccb13-6e69-4dd2-98c4-ce7d3b03a5aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b543b2-c139-42af-8131-a23d8e6ecf61",
            "compositeImage": {
                "id": "d1092599-d939-4766-9448-590a49d1756f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252ccb13-6e69-4dd2-98c4-ce7d3b03a5aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8efa74c-8af7-4890-bd24-d03f3effc365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252ccb13-6e69-4dd2-98c4-ce7d3b03a5aa",
                    "LayerId": "9fe423ed-4244-4e8c-b28f-212ee19cb698"
                }
            ]
        },
        {
            "id": "61149288-f14a-40e5-b251-8b8682ff9997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b543b2-c139-42af-8131-a23d8e6ecf61",
            "compositeImage": {
                "id": "47c5ff22-df4d-44fd-9c3a-71b2661afb29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61149288-f14a-40e5-b251-8b8682ff9997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f55a9d6b-b8f3-438c-805b-c3415a2f4bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61149288-f14a-40e5-b251-8b8682ff9997",
                    "LayerId": "9fe423ed-4244-4e8c-b28f-212ee19cb698"
                }
            ]
        },
        {
            "id": "27facfd6-4935-4567-862e-c7ba1913860d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b543b2-c139-42af-8131-a23d8e6ecf61",
            "compositeImage": {
                "id": "9cc78c7e-d59f-47e7-92e3-2c44b13a7e3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27facfd6-4935-4567-862e-c7ba1913860d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e82c0be-1828-48fa-aae2-3b2a3e53d59d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27facfd6-4935-4567-862e-c7ba1913860d",
                    "LayerId": "9fe423ed-4244-4e8c-b28f-212ee19cb698"
                }
            ]
        },
        {
            "id": "4cb0bb03-b883-4708-a2bb-c4ede28ea979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b543b2-c139-42af-8131-a23d8e6ecf61",
            "compositeImage": {
                "id": "cf9cb28c-d5f4-486a-807c-2933c0d1b0e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb0bb03-b883-4708-a2bb-c4ede28ea979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78cd30f-00c4-4997-ac12-7fceda1a3283",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb0bb03-b883-4708-a2bb-c4ede28ea979",
                    "LayerId": "9fe423ed-4244-4e8c-b28f-212ee19cb698"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9fe423ed-4244-4e8c-b28f-212ee19cb698",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21b543b2-c139-42af-8131-a23d8e6ecf61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 58
}