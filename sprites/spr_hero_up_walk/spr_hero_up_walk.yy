{
    "id": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_up_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 18,
    "bbox_right": 56,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f2c91d1-b9ef-4e7e-b844-62c98f68bad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "132c8eb0-301a-43f9-bf7a-c14113043d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2c91d1-b9ef-4e7e-b844-62c98f68bad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c449654-ee56-49cc-a0f3-812776e55719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2c91d1-b9ef-4e7e-b844-62c98f68bad3",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "a01da191-9b6a-40f5-bb27-e2d11284a7c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "e263f1a0-6ee9-4e92-abbf-a7f70f2da2c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01da191-9b6a-40f5-bb27-e2d11284a7c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7724daff-11db-4353-892f-54884932542c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01da191-9b6a-40f5-bb27-e2d11284a7c9",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "8662014a-195b-435a-ac38-2485fcebcf66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "9941435a-79ca-4629-b97d-5340ccb72aea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8662014a-195b-435a-ac38-2485fcebcf66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a204922a-6f10-466c-984b-259f9d0690b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8662014a-195b-435a-ac38-2485fcebcf66",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "8646957f-4690-4324-bf8f-50bb04419978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "7eaab641-53ab-47f7-b157-5b88e26d4373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8646957f-4690-4324-bf8f-50bb04419978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aef5d38-7246-40c4-abe9-0b172ff7299e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8646957f-4690-4324-bf8f-50bb04419978",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "de46fc06-ded7-4470-9f12-76189822d95e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "e4cfb2f5-639f-4d64-8000-e346f7b6ae59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de46fc06-ded7-4470-9f12-76189822d95e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b0d58d1-138a-45a9-830c-534b979c3094",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de46fc06-ded7-4470-9f12-76189822d95e",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "d81a63ef-a7d7-4fa2-8f41-7098032b48b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "66d8af1a-fdf3-45ad-9c14-64003e4cbf55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d81a63ef-a7d7-4fa2-8f41-7098032b48b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db6923f6-c85c-4c10-9378-e463b21cb0a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d81a63ef-a7d7-4fa2-8f41-7098032b48b6",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "ad2faa32-55f2-4781-a858-1b1befbe203b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "e61463e0-3d6a-41e6-8565-c01ee14fbd96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2faa32-55f2-4781-a858-1b1befbe203b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aeb967e-604b-4648-93e4-af3d673af7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2faa32-55f2-4781-a858-1b1befbe203b",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "aa0cb8bd-9196-4a92-a50b-dc7b6c814a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "37cbe823-fb04-4f76-a96b-e4ff114282c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0cb8bd-9196-4a92-a50b-dc7b6c814a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b338247-3ca0-4603-a657-f85ddae7c46f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0cb8bd-9196-4a92-a50b-dc7b6c814a23",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "c93d90ab-6808-4744-a857-0e9ebee7c018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "5e70616e-0c2f-4d99-a9bd-283f6d5738d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c93d90ab-6808-4744-a857-0e9ebee7c018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e13aba-2a1c-4368-bd98-cf8dd5bb223f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c93d90ab-6808-4744-a857-0e9ebee7c018",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "ab72dda2-bd29-42e0-802e-e48e34be1a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "2b867cd5-ca8c-46a3-a3af-1272706735e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab72dda2-bd29-42e0-802e-e48e34be1a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e326d276-9698-4491-96e0-f9bc2d930b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab72dda2-bd29-42e0-802e-e48e34be1a7c",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "c1bf162a-6428-47e5-bde9-aaeba851e97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "9d727338-f1f8-4976-88bf-9022be6aa17a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1bf162a-6428-47e5-bde9-aaeba851e97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6169865e-7836-4ae4-9365-f5e6c73b3d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1bf162a-6428-47e5-bde9-aaeba851e97d",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "2f24326e-1b53-4f4f-a03b-ed2b2430b801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "116d89ca-b9a2-4934-8bbb-e3f7e3516cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f24326e-1b53-4f4f-a03b-ed2b2430b801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb2498c-8db9-4494-8d5a-e2ebb7ff976e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f24326e-1b53-4f4f-a03b-ed2b2430b801",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "ffc6334a-66d8-4c0b-af43-055d999e315d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "98fc9a0d-a9b3-4146-9db3-a4acfd499372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc6334a-66d8-4c0b-af43-055d999e315d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "971822b0-45f3-4f4a-98c8-1305fe218375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc6334a-66d8-4c0b-af43-055d999e315d",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "65010e5f-e47e-409b-911a-eeae539d0962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "ff4c8276-e957-435f-a039-f286a00c2814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65010e5f-e47e-409b-911a-eeae539d0962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9673f93f-5905-42cc-be99-12137812aafe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65010e5f-e47e-409b-911a-eeae539d0962",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "146a5669-1a27-4418-8161-6ad73ffcad59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "f23b6ae5-276c-4342-87a6-23c966b85438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "146a5669-1a27-4418-8161-6ad73ffcad59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b3edfa5-71ad-4850-8361-345d9607d379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "146a5669-1a27-4418-8161-6ad73ffcad59",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "5256c38f-e5cb-40c2-ba70-942dcea79c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "017a176e-4ece-4a27-8753-2b7744c5516a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5256c38f-e5cb-40c2-ba70-942dcea79c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6715b5c2-076e-44c5-a6f7-6adadfdeca79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5256c38f-e5cb-40c2-ba70-942dcea79c7b",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "69a8b7a8-ccda-4ebe-bd66-96310acaeac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "9d65b74a-1d5e-4312-a09e-ca9f7e32f30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a8b7a8-ccda-4ebe-bd66-96310acaeac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0cfaee7-ec9f-408a-a927-c5a44d0599cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a8b7a8-ccda-4ebe-bd66-96310acaeac3",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "1add3d9f-85f6-4951-9764-29e3523c41d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "18476b83-1b5e-421e-84ea-c25e2a75c8fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1add3d9f-85f6-4951-9764-29e3523c41d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caef4885-6d61-405b-99d7-cee72b6532c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1add3d9f-85f6-4951-9764-29e3523c41d7",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "94f88205-ca2b-4094-8dd4-075d9987a8f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "bbc691c6-a6ef-4f45-b33c-6e440925daaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f88205-ca2b-4094-8dd4-075d9987a8f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d33044a5-671c-4956-8da7-5da559f5662d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f88205-ca2b-4094-8dd4-075d9987a8f9",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        },
        {
            "id": "75222527-f42a-4588-aade-b69234a22187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "compositeImage": {
                "id": "7490cb79-bd41-4844-b54c-84c7562dfc6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75222527-f42a-4588-aade-b69234a22187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d0c1e9-6162-407f-a463-31f48abf595d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75222527-f42a-4588-aade-b69234a22187",
                    "LayerId": "f264330f-251e-4d0a-a23f-cd4cb43c5e65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f264330f-251e-4d0a-a23f-cd4cb43c5e65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c41d0a67-56d3-4dde-9bbc-69adf44e9a4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}