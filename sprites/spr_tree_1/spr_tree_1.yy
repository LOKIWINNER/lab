{
    "id": "64755610-3590-475f-82fa-9163b3b90962",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 44,
    "bbox_right": 78,
    "bbox_top": 138,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0ee88f3-f7fd-44e5-aaca-41072f7cea2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64755610-3590-475f-82fa-9163b3b90962",
            "compositeImage": {
                "id": "ae9698ee-635e-492d-b17c-86dc102b787a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ee88f3-f7fd-44e5-aaca-41072f7cea2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357f34ca-ff55-4db7-99e7-c29cae659e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ee88f3-f7fd-44e5-aaca-41072f7cea2d",
                    "LayerId": "9c780268-bc93-419c-a0a3-526bedd62801"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "9c780268-bc93-419c-a0a3-526bedd62801",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64755610-3590-475f-82fa-9163b3b90962",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 61,
    "yorig": 146
}