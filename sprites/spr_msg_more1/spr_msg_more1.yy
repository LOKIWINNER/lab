{
    "id": "712cdcbc-1456-4c57-8775-7130c27fe996",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_more1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e4da8c4-989b-4cd5-aac4-d2fe13bf2b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "712cdcbc-1456-4c57-8775-7130c27fe996",
            "compositeImage": {
                "id": "ec3ef6d2-44ec-47a4-8c3d-17e2e1ea8c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4da8c4-989b-4cd5-aac4-d2fe13bf2b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa31b6d0-9f40-4bca-bfb3-87322c5bd55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4da8c4-989b-4cd5-aac4-d2fe13bf2b08",
                    "LayerId": "a6f122ab-d896-4206-82be-1b70fe8d04ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a6f122ab-d896-4206-82be-1b70fe8d04ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "712cdcbc-1456-4c57-8775-7130c27fe996",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}