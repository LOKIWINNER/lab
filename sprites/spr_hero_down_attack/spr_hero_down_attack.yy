{
    "id": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 9,
    "bbox_right": 54,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c2d499e-65d9-4fb8-9e85-b91fb581262d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "d6090abc-f911-4f4d-9a08-c345e56501f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c2d499e-65d9-4fb8-9e85-b91fb581262d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bf7546-aa26-40ad-bf24-60604c79e728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c2d499e-65d9-4fb8-9e85-b91fb581262d",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "ad6c35b2-024d-49a8-9dea-0beaf38d6c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "98f26459-7bf6-4f50-83c2-f4a4d9a001ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad6c35b2-024d-49a8-9dea-0beaf38d6c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f234ff-f724-4d6a-a9f2-697d2e7d79a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad6c35b2-024d-49a8-9dea-0beaf38d6c6e",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "52383885-7b7a-44ed-b2a5-abb63afd2383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "06ad7567-bdae-4619-b977-2e115c8d0770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52383885-7b7a-44ed-b2a5-abb63afd2383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8680a79b-3991-4a62-a02d-1ca534ee5b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52383885-7b7a-44ed-b2a5-abb63afd2383",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "0539b21b-1b86-4bfc-84b5-2465b55a2563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "e7d0f44d-ba57-48ec-bdac-1631919fe328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0539b21b-1b86-4bfc-84b5-2465b55a2563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9048966-4636-4f43-9030-cd3d8c0f443e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0539b21b-1b86-4bfc-84b5-2465b55a2563",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "54ca9f03-4d7a-49b7-a034-e8a1d4925991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "a696b1b3-6aad-4f17-9b3c-0f69812076f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ca9f03-4d7a-49b7-a034-e8a1d4925991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e6bf883-b9e8-419e-98d2-794db285a569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ca9f03-4d7a-49b7-a034-e8a1d4925991",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "aa5bfe42-f59c-4fac-bff3-1d01d5ca8b35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "d36cbd4d-ef57-438a-85e1-80ecebec383e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5bfe42-f59c-4fac-bff3-1d01d5ca8b35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b76111df-e9a8-475d-a555-f8492aad2c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5bfe42-f59c-4fac-bff3-1d01d5ca8b35",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "ce21e7f9-9782-4902-bda6-fbcfa1afd402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "2a3d8b18-158a-4f40-a2ed-1851dba50731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce21e7f9-9782-4902-bda6-fbcfa1afd402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66ae8f1-74b0-429b-9636-f4aa1dccc6fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce21e7f9-9782-4902-bda6-fbcfa1afd402",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "6960f907-fc9b-4a15-b88f-c83d9cf7b926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "3873862c-be10-4059-91bc-fd1809c0cbca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6960f907-fc9b-4a15-b88f-c83d9cf7b926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f520b22-f438-44bd-8037-473639007339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6960f907-fc9b-4a15-b88f-c83d9cf7b926",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "baac8fbe-d19d-4955-8056-81f536c1c8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "e1aee865-7801-45ae-8566-0cfd6f40cba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baac8fbe-d19d-4955-8056-81f536c1c8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63ca22a-49eb-4890-8322-e73b451c5872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baac8fbe-d19d-4955-8056-81f536c1c8a1",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        },
        {
            "id": "b793dc2e-d154-48de-8ef8-cfb7ac9d90d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "compositeImage": {
                "id": "bf1cfe89-841a-4772-97b7-79eb24832c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b793dc2e-d154-48de-8ef8-cfb7ac9d90d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c62ac2f2-b4c6-4d1b-bc26-c31103dd6eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b793dc2e-d154-48de-8ef8-cfb7ac9d90d2",
                    "LayerId": "aff7263c-8cf9-4572-8632-fa6e954b7351"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aff7263c-8cf9-4572-8632-fa6e954b7351",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3504456-98b6-4beb-b31f-6c7df9be5fb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}