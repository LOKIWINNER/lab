{
    "id": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_attack_mask_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da17a382-02b7-4b30-b843-0d7de56d1962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "4d104379-6821-47f3-a4bf-e85cd92ca0d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da17a382-02b7-4b30-b843-0d7de56d1962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a436c380-1213-49cf-8bcd-6002b8b4c4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da17a382-02b7-4b30-b843-0d7de56d1962",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "cdca5deb-0856-4dd1-ae0c-d0e1b8c9f345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "b51097bb-59c7-4086-8ec7-740a642ea362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdca5deb-0856-4dd1-ae0c-d0e1b8c9f345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0133be3d-86b0-4450-91bc-bf1726f287f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdca5deb-0856-4dd1-ae0c-d0e1b8c9f345",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "3ab84a4e-ae53-4201-88c9-f60ece5c6a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "0cf72f42-04d8-4c02-baa1-127276388ba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab84a4e-ae53-4201-88c9-f60ece5c6a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767f313a-6398-465d-800c-5e9cdf47aeeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab84a4e-ae53-4201-88c9-f60ece5c6a63",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "254b050a-347d-4ae3-87d5-7bd1153d92f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "cdfbdce4-0a4b-4bd7-afce-388b7bd9d949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "254b050a-347d-4ae3-87d5-7bd1153d92f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a8b96d-c4f1-4d40-9753-ee941e4ffcbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "254b050a-347d-4ae3-87d5-7bd1153d92f5",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "b3017d64-7517-43bf-8f21-9c43d1bb63e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "d5c4e942-349c-4623-9757-66b930b5978f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3017d64-7517-43bf-8f21-9c43d1bb63e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ddc26e5-ab02-42eb-875f-75e6f59a033f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3017d64-7517-43bf-8f21-9c43d1bb63e9",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "c8ac216c-2378-426f-9833-715fd37394ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "c63c7f6c-50c7-4ab4-9e68-13f0cdd81408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ac216c-2378-426f-9833-715fd37394ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d756e2ba-cc5d-4bba-86e1-8c651ca9b2fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ac216c-2378-426f-9833-715fd37394ae",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "ac9282de-9689-40a6-ba63-364ef5e94c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "7f24aee8-c389-49f7-83fc-7947ec39cb80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9282de-9689-40a6-ba63-364ef5e94c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba761e45-cc35-47bb-849c-20ef7e007d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9282de-9689-40a6-ba63-364ef5e94c82",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "4e8a0bc9-71d8-4c6f-aae9-c255b7a9944a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "4b6fae67-85d9-40d7-bb85-47e0148413a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e8a0bc9-71d8-4c6f-aae9-c255b7a9944a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3080134-c809-4cab-8a59-5e00ce6c0e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e8a0bc9-71d8-4c6f-aae9-c255b7a9944a",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "20aeefd2-b2b0-4358-beb4-f53d82675eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "1335b742-3d19-4c20-9277-49e0925247e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20aeefd2-b2b0-4358-beb4-f53d82675eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e44efe-7e78-4265-88bc-be79260ffee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20aeefd2-b2b0-4358-beb4-f53d82675eba",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        },
        {
            "id": "aab0e7bc-c6af-4f01-af0b-3b24ef10c246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "compositeImage": {
                "id": "0ab5263e-c0a6-4af9-a6e3-b674537190f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab0e7bc-c6af-4f01-af0b-3b24ef10c246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "303b2a79-17e0-4a27-8bee-96ea9504a4df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab0e7bc-c6af-4f01-af0b-3b24ef10c246",
                    "LayerId": "f2d980de-40b6-4157-a09e-8d92b3411908"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f2d980de-40b6-4157-a09e-8d92b3411908",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c25d655-8603-4d43-9b46-0bc400c8a4f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}