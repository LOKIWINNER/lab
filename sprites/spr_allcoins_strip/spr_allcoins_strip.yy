{
    "id": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_allcoins_strip",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45ad3cdd-e77f-4434-8444-cd5dcaefd845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "2b765b3e-7e84-40eb-ab8c-30917d52c531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ad3cdd-e77f-4434-8444-cd5dcaefd845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e85ce71-2e3a-4f55-b380-f73c56bae6c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ad3cdd-e77f-4434-8444-cd5dcaefd845",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "d0c95e9b-8164-47b7-b3fa-2d9e1c644e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "e34322e7-2b50-425d-9b91-5d9010f8e675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c95e9b-8164-47b7-b3fa-2d9e1c644e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace489c3-7f53-4fd3-bb9d-1c23c3f58d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c95e9b-8164-47b7-b3fa-2d9e1c644e39",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "70b4712e-6d14-4972-8535-d772f7872cfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "455954d4-9cae-42a5-b27d-a38a2ae5675d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b4712e-6d14-4972-8535-d772f7872cfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b6f0792-9999-438d-bae2-093345df99bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b4712e-6d14-4972-8535-d772f7872cfc",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "c5bd7991-0f0e-48eb-a108-5eb24a9eeed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "01cf28b4-be26-4c38-988c-f501719a6f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5bd7991-0f0e-48eb-a108-5eb24a9eeed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe7a23a-4484-4790-ba93-a1d379dc8361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5bd7991-0f0e-48eb-a108-5eb24a9eeed1",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "793e9ac8-f349-405e-8275-99764ec9fbd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "8390b544-a103-4b06-9bb1-5e7b13b4f8e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "793e9ac8-f349-405e-8275-99764ec9fbd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1e3bb39-2e46-4de8-8c23-e8a7fbd89959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "793e9ac8-f349-405e-8275-99764ec9fbd7",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "ef5cd2d2-56ea-4450-84b4-c4a589dc74a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "469c842f-00fc-4c9b-ae76-2440d4407683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef5cd2d2-56ea-4450-84b4-c4a589dc74a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5b60ff6-a1dc-4696-b319-6f3847af5c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef5cd2d2-56ea-4450-84b4-c4a589dc74a3",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "fd5336c1-0640-4892-bacf-15ce575518a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "c2a0d0bf-4ebe-42e8-ac24-a3dd9c19b29e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd5336c1-0640-4892-bacf-15ce575518a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60facb00-30b5-44fb-a897-1c589ba345d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd5336c1-0640-4892-bacf-15ce575518a3",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        },
        {
            "id": "fdf64b22-9e63-405b-adfd-a4d4341f54fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "compositeImage": {
                "id": "34daecb1-2e6e-4172-932d-c16601278404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf64b22-9e63-405b-adfd-a4d4341f54fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bec5ed0c-ec58-4c13-b19b-7b1fbd65d7c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf64b22-9e63-405b-adfd-a4d4341f54fd",
                    "LayerId": "b3423e58-e691-4036-8279-02aaed18c641"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b3423e58-e691-4036-8279-02aaed18c641",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28a9010f-3c65-4c9d-bcd4-2c33ed5ddf93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}