{
    "id": "6fadb697-b438-49f9-b419-e016ffdb3d99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box_z",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f733a2d6-cdd3-48e8-9fc6-98026827dc8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fadb697-b438-49f9-b419-e016ffdb3d99",
            "compositeImage": {
                "id": "da0423b2-6996-4702-b384-f168d67e7570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f733a2d6-cdd3-48e8-9fc6-98026827dc8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27b9ef04-d3e3-4486-84aa-07ffa470c440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f733a2d6-cdd3-48e8-9fc6-98026827dc8b",
                    "LayerId": "12b53112-308a-4a37-bec8-9ba10cbd55a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "12b53112-308a-4a37-bec8-9ba10cbd55a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fadb697-b438-49f9-b419-e016ffdb3d99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}