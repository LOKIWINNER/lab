{
    "id": "50c8ad3b-dfe6-4a32-a469-16b28fc32bc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "060d48c8-77bb-4678-9d36-b6ac6aa4a893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50c8ad3b-dfe6-4a32-a469-16b28fc32bc9",
            "compositeImage": {
                "id": "9e78d3bc-f16d-4c8f-983f-32e6c5d00a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "060d48c8-77bb-4678-9d36-b6ac6aa4a893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f4a076-6f64-4dba-94ad-6d360de85117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "060d48c8-77bb-4678-9d36-b6ac6aa4a893",
                    "LayerId": "499ce183-6355-482f-8006-f921555c53ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "499ce183-6355-482f-8006-f921555c53ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50c8ad3b-dfe6-4a32-a469-16b28fc32bc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}