{
    "id": "f672286f-418e-4508-8c8d-d92d4b5dbdba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 6,
    "bbox_right": 45,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c1ed1d1-e18e-4bff-87e3-df33883d4a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f672286f-418e-4508-8c8d-d92d4b5dbdba",
            "compositeImage": {
                "id": "d3fc2f6b-80b0-4180-953e-a0b586627516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c1ed1d1-e18e-4bff-87e3-df33883d4a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e409a0-91a3-4c6d-8622-201d3bbbf741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c1ed1d1-e18e-4bff-87e3-df33883d4a2d",
                    "LayerId": "93eaea56-0dd0-450a-9e08-2d326da625a0"
                }
            ]
        },
        {
            "id": "14e87fd6-6297-4bf4-bf30-c2b54a5ed675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f672286f-418e-4508-8c8d-d92d4b5dbdba",
            "compositeImage": {
                "id": "d3869a45-c221-4be8-810b-b3d07179f86c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14e87fd6-6297-4bf4-bf30-c2b54a5ed675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5687aa-b4a5-445f-bda6-656676d1adac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14e87fd6-6297-4bf4-bf30-c2b54a5ed675",
                    "LayerId": "93eaea56-0dd0-450a-9e08-2d326da625a0"
                }
            ]
        },
        {
            "id": "73cde7a6-c561-4d8b-a945-79627639f1d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f672286f-418e-4508-8c8d-d92d4b5dbdba",
            "compositeImage": {
                "id": "ff20a9fe-82a0-49cc-b1a7-6ea863eff047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cde7a6-c561-4d8b-a945-79627639f1d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5855e66f-08a7-4201-9e68-4035b3e985c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cde7a6-c561-4d8b-a945-79627639f1d4",
                    "LayerId": "93eaea56-0dd0-450a-9e08-2d326da625a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "93eaea56-0dd0-450a-9e08-2d326da625a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f672286f-418e-4508-8c8d-d92d4b5dbdba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}