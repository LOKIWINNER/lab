{
    "id": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "copper_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dffa2947-6bbd-486b-bc50-79a03e038e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "9abe431c-4185-4c1a-a636-45203a6e73dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dffa2947-6bbd-486b-bc50-79a03e038e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2335d22e-0fef-4187-a297-28a5d50b3616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dffa2947-6bbd-486b-bc50-79a03e038e09",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "1174ae2c-462a-421f-9e37-32bc60bbc01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "5d85ba87-4990-4978-8834-12e5a0b5de3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1174ae2c-462a-421f-9e37-32bc60bbc01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c84d708-e2cb-4163-91f8-824aae1f9946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1174ae2c-462a-421f-9e37-32bc60bbc01c",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "8eb34224-dc2b-4427-948f-4273de1b2308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "aabf2f9d-ca5c-4bc0-97f1-cb5f7c2d810f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb34224-dc2b-4427-948f-4273de1b2308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383b2105-2f9e-46b9-8853-5aa136f787db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb34224-dc2b-4427-948f-4273de1b2308",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "6182c9bf-8716-48b1-aa28-07e6bc259221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "47d1f909-d8f9-450b-85ed-ef1d433cfdf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6182c9bf-8716-48b1-aa28-07e6bc259221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca477fb8-1e46-4fa9-8b94-cb5469de9967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6182c9bf-8716-48b1-aa28-07e6bc259221",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "850685d3-7917-448e-b98f-755369d90f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "7f706fd1-b864-4322-a794-0edb8ccef255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850685d3-7917-448e-b98f-755369d90f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6adefffc-2315-4b31-bc80-eb0ffd54f4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850685d3-7917-448e-b98f-755369d90f12",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "7b7fc0ec-bfbd-4d6f-90dd-3a65a301fe35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "1d4f99bc-6eec-4be9-9d93-e25a88b9762a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7fc0ec-bfbd-4d6f-90dd-3a65a301fe35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ba53b44-0bb2-428a-8c9b-67021fbabd1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7fc0ec-bfbd-4d6f-90dd-3a65a301fe35",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "8e3f46a7-c6f3-47a2-a043-07d5cacedac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "5c69d3e8-d3f4-4c18-ba63-920381a6e64c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e3f46a7-c6f3-47a2-a043-07d5cacedac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c694b884-9fdd-4d45-bdc4-4fa5dd3c2125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e3f46a7-c6f3-47a2-a043-07d5cacedac7",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        },
        {
            "id": "048cc94f-33ad-4995-b69e-e54abe8a98cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "compositeImage": {
                "id": "c5a95ca1-f945-4e2e-9a06-2c47b6a9f2d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048cc94f-33ad-4995-b69e-e54abe8a98cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6c1454b-c45d-41d4-9c57-e4bcbc364d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048cc94f-33ad-4995-b69e-e54abe8a98cf",
                    "LayerId": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e5a57ab8-edc7-4d9f-bece-77bcca9ec168",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5744674-14b4-40ad-b00a-6b1e8e30b6aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 28
}