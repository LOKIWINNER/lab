{
    "id": "16874898-c7e5-4443-8077-39b812687d26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skeletonball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 8,
    "bbox_right": 63,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96420a10-76c5-4986-a639-5448380ae196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "60dcf287-e9c0-4cd7-bdf6-6ecd6abfa824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96420a10-76c5-4986-a639-5448380ae196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96c7ed9b-9588-4eda-8349-2c6a0305f79b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96420a10-76c5-4986-a639-5448380ae196",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "7d1465f7-acc3-4bfe-aafc-08a5d8ed741e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "ef075feb-1306-4484-aa96-03e71b2ab755",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d1465f7-acc3-4bfe-aafc-08a5d8ed741e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d01be7a0-d2ba-432e-8b5b-9881d3fe04de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d1465f7-acc3-4bfe-aafc-08a5d8ed741e",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "d6c96841-3b8b-4f86-bf6b-7573fe31be2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "3267be51-e5da-4941-a516-998e5240cd67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c96841-3b8b-4f86-bf6b-7573fe31be2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c7514e3-cc3f-4e64-aefb-156f46e6e663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c96841-3b8b-4f86-bf6b-7573fe31be2e",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "e835e967-d4e8-4440-8adc-6f8e768c8ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "64b96ca9-245a-4bcb-b5c4-fb152e0d60af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e835e967-d4e8-4440-8adc-6f8e768c8ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5579f4f-3d33-430d-be54-9c2d02b8a5e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e835e967-d4e8-4440-8adc-6f8e768c8ed2",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "31eccca3-f62c-48bc-b050-f04f1dd1b08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "d8ef9399-231c-4374-a23b-0769975d1794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31eccca3-f62c-48bc-b050-f04f1dd1b08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5d4bfc9-8662-4ea5-8d28-af0bd78fb277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31eccca3-f62c-48bc-b050-f04f1dd1b08e",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "87298b06-c151-4771-b027-66dca385de66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "a1275b98-61db-41df-8824-4f86c3f5d6a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87298b06-c151-4771-b027-66dca385de66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9985a56e-83a2-457c-90d1-e814e41fed2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87298b06-c151-4771-b027-66dca385de66",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "b01088bf-d402-4787-8283-f60dc8ee446a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "fce5824c-f806-43f5-b6c8-2b3d5e07b7f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01088bf-d402-4787-8283-f60dc8ee446a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b0cd60c-7bea-4cb7-bc2c-6c28c5dbbeaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01088bf-d402-4787-8283-f60dc8ee446a",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        },
        {
            "id": "fe4ab263-ed23-4357-9e06-b6aa9cb29113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "compositeImage": {
                "id": "fc29f2c2-cc38-494d-8529-7c46ea522d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4ab263-ed23-4357-9e06-b6aa9cb29113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa64cd0-6da9-4b01-b52f-8e9585fd8526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4ab263-ed23-4357-9e06-b6aa9cb29113",
                    "LayerId": "30a22d76-8510-45cf-9122-b6d13b3fa5f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "30a22d76-8510-45cf-9122-b6d13b3fa5f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16874898-c7e5-4443-8077-39b812687d26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}