{
    "id": "94704913-ae87-4969-a4d4-28be1ba68a14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_attack_three",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 9,
    "bbox_right": 53,
    "bbox_top": 37,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "413a0948-1bb3-4029-9d0a-76ac2fa13119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "77d05c77-e1ef-4c34-8c57-1ee79e3aaa25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413a0948-1bb3-4029-9d0a-76ac2fa13119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f73cefa-956c-4ef2-ae75-501c389126c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413a0948-1bb3-4029-9d0a-76ac2fa13119",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "775f27f5-67c5-40f9-8a4d-dfbe4a623272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "f0c8cc03-353b-48f2-838e-7e5f844a5fa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775f27f5-67c5-40f9-8a4d-dfbe4a623272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9165955c-e939-4861-bea0-9f93ff122bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775f27f5-67c5-40f9-8a4d-dfbe4a623272",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "51de3a65-e917-42e5-95d7-b9ea4220eb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "184574cb-d286-4a83-b858-bd60f5dce20b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51de3a65-e917-42e5-95d7-b9ea4220eb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1c0bd3-8cab-42e1-a521-3f460b4ebb08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51de3a65-e917-42e5-95d7-b9ea4220eb3f",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "ad86e643-f8fb-4f5e-b1fa-c57487de6a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "b3160b38-29c9-4d43-8169-541f674972ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad86e643-f8fb-4f5e-b1fa-c57487de6a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2758bf60-64d4-48ad-b260-a2252602edb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad86e643-f8fb-4f5e-b1fa-c57487de6a69",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "ae947609-a883-418d-a02a-384bb68c4fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "ede5d099-ba23-4cf6-939c-868aa24e489d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae947609-a883-418d-a02a-384bb68c4fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffff5b0f-1b99-4e3b-b86b-62ed67e9c6a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae947609-a883-418d-a02a-384bb68c4fef",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "61d64cf5-456d-4b66-836e-a12e0660dde6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "db9bc51e-11ca-412f-b47b-f31af99bb9c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d64cf5-456d-4b66-836e-a12e0660dde6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb4950d-71d1-4fef-be74-6d07c573cd34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d64cf5-456d-4b66-836e-a12e0660dde6",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "e32d9ee3-05db-46a1-8049-f7b78d00dcd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "27d4d4b9-ff90-4be3-ad06-27b77c91d22c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e32d9ee3-05db-46a1-8049-f7b78d00dcd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8161aa-79cb-40e4-a1e4-089687cb0938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e32d9ee3-05db-46a1-8049-f7b78d00dcd9",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "d592844d-a677-44b4-99d5-a513c980000d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "e8067e70-7898-4dfc-bf6d-2c450bbf6292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d592844d-a677-44b4-99d5-a513c980000d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ff1d98-fbfc-4dee-99dd-9c12d2a58c1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d592844d-a677-44b4-99d5-a513c980000d",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "427a09f5-8693-4503-a77d-52e227c7cbc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "080127d4-8f01-40ba-93e0-6ed78d6992fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427a09f5-8693-4503-a77d-52e227c7cbc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01757bc4-3665-41cb-a064-97ac38730e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427a09f5-8693-4503-a77d-52e227c7cbc4",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "00b8631e-4ec2-408b-a240-e2ea4afaeedb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "c428d236-6384-4610-b118-df34096d7eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b8631e-4ec2-408b-a240-e2ea4afaeedb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "025bbd72-c600-40cd-9c9a-4f77f0891e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b8631e-4ec2-408b-a240-e2ea4afaeedb",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        },
        {
            "id": "c4f5c994-f7a6-4fb0-a6a3-f713dedb5e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "compositeImage": {
                "id": "78d44e62-f422-486e-8806-4c02e116c0c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4f5c994-f7a6-4fb0-a6a3-f713dedb5e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "193aa456-76f6-4835-979e-4ead6003f1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4f5c994-f7a6-4fb0-a6a3-f713dedb5e9f",
                    "LayerId": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e5c1f98e-edc8-423b-8e10-693b24c2a5bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94704913-ae87-4969-a4d4-28be1ba68a14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 35
}