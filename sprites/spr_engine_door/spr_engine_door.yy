{
    "id": "d708c672-f164-479e-a513-52b1f901cee5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_engine_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "606690b8-6a98-4cfc-bf09-356a51ae49ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d708c672-f164-479e-a513-52b1f901cee5",
            "compositeImage": {
                "id": "46112550-06b2-4411-9171-077d95b67fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606690b8-6a98-4cfc-bf09-356a51ae49ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c046a37d-8152-4b8b-accf-1f940082d170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606690b8-6a98-4cfc-bf09-356a51ae49ea",
                    "LayerId": "071d9c0e-0419-413d-8483-ab9284d5cac3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "071d9c0e-0419-413d-8483-ab9284d5cac3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d708c672-f164-479e-a513-52b1f901cee5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}