{
    "id": "4a0ef248-dc17-4695-b020-2895ce5771a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_knight_down_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 1,
    "bbox_right": 78,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eda4e03b-8a0f-473c-ac8b-cf654d216cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "d1df8b43-0702-46ca-91f7-3df48394710e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda4e03b-8a0f-473c-ac8b-cf654d216cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e7b2ef-2b7c-49a4-9d66-d14a73b906df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda4e03b-8a0f-473c-ac8b-cf654d216cf4",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "59bd3f3b-d5d2-4cae-91fb-4091b93676ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "ed24ca1f-3921-4929-bb7c-a5b2d5ebb9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59bd3f3b-d5d2-4cae-91fb-4091b93676ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f6d7c5-7de9-4acb-b15d-e0177bd968ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59bd3f3b-d5d2-4cae-91fb-4091b93676ca",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "76d6ee22-4a36-4596-beb1-f49ad0814904",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "552244f8-9053-4807-a1cb-02015fb2f904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d6ee22-4a36-4596-beb1-f49ad0814904",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e533179-17ea-4106-a856-865efc07856c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d6ee22-4a36-4596-beb1-f49ad0814904",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "66382f23-8eab-4406-a0c3-c27a2f32cf4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "3053c5f9-ecbe-4bc0-87a9-bdde236f8db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66382f23-8eab-4406-a0c3-c27a2f32cf4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0535cb0b-71ca-4245-b5bd-5dfe94d12b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66382f23-8eab-4406-a0c3-c27a2f32cf4a",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "bdad1096-4d3b-40be-930f-d0446bf5435b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "14481018-c4a0-43c4-b648-3f7f6c075f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdad1096-4d3b-40be-930f-d0446bf5435b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeaabc95-ae63-465b-9246-7b93e3d5c9c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdad1096-4d3b-40be-930f-d0446bf5435b",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "9ed152ce-75ec-4c18-a5d4-af181df6ab9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "1d0dfcc7-db5c-486b-9fc0-111549047981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ed152ce-75ec-4c18-a5d4-af181df6ab9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad5c55c8-9dc1-436c-b0e9-1490af31d9e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ed152ce-75ec-4c18-a5d4-af181df6ab9d",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "de9fa33a-6f56-442d-8525-83831cd6daca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "bfa3c773-25f1-4212-810f-192d9034b28b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de9fa33a-6f56-442d-8525-83831cd6daca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71631cfb-37bb-4eb5-b112-5ee3b7d41f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de9fa33a-6f56-442d-8525-83831cd6daca",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        },
        {
            "id": "423f6458-819b-453c-9b9e-e413adc0948b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "compositeImage": {
                "id": "6b29eb27-96d7-46b8-b119-5fa0145bcd87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423f6458-819b-453c-9b9e-e413adc0948b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36a97a71-1f2d-46db-97ce-dec80ecb1053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423f6458-819b-453c-9b9e-e413adc0948b",
                    "LayerId": "cd552949-cae7-44bb-9c73-0dde6a013e59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "cd552949-cae7-44bb-9c73-0dde6a013e59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a0ef248-dc17-4695-b020-2895ce5771a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 107,
    "xorig": 56,
    "yorig": 67
}