{
    "id": "f175b7e5-f425-4655-a72a-9dddb144ec31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b690c8c9-996d-4506-84bd-7ac55ae31ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f175b7e5-f425-4655-a72a-9dddb144ec31",
            "compositeImage": {
                "id": "7a5dac4d-50b1-49e0-9109-f7f91372ed87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b690c8c9-996d-4506-84bd-7ac55ae31ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b851cf04-ea5b-413a-90bd-7b34ac6bea06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b690c8c9-996d-4506-84bd-7ac55ae31ae1",
                    "LayerId": "94ca2999-0622-4e17-82ed-8e1dad9ef89c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "94ca2999-0622-4e17-82ed-8e1dad9ef89c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f175b7e5-f425-4655-a72a-9dddb144ec31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 8
}