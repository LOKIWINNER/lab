{
    "id": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_shield_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 7,
    "bbox_right": 92,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1d89b9f-dc6c-4ef7-904d-bafa4029cdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "55d77867-c5ca-4afb-b4f5-a859bfc78951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d89b9f-dc6c-4ef7-904d-bafa4029cdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e8e248a-a663-45cd-9c22-78446e78fdf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d89b9f-dc6c-4ef7-904d-bafa4029cdb0",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "80de67b9-d0ab-4afe-bff0-665cfaf8df22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "a79aa321-0622-4048-9836-ae76e0afea65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80de67b9-d0ab-4afe-bff0-665cfaf8df22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133e7a16-823e-4d7d-bc60-5a61e15b64c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80de67b9-d0ab-4afe-bff0-665cfaf8df22",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "b43bef31-d9a8-46b2-845d-c7dc0416d726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "9b0b2823-da1b-4782-861c-2257df2ce96e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b43bef31-d9a8-46b2-845d-c7dc0416d726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b83b63-7755-4373-8ead-8aa37707ac5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b43bef31-d9a8-46b2-845d-c7dc0416d726",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "8d167d6a-f105-4d80-a25b-5e1f53191bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "5a0a8008-a956-4e56-9a68-d09c350f7539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d167d6a-f105-4d80-a25b-5e1f53191bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dbcadc2-aec6-4fb6-b861-75b838621062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d167d6a-f105-4d80-a25b-5e1f53191bb2",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "e6157021-81d7-40a8-bd09-a3ed07edf35c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "36fcb235-aeff-418b-926f-ed115ee3d4a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6157021-81d7-40a8-bd09-a3ed07edf35c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96a90749-06bc-4cca-818b-9e266717cac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6157021-81d7-40a8-bd09-a3ed07edf35c",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "7d4a8f7a-eca4-4e61-9707-959ee459046a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "1e108a89-846e-40bd-9480-db682c75233c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4a8f7a-eca4-4e61-9707-959ee459046a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59e0239-1d46-4c20-938e-1d1766289f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4a8f7a-eca4-4e61-9707-959ee459046a",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "f25fc5e0-aea7-44e1-a71c-ab60fe969503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "5a38f96b-b616-4c42-8036-1c88d41fc9f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f25fc5e0-aea7-44e1-a71c-ab60fe969503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ffa46f1-c300-4974-8573-36ba459865df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f25fc5e0-aea7-44e1-a71c-ab60fe969503",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        },
        {
            "id": "db54678a-c2a2-4b16-a138-4914778f2349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "compositeImage": {
                "id": "82d9fb9c-baac-4e29-a4cc-df7c9afd92a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db54678a-c2a2-4b16-a138-4914778f2349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b75ae44-fd17-4bbf-a88a-97c287a95d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db54678a-c2a2-4b16-a138-4914778f2349",
                    "LayerId": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 134,
    "layers": [
        {
            "id": "d4c3075d-44fd-4504-8da2-2eba5eaaedf6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a8dddd3-f007-4feb-ac19-d5bce3538f52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 61,
    "yorig": 74
}