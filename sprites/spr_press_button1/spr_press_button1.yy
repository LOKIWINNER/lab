{
    "id": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_press_button1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 144,
    "bbox_left": 28,
    "bbox_right": 114,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6221f124-2a38-4433-ac64-54d343f117da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "281ad0cd-e500-48d9-8217-fc87f3a8d48f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6221f124-2a38-4433-ac64-54d343f117da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "397f6f67-8db9-4719-846b-d24e7d356154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6221f124-2a38-4433-ac64-54d343f117da",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "0060a21a-3157-437e-921b-18459a4827f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "3c89d39f-5a32-4ec7-82d5-d6417b82c3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0060a21a-3157-437e-921b-18459a4827f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb4c5ce-b03e-4cac-a6ac-265729faf792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0060a21a-3157-437e-921b-18459a4827f9",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "13f3f8c3-dccd-4809-b63c-c66ecf8b8292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "9b39adff-ee96-46c9-b8f9-5fa641456b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13f3f8c3-dccd-4809-b63c-c66ecf8b8292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d01ac654-3651-47ca-8746-af77b55270fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13f3f8c3-dccd-4809-b63c-c66ecf8b8292",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "2d1359bb-9a65-4c3e-81d2-96aecd56f7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "31fa3970-4029-4e50-871d-b23639ddccce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1359bb-9a65-4c3e-81d2-96aecd56f7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158efb4d-4c3e-4d05-8dfd-a92a3ec96384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1359bb-9a65-4c3e-81d2-96aecd56f7d2",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "291a58d9-8a20-417e-be73-e8fa6d4b7a14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "39682e35-9820-4c20-acbc-0f903a22ce9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "291a58d9-8a20-417e-be73-e8fa6d4b7a14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1bfd5c0-74c7-4549-ac29-9b4397eca082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "291a58d9-8a20-417e-be73-e8fa6d4b7a14",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "895e7c76-8cfa-4a1f-b00b-56531e9ae619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "8256fb7b-f1c7-49fe-b0b8-2f14a690dc16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895e7c76-8cfa-4a1f-b00b-56531e9ae619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed59ddb-e14e-41de-9419-d14f289cf67e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895e7c76-8cfa-4a1f-b00b-56531e9ae619",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "428b73e0-26d3-48f8-b634-76b4a1179d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "70757173-ecc4-4ad5-9b9b-c9e930163053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428b73e0-26d3-48f8-b634-76b4a1179d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd71c8d-305b-4436-8d1f-849b1135c0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428b73e0-26d3-48f8-b634-76b4a1179d1f",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "7e9b073a-4e6b-47c4-b92c-af5679777afd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "c4443c7f-2169-4064-b9fb-367e232f9e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e9b073a-4e6b-47c4-b92c-af5679777afd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb1dc6d3-bba9-4bac-8858-19d25fc9e220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e9b073a-4e6b-47c4-b92c-af5679777afd",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        },
        {
            "id": "19e491c1-b1ce-4d32-b1fa-6f60bbb0bb74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "compositeImage": {
                "id": "3d33f054-994e-46d4-9ae2-194830f3aaa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19e491c1-b1ce-4d32-b1fa-6f60bbb0bb74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99290164-b579-4992-89e4-8e1c119a7704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19e491c1-b1ce-4d32-b1fa-6f60bbb0bb74",
                    "LayerId": "843de57d-6ad6-4dff-bdce-2f1573ef163a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "843de57d-6ad6-4dff-bdce-2f1573ef163a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa4b5c36-1ec4-435d-9640-cb0abb32bdae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 71,
    "yorig": 108
}