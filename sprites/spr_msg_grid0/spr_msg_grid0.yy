{
    "id": "25feb9ce-6f54-4851-b52e-0d4ec1f19635",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_msg_grid0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3619845-2148-4183-b3e7-339e045d39fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25feb9ce-6f54-4851-b52e-0d4ec1f19635",
            "compositeImage": {
                "id": "f9aaf708-667e-4336-982e-fe090a844abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3619845-2148-4183-b3e7-339e045d39fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d999014-2732-4977-9e60-587d0df31e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3619845-2148-4183-b3e7-339e045d39fa",
                    "LayerId": "cb02c456-428d-4083-b19d-1ec7397f844e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "cb02c456-428d-4083-b19d-1ec7397f844e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25feb9ce-6f54-4851-b52e-0d4ec1f19635",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}