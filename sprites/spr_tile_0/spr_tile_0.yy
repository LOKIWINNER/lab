{
    "id": "3b37e530-9759-424f-9984-da23be3f7dea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 959,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d009893-6c41-49da-b8fc-11fa171ec129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b37e530-9759-424f-9984-da23be3f7dea",
            "compositeImage": {
                "id": "fd572435-2519-4a8d-8dff-d8f804e79dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d009893-6c41-49da-b8fc-11fa171ec129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "accfaf58-2868-4532-8323-f1b026ed8de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d009893-6c41-49da-b8fc-11fa171ec129",
                    "LayerId": "9ccbfbfd-8b52-465c-aa86-e241fd8f9299"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 960,
    "layers": [
        {
            "id": "9ccbfbfd-8b52-465c-aa86-e241fd8f9299",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b37e530-9759-424f-9984-da23be3f7dea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 163,
    "yorig": 103
}