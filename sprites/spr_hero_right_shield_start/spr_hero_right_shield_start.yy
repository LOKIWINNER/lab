{
    "id": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_shield_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 22,
    "bbox_right": 49,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5f5c79d-5ffb-4f7a-9e08-d23610f53f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "d9d980c9-69a6-4453-a576-205f59d6cb1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f5c79d-5ffb-4f7a-9e08-d23610f53f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b77a97-f146-4918-a737-54a959cb4dec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f5c79d-5ffb-4f7a-9e08-d23610f53f2f",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "034fbe3b-2541-483e-b3a0-ba2ea9e2d4df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "2b626dce-6250-48a7-8dfa-b0a28fa6be8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "034fbe3b-2541-483e-b3a0-ba2ea9e2d4df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80d6defd-7d17-4210-a15f-8da09f2a7aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "034fbe3b-2541-483e-b3a0-ba2ea9e2d4df",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "61f778c5-dc90-482e-966a-547b9db894fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "9833514f-e0bb-4d37-a669-2a9577fce7fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f778c5-dc90-482e-966a-547b9db894fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08e9dc9-c299-489d-ac36-47917da26ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f778c5-dc90-482e-966a-547b9db894fd",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "362999a0-30a7-4b12-8c85-f56d5a76ef69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "4da17e9d-81e2-4274-8c75-a92a943f394c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362999a0-30a7-4b12-8c85-f56d5a76ef69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a351095-fabb-4ce8-8df1-670404690a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362999a0-30a7-4b12-8c85-f56d5a76ef69",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "ba6afadb-e148-4df1-b43a-466c4ac39681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "fa983bcb-5f7c-429b-aae7-a039a84a1fd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba6afadb-e148-4df1-b43a-466c4ac39681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8448c1-7eb7-48f1-8b62-d19185712d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba6afadb-e148-4df1-b43a-466c4ac39681",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "cc381997-c369-4c56-a991-ccbb33551c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "c816d622-1139-4a14-a15c-a2537f00faed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc381997-c369-4c56-a991-ccbb33551c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "439d988e-71ea-4dc6-b555-f4201605720d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc381997-c369-4c56-a991-ccbb33551c4e",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "78c261ce-c890-487b-b2ac-d68359486f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "744f6f08-d8da-4f1c-8628-f4de250c0148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78c261ce-c890-487b-b2ac-d68359486f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee668809-7ece-49ab-a910-d8fb1b43e5ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78c261ce-c890-487b-b2ac-d68359486f36",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        },
        {
            "id": "dc4c84a8-cb5a-4167-9a86-0a893a998a5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "compositeImage": {
                "id": "41086ec3-acae-41ed-8887-0a39db3f7e2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4c84a8-cb5a-4167-9a86-0a893a998a5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ef0cec-480d-40a0-8718-74b409b5186b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4c84a8-cb5a-4167-9a86-0a893a998a5e",
                    "LayerId": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6df77ed3-3cb9-4db3-8c2d-e0f1abc6e670",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5b6a14c-635f-450e-a759-48ea5e8deb65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 34,
    "yorig": 36
}