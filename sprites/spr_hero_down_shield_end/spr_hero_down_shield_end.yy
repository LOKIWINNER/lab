{
    "id": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_down_shield_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 7,
    "bbox_right": 92,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1330bb4-0482-4fe4-8d79-5a3382c0f260",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "fd3a3dc0-f0ac-45ce-88fc-6d5202de8e78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1330bb4-0482-4fe4-8d79-5a3382c0f260",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c644f4-8620-4b2d-848e-963163f0b77d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1330bb4-0482-4fe4-8d79-5a3382c0f260",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "6697b625-8747-4c61-a44e-1bc674ee2165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "ccad8472-8b6f-414b-9cc6-f5c4edbc1a33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6697b625-8747-4c61-a44e-1bc674ee2165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7edb177-d186-4554-90f5-bb9f76c694dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6697b625-8747-4c61-a44e-1bc674ee2165",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "9827ebc3-ff29-4e89-ad06-2520c966a4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "75ea9285-9f4e-4ae0-91de-a40ba9172049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9827ebc3-ff29-4e89-ad06-2520c966a4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01207631-f47a-46bd-9375-87406c9e37f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9827ebc3-ff29-4e89-ad06-2520c966a4ef",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "ca088349-c2dd-4cbf-9333-e2b80d6b4705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "bc63da8f-e291-48f5-a06f-897acead511b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca088349-c2dd-4cbf-9333-e2b80d6b4705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c153c79d-2dd2-49ed-9587-a940df1ede74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca088349-c2dd-4cbf-9333-e2b80d6b4705",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "4759247d-9964-4da5-ad51-431b601c0fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "39301a4e-f9a5-4664-bd25-591620abfd5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4759247d-9964-4da5-ad51-431b601c0fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ac4f124-f0f1-4a91-86ab-c92ad520e7a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4759247d-9964-4da5-ad51-431b601c0fd8",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "98c3c531-9ef8-49e7-a070-f1ae5054d4f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "c0ec78de-2d6a-4648-9289-3b40ca5e4cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c3c531-9ef8-49e7-a070-f1ae5054d4f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136bf76e-44c7-4180-b533-35a6ddc6665b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c3c531-9ef8-49e7-a070-f1ae5054d4f1",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "5f7277fd-9d3e-45e0-b1a4-a7381ba550c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "58a7086e-c22d-436a-b7ee-fd2e1280b50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7277fd-9d3e-45e0-b1a4-a7381ba550c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f376c6c-f329-4eee-a77b-6020259c4a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7277fd-9d3e-45e0-b1a4-a7381ba550c8",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        },
        {
            "id": "54e97b3b-6e81-4365-9e37-59212cc6cfec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "compositeImage": {
                "id": "58b1bf59-f9a1-4175-a45b-7f5107cca762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e97b3b-6e81-4365-9e37-59212cc6cfec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc4e3951-e584-4ee0-94de-d469583184ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e97b3b-6e81-4365-9e37-59212cc6cfec",
                    "LayerId": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 134,
    "layers": [
        {
            "id": "6a28ddf6-b5a2-4dd8-aa8d-57af376daac7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0e19561-715a-4ddf-b5e1-3e2d985f6cda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}