{
    "id": "9b314862-8710-4939-a708-0f4abd96da7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right_attack_three",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 40,
    "bbox_right": 63,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cedad81-8f15-4b19-8c7f-b21a32abb941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "513ad62a-567f-4c6a-b491-2a1754601699",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cedad81-8f15-4b19-8c7f-b21a32abb941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4d7ac4-d2c2-47f6-8cfc-79b5f45c5acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cedad81-8f15-4b19-8c7f-b21a32abb941",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "d1012118-2802-4f37-a390-fc4f58a20a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "c73ccb37-726d-4768-8bb6-bf561b8ccb5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1012118-2802-4f37-a390-fc4f58a20a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2fd96f8-c3d2-4797-bc88-d1204c467287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1012118-2802-4f37-a390-fc4f58a20a38",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "7c1f26bc-d6a0-45af-8638-9d60f783d084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "3b3edd02-5963-459c-89a4-2be3585ddb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c1f26bc-d6a0-45af-8638-9d60f783d084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9de6b165-8812-450a-bde4-218c57e16af9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c1f26bc-d6a0-45af-8638-9d60f783d084",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "cc189a8b-faa9-4552-ac1f-de0c22456250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "73771324-20ef-442b-a71c-4c871f0206ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc189a8b-faa9-4552-ac1f-de0c22456250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2dcd786-bb73-4722-b743-560bcbb68ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc189a8b-faa9-4552-ac1f-de0c22456250",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "13242a02-24a5-498d-a902-9c0ffcbe9c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "003fe0dc-fd6c-4af8-aa46-46c090a40d7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13242a02-24a5-498d-a902-9c0ffcbe9c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5157a68e-bb02-43f2-8264-24e9ae654762",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13242a02-24a5-498d-a902-9c0ffcbe9c9e",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "8da7953b-72aa-4aa1-8bd7-dc433cb3c4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "34c422e8-f4d4-43a9-94af-515729e54072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da7953b-72aa-4aa1-8bd7-dc433cb3c4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede35d42-b21b-4829-b715-5be09dfafdc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da7953b-72aa-4aa1-8bd7-dc433cb3c4ef",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "5652c182-4caf-43a8-ab2e-e8fdf9a5ce49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "14860016-7e68-4874-befd-11bf3bcf68af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5652c182-4caf-43a8-ab2e-e8fdf9a5ce49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "322cb9ba-9d41-490c-b97e-bc8b7a5a8bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5652c182-4caf-43a8-ab2e-e8fdf9a5ce49",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "4033cba8-344e-4253-bfaf-a6dbf3291a19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "5732570a-3e3f-495b-a0cc-08c13729cdef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4033cba8-344e-4253-bfaf-a6dbf3291a19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b276ed-71d8-4c32-9219-0caf4a1794a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4033cba8-344e-4253-bfaf-a6dbf3291a19",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "e603ad80-24d6-4f7c-a8ac-c91ed7739cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "7e24ed1c-a35c-4649-8cbe-5adbff0e7dfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e603ad80-24d6-4f7c-a8ac-c91ed7739cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b19f47dd-f131-4c8f-bbd8-6ca515479bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e603ad80-24d6-4f7c-a8ac-c91ed7739cc5",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "e675dd40-90d2-485e-a055-c001f6782e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "b80a5688-7e67-4328-8fd8-10550dea284d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e675dd40-90d2-485e-a055-c001f6782e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a82a9de0-8113-4a58-b876-fcea13cdbb19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e675dd40-90d2-485e-a055-c001f6782e72",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        },
        {
            "id": "fb235635-eb9a-48be-a7c5-19181079fecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "compositeImage": {
                "id": "b7344142-74c0-49da-93b3-fc9dda688343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb235635-eb9a-48be-a7c5-19181079fecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b2050f-35e0-40bc-b379-0474be5e766c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb235635-eb9a-48be-a7c5-19181079fecd",
                    "LayerId": "d73086ff-42e8-4fe5-ab92-ada310e4d342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d73086ff-42e8-4fe5-ab92-ada310e4d342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b314862-8710-4939-a708-0f4abd96da7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}