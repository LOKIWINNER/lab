{
    "id": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hero_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d21df334-3555-4daf-a08c-8c0f8de461e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "cf9aa6ff-d197-46d8-8e4f-3863ec614cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d21df334-3555-4daf-a08c-8c0f8de461e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "483fffc3-91d9-4140-89d4-15577c204439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d21df334-3555-4daf-a08c-8c0f8de461e7",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "4baeff74-3377-40df-9fa4-17db5fb799c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "877055df-574b-49f6-b716-2dd0cebf5d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4baeff74-3377-40df-9fa4-17db5fb799c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e304592-bc9d-4e3b-abdd-03367fefda19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4baeff74-3377-40df-9fa4-17db5fb799c8",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "1e1c12ca-e6d7-4838-91ea-dfc2c038f8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "b44f2472-4c90-4878-982b-01774b0343f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e1c12ca-e6d7-4838-91ea-dfc2c038f8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc74182d-521e-4581-8760-5331fb85e13f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e1c12ca-e6d7-4838-91ea-dfc2c038f8ca",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "f2ab43e0-1df4-4779-94d4-23ef3c5d2290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "13dfd396-993e-4268-8f20-dc31db53fe74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ab43e0-1df4-4779-94d4-23ef3c5d2290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef1d9ce6-5fca-4622-bdb9-12e331111fec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ab43e0-1df4-4779-94d4-23ef3c5d2290",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "26534f9b-ecae-43d3-ad53-53c25e8733f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "be5c45b5-e8a6-41ca-b373-bab4e67fe5fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26534f9b-ecae-43d3-ad53-53c25e8733f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb14abd-52f4-46fc-8b3b-739d66c926ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26534f9b-ecae-43d3-ad53-53c25e8733f1",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "8b5a91ba-939e-4934-89c7-05b859b85ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "4ef70f49-5c47-4279-bd84-a67124a29d00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b5a91ba-939e-4934-89c7-05b859b85ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49b129c5-6dd3-40bc-bcfa-d52f713a8289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b5a91ba-939e-4934-89c7-05b859b85ab3",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "bbf95efe-3762-4bad-82fb-8f031edc4893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "d0734eab-8daa-44de-aaa4-66bb9b5224e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbf95efe-3762-4bad-82fb-8f031edc4893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6771d15-ad25-4ac9-8267-506324142ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbf95efe-3762-4bad-82fb-8f031edc4893",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "3dbcec65-aadd-4b6a-9c85-b1ba60275680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "7d707ee0-a4ac-4593-8314-91c86bee6779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dbcec65-aadd-4b6a-9c85-b1ba60275680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13f6958b-65d9-42b6-b5f7-a4b56c7b1f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dbcec65-aadd-4b6a-9c85-b1ba60275680",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "f8dbe2bf-4205-4aaa-b42f-3505726150d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "c9c85488-916b-4a96-9164-36e86d813b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8dbe2bf-4205-4aaa-b42f-3505726150d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb07dcac-eec3-485a-ad50-0b5b55974992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8dbe2bf-4205-4aaa-b42f-3505726150d6",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "5dfd54eb-f270-4700-bdcf-db0f38ea498d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "146d579f-c2d7-4e40-9937-b5aa71a962b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dfd54eb-f270-4700-bdcf-db0f38ea498d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f330279-c32f-44b4-9dc1-dd59e5b0778a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dfd54eb-f270-4700-bdcf-db0f38ea498d",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "1967e9a7-c3a3-4d4c-8712-a7eb82b4ddd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "232608b7-32e5-49d1-82b7-b816e22cb0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1967e9a7-c3a3-4d4c-8712-a7eb82b4ddd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f63038-c759-4c70-8b1d-504cbfb7d3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1967e9a7-c3a3-4d4c-8712-a7eb82b4ddd5",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "986a35f0-af6b-4a47-8bcf-24daadb611e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "a643c430-f108-4c81-964d-d1ba6e3e3193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "986a35f0-af6b-4a47-8bcf-24daadb611e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5df9c0-48db-4ec4-b743-b407db30fe02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "986a35f0-af6b-4a47-8bcf-24daadb611e8",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "5d0deb3b-eb00-4c9e-b8a1-3469a00363bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "60d13985-2d60-4c93-adbe-bc2e1da35eec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0deb3b-eb00-4c9e-b8a1-3469a00363bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbfed04a-ddee-4a65-8d13-a25a73b4dcfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0deb3b-eb00-4c9e-b8a1-3469a00363bd",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "bd1890e7-230e-47ce-b7d1-235658d286a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "65cd685f-8275-4b1b-98ee-e5913c523c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1890e7-230e-47ce-b7d1-235658d286a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fe005d-dce3-45c3-b8b2-97db27168b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1890e7-230e-47ce-b7d1-235658d286a4",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "78c93ef6-ddc3-4dc9-a154-219ca25233e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "05dd3c1e-b9f0-447d-a1aa-60be0b7f2744",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78c93ef6-ddc3-4dc9-a154-219ca25233e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2276420-583a-4a95-979d-fe7de70ed051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78c93ef6-ddc3-4dc9-a154-219ca25233e6",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "9f74b00b-a71a-4df5-ba88-e18d284d22c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "367c5394-2f77-4a4a-b90c-f11f63b2c1b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f74b00b-a71a-4df5-ba88-e18d284d22c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586444a1-9699-4334-8f55-62939c5e1010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f74b00b-a71a-4df5-ba88-e18d284d22c1",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "df757cb9-5bc8-4f3e-a92a-1262dd913745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "a00e5e3f-68fa-4f7e-b56a-7f3038ee221c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df757cb9-5bc8-4f3e-a92a-1262dd913745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6efa934-9dc1-49ec-9aad-aa91225866c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df757cb9-5bc8-4f3e-a92a-1262dd913745",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "e36e925d-c17b-4882-830e-f24037670f70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "26dfdfe6-7d01-47fa-bfee-400643fc2248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36e925d-c17b-4882-830e-f24037670f70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f7b7601-0894-4009-9c5e-f8fa4a6f2d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36e925d-c17b-4882-830e-f24037670f70",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "2d1d5a53-580e-4072-b60e-f2997be8a622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "607457f4-0ec5-42d9-8d37-2baf88ff3193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1d5a53-580e-4072-b60e-f2997be8a622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "369bbaa3-0372-48c0-b710-881def571b30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1d5a53-580e-4072-b60e-f2997be8a622",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "909dd09c-e10f-4f1e-80e8-5f5c0a63b811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "def92495-917a-41b6-8eca-9f6206c89765",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909dd09c-e10f-4f1e-80e8-5f5c0a63b811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dde8bc7b-f37e-43cc-82e5-ef87798687e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909dd09c-e10f-4f1e-80e8-5f5c0a63b811",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "ea5d998b-5f94-46ed-99af-2f1870ed8eff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "8850b9b8-5a6f-423c-88e5-124194d595d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5d998b-5f94-46ed-99af-2f1870ed8eff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "724cfab6-73d9-4ef0-9fa1-19899836c22d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5d998b-5f94-46ed-99af-2f1870ed8eff",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "e8e582da-4d18-4896-875f-1601082178a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "6c0da4e5-93d2-4658-870a-a79c78b003ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e582da-4d18-4896-875f-1601082178a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c7f32fe-daca-4a6f-92db-777e39d83e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e582da-4d18-4896-875f-1601082178a7",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "3e2ebf3c-22fb-479e-81e6-c1e9570cfba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "b31af866-c218-418e-97d5-a8f633746495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2ebf3c-22fb-479e-81e6-c1e9570cfba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35126172-82f5-48b8-9558-cbe1b7a34219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2ebf3c-22fb-479e-81e6-c1e9570cfba3",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "b8887897-e65d-4ab6-a6ea-14f7d82e1510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "1acc48b1-5ac6-44d1-916e-e32c067b3a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8887897-e65d-4ab6-a6ea-14f7d82e1510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5fb76e0-bcbb-4242-8f6e-4a4786eef496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8887897-e65d-4ab6-a6ea-14f7d82e1510",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "63390777-c1e1-49e7-a384-c6f1ff3153b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "490dd4dc-ea49-4ff3-9785-8dcb7405b44d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63390777-c1e1-49e7-a384-c6f1ff3153b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ada4bc3f-e754-48a6-9646-f39c2347aad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63390777-c1e1-49e7-a384-c6f1ff3153b0",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "0a9beefe-29ed-458d-815b-cc04bf0138de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "bc0aebef-5091-4410-b24b-5b2af987721b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a9beefe-29ed-458d-815b-cc04bf0138de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bac76b3-0cc3-4429-a597-85b0a0d4286e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a9beefe-29ed-458d-815b-cc04bf0138de",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "f77aca08-91c9-4d64-a589-6166fe483cfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "1d411256-4c4f-47ee-b5ee-6fde15bb44ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77aca08-91c9-4d64-a589-6166fe483cfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ea7815-45dd-45ae-b0b9-3068e7d7ae5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77aca08-91c9-4d64-a589-6166fe483cfb",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "06a82275-b30c-4d24-b154-baecfda1fd2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "bb23a3dc-e09f-4f59-865e-bd56ac73d6d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06a82275-b30c-4d24-b154-baecfda1fd2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c9433b-2695-4b9a-a51f-0280ac2b3635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06a82275-b30c-4d24-b154-baecfda1fd2a",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "cb6a2fa2-6d39-49cc-a17e-24c50e7afe35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "942f8c39-537b-4a2b-93b5-8534c7520e9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6a2fa2-6d39-49cc-a17e-24c50e7afe35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3657ebe4-97c6-448d-bc1f-e99507df635d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6a2fa2-6d39-49cc-a17e-24c50e7afe35",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "ff8236a4-92ab-4bea-9e2a-20b11950b9db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "8fd3b3f5-6e46-4592-8b91-3b7812962abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff8236a4-92ab-4bea-9e2a-20b11950b9db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8c3474b-c980-425a-b507-068d2975277b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff8236a4-92ab-4bea-9e2a-20b11950b9db",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "2640a34a-e8d3-4648-9ffb-ea670ff377f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "1a625c7c-434f-4110-b56b-9b1943db3cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2640a34a-e8d3-4648-9ffb-ea670ff377f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "735a3c78-74a2-4600-9123-42d8f72c79ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2640a34a-e8d3-4648-9ffb-ea670ff377f2",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "da0f27a1-54c0-4ee2-80ac-81ed50e512f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "4efb0339-7a2a-4e46-8a94-37db650c83a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da0f27a1-54c0-4ee2-80ac-81ed50e512f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4e5b13-ee72-47ce-b26c-86cc25912fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da0f27a1-54c0-4ee2-80ac-81ed50e512f3",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "5af4259f-1170-4e7b-8795-55fc3f225e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "5ac1bc7f-282d-4c7c-8ace-8d7eaacbc78c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af4259f-1170-4e7b-8795-55fc3f225e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db7e34cb-ef0b-494d-abdf-f034bec9656c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af4259f-1170-4e7b-8795-55fc3f225e6d",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "a8ecb13f-6f6e-4f9c-ac6a-006e1b03470a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "6cfde30c-e0c8-4809-bcb9-b543b50e6762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8ecb13f-6f6e-4f9c-ac6a-006e1b03470a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd4aeaa-1706-4a3f-9a83-4d443691c377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8ecb13f-6f6e-4f9c-ac6a-006e1b03470a",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "17534f95-ef9d-4dc6-995a-a740ef9d91a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "d6ceb940-f5f4-4485-8e04-cc0971df7911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17534f95-ef9d-4dc6-995a-a740ef9d91a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b8208f0-d0f7-469b-85aa-82c0853a6323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17534f95-ef9d-4dc6-995a-a740ef9d91a2",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "19f9ea13-1c0a-42bc-823c-a9a89546cb84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fef35089-bbc6-46b1-bbe5-b1c0959b4e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f9ea13-1c0a-42bc-823c-a9a89546cb84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "432708dd-b023-48b0-9d3b-6683d3c3891e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f9ea13-1c0a-42bc-823c-a9a89546cb84",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "a4ea29d5-a7ae-4147-9971-b0ea17c7e3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "b59e0bd7-0b9d-48f1-a66e-93b832a2e431",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4ea29d5-a7ae-4147-9971-b0ea17c7e3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d58a179-e75e-49fb-bac4-9172fd88393b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4ea29d5-a7ae-4147-9971-b0ea17c7e3e5",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "0e05ec55-27ed-45af-92e0-eee32c4144de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fd79cdd2-5e43-4c49-bb14-07dafd5102b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e05ec55-27ed-45af-92e0-eee32c4144de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d008be-c3e3-4204-bc28-840a88adaaba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e05ec55-27ed-45af-92e0-eee32c4144de",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "a86b3787-3098-4d99-ae04-f0f16d64853c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "4143aae8-8aed-4575-b7c9-bcb35b52cafc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a86b3787-3098-4d99-ae04-f0f16d64853c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b4202d-7983-45a9-a7ff-ae2ee4be9df2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a86b3787-3098-4d99-ae04-f0f16d64853c",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "26970196-6f13-4191-9897-81e3dc4b17a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "01e615f2-cad5-4de6-bb52-4ab802500cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26970196-6f13-4191-9897-81e3dc4b17a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00fe6034-51e4-4ba4-b4c0-6135b2c6b841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26970196-6f13-4191-9897-81e3dc4b17a6",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "46d67fad-028b-4a14-9419-d9af48524028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "96dcbd8b-5732-4933-8811-98fe0fb4c5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d67fad-028b-4a14-9419-d9af48524028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e762cb12-5f7c-4ce2-8063-61f9bda39055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d67fad-028b-4a14-9419-d9af48524028",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "9ac1d6fa-c1cc-4b57-b992-6e61ac372622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "e48291e1-8660-4694-a395-05400fd3c9e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac1d6fa-c1cc-4b57-b992-6e61ac372622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b862bf-f8c1-4adf-8bf1-8f4aad31cdd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac1d6fa-c1cc-4b57-b992-6e61ac372622",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "e55b3108-d392-431e-ba1a-bb683020304b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "3f10bd18-64bc-437c-b5fa-54951f5eeb3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55b3108-d392-431e-ba1a-bb683020304b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7999f23-6b1e-4504-b8b9-a44478ca99c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55b3108-d392-431e-ba1a-bb683020304b",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "b79b15e9-8e8c-4663-bc52-34316a7dce5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "b7862e29-01b3-4959-b35d-94430345ac4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b79b15e9-8e8c-4663-bc52-34316a7dce5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53af8cd5-de50-4cc1-bc53-9cbe721e918c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b79b15e9-8e8c-4663-bc52-34316a7dce5b",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "10797454-2527-4acb-81b6-29258b74bd25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "19dd7ffa-645f-47ce-866f-15b39f4711c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10797454-2527-4acb-81b6-29258b74bd25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7dd94a3-1a33-4537-92db-015e3345c936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10797454-2527-4acb-81b6-29258b74bd25",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "3e820825-4f8e-4095-ab48-4a273fce50a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "3eae058d-9dc9-43c3-93c1-3cca5096c0ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e820825-4f8e-4095-ab48-4a273fce50a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eee923f-8a7c-46d7-b49d-05640fd3f080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e820825-4f8e-4095-ab48-4a273fce50a7",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "afb4f576-bd05-4a2c-aac6-f40f3b5e915f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "53fe0b9b-befe-4257-a312-3bfe0c2adf57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afb4f576-bd05-4a2c-aac6-f40f3b5e915f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2720e676-8c8e-4cb8-98c7-34d6c8a2849f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afb4f576-bd05-4a2c-aac6-f40f3b5e915f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "61d9261a-39a4-43f8-86e7-4abeb0d066f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "24b41b8b-09d1-460d-9eca-4ca5b988aec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d9261a-39a4-43f8-86e7-4abeb0d066f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cdda500-5d58-49fb-a1da-71d2c958c9d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d9261a-39a4-43f8-86e7-4abeb0d066f8",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "cba1c010-3a15-4def-809c-6f1f6fece5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "72359f5a-e854-401a-b137-45f061b1ccf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cba1c010-3a15-4def-809c-6f1f6fece5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4e8a0c1-e7a3-4e4d-889a-6eb7a263387b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cba1c010-3a15-4def-809c-6f1f6fece5ed",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "318e0bd6-f798-4050-9e1d-be2ebe55b700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "db719cc7-d655-4f18-b137-e6d1915712dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "318e0bd6-f798-4050-9e1d-be2ebe55b700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d6a600-5582-44d2-a600-0335ba5924b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "318e0bd6-f798-4050-9e1d-be2ebe55b700",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "458346a3-64b0-41cc-a362-04da93086f89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "73a23522-2c1e-4ce4-9ff9-c4be6f262d5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "458346a3-64b0-41cc-a362-04da93086f89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863e843c-e93b-43e1-b307-c0cc3f3d91aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "458346a3-64b0-41cc-a362-04da93086f89",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "880a0e19-867e-4abb-a115-95d85bb0df03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "01c9d6d0-b1db-4336-8c97-58de3d9dd570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880a0e19-867e-4abb-a115-95d85bb0df03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83a37f8-093e-4f8b-b240-9d62e37b4526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880a0e19-867e-4abb-a115-95d85bb0df03",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "7b3d91ed-e904-45b3-8d10-65f92df68bdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fd64d86c-a3b1-4f73-b7e9-ea9dd25f6074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3d91ed-e904-45b3-8d10-65f92df68bdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa0b05f-ba0e-4be5-b7f4-364050e0cb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3d91ed-e904-45b3-8d10-65f92df68bdf",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "45b5f5ca-1482-47ac-b9e6-010fc7453841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "c7a66c7f-6add-4839-a5e2-94bf68cfdcfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b5f5ca-1482-47ac-b9e6-010fc7453841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca4fbc4f-464b-4559-b7ae-daf7653c7e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b5f5ca-1482-47ac-b9e6-010fc7453841",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "def61c06-bae3-4f7c-9e5c-4ba45c3cbc81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "8a29baca-adbd-43dc-b405-89290b6990ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def61c06-bae3-4f7c-9e5c-4ba45c3cbc81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0f7032-6692-4a31-9d57-828e6fd72e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def61c06-bae3-4f7c-9e5c-4ba45c3cbc81",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "53460354-17ae-457b-891d-7f0176e30016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "e15b14d3-e74a-40bf-938b-016acb450c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53460354-17ae-457b-891d-7f0176e30016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96269211-e862-424f-9aa4-b3dd78700d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53460354-17ae-457b-891d-7f0176e30016",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "520b6a06-e0bb-4241-87d3-db09cf9cd7bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "33c6af8e-9e20-4632-9895-9490398f180c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520b6a06-e0bb-4241-87d3-db09cf9cd7bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05364237-a27b-4735-97a2-1b9690a87cf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520b6a06-e0bb-4241-87d3-db09cf9cd7bc",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "7facbdc8-8760-41eb-b522-db2bb5c78bac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "46256c5d-257b-46fc-aaef-b86348be9627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7facbdc8-8760-41eb-b522-db2bb5c78bac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aea97953-e1fe-448d-abb9-be361b6c90dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7facbdc8-8760-41eb-b522-db2bb5c78bac",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "5aefea5d-3109-4904-9207-23d3cc06023f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "c575bc0f-8251-4d47-a785-2d81078697ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aefea5d-3109-4904-9207-23d3cc06023f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ed0d46-e5a5-4b2f-a35e-7f15e78e73f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aefea5d-3109-4904-9207-23d3cc06023f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "8f56fe60-338c-4105-be4b-d4eb16ecc54f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "2efa50d1-5a66-4887-9655-708e86ea895f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f56fe60-338c-4105-be4b-d4eb16ecc54f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a184996d-c7e4-4ef1-a6f0-c2712459d4fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f56fe60-338c-4105-be4b-d4eb16ecc54f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "c3cc3178-9ba3-4aa1-9aa2-d2f771992e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "db97982e-1dfa-42d7-8028-3947d35f6817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3cc3178-9ba3-4aa1-9aa2-d2f771992e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0554f3-2749-43a4-b712-2b0fea6f8951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3cc3178-9ba3-4aa1-9aa2-d2f771992e9f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "a56f7849-b854-4e53-9ef1-eafe793e9547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "0973ff12-9c61-43e2-8e63-4207900f0837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a56f7849-b854-4e53-9ef1-eafe793e9547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87e345d5-be45-45d5-bf3d-fd943d23175a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a56f7849-b854-4e53-9ef1-eafe793e9547",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "bf01644b-7dd0-47b3-aabe-99e668fb6f97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "2143aff0-edcf-4f01-b56b-6e4a318efbce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf01644b-7dd0-47b3-aabe-99e668fb6f97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13281ec7-c6a6-465d-96cf-8b683a6b62b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf01644b-7dd0-47b3-aabe-99e668fb6f97",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "4903e9af-07b0-48ec-93de-2b0940ec2bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "e4e14599-8c55-4b0e-b1d8-f046bd55ec40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4903e9af-07b0-48ec-93de-2b0940ec2bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0432dc8-29fa-4197-9c09-3a18658d0d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4903e9af-07b0-48ec-93de-2b0940ec2bb1",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "6612c7ee-fed4-4ae4-bd7d-759a48c16341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "4e1faf41-d877-4a31-bcbc-a5c90cfbb64f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6612c7ee-fed4-4ae4-bd7d-759a48c16341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64fb710-7eb0-451f-9aa7-a3b3e7f05ef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6612c7ee-fed4-4ae4-bd7d-759a48c16341",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "db8ba7e4-035c-48b9-84b6-ea1d656618f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "b56d9795-27bd-40d2-8d69-ed752f9b77c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8ba7e4-035c-48b9-84b6-ea1d656618f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0492fc3e-267a-4831-8e74-4f532d11ba49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8ba7e4-035c-48b9-84b6-ea1d656618f1",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "ac038e2c-0b14-4120-ac07-911496533bec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "22af7d83-6345-4f93-a662-607b4623c238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac038e2c-0b14-4120-ac07-911496533bec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e8132a-9d15-4d3f-b836-db7f7274432c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac038e2c-0b14-4120-ac07-911496533bec",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "8a2ae7a2-b945-49ce-b0be-f626f55af975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "406a3e6f-dc64-4790-a4f2-61db85ab92a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a2ae7a2-b945-49ce-b0be-f626f55af975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a482b9b4-63a4-47ef-a703-34bad9797a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a2ae7a2-b945-49ce-b0be-f626f55af975",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "4866013a-722c-45ed-a6d6-d0962764eded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "e07f6ccb-212e-48e5-a37f-4f8598fa0b15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4866013a-722c-45ed-a6d6-d0962764eded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e08dfe-6fbd-4a33-80d6-1faffe2f3366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4866013a-722c-45ed-a6d6-d0962764eded",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "c65cc5e1-1c47-48d4-b4e3-19b175d14eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "84e74b68-3f06-44ad-8f6f-efb9d418369c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c65cc5e1-1c47-48d4-b4e3-19b175d14eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0cc5112-d9db-4c41-836b-61a32c9a375f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c65cc5e1-1c47-48d4-b4e3-19b175d14eaf",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "7a314b28-b682-464e-b995-e48b8f99a2d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "cd4f8205-5249-46c0-805d-bf386c622196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a314b28-b682-464e-b995-e48b8f99a2d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22e96752-03dd-4b1c-98e5-a0bb804604e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a314b28-b682-464e-b995-e48b8f99a2d5",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "35bbac4b-74b3-469e-ad48-0e542fec2c91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "7c45317c-0d09-471a-be65-2f15a32bc6b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35bbac4b-74b3-469e-ad48-0e542fec2c91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7875dfe-1f8b-4dae-b6cf-c6e70730dff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35bbac4b-74b3-469e-ad48-0e542fec2c91",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "f631fda2-b2bd-4908-b118-3a2a1f1fee9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "249b5756-60a0-40ee-8a39-34c780037ed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f631fda2-b2bd-4908-b118-3a2a1f1fee9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b796f1-d0a1-4c13-92ec-9110cf7158b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f631fda2-b2bd-4908-b118-3a2a1f1fee9f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "0ffdf261-3575-44b4-9c57-8f8ab1930b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "17ab0e34-022f-4cea-8893-e85371536083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ffdf261-3575-44b4-9c57-8f8ab1930b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e67f9406-b7d1-4097-9ddf-540c4573e7ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ffdf261-3575-44b4-9c57-8f8ab1930b1f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "660e5ed2-4b2a-4529-82d1-840e529fa551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "a186adf6-39e2-4297-87c6-1a6ec11529de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660e5ed2-4b2a-4529-82d1-840e529fa551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95735775-c9c4-406e-bddf-3f09eba5d748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660e5ed2-4b2a-4529-82d1-840e529fa551",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "289e673e-bda9-4515-81a2-e0ddca09c26d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "7771d5da-a173-4fad-8b65-8470b6b7f355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "289e673e-bda9-4515-81a2-e0ddca09c26d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22098d05-a359-4a70-82b3-2c95845638a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "289e673e-bda9-4515-81a2-e0ddca09c26d",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "b727ef40-1c10-4dda-b16f-e304092cc170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "6e895d51-5056-4fc2-adf4-75f22c367939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b727ef40-1c10-4dda-b16f-e304092cc170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef05a39b-c289-428c-b986-3d801e1e8f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b727ef40-1c10-4dda-b16f-e304092cc170",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "81458250-ed72-4f13-aa9b-60ee487fcc9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "12c21e53-cf49-4d84-ab9e-e1b86c53aed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81458250-ed72-4f13-aa9b-60ee487fcc9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6542156-f213-4364-a004-15890b0df4fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81458250-ed72-4f13-aa9b-60ee487fcc9a",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "5838087a-cf94-4a18-ba2b-1e6985c5745f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "0c66e4c2-a091-44e5-b683-2801ccf123b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5838087a-cf94-4a18-ba2b-1e6985c5745f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63d54d05-183d-44bb-a89f-3d7437c75f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5838087a-cf94-4a18-ba2b-1e6985c5745f",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "435973bb-5ec4-490d-9da9-3a22f3b286e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "1f80fe36-c37c-4535-b5b1-ce4473b7488c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435973bb-5ec4-490d-9da9-3a22f3b286e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd785c4c-a410-4f70-8a54-ceb8d2f7226a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435973bb-5ec4-490d-9da9-3a22f3b286e0",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "c9d10c20-b0d5-4973-9555-f3b9c49d1987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fa3af7d3-bc53-48b3-8dfc-c6684e6656c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d10c20-b0d5-4973-9555-f3b9c49d1987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4820ed3c-1e66-42c8-986d-77120558c7e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d10c20-b0d5-4973-9555-f3b9c49d1987",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "3162ef2c-34f5-4419-a468-199b2936c6bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "a86d7da3-b9f9-4abf-a206-beda8256cfee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3162ef2c-34f5-4419-a468-199b2936c6bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89794f1-7072-4892-91cc-8ba918e0581d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3162ef2c-34f5-4419-a468-199b2936c6bf",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "28870ceb-06ba-4886-8440-66ab69fad1ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "9db472c6-c994-40b9-93d9-dec9153bc5c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28870ceb-06ba-4886-8440-66ab69fad1ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c05f2a10-2f06-44b7-8101-4b8929529079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28870ceb-06ba-4886-8440-66ab69fad1ad",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "3120677a-7fa0-4325-9bd7-ad460a4522fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "330e451c-4b56-43bf-8b0c-18e707b4b0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3120677a-7fa0-4325-9bd7-ad460a4522fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2edb5668-4bc4-4360-b7e0-2734c1583c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3120677a-7fa0-4325-9bd7-ad460a4522fd",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "b15be18f-deaa-484f-b82b-ffd598654ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "133106a0-8b78-45df-8afd-9856813486d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b15be18f-deaa-484f-b82b-ffd598654ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a9d1b5-d4fd-4821-bc42-bf486be9ba1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15be18f-deaa-484f-b82b-ffd598654ef3",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "01c66fba-be50-4171-bda7-9435b9952080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "6de76f2f-59bd-470a-b124-05d923a2e641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c66fba-be50-4171-bda7-9435b9952080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16fe0c1d-e2ee-441e-815c-3792b3a879d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c66fba-be50-4171-bda7-9435b9952080",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "0f2bf726-c527-438f-a54e-1b46e93a0342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "bf82e4d0-bba8-4e0b-abb2-fd1b65a81f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f2bf726-c527-438f-a54e-1b46e93a0342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f7a9ca8-6744-433a-8641-74de171cb4db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f2bf726-c527-438f-a54e-1b46e93a0342",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "c1ef7fa5-74f9-418b-a0c8-f6c066366d4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "bd5518d2-519c-4e3c-a7d5-8737ba010b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1ef7fa5-74f9-418b-a0c8-f6c066366d4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f25a77-8a62-4e6e-9e0d-13eebc476766",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1ef7fa5-74f9-418b-a0c8-f6c066366d4e",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "7cd82f30-5d08-401f-83b4-da17f9bf6cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "d1688568-3d24-42a6-b074-1216d265cb0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd82f30-5d08-401f-83b4-da17f9bf6cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4354209-e774-4fd7-bd6f-9fe300d8cf50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd82f30-5d08-401f-83b4-da17f9bf6cf0",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "8ba92e53-e0ea-4831-9693-89e73742b2bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "3a455d75-e09f-4e20-a2ff-82f82f29b0b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba92e53-e0ea-4831-9693-89e73742b2bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72804daf-9aa9-4317-944e-7d625ce98c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba92e53-e0ea-4831-9693-89e73742b2bc",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "b92c44e2-6f8e-4f2e-b8e7-3911b8e3540a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fa8cfe06-6ec5-4847-a810-dfd2f7308279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92c44e2-6f8e-4f2e-b8e7-3911b8e3540a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c9cf88-c812-4ede-ba8d-8d4605f20a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92c44e2-6f8e-4f2e-b8e7-3911b8e3540a",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "7b34d51d-96ed-4cc3-b3f1-e0b8e0349037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "baef91be-6fa4-427b-83a4-3971f68142ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b34d51d-96ed-4cc3-b3f1-e0b8e0349037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd2f867c-8062-4747-9945-ff32b5e1b1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b34d51d-96ed-4cc3-b3f1-e0b8e0349037",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "1e4a252e-8ac8-4e56-96bf-36aa464b4d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "794a1bc6-7415-4870-a8da-40458530a3cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e4a252e-8ac8-4e56-96bf-36aa464b4d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9538e63e-550d-4a5d-94ec-0bfc8365810c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4a252e-8ac8-4e56-96bf-36aa464b4d0d",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "d9f93038-4696-4af2-9649-0f603ac37076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "37e88c08-e9e9-49af-bf7f-30a0cac30e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f93038-4696-4af2-9649-0f603ac37076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2be3cfc8-0171-4092-8a1e-05d418b2fed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f93038-4696-4af2-9649-0f603ac37076",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "5b51ac15-3d39-4723-98f3-24991d156b29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "2c6257b6-aad5-4f5c-9bac-319010d63fae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b51ac15-3d39-4723-98f3-24991d156b29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f330fc-dddc-4d78-8ad3-148ba96fc93d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b51ac15-3d39-4723-98f3-24991d156b29",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "613bb0a5-73b1-4fe9-8651-987747299f5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fe577fdd-54d9-4ab1-843e-a35b5037bf41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "613bb0a5-73b1-4fe9-8651-987747299f5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e14ecbc2-fc0c-4bb2-b916-7efb609237d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "613bb0a5-73b1-4fe9-8651-987747299f5c",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "1fe35a94-517c-4bcc-8015-f0f0270dde4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "7ed1983d-e407-453c-8d81-d26dd5b4539a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe35a94-517c-4bcc-8015-f0f0270dde4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "260e2b5e-58a4-47d8-b843-97d0d49b05ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe35a94-517c-4bcc-8015-f0f0270dde4e",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "9a35dedb-59d8-456e-9f67-02c3c4b9e2b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "e5fd2366-590c-46ca-808e-412aed8ab91c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a35dedb-59d8-456e-9f67-02c3c4b9e2b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a39b77b7-1960-4eea-ad2c-24cc5ccc2297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a35dedb-59d8-456e-9f67-02c3c4b9e2b6",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "f996c2ce-ef29-458f-9c2f-99fcca9d1daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "2ea2f735-d7b2-4150-947d-54e5a2da4438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f996c2ce-ef29-458f-9c2f-99fcca9d1daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68444af-12dd-4f41-8dca-64c98d4863bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f996c2ce-ef29-458f-9c2f-99fcca9d1daf",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "cb20c287-c5a4-47df-bcc6-9a7e44ed0c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "06faf653-5ec4-4843-97a1-31c2594e57e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb20c287-c5a4-47df-bcc6-9a7e44ed0c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766dcc50-65c3-43ee-87d8-1ee48d6741d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb20c287-c5a4-47df-bcc6-9a7e44ed0c09",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "a5c0b3b0-6d99-44e0-93e8-37e146ffbf4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "00b1e9a9-404b-4617-a394-334be0fd756f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c0b3b0-6d99-44e0-93e8-37e146ffbf4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88306e51-9318-4316-a732-e0b82091fb1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c0b3b0-6d99-44e0-93e8-37e146ffbf4a",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "46aff08d-597d-49a1-aefc-67b0193aeee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "922bb264-31f4-4faa-b48e-8d0e903eae91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46aff08d-597d-49a1-aefc-67b0193aeee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "410eab9a-be54-46ba-8011-2d13e1446201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46aff08d-597d-49a1-aefc-67b0193aeee4",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "03321e80-0285-416f-80ff-54194b56b7b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "1f9dc76f-1c91-49de-a71c-e885dcdc0962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03321e80-0285-416f-80ff-54194b56b7b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b5f536-79d0-4ef2-bd7b-45a5bf18ee86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03321e80-0285-416f-80ff-54194b56b7b2",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "86071da7-2c34-4d84-998c-85090dcac191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "bac8b708-055c-4628-8396-93a385f6c689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86071da7-2c34-4d84-998c-85090dcac191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73fe2ad3-3707-44fd-ad0a-fd5515b3775c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86071da7-2c34-4d84-998c-85090dcac191",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "784bfbfb-43ba-4a04-a46a-8c47facae0f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "37f99e5a-95ad-4147-bcdf-f25c2666cf10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784bfbfb-43ba-4a04-a46a-8c47facae0f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa04cbda-b311-4ae7-a17a-4834ab196d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784bfbfb-43ba-4a04-a46a-8c47facae0f3",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "4569af60-f1a3-4387-ab29-ca30d80efd30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "5b657a0d-6736-4fd3-96e8-fc247800fe7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4569af60-f1a3-4387-ab29-ca30d80efd30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d1382b-f257-49c7-8f5c-b4cf17dc1df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4569af60-f1a3-4387-ab29-ca30d80efd30",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "4c003efe-2a89-4e8e-81b7-e942dd599bf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "a94155dc-36e1-4ce5-a905-27f5960655f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c003efe-2a89-4e8e-81b7-e942dd599bf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed1769da-c0e4-4b9d-921d-01666f6561a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c003efe-2a89-4e8e-81b7-e942dd599bf3",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "fd6d2de9-1980-4f57-89f5-bf921e4b8934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "38b3907d-d375-4f63-8430-eb587c94a2d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6d2de9-1980-4f57-89f5-bf921e4b8934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8eb6c5-68ea-4fe1-8827-6c4ad0d85469",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6d2de9-1980-4f57-89f5-bf921e4b8934",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "3070f977-30d5-4f18-bb2f-5c1f35d18649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fbf7ad7f-cccb-45e8-9e9d-d2c5a3af16ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3070f977-30d5-4f18-bb2f-5c1f35d18649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1509d27b-a302-41a7-bd7e-14ef83105d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3070f977-30d5-4f18-bb2f-5c1f35d18649",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "d39d9563-e2d6-45f1-a34a-849078d82c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "f0fc1385-00dc-4afe-8df2-8fcf653d6734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d39d9563-e2d6-45f1-a34a-849078d82c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af13e5e6-dbb0-4f14-8101-9a9bd8036542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d39d9563-e2d6-45f1-a34a-849078d82c37",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "65bb4f1f-6e0a-4197-97ff-d01668f47bd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "b5d4b8d0-380f-40f3-a793-e58a840493da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65bb4f1f-6e0a-4197-97ff-d01668f47bd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8ace4c-e37e-4a97-88a8-66280f0e70d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bb4f1f-6e0a-4197-97ff-d01668f47bd0",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "927158a4-7e45-47e0-9d15-54aad4e4b257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "070c750f-f88f-40d9-8b56-c4216001715b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927158a4-7e45-47e0-9d15-54aad4e4b257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7639641d-e0c7-482f-956e-ef9220aa76e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927158a4-7e45-47e0-9d15-54aad4e4b257",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "6529b501-07d1-4a43-a0d2-83c1d9e616ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "7de059e8-ecde-4ed6-94cb-d823eed40718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6529b501-07d1-4a43-a0d2-83c1d9e616ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a67633df-eae6-4188-9aa3-a6adb17a53f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6529b501-07d1-4a43-a0d2-83c1d9e616ee",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "6a9277d8-d4f6-4e1f-9e63-18d6f22b640d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "d27f0b60-4dc7-412b-b51e-c5247003918f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a9277d8-d4f6-4e1f-9e63-18d6f22b640d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6a62fb-833a-44f0-b0d6-263c2e8afa0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a9277d8-d4f6-4e1f-9e63-18d6f22b640d",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "0780bc66-a938-4568-ac93-553f5ef32fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "f5502bcb-0136-4d4a-abe7-192cfebe8981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0780bc66-a938-4568-ac93-553f5ef32fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f44e91-35f0-4d69-9807-a5d30e9e1525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0780bc66-a938-4568-ac93-553f5ef32fa6",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "189e1207-bff6-4556-8cc6-3aa504ada828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "8908037b-ec1b-45f9-a1fd-dd53b13cf251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189e1207-bff6-4556-8cc6-3aa504ada828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde2cc78-db7d-4e23-9a2a-3de58a952e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189e1207-bff6-4556-8cc6-3aa504ada828",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "c2c4b667-f383-4d92-8d94-1bf9a84d17b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "0a9e2e1a-33ee-4b4c-a440-9ab1925db2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c4b667-f383-4d92-8d94-1bf9a84d17b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a821f5a6-715d-48a7-ae5a-65e72cd3381d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c4b667-f383-4d92-8d94-1bf9a84d17b4",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "0a3cf761-8758-4c7f-bc8b-afa538f04207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "3eddeea2-1d63-4b45-8b63-fddef962613f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a3cf761-8758-4c7f-bc8b-afa538f04207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a30cdef-cb47-4f83-be14-c8ef154d05cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a3cf761-8758-4c7f-bc8b-afa538f04207",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "d1375dab-8837-4fd8-a1fa-8dab925bdd23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "09d1966a-130c-48c5-a049-eacf588b940f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1375dab-8837-4fd8-a1fa-8dab925bdd23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b772cc14-18c1-48d1-a1c2-c8c1ea0647c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1375dab-8837-4fd8-a1fa-8dab925bdd23",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "e460b069-254d-4c14-b986-d9316f0367bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "fbf1c5c7-b1ac-45ad-a53c-0213c9db1a35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e460b069-254d-4c14-b986-d9316f0367bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acdd8e19-cab6-4e15-9b21-322db2bce8e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e460b069-254d-4c14-b986-d9316f0367bf",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        },
        {
            "id": "ee385650-b9bc-455c-9687-24247b43c817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "compositeImage": {
                "id": "9e76ce6b-f411-44bf-9800-a9302e2e7cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee385650-b9bc-455c-9687-24247b43c817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb28be3e-c1a5-4f82-8d08-e7a3ae094cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee385650-b9bc-455c-9687-24247b43c817",
                    "LayerId": "a79cfec4-2b76-46ed-8cac-45bc44387c2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a79cfec4-2b76-46ed-8cac-45bc44387c2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1490cee5-05fe-40bb-a6c3-094ed5944d2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 35
}