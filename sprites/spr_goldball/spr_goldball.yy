{
    "id": "81579996-41a2-4fb7-862c-cc27b1feda91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goldball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 8,
    "bbox_right": 58,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e334f54f-778e-4710-8848-25574f59b4c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "f52082c7-1d28-4155-bb6d-49a57adde14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e334f54f-778e-4710-8848-25574f59b4c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b2723cd-096b-40b0-9b52-28ce42936478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e334f54f-778e-4710-8848-25574f59b4c1",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "f39ae6fb-00c0-48cc-9f7e-e24f6caa34c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "a5e3ba68-5091-4d44-9dfd-3f6eac666feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39ae6fb-00c0-48cc-9f7e-e24f6caa34c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1092f150-3839-4d71-bc55-f3ec92d44b10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39ae6fb-00c0-48cc-9f7e-e24f6caa34c6",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "6fbc36bb-1958-4b97-b559-c331b32347dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "b8e7ef89-dcd9-4f1f-ac58-dae9f0c686f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fbc36bb-1958-4b97-b559-c331b32347dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28a92425-6fed-4ead-ac1f-a27cdece599e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fbc36bb-1958-4b97-b559-c331b32347dc",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "08c51037-a9da-4845-8d83-a4ff6e29f897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "2ecfd90a-5bc1-4b4f-94e5-c8537e907390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c51037-a9da-4845-8d83-a4ff6e29f897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "236ec02a-f925-49a8-be02-0f94544a9af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c51037-a9da-4845-8d83-a4ff6e29f897",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "69c00873-caab-486e-9e9d-5d70cdb109c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "53a39392-d97c-4dfd-8dcb-25a57a473adf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69c00873-caab-486e-9e9d-5d70cdb109c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "028a2960-3c22-43a5-a78f-bd582ea2f87a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69c00873-caab-486e-9e9d-5d70cdb109c4",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "4579f046-a77f-49cc-b6c1-2bb45c6d4490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "60a2e47c-78ee-4256-bcc4-7ff6f130949e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4579f046-a77f-49cc-b6c1-2bb45c6d4490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8db3a2-e31a-4ecf-9cda-6a1ab058fc10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4579f046-a77f-49cc-b6c1-2bb45c6d4490",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "12040f1f-0d47-40f0-a8e1-209db3ddd353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "6c04f706-42cb-4791-81f0-ed72c061fece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12040f1f-0d47-40f0-a8e1-209db3ddd353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049e321b-7627-49cb-9225-7a7abe037513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12040f1f-0d47-40f0-a8e1-209db3ddd353",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        },
        {
            "id": "744691f9-0d09-4a32-8cac-410bbc2f0c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "compositeImage": {
                "id": "e5472165-453a-49b3-98c4-b13e940346c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "744691f9-0d09-4a32-8cac-410bbc2f0c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de13f320-b8d7-4cd0-84fd-1a0bacb1c111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "744691f9-0d09-4a32-8cac-410bbc2f0c33",
                    "LayerId": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "579e90d8-ffe3-47dc-bbdd-2bb93e99ab27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81579996-41a2-4fb7-862c-cc27b1feda91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}