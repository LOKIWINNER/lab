///maze_carve(grid id, passage, wall, x, y)

var grid = argument0
var pass = argument1;
var passf = -40;
var wall = argument2;

var xx = argument3;
var yy = argument4;


var temp = ds_list_create();


while(true){

    grid[# xx,yy] = passf;
    
    ds_list_clear(temp);
    
    if(grid_at(grid,xx+1,yy,pass) == wall && grid_at(grid,xx+2,yy,pass) == wall)
        ds_list_add(temp,0);
    if(grid_at(grid,xx,yy-1,pass) == wall && grid_at(grid,xx,yy-2,pass) == wall)
        ds_list_add(temp,1);
    if(grid_at(grid,xx-1,yy,pass) == wall && grid_at(grid,xx-2,yy,pass) == wall)
        ds_list_add(temp,2);
    if(grid_at(grid,xx,yy+1,pass) == wall && grid_at(grid,xx,yy+2,pass) == wall)
        ds_list_add(temp,3);
        
    if(!ds_list_empty(temp)){
    
        ds_list_shuffle(temp);
        var v = ds_list_find_value(temp,0);
        
        repeat(2){
            if(v == 0){xx++;};
            if(v == 1){yy--;};
            if(v == 2){xx--;};
            if(v == 3){yy++;};
            grid[#xx,yy] = passf;
        }
    
        continue;
    }

    ds_list_clear(temp);

    if(grid_at(grid,xx+1,yy,wall) == passf && grid_at(grid,xx+1,yy,wall) == passf)
        ds_list_add(temp,0);
    if(grid_at(grid,xx,yy-1,wall) == passf && grid_at(grid,xx,yy-2,wall) == passf)
        ds_list_add(temp,1);
    if(grid_at(grid,xx-1,yy,wall) == passf && grid_at(grid,xx-2,yy,wall) == passf)
        ds_list_add(temp,2);
    if(grid_at(grid,xx,yy+1,wall) == passf && grid_at(grid,xx,yy+2,wall) == passf)
        ds_list_add(temp,3);

    if(!ds_list_empty(temp)){
    
        ds_list_shuffle(temp);
        var v = ds_list_find_value(temp,0);
        
        repeat(2){
            grid[#xx,yy] = pass;
            if(v == 0){xx++;};
            if(v == 1){yy--;};
            if(v == 2){xx--;};
            if(v == 3){yy++;};
            grid[#xx,yy] = pass;
        }
    
        continue;
    }

    ds_list_destroy(temp);
    
    return grid;
}
