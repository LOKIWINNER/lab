///room_goto_transition(room, transition, [steps]);

if (!instance_exists(argument[3])) {
    with (instance_create_depth(0, 0, 0, argument[3])) {
        targetRoom = argument[0]; //The room we want to go to.
        kind = argument[1]; //The transition kind we want to use.
        
        if (argument_count >= 3 && argument[2] > 0) {
            time = argument[2]; //The amount of steps our transition will transpire (default is 30).
        }
    }
}