/// @description maze_edge(grid id, x, y, passage, wall)
/// @param grid id
/// @param  x
/// @param  y
/// @param  passage
/// @param  wall
var grid = argument0;
var xx = argument1;
var yy = argument2;
var pass = argument3;
var wall = argument4;

if(grid[#xx,yy] != pass && grid[#xx,yy] != -40) return false;

var ww = ds_grid_width(grid);
var hh = ds_grid_height(grid);

var inside = (yy > 0) && (yy < hh) && (xx > 0) && (xx < ww);


var qone = inside&&(yy >0) && (grid[#xx,yy-1] == pass);
var qtwo = inside&&(yy < hh-1) && (grid[#xx,yy+1] == pass);
var qthree = inside&&(xx > 0) && (grid[#xx-1,yy] == pass);
var qfour = inside&&(xx < ww-1) && (grid[#xx+1,yy] == pass);

return (((grid[#xx,yy-1] == pass) + (grid[#xx,yy+1] == pass) + (grid[#xx-1,yy] == pass) + (grid[#xx+1,yy] == pass)) <= 1)
