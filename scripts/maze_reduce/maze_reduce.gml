///maze_reduce(grid id, passage, wall);

var grid = argument0;
var pass = argument1;
var wall = argument2;

var w = ds_grid_width(grid);
var h = ds_grid_height(grid);
var ret = false;

var i,j;

for(i = 1; i < w-1; i++){
    for(j = 1; j < h-1; j++){
        if(maze_edge(grid,i,j,pass,wall)){ if(grid_at(grid,i,j,-192) != -192)grid[#i,j] = wall; ret = true;}
        //if(maze_edge(grid,j,i,pass,wall)){ if(grid_at(grid,j,i,-192) != -192)grid[#j,i] = wall; ret = true;}
        if(maze_edge(grid,w-i-1,j,pass,wall)){ if(grid_at(grid,w-i-1,j,-192) != -192)grid[#w-i-1,j] = wall; ret = true;}
        //if(maze_edge(grid,j,h-i-1,pass,wall)){ if(grid_at(grid,j,h-i-1,-192) != -192)grid[#j,h-i-1] = wall; ret = true;}
    }
}

return ret;
