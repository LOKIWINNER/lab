counter ++;
//Translition triggers
if(counter >= room_speed * 3){
	var change = choose(0,1);
	switch(change){
		case 0: state = ENEMY_STATE.normal; exit;
		case 1: 
		_x_input = choose(-1, 0, 1);
		_y_input = choose(-1, 0, 1);
		counter = 0;
		
		if (_x_input == 0 && _y_input == 0) {
			state = ENEMY_STATE.normal;
			action = ENEMY_ACTION.stand;
			exit;
		}
	}
	
	//Sprite
	action = ENEMY_ACTION.walk;
}


if(collision_ellipse(x-96*2,y-96*1.5,x+96*2,y+96*1.5, obj_hero, false, false)){
	state = ENEMY_STATE.alert;
	action = ENEMY_ACTION.walk;
	_x_input = 0;
	_y_input = 0;
	x_speed_ = 0;
	y_speed_ = 0;
	exit;
}



x_speed_ += _x_input * acceleration_;
y_speed_ += _y_input * acceleration_;

var _speed = point_distance(0, 0, x_speed_, y_speed_);
var _direction = point_direction(0, 0, x_speed_, y_speed_);

if (_speed > max_speed_) {
	x_speed_ = lengthdir_x(max_speed_, _direction);
	y_speed_ = lengthdir_y(max_speed_, _direction);
}

if (_x_input == 0) {
	x_speed_ = lerp(x_speed_, 0, .1);
}
if (_y_input == 0) {
	y_speed_ = lerp(y_speed_, 0, .1);

}



