

if instance_exists(obj_hero){
	var _speed = point_distance(x, y, obj_hero.x, obj_hero.y);
	var _direction = point_direction(x, y, obj_hero.x, obj_hero.y);
	
		
		
	
		if (round(_direction) > 225 && round(_direction) < 315) {
			facing = 3;
		} else if (round(_direction) > 135 && round(_direction) < 225) {	
			facing = 2;
		} else if (round(_direction) > 45 && round(_direction) < 135) {
			facing = 1;
		} else {
			facing = 0;	
		}
	
	if (collision_ellipse(x-26*2,y-26*1.5,x+26*2,y+26*1.5, obj_hero, false, false)){
		attack = true;
		
		state = ENEMY_STATE.attack;
		action = ENEMY_ACTION.attack;
		
		exit;
	}
	
	
	
	var r_a_m_f_ = run_acceleration_max_;			
	
	x_speed_ += _x_input * acceleration_;
	y_speed_ += _y_input * acceleration_;
	
	if (_speed > max_speed_ + r_a_m_f_) {
		x_speed_ = lengthdir_x(max_speed_ + r_a_m_f_, _direction);
		y_speed_ = lengthdir_y(max_speed_ + r_a_m_f_, _direction);
	}
	
	if (y_speed_ > 0) {_y_input = 1;} else if (y_speed_ < 0) {_y_input = -1;}
	if (x_speed_ > 0) {_x_input = 1;} else if (x_speed_ < 0) {_x_input = -1;}
	
	
	if (round(_direction) > 225 && round(_direction) < 315) facing = 3;
	else if (round(_direction) > 45 && round(_direction) < 135) facing = 1;
	
	
	//Transition triggers
	
	
	
	
	if(!collision_ellipse(x-96*2,y-96*1.5,x+96*2,y+96*1.5, obj_hero, false, false)){
		state = ENEMY_STATE.walk;
		action = ENEMY_ACTION.stand;
		_x_input = 0;
		_y_input = 0;
		exit;
	}
} else {
		state = ENEMY_STATE.walk;
		action = ENEMY_ACTION.stand;
		_x_input = 0;
		_y_input = 0;
		exit;
}
	//Sprite