var god_mode = argument[0];

// Move horizontally
if !run x_speed_ = lerp(x_speed_, 0, .1);

if (!place_meeting_3d(x + x_speed_ + sign(x_speed_), y, z, obj_box_par)){
	if(!place_free(x + x_speed_ + sign(x_speed_), y))                  
	{                                                               
	    while(place_free(x + sign(x_speed_), y))    
	    {                                                           
	        x += sign(x_speed_);                                      
	    }
	    x_speed_ = 0;
	}
} else {
	x_speed_ = 0;
}

x += x_speed_;




if (instance_exists(obj_maze_generator) && !god_mode){
	if x_speed_ > 0 {
		// Right collisions
		if (grid_place_meeting(self, obj_maze_generator.dungeon_new)) {
			x = bbox_right&~(CELL_WIDTH-1);
			boom_x = bbox_right&~(CELL_WIDTH-1);
			x -= bbox_right-x;
			x_speed_ = 0;
		}
	} else if x_speed_ < 0 {
		// Left collisions
		if (grid_place_meeting(self, obj_maze_generator.dungeon_new)) {
			x = bbox_left&~(CELL_WIDTH-1);
			boom_x = bbox_left&~(CELL_WIDTH-1);
			x += CELL_WIDTH+x-bbox_left;
			x_speed_ = 0;
		}
	}
}





// Move vertically
if !run y_speed_ = lerp(y_speed_, 0, .1);

if (!place_meeting_3d(x, y + y_speed_ + sign(y_speed_), z, obj_box_par)){
	if(!place_free(x, y + y_speed_ + sign(y_speed_)))                  
	{                                                               
	    while(place_free(x, y + sign(y_speed_)))    
	    {                                                           
	        y += sign(y_speed_);                                      
	    }
	    y_speed_ = 0;
	}
} else {
	y_speed_ = 0;
}
y += y_speed_;

// Vertical collisions
if (instance_exists(obj_maze_generator) && !god_mode){
	if y_speed_ > 0 {
		// Bottom collisions
		if (grid_place_meeting(self, obj_maze_generator.dungeon_new)) {
			y = bbox_bottom&~(CELL_HEIGHT-1);
			boom_y = bbox_bottom&~(CELL_HEIGHT-1);
			y -= bbox_bottom-y;
			y_speed_ = 0;
		}
	} else if y_speed_ < 0 {
		// Top collisions
		if (grid_place_meeting(self, obj_maze_generator.dungeon_new)) {
			y = bbox_top&~(CELL_HEIGHT-1);
			boom_y = bbox_top&~(CELL_HEIGHT-1);
			y += CELL_HEIGHT+y-bbox_top;
			y_speed_ = 0;
		}
	}
}






