/* SL_engine_ini_end();                */
/* Завершает инициализацию системы */

var i;

if sl_ambient_color        = -1  sl_ambient_color        = make_color_rgb(sl_tod[sl_tod_index,1],sl_tod[sl_tod_index,2],sl_tod[sl_tod_index,3]); // Цвет окружающего света
if global.sl_ambient_light = -1  global.sl_ambient_light = sl_tod[sl_tod_index,4]; // Мощность окружающего света

global.sl_light_gbuffer       = -1; // Глобальный размер буфера для рендеринга огней
//global.sl_lightlist[0]      = -1; // Список световых объектов
global.sl_castlist[0,0]       = -1; // Перечислите значения жеребьевки сделано с SL_cast_sprite
global.sl_castlist_index      = 0;  // Список sl_castlist управления
global.sl_texlist_light[0,0]  = -1; // Перечислите значения жеребьевки сделано с SL_draw_sprite_light
global.sl_texlist_light_index = 0;  // Список sl_texlist_light управления
global.sl_texlist_shad[0,0]   = -1; // Список значений жеребьевки с éffectués SL_draw_sprite_shadow
global.sl_texlist_shad_index  = 0;  // Список sl_texlist_shad управления

// Переменные, относящиеся к синхронизации отображения / буфера
if sl_buffer_sync
{if sl_buffer_xmargin = -1 { if view_hspeed[global.sl_viewid]>=0 sl_buffer_xmargin=view_hspeed[global.sl_viewid]+5 else sl_buffer_xmargin=10 }; // Буферная Маржа
 if sl_buffer_ymargin = -1 { if view_vspeed[global.sl_viewid]>=0 sl_buffer_ymargin=view_vspeed[global.sl_viewid]+5 else sl_buffer_ymargin=10 }};
else { sl_buffer_xmargin = 0 sl_buffer_ymargin = 0 };
sl_view_xprevious = camera_get_view_x(view_camera[global.sl_viewid]); // Подробная информация о представлении к предыдущему шагу
sl_view_yprevious = camera_get_view_y(view_camera[global.sl_viewid]);
sl_view_xspeed    = 0; // Скорость передвижения по мнению
sl_view_yspeed    = 0;

// Поверхности слоев
for (i=0; i<sl_layers_count; i+=1) sl_layers_surface[i] = -1;

// Переменные, связанные с солнечными оттенками
if sl_sunshadows_layerscale[0] = -1 for (i=0; i<sl_layers_count; i+=1) sl_sunshadows_layerscale[i] = (i+1)*(1/(sl_layers_count+1)); // Растяжение Факторы, солнечные оттенки для разных слоев
sl_sunshadows_refreshcounter   = sl_sunshadows_refreshrate; // Управление частоты обновления солнечных оттенков
sl_sunshadows_light            = 1; // Управление яркости солнечных оттенков
sl_sunshadows_margin           = max(sl_sunshadows_margin,sl_buffer_xmargin,sl_buffer_ymargin);

// Поверхности солнечных оттенков
sl_sunshadows_surface1[0] = -1;
sl_sunshadows_surface1[1] = -1;
sl_sunshadows_surface2    = -1;

// Поверхностные окружающей тени
sl_ambientshadows_surface = -1;

// буферные зоны
sl_buffer_surface1 = -1;
sl_buffer_surface2 = -1;
sl_buffer_width    = 0;
sl_buffer_height   = 0;
