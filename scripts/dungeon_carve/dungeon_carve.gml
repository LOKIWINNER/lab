/// @description dungeon_carve(grid_id, pass, wall, room_number, maze)
/// @param grid_id 
/// @param  pass
/// @param  wall
/// @param  room_number
/// @param  maze

var grid = argument0;
var pass = argument1;
var wall = argument2;
var r_number = argument3;
var maze = argument4;

var w = ds_grid_width(grid);
var h = ds_grid_height(grid);




var i,j;
var xx, yy;
var try = 100;


while(1){ // здесь создаются комнаты

    try = 100;
    r_number = argument3;
    ds_grid_clear(grid,wall);
    
    while(r_number > 0 && try > 0){
    
        //Pick Random Coords
        xx = irandom(floor(w/2))*2;
        yy = irandom(floor(h/2))*2;
        
        //ЕСЛИ ВЫ ХОТИТЕ ИЗМЕНИТЬ ВОЗМОЖНЫЕ РАЗМЕРЫ КОМНАТЫ, СДЕЛАЙТЕ ЗДЕСЬ
        //ИСПОЛЬЗОВАНИЕ СЛИШКОМ БОЛЬШОГО ИЛИ СЛИШКОМ МАЛЕНЬКОГО ИЛИ НЕТЧАТОГО ЧИСЛА МОЖЕТ БЫТЬ СОВЕРШЕННО РАЗРЫВАТЬ АЛГОРИТМ, ТАК ЧТО ОСТОРОЖНО
        var ww = 6+irandom(3)*2;
        var hh = 6+irandom(3)*2;
        
        //The room is free
        if(ds_grid_get_min(grid,xx-1,yy-1,xx+ww+1,yy+hh+1) == wall && xx+ww+2<w && yy+hh+2<h){
        //if(ds_grid_get_min(grid,xx-1,yy-1,xx+ww+1,yy+hh+1) == wall){
        
            //Carve it
            ds_grid_set_region(grid,xx+1,yy+1,xx+ww-1,yy+hh-1,pass);
            r_number--;
            
            //Add a door or 2 - добавить дверь
            var dx,dy;
			
            for(i = 0; i < 4; i++){
            
                if(i == 0){ dx = xx; dy = yy+2+irandom(hh-3);} // дверь слева (обязательна)
                if(i == 1){ dx = xx+2+irandom(ww-3); dy = yy} // дверь сверху
                if(i == 2){ dx = xx+ww; dy = yy+2+irandom(hh-3);} // дверь справа
                if(i == 3){ dx = xx+2+irandom(ww-3); dy = yy+hh} // дверь снизу
                
				// Заносим параметр если есть проходы
				// Для верха и низа -10, для лева и права - 20
                grid[#dx,dy] = -20*(dy == yy || dy == yy+hh) -10*(dx == xx || dx == xx+ww);   //-10 ud fix -20 lr fix
            }
			
            
            
            
        }
        try--;
    }
    
    if(r_number == 0)
        break;
    
}


for(i = 1; i < w; i+=2){
    for(j = 1; j < h; j+=2){
        if(grid[#i,j] == wall){
            maze_carve(grid,pass,wall,i,j); // тут создаются тропки
        }
    }
}


//Fix doors

for(i = 0; i < w; i++){
    for(j = 0; j < h; j++){
        if(i == 0 || j == 0 || i == w-1 || j == h-1){ grid[#i,j] = wall; continue;};
        // фикс дверей
		
		if(grid[#i,j] == -10){
            grid[#i,j] = pass;
            if(maze_edge(grid,i,j,pass,wall)){
                grid[#i,j] = wall;
                grid[#i,j+choose(1,-1)] = pass;
            }
        }
        if(grid[#i,j] == -20){
            grid[#i,j] = pass;
            if(maze_edge(grid,i,j,pass,wall)){
                grid[#i,j] = wall;
                grid[#i+choose(1,-1),j] = pass;
            }
        }
		
		
    }
}


//Уменьшить лабиринт
while(!maze){
    if(!maze_reduce(grid,pass,wall)) {
		ww = floor(w/2);
		hh = floor(h/2);
		grid[# ww,0] = 0;
		grid[# ww,h-1] = 0;
		grid[# 0,hh] = 0;
		grid[# w-1,hh] = 0;
		if (grid[# ww,1] = 1){grid[# ww,1] = 0;}
		if (grid[# ww,h-2] = 1){grid[# ww,h-2] = 0;}
		if (grid[# 1,hh] = 1){grid[# 1,hh] = 0;}
		if (grid[# w-2,hh] = 1){grid[# w-2,hh] = 0;}
		
		for(i = 0; i < w; i++){
		    for(j = 0; j < h; j++){
				val = ds_grid_get(grid,i,j);
				if (val !=1) val=0;
				ds_grid_set_region(dungeon_new, i*dungeon_fix_w, j*dungeon_fix_h, i*dungeon_fix_w+dungeon_fix_w, j*dungeon_fix_h+dungeon_fix_h, val);
			}
			
		}
		return 0;
	}
}


ww = floor(w/2);
hh = floor(h/2);
grid[# ww,0] = 0;
grid[# ww,h-1] = 0;
grid[# 0,hh] = 0;
grid[# w-1,hh] = 0;
if (grid[# ww,1] = 1){grid[# ww,1] = 0;}
if (grid[# ww,h-2] = 1){grid[# ww,h-2] = 0;}
if (grid[# 1,hh] = 1){grid[# 1,hh] = 0;}
if (grid[# w-2,hh] = 1){grid[# w-2,hh] = 0;}

for(i = 0; i < w; i++){
    for(j = 0; j < h; j++){
		val = ds_grid_get(grid,i,j);
		if (val !=1) val=0;
		ds_grid_set_region(dungeon_new, i*dungeon_fix_w, j*dungeon_fix_h, i*dungeon_fix_w+dungeon_fix_w, j*dungeon_fix_h+dungeon_fix_h, val);
	}
	
}


return 0;
