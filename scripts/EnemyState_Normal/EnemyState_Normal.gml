// Ограничение движений на концах комнаты



	_x_input = 0;
	_y_input = 0;

if (_x_input == 0) {
	x_speed_ = lerp(x_speed_, 0, .1);
}
if (_y_input == 0) {
	y_speed_ = lerp(y_speed_, 0, .1);
}
//Behaviour
counter++;

if(counter >= room_speed * 3){
	var change = choose(0,1);
	switch(change){
		case 0: state = ENEMY_STATE.walk;
		case 1: counter = 0; break;
	}
}

//Sprite
action = ENEMY_ACTION.stand;

if(collision_ellipse(x-96*2,y-96*1.5,x+96*2,y+96*1.5, obj_hero, false, false)){
	state = ENEMY_STATE.alert;
	action = ENEMY_ACTION.walk;
	x_speed_ = 0;
	y_speed_ = 0;
	_x_input = 0;
	_y_input = 0;
	exit;
}