///@description scr_cutscene_instance_destroy
///@arg object_id

with (argument0){
	instance_destroy();	
}

scr_cutscene_end_action();