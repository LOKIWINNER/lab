
//Transition triggers
if instance_exists(obj_hero){
	run = false;

if (attack && animation_hit_frame_range(2, 3)){
		ProcessAttack(obj_hero, facing, 10, self);
		attack = false;
}
			
	if(animation_end()){
		
		var _direction = point_direction(x, y, obj_hero.x, obj_hero.y);
			
			if (round(_direction) > 225 && round(_direction) < 315) {
				facing = 3;
			} else if (round(_direction) > 135 && round(_direction) < 225) {	
				facing = 2;
			} else if (round(_direction) > 45 && round(_direction) < 135) {
				facing = 1;
			} else {
				facing = 0;	
			}
		
		if(collision_ellipse(x-26*2,y-26*1.5,x+26*2,y+26*1.5, obj_hero, false, false)){
			
			state = ENEMY_STATE.attack;
			action = ENEMY_ACTION.attack;
			attack = true;
			
			
			//ds_list_clear(hitByAttack);
			exit;
		} else {
			state = ENEMY_STATE.alert;
			action = ENEMY_ACTION.walk;
			
			exit;
		}
	} 
} else { // Если герой мёртв, то будет гулять
		state = ENEMY_STATE.walk;
		action = ENEMY_ACTION.stand;
		_x_input = 0;
		_y_input = 0;
		exit;
}