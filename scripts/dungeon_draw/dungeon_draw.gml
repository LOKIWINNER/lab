/// @description dungeon_draw(grid id, sprite, wall, x , y);
/// @param grid id
/// @param  sprite
/// @param  wall
/// @param  x 
/// @param  y

var grid = argument0;
var spr = argument1;
var wall = argument2;

var xx = argument3;
var yy = argument4;

var sw = sprite_get_height(spr);

var ww = ds_grid_width(grid);
var hh = ds_grid_height(grid);

for(i = 0; i < ww;i++){
    for(j = 0; j < hh;j++){
		if(grid[#i,j] == wall){
	
			var qone = (i < ww-1) && (grid[#i+1,j] != wall); // право
			var qtwo = (j > 0) && (grid[#i,j-1] != wall); // верх
			var qthree = (i > 0) && (grid[#i-1,j] != wall); // лево
			var qfour = (j < hh-1) && (grid[#i,j+1] != wall); // низ
			
			var _tile_index = qone+2*qtwo+4*qthree+8*qfour;
			draw_sprite(spr,_tile_index,i*sw+xx,j*sw+yy);
		}

    }
}





