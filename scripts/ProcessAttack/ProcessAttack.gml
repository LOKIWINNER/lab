/// @description animation_end(sprite_index, facing, damage, inst)
/// @param object 
/// @param facing 
/// @param damage
/// @param inst


var _obj = argument0;
var _facing = argument1;
var _damage = argument2;
var _inst = argument3;

var sx, sy, ww, hh;
		
var old_mask = mask_index;
_inst.mask_index = -1;

ww = (_inst.bbox_right - _inst.bbox_left)/2;
hh = (_inst.bbox_bottom - _inst.bbox_top)/2;

switch _facing {
	case 0:
		sx = _inst.bbox_left + ww; 
		sy = _inst.y;	
	break;
	case 1:
		sx = _inst.x; 
		sy = _inst.bbox_bottom - hh;	
	break;
	case 2:
		sx = _inst.bbox_right - ww; 
		sy = _inst.y;	
	break;
	case 3:
		sx = _inst.x; 
		sy = _inst.bbox_top + hh;	
	break;
}
_inst.mask_index = old_mask;



if (attack) {
	with (instance_create_depth(sx, sy, 0, obj_hero_attack)){
		image_xscale = ww*2/32;
		image_yscale = hh*2/32;
	
		obj = _obj;
		damage = _damage;
		inst = _inst;
	}
}


