///@description create_ds_grid_from_array
///@arg type
///@arg array

var ds_grid;
var type = argument0;
var array = argument1;

if is_array(array[0]){ // При создании новых квестов, в последствии должен будет загружаться из файла

	switch (type){
		case "all":
			var array_num_quests = array_length_1d(array);
			var array_w = 8; // Нужно указать наибольшее возможное значение array_length_1d(array[0]);
			ds_grid = ds_grid_create(array_num_quests, array_w);
			var xx = 0;	repeat (array_num_quests){
				var quest_array = array[xx];
				var array_h = array_length_1d(quest_array);
				var yy = 0; repeat(array_h){
					ds_grid[# xx, yy] = quest_array[yy];
					yy++;
				}
				
				if (quest_array[2] == 0){
					var _ds_map = obj_statistics.ds_map_enemy;
					var _qa = quest_array[5];
					var _num = array_length_1d(_qa);
					if (is_array(_qa) || _num > 1){
						var _array = [];
						var i = 0 repeat(_num){
							_array[@ i] = ds_map_find_value(_ds_map, _qa[i]);
							i++
						}
					} else {
						_array = ds_map_find_value(_ds_map, _qa);
					}
					ds_grid[# xx, array_w - 1] = _array; // Добавляем количество, сколько сейчас уже убитых
				}

				xx++;
			}
		break;
	}
	return ds_grid;
} else { // При добавлении квеста от NPC
	switch (type){
		case "kill":
			ds_grid = obj_quests.ds_quests;
			var ds_quests_w = ds_grid_width(ds_grid);
			var ds_quests_h = ds_grid_height(ds_grid);
			ds_grid_resize(ds_grid, ds_quests_w + 1, ds_quests_h);
			

			var array_num_quests = array_length_1d(array);
			var xx = 0;	repeat (array_num_quests){
				ds_grid[# ds_quests_w, xx] = array[xx];
				xx++
			}
			var _ds_map = obj_statistics.ds_map_enemy;
			var _qa = array[5];
			var _num = array_length_1d(_qa);
			if (is_array(_qa) || _num > 1){
				var _array = [];
				var i = 0 repeat(array_length_1d(_qa)){
					_array[@ i] = ds_map_find_value(_ds_map, _qa[i]);
					i++
				}
			} else {
				_array = ds_map_find_value(_ds_map, _qa);
			}
			ds_grid[# ds_quests_w, xx] = _array; // Добавляем количество, сколько сейчас уже убитых
		break;
	}

}






