/// @function place_meeting_3d
/// @arg x
/// @arg y
/// @arg z
/// @arg obj

//Args
var _x = argument[0];
var _y = argument[1];
var _z = argument[2];
var _obj = argument[3];

var _list = ds_list_create();
var xyMeeting = instance_place_list(_x, _y, _obj, _list, 0);
var zMeeting = 0;
if (xyMeeting > 0){
	//var _grid = ds_grid_create(2, xyMeeting);
	for (var i = 0; i < xyMeeting; ++i;){
		if (rectangle_in_rectangle(0, _list[| i].z, 1, _list[| i].z + _list[| i].height, 0, _z, 1, _z + height)){
			zMeeting++;	 
		}
		//_grid[# 0, i] = _list[| i].id;
		//_grid[# 1, i] = _list[| i].z_floor_;
	}
	//ds_grid_sort(_grid, 1, false)
	//var _inst = _grid[# 0, 0];
	//var _inst2 = _grid[# 0, xyMeeting-1];
	//ds_grid_destroy(_grid);
	//
	//
	//
	//if (rectangle_in_rectangle(0, _inst.z, 1, _inst.z + _inst.height, 0, _z, 1, _z + height)){
	//	zMeeting++;	 
	//} else if (rectangle_in_rectangle(0, _inst2.z, 1, _inst2.z + _inst2.height, 0, _z, 1, _z + height)){
	//	zMeeting++;	 
	//}
	
	//ds_grid_destroy(_grid);
} 

var _list2 = ds_list_create();
var xyMeeting2 = instance_place_list(x, y, obj_box_par, _list2, 0);
if (xyMeeting2 > 0){
	var _grid2 = ds_grid_create(2, xyMeeting2);
	for (var i = 0; i < xyMeeting2; ++i;){
		_grid2[# 0, i] = _list2[| i].id;
		_grid2[# 1, i] = _list2[| i].z_floor_;
	}
	ds_grid_sort(_grid2, 1, false)
	
	var __inst = _grid2[# 0, 0];
	var __inst2 = _grid2[# 0, xyMeeting2-1];
	ds_grid_destroy(_grid2);
	
	if (z > __inst.z){
		z_floor_ = __inst.z_floor_;
	} else if (__inst.z > z && z > __inst2.z){
		z_floor_ = __inst2.z_floor_;
	} else {
		z_floor_ = 0;
	}
} else {
	z_floor_ = 0;
}
ds_list_destroy(_list2);


if(xyMeeting && zMeeting){
	coll = 1;	
} else {
	coll = 0;
}




return xyMeeting && zMeeting;