var resolution = argument0;

var width = 0;
var height = 0;

switch(resolution){
	case 0: // 1920 x 1080
		width = 1920;
		height = 1080;
	break;
	case 1: // 1680 x 1050
		width = 1680;
		height = 1050;
	break;
	case 2: // 1600 x 1024
		width = 1600;
		height = 1024;
	break;
	case 3: // 1600 x 900
		width = 1600;
		height = 900;
	break;
	case 4: // 1400 x 990
		width = 1400;
		height = 900;
	break;
	case 5: // 1366 x 768
		width = 1366;
		height = 768;
	break;
	case 6: // 1280 x 1024
		width = 1280;
		height = 1024;
	break;
	case 7: // 1280 x 800
		width = 1280;
		height = 800;
	break;
	case 8: // 1280 x 720
		width = 1280;
		height = 720;
	break;
	case 9: // 1024 x 768
		width = 1024;
		height = 768;
	break;
	case 10: // 640 x 480
		width = 640;
		height = 480;
	break;	
}


display_set_gui_size(width, height)
menu.width = width; menu.height = height;
window_set_size(width, height);
//surface_resize(application_surface, display_get_gui_width(), display_get_gui_height());

var baseWidth = 1280;
var baseHeight = 720;
var aspect = baseWidth/baseHeight;


if (width >= height){
	var _height = min(baseHeight, height);
	var _width = height * aspect;
}

//surface_resize(_width, _height);

global.view_width = width;
global.view_height = height;
view_set_wport(0, _width);
view_set_hport(0, _height);
window_center();



