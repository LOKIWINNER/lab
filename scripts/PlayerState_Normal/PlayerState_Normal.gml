//Normal State



// Ограничение движений на концах комнаты
if x>room_width x=room_width; if x<0 x=0; if y>room_height y=room_height; if y<0 y=0;
//if(_x_input != 0 || _y_input != 0) { run = true;}





var _speed = point_distance(0, 0, x_speed_, y_speed_);
var _direction = point_direction(0, 0, x_speed_, y_speed_);

var r_a_m_f_ = 0; 
if (input_run && sp > 0 && !shield) {
	r_a_m_f_ = run_acceleration_max_;
	sp -= .5;
}



if (_speed > max_speed_ + r_a_m_f_) {
	x_speed_ = lengthdir_x(max_speed_ + r_a_m_f_, _direction);
	y_speed_ = lengthdir_y(max_speed_ + r_a_m_f_, _direction);
}



// Постепенно уменьшаем скорость если не нажаты клавиши
if (_x_input == 0 or !run or shield) {
	x_speed_ = lerp(x_speed_, 0, .1);
}
if (_y_input == 0 or !run or shield) {
	y_speed_ = lerp(y_speed_, 0, .1);
}









if (input_jump && z <= z_floor_+1){
	z_speed_ = -z_jump_speed_;
}
z_speed_ += z_grav_;
if (place_meeting_3d(x, y, z-z_speed_, obj_box_par)){
    z_speed_ = 0;
}

if(z_speed_ < 0 and !input_jump_hold) z_speed_ = 0;
if (z - z_speed_ < z_floor_){
	z_speed_ = 0;
	z = z_floor_;
}
z -= z_speed_;






// Анимация атаки если нет прыжка
if(input_attack)
{
	attack = true;
    state = PLAYER_STATE.attack_one;

    exit;
}


// Анимация прыжка
if(z > z_floor_+1 && !shield){
	action = PLAYER_ACTION.jump;
	exit;
}





// Стандартное положение игрока
if (!shield) {
	action = PLAYER_ACTION.stand;
} else {
	action = PLAYER_ACTION.stand_shield;
}



if(_x_input != 0 || _y_input != 0)
{
	if (!shield) {
		action = PLAYER_ACTION.run;
		max_speed_ = lerp(max_speed_, 4, .1);
	} else {
		action = PLAYER_ACTION.run_shield;
		max_speed_ = lerp(max_speed_, 1, .1);
	}
}



// Поднять щиты =)
if(input_shield)
{
    shield = true;
    exit;
} else {
	shield = false;	
}

