/// @description grid_at(grid,x,y,default)
/// @param grid
/// @param x
/// @param y
/// @param default

//Returns default if outside grid
//Or the original value if inside grid

var gr = argument0;
var w = ds_grid_width(gr);
var h = ds_grid_height(gr);

var xx = argument1;
var yy = argument2;

var inside = (xx >= 0) && (xx < w) && (yy >= 0) && (yy < h);

if(!inside)
    return argument3;
else
    return gr[#xx,yy];
