/// @description show_message_up(text)
/// @param text 


var _text = argument0;

with (obj_control){
	show_mess_up = _text;
	show_debug_message(show_mess_up);
	sy = 0;
}