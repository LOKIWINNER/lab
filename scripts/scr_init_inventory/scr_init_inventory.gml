/// @param InventoryName
/// @param Visible

var inv = ds_map_create();
ds_map_add(inv, "tabMap",	ds_map_create());
ds_map_add(inv, "invName",	argument0);
ds_map_add(inv, "locked",	false);
ds_map_add(inv, "visible",	argument1);
ds_map_add(inv, "tabList",	ds_list_create());

// Add the Inventory to the Master List
ds_list_add(obj_inventory_controller.masterInvList, inv);

// return the Map
return inv;



/*// INVENTORY STRUCTURE

Layer 0 -	Master List:		Вмещает весь инвентарь.
Layer 1 -	Inventory Map:		Содержит основную инвентарную информацию, а также карту для всех вкладок Хранит основную инвентарную информацию, а также карту для всех вкладок.
Layer 1.5 - Tab List:			Содержит имя всех вкладок
Layer 2 -	Tab Map:			Содержит основную информацию о вкладках, а также таблицу вкладок
Layer 3 -	Tab Grid:			Содержит ячейки вкладок
Layer 4 -	Cell Map:			Содержит базовую информацию о ячейке

*/// INVENTORY STRUCTURE