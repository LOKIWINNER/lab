/// @description draw_my_button(sprite*)
/// @param sprite 

if instance_exists(obj_textbox) exit;

if (argument0) {
	var _sprite = argument0;
} else {
	_sprite = spr_press_button;
}

draw_sprite(_sprite, image_index, round(obj_hero.x), round(obj_hero.y) - obj_hero.z - 50);
draw_text(round(obj_hero.x) - string_width("E")/2, round(obj_hero.y) - string_height("E") - obj_hero.z - 50, "E");
