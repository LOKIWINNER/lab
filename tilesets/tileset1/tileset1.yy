{
    "id": "f08c881a-07b8-4a02-89d1-06fa98ed45f1",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset1",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 2,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "8aa96ca6-eab3-444c-b868-04c3acd0a188",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            0,
            0,
            0,
            1,
            2,
            3,
            4,
            2,
            3,
            4,
            1,
            3,
            4,
            1,
            2,
            4,
            1,
            2,
            3
        ],
        "SerialiseFrameCount": 4
    },
    "tile_animation_frames": [
        {
            "id": "8fe725d6-a91d-43e1-9a4e-a9c69a426bc0",
            "modelName": "GMTileAnimation",
            "mvc": "1.0",
            "frames": [
                1,
                2,
                3,
                4
            ],
            "name": "animation_1"
        }
    ],
    "tile_animation_speed": 15,
    "tile_count": 5,
    "tileheight": 96,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 96,
    "tilexoff": 0,
    "tileyoff": 0
}