{
    "id": "b76a3629-9b1c-49a4-9a8c-bae3863f5270",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_big",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e73d9d84-3180-455d-b24f-4c230c7f9c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4e48cc05-a0a9-4ef5-86fa-a5a80b33d4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 335,
                "y": 66
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0c84c1ef-1a2d-411d-8dd0-533106e5cc61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 339,
                "y": 66
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6dac9733-fcc6-43cc-8c7e-fda316e13f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 347,
                "y": 66
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "077a09ca-96f4-4bd6-ab35-5fbe2aa73026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 367,
                "y": 66
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8e5e9474-c736-4f5f-b199-892f20f0597c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 381,
                "y": 66
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e7a1888c-e0f4-45fa-a8ef-7cab93c7af36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 398,
                "y": 66
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "96178105-af65-43ca-8c47-27ef88a82701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 412,
                "y": 66
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3aa3a3ae-0994-484f-ac7d-e9dd18df7ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 427,
                "y": 66
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c8cceff7-805b-4fef-985a-69399bec9104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 500,
                "y": 66
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "98cdcdc5-2812-4cd7-835d-c950f305ee2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 435,
                "y": 66
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "09dc3522-dedf-4455-b0db-85a9811b882e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 447,
                "y": 66
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fd63049b-518b-4908-911a-6a7d238769b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 457,
                "y": 66
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1023307a-71e0-40ab-8a78-0431b8f691cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 462,
                "y": 66
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "103fc912-825c-41e7-98e8-a0b883f9cf2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 471,
                "y": 66
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "22014881-88cc-4c1c-bbe1-e005e1810dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 476,
                "y": 66
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f8d09f44-f50c-47a8-8831-214fe6c5cc20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 487,
                "y": 66
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c388fcfd-f080-47ac-b465-82dfc4cee57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 327,
                "y": 66
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4d53d764-d813-44a0-bf32-000790d49b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 416,
                "y": 66
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "69965f3d-6280-4d48-a87f-fd8b6a0f3fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 316,
                "y": 66
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "89fbe463-e9d3-4e56-8ccb-970ce2502004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 179,
                "y": 66
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2c8b1e8d-d44c-4ff8-904b-b0b901924e54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 106,
                "y": 66
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "68eec311-6e8f-4741-a5c9-ad37939c45e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 119,
                "y": 66
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "98f6a6ff-7b4c-4db7-b964-3fe5702a77e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 131,
                "y": 66
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "21a70f8b-d7ca-4ac6-bc3f-f40934a43636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 144,
                "y": 66
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e084c648-4409-429d-b2fb-f4f357c561e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 157,
                "y": 66
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "08e67615-153a-4d1b-bf3e-2c0603bde0cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 170,
                "y": 66
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b72f8acb-2724-4ac9-bbdf-dc7fa0f3edff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 174,
                "y": 66
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "fa50d6bb-d78c-47d5-bfee-78e9df130d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 192,
                "y": 66
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "18e3218f-0bef-452a-a546-618341f7a36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 293,
                "y": 66
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "92ca02e8-62d3-4bfa-89cf-0c741a286ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 200,
                "y": 66
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "988df0c0-dfe7-422d-9b18-5a9886629b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 209,
                "y": 66
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "44b79495-3b4f-4ccc-af57-e6114bb2e3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 220,
                "y": 66
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "721b1ba8-e27d-4546-b116-b414e770e0f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 239,
                "y": 66
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ad4d06d3-e609-4493-93b2-065463b62843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 254,
                "y": 66
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "03f4b92c-7ce1-42ba-ad57-98716a2f2170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 266,
                "y": 66
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ab76a479-1009-4311-b74d-57c12a802e22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 279,
                "y": 66
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "086f000b-8787-4152-b5c6-476b5e8902e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 303,
                "y": 66
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "035858fa-5cff-4481-bac1-f324fe3dec97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a45089de-87ba-4c9d-93aa-93e612b006f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 14,
                "y": 98
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "55e125d7-c655-4296-9d56-c3f9e6217c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 29,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a7bf01c4-e5f4-499a-a120-ab28b3c9a610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 252,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "fb224931-0778-480f-85cf-b94993ad547d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 264,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4b960f14-dd01-4fa1-83fc-7ade0136298b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 278,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "52c83a82-7683-404e-9a42-0a484ab570e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 291,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "89a5fe52-8698-40ae-ab34-0b0e60a1caec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 303,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5d8615e3-5cc7-45b4-820f-0c03c2c37ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 322,
                "y": 98
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4ee720fc-bce9-4a43-8610-25d17a082536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 339,
                "y": 98
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "239596ea-b499-4e55-8c51-536b93932633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 356,
                "y": 98
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c9a4ea1d-f5e8-487e-a223-b8251ee342ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 367,
                "y": 98
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5fa3c925-979a-492e-9be7-403620a851b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 386,
                "y": 98
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2e410d99-f47b-4c53-8a1f-6b3736e01974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 400,
                "y": 98
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "97a64642-7402-434b-b30f-f37a72f4e053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 415,
                "y": 98
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3490b6ef-84f8-4456-a393-9313048546e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 431,
                "y": 98
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "521929cd-b8a1-4b06-b272-a1e14b711f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 445,
                "y": 98
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a94b66a1-9ff2-4c83-b382-e1ebf073dc5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 460,
                "y": 98
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ae460bce-3d91-46b6-ada8-c60ffc65138d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 483,
                "y": 98
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9f393663-e608-4ae6-8ef9-b945c0a3c3d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "53e03385-5c91-4631-b6c9-c605a416e13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 237,
                "y": 98
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0dbee229-802b-44ae-a7d6-e7186eceb316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 230,
                "y": 98
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4d0a4112-2328-4b78-b0fd-1ad8695ba542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 220,
                "y": 98
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6818bfde-f142-4455-a539-558c22138f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 121,
                "y": 98
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b1b55cdb-aec9-445b-a795-18e18e7e5d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 44,
                "y": 98
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bf663ec2-0dfa-46b7-8e40-2eb23fb2cb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 54,
                "y": 98
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "94e4f9d8-7c4c-4f6a-a9bc-5ff8ac3ddf08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 4,
                "x": 70,
                "y": 98
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d882dcc0-b384-4e7d-b4ad-9ae92bb6ddfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 76,
                "y": 98
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a68e33f6-0630-485f-85f3-2d456f69b14d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 87,
                "y": 98
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f8762e5f-2185-47a5-a0c2-6907bbfa35e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 98,
                "y": 98
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1877ca7e-97a8-48a9-85e0-6cec7ce20dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 109,
                "y": 98
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "77bc2162-7917-41da-9b47-7b89256a32a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "97dd44cf-5bdc-4990-8267-67ac754fe7d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 209,
                "y": 98
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d7ee6434-5349-4d8e-9524-c3c04a5e2074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 140,
                "y": 98
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "673eded8-991c-4d44-b022-11f0147e89a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 151,
                "y": 98
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "07da1dca-f0e1-42eb-ad78-de00eabc985c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 3,
                "x": 163,
                "y": 98
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4c62552c-76c0-4a6a-96b2-81d2e6673fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 168,
                "y": 98
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b91188ca-c90b-462c-9a88-e2181199d6a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 177,
                "y": 98
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "57028a0a-e836-4097-a4ec-4da3c978a737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 188,
                "y": 98
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "47dd780f-6a8d-4f33-b506-3180974bf025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 192,
                "y": 98
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e5de0d0c-d9bc-42d6-8e01-4953a64f6369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 95,
                "y": 66
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9e7354e9-7d15-45d1-a1d8-74498804eac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 17,
                "y": 130
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b5308eaa-27af-49e7-9537-45e1c423fe13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 84,
                "y": 66
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0f5c596d-b866-4eb6-9825-b64351a0746b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 62,
                "y": 66
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9de44dfa-7576-4ae6-9d64-8892b78834d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 313,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bc2c3a0e-a938-4bdb-87d3-7eb21a37763b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "03491dd9-8887-4699-b76f-10736567da75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "79feabed-e3ee-4aeb-8441-177715e2e93a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "49dac8f5-da78-454c-a23c-91c15f7aee6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a25dd3a9-4ae2-4e50-8111-7684cc17434a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d4318fe8-03a4-4e8d-a9f2-9486a9fad997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 381,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "18320e91-45df-4ed9-81c0-2adb6f659858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "174f628d-a68f-453a-8027-509b1b1bcff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 493,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3adfce50-f2fa-4c7b-8248-12cec00e64a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1564eb71-6fa1-4d25-8514-9916b4ed799d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 4,
                "shift": 9,
                "w": 1,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "708118bf-9f57-487f-87b5-92c771de1051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 431,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a2e030e2-9b7d-4ad4-bf20-d8c535ec2a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "6c7be3eb-ef67-4378-a80d-2959247356d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 453,
                "y": 2
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "dfdcf49e-d729-4965-999c-838dc899cbb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 466,
                "y": 2
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "d172e966-80a5-485c-bce8-b86dc2f46371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 481,
                "y": 2
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "6c6f6974-b8d5-4c54-af31-ee638114ae0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "86e4f810-341b-4525-9218-1a88217d9df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "42d0004b-3a31-495c-a38d-306ea4debcbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "a24de178-ad02-4826-9033-e5e40fb97961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "5550f7ad-83cd-4248-9e1b-a56423a4aa58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "70ec7159-7c36-46ed-a4cf-0072923ddda0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "327a40e8-ee86-4fd1-8a7d-6e484875d56e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "45077ce1-974a-4ea3-84fe-55f628d52703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "b87d372b-12f4-425d-9806-7f68f2c2f567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "5269a34c-0354-4502-8a88-f3e39e60d26b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "1fc58597-fc86-434c-83d9-069ed635bf3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "cb239dac-6029-481d-939f-a406541ea483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 30,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "fbd78895-b740-4f70-9887-db628221b44a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "cd239aee-1dbd-444d-95cb-4c0938e39ef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "91e9f299-0238-4d61-b163-2a1616fba658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "f3ca4d37-5056-40a8-9c50-d9b988021830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "e64e4f27-4a4d-4b6c-bc67-29775e57f372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "48d0f528-1886-47e3-9f54-16141c242a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "a030346c-0533-4044-b1dd-c5d549a02988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "fe8889af-6633-4cd5-98a6-6657c45635fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "e00cebfa-55a8-45c8-9944-3d9a0a26fdec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 268,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "14c074f0-7162-474e-ace9-cf498b1cd442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "2141ed19-e888-42ef-b05d-1ba3d76d32b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 15,
                "y": 34
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "42897f6f-db26-401f-adcd-79829cf3f954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 34,
                "y": 34
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "5543276e-2281-423d-8635-9bedb9071b4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 326,
                "y": 34
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "071b648f-b140-49a4-8cf2-a097b8c817f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 30,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 343,
                "y": 34
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "8ce7e63d-92ae-4744-9d8a-b55844daf3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 365,
                "y": 34
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "1d42ff10-dfcd-423e-a7b7-22d32dc02572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 377,
                "y": 34
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "98d608e9-be41-432c-8f4b-f384731634af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 30,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 391,
                "y": 34
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "eb07e992-2f90-4e1a-bf0b-00960d1d4af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 415,
                "y": 34
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "1cb57b08-10c0-4caa-815d-f5cdfc924134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 429,
                "y": 34
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "1a73da95-bd0e-459b-96f2-602d65251df4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 440,
                "y": 34
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "ad16d645-a191-4b1e-914d-016b477fe7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 452,
                "y": 34
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "978ae439-2727-492e-bc1f-0ca0396eef7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 463,
                "y": 34
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "63efde33-e709-4cc6-846b-236d4bc50f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 474,
                "y": 34
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "1f026a8b-4278-4ff4-9c15-2e0645b79701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 488,
                "y": 34
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "309ac35c-beea-40c4-a2cc-1c207a0261ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "0d413c2f-28cd-47e8-99c0-94ce5d940c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 30,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 66
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "b7d9d265-134f-4477-a8c3-00e72a19d641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 25,
                "y": 66
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "8b5f5a9f-b746-4491-af26-b13b184ac454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 38,
                "y": 66
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "dbcd8718-5cd9-4f94-81f6-8a9241099764",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 51,
                "y": 66
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "9ef8e82f-552e-418c-97f9-343d0d0f0e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 312,
                "y": 34
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "b4ca1654-26c1-4e58-b951-a321618716d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 295,
                "y": 34
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "8235c416-f6b1-4c26-a349-28d0a7ee3b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 283,
                "y": 34
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "ad66a6c4-0c91-4875-baf1-e5c339a9a1d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 142,
                "y": 34
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "11d1c353-643a-4ece-b3e7-1564477b50e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 55,
                "y": 34
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "369145e0-e878-4ca6-8b22-ea7e9cbbed6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 67,
                "y": 34
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "c7064c6a-fc60-48ee-9232-8954c3e12625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 78,
                "y": 34
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "797e4fad-e953-45c6-9fcd-3a89c0926ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 89,
                "y": 34
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "1bf24904-cdd8-44c2-b6d7-1ca9cc866269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 99,
                "y": 34
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "72895f9e-e996-44ac-9744-b32973371500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 112,
                "y": 34
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "dd07614e-c258-4b1c-882d-d49bff79df5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 129,
                "y": 34
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "c7beeafb-cece-4b5b-ab41-6c72bd855083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 155,
                "y": 34
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "6c9e7ace-a458-4a88-9d74-824981134cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 272,
                "y": 34
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "73540d25-009e-47b9-b81f-68c01356dd34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 167,
                "y": 34
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "1608370a-9e16-4f19-8df2-c76fca0b06b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 183,
                "y": 34
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "7fbb26c3-b911-4d55-a53a-ee81c3987cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 200,
                "y": 34
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "271f2d62-1713-447c-9cc8-6042087d10f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 215,
                "y": 34
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "6973c288-c4bf-4b15-8200-5b9aa6f5c2ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 232,
                "y": 34
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "60013a49-d94d-4483-8bc1-f7d791635f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 242,
                "y": 34
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "eb1b626d-21b5-4000-83b9-a4c19b588c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 253,
                "y": 34
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "487dc630-b3e2-4292-aba4-d9cbb6dc70a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 73,
                "y": 66
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "c663a5ce-004f-4d0b-adac-dd8f842f2bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 28,
                "y": 130
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}