{
    "id": "c9617552-65ea-4207-9830-9d3dca5d8324",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_engine",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b8ab786f-5119-4301-a8e6-3836750042b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a533027f-20f4-4d3d-9568-e383d4e68bdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ce94996f-a4b2-499d-836f-4b4c351d27be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 239,
                "y": 56
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cc580d1c-26b5-4ea1-9d7f-9552a504f4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8ed2066e-a149-4aad-8642-caf94ccfc9c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a2b3dd74-e5b0-43c4-bc9f-0a8a9cdcba79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "66480d5a-ff6e-4826-81ff-fb94ddaa8be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 74
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d8a23c61-293a-4c2c-afb6-20672349a2ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 35,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "60835766-70f2-4732-bf99-1096c46a5ebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0bab7330-a255-4951-a26c-79043c57cecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 99,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "395bf4d6-9342-4b3e-a15a-473e5d75dc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "912fa059-56f2-45f0-944e-358ad168ed65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 61,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d445b2e9-0ede-47ca-b399-f575b16a58fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 70,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4df16bf4-1491-4de7-afed-5e7c6b0e5bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ac6adc30-2080-4729-ae55-172df7e0650b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "12cb1b3c-8428-4298-a804-983c9166e21e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "476e7342-a7d1-45a9-badb-212069364a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ce8b7ece-a405-4885-80d7-fee318e14778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "843d2d87-528e-4c64-9c4b-8b91a9efa420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d80b36c0-6e79-4325-8bf4-5172fb330bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 220,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "aad35ce6-bcb1-43b2-8f47-411442dc3143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "02621554-9f2d-4f3e-822f-038eb4c2aa69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2211b9f7-de28-48a9-9cb4-de4c2357a6bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "39d9509c-7d4a-43d7-98d7-eb190f3d5022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8f60b199-9b44-4b93-bc30-b3a2d5a51101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e043a3e7-e3e9-406c-9926-ec3be2b0b45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 91,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0702f9b2-9fca-4ee9-9e95-e65ac4d77a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7c2f0769-1c63-420f-ab7e-d31ad780a6d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 104,
                "y": 56
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ed7ffbd9-ec99-4b63-af43-13465aa4b821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 117,
                "y": 56
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "67a24a8a-3571-4319-8f27-c340b1c0cf5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 202,
                "y": 56
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ab6a83af-bcbb-4448-a53a-3d86d315d78d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 126,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bd7ca9dc-9c08-4a8d-9ab9-6165bf6decfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 56
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "49950259-0249-4823-8af4-3daf04bf34ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 144,
                "y": 56
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "038267bc-24b1-43a2-8290-53bc6bc3d37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 159,
                "y": 56
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c36f4768-5b71-40f0-bdba-ea99b203e121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 171,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ddcb2c1b-4bef-499e-abf1-66945d75df8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 56
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "56b288af-82db-47ec-b3e9-2e449c37e7db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 192,
                "y": 56
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2f80a6d8-5573-4152-9b26-7313233141e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 211,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "984f5270-bb27-4947-82d8-9c99ba470966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 105,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9d9539fc-5ec2-474e-b831-f1c7d903de43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6182e927-a028-4c02-a8df-1535caa8c08a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4644fffc-9276-4e20-815a-79f82ff76ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 43,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "94f3feba-0333-4601-9038-4749a1fc9108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 47,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "eacd7884-9452-4aa1-bc13-c26fa66787e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 55,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "16c720ba-d19c-47a5-abf7-e7b38f94552a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ab749b5a-c729-4379-8b08-594e65dce846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 75,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d502397e-f7f3-4845-836b-56b0cc883d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4f41bfab-7d21-4256-8fff-3b75571a69fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 92
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fe8e2a08-bd63-4a4b-973e-8836d0a9fd2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 110,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d47525b8-5514-4f9c-9903-bf910781e4d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 120,
                "y": 92
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7c46beec-a481-4d0b-a4e0-3a9a0d91e4d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 92
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f3e367e9-d5e4-48ad-89e4-a956f35e22b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 143,
                "y": 92
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0e1c02e4-3c7c-483a-9160-8edc1a79a383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 153,
                "y": 92
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0d636f78-f9ec-4161-8770-72c7e0d6c1e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 92
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e2c517e1-8b6c-4237-9134-f4afdd6a7b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 173,
                "y": 92
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ce04b147-39be-454c-a52e-ff86d3707a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 184,
                "y": 92
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a06e85d1-f5a3-4b69-861c-d1d613ee1229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 199,
                "y": 92
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "da0aa0c9-9099-40f6-9aed-5849752a1cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c3a21fe3-82c3-401a-a99f-61b848a2b268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 33,
                "y": 92
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "56b92cba-2b2b-45f7-8500-822af7042e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 27,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "86fcb2e4-db06-4326-beed-feecc97fbec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e3ef5ac4-a300-4018-bcdc-0e6a1f5f1b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b6fef32e-776e-4b46-ad5a-c028d76851b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 136,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f6d7de7b-0b20-451e-88bb-74d3ab092e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 144,
                "y": 74
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "227faa39-2314-44a1-ac6c-aa925ee354f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 155,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8d273a9f-ba37-4b19-a5fe-c07924bdedb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 160,
                "y": 74
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c23bb546-7ad4-407a-b9d6-55e9da1e2345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 169,
                "y": 74
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "71198994-50ad-4eb6-9c0c-bdfcae62cb7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "779add15-61f9-464f-8bf9-36812ab8ba67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 187,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "246580c7-05c5-4e5c-90b1-1e59fc7fc5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "91d54034-736b-401d-9596-d8e5bd9c4c6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 14,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4713c470-278d-4b73-be4f-49c4420ba677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 210,
                "y": 74
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bf3504ad-c4e6-405f-a3cd-545ffb567e66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 219,
                "y": 74
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0f4fc55e-2db0-4d70-839d-c8f2c0683215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 228,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "74e216ec-54dc-4cde-8837-427385690af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 232,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "431f0e3b-5bd6-4ee8-84d3-389dd3e1e721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 237,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "708c68cb-1df1-411b-abc5-bd81d3bfbe41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 246,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4d527a8e-7868-4bd7-9c89-1dd9e49e57df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "57b7524d-202e-4191-a579-149c99f509e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "57094781-bb3c-4974-a082-50d2bcd24aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 221,
                "y": 92
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "90b3aa1c-f144-46be-9fdb-e59956ee44b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2cb20e79-649f-4a33-a1d6-55f49607acb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 19,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9eaa67b4-6c89-46e7-9c87-0e4ba39fcced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a02d184d-a92c-49e0-9867-018a6846397b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "32b8ae21-669a-4888-81a3-aae14009b71a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d00e9259-e23c-4288-9838-10dd5f096d63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4a5a4686-0dd5-46f8-9741-4553028dd045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8042621c-150e-4419-bcb1-4da0d41274ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 11,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b9a91c77-aeaa-44b0-8402-dd9e53476692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 20
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "8f1f4d53-2e5b-4769-a833-7f8f23945bda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 20
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3a72093a-88d0-463b-a0b5-7b74ec9dc2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 20
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4a4e215e-ca2f-41b0-9cf8-145a3df38ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "682dd250-fe05-443b-a2a6-8005cb1b34f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 57,
                "y": 20
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "401f4be6-7199-44d2-b81a-351694cf3639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 61,
                "y": 20
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f95da244-1231-4619-8e76-0ccb6dc76700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 67,
                "y": 20
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "b5b71f46-d9d1-4ef2-8652-d0c190445024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 20
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "74814016-a5d3-4e1b-b566-7b1ba6ab6742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 20
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "6ae061cf-60dc-473c-9c7b-4b04b554bddb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "44d898f6-2a54-4f00-8de4-ed14c087b06b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "2b6cabf7-5a32-4d6c-9193-80b373b2a0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 20
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "5eec7aa3-b717-4a5b-868d-b950bccd04aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "0d72908f-b623-4e53-94db-c989ea69fb1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "fc137bdd-ed51-4924-807c-62a8adfe3121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "f0a7b927-050f-4c84-bbf7-8325350184dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "1c3b1e41-0087-4146-a0e7-fe1dc748d24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "a0ac5428-5d62-42e8-b345-243ac235469e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "c963d02f-3d69-49c0-83a2-1c58d4785930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "88d2719a-4017-4b2b-98da-739d02efe92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "a86da338-995b-4398-ba52-2228427b92aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "8810760d-054b-4467-9f06-1c484046961e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "58a1f5de-ee6a-4489-8a9e-e4359aa19c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "b63e24a8-017c-4d28-b352-529ff3d124f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "f9191f52-a927-4eb5-a913-7e12c38d1291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "5e9f3b03-bc8f-4134-b648-626bda58e651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "a8b8f5fc-88e0-4974-9f20-c3e63e79c9ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "71d1feb9-3481-43d1-99c2-879076a0b6ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "7c1fbe14-be01-401f-a9a5-6e6b8d590a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "f9939e77-f47a-4d14-94d3-22abeac4699b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "c5cc89c7-e6ff-4537-abc5-806449146095",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "6c1031b8-8351-4dd7-8068-e4bafbe7e365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 20
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "32ea5f62-c2e0-4732-9ecf-290c1b4f1fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 127,
                "y": 20
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "4df62341-e249-486f-889f-88bae099cc67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 139,
                "y": 20
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "882d4de7-e2b7-4357-b669-6d0c756594db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 103,
                "y": 38
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "b42380c4-4134-4a33-9029-3d35f32ca25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 38
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "7862790e-aea5-4441-bed7-87b0633a5f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 38
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "fb3c0279-31ca-4ae7-bc6d-e68966e5c75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 38
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "1ff75c9f-0cf6-4b02-a71e-957fd59a2f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 148,
                "y": 38
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "a73f8eaf-67b6-47ac-ba79-8cdad3e09b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 162,
                "y": 38
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "36ebfca5-e23a-4aea-a746-065f340c8d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 173,
                "y": 38
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "44be76d4-073e-4f8e-9b5e-5569f4893f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 182,
                "y": 38
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "50f23fba-3e63-4c30-9d6b-a2c2fe8ada21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 191,
                "y": 38
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "66ff8b8a-dd79-49f5-a36a-f5d590f18840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 200,
                "y": 38
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "08a5b687-ffdf-49e8-b7da-ba0fd40785d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 207,
                "y": 38
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "14527acf-c3ef-42d5-b82f-7ddf40179a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 217,
                "y": 38
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "60f2eeff-ec24-4604-8e2e-c0251a755c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 226,
                "y": 38
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "ef97d7e4-d942-4941-8d66-e4fc60bbf390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 238,
                "y": 38
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "a07236ce-45cc-40f3-8979-b5d0f88d85e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 246,
                "y": 38
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "2ada9077-49cd-4da0-aab1-3bb1cf219889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "e3ca16ec-9f8b-4422-8be6-dc8c41540440",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 56
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "cd96e6e0-248f-42a2-8d19-560d66bcbffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 94,
                "y": 38
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "40e45f8e-ed83-4f01-8a94-b6553025aa12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 38
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "d66c1c69-9f62-4e83-ab5d-98196054265e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "79c5f3a3-fc59-400e-a57d-df9781893063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 218,
                "y": 20
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "f5a98c43-227a-4096-9d29-f2c84cea76d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 152,
                "y": 20
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "bcf48db3-f01e-4bcb-9934-e12216ca99c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 161,
                "y": 20
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "9d249147-130c-4888-b90d-cfc8d7a47f54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 20
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "06388011-59b6-42e9-9ace-c3096f412cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 179,
                "y": 20
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "0e07af1e-0176-4f89-a764-d6c9aaa82879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 187,
                "y": 20
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "ee963c3b-3a11-461c-9cd1-973e8eacf2cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 196,
                "y": 20
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "85190690-05d2-4e92-b8b5-ed91c18009dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 209,
                "y": 20
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "e74e25fe-56ad-4689-b057-f32727f2cd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 227,
                "y": 20
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "db592bc7-1f66-4d01-8346-4eee4e319cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "8296a835-e0d6-4937-bc6c-ea638e2f3152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 20
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "04900c08-a189-47e4-8bf5-cc3e6bd8f67b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "fdba3846-3cd2-4a86-b506-0cf325eef184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 15,
                "y": 38
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "2a130c5e-7d05-4629-9107-3c4a8342f05e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 38
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "ac33bb5e-1692-4b47-8175-e01c331f6bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 36,
                "y": 38
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "e4920406-b7e7-4c19-8c25-da63954059be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 38
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "9e8918b3-cc56-412e-8194-e262442a7f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 54,
                "y": 38
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "c19836a4-9c7d-454c-88fd-96e9f18f6fa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 28,
                "y": 56
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "69029dca-5bf6-4581-bed0-35688d21c0b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 92
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "ca6ff4b9-563e-4a33-991f-77c35ab6d8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "5d81a8a6-3fb1-4ca1-8199-2553292a0843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "6c3f7dda-8c8c-4958-9b41-eed1173ed6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "94275f04-d664-457a-a3ea-47e78b6e19fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "87a7ac74-6897-4b0a-a6ae-4c62ccb238e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "c6d2167d-95ce-471b-acda-f355cea35617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "4969788e-57ac-4d21-af82-54102504df0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "0b484daf-e023-4bf9-a6c9-f1b5723e7230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "8e292df4-75c0-49e0-a8a6-8f92ccd63f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "364aabaf-b175-4c5f-8556-7350e5ea8aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "ba5ab5e0-526b-4f42-befc-52555eb5bdad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "4572d89b-c53b-4077-a985-102153e0a748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "4a5f80a8-1287-42e3-9fb7-bc72681e164d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "916f0f67-cae1-490b-a7c4-1ad502ec7bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "6e327e26-1c49-48e4-a617-97f987a217ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "a214596e-2627-4437-8632-2c11aa924a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "a906f0e1-62c8-40bb-bb49-5f98e6638b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "8402627f-fabb-4082-9e80-df2f5aadc544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "9ad0c6f4-a8fc-4c0d-be9c-1d649c898374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "1c8c3c6d-3767-4a8d-a4ec-43ab34c275e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f5686bad-ea8b-4a96-93a5-c16e294b39f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "65aec862-bfff-4c2d-954f-6cb529d38970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "5ac56c86-0cfc-4a12-8f8f-80bebcddb156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "bc8e8a9b-37b0-49b5-8e10-48e800dd5549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "0e5d67fc-05ef-4cf3-9a8a-580c2cd44efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "2e2a4185-2b0a-4b35-a358-dfba9af8d0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "ff15a90a-06ab-45bc-a9b0-171dd6e5f4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "cf83ccc4-b277-46d8-9a97-b1abd1f0fa7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "bde38937-2ca9-4833-ba1f-cc92d1221a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "a9c64eae-f6ed-48ee-85dc-ab55e12e7521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "c7d525a5-a673-4c7b-ab72-4dc479916dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "426634d1-9d5d-410d-bb00-43bbc961c7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "56f9b6d4-e7db-4bf2-85a1-e18cca9d7cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "0109c3aa-98ac-4e19-96e0-1ea60f6efe3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "b3ecab3c-c223-4ff9-8632-cb0202dc5b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "e57c2858-2015-42f4-8289-daca0abcdc34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "8c100699-2f4a-4616-90d1-402fe0431745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "71c2eac8-442b-4c60-9e6d-31dc03079f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "cfa5d68e-8932-4486-9535-9b12dd6587d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "e7140e56-7fe5-42ef-8baf-dcdc725ecffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "2dd03e47-2a2f-4d90-bff8-13ca2151b055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "706ce387-85af-4a85-9c95-323c1230db05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "e42d332b-981b-4c42-90b5-67826d5a6014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "71267d27-7828-4e6c-ae3e-c13d05619f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1058
        },
        {
            "id": "d4120b59-775c-4ac4-a9ed-f81a88243819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1063
        },
        {
            "id": "676d84f7-5cf6-40f4-a155-8b6a3dd9b5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 44
        },
        {
            "id": "fc05da09-9da8-4223-a67b-b5097db26314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 46
        },
        {
            "id": "d2663770-1fad-4b88-80d2-e0c60f50637c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 44
        },
        {
            "id": "a7279891-7dff-4855-9fa6-5a4ee7d61608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 46
        },
        {
            "id": "1c0e2799-f93a-461b-9647-f5c59176a175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1076
        },
        {
            "id": "207a36f8-ee8d-4399-9a61-68289a938637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 44
        },
        {
            "id": "d8561e9a-d9ce-4a87-b213-554e55ba827d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 46
        },
        {
            "id": "45595b7a-c341-4766-a59b-611dd5e741ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1086
        },
        {
            "id": "1fd7eeae-f3ac-4065-8d49-75ef32919db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 44
        },
        {
            "id": "06ab204a-d5d7-4074-a8fa-372a6a6ca77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 46
        },
        {
            "id": "82d01a56-3422-4998-884a-f7bb713ce3d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1076
        },
        {
            "id": "c015a2c9-f7a0-4ff2-92e3-008be53af1a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1058
        },
        {
            "id": "dcfecc09-9533-4602-aedb-67bd684343f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1063
        },
        {
            "id": "4ed8d642-1c50-4d0f-bafa-31cbbc3c864b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 8217
        },
        {
            "id": "f76400a6-f7e2-4d89-8ba7-f48c95c4e1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 44
        },
        {
            "id": "b4dabe0e-30e7-4c30-bb5b-f2f8f1a30dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 46
        },
        {
            "id": "048e0968-84ec-46ac-8a6b-589b88ca2b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1090,
            "second": 44
        },
        {
            "id": "f19652a8-4c73-4f2c-8b73-e1366de700cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1090,
            "second": 46
        },
        {
            "id": "bb39799c-ab46-424b-a110-180d4d4439b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 44
        },
        {
            "id": "eb265523-6e4c-4c39-8468-63d56a649ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 46
        },
        {
            "id": "f98ae681-0311-4d9e-b058-95dce8f5ae2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1100,
            "second": 1090
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}