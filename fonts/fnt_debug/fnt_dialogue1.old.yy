{
    "id": "c917f5c9-f477-4a40-9a9a-8cb4f326b348",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "78a4501a-412e-4bb6-b421-757be97260da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 59,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7d00e2e4-f4a3-40b4-8ea4-58b3a0c3cb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 59,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 164,
                "y": 307
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "12e06f7d-5fb4-493a-b857-3716a93907fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 59,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 171,
                "y": 307
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "af303a4a-97a0-4b72-88e5-932d99e98782",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 59,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 186,
                "y": 307
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0c5a2847-bca0-4479-81b6-d8758a1c935a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 59,
                "offset": 2,
                "shift": 30,
                "w": 24,
                "x": 224,
                "y": 307
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "39cbbcf3-7cb8-4225-af59-432bd109ec29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 59,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 250,
                "y": 307
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d968e993-8963-4f0c-802f-8ff954fa3962",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 59,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 283,
                "y": 307
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a7903bb2-c0f0-4419-b309-08ac74cfd2a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 59,
                "offset": 5,
                "shift": 17,
                "w": 5,
                "x": 311,
                "y": 307
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "56f8283f-4e7f-4895-87b0-ba6d178f8e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 59,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 341,
                "y": 307
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9a9832f3-5ec6-4aa0-aab3-72053fc321c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 59,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 481,
                "y": 307
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a23c358c-ffb9-47fa-a481-809f5d01205c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 59,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 356,
                "y": 307
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "265dbb1d-62e1-4708-bd1b-386409b4ce55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 59,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 379,
                "y": 307
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9986accb-8214-431c-b57f-7e073e05242d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 59,
                "offset": 4,
                "shift": 12,
                "w": 7,
                "x": 400,
                "y": 307
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4c23f3e5-27e5-401b-b41b-db939ead99f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 59,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 409,
                "y": 307
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "250b5634-26e1-4d89-b7fb-95d06624dcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 59,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 425,
                "y": 307
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a7a8b61f-8cc7-4507-ab00-2136c623757e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 433,
                "y": 307
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "29462865-e42e-4fd9-807d-2abee00163c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 455,
                "y": 307
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5a245601-161d-4dba-b16d-e259bf5e069b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 59,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 148,
                "y": 307
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "93186d2c-476c-4946-900d-88163aa20f6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 318,
                "y": 307
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e70386bc-f4c1-4a64-957b-f85079c90ccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 126,
                "y": 307
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7f6af549-9e8e-445f-a53c-cfe9c3718af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 349,
                "y": 246
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fa1b55d8-752e-464d-ad91-e7d35ac91675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 206,
                "y": 246
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f89b0c94-a56c-45c2-ab11-9991cb6e39ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 231,
                "y": 246
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c5df965f-68cc-4173-afe0-231b8641a9b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 255,
                "y": 246
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2cce8b3a-8d48-42e0-8257-b24e2aa21c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 282,
                "y": 246
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "437faebb-9a9b-48d0-8306-a294b6d455a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 306,
                "y": 246
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "fd0f1516-f9d0-4bdf-a7f2-8c465af83baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 59,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 331,
                "y": 246
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "385d7c16-9933-4dd2-89f7-915721ad5259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 59,
                "offset": 1,
                "shift": 13,
                "w": 8,
                "x": 339,
                "y": 246
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4e8ddf33-4a76-4f88-ae47-a3eb715de75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 59,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 375,
                "y": 246
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a0f9f140-8282-4967-9b19-78ad232b87a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 81,
                "y": 307
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "cb721ee6-be4a-4533-a23a-f4eeb28bf730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 59,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 391,
                "y": 246
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b4e787ec-7328-4a7a-81d7-8adc601e463a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 408,
                "y": 246
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "58326078-65be-4eb0-9da4-80876644729f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 59,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 430,
                "y": 246
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "77032cc1-2cf5-4d18-bf7f-7523d1277228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 59,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 467,
                "y": 246
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6d040fc5-4b05-4db9-beee-1e799da27044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 59,
                "offset": 4,
                "shift": 27,
                "w": 22,
                "x": 2,
                "y": 307
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6c9fede7-10aa-4c88-862e-8423c786dfee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 26,
                "y": 307
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a80c6e2f-d979-4f2b-974d-029558659e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 59,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 53,
                "y": 307
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "25623a19-d8ad-4c53-a8de-149d92987d51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 100,
                "y": 307
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bf460ad1-02de-4447-ac35-4bf0cd85023e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 23,
                "x": 2,
                "y": 368
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "664a5565-8c1f-4655-a49e-f5d256eed025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 27,
                "y": 368
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7aa2cc6c-4e56-4410-97fd-dfa2b5f291da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 59,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 57,
                "y": 368
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e2df921b-4219-406a-8c9a-76c1ca931dde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 429
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f1fbf85b-bb3b-46c2-b180-a895c02ea98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 26,
                "y": 429
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0afb012d-b239-4877-baed-ba49bac279ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 59,
                "offset": 4,
                "shift": 26,
                "w": 23,
                "x": 55,
                "y": 429
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f4666260-e8e8-4d01-a670-cb911d3f70d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 59,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 80,
                "y": 429
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ebf76b38-b085-48ee-8a4c-4764328d291d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 59,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 103,
                "y": 429
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "90770abd-2c5a-4376-bc8a-66e1ba43a700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 140,
                "y": 429
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e606ff9c-30e1-4b58-be10-0c3a3c769e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 173,
                "y": 429
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "263d8f0d-3380-4abc-9b87-eae0b3976668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 206,
                "y": 429
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bddf2e88-10e0-414a-af5c-adb3b297a523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 228,
                "y": 429
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "04c92fbc-b2d3-4123-958d-ad8f0f1ef663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 266,
                "y": 429
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "271feebb-02dd-4651-8e9b-c4146541ef63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 59,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 292,
                "y": 429
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4d146cc9-220f-4407-b597-02ad9b88bb69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 59,
                "offset": 2,
                "shift": 29,
                "w": 29,
                "x": 320,
                "y": 429
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8661ee81-4e88-47b2-bd4a-eb1187927746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 59,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 351,
                "y": 429
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "76bcddbf-19d8-45ce-9f4d-ec5f352223e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 59,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 380,
                "y": 429
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "5047a495-4836-4153-b229-7ad5711999c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 59,
                "offset": 2,
                "shift": 45,
                "w": 43,
                "x": 407,
                "y": 429
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fa000035-3192-43fb-9752-0f7c927956e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 59,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 452,
                "y": 429
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6243e69a-a9ba-46fd-bbb9-880329f98996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 483,
                "y": 429
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c678933c-a5e5-4773-993e-d62e175c4141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 59,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 468,
                "y": 368
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b1d3e6df-c907-49a7-8c4e-af02ad9f06e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 59,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 454,
                "y": 368
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fcee237e-3d4c-416f-9200-ec0483d4cbb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 59,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 433,
                "y": 368
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "27edc057-6b57-4fa8-a763-d5f8d4b5df7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 59,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 241,
                "y": 368
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "df341dc4-73da-4659-99fd-372f0d6d047c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 59,
                "offset": 4,
                "shift": 25,
                "w": 18,
                "x": 87,
                "y": 368
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "607bcc2d-eec8-42f2-ad21-614e2f6e9510",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 59,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 107,
                "y": 368
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f5ea5c4c-072d-42c3-8e95-bc0093ea3f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 59,
                "offset": 3,
                "shift": 24,
                "w": 10,
                "x": 138,
                "y": 368
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "64d62ab1-b762-429c-ba1a-3ac499b83dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 150,
                "y": 368
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2780bf1a-e528-4fa2-a20f-fc562e41941c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 173,
                "y": 368
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "45fd18e5-c2e5-4d84-9bcb-5b39710cf70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 196,
                "y": 368
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c279e05b-9f11-4a78-97e6-8339a03261e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 59,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 217,
                "y": 368
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ea420837-cc02-450b-be58-8955e5fb5a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 255,
                "y": 368
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c0d20818-836d-4d4f-a2b9-d29310f90a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 412,
                "y": 368
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "95ffbda4-99eb-444f-9dd6-e15a27386b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 279,
                "y": 368
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "79699a6e-c497-487c-a3b2-217d1724553f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 59,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 302,
                "y": 368
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8a8e83f9-6a30-4dda-a2a8-ab5a63e8ae6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 59,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 324,
                "y": 368
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "42073355-ef30-4bb5-8d57-94b858b7ceeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 59,
                "offset": -1,
                "shift": 17,
                "w": 15,
                "x": 333,
                "y": 368
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2c371355-4965-4270-acc2-8a771aa97522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 59,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 350,
                "y": 368
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "147717a9-3913-4c18-a949-033309af6f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 59,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 372,
                "y": 368
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f3980e39-0c48-455e-ae85-b6840ef1ffc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 59,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 380,
                "y": 368
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2120999d-d131-41a0-9742-2b7690bbfc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 184,
                "y": 246
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2eaf65c2-ac16-4748-ab86-ed0687ffbc6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 490
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "099f16a3-bb8d-471c-88df-e53b455bfa47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 162,
                "y": 246
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b7d6a5f9-92eb-4805-8262-e33a6b489458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 119,
                "y": 246
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a8839a74-d3ab-47ae-8c03-39fb908b6f6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 59,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 125,
                "y": 63
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f7426fad-84a5-4040-8445-0a1e22cc8d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 59,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 145,
                "y": 63
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "afca3d81-5967-4413-91a4-41d3194a243d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 59,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 167,
                "y": 63
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5be0aca4-1780-4906-be67-a558d14b3995",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 188,
                "y": 63
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "540637cc-9ff6-4e65-8fcc-04138e833d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 59,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 209,
                "y": 63
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "31c8f38a-6bb6-4c31-9ca2-425726d169ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 231,
                "y": 63
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0de3600c-740b-49c2-94cc-10c3df92463a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 59,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 261,
                "y": 63
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5259ca30-c496-4f5a-b377-68bd506a0625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 59,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 311,
                "y": 63
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6dd6bd40-615b-4cdc-ba20-af52b06bd3ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 479,
                "y": 63
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7ad51af7-6e94-4bd4-baa9-2b7aa22531c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 59,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 336,
                "y": 63
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "370adfa2-30d7-4bd7-8e18-8531b1bd8ab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 59,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 353,
                "y": 63
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "89820110-6f4c-4dd6-8e89-2e440fd808c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 59,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 360,
                "y": 63
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c25cd398-e5db-41d8-bf95-c5eeb93a1c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 377,
                "y": 63
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "093cd254-2983-4fdf-acf6-149977337bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 401,
                "y": 63
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "476d583e-50fa-4467-a4ff-b2b55acd36a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 59,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 427,
                "y": 63
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "33d5547c-74c8-4e35-8057-26d03adc7fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 456,
                "y": 63
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "ad61d718-ce72-4ed7-bd44-d6c982c51000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 59,
                "offset": 4,
                "shift": 27,
                "w": 22,
                "x": 101,
                "y": 63
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "6a477af0-b839-4f27-9602-ddebe63a8741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 59,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 287,
                "y": 63
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "7e75f710-cb95-4483-8189-f539c3d027eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 59,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 68,
                "y": 63
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "3f2a0c62-b257-4854-9d83-bde45e8397d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "2446843e-1b7c-46fe-bb15-8ac9fb3957cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "97a0f461-d36c-4c9c-a337-273fd0244a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "0c682fca-ba5e-472b-a303-d6fbbd0889c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 59,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "946cda9a-e2ef-4d7c-87bb-b01c3caa7842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 59,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "328bdee4-1e55-4b41-8fae-1300ec56e5bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 59,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "8046c339-9690-406d-b346-5d3a616b366f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 59,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "daad5efa-e24f-477a-94d8-6a820a44c565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 59,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "4024be3d-4367-4d88-b49e-1b836d0431bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 59,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 260,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "f800d513-76ee-4043-9681-7f07b2eaa689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 2,
                "y": 63
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "61cf7c4d-b1c5-4a22-bb43-358129b7a300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 59,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "8d5519ed-69a0-4dac-98e9-097120e38872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "b49a3990-ee1a-4711-8070-6f2ba0584663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "e1fb1089-075e-41f8-b0fd-1ea11ac24b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 59,
                "offset": 2,
                "shift": 29,
                "w": 29,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "6551530f-fbe1-493c-aedb-033e5a17aeff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "553bcc42-5ddb-4ac3-ad67-86478302efeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 59,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "f2a59044-e44d-4aad-9af5-bd0b780b730c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 59,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "bfdd923f-97db-4f65-a7e3-887c53193e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 59,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 35,
                "y": 63
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "47c5e1ca-2a50-42de-aebb-02faf569c22a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 2,
                "y": 124
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "d1b488e9-8d47-4e8e-afe6-ed11cf1e9586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 59,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 26,
                "y": 124
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "9d9b0924-4a24-4deb-8698-0d625dfd14f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 59,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 63,
                "y": 124
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "15780b8c-0d40-4735-a3d2-cdb8f4a59af7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 59,
                "offset": 0,
                "shift": 33,
                "w": 31,
                "x": 145,
                "y": 185
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "f1e5cb67-19b3-4e00-bd0a-6120228cf5f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 59,
                "offset": 3,
                "shift": 46,
                "w": 42,
                "x": 178,
                "y": 185
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "62cb92aa-8a0d-4f95-89c6-668cc8a2e5c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 222,
                "y": 185
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "9d3761e5-9ff9-4849-b1b4-192ad91de8d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 59,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 245,
                "y": 185
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "4b2f35ce-25a9-4acc-8f2c-ce9c6a87c9e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 59,
                "offset": 1,
                "shift": 49,
                "w": 46,
                "x": 272,
                "y": 185
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "df33b38e-b038-4ce0-b9e7-c823e155ca51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 320,
                "y": 185
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "5ffca6ad-4b01-49ae-bd40-e14ee6dd0ab6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 347,
                "y": 185
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "c47d6e06-b7b2-4c3f-992d-f365aaa98fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 370,
                "y": 185
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "14f26ed7-7489-44c6-87f8-b514b1af3c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 393,
                "y": 185
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "76638daa-7c70-4ab9-872c-82bc00ae317c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 59,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 414,
                "y": 185
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "9ca1f5fb-d4f0-487e-9bc6-e9408b4ea18f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 59,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 434,
                "y": 185
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "14c080a2-ea55-4e10-ba8f-c6aafffd2ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 461,
                "y": 185
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "02eb1309-7ddb-4aa3-b998-78c72ebb452c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 246
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "8399d6b7-f48c-4182-ac3b-fad6f221617b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 59,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 31,
                "y": 246
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "9d84fc17-0156-4cf3-9a15-261c22b28ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 48,
                "y": 246
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "a4a8150b-f055-4504-bdbc-9d17d7effd18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 73,
                "y": 246
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "2e09867a-a349-409a-9000-8861df1c8463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 59,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 98,
                "y": 246
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "1403deee-c758-4ff5-bad5-1122803b7161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 59,
                "offset": 0,
                "shift": 28,
                "w": 25,
                "x": 118,
                "y": 185
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "a4d2d613-a821-4ed7-a24c-eee35e1f50c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 59,
                "offset": 0,
                "shift": 32,
                "w": 31,
                "x": 85,
                "y": 185
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "7f684cd6-38ca-42ee-a442-7e2f1154d4ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 62,
                "y": 185
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "83c5b96c-6e80-40a8-bdfb-777e431549aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 59,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 277,
                "y": 124
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "308ee0ec-7a65-403a-b556-9b7cbfdcb330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 59,
                "offset": 4,
                "shift": 28,
                "w": 21,
                "x": 104,
                "y": 124
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "e071ad5b-a85e-4381-8df6-736c06c1830a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 127,
                "y": 124
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "08484f20-1f9a-460b-8022-978671dd804f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 149,
                "y": 124
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "ebd50fc1-b640-4b53-933e-8dccde675bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 59,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 170,
                "y": 124
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "5c9509d8-4914-49bd-be0e-75dbadaf9ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 59,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 191,
                "y": 124
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "26562e28-3148-48e3-834b-87408ae0e305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 59,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 216,
                "y": 124
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "1b07e779-746d-4129-8d48-ee13102f8ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 59,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 251,
                "y": 124
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "84799acd-e53d-4451-ac70-0ee0e052e048",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 59,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 303,
                "y": 124
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "84e61571-a3cc-42b0-a0d4-b2c32494e1e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 40,
                "y": 185
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "2f60385c-7bd3-4c34-b5c9-9ad4dcfeffb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 59,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 328,
                "y": 124
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "9858431d-c9b5-4170-a300-73065412d616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 32,
                "x": 359,
                "y": 124
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "858a28f5-2110-4e82-988a-efe2130b0e95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 393,
                "y": 124
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "ba2021df-1b2b-495c-9ae7-316fa3426db9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 421,
                "y": 124
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "d893b4e9-dd30-461c-905a-7b031ebac1f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 59,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 454,
                "y": 124
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "16321ef4-a19f-4576-84c2-199fec2251fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 474,
                "y": 124
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "ce2e7a14-2b30-4b30-ac89-f8c88a79afc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 59,
                "offset": 1,
                "shift": 39,
                "w": 36,
                "x": 2,
                "y": 185
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "365c1e31-b863-4dcd-a1b4-d2ed3c1f9376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 59,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 140,
                "y": 246
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "c120a7d8-0ed3-442e-80aa-f2714a9013e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 24,
                "y": 490
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 32,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}