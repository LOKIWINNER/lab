{
    "id": "f03c8d02-a7a2-43e3-83b0-b4b8126b9b61",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_debug",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "29fed1ec-7e8e-4a60-a2a9-757dec234cbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0d11399d-1b03-4df5-aed6-868bfd6c0e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 32,
                "y": 190
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b69ed7c4-507a-4a1a-9438-117300095483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 39,
                "y": 190
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "759e6ae7-7cc6-4bb7-a016-a2dd91f1eae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 51,
                "y": 190
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "88fccdd6-9fe9-4fec-be1c-241d49aa06ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 80,
                "y": 190
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a2c50107-ea2e-4b29-915c-e71eac70967a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 101,
                "y": 190
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ec9bc964-28c7-454d-86e9-2434fadeeea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 127,
                "y": 190
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ff729f05-cf29-4f44-ba9d-addcd9b206f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 148,
                "y": 190
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8d86e029-01c0-4495-b0a9-be1b5a85cda0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 172,
                "y": 190
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c28b52eb-346f-4eac-8378-ae0d3a2ea6bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 284,
                "y": 190
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "85537166-a531-4905-96a9-94e955894a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 184,
                "y": 190
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b8261dc4-08dd-4b2b-95a4-b73e74f013d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 202,
                "y": 190
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8c5fefbd-61fe-401d-adfb-20e05542a06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 219,
                "y": 190
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b1aad87a-f5f0-45d3-8f8f-0097c5d22d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 226,
                "y": 190
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0800c9b7-9586-4237-99c3-84017064e9ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 239,
                "y": 190
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2cf3cd4a-7669-450d-b3f9-562ad4e9e659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 246,
                "y": 190
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d2ae5d3d-a340-44a3-a2e8-e5bbe386fec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 263,
                "y": 190
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a59546d2-edae-4447-96c1-0f988df4105a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 19,
                "y": 190
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d40605c9-7958-4378-943c-a86797f0455c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 154,
                "y": 190
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "196c0afc-c720-4097-8986-bc39ba249952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 2,
                "y": 190
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9019f42e-c239-4483-bf58-816b215ee05a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 291,
                "y": 143
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "72477d2e-482f-4375-a056-40abb369f93a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 178,
                "y": 143
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "502597fa-ea3c-40b8-803e-81486e242f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 198,
                "y": 143
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0e0ade3f-109c-47fe-bb6a-7a5318da3cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 217,
                "y": 143
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "10d4327b-80a4-46f3-9f3b-e9b776e13d6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 237,
                "y": 143
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4f2642cb-719b-41dd-99dd-874bbd8c73d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 256,
                "y": 143
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1875b26c-400d-4f8c-af8c-3f5a39bcbb4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 276,
                "y": 143
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "663d1b0e-44cd-4c28-beaf-96306ca1cd46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 283,
                "y": 143
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "de836834-7164-4cc1-8491-e686b091f06e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 312,
                "y": 143
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "80e2ec54-8a0f-4c72-b5a8-f4d12ca0daed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 468,
                "y": 143
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fb06f807-6783-4772-875e-d197717afa77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 324,
                "y": 143
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "904910c2-5f39-46f6-a58b-5152a8146de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 338,
                "y": 143
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "89012c5f-1846-47f2-b8de-844b398b3c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 356,
                "y": 143
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0d803682-f469-4bfc-af5b-c1ae6310b59a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 385,
                "y": 143
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "91b45566-b1de-4058-8d3f-ced7f3da0818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 407,
                "y": 143
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "de12400b-226d-4ead-af9d-2bc9752d1b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 426,
                "y": 143
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "56d4bab8-2f23-41b2-adaf-4eac2c1417f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 446,
                "y": 143
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "186139ea-ac4d-4536-9b22-8c129bb46676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 483,
                "y": 143
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "71ad6b0e-5fee-4627-9ca6-9c70404b4801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 296,
                "y": 190
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c8f83561-5dbd-45f5-85eb-f1b1805d7dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 315,
                "y": 190
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bd35b4e8-2a72-45ba-8762-9e4384aa39c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 338,
                "y": 190
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "01776806-9c63-4155-8310-aafcad2a71bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 173,
                "y": 237
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1c76e0cc-8276-43be-ba7b-24b206b2d0b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 191,
                "y": 237
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b897a4ed-f59c-45d8-98c2-916397a93bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 213,
                "y": 237
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c85f8dba-94dc-447c-be45-54758efc4543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 232,
                "y": 237
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "01e66e95-82d2-4fa4-8b80-16bfac150ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 251,
                "y": 237
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "714d05c6-348b-44f9-910c-6eda39fa815a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 280,
                "y": 237
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3fac1fe0-652c-4a7b-9774-dcb6f6a1adac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 306,
                "y": 237
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "945cef7a-cd0c-442e-a346-2655cf35a402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 332,
                "y": 237
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3a2fba5b-d7fb-4780-b6b3-5b6c7bc6bcd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 349,
                "y": 237
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b4acca41-14dd-436f-bdac-2182728c247c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 378,
                "y": 237
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "156c9340-9124-4cb0-9e2b-75711ed57517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 399,
                "y": 237
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0934ca30-e015-4f15-b18d-82efcfab6601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 420,
                "y": 237
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e743e4c5-0846-4c02-8078-c01a43c82e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 444,
                "y": 237
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8bc55a5c-7087-4d62-abaa-3c0256fbbea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 466,
                "y": 237
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b9d5be8e-5cae-4cd4-955d-352a41b9bce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 2,
                "y": 284
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ad3c2aa8-5a53-4ea9-9241-d0bab09d98fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 35,
                "y": 284
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f77d50c1-b626-4d7e-809a-d399e9495143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 58,
                "y": 284
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2a9a0099-54f3-4ef0-a96a-fd2cc4b00bc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 150,
                "y": 237
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5e5e7c7d-3b0c-429b-bc2e-2e034fe8d057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 139,
                "y": 237
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f9498b66-ba9a-4ffc-936c-bdb966fc6c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 123,
                "y": 237
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "62f49240-67ce-4cfa-af56-f9c23ada9f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 482,
                "y": 190
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "16cfe6fb-3f84-4b6d-9ac7-2121c5484285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 361,
                "y": 190
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c0a35307-82ed-43b4-8212-0c4a1872e8f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 376,
                "y": 190
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4304ba01-d67a-4b77-8319-8b4d4b9b6a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 8,
                "x": 400,
                "y": 190
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "724e28e2-deae-4e77-b6e9-59308d24e27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 410,
                "y": 190
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9cc9312d-5717-435c-bce6-9b85a967684a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 428,
                "y": 190
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9bc38cdd-8f06-4678-ba7d-34aba48e8749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 446,
                "y": 190
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0ce76e56-b3c3-4ada-a9b7-8c8e3cc046ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 463,
                "y": 190
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "188f3683-e5f3-4d25-8f55-9668053b8f08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 493,
                "y": 190
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "7a75ae82-0ada-4ef9-ac0d-480ca4b72c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 107,
                "y": 237
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ced8e495-aa10-445c-889f-eaa42f8e785c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 237
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "fc37a82b-3fd7-4e80-920a-79297316252a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 20,
                "y": 237
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "55fd40c5-1b70-4776-bbd7-1f7e4bda0b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 37,
                "y": 237
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5eae788f-aeed-4384-88e9-7fbdbcc3fbd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": -1,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 237
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4c46585e-cf72-4da3-9530-5a5d764da479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 58,
                "y": 237
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d0b4c61e-4dda-49d3-b992-c7c9c3c03d14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 75,
                "y": 237
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dfe9fb11-8f13-4773-b5cc-0f0a80393c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 82,
                "y": 237
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "62a15f8c-f737-4ffb-b0b6-8d1917f8d390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 161,
                "y": 143
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9476b324-4f1b-417d-a4e8-715e20a2c7ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 80,
                "y": 284
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "29484832-8615-4235-9972-2e553045a39a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 144,
                "y": 143
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f0eedd92-e67d-4499-8a78-b57858c896b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 110,
                "y": 143
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a6a6bbe7-5961-47db-ae16-c82ff4006672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "44d33b35-3f36-4505-aec4-4f3e8283ad0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9b9bf0e1-7771-4cd7-b964-1fc7b647aa3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "684270c3-3c81-4a6d-9c16-c15d6207209a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 18,
                "y": 49
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ce0c5b36-dfa4-47c8-aecf-a436906f3252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 35,
                "y": 49
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d559653c-f51c-4b91-a600-7c109af432bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 53,
                "y": 49
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cff4858d-bb58-4e14-83c4-9e611ece14f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 76,
                "y": 49
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "23f218d5-193b-41d7-83bc-d74376592d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 115,
                "y": 49
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1149c02f-119f-477e-bf06-dd63c4f176ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 244,
                "y": 49
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "84c971ac-96ca-4325-b07e-92dc0487b4a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 134,
                "y": 49
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4b940dfe-aedf-4ec3-bf6b-948338cc3794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 5,
                "shift": 13,
                "w": 4,
                "x": 147,
                "y": 49
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c70ab534-6361-4005-b002-4d822aada0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 153,
                "y": 49
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "00b86681-3d35-4e56-85ac-e98c80612f40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 166,
                "y": 49
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "29bd8f9b-a102-4a85-8a6c-af50abbcb90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 185,
                "y": 49
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "78f3bf2a-6c7b-45c8-a1a6-10072b067efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 204,
                "y": 49
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "ea34147a-facd-4136-90a8-6e21c935fc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 226,
                "y": 49
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "801f8aaf-8002-4c01-ace6-f84a70c43d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "5243e102-ae70-4bd2-8f30-075193a5476d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 96,
                "y": 49
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "d44a12bc-97f8-4524-984a-0f2a29dfbb77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "ee05374c-2d0a-4544-a171-a041d5766700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "7c2862d7-27db-413b-9529-7f00e9f464cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 45,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "3adac01c-450e-4cbf-85b9-cf815bd63a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "ddabed38-ec22-4116-ac56-f65a058b6a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "0e4c3d7b-d6a6-4111-a4fe-06a609ed3ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "9d45fde9-1a9f-49fb-b312-491b004c275b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "4c9e4d8b-a6fb-4e66-86ae-b0da48abd3d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 45,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "71ee3c40-80e3-47a9-b728-0d66999a30aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "d9dad7f0-b827-4512-bfa2-2e1a2f40b73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "ec77bc70-78ae-4f14-9735-39d8fa7b9358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "ff21d63f-ad36-485d-98f6-ca9073c46394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 45,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "553217fd-e09b-4eee-b04c-33ee24d2d6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "b300cb67-9da5-4e99-ad68-5e9fd8c9714b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 265,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "6225ef39-a469-462e-b956-48f712bbd11e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "701102dd-0801-45ee-8005-9e3dc0d651f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 309,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "859da272-bdbe-451d-897f-ad1c93d18c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "961a4dbe-3f3b-459f-bde9-9a61e33cb96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 352,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "8d29f215-10b6-4c26-b7d2-47b78a3ec882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 401,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "2b4b404f-289d-4f92-a871-095ae5e83caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 262,
                "y": 49
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "422cf73a-9206-423a-a5e8-ef45b2c23953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 45,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 281,
                "y": 49
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "97b095b6-21cc-4a7d-9b9b-35d5aa0c944b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 45,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 310,
                "y": 49
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "d486e071-39f1-466d-8e54-041264ac1fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 45,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 261,
                "y": 96
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "887c1761-071f-4391-a5d0-da6642137542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 45,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 286,
                "y": 96
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "7240fabf-06b3-4b97-9df5-a0e7b1238526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 319,
                "y": 96
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "f784e6a2-fce9-463b-9543-cb01a292e21a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 337,
                "y": 96
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "c193f06e-9d3a-4c2b-87e3-c57fa7746ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 45,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 358,
                "y": 96
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "867f13cc-6c0a-4531-b912-9c21f4d8d9bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 394,
                "y": 96
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "f7194b35-c2b0-42e3-8d31-b904669aa7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 415,
                "y": 96
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "ae6e59d0-cc73-4f89-ab98-0bd6b32d2b93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 433,
                "y": 96
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "8d3d3893-2e45-4519-bd13-f64cd2f849a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 45,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 451,
                "y": 96
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "be5ae104-2fc2-4ff6-a0bf-00db9319efbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 45,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 466,
                "y": 96
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "04bb646a-fd2a-4ca5-9c0a-da9609a8692d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 481,
                "y": 96
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "e2f17fbb-def3-4fff-b2e3-176865c52d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 143
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "e358256f-8777-4ee2-9578-0bb19f0c7cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 20,
                "y": 143
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "47923db6-6b91-479d-8e9c-2dc3f3091ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 45,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 42,
                "y": 143
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "656edc89-02ef-41c7-b451-5c3ccf8ee80d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 56,
                "y": 143
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "87745478-2e7b-4fc5-be01-1c2f87f49aeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 75,
                "y": 143
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "10de32b4-da9b-4cf8-ade4-b4cd7f504171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 45,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 94,
                "y": 143
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "41a126dd-a705-4733-af71-a7aa8d7f9f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 45,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 240,
                "y": 96
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "315f045b-05fb-439b-92d8-8cf97fbc19c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 45,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 215,
                "y": 96
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "85496393-9eb3-45f7-b1b5-1940c910b33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 197,
                "y": 96
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "823d4059-7c03-4982-a002-2819a312fb0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 475,
                "y": 49
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "06d27253-665d-4e68-9771-83d66680fac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 45,
                "offset": 3,
                "shift": 21,
                "w": 16,
                "x": 341,
                "y": 49
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "83e21574-f275-4e9d-9aff-a5d217d6bc43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 359,
                "y": 49
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "221ebd4e-332a-448e-9ab3-87919be3b6c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 376,
                "y": 49
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "64f4ebaa-5fee-4b1a-ace4-7afba69861b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 45,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 393,
                "y": 49
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "295e3d64-2788-49b2-bd62-b592a6d80605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 45,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 409,
                "y": 49
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "c50de92c-3f63-4cac-85f8-0dcaaf39bb86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 428,
                "y": 49
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "89bf0145-3b87-42dc-a5f1-97704957f1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 455,
                "y": 49
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "2aa4208b-aa1e-4f80-8908-40ad70eac5fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "347fcea9-19c7-4721-8298-24f8b3ad4a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 180,
                "y": 96
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "9f0de4f0-8cac-4683-9b71-d6b9be035556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 21,
                "y": 96
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "abbfb3e2-36a8-4893-898f-fc2a3783b2c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 25,
                "x": 46,
                "y": 96
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "7fd66d03-b3a8-4cf2-82b5-5eacfab629e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 73,
                "y": 96
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "02175df3-060a-41e0-88fa-fa3a52c35c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 95,
                "y": 96
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "350bf70f-453a-4310-a4bc-e6293931b5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 45,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 120,
                "y": 96
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "16ab44a9-868a-4dfb-8ad2-e28a1e214c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 135,
                "y": 96
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "5141cbd4-1ba2-43aa-b13b-26a526147321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 45,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 151,
                "y": 96
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "a0fc9a0e-9c25-4769-b20e-299373c77dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 127,
                "y": 143
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "6d88cdce-33eb-4013-9d0e-222d5086e932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 97,
                "y": 284
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}