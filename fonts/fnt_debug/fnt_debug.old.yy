{
    "id": "f03c8d02-a7a2-43e3-83b0-b4b8126b9b61",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_debug",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "66ac3731-2872-4dde-9389-b7dae2d9307b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 59,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dca792d7-95b3-4b63-a8af-58bfd5f414a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 59,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 164,
                "y": 307
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b07a1dc7-8ef2-4a04-81c8-1ce5addec781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 59,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 171,
                "y": 307
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5a28c61b-f604-424f-975c-b1305279ff73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 59,
                "offset": 0,
                "shift": 36,
                "w": 36,
                "x": 186,
                "y": 307
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d2636aa6-1526-4f3c-a372-f2aeaf658cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 59,
                "offset": 2,
                "shift": 30,
                "w": 24,
                "x": 224,
                "y": 307
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "260ed21a-549a-4b9b-8328-72177565bbeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 59,
                "offset": 3,
                "shift": 35,
                "w": 31,
                "x": 250,
                "y": 307
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4b7a36db-9cfd-48b1-980b-2ab5388551db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 59,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 283,
                "y": 307
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6af61543-ed3c-4181-8ebb-f378c025d9de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 59,
                "offset": 5,
                "shift": 17,
                "w": 5,
                "x": 311,
                "y": 307
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f8647396-e9a5-4aaf-bcf2-0be07b43687a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 59,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 341,
                "y": 307
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7b652855-6086-4863-a795-2458eeef41a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 59,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 481,
                "y": 307
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "aae4d355-a3b1-4020-8ec6-574726baf816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 59,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 356,
                "y": 307
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6514d360-f70c-4114-8493-b942c30c2527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 59,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 379,
                "y": 307
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "27e55ca3-abca-4916-b087-cf02954f85b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 59,
                "offset": 4,
                "shift": 12,
                "w": 7,
                "x": 400,
                "y": 307
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f2cc2e91-05a1-46b9-8a99-89b333191432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 59,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 409,
                "y": 307
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9f6d453a-dbfc-4d19-ad88-26d0354ffa92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 59,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 425,
                "y": 307
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "559c37c3-27ea-4ef4-9aed-38aa3a128116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 433,
                "y": 307
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9af88ca3-1ba7-4a6a-9d85-d5ee1b6ccc05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 455,
                "y": 307
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7b165901-a3cf-41ff-8ec0-e4a7c9c82447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 59,
                "offset": 3,
                "shift": 19,
                "w": 14,
                "x": 148,
                "y": 307
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "21997e8e-0b19-4834-8eb0-76d52672980c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 318,
                "y": 307
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "db02e543-47cb-43d4-bee2-6a4a18c72e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 20,
                "x": 126,
                "y": 307
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9593d860-12f6-43a0-bcf4-55a1f0f9f87e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 349,
                "y": 246
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2d687b5e-494c-48ea-8cec-141734c51067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 206,
                "y": 246
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "392b4cdb-092c-4721-94f0-f02fb3314e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 231,
                "y": 246
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b2d78e3d-5cae-468d-8218-3e399ead3958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 255,
                "y": 246
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "30e3b058-ec4a-4e2b-b8d8-28e14c05fc74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 282,
                "y": 246
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "71f75264-f11a-47a5-a27a-7e9ff36fc269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 306,
                "y": 246
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3d72074b-5f9b-4cc2-a0a7-c727666b2622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 59,
                "offset": 3,
                "shift": 13,
                "w": 6,
                "x": 331,
                "y": 246
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "10b31aeb-6ca7-43fd-8833-5212f7933493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 59,
                "offset": 1,
                "shift": 13,
                "w": 8,
                "x": 339,
                "y": 246
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "62f0ad07-7e9c-4ef7-8720-bbdefbd994ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 59,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 375,
                "y": 246
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2ef20506-18da-4cc1-8e9b-5ef98ba40bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 81,
                "y": 307
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1a732dea-1812-4858-8f6d-d6bd68dfcd79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 59,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 391,
                "y": 246
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1f840029-1dbe-4210-bbef-4c7509bec414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 408,
                "y": 246
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "370e59df-df23-4041-aa65-930064ca77f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 59,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 430,
                "y": 246
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "af9c4d25-e0a8-4247-88d9-70f091e53989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 59,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 467,
                "y": 246
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "64b9f19d-549d-48a5-94a8-98fb06c36621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 59,
                "offset": 4,
                "shift": 27,
                "w": 22,
                "x": 2,
                "y": 307
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5442b193-8bc0-486b-a062-871eba075eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 26,
                "y": 307
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dd8feaf9-0d22-4e47-b68b-7b16e29aa3ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 59,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 53,
                "y": 307
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4c5c7caf-1f7f-428c-8f1b-4605d549b256",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 100,
                "y": 307
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ff916b5c-0de3-40cf-8a4a-3cb458cbb9e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 23,
                "x": 2,
                "y": 368
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9939d237-3456-46aa-b147-95879b74e503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 27,
                "y": 368
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e3d4ca9f-f12d-45f9-b0a6-eced3b636bd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 59,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 57,
                "y": 368
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "df72664c-3d3f-4498-9d80-eea7d0e938ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 429
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "46b22176-00ae-42a2-8f05-2e459e343240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 26,
                "y": 429
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2650518d-d40e-4832-8eca-a56148513f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 59,
                "offset": 4,
                "shift": 26,
                "w": 23,
                "x": 55,
                "y": 429
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "16a56132-1305-4a1a-8c46-7c2f81376e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 59,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 80,
                "y": 429
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d94f6d56-4bc9-4899-9393-6bef5afe0a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 59,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 103,
                "y": 429
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3d155a02-99f8-473f-8fb3-41908779628e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 140,
                "y": 429
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "eb940236-c07e-405f-bcb3-5a705a242f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 173,
                "y": 429
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c507241d-dc4f-427c-92e1-c2dd4aee010e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 206,
                "y": 429
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "376fb231-c4a0-4b93-8399-711fb0f2d0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 59,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 228,
                "y": 429
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "149dc05d-3f1e-46d0-afba-6f46c27db3ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 266,
                "y": 429
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ca691af2-18d6-4c34-9b0c-98ad8b090656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 59,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 292,
                "y": 429
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e905f333-d60a-4e03-9b3f-4058d13a67c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 59,
                "offset": 2,
                "shift": 29,
                "w": 29,
                "x": 320,
                "y": 429
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4f921d3c-9b0c-4cd1-9f63-bfe3f3b382f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 59,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 351,
                "y": 429
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "70368d9b-df8c-4adf-b2ff-3a61290b573c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 59,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 380,
                "y": 429
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9cfe6c28-4bad-4c85-be1e-bebb259a1408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 59,
                "offset": 2,
                "shift": 45,
                "w": 43,
                "x": 407,
                "y": 429
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "43e3b381-c096-4d73-8cac-9f8400f78a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 59,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 452,
                "y": 429
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e3db7d96-ed58-4961-9ed7-9779779cfa9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 483,
                "y": 429
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4b32a097-1e07-41c2-88c1-c80491772f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 59,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 468,
                "y": 368
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6fdabc78-be2e-4a0a-9bcf-84fd26db3fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 59,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 454,
                "y": 368
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "16be5997-1797-4f32-a45a-f939c4f523e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 59,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 433,
                "y": 368
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f7f48a80-4fa0-4ac3-a502-8fa12017db4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 59,
                "offset": 3,
                "shift": 16,
                "w": 12,
                "x": 241,
                "y": 368
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3383ee08-664b-429e-9db0-2cd0dcacbb76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 59,
                "offset": 4,
                "shift": 25,
                "w": 18,
                "x": 87,
                "y": 368
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c43f6361-ba22-4a13-bb0b-f7128ba95376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 59,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 107,
                "y": 368
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f30a53f0-5143-4a5b-92ea-2ed5e68a9210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 59,
                "offset": 3,
                "shift": 24,
                "w": 10,
                "x": 138,
                "y": 368
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b1ed69f0-27a5-4b42-b052-62f6b98075a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 150,
                "y": 368
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dc946268-a1e3-43a9-bd96-71d92b9064dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 173,
                "y": 368
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a5e0f945-50fa-4b6b-b250-b9dfded53b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 196,
                "y": 368
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ba9aeb7d-ac5c-4f5b-b91e-5b0296c80ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 59,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 217,
                "y": 368
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9ec45ea8-866b-4386-abaa-d711678d1d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 255,
                "y": 368
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f7c39d53-bead-4867-b9f0-7d38d7b491b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 412,
                "y": 368
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d84a1c97-4ed5-452f-90c8-046a7117c080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 279,
                "y": 368
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "aaae46b6-9be1-4769-9e91-350493adbd6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 59,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 302,
                "y": 368
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4423b064-60a6-40d4-8647-f3e7ce942d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 59,
                "offset": 3,
                "shift": 12,
                "w": 7,
                "x": 324,
                "y": 368
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3a48e8d4-a6b3-4ce8-9559-5acefe1e34cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 59,
                "offset": -1,
                "shift": 17,
                "w": 15,
                "x": 333,
                "y": 368
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1dfedcd1-d28e-4195-b420-6168c1e1cb4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 59,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 350,
                "y": 368
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f22f9ece-9ec5-410b-8107-a79efeb79f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 59,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 372,
                "y": 368
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd7ae7ac-43eb-49fd-97c9-d6fc10c586b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 59,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 380,
                "y": 368
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9be89340-ca0c-4e7b-930a-91d32426cf80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 184,
                "y": 246
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "982f8b3c-ce0b-4f2f-85de-a242d2a1e85d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 2,
                "y": 490
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4b62f354-c6ce-46c1-8514-68c14482b3bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 162,
                "y": 246
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8d1c0a85-37d4-4cc1-b3fa-2ffbe2fb3238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 119,
                "y": 246
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a62b9b17-4759-46af-a5e6-2359e8281a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 59,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 125,
                "y": 63
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c48359e4-13e7-41fa-af3d-58c96c110732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 59,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 145,
                "y": 63
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7c1cbf33-71e5-4e67-8ea5-4f27c1c956fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 59,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 167,
                "y": 63
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "99b93b89-4d9b-4394-a7ce-9fc95a69e47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 188,
                "y": 63
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "650e8c45-7c2f-4c12-a29e-189981995579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 59,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 209,
                "y": 63
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "51933634-a639-4aed-a029-e6985adf1f02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 231,
                "y": 63
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cf86bd2d-bd9b-4dc0-8aec-c8662060091e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 59,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 261,
                "y": 63
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d4b0353e-e2ae-49b6-b699-75357d2814d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 59,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 311,
                "y": 63
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c0268662-33d9-4ef0-8dc9-c797de4d8474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 479,
                "y": 63
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a3635302-3660-45ff-a7e1-9ffbbe83d8e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 59,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 336,
                "y": 63
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8af09aee-78e2-451f-9cad-3a77385111a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 59,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 353,
                "y": 63
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2824615e-b7c5-4067-b17d-417bca1716d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 59,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 360,
                "y": 63
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f757efee-65e9-44b7-9706-7ca126abc2ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 59,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 377,
                "y": 63
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "989d3af1-c696-488c-b12d-8ac43468998d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 401,
                "y": 63
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "c2916455-a27a-4a2a-8b0f-336e7dc1255b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 59,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 427,
                "y": 63
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "7889bf56-85be-4b45-82b9-ea34df235413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 456,
                "y": 63
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "eb8687c7-16c5-44e2-b9c1-65a11acb3122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 59,
                "offset": 4,
                "shift": 27,
                "w": 22,
                "x": 101,
                "y": 63
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "42435d8c-7926-4963-85be-fb3ff516afc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 59,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 287,
                "y": 63
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "33da4a94-adc4-41be-b6e9-a6210e9bb9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 59,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 68,
                "y": 63
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "84dad79e-b47c-41a9-9339-aff7b947d5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 24,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "9f4b7537-3dfc-4933-aa0b-5fc25249d7b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 59,
                "offset": 0,
                "shift": 37,
                "w": 37,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "0ad00296-0681-48cf-b759-7b8ae4dcb768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "f7a69343-144e-40b7-956f-ba93ec1a1b99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 59,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "5f9a0608-b049-4b78-a0f4-c4cbce039e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 59,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "e92a4194-e70b-4e3e-9397-97c4770ec9d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 59,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "3589bdbd-9200-4514-b02a-8e76e2289602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 59,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "34c471f1-5ba2-4920-bb7b-3d5015d488bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 59,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "6ffe7de6-dd6a-48d4-989c-8744fe5266f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 59,
                "offset": 3,
                "shift": 33,
                "w": 28,
                "x": 260,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "96f1700c-8df4-44de-b19d-d9ac79c04691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 2,
                "y": 63
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "00c5b53c-59a5-4a7b-b9f6-b3f1c1912210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 59,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 290,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "7e6dee18-99cb-4c43-b66c-259a8c5945a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "bd3401e4-8a35-4f09-b270-5fc89d647bf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 59,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "e8351723-c7a3-4f2c-b3fd-0b45251b6b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 59,
                "offset": 2,
                "shift": 29,
                "w": 29,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "f50fbfb7-a2d9-4e32-89f4-4e70c33a2913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "d69d93f2-5398-4c30-9a15-77e8fd316eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 59,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "f9c35db4-8b28-45d2-a101-174793ca9d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 59,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 463,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "776f19aa-28e5-4b88-a020-3639343d6416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 59,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 35,
                "y": 63
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "b7308ae7-ce6b-4c6e-baae-2ebe53120589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 2,
                "y": 124
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "49512e75-da48-46c9-b1f3-002cce411561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 59,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 26,
                "y": 124
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "26a8e2ad-d142-4f22-a7b0-35114b870530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 59,
                "offset": 2,
                "shift": 41,
                "w": 39,
                "x": 63,
                "y": 124
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "8e800816-227d-401b-b651-2fda8236d5e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 59,
                "offset": 0,
                "shift": 33,
                "w": 31,
                "x": 145,
                "y": 185
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "bb5b5bd6-1365-42de-8959-6044e13a87b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 59,
                "offset": 3,
                "shift": 46,
                "w": 42,
                "x": 178,
                "y": 185
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "4c111591-8ef2-4601-a615-a5b404f7e316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 59,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 222,
                "y": 185
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "b51b6d15-4aca-490a-9714-bf4ada094c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 59,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 245,
                "y": 185
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "2e1042bb-9a4c-4463-aa7c-b31a8404187b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 59,
                "offset": 1,
                "shift": 49,
                "w": 46,
                "x": 272,
                "y": 185
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "7dfe868c-d6de-4e46-ab10-48425050f18f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 320,
                "y": 185
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "f2654f7a-719e-480a-b957-d1f00b27d541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 59,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 347,
                "y": 185
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "12bd8049-fd3b-4522-b47a-8565a50f6c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 370,
                "y": 185
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "26f9c6ae-a27b-4f0f-89ac-1aa380e8cc2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 393,
                "y": 185
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "d2e66a90-8d3b-449c-b67a-9a7235103bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 59,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 414,
                "y": 185
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "aa6f4bd8-f74e-4fe0-b669-e8bf370c934b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 59,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 434,
                "y": 185
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "e2d3d9cf-3025-4ad7-a405-33b2dd6c86e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 461,
                "y": 185
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "261f37f8-05e8-4b72-8473-423c95a2edce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 59,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 2,
                "y": 246
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "529b340a-8d3a-4727-82f1-15e8fc553775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 59,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 31,
                "y": 246
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "3b6a4399-319a-41db-84a4-2766128d4f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 48,
                "y": 246
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "fa63cca5-0752-43d9-8241-cf1d44c22ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 59,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 73,
                "y": 246
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "19173fbc-7476-49da-98b3-137298611ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 59,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 98,
                "y": 246
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "3f479559-2b9f-45da-afdc-704a07bed9ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 59,
                "offset": 0,
                "shift": 28,
                "w": 25,
                "x": 118,
                "y": 185
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "ae32e615-d443-4f89-9fba-11b0b2b07d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 59,
                "offset": 0,
                "shift": 32,
                "w": 31,
                "x": 85,
                "y": 185
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "13fa6e80-9ff2-464a-917e-74540a010cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 62,
                "y": 185
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "610a4d83-d792-448c-9c3f-6cb711e4e47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 59,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 277,
                "y": 124
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "568e1abb-3db1-4b63-bd81-8aed2dccd612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 59,
                "offset": 4,
                "shift": 28,
                "w": 21,
                "x": 104,
                "y": 124
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "46a07b10-c85b-487f-831b-6ba0ad617a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 59,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 127,
                "y": 124
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "2f3230b9-7e34-4d09-879c-93ab68aaa780",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 149,
                "y": 124
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "27157441-da8e-4642-b5c3-555d6fcd7761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 59,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 170,
                "y": 124
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "626e5d76-bdd3-483c-a765-de8cb5f3c62b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 59,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 191,
                "y": 124
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "bc8aab01-ce67-4d15-9857-5f9371e60207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 59,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 216,
                "y": 124
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "36c5e8ad-0b5d-445e-a729-65a0bf8f7af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 59,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 251,
                "y": 124
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "843a137b-e163-4f64-a9e1-21e8630427cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 59,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 303,
                "y": 124
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "7513da61-4046-49ad-a93a-c9631d0803b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 59,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 40,
                "y": 185
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "72a2e876-e533-4c62-84f7-f7d4e323b365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 59,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 328,
                "y": 124
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "c91f3500-4a49-4257-9d37-b92292e57594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 32,
                "x": 359,
                "y": 124
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "2cd75cfc-0d8d-44a7-9b46-fbc5f89a7dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 59,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 393,
                "y": 124
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "9624859f-ff68-4e4e-beda-c4478750f70a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 59,
                "offset": 2,
                "shift": 34,
                "w": 31,
                "x": 421,
                "y": 124
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "cf92890e-6e5e-461e-9925-6edc9849e98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 59,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 454,
                "y": 124
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "d5a87208-28cc-4c6e-8e38-96df3d46f511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 59,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 474,
                "y": 124
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "ea42857c-1b9b-4c17-84fc-efd92e5c51d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 59,
                "offset": 1,
                "shift": 39,
                "w": 36,
                "x": 2,
                "y": 185
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "8e5c3cfa-c8a5-4e56-a97c-fabbc49e1f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 59,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 140,
                "y": 246
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "9da7bb89-d153-4c93-a9e9-f82198f7cd56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 59,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 24,
                "y": 490
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 32,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}