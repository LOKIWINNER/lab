{
    "id": "8739b26d-ecdc-4073-8c56-06238dffd0f7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_GUI",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4bef4630-751d-423e-aa8e-13a4568278ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "33d74774-2dd4-4f8c-9ba5-36b45b5e9f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 88,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "578f91cf-b97a-449a-965b-62e039d0d844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 93,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "13820ae5-8e28-4a51-abc4-2ffeb760e5de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 101,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4af3bef3-50f0-4dea-8e94-247bd10ceef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 119,
                "y": 142
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4f2809aa-8a73-44e4-bec2-027f22a99c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 133,
                "y": 142
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6f9c3a75-b8a6-4b58-96b2-2cafccc82901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 149,
                "y": 142
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "823c5abd-7cd4-4c8c-a7f2-d2a0766c12a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 163,
                "y": 142
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8c0e0720-954d-4908-ae72-6be21a22f9d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 180,
                "y": 142
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c5a64a2c-a8dd-44cd-815e-c590fa59f3dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f220e274-11c2-4c10-a3b9-426f385bf41f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8495be36-5640-4eb8-b44a-e92b31f2ebe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 199,
                "y": 142
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "aa79fba2-0b69-486c-a0f0-0012f1485eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 210,
                "y": 142
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3bb9489f-6d62-4f16-b973-287020307022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 216,
                "y": 142
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7cf72cf5-09a4-4ed5-9e04-fbb73a868652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 225,
                "y": 142
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "25040041-d5fd-4147-b0cf-6378c61662f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 230,
                "y": 142
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "84aacbf9-c3cd-4cc6-bd3f-6143a15a5f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 242,
                "y": 142
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9a1a3754-2be7-47f5-b4fc-feb8bc46e425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 79,
                "y": 142
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3a896c52-3a99-4d4a-893b-0a0a5caf9a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 142
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "411e785b-4554-436c-be7a-509e3946b81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 67,
                "y": 142
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ed9ac0de-e19a-4b7e-9138-79315967296d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 180,
                "y": 114
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ef213997-ab74-4829-b2b6-bdc5c25585df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 107,
                "y": 114
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "43ead683-3bdb-4113-b976-1905cac6737f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 119,
                "y": 114
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8985ef1a-33fb-4858-ad31-419855ce5235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 131,
                "y": 114
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0ca38d81-84ac-490e-9c27-a9cce45a3938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 145,
                "y": 114
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "dbee1e1f-54dd-4362-849f-7b18b51684b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "893e0d42-f375-4dee-90e5-572c6c7d6a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 169,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "723360a8-f04b-4151-bab2-5e7604534c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 174,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aedd31a5-4112-4e20-9b56-0647f40199c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 194,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fa910f3e-bd4f-4b3c-ac83-fdc6da2e7e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 142
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e5d6ec33-e9de-4af5-abb0-d6775d0b3418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 202,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "442ac208-862a-41b4-a5cd-1e4fa5623447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c6412278-5f3f-44ef-ad96-694d5bf235fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 222,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "34184f4f-76ae-48c2-a1c7-c62cd2f921d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 114
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c5cc4297-5303-4592-9fc6-c60bbe84e7cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4aec0bf7-82a2-4b49-b503-266d08457e78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 15,
                "y": 142
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "30aa23eb-28b6-40d9-b5e1-25ccf4babef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 29,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "355aea06-b89a-4690-a8be-4949200963b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 142
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "914a6557-3432-4712-a591-ccc95f4e4388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 170
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f1882925-3c9d-4f11-84e2-076cec58a026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 23,
                "y": 170
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fa28e583-05e1-4a37-8f90-40d26b38c18c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 38,
                "y": 170
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "153183e4-767a-4d45-8e8f-72c4b51121ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 198
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "70fea699-ec3a-468a-8d66-83f987e53f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 37,
                "y": 198
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8f2132b8-b5ca-4c84-ba17-0ddada224ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 52,
                "y": 198
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a5a31989-dcf3-4a99-a524-f108df27b441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 65,
                "y": 198
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "58f573f6-5da8-4992-b3ec-25ad79371a54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 78,
                "y": 198
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2d0367e2-8f23-49a7-8cc1-b2540a0f8ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 96,
                "y": 198
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2d5caacc-d4cc-4a78-ae06-221bb0257926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 112,
                "y": 198
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f975a437-74c4-4ebb-9417-b3310eba4286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 198
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8e21235b-8625-4ca9-ab0b-ec0e4adb3055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 140,
                "y": 198
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "30f45fad-8f35-48e4-8ce4-1fe39ff129fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 159,
                "y": 198
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3e6af037-7731-4d10-9093-a592a059e93d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 172,
                "y": 198
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "738af953-59d7-4e69-bc32-11eebc46fa62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 186,
                "y": 198
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "13f3d030-09b2-4e09-b10d-0a9e565f7d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 201,
                "y": 198
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6b2c855d-aa6b-4278-80d6-2b9d764a90af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 215,
                "y": 198
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9602c692-c828-4d99-bbb7-f13d4d45ec6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 229,
                "y": 198
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "06c3831a-67e6-4e68-9bde-ca3cdafb2d12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 226
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9859bc72-3faf-450d-bf1a-646aa032c6e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 18,
                "y": 226
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "671d5768-7669-4aab-b560-3090306b9de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 10,
                "y": 198
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "89cb9249-84dd-48c3-a014-8eb5f23682cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 198
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "42e3eb6e-a6c2-4428-aea8-a70f3164d755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 238,
                "y": 170
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "fc754d0c-3971-463f-9234-14f16c722a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 135,
                "y": 170
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b572dc87-7adc-413b-908d-b9dd23ac4e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 170
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a4d87dd3-795a-48c3-8ac8-6a2deed9ae75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 64,
                "y": 170
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5ae35d92-cc54-4272-9dec-37b88a934dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 5,
                "x": 80,
                "y": 170
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d55e2254-c2e8-4af3-bf73-2e05e34f732c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 87,
                "y": 170
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ec8f4e3d-1a6d-4552-8d14-809981fe69d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 99,
                "y": 170
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ebb08084-8276-4324-a3a4-69b200a20f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 111,
                "y": 170
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e3a30b83-8c53-4973-bea1-b0e095c52dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 122,
                "y": 170
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e0e5992d-0de8-43f8-a6b6-aa8bfc789cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 143,
                "y": 170
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cb89fd8b-4bef-4814-bf1e-d171d466a62d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 227,
                "y": 170
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "dd77b511-16d3-4c91-87d0-0ea19d8915b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 170
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f13aa18b-f74a-421e-8a0a-aaf1c9117acd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 168,
                "y": 170
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1bad2ae1-8c0c-427a-bb8e-bf8e7790ee0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 179,
                "y": 170
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4dbd31a2-ec2d-4104-bdad-d2a5e6ba3f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 185,
                "y": 170
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6119a4e4-26f0-4972-97b4-e9aacc64f650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 195,
                "y": 170
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2db6ed74-66d3-49bf-9699-23c6a2f3bfbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 206,
                "y": 170
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "df0212f3-ffe5-4127-92d8-a27e8d2aead1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 211,
                "y": 170
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ee7138a5-d235-4258-83ab-06c9f00f39ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 96,
                "y": 114
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "acf0056b-42fb-49c9-a7e8-68bee53b0d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 226
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6e5fd456-6d13-4d71-ab25-dfd76fd03bcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 85,
                "y": 114
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ca5aa9e0-b2be-4bc1-9947-18baf2410a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 63,
                "y": 114
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6af8e335-d495-4bde-9786-00b021220c27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 63,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b62e313e-7af0-4c76-b093-12868b7822ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 73,
                "y": 30
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5659912f-e15b-44d2-9a8c-82433f4b6e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bbbb06ec-67d5-4098-aa0c-338a3ac0f867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 95,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "782285e8-e684-4b29-afde-ce99bfa00b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 106,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "52c4ca68-a784-4b8d-a4ee-7253494a6ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 117,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e8cc01d3-6220-4a18-8070-e9bba25a2306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 132,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7cb7c068-ff14-4cd4-b61c-dcab259776f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 158,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "fcbfc16a-7157-47b5-b164-bcbf6e754f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ae936a8f-4945-4bc1-9022-e4d5a8aa3157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 171,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7a301a10-ff4b-49d9-872a-9915faf3fc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 180,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5ff7fbee-68c1-4080-a321-d9636b20fcff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 184,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "defcdc25-ee95-4afc-9c4c-be98b59b4aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 193,
                "y": 30
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "04f50dae-451b-4418-a006-6ace0ede1abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 206,
                "y": 30
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "153854f1-ee32-42ee-89c3-63de44c00ffd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 219,
                "y": 30
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "bf0f4968-67b9-4bd9-ab12-391bed5f387c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 233,
                "y": 30
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "0e24d613-4222-4e39-b2b4-e4613011f3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 50,
                "y": 30
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "df1088b4-2ce4-4e1f-af1d-357e2d0a335f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 30
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "0f6aeb11-5dac-4c18-97df-134c8c2c02b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 30
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "0db7918e-a197-4d12-a359-2e5cf4809a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "3ef0b336-f951-47c4-b9ee-f58094fc4879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "538b386a-7dcd-4188-b830-efd37bd8bba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "d7513bab-296c-4bc6-9922-5050b771627f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "04968137-57a4-46da-bec8-1060d46f1cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "56300ffb-8254-4cff-8788-7d136244d876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "ebbae995-6414-4c5e-bb78-d6af43e22237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "4498b59a-a196-49e4-aff4-a9a25ada58e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "f88426f8-429a-40cd-a35a-eb93dfb8693d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "6bf37c17-f624-456f-b777-2ae7d12a749b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "55a8f6b7-d6ad-445a-8c72-2bb577aeda9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "a3e33d6f-b7cd-4e85-8f0f-742923d002c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "1c99e154-b9a4-4995-91f2-2755593c53e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "b70659de-670e-48aa-bace-6fdfb7accc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "2e6366d5-a027-478f-8e89-4be544d12670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "c09c0884-050a-43a8-9d35-c71643cf266c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "bebd7593-d2aa-4677-a748-bf20c1fa42c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "5f731a5d-9d65-4e68-a817-5dc4147411a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 30
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "5fab0037-9c20-42ac-a383-4d831d3884e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 13,
                "y": 58
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "0d162edd-4bd5-45f7-a9b9-62678b193086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "fa3fa49b-efb2-4993-a4a5-e2d37f4d2bd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 45,
                "y": 58
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "fb9a4152-029a-4384-8df1-7d4af69f2838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 84,
                "y": 86
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "03cc5fcd-0145-46f1-b370-b1ac7d747c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 100,
                "y": 86
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "3fd1b07a-64b8-457f-bbcf-17f385acc4e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 121,
                "y": 86
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "ecd4aa6b-a5de-4c6c-96eb-36d01ff1ed39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 133,
                "y": 86
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "f49ee245-0bf7-42b3-b7ec-9648b4ab5b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 26,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 147,
                "y": 86
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "c23a4d6c-f952-4a12-8011-e29457a83e45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 170,
                "y": 86
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "731ea45e-c454-4f92-8e91-4ee26631763d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 183,
                "y": 86
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "b4472e48-ee76-46d4-857d-9893cb154895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 86
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "b4cfef47-5cfc-4685-ac35-886445bcf9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 86
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "cf1e4fc5-7791-4aea-b5bd-8595ee6cac1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 217,
                "y": 86
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "1b6e1c65-91dd-492e-b919-d752728b08e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 227,
                "y": 86
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "88f16051-873e-4561-a1e6-e26c8b1edb00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 240,
                "y": 86
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "8dcd83e7-bed0-4eaf-bebb-f9988f3b59c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "fb5ae446-e065-4085-9562-24c057515e36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 114
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "73ac893a-70af-4f8e-a3da-93d1ead3aaec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 27,
                "y": 114
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "8fee18c0-c210-48f9-ba40-933415217415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 40,
                "y": 114
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "55a860e5-7835-42ce-ac62-fd710bc0f7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 53,
                "y": 114
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "71acd901-e8bf-47ab-a771-8b40b832f81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 71,
                "y": 86
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "eebb0eb2-aa9d-4e01-8701-00b5e0ceaf42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 55,
                "y": 86
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "e69cb4a7-a277-4234-b56c-9492d7ef1878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 43,
                "y": 86
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "c8c4bc3a-cb73-4fb1-986d-a0395d27fd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 154,
                "y": 58
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "435f8d33-13a2-490c-abc5-01aea87ec40b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 65,
                "y": 58
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "21bd9661-e769-4ea4-a865-b54e3ab4cdf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 78,
                "y": 58
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "49d491e4-d208-492a-9f6c-f797a78df57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 89,
                "y": 58
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "f64ef29c-32bf-4ee7-afa5-09c7d3cf9c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 100,
                "y": 58
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "016dd314-9957-4c1e-b899-9512ea380065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 111,
                "y": 58
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "3cd86546-6bdf-4c8f-98c4-23bae6f5f1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 124,
                "y": 58
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "cb3ceccd-caf0-4c31-aa3d-fa1246317099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 141,
                "y": 58
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "b1fcbf52-aa1b-45b9-86fc-2f3c2f951b62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 167,
                "y": 58
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "8aa0e94d-b80c-458f-8f75-421304abb8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 31,
                "y": 86
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "630eadf0-ec1d-4ad6-a12f-673f9f469e24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 180,
                "y": 58
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "f61b2642-cf48-418e-bdb2-17d5c126c288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 195,
                "y": 58
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "8339e7ef-d5ff-4d83-aece-3bdc337efcb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 211,
                "y": 58
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "5cc060ea-53aa-4248-a867-308cd841ebc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 225,
                "y": 58
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "6045bf26-9cc4-45c7-a617-afa222b32fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 241,
                "y": 58
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "70ca6b99-e730-4e0f-a8e2-861dda64ab8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "a2dca6a7-73d2-4ac8-89d5-ae26ef04fb86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 12,
                "y": 86
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "cb300c2d-c25f-4495-bc2c-b860f946f407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 74,
                "y": 114
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "25b81385-89eb-469e-8cb9-b59e510f3062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 44,
                "y": 226
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}