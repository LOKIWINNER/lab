{
    "id": "8739b26d-ecdc-4073-8c56-06238dffd0f7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_GUI",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "796de617-0e6c-4878-8d98-c0f552bab886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 56,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "038c571e-ae43-4821-acc4-6490f5724bff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 56,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 488,
                "y": 234
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "62900f27-8ed2-43c2-b14d-2da5fbfa7887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 56,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 495,
                "y": 234
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5fdb5f1c-ee59-43b3-b2dc-36a98699f897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 56,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 2,
                "y": 292
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "66ad82d9-ea2f-48d6-8d4c-15c4bd35a417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 56,
                "offset": 1,
                "shift": 28,
                "w": 23,
                "x": 38,
                "y": 292
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8d95e823-9a0d-44f1-8fb7-38e5a41afbf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 56,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 63,
                "y": 292
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8006d09e-0423-40dd-abc4-a31d786bdd0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 56,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 95,
                "y": 292
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9a180723-a160-405e-8fbc-da5f3a0034fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 56,
                "offset": 5,
                "shift": 16,
                "w": 5,
                "x": 121,
                "y": 292
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "13489e13-adb3-4e6a-b737-ef515a516cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 149,
                "y": 292
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "22f404ec-e7b8-4343-822c-86d47a2be7f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 56,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 281,
                "y": 292
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "68f73f6c-3aba-4d80-b503-185972edcd8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 163,
                "y": 292
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a4064fe5-d7f3-46d0-a226-ace8ef72e8e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 184,
                "y": 292
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8b13ac0c-d37f-4e42-b885-55a88deaba47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 7,
                "x": 204,
                "y": 292
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "32854046-5bb0-4b79-8b7e-a14d156408ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 56,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 213,
                "y": 292
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "63136cc1-21a9-40a8-afdf-42d486c30dd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 56,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 228,
                "y": 292
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "005ae684-317e-4c9c-83f6-b9c64468dc5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 236,
                "y": 292
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c43b9e74-28bc-4e1e-8a72-8c861b719a07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 256,
                "y": 292
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1f55176e-df56-434d-9057-9618360aa547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 56,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 473,
                "y": 234
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8bf4acd5-c4c0-4e60-ab77-03061b0d3bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 128,
                "y": 292
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5a7e47c2-d123-4612-8601-b4150bdb0c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 451,
                "y": 234
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6a069238-136f-4d56-a4e0-12d37466df64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 195,
                "y": 234
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "67ea98f7-3ec4-4632-bae9-af76ca327102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 62,
                "y": 234
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "59705175-1614-440c-a407-273a1abed3bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 85,
                "y": 234
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "397497de-bfcb-47ab-af69-d2530ba57337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 107,
                "y": 234
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "74a36dc7-763f-4513-96dc-55814a3aad56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 132,
                "y": 234
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b06d0ccd-84e6-472b-9eb6-8bf611928db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 154,
                "y": 234
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c13e922a-b9cb-4a2a-b1f0-452189ce01c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 56,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 177,
                "y": 234
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f5621832-817d-4162-8c8d-e701efdcaf4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 56,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 185,
                "y": 234
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4b8a8896-9719-4dc7-b1f4-8af450de2a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 56,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 221,
                "y": 234
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d0fbcd7c-1d14-4385-b764-c3c1186d3aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 408,
                "y": 234
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "78446681-0ef8-48e3-bc99-1d186a91669c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 56,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 236,
                "y": 234
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "326c9b41-a630-422a-b021-0a8dbbb97407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 252,
                "y": 234
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b966bbc3-46dc-4a7c-86fc-5b90e50cc993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 56,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 272,
                "y": 234
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a4d5a490-0ba1-43f7-88c4-6460644ced44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 56,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 307,
                "y": 234
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "01766558-fb5c-4ec7-9397-ae2b2ef987dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 56,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 334,
                "y": 234
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b979eca0-c21a-4cd2-a064-b4b19c9137d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 357,
                "y": 234
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8a783287-46ed-4c08-a3cf-ea3318fd4d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 56,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 382,
                "y": 234
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c21eab8c-1bfb-4d5c-bd64-2d3431aa3cbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 427,
                "y": 234
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ae33c9b0-637a-4186-9dde-deffa6cbfb52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 295,
                "y": 292
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "56278118-a822-4728-8b6d-e9a58a964ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 318,
                "y": 292
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1b9b25ed-c12e-4b34-b835-a7102e9a6004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 346,
                "y": 292
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "cb07a63d-b9c4-4eae-873c-5436657ddcff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 265,
                "y": 350
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6e043879-3e03-4fd8-b90b-7724615091ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 287,
                "y": 350
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "810bb107-ff7c-411c-bb81-360443034f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 56,
                "offset": 4,
                "shift": 24,
                "w": 21,
                "x": 314,
                "y": 350
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fd7d987e-b840-4e4c-9d6f-73bbc06e6097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 337,
                "y": 350
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a915d69c-469f-419a-b1e2-3651015f70b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 56,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 360,
                "y": 350
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8e0d4b43-1a25-4907-8793-07c467285920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 394,
                "y": 350
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "918bab4c-ad86-406d-a33c-62eb32e680ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 425,
                "y": 350
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a6946ef9-3f86-4d14-9c5b-c18091997ced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 456,
                "y": 350
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e3dba522-ce34-4275-b8ef-dfc6b705d7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 56,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 2,
                "y": 408
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "796c3e9d-1d60-468b-bdeb-f33a35eaab33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 38,
                "y": 408
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b1f8602e-8caa-437d-80c4-6eecb1865e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 56,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 62,
                "y": 408
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a77d19cf-7a41-45b6-8e1a-56c34ff5872d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 27,
                "x": 88,
                "y": 408
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c3588f0b-6437-48a6-bb1a-87264c6015b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 56,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 117,
                "y": 408
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5e0a3c76-f84a-4be4-9aae-4e4b181513cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 56,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 144,
                "y": 408
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a729eda4-5376-42ad-8834-8aae070fa891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 56,
                "offset": 2,
                "shift": 42,
                "w": 39,
                "x": 170,
                "y": 408
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "151a2442-dd02-4c31-b106-6a306b70f154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 56,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 211,
                "y": 408
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4bb5d00f-fe57-465b-81a3-76f4cf25c055",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 240,
                "y": 408
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "485d309c-79c4-40d4-9383-f416688adc67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 56,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 237,
                "y": 350
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c1fada9b-248c-43b5-967b-47f5eee99ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 56,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 224,
                "y": 350
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2fb47556-4a49-4786-8acf-d9f54fdcc4c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 56,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 205,
                "y": 350
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "91a2ccfc-76a3-4313-9a83-c60d3a3cea34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 56,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 24,
                "y": 350
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "066e291a-0227-4fb6-aa76-b33114cf0093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 56,
                "offset": 3,
                "shift": 23,
                "w": 17,
                "x": 375,
                "y": 292
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "88abbf57-7c9b-490d-9478-6db4f474c7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 56,
                "offset": -1,
                "shift": 25,
                "w": 27,
                "x": 394,
                "y": 292
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3b86055f-e95a-4b92-978a-f57e72a6f8d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 10,
                "x": 423,
                "y": 292
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "225414cb-ff7a-4703-a9fc-55300cfa265e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 435,
                "y": 292
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a9300b2d-12fd-4550-884b-11c84ea2e025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 456,
                "y": 292
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5cd59eaa-3855-4d4a-b99f-ee4c95c51697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 478,
                "y": 292
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d9940e04-a73a-4e8c-8525-fea7505b38cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 56,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 350
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4c2ef9a9-5b87-46af-baa8-12a2a7960fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 37,
                "y": 350
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b033b193-0cfc-4dd4-a417-4cbb4d9b8cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 185,
                "y": 350
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "25dc8843-8495-4181-aea0-229f0cc854ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 60,
                "y": 350
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "85893cc0-f4bb-48df-a85c-608f1fe9c7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 56,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 81,
                "y": 350
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b79d2ab0-1cba-4b72-94f8-d001faaaf680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 103,
                "y": 350
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0ed6176d-923e-448a-9e65-1f3b240bd17a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 56,
                "offset": -1,
                "shift": 16,
                "w": 14,
                "x": 111,
                "y": 350
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "57c51e61-10cf-4b34-a47f-d48759c346a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 56,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 127,
                "y": 350
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e4734b0e-885f-404c-b1cd-c8ffa91f0886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 148,
                "y": 350
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6b670273-fe00-4081-a9a4-2bc11c8c82cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 155,
                "y": 350
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cfe44399-afb0-4824-8994-fd07467c2ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 234
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d723897a-cc6b-45ad-bd46-e523a2b68030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 266,
                "y": 408
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5b09a19d-af2e-4b12-8c80-e2d818470184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 234
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "970eb093-3498-4997-a555-4fed4db02697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 488,
                "y": 176
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "43294416-cc4b-4dce-a3ef-ef70256fafb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 86,
                "y": 60
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8abc4682-f16c-44de-9432-66155e109785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 104,
                "y": 60
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "63ac9088-76bb-40cc-a075-6922c993728a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 124,
                "y": 60
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d336f69e-cf86-4efa-849c-16a6c5fe9f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 143,
                "y": 60
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2abc8c7c-7854-4b81-b756-4ba3f6a24b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 163,
                "y": 60
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "db764191-3483-488b-be33-f7d1c83bcd33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 183,
                "y": 60
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "aa1694f6-e1a2-4ba3-83c6-14814c688253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 211,
                "y": 60
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "83cab61a-716a-4e72-857f-824e8b62ada0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 56,
                "offset": -1,
                "shift": 21,
                "w": 21,
                "x": 258,
                "y": 60
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c27441c7-82bb-4fba-9961-217ff7c75ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 56,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 417,
                "y": 60
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "702563e2-36b2-4f80-90d1-d28877cbbadd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 56,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 281,
                "y": 60
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f8b951cd-5f9e-49fd-8e74-59cbbf518284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 56,
                "offset": 6,
                "shift": 17,
                "w": 5,
                "x": 297,
                "y": 60
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a82fc781-5f0f-4dcb-9a66-60042cadeadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 56,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 304,
                "y": 60
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2e20e1dc-53f7-48a3-8c5e-cd58757bc59a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 320,
                "y": 60
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "24b173a9-401a-42e7-90ce-fbe98d8ba205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 344,
                "y": 60
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "7af69ae6-6115-4457-ae51-e691437a593f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 56,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 368,
                "y": 60
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "66e3b8b4-6db9-47af-b16a-d393480185f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 395,
                "y": 60
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "2e1c304c-5ab3-4345-be0f-b8cd4bad75f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 56,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 63,
                "y": 60
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "3a02fd85-2f6c-4e0e-b972-74b1ae59cb19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 56,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 235,
                "y": 60
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "333d3b36-ad24-4254-8c66-97b79fc8bb21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 56,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 32,
                "y": 60
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "61179ff6-00d5-4e18-8756-6d9f545cba4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "e6036787-8770-4a53-ad91-95ca7072d889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 56,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "76b14e2c-1b76-4cc6-b646-b9e0808f9d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "c227d86b-f2cb-427a-9a96-6e4e1d79c56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 56,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "532fc75d-9432-4426-8885-061d52d28a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 56,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "e207009e-15fc-4b90-8e59-03b86c3e39de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 56,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "9c8df919-0ba5-4800-af93-c46493d7b486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 27,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "f8b8aa05-5bac-4d87-af54-8e3b37a0ec34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 56,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "cff713b6-a260-4ef4-8170-701a176c9c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "97972363-4f44-4400-99cf-52d77c07b38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "3c77a24f-ee01-492f-b4aa-39eef989cf16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 56,
                "offset": 2,
                "shift": 36,
                "w": 33,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "efb54342-c739-479c-848a-42c09487e9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "e5e76576-7b52-47d8-9ad2-456a36e22a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "06c54a86-9ec8-4094-b98b-2ca44f0534da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 56,
                "offset": 2,
                "shift": 27,
                "w": 27,
                "x": 351,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "a8680f5e-c224-431f-83c5-d5d3bb30dea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 380,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "47bf02c5-b542-420c-8772-0a2fe00b4d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 56,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "eefa94b9-d018-49b3-9ac8-d2450b747d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 56,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 432,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "065f47a6-c22f-45e5-8166-2f70be25e49c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "62d3e7a4-d240-4a93-b87f-c33b3d6652fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 56,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 438,
                "y": 60
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "22a484f1-c8b9-4639-a9a6-2ecfc13e0f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 56,
                "offset": 1,
                "shift": 36,
                "w": 33,
                "x": 460,
                "y": 60
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "d1362c7b-9570-48fc-b84e-0cda67f957f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 56,
                "offset": 1,
                "shift": 38,
                "w": 37,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "e1ae43d1-aa4a-4aa9-a98d-10558932a165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 59,
                "y": 176
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "5c3b47b9-8e4b-45a0-a172-c5a71422a6a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 56,
                "offset": 3,
                "shift": 43,
                "w": 39,
                "x": 90,
                "y": 176
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "02b6593b-61f0-4c31-81e3-8fc848cece1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 56,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 131,
                "y": 176
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "4c01c322-4f5f-4e95-aef6-3dc2a6d0eb72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 56,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 153,
                "y": 176
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "9aea9d79-695c-4ae0-b797-db0723e4bd81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 56,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 179,
                "y": 176
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "e8e4a8cf-59e4-4374-a076-9eb1a661d4d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 23,
                "x": 224,
                "y": 176
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "309f5001-6ad1-43e5-947b-76030d923b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 249,
                "y": 176
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "97f9fcb3-836a-4a04-ae65-e615d2546db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 270,
                "y": 176
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "dd49640f-005c-4fcc-aab5-9c18ad487287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 292,
                "y": 176
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "799ff18a-cf5e-4cae-8e8b-794fd02747fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 311,
                "y": 176
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "8577ef19-dd97-4996-be0e-336c99e7f160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 56,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 330,
                "y": 176
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "eb6ee704-a5bd-4423-b6a7-fc632c0ead0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 355,
                "y": 176
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "bb881987-1693-4e4d-a61c-a32f5c66dc4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 56,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 378,
                "y": 176
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "25e0fdb6-001a-44a2-99ec-a14f101154b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 56,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 405,
                "y": 176
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "215a0b62-9d04-4dd5-9443-8bca2a65ce3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 56,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 421,
                "y": 176
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "f4b6e3ae-01c1-4d7c-b39d-1dea166cacc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 56,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 445,
                "y": 176
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "d245dcc3-9c15-4cf9-9efb-182025d380f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 469,
                "y": 176
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "e5636027-912c-4350-9bf5-75f5af6e74dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 56,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 33,
                "y": 176
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "492182e0-a85f-44db-b8a2-4e73bb5bb58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 56,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 2,
                "y": 176
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "a9c10d62-9028-460e-9b6c-a8cd5e7d1b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 460,
                "y": 118
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "e59e8959-0a92-4310-96cd-6a0f4e2e0ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 201,
                "y": 118
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "9345a53b-c351-4700-8668-9e59484e1e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 56,
                "offset": 4,
                "shift": 26,
                "w": 20,
                "x": 41,
                "y": 118
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "7811d4c9-899d-46af-a7c9-b58d1992e8d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 63,
                "y": 118
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "f4181498-01a8-4b6c-b63d-9099810b73f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 83,
                "y": 118
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "e6c84c2f-071b-4573-b0f1-e118d6756762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 102,
                "y": 118
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "b8c5bd99-5d2a-458b-95b2-758414e3454b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 56,
                "offset": -1,
                "shift": 21,
                "w": 21,
                "x": 121,
                "y": 118
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "ba70621c-6be8-41d8-9127-0073c9a844d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 56,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 144,
                "y": 118
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "3a91e7c1-452a-465d-aa6f-9980d737f889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 177,
                "y": 118
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "9229d5ff-7fad-485c-8366-73ec404fd5c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 56,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 225,
                "y": 118
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "bb97afb5-c2b0-46d1-a21b-62c7aa6157f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 56,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 439,
                "y": 118
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "1c5905fe-70b8-4476-9d05-0763323a9c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 56,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 248,
                "y": 118
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "e932e99c-36e1-4021-8d75-8873fca3aa7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 30,
                "x": 277,
                "y": 118
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "19a0ebd7-7a9f-43d5-860e-24795961b5e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 309,
                "y": 118
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "6a694c4c-cd8b-4be9-8730-4d5ed22ebd61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 56,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 335,
                "y": 118
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "15a01fe3-d942-49c6-bf2c-a28df0acfa76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 56,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 366,
                "y": 118
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "23b52689-058a-42c6-abbb-f1b75e5e5769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 56,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 384,
                "y": 118
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "0d589bac-d6d2-4c4c-be34-9c2e858c87b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 56,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 403,
                "y": 118
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "f86de5a9-1ef0-484b-ba85-2d9af4a24324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 56,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 234
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "f4d9fdd6-d0c7-4618-91fe-8b632842a27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 56,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 286,
                "y": 408
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}