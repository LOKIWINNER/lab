{
    "id": "c917f5c9-f477-4a40-9a9a-8cb4f326b348",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "315f6dc6-050a-4cd0-8c81-3d127d29921e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0ee0aad3-8b14-40eb-b323-60cafcf63852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 275,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a387b502-3fd7-458d-9999-01e0745b6ed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 281,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cd7817fd-370a-4f1f-a553-1134460fe9d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 292,
                "y": 119
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a8766a10-ab4d-4ec9-a907-002b85c4aa91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 317,
                "y": 119
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "71631bbc-2100-4767-9b48-1a01d131bcf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 335,
                "y": 119
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2475a788-4ab9-4973-a568-42fb8953dd9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 358,
                "y": 119
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e0ce8d0d-a8b6-41c9-bb34-498a28822c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 377,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "79b98eb8-8d22-418d-9f5a-b75405415951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 398,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "049ffbbe-f2b2-4fd7-b85f-682f594e7613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 496,
                "y": 119
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3041080e-fe76-4cd8-93a4-69e788c75148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 409,
                "y": 119
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6cc02b6c-f5f1-448f-9158-047f37dbc74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 424,
                "y": 119
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f8ffa9c9-a56d-4eb3-a79d-bc4dabcb4fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 439,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "dfa5943d-4fee-45c3-88c9-46276dc47ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 446,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "900dd306-9dbf-4810-a429-92268130ba54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 457,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8ae3deb8-ccdf-4c0e-9b0a-9425d2696a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 464,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ea704bb6-fbc2-46af-baf1-7366927ba4ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 478,
                "y": 119
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d7519a76-ab4c-4f61-9f7b-29851a38e132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 264,
                "y": 119
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f2fdb82b-624a-4c9e-9040-ba1959f39d43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 383,
                "y": 119
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b016e270-9913-40cd-87a9-910bf183dfd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 248,
                "y": 119
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d0b68ec5-add2-4570-9b7b-08f79537b9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 66,
                "y": 119
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6d79c72d-ce4a-494e-bc32-40ed1e54f31e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 463,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3a255800-649b-46b4-9978-3b700c962a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 480,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "87db029c-fcfa-473b-bc42-c9456f0222dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ad531a29-e1a3-41c5-a90d-afcd640e7a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 119
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e899244a-9112-40cb-b409-6009c9601b8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 36,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "312dab25-2bff-4b0a-8388-5debdf530fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 53,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a6f2d22f-2cee-46d1-a6af-8cd78f006578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 59,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "83339a8e-2654-4a99-b129-db4ca043de8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 119
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2c4f60a4-c877-4514-838d-944817b190a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 218,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "00f2c950-dbf9-47f4-9f10-33fea9728b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 119
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bbaa5195-8b04-447d-8e85-617d061d9886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 107,
                "y": 119
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "872f4748-cbfb-4cbe-925e-368a10bdb6cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 122,
                "y": 119
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "921cc3b9-8bc4-4a33-869f-18976d56faa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 147,
                "y": 119
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bcd239dc-6883-430b-8610-21e9ce3055a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 166,
                "y": 119
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f42f56f9-77ae-4b7d-a51a-1e224e46c4a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 182,
                "y": 119
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "227358a5-32d1-4f21-bbca-9f60a4361e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 199,
                "y": 119
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "27ea9c14-33ff-4c5a-96ff-8f3acbcacc8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 231,
                "y": 119
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "820eca03-3d93-432f-9f35-d5fba6edd1e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "befd41a9-5a72-473b-a0c8-48df5525acb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 18,
                "y": 158
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f29b8f0b-4a5e-428e-8874-971eedac5e01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 37,
                "y": 158
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4b807d08-5efe-4775-9579-fab26acc822c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 340,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "68dfe1e1-660a-41c6-bd76-d8cd8da764e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 356,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "71967884-8a67-49bc-867c-30280966cb23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 375,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cfe3160f-bbe1-4166-926f-312e64531b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 392,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8621f2d4-d7d9-4d1c-b014-da2907c22b89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 408,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "6bea7676-2a23-4155-af87-b9fe87d32c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 432,
                "y": 158
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "eba6185f-fa2a-4efd-a5e5-8cf4a8631e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 454,
                "y": 158
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "761154fe-2e53-419b-8613-e1f7163a3d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 476,
                "y": 158
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8eff9782-7b5e-4f7c-a049-9af528cccdf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 2,
                "y": 197
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bedeb080-7e1b-48a2-8c75-633fcc4c0164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 27,
                "y": 197
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f2638586-4a72-4fd7-b167-a80f2ef48de3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 45,
                "y": 197
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "25e94779-f924-408c-8d54-5299caad7b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 19,
                "x": 64,
                "y": 197
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d93d4ba7-4652-4c51-b48a-0555052fe039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 85,
                "y": 197
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f737dd6f-fe78-4919-9f2b-beab3b9a2ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 104,
                "y": 197
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3a6da77c-0092-4af5-acd8-c4cb80da15b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 123,
                "y": 197
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f65c088c-8ccb-4247-9a0c-9f2aa1d50469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 152,
                "y": 197
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9c59582b-a12f-46ad-956b-15d0b501bb91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 173,
                "y": 197
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "17c80d50-72d7-4852-a869-fb392659e694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 319,
                "y": 158
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "78d65bc8-9c34-42a7-9f45-315e933b4334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 309,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "28d75631-1ba6-4564-b117-546ac8cccea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 295,
                "y": 158
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "094c2bfc-2e85-42c0-a44f-951726849e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 162,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cf951c02-f8fe-4661-b8f1-37b18da3b338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 57,
                "y": 158
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0fe983ce-95c5-48b0-9cf5-45242d200e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 71,
                "y": 158
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9fef5d22-8af3-4d20-84ce-69e3e9ef983b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 7,
                "x": 92,
                "y": 158
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "12c2c100-f802-47fd-a7b9-4e6921a236ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 101,
                "y": 158
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ba79e6eb-76ea-40b2-b8c4-01202ecd0c8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 117,
                "y": 158
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f3198f08-6f29-40a1-8fc2-8d6cfdebfa42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 132,
                "y": 158
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a663573b-907b-4772-ae87-554db8a69eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 158
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "54685432-6bd6-4a5b-935c-179743ccab17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 172,
                "y": 158
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ea1f4f8c-eda3-4fd8-9beb-ddd624020e1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 280,
                "y": 158
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7209f0db-e965-44a6-958b-37307826bb10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 188,
                "y": 158
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d6dd70aa-d0e7-446a-8faa-f796a589799a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 204,
                "y": 158
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "221231d4-3994-437d-b0e2-5b2fa8adfe3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 220,
                "y": 158
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "dba22223-409b-4a2b-8edd-5c06fe5fbd17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -1,
                "shift": 11,
                "w": 10,
                "x": 226,
                "y": 158
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d4a8be32-58bb-4f91-a251-7ea0568572ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 238,
                "y": 158
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "cb56f2ac-f07e-4026-b4ca-3a37c8f32651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 253,
                "y": 158
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "115d1e80-7ce9-446b-8bfa-cfd41697b330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 259,
                "y": 158
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8b333e69-c23d-48d3-b5f4-5812238ccbc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 448,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "33ee6fc9-c3e3-4541-8b57-da71fe6c5145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 192,
                "y": 197
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e11b1352-70e8-4909-9adb-cb5e5caba820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 433,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a27334ea-a256-436f-a269-945e5bbf47f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 403,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "33f228dc-124f-4d10-857b-d67ab2bb7a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "59919855-84b0-4311-b50d-01b7581d225c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "746bfc78-4a3e-405b-9b08-2d04bde8921d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "76ad3a1b-921d-4f75-b049-3ddb931d4bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2348d936-54fb-49cd-b786-41032cc8efc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1f306b18-bebf-4f06-94b1-f627db4d649b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "85cc6e7d-bd22-4741-8f29-f539de12003a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "89750964-d741-4fc6-94d0-03a5a4750801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 36,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d5e7b850-6cf9-45f6-b0ca-4957a4860ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 150,
                "y": 41
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b8a55ba0-f3ea-44fe-8038-a46505d211a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 41
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f76469f0-eac2-4491-aa30-fb1102818fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 65,
                "y": 41
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fc96c9ce-270f-427a-911a-72a356ebe51f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 70,
                "y": 41
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bf64e25a-5635-40de-bfc9-92de93f07690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 82,
                "y": 41
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "b37308be-ac2c-4af6-8197-528ead9d748c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 99,
                "y": 41
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "c0c45714-8174-4391-b29a-19159a445212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 116,
                "y": 41
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "9d79ccb5-b115-4743-90c6-45cb593474f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 135,
                "y": 41
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "52d7049c-ca85-452a-8819-0b43d86b550f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 392,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "7546fa57-cb5c-4208-a7bc-ccf34829aba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 20,
                "y": 41
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "8647a2ec-c033-40b2-937f-ba669f8e7ece",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "3c41537b-6b0c-4274-819f-be2fd9e655c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "735cdeae-daf4-4857-93bc-3f87b157af18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "50413f12-a3af-41cc-800d-d769156fd7b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "810517eb-1911-41dc-bc80-13f21255b0bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "ddf182de-e768-47ec-a383-794d92a8083d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "7d9447d2-699c-4260-bdd7-af8007c6b9f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "c3a76846-edd1-4ba4-93fa-76ec5bf5ab7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "f4592c5a-6428-451e-bed6-5ef87284503b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "07c5707b-8600-4258-998c-deaa2f0aaf76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "f6a8e60a-64bf-4565-8f1b-f32128628ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "68c06b21-4fe8-42ff-ab77-593f8ee8321e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "51cb0fe6-15cb-46bd-9cfb-8d338058f783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "2bba559a-7176-455a-8337-17e368cc9807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "b648154d-d6f8-40b4-b9bd-82e54d4a7f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 19,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "9e85e267-1d20-459c-8f0e-396a5c047042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 269,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "3c86e71b-b3a1-40f9-ac7a-7bffc6d74154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 288,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "1cd66d03-b02f-4934-832f-d8f9ba1d6c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 306,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "b87d5b5e-ac9b-4b6c-bb66-5c6baa4c1c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "845f9916-7cb1-4e9c-b1dc-3463f44ebab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 165,
                "y": 41
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "f0241d5f-b3a2-440a-9f07-13a1fbfb65d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 181,
                "y": 41
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "1edb6d46-983f-4e7a-944c-b513ed726700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 37,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 205,
                "y": 41
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "6c815026-3a6e-4dc6-8edf-60c8086eed54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 98,
                "y": 80
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "c293ccc7-58ad-4615-a62e-b7a03beca45d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 37,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 120,
                "y": 80
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "65cf46d0-d227-4dc7-a49d-110bd5536c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 148,
                "y": 80
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "06066428-5f7a-4fde-bb01-18c4e6c5f1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 163,
                "y": 80
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "65c2a578-2a7d-496b-8861-93d6007d7ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 37,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 181,
                "y": 80
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "2a60f2b2-216c-4726-8af3-b06187a0de9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 212,
                "y": 80
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "f60c7bcf-ca94-4b3b-8819-0fdc6a1419b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 230,
                "y": 80
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "d8c54dc0-da70-4e98-90da-15ff32ab83c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 246,
                "y": 80
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "ba459eb5-54a0-4577-802f-c3d26c8e44f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 261,
                "y": 80
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "99b1eb5a-03ab-4105-aaa1-22acadaf681e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 275,
                "y": 80
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "c3d9ca59-4646-4e0e-a138-f7549fd0f36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 289,
                "y": 80
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "64e010e3-98c8-4be0-85de-7fc63740e858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 307,
                "y": 80
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "4588e809-60dd-43ad-a292-2ec469567bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 323,
                "y": 80
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "e70b0a00-7c11-4f81-9de3-b6c3aeb8e961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 343,
                "y": 80
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "d39668a3-38f1-4766-9369-4d472358c1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 355,
                "y": 80
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "5d8baa15-d1ed-407d-870d-76804603fa5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 372,
                "y": 80
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "9847331b-2d83-4c33-bd8f-f264bcd0e857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 389,
                "y": 80
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "fe29b62b-df9a-4fb3-af8d-6dfce86abc60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 80,
                "y": 80
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "67efd012-2cae-4618-9ccc-2a06bd30c2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 58,
                "y": 80
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "b149d1c5-1670-4b21-bcc6-7b9badb42d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 42,
                "y": 80
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "fc3d8253-7b9f-495e-bd58-886f1d146734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 349,
                "y": 41
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "28aa94bb-7359-4932-bd55-a571339ae90d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 232,
                "y": 41
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "c9dffc62-cc7d-4bf4-a193-0e745e4cbf4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 248,
                "y": 41
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "f9a431b7-8c48-48f5-91bf-b5fc20adbb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 263,
                "y": 41
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "d3eae316-1054-43a9-85ec-39800ebabbfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 37,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 277,
                "y": 41
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "f332982b-fe39-42ed-969b-4d2528e835b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 37,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 291,
                "y": 41
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "145e5b50-f472-4cf5-8533-aff503e9979a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 308,
                "y": 41
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "9654eabe-301d-41a6-9cb5-be7fd3ae2bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 331,
                "y": 41
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "912441f0-bf98-43be-a18a-7f35e4c31caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 367,
                "y": 41
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "6fbe79d5-2902-425a-a80a-296527d731ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 27,
                "y": 80
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "e22e0c57-9043-4078-8c48-e53c2ec4a2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 383,
                "y": 41
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "800ac70b-9925-433a-9f7d-6c5d09bc76cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 404,
                "y": 41
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "66b27ad5-4313-4b9e-80eb-469512c00b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 427,
                "y": 41
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "38c76f3f-aaa2-45f1-a5ab-fb61c8dde0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 446,
                "y": 41
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "4aaf9e68-f23b-4235-9a30-e21bd0b9f1bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 468,
                "y": 41
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "f1336e50-2ce7-428b-9f4b-916c76078af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 482,
                "y": 41
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "09995dc0-b806-4258-973a-8504c9402fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "440ee746-b76c-44eb-b518-0f57ad5a9f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 418,
                "y": 80
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "df246bfb-1b41-4fa6-a0e9-ad8b3a60fc39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 206,
                "y": 197
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}