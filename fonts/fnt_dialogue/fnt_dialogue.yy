{
    "id": "c917f5c9-f477-4a40-9a9a-8cb4f326b348",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_dialogue",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8d0fd5ab-2e9f-4b89-b069-b912e478d86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0e7902ea-852f-4959-be6f-c76ba8b165a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 88,
                "y": 142
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2b10beae-a204-42b0-a3f4-486dc4706cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 93,
                "y": 142
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9c4c8df4-8707-40ef-a9d8-8827a1683202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 101,
                "y": 142
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d1a2513c-373b-4e9a-be73-76a5c42b20f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 119,
                "y": 142
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fdf58907-e491-4136-9dc9-4518ee8df711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 133,
                "y": 142
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f26365f5-c668-480f-956a-894a44550bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 149,
                "y": 142
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0850062d-2517-4e26-8d0a-73f7a47e80ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 163,
                "y": 142
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "78b90609-bb8d-4131-9db7-23176ba8d563",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 180,
                "y": 142
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "dbe2ee36-afa0-4955-8baf-ce7de59455f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "165de41d-7383-4ca1-9709-83727866be09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "def8c655-94a8-4eef-9e6c-8ea3fe370a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 199,
                "y": 142
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fa38a9fe-0a7b-4ede-9a41-64d40daba8ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 210,
                "y": 142
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "98ed3e46-5fec-4422-82fe-400f187a7a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 216,
                "y": 142
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "64248863-044f-4271-8082-8d95cba864cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 225,
                "y": 142
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c05f19c5-9195-4937-80fa-bdbcf3f1dfbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 230,
                "y": 142
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c80fd049-b8d1-4d6b-8617-3f5c906e53ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 242,
                "y": 142
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "371bfaf8-cb03-4550-a95b-d9f368814b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 79,
                "y": 142
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2d9dd7e8-f15c-4b2c-bc6e-ab0e7354bcc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 168,
                "y": 142
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "330933d2-a56b-4d71-be84-6be51c69cbf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 67,
                "y": 142
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5b633b04-8a80-40f4-bbd5-383ac8db2863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 180,
                "y": 114
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4141b7d6-bfec-4de9-a2f9-b0bb2b11082b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 107,
                "y": 114
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3e92e8c1-67eb-4f3a-9be1-3c6f2659439b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 119,
                "y": 114
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b5435748-33c4-4034-b2ad-b5484dd95885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 131,
                "y": 114
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c5220c4a-46fc-425e-b2d8-0176c15f0cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 145,
                "y": 114
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d9e050e4-a789-4970-a25f-de10f1c87bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a04f4401-7518-4ab8-9101-da789e7850c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 169,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fcae79c9-7e3c-417e-b2af-c81f01778801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 174,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4c1351da-3af9-4fb2-9c18-855ca06d0097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 194,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2467b900-a1ca-4d54-8204-4256b126f4cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 142
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d1231afb-e0df-497f-bc98-da10f0d8628c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 202,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5cadaa95-9b4e-423a-9e00-69749f2cfce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9544addd-a9de-4b0d-88b0-426c04690184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 222,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ab377580-7752-4f50-9a4d-d2401fc3f481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 114
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c73a9b00-fb1b-4a68-b078-caf13b692ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "97d585c7-83c1-49d8-a749-5fe9247580bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 15,
                "y": 142
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9f61558e-fc84-48cb-8002-c4291ab77f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 29,
                "y": 142
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e3e49afe-8ea9-413b-938b-bbe326ad7bc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 54,
                "y": 142
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8f119818-93f8-45db-beb6-0cb7a9cfdb75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 170
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c970cf3b-5f76-4493-a999-9eef32962c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 23,
                "y": 170
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8ec3b4ee-4a8e-4b65-bdf4-4f4efad48115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 38,
                "y": 170
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d3e6e746-7a83-4473-bec9-cf29649fec81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 198
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "239ed48a-49a0-43f6-bc8d-8f4d206439c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 37,
                "y": 198
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3578bc25-81cd-4878-a553-4caf87158c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 52,
                "y": 198
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1e415550-75b0-4ebd-8869-f1b68d9e9614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 65,
                "y": 198
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9d20dff6-a832-498d-89bb-3583577be839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 78,
                "y": 198
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8ffdc438-482b-4659-a21e-576ca0012f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 96,
                "y": 198
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a381d7e5-d5af-45e1-8f3e-0df3d5c823cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 112,
                "y": 198
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a79d23de-3a64-48bb-b45f-23a594be55d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 198
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f7820b62-1560-4242-981c-8c86a2d520b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 140,
                "y": 198
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e03beb8d-63bd-48ff-835c-1952bf0a625b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 159,
                "y": 198
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9c8a887f-54b2-4ebb-bab0-bbfb9b05b906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 172,
                "y": 198
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "510e17f1-cd17-4e64-8725-85e11a031b87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 186,
                "y": 198
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f088782c-cf47-4837-aed6-700d26259006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 201,
                "y": 198
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "fe723aa1-f929-4118-be26-9edfcf5fd818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 215,
                "y": 198
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f0eadbc9-c0b6-4a57-a4c4-760d61e089d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 229,
                "y": 198
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2b1cb6ce-a74e-4ca7-8e15-bc557152deb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 226
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d8ba2b78-4829-43ae-8e08-090226be0c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 18,
                "y": 226
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "70f4e09f-2053-4a66-8ff3-1a63d2197543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 10,
                "y": 198
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a27708fd-14a5-421a-954c-6ed1e9d8f52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 198
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d7847a80-d947-4251-92f0-ee1c8657ef21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 238,
                "y": 170
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "250bec39-9b0a-443b-bb00-24269edc2d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 135,
                "y": 170
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d1a6fee0-48c0-4242-a9be-fde2e1d30625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 53,
                "y": 170
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a2693d70-6cd2-4a81-9484-2d2b7eaf4bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 64,
                "y": 170
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "216ddb80-113d-483b-9cb3-d3c05ed42938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 5,
                "x": 80,
                "y": 170
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "eb8e478d-83f5-44c9-ab45-00f31b20933c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 87,
                "y": 170
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "21997f9b-69a0-4ef1-a60c-29c0c79991fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 99,
                "y": 170
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4eb68735-e587-4403-9367-3398adb8448f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 111,
                "y": 170
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8d654038-1c60-445f-a151-2e0a61584a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 122,
                "y": 170
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "cbed9485-e732-4489-b2c1-9a1e028399a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 143,
                "y": 170
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2d5f2101-8d6c-4da7-bdc7-31bc75b9eb93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 227,
                "y": 170
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8ca33288-15c5-4f01-93d4-c47d354f288b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 170
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "20c78076-f010-45c5-8e1d-7f1451753bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 168,
                "y": 170
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6a3f8fcf-09cf-4f28-8200-fa37044927b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 179,
                "y": 170
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "70200bf3-cf79-4d74-a7d3-209db8843604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 185,
                "y": 170
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7a146c1c-2948-4812-b90f-b80a82e8ef12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 195,
                "y": 170
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "df63b59f-d487-434a-a2c8-2740c17fada2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 206,
                "y": 170
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d79bf265-c538-42dc-b7e8-cf51320b61e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 211,
                "y": 170
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2fa90c38-cb06-40ef-b5fe-682d9f24ca8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 96,
                "y": 114
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "65f2ec46-a4ad-4f94-8cbe-db23c7aaaee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 226
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7f5f95f3-7536-4057-8995-e048c4c9851a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 85,
                "y": 114
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "140026dd-983b-471c-b517-8cf01ff37655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 63,
                "y": 114
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c0471033-5a38-4b13-9501-5035958ffc34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 63,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "80d7883d-3a21-4ac9-9074-222d4ebaab7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 73,
                "y": 30
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e61639fd-a502-46a3-bc9a-6e1e76f6a6d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1c51ba1a-ab25-4f0a-98df-823f2f184570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 95,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ea8d13e0-7e25-450a-84bf-b922a9b02efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 106,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dd6c788e-a8e6-458a-b668-d4cd952643bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 117,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "17d25054-fa49-47cd-acfd-074703ae4b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 132,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cbb65ba7-442c-4371-9f01-36e739501276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 158,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "88fbe9e9-fa09-40b1-af32-7e3ea1e497d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d64ee479-88c7-428d-99a2-4e9a97a3ec70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 171,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3b29b765-d620-4bef-b89a-b474cde1d901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 180,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "86da2970-f1db-4372-a125-df761c4e0341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 184,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6cf42782-a790-4763-9021-06a15bf6d96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 193,
                "y": 30
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "af2319db-9bf4-4826-93fe-f89443876c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 206,
                "y": 30
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "8581c8c2-88e0-402d-b28c-007bdfafa2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 219,
                "y": 30
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "d238d7e9-e5ff-43ae-ad73-a47798eb93a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 233,
                "y": 30
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "aba5e3c5-d785-4cba-9ee1-16e475ea7429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 50,
                "y": 30
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "73e06acb-c860-40e2-860e-70702de7a734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 145,
                "y": 30
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "6a76a0f4-daf4-44d1-812e-b53d7d782fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 30
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "02b1a9bc-70c9-40c3-9fc7-fca1e54fbb45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "7d4cc3be-8357-4910-9881-437cce77912c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "4ad521e5-5d3b-4d0c-8d51-3245e8f63d95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "10799cb0-77fc-4582-80ef-cc64b263391d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "c17a2cb9-a38d-47b1-acaf-653365f21366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "102e8a1e-689f-4504-81cc-325536604390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "fd1ea1ba-b851-47d9-8856-35ff8aab8bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "4b366d9b-8dfe-4542-84b4-23e2dabf5f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 26,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "ef759090-df20-4712-9823-cfb269ede2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "e2de859c-1e6c-415d-8933-0de780e2a8c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "6b052326-a8f6-4aed-b8d9-2e77378b6090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "d664d107-53b9-4980-b1e5-3449cdc21754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "37d269f9-8d81-4762-b2e0-a65e88bf4283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "a31c3397-207c-47a0-b363-5ec310fefa3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 26,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "8df0d450-6440-4fb9-821c-466d4f7d756c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "2715352d-9fd3-42fc-89d4-cbc99dab4ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "253f09a6-b468-4418-8554-2ca872f4c5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "b7a2474e-96a5-4051-a6be-48db0452eac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 30
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "12128099-5ae8-42fd-b9ad-1c73168105eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 13,
                "y": 58
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "a824f177-0d47-4a91-8621-38a6d0323bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "31f850d5-3baf-43fb-9747-6a57955ddf1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 45,
                "y": 58
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "691d58ef-cb52-4797-b3e7-1f379c60a468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 84,
                "y": 86
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "c5218082-f5bb-4029-a374-48ba5e33f99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 26,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 100,
                "y": 86
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "3ac3b683-7922-4ddf-b6a0-cd49b0ba0823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 121,
                "y": 86
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "1c1fe7b8-b816-476e-b83d-24616acb1f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 133,
                "y": 86
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "52befc38-32d1-4d10-8671-c6f616d38a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 26,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 147,
                "y": 86
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "b1f11550-8dfa-46ac-b1fc-58242abf20ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 170,
                "y": 86
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "fa6ae8a2-aba5-4f5b-826c-a98525f962c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 183,
                "y": 86
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "d18eef46-7590-4342-9e23-6f9677bc6660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 86
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "2ddb3e25-89b6-43fc-bade-79a9b67c2164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 86
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "25f2aa29-94f6-4e12-a4ae-1d68c94d85fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 217,
                "y": 86
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "776eea51-8524-469a-9505-07249187da27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 227,
                "y": 86
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "32ba48aa-1181-457c-aeb1-7afe3b8b2b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 240,
                "y": 86
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "7333a2e8-3662-4674-8d61-185e1a1ef28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "6d201760-292e-4b69-a8da-86e8e76a269e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 114
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "a1793056-b7a3-49c4-afc4-fa12dfece148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 27,
                "y": 114
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "a5489c3c-bfe3-4245-9d0a-f8b520a9a4d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 40,
                "y": 114
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "99f722da-0e8e-4333-af8b-93e693b7c500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 53,
                "y": 114
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "78f4815e-1782-45bc-9e4f-5ad2a76a0219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 71,
                "y": 86
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "b613e84c-696b-4ab4-8c5b-9a3ce305bd3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 55,
                "y": 86
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "c2a74d7e-9ebb-4bfc-9856-19bc8d04bdba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 43,
                "y": 86
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "6283b688-e7e8-4238-b384-ad61836419f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 154,
                "y": 58
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "bf863d89-57c6-41ba-99d7-9287e2475da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 65,
                "y": 58
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "1077f3e9-e41a-4199-8f4f-ce4eca051f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 78,
                "y": 58
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "323ef61c-48b5-456b-852e-efbce5fbe4bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 89,
                "y": 58
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "bc90e7ae-6a4b-4327-baa6-077eebe0f27e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 100,
                "y": 58
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "677d7041-9903-4df6-aae2-39d1ed9e8310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 26,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 111,
                "y": 58
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "5bff9b41-bc9a-4a58-a713-955b79e2ecd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 124,
                "y": 58
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "58817639-b4b3-41cf-a414-65e20db221ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 141,
                "y": 58
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "1a7747e5-f94f-4bb7-a709-d2b6549a3ec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 167,
                "y": 58
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "a0f4e2a0-eaf7-4bed-93c4-e33a1d6aaea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 31,
                "y": 86
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "15adfdb9-9cbb-465e-8ad4-d108136cd1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 180,
                "y": 58
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "0ebfe867-874a-4e07-b8a0-536fc8bbb514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 195,
                "y": 58
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "4adb301c-21e1-4c24-a12b-b13e32f7bc3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 211,
                "y": 58
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "ac206c43-d526-4711-8c3b-5f735859b0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 225,
                "y": 58
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "144b37a6-a610-43fb-a80f-f87aec4f3019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 241,
                "y": 58
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "867d6975-a9d5-4a3f-a1d7-017d08067725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "8cbd7bca-ed41-43a0-bd1a-252f6952f0ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 12,
                "y": 86
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "a82227b5-60b7-4c7d-a782-e3c13633a99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 74,
                "y": 114
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "87e6ccc9-b644-4e73-8ccf-222002d597b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 44,
                "y": 226
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000d\\u000a0123456789 .,<>\"'&!?\\u000d\\u000athe quick brown fox jumps over the lazy dog\\u000d\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000d\\u000aСъешь же ещё этих мягких французских булок да выпей чаю",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}