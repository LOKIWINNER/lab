{
    "id": "c9617552-65ea-4207-9830-9d3dca5d8324",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_engine",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2c97d04f-5490-42aa-86d8-83e80d024bbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5ec21ef5-fbf9-4c58-941d-0050a729457b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 235,
                "y": 56
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9cb24349-b5ca-442b-b16e-4ec22d6cb235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 239,
                "y": 56
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "7049172b-ba04-47b9-995b-3afabe6386c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 245,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b2101ad5-4ed0-4a73-bd5a-9007ab91b385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c10cd625-afc6-4388-a150-3ef93110480d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bfbb1708-c4ce-41c7-a5fa-bd8daec6eab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 74
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "79c10fe7-6124-472a-8b3c-c720e8ff7000",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 35,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "18741490-9059-49e9-a00a-616d0491e693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 48,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "49822b95-c458-4dcf-ae82-90cbedb5ea66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 99,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e294bf5c-0c99-4cfa-a52f-0e4f1d555a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1f9046ff-0ace-4994-8c9d-7b521a4333df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 61,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "97bf515f-4abb-456f-84f0-7abe41a9eda7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 70,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "82b2d463-32fa-41ad-a20a-dd426b12d366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 74
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "37276849-3fa8-478a-9c05-7913a30aebe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "09a13763-cd57-445f-82da-ffb0c30b19c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 74
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0d2f2c46-6d01-45e5-a76e-5df7d432de1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "bf17baaa-e92e-4996-9df6-39afe4d9adb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 229,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2aa94350-831f-4e70-b67e-18c98199f94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "81e4c41d-d6e3-40df-b5f1-4eec3f4fcaca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 220,
                "y": 56
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3e76c3ab-f0e1-4592-bd29-f963a5b79a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 56
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "52781070-a1ff-4411-8aca-dd890a1eec99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7c372f7a-6761-45e5-bba2-25de0ea0590a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8ad3591a-e5a6-44d1-8362-f6520b14f664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cabb7427-e9e6-4d7c-bab2-479f1b79a2b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ab8f87ff-d85a-439a-8540-ea8413bf5cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 91,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "30f7ec21-6870-474d-803f-2ace5cbc0891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 100,
                "y": 56
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "637b02f8-6f78-47a1-8c76-892fbc7ef2bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 104,
                "y": 56
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e7f02da1-ee50-4c9d-9842-d227c1b62660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 117,
                "y": 56
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5a0aa572-42e3-4322-a7f1-fbc5c37817ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 202,
                "y": 56
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ddd35d71-de1d-4d69-85f7-70c9391bf299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 126,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7fd491dc-9da6-4039-a67c-d9303c9d8e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 56
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5e3cb8e7-1b70-4bea-b03c-3766f53b2a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 144,
                "y": 56
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e0d8719e-2ddf-4c4f-a641-24e1a7058562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 159,
                "y": 56
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ff385f48-2a76-4e6a-82d5-2048e313f08e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 171,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "06b0b4b2-4a12-4475-af62-65f083f99570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 56
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "10a6ec22-a12f-4aa7-967e-5fedff3113b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 192,
                "y": 56
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a0e014cb-6cf7-4223-9e88-0b6170aacaf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 211,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cdf5e393-56d4-4749-a139-0387e181ca97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 105,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fab3bc1e-c0cc-4c52-adc3-e3c9f29ab53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f83a5f73-ca0a-4d95-bc01-bd1615187224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bd49e207-1d94-44b8-b2b1-00a213653ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 43,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "33cea23e-ec73-417d-b72d-208e90bf950f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 47,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "35ca99e4-cbab-4ef3-93be-f17842c21529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 55,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "06498794-14dc-4192-a8e3-b5f83cc37b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "8c7e9ed1-b772-4299-a88e-46687101f5c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 75,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "324de482-2cf6-4bf3-ae42-20638a352988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cae9094c-361b-49e2-9a11-32102dfcbd3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 92
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "07b171bc-8180-4a61-97fb-d326e70cd941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 110,
                "y": 92
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f9cac990-1a0c-455e-832f-8afe00a880b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 120,
                "y": 92
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c13895b9-1412-4bd5-ab7f-e013e1d277a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 92
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "adcd066f-d015-4234-a21a-7ec1f0c56d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 143,
                "y": 92
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "093130d5-78aa-4d18-b503-065259e77f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 153,
                "y": 92
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1c86a6b7-7d7a-43d8-ab81-06c3b5e327d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 163,
                "y": 92
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1055d43e-ba7d-4c26-a182-f364d96c891e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 173,
                "y": 92
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0b36f670-2c58-4fb0-81cb-36458f3d2d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 184,
                "y": 92
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5e88446f-0932-4ed8-9ecf-3dd26df0045a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 199,
                "y": 92
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "22600ea4-56cc-4d97-8e60-6dd2c7c8bd39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2dcec548-586c-4943-b204-75f021ba1f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 33,
                "y": 92
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a58c53f6-aaef-4217-a062-0c944bbec80f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 27,
                "y": 92
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "db24d05f-aab6-4661-b73a-990fe0cc5ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e7514c3c-23c2-4e6f-804a-9e4c50aeab13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b1d3b0f2-5ab5-49be-a8ea-44ce0fb47662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 136,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5c420846-5b52-4456-ae85-64fed3d62562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 144,
                "y": 74
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3a564d36-6eb4-4b4b-9072-87cf47714c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 155,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a112aa0e-7351-4b86-9059-b792d00e7d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 160,
                "y": 74
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "91d4a8c3-dc96-4a8b-aae3-77c296707112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 169,
                "y": 74
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "eda3a3b2-8832-4f9b-be26-dec440d5323a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 178,
                "y": 74
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9a119d38-ba9b-43a1-b352-d6434958280f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 187,
                "y": 74
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "734ebd43-eb6a-4904-b1ce-22ba326916e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b0533e6f-3531-4fbb-bac3-ffc4a9a76f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 14,
                "y": 92
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "318a7f02-0d1a-4499-bf88-ece7d7092f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 210,
                "y": 74
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ae28030c-104d-4c09-8a9c-f4ea3c278ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 219,
                "y": 74
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "66fee236-6467-4946-82ad-00191865cc2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 228,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "0b335187-440f-4cb4-b944-a72e8ad0fc84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 232,
                "y": 74
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d9909e01-96c7-4dc0-b353-4ecaad09949a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 237,
                "y": 74
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "eefd914a-be3b-425e-b80d-79b68ec09a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 246,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "504a2d8d-1fb3-4e4d-9d0e-8a15cd2487bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5f1e942f-63d8-45c6-988e-77539f4d04d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "af5037e5-69d3-4fc4-8202-ecbd1adf222f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 221,
                "y": 92
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cc65ec7d-96ab-42ff-93ca-1e8221af4cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 37,
                "y": 56
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ee88a687-8812-4c82-acf9-98c463a9f363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 19,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7c240520-ae66-4742-8321-332569d30bdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f79e9198-eb68-40e8-a59c-c0a7c02c94f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4579f350-cffa-4aea-9d38-2f570ca8af8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bb49110b-7328-4fde-9261-dc8486b70024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c2b8884c-cea9-4146-822b-4b28aa97a1c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7482e795-21a4-48ea-8f1e-4257f3578a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 11,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a636fa04-ded1-4072-a6f4-fc80bf907639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 20
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a56b9b66-1b03-4c98-b150-36060bef496b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 20
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "370792a4-f8e8-4c81-86bd-845c4255201b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 20
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "2dd88035-87fe-46aa-8495-5be69f9a6990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "559eeaad-52d1-4e9e-a34a-65117f54407f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 57,
                "y": 20
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bfad2fb3-7669-4056-a6c3-54d97990d55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 61,
                "y": 20
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a3de908f-27c5-48f8-8622-03fc36782a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 67,
                "y": 20
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "ce74856d-aa7f-49bf-9df8-8ffae7be088c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 20
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "0ddd7400-3239-4598-9017-a39fd781d4c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 20
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "93a3855f-a09a-45fd-9b09-0d7964d6232f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "eaba8825-f20d-4335-ad1e-35a13df6df8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "2406fa26-0bd1-4d38-89ee-1a98a4f68a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 16,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 20
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "384bfce7-792b-4806-9191-8318d7708aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "e416f151-feaa-4953-b758-7f47759b9d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "d6de0db5-ed91-40d5-a11d-61b51561b68c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "b8f62e32-e19d-40a5-b3f3-e07c69af7cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "c24d0121-ad53-4cb0-ac2c-2230beb1ff10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "2c6824f0-5566-42c6-852e-7175762d5a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "8a96ae64-99ca-4d45-bc10-e592dcdc1b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 16,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "2457b6ea-5e1a-42a9-ae53-e07b13524ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "0974c51d-33d1-4629-b2fd-ecdf1c7d0c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "f37fbb51-a98c-487b-a06e-53d10751ed9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "7569d734-17c9-4af4-87f9-d62e5517fd6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "50d1a0a2-d723-446a-a276-d32286010599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "f555257b-b9ff-47d6-ba24-6f293583606a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "190c0e13-ac09-4513-8516-dbe99e3c5438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "709b6998-a69f-4fcb-9564-47707eedff6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "d48a3d61-52ce-4e99-af46-ba450c7e6670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "ca95de69-f511-4898-adc2-af3c7c7c8a9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "ce4580f2-02eb-4979-980d-11ca76667ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "57f0a4bf-4bcb-4875-93d5-7678653589d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "23ae0562-972f-421c-948a-dc8963a85d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 20
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "fc5d9d40-3141-494a-83b2-e9c0b21f25d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 127,
                "y": 20
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "4d5f3ee1-7955-491d-a291-43ec842abd0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 139,
                "y": 20
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "b4bdfd70-8a7f-4b01-9cbd-bd375ac14faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 103,
                "y": 38
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "7eda93fb-7219-4bb2-838f-0c97ed403f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 16,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 38
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "b4118602-7cf1-4a7e-9ad1-ded77ce635d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 16,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 38
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "d72ea3dd-fbb8-46a7-976a-bad209b089de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 38
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "c8217761-3c7d-415c-86b5-ad8b9f124ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 148,
                "y": 38
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "addd7b19-fa7b-46e5-9592-39984b11715f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 162,
                "y": 38
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "a6c12a8c-359a-4b5e-90ba-ca845588c557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 173,
                "y": 38
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "1013a876-e484-4cd1-9fb7-21b1d1d51f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 182,
                "y": 38
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "eced8fe6-e722-4776-be9c-7f4eceadf93d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 191,
                "y": 38
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "a6709af1-3a9a-4403-93ed-1d8d6e5cdbb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 200,
                "y": 38
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "deabcb7d-1afe-458f-b8c2-e76aac509101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 207,
                "y": 38
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "d40d7ea7-a1ca-4cf8-a945-89f6f4c69447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 217,
                "y": 38
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "04a9b55e-95d3-4b14-a61d-b3b915dcdd34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 16,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 226,
                "y": 38
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "70c0d590-0e07-4e47-b00f-e75ccdd4aee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 238,
                "y": 38
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "e8a927bf-d51b-4405-8723-4898dc6e092e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 246,
                "y": 38
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "0edb671e-4230-4793-b6ad-f04c5b28fc38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "29207c36-2bab-4de1-b4ab-530938b997c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 56
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "e7e4a30b-0dba-4a62-ba7f-d9eb7ff55761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 94,
                "y": 38
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "0dd0a5bc-1cca-40c1-ba07-9f5f75e721e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 83,
                "y": 38
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "6ff717f2-4a14-4e18-92e8-95691a4df7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "69f0d0a5-1761-4fa3-be09-8f8e7b581b0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 218,
                "y": 20
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "d72923b2-5347-4241-89a4-ccabf5192530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 152,
                "y": 20
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "babfa17d-e441-45b7-9189-8b32bd934314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 161,
                "y": 20
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "9002ded4-f40b-42b0-b14a-85565d400f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 20
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "c138d563-7617-456c-8ffc-50ef42d1507d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 179,
                "y": 20
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "c1709080-502f-4aff-a44a-66c98f91e9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 187,
                "y": 20
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "0227a3a0-eaea-4ed5-a038-aff6f795f427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 196,
                "y": 20
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "910ea90e-2315-4f2e-b913-73ecb507439c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 209,
                "y": 20
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "3a37ad71-f3cf-44ca-8d59-0723343c77a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 227,
                "y": 20
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "d13d4698-6a93-4880-960a-d2956d2a0709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "7c845f0f-15b6-490c-96fd-bdce2a229830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 20
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "17392852-97d7-4eba-8024-d5ea13901416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "5eb44835-ef5e-45b7-adf9-5cd29c396bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 15,
                "y": 38
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "061bf860-906f-4bae-b886-bef2cdcca9f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 16,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 38
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "45b3f5dd-576a-4b22-ad0a-1bf398d50980",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 36,
                "y": 38
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "cca4bab2-7cd8-4a5d-b22e-44dcf958e797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 38
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "dd4181b3-7607-4bdf-a632-d33d85a84cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 54,
                "y": 38
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "63594e13-ac2d-49ee-acc2-db1ca21ddd60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 28,
                "y": 56
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "15cf7170-cee4-497a-9d2c-625dc840cf8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 92
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "af6ea301-a3f9-493f-a926-657be919c0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "38d90531-542f-4982-942a-703bd38c644e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "8b8cd676-d7ea-42f0-84ad-b3c856b41e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "017cd215-fe7e-4edd-82e1-84b4ddd82fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "d4354b38-1b65-4ed9-a772-cb2721e64b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "55fa4786-fe1b-4060-b8df-7f38aa89ada7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "b43faf38-8158-4e26-8f74-24e46b3c1901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "e9a5a2a8-35ea-4dfe-9b2b-8178a6c166f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3084276c-9684-4732-b8ce-95800c39b3d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "e7d37db1-9adb-410d-b416-d4b9d84ad850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "ca5ffc18-ea3f-4819-a1bd-a0971077bcd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "632cdcfa-9c7c-41b5-986e-da2497e1456e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "37e57299-ca94-4ab2-bd0a-6d6115189ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "9ad20d5d-c596-466d-9ecc-1806c36e2d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "77a0b6ac-aebc-45bd-91f6-c14122b96584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "8407a7ce-a116-478a-a3ae-50c9225be59e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "b4a3978b-73bb-4b5f-82ab-76469557ed8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "03d83131-4edb-4fed-964c-0e52b4070433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "babc27a4-6379-489c-a7ac-08697a71b0b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "a273ad50-bffe-4823-833b-d7ca8b25858d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "7d7739ed-ea71-49e5-bae0-81df04180d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "029c0798-e485-4526-b1a2-2d31bad5e887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "7cd2d7ab-9a0a-4681-b9cf-32519128bb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "6ed626ef-04ab-4d86-96b8-6c85ac90dca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "281b843c-58b9-42d9-9155-93b8d838c2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "7f889ec3-4a56-41da-aa40-40f1c21d54b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "157b69a1-e984-4d54-bcc5-ac77e4f6ef99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "2140e7a7-6e98-4fec-bd47-340be7731bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "a2852308-3c23-4941-b2f4-85514bf69558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "6d6fed75-c1e1-4e67-91c8-ee0b578bb5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "70106151-b78c-4106-9ff7-ca5a1040be82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "0e459e01-094a-4140-9660-af13cc232be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "cec3e5a4-1498-4394-9ee7-75b2f5439386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "e847cfcd-e5bd-4823-b4fd-e7b417fd6bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "6b6fa4f5-dd03-4a49-b050-f876cb63181f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "b41bfd46-428a-462c-b0f3-6a088286d97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "f951e9f9-cb89-4475-8a88-0ee6723b3416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "d1f70407-a304-4863-bac7-1db30618d4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "a6113a90-593a-4e3e-ba79-c9eabbf3c6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "0c7fd6d9-f3dc-4b7e-b0e5-d849cb71bc94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "14d2474e-f92e-43f6-a955-c01593d73c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "3f232cd4-5901-44c2-9fe2-61d81c608ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "cdc3b5e1-b7a3-40b5-b553-3fe4f183ed90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "fff5f5c1-2936-4ffb-9bf8-4cd8985a4fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1058
        },
        {
            "id": "39823461-dda5-4939-8b1f-3b53f8596942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1063
        },
        {
            "id": "d1e7a11a-ef29-4d70-9326-e89297d12003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 44
        },
        {
            "id": "4f5bac45-bf0f-4235-91ff-1dd30841af94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 46
        },
        {
            "id": "58f8d23c-6b10-496d-a552-61e280161584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 44
        },
        {
            "id": "d7c52f99-bce5-420d-ba9f-322e717f547b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 46
        },
        {
            "id": "0cd27864-8627-4439-8fa7-4ec57f71f3de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1076
        },
        {
            "id": "5c7fb2ea-9ff1-42c9-a5f7-5d24aa60fc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 44
        },
        {
            "id": "395744f2-cdb9-46db-b340-6175305e600e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 46
        },
        {
            "id": "a77ca78a-dfb9-4994-8f27-894a84508805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1086
        },
        {
            "id": "2ef33759-cc6b-4537-aa72-eb50170eda8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 44
        },
        {
            "id": "f1b2cebb-fa3d-450e-9ed5-9668e7b3ccb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 46
        },
        {
            "id": "310a9423-0a8f-40a5-b7b2-688edaf5ff6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1076
        },
        {
            "id": "80afc96f-4e5a-40b7-a3c3-baebcd293e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1058
        },
        {
            "id": "1d65a90e-587c-4045-939f-25400a36a4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1063
        },
        {
            "id": "20c59d49-b465-4b8d-9405-56395350a34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 8217
        },
        {
            "id": "ed63c299-4c56-42e3-89f4-ab478391c58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 44
        },
        {
            "id": "0397efd1-d565-4c48-8a5b-5cc7a957f675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 46
        },
        {
            "id": "572f5596-225b-499f-b938-876135bb1879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1090,
            "second": 44
        },
        {
            "id": "55dfa0bf-a8c2-4655-b3a4-e42619af5649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1090,
            "second": 46
        },
        {
            "id": "5ee486f3-d772-4ecd-88a1-3939cdcf819a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 44
        },
        {
            "id": "db8b4704-d40b-4d48-b462-ce3ba51848dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 46
        },
        {
            "id": "8ccadf82-fa2c-4a50-8ad3-213b6dc0419b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1100,
            "second": 1090
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}