{
    "id": "8f8ff844-c0af-4352-bef7-dabafa549ddd",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_small",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3ce6989a-29be-431c-8a25-a846bd8adc57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2c07c427-0c17-44b0-bd87-db03423bf04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 163,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c8adef55-fdc2-4ea2-a888-73ae97ab88c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 166,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "45cf767e-b01f-45b9-82ff-bf763aa96efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 171,
                "y": 65
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "999c2e8f-5f5b-4d94-9cee-f1ee54b00959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "d82aa1d3-fc89-4f62-8261-accb222dc276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 193,
                "y": 65
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "154c5dce-5975-44a3-9ee3-201817d265a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 204,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5a3a82ac-edcb-40af-8d65-29669ace69ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 214,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a4f3758b-ab23-467c-a823-e6353e67b219",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 225,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d1628092-53d8-4bde-8ec6-aa881f12da52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 27,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "11d973ca-7534-448a-a5f3-a92eeb16ceb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 230,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0749e280-dc70-437c-b3b1-da00f4d6eef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 238,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "59e32870-ea92-4a7f-a4f8-6e6d19d1ec78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 246,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "07249ea9-ac3e-4e93-a153-37c07f15825c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "60787540-5dbd-4136-ad41-9eda8ba2c689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 8,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "456abe3b-9eaa-4ebc-b9cc-39f2215ff3b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 11,
                "y": 86
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9de0cacd-13db-4822-baec-6720ed97378e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 18,
                "y": 86
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "40a4a996-7bc8-4283-b05b-3b87eefb69da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 157,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d9198865-5cba-40da-8a89-065c4f2579fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 217,
                "y": 65
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b1645d07-c0b1-4cff-96bf-5ce5ef9f7b78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 149,
                "y": 65
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5e3f748b-e3c5-44b7-9756-26f6caf4d1d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 65
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8422599d-67dc-4de1-a8a7-7ff6109cd126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "885fc5e9-6018-4a21-a223-4b247e9f9c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ae8ff8ef-999a-41c4-8153-e811844efd95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 18,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8fb3c338-68a4-43e7-928c-4742066d4cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 28,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bca87113-3fab-44c0-9183-8e03a27ea07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 36,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1fb1e4ca-90a9-4c26-b65a-f017fd3ea1a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 44,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0906854c-d28a-45a1-bd1f-a775ec2e82a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 48,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f2ccf084-2cb8-4763-ac12-80cee2ea0806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6a717bdf-c93d-4dc9-8b0a-557ae7a786d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 133,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "18cfad40-c7d7-4f24-a5fe-1c8d1b43d1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3868e8b6-5f97-486b-a6a4-b25a3cda3a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 75,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9ddf8e1b-3348-4c90-9ac9-b9f2531dca6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 83,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "24386570-9d90-4d49-95c6-4aa2b48e3a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 95,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d4481f4c-eea1-4ac6-b18c-22337bf67ce4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 105,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9072e77c-aa16-443c-95c0-e7f6d13f7239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 114,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1677b7d3-2de1-4b67-8aea-e0393b4c6e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 123,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "189ccf57-bcd0-4eef-9ae7-b700a5d39557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e596a6ef-efc7-40dc-b33f-4e14d59cbe6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 32,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1401acbe-10b3-4665-89e5-21b004a84b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "11e33c1a-7d42-496d-8fc5-aee5c3d32a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 51,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b8105e38-d8a0-4b93-bf27-e53f6f140623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 207,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d54985ed-891e-4dbc-af3a-4f3b38373f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 216,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1f032931-f72e-4936-90c3-2db98fc8198f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 225,
                "y": 86
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b988ade9-65db-4dff-abc8-b275f00008bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 234,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f07cafa5-e638-4558-83a5-7c41e9aa2292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 86
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4121894e-bc77-4b96-aaa5-9075eca02e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d3f0a12b-7543-4c55-96c7-0435c8b08914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 107
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a0de694f-df07-42ee-a491-d9d52c22fff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 24,
                "y": 107
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3bb2d8aa-8f2e-444e-8515-b70ae7c4a179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 31,
                "y": 107
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "318df093-008f-475b-8289-b3d67e9a15ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 44,
                "y": 107
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "5e1be6ed-1e1a-420b-9d17-ba0f627ae3a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 53,
                "y": 107
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9bdfaca1-423c-4a93-b3e2-ac7fb8e15bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 107
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "64864ec1-5c54-4eef-82fd-fad9c1505108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 107
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "354864d3-b8e1-4632-8b20-f37406b1dc9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 82,
                "y": 107
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c981b96c-cc47-46fb-8e34-7f04d5616861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 91,
                "y": 107
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "288650c5-5165-4143-b035-1dda4090acd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 107
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ab90866b-e712-443c-93f1-7d4270e37824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 107
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d32a7756-ab5b-4ddf-9429-8c079209bc1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 86
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "36f931d5-a197-44c9-bd86-59ec397b9d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 191,
                "y": 86
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1bdb9716-db45-4b56-8dc8-02484d77a45a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 184,
                "y": 86
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9f6a9bb2-057a-46bf-8946-c1660e0bf464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 114,
                "y": 86
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "8ff13c53-beed-47c6-9c8d-ccca01020e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 61,
                "y": 86
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9d86c131-0aed-47ae-a5c0-a38360777f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8944c102-6925-4d75-aa0a-ddf740d1aa0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 78,
                "y": 86
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "229d78f3-93cb-4f26-bc6b-5da851fa5276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 83,
                "y": 86
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "40efc3ec-d73e-4364-9ee1-06e8c841e6e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 91,
                "y": 86
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9e1f797c-190b-4aae-b578-f71f8159aa8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 99,
                "y": 86
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3433987f-c9ef-434a-83d4-26a93e757438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 86
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e2f2a53c-d86c-40b9-98a1-a121910d98da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 119,
                "y": 86
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "95290033-7a7c-4307-b8cb-a2f2fab47f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 176,
                "y": 86
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8a41094e-7669-4537-9475-8ce99f694856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 127,
                "y": 86
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "07118018-cdaf-45d6-a91f-0c4f0ce69899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 135,
                "y": 86
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7dd9f6ae-0f10-48d8-a723-0532a74e9d9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 143,
                "y": 86
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "831a3f39-3886-4777-9b3d-f925e109deac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 147,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3f2fa230-032f-42c7-b22e-dfb6f63c6530",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 153,
                "y": 86
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "a6c3c59d-4476-4448-aa57-9551009837dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 161,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "70ecad43-16b6-40f8-92bf-d74a0c829b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 165,
                "y": 86
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a0211488-2ca3-4318-8c3b-84da744f16c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 242,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ab3525e7-1063-4569-9fa2-7914ced0bb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 126,
                "y": 107
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0b59111d-38d1-4562-8c3a-a8f4334e17cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 235,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "909efaab-f492-4af4-8b22-a3080c5ee74d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 219,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1819ee53-75ae-4efe-a269-c4f2d6f89628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "93c8679e-1693-4a4e-b748-9ff84921dc75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "821bd539-7b1a-4f28-a9b6-cd717d9f285b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9eeb10f7-9a6b-447f-b933-d332196b11ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f033dceb-c433-4fff-8285-48d00c9195bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "82c1cfd0-ecec-4027-983a-500641172473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c5f896d6-5597-4cc4-a8b7-8a680da0a1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5eae5da3-f4d5-45b7-b143-a63f0acd5cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "71c03099-4a54-4d45-a334-4b0c558facd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 90,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "945a943f-8436-4a81-9579-ebaec3a30b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 40,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f7e03a1f-c73c-46b2-8e3e-39d8fc3d8c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 46,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "454f5e38-26c7-4d1c-83bb-515bc9fd2093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 49,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "178b46c1-ebec-4333-92ba-d3230dbe1a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 55,
                "y": 23
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "c533c3bc-67a9-4f4c-834b-c58ff8b1e1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 63,
                "y": 23
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "71d198bc-262b-419d-81c4-59e16fbc3c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 23
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "d3c72968-3e19-4afe-a006-19c23470a4a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 82,
                "y": 23
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "2ab3acbd-fbe2-4fe1-bce8-bf7e0533344a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "a1fcb290-9b3e-43cf-b0b6-b1706652117f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 22,
                "y": 23
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "f16896c3-36e0-48b2-876d-1a90a96b0276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "fa7aa4a4-d24b-49ec-926a-87c49523a712",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "b4fad592-a022-4989-b347-6c821278c6fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "c6b91e7f-5cb3-4562-9a52-3c1f541848c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "84ac3b06-c8e7-4f1d-a41d-d3e8a9557e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "1cbe9902-6edc-4aaa-b48e-519ea5a65125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "6664df5c-67dc-4650-a698-9fb407ee7ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "edef91d4-e1a0-4693-85c4-377ab5715dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "f8581551-d9eb-445b-b7c4-4a07962ec24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "f73f5479-cbc4-488e-b2ef-5808a3051d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "bb30a353-9464-4503-9b01-40e1975fbe7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "dfd91e15-84e9-4f05-88dd-103df348e00b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "da3af445-814f-41bc-aae7-23a350636a60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "14bcda15-fffc-4bc1-9e03-f24315584797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "9b71fc67-823b-453a-a897-5b1bf0c5938a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "0eccda04-c841-4369-bd46-312779d78285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "767b200d-82ad-4654-8579-b91525772412",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "e7b78d11-0e0f-4ebc-b6ba-3c8acc243c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "e8b8f20d-1311-4f85-98bb-0825e1d6edca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "a0308ab2-2b5c-4848-ab74-0dce862f9e48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 23
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "8a59ddab-1b87-44d7-adc6-8a570e6d98bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 106,
                "y": 23
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "9a8520f6-f47d-42d5-82c7-58416005cca3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 118,
                "y": 23
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "5980bba8-a5aa-40eb-98bc-12a140706fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "4245c086-ce7f-45d5-add4-7745e67f2957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 77,
                "y": 44
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "6d5cbfde-11e1-456a-a3ef-e4d064fed7fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 91,
                "y": 44
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "e8fc5f58-5131-41ed-a479-e66673b2690a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "eb4fd160-465b-47d7-8717-404c745f2c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 108,
                "y": 44
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "c19e7be3-ed4d-467d-a213-75c599e4061d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 123,
                "y": 44
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "a519343f-4def-4f50-8c2c-a205366b8bbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 132,
                "y": 44
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "26a70be9-013f-41cc-9db6-76d164e49306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 140,
                "y": 44
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "7f667f26-2d01-4086-b6f5-e8f345771618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "880add5a-f872-447b-bda0-3222e64ce6e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "4bf655ae-5f75-4d75-a283-80612e772af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 163,
                "y": 44
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "0d65d057-58e1-4823-96ab-8c440c3c528c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 172,
                "y": 44
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "9c48d1a2-bf6c-44fc-a4ce-638a2bcc9291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 180,
                "y": 44
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "3a1d1ef6-36d4-4c09-bb54-468e60c25fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 190,
                "y": 44
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "38b831dc-8e15-4003-9f88-a2d0f7f956f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 196,
                "y": 44
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "0b220337-d89b-41cb-9642-17f90f3a6f46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "2ca94cc1-b4ff-4d41-b586-671a11d74ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "a7bdf223-0b99-4531-b053-1cec281ae98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "cf357be2-b55e-4ba9-91d8-fb47f8dd97f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 45,
                "y": 44
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "8388aba5-5d5b-4d00-b2cc-1f8e09ca3afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 36,
                "y": 44
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "8f8708ae-a49b-4439-986a-2baf63db76cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 191,
                "y": 23
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "7d11bde1-37c3-456e-b3b6-9e03f584c33c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 131,
                "y": 23
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "152ce274-4479-43a2-b530-e3816e4ad047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 140,
                "y": 23
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "e94a89d6-d3c1-496b-a8c0-48d298f7b9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 147,
                "y": 23
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "8053bbcc-f089-402b-b14e-d87f5a360aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 154,
                "y": 23
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "dc0dd701-54d3-4a47-8b11-4f0e94368d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 162,
                "y": 23
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "aeeef7a2-9c25-46eb-8a79-a643e02432cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 23
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "ebb9c61c-fca1-4d4f-a531-51031978369e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 182,
                "y": 23
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "bf0f296c-d34a-4013-aad3-3ea2636bf993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 200,
                "y": 23
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "d8e99acb-5fa4-4a2d-93bc-a521be6e7501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 28,
                "y": 44
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "08ddb726-0374-4fe7-baec-f002791c801d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 209,
                "y": 23
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "a8eeacd8-b751-40b5-af4b-31d0c85ea272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 219,
                "y": 23
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "812e4933-d235-44b7-ac8e-e8007ab1534c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 230,
                "y": 23
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "88b9cc42-a5fa-4622-afb8-10e4914206e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 240,
                "y": 23
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "fb6bb4cd-82af-416b-ae4e-304e69c80963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "b433adc1-0397-4307-b56d-074b34254a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 9,
                "y": 44
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "c864258e-8cd1-46ac-ac9e-cdadcf9675b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "71761102-ea0b-44d0-a2bc-f180b79007d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 227,
                "y": 44
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "24858b91-f3f1-42f1-9574-4b074eb99d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 134,
                "y": 107
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}