{
    "id": "8f8ff844-c0af-4352-bef7-dabafa549ddd",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_small",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "40720048-26da-4fbf-9dff-d136ef93fc1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a552512c-badd-4da9-87d7-e7ea49ac36c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 148,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "96214931-26db-4cf9-8116-ea6511d6cd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 152,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e52c244b-aebf-4f48-893c-2a62e044b192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 161,
                "y": 119
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b4caff3f-63ab-4d10-ac24-30129ae4a099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 185,
                "y": 119
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "63f38d6d-14bf-4ccc-bb0e-fa8bcc360e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 202,
                "y": 119
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a649ef76-efde-4a09-af4f-3b31a320d5ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 223,
                "y": 119
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "352c08ad-63a5-4ab1-a2a9-2994829054f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 241,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "73919034-8104-4429-b383-0e1a0723e4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 260,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "07c1bbf1-c1e2-43ac-8d64-396dbf673c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 349,
                "y": 119
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8a567964-32ad-4537-9c97-4dcb092009c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 269,
                "y": 119
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a427808f-bef0-4403-b8ca-e686d61a948e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 283,
                "y": 119
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e5be14ed-1997-4be5-8e5e-ee10db4ae2f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 3,
                "shift": 7,
                "w": 4,
                "x": 296,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "78205e63-56d2-4035-887d-c19fd98082f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 302,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e634ce41-61db-4635-952d-4b20464eb5f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 313,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a7a57e40-efac-47a2-a940-49f259853dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 318,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "bfd0ae1d-55ea-4ac4-858a-05209a850db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 332,
                "y": 119
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f9f2a3e9-d7ce-4280-bf85-abde315c18c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 137,
                "y": 119
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8b00c898-01e1-47b7-b5ee-90de8a628c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 245,
                "y": 119
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5808b3e9-fd00-488c-9085-11f6e9a8b29b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 123,
                "y": 119
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0cb3e988-73b7-4dc0-b15b-cc00303efed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 450,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d1ab9767-f120-429e-aa06-184e5910da3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 360,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1a4b8310-2aa7-4b73-86ea-c962bd944a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 375,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "34ffb79e-da85-4d1b-a2ca-b0fa990ec478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 391,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "28809e07-84ea-4331-9351-9136148aec0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 408,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a1f5f5f7-e592-4f5a-9585-280b6643881e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 423,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2c0ca16d-f70c-4680-8220-dff8cbb0a719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 439,
                "y": 80
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "99309f79-473f-43fa-8867-81a620a3a2fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 444,
                "y": 80
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5446c6be-50b3-4f1e-983e-367782a2927e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 467,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a20af904-2893-4780-a488-d7a0f79f93ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 94,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0cca8f6f-b836-4221-9858-126b7a6fc127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 477,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "156cc542-42d1-43c7-ac7c-7ea4caca7e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 488,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b1bcf0c3-cbb1-453c-9283-6eb99f5e7f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "83839ed9-1e1f-473b-8101-6823d9fb5a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 26,
                "y": 119
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8a889014-13a8-46aa-9c09-6bd3012cc1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 44,
                "y": 119
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "69e888de-7bc7-4e01-b6c8-546f2bb776cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 59,
                "y": 119
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ae3b7817-32e6-46be-abac-cf7e2595e827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 119
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b8d28ca5-4690-42b7-a80c-2a7f487ab2e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 107,
                "y": 119
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "55a30c17-55d0-4407-a21a-ca3777de6121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 358,
                "y": 119
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0bfc7e05-4046-4d7b-96c6-1a668f4f5e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 374,
                "y": 119
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7662aa33-0c6c-4673-a6cb-eea4538e254e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 393,
                "y": 119
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0426ba6c-79c3-4481-b93d-a652ad5d9678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 164,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "01eb2509-fe2c-46b8-93bc-5889390da6d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 179,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7e5bd2c3-a165-4d86-ba6c-f358c8c2d3bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 197,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7982a006-89fd-487e-93b7-993d91df9ae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6e5868bf-0d02-4187-9182-b60da0f462b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 227,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0a13172d-012c-48c2-832c-233e63f8614a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 251,
                "y": 158
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bc5df542-1da4-442f-961d-d5e649d20f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 271,
                "y": 158
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1b40f5ab-7cc9-42a2-9101-df138f235281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 291,
                "y": 158
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e31a4ddb-ed24-4ff2-864f-f6520a99e901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 305,
                "y": 158
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "21751230-7d55-42f8-aa9d-96ce561b01e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 329,
                "y": 158
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3e759075-7a2c-4f0b-a5b6-d3cf355ee056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 345,
                "y": 158
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "84c61ec9-489c-4c16-9888-30c64c1cfff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 362,
                "y": 158
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "35a648b9-02fe-43b5-99a0-e1609fde770e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 381,
                "y": 158
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "061f89a2-2014-45e2-990f-938e24b8c562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 399,
                "y": 158
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a31a1e81-f77b-4357-88dc-da43c2ec8cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 2,
                "shift": 28,
                "w": 26,
                "x": 416,
                "y": 158
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fae70152-4835-4bc4-9ccf-53805d1d4262",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 444,
                "y": 158
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "88302e32-defc-462d-b551-24f46988d22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 464,
                "y": 158
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8e9656e2-5a39-421a-b51d-3fd48c68dafe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 145,
                "y": 158
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "369df15f-9100-435a-be5b-ea49b9f5ee08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 136,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "dc2163d5-13f2-4cfa-abb8-957233f9b77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 123,
                "y": 158
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "319938ed-d01c-4628-af99-028c693ce0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5b19055a-bf48-48ac-9a78-59024f9cae82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 412,
                "y": 119
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5d5c5458-5155-4557-8dff-6ca58bd2c2d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 424,
                "y": 119
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cc470a87-382b-41ba-adcc-eae5512968e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 6,
                "x": 443,
                "y": 119
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a36bfb2f-618f-45d9-a88a-493e7e0fae66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 451,
                "y": 119
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "08cfda1d-c61c-4523-a361-1304fcc71961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 465,
                "y": 119
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6d59eced-e006-408d-8e30-a7a0a6768250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 480,
                "y": 119
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9b107fc3-b7de-4d25-add4-072df3598a4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 494,
                "y": 119
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4b641a1b-64c0-4940-bcba-bf0183e18786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 11,
                "y": 158
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f42d8920-bd53-4929-90c3-587b07cf97d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 110,
                "y": 158
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6b07be6e-5b7d-45b2-925e-d828c57422aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 26,
                "y": 158
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0ce9a782-90d4-421a-aa1b-57cba48350ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 40,
                "y": 158
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "de73afa7-b39c-4fa6-822f-a3e27fb2bb7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 54,
                "y": 158
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "737b0fb8-245d-46af-88e5-4b6323b3dd2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 60,
                "y": 158
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a6900006-a534-4f95-a242-70525d7c5deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 71,
                "y": 158
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bab108cc-7084-4aa2-83dc-d5eb8fd96378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 85,
                "y": 158
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ddb71db2-bd19-4137-833b-fa9df90124b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 90,
                "y": 158
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "22c00c93-c447-48ca-b960-1c095c82856c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 347,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "87529a0a-b58e-4090-ab21-d810c9f8e5bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 482,
                "y": 158
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "226bee10-ed81-4696-9211-a17d6b8bb0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 334,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4292e1f2-8242-4dce-8fa4-cfd879c5b579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 307,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a9126db2-9651-4492-a0ff-4addcf120296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bda20941-9bd3-4a75-a842-8759999e605a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 402,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d4d0aaaa-c016-4817-9cb3-83d41016d0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "03a95cc8-de28-4463-928f-c891a0bee6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "29ae2a99-e023-4c37-8e5e-fd99f6aa19c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0e3c0577-b5c2-439b-9ab1-45816617d40c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5c4cc0d0-3da2-46a9-8cf0-dad5affa6a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4536f264-0517-408f-8b10-91d20bd72708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f3e732a8-2cd9-4b0a-bbf1-60c9197bb8cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 109,
                "y": 41
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6060be2c-45e2-4f1f-8020-d2e2b2612c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 18,
                "y": 41
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a2e60e92-13f7-422e-b7c3-46be16a60b22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 5,
                "shift": 11,
                "w": 2,
                "x": 29,
                "y": 41
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1b7b86d8-d495-41dd-a2c3-50c30c6fa883",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 41
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c588a11a-017a-4e8e-8408-6d2af180725c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 44,
                "y": 41
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "a15b5b90-915a-4aef-a6d4-1a854b488d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 60,
                "y": 41
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "d96d786d-e359-4afd-9119-8d187e3db586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 76,
                "y": 41
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "a8932997-6132-4fe1-a154-49d13a4a2d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 94,
                "y": 41
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "e1ae9aef-61c6-4d26-80e8-17a75b6300ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "954f0a71-b58e-4d74-a436-313934113e38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "8bdc437f-9323-416e-81ce-71a82b474a56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "04ce5e64-5647-4be9-a5dc-5c346d759f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "007c374e-ea54-4b91-8f97-79bbc4f4bbbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "e6312ba1-7c44-4b85-8b45-8d45b8f2c414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "171fa633-5a50-4ad9-b716-859abf1bcd36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "5490b83a-e53c-4c99-9cfe-a2bc5b521e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "a8d5f672-d2a0-4b23-8648-888c5a7b24d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "56c2dd54-eb10-4fd7-8ae3-0da076c00c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "468e2eca-b76b-4db1-a896-45b38e653560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "11aff5a1-a290-4232-9e0b-f5e8f5b67e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "97a6afbc-c78f-4872-a198-bbd44b716f82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "63763bea-7d9e-4788-8581-5366be39f20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "74cae2c3-5520-4aff-a130-906eddb9eeea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "f593e7c1-c1af-4335-b5c7-d78e4336a1f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "61f5e71b-3914-47c9-b655-88a0afc3f731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "e464998f-e0ce-4480-afd0-65eabf482c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "3e4e160c-95c8-4265-8ed6-282b20e76671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "61e530c1-2c7b-404e-8795-0d210d174c53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "e30e330e-0bed-4a51-b780-27292931e8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "8b5eddb8-1889-454c-b181-f18d9f211319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 123,
                "y": 41
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "a30d8474-3889-4010-b320-f27136469bc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 139,
                "y": 41
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "64fe83d8-6b5b-4215-971f-12be5c36a39d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 37,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 163,
                "y": 41
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "f2894d10-6b00-4901-b9c1-080e6d82930b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 20,
                "y": 80
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "b1da7007-e98f-4ee3-9fd4-c808051b067f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 37,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 40,
                "y": 80
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "45730cc4-65ca-472b-b59c-75754851947b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "d5c75b38-91c9-46f6-9440-8e8c24019b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 83,
                "y": 80
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "1797ce00-fa78-4470-8084-b0701571c004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 37,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 100,
                "y": 80
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "6aa0a1db-e23d-4430-9d06-ecd36d6a7d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 80
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "4f31289a-eea8-4f2a-9823-bd8eae862a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 147,
                "y": 80
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "de2f6938-c0bb-45d4-b22f-8d6b064ab77d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 161,
                "y": 80
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "4447c14c-8819-4eeb-92b2-e7b78b03c3cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 176,
                "y": 80
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "c1254a04-99c3-4f4b-a384-15e697e53e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 189,
                "y": 80
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "71b07fff-3b9c-477a-8b79-6c2720c66e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 201,
                "y": 80
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "61c361f2-8ffb-43fa-94d9-986ea38dd017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 218,
                "y": 80
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "f7c7f16f-28b9-4a97-9da2-7545db97514d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 233,
                "y": 80
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "5087644b-2dd0-4931-a247-76bffea3a936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 251,
                "y": 80
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "03fdebab-e057-4d0c-86d6-359ba14a1dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 262,
                "y": 80
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "1b881e5a-ba6a-4f46-9e36-d5d684aa4218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 278,
                "y": 80
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "272bf6d1-8709-488a-9f52-a4d4ec208723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 294,
                "y": 80
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "15b311db-766c-48cc-ba83-94097da92100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "61f8222e-2f88-4d9a-894e-48528e8a7faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 41
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "c717ef58-5ea5-47d6-9958-ae0b730740c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 475,
                "y": 41
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "be8d830a-e9f4-49b6-b0af-db23027b8691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 298,
                "y": 41
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "8972d766-03fa-42e8-8fda-a1698a9049cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 189,
                "y": 41
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "fe2b30a3-421c-4738-a9b3-4adf67f1da11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 204,
                "y": 41
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "d7695daf-1746-481b-8718-f91d54a83ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 217,
                "y": 41
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "51518067-2ed9-4165-8f9d-67be3fe58dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 231,
                "y": 41
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "03bf217b-cfd8-476a-8803-da384d8eed5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 244,
                "y": 41
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "06f6dfb6-c00b-43a8-830d-a4f75c080784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 260,
                "y": 41
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "d2b95583-7a08-4d67-b43b-ac8d2c07c584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 282,
                "y": 41
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "31608f83-a1ff-459b-a699-e71e1817c81d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 314,
                "y": 41
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "1be84392-60b7-49ac-9d24-bffb54ba77b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 461,
                "y": 41
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "5279d138-85d2-46b1-b7d4-44c6156170b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 330,
                "y": 41
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "ca46e6af-4056-4554-a4e4-101b27ecddf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 350,
                "y": 41
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "ae88e76c-f1a3-4e5f-ace3-b5dcc80036a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 372,
                "y": 41
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "78a2cccf-b147-4ca5-8bc0-650b381932da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 390,
                "y": 41
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "3be58cfe-b2cf-4bb2-8ad2-4f642d4af77d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 411,
                "y": 41
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "4a48a419-9c6d-472c-89d1-94fbc230414c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 423,
                "y": 41
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "324d7057-dfc2-4216-b315-3e9c8de1b7ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 437,
                "y": 41
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "25497a0c-40dd-4e10-a6a3-9769e2382220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 320,
                "y": 80
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "24a503cf-17ce-4601-8529-c222ba87c3b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 496,
                "y": 158
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}