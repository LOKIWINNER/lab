{
    "id": "b457de92-7943-4bea-bd87-0f7cda8505fb",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_console",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ec251681-15c8-4945-a0ef-56194c547919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c10340ad-608b-4694-b1f0-2ce1675da961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 218,
                "y": 102
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "92534904-bda9-4fbd-b7ef-8e15c34ef42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 223,
                "y": 102
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ca317a5f-9fa0-45b0-a07c-6725feab5f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 232,
                "y": 102
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "deef9267-58c6-43c7-a3de-7a6e4bf54ca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 127
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7377942b-a9b0-44e8-831e-1e62d0468908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 13,
                "y": 127
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0f52ff72-cc70-4ce3-86e7-a79c1093116a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 26,
                "y": 127
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "27edee5c-3fca-4f28-9875-e20127b3ab53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 39,
                "y": 127
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "aeaa71c2-9ccc-4bd6-9f93-6f19c2a5af04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 55,
                "y": 127
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "75810a5d-c5bb-4747-a19e-120c47a89fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 136,
                "y": 127
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cc997500-ff72-455f-9f73-e5c686c6c6ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 64,
                "y": 127
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "72b70a41-d4c5-4af0-8aa3-8460ec93c4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 75,
                "y": 127
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b1d61b4e-30fe-4e14-bf2e-a9a8d172c662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 88,
                "y": 127
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "18f1bb93-8395-4f9a-97cd-a8653c09dfd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 96,
                "y": 127
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9773ac3c-62bb-47bc-b1c7-df03aedb2335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 105,
                "y": 127
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c550235a-8ebd-477c-b488-fb5ee5ec62f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 127
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "62bf8556-6611-41a2-b1af-7d04a7d057b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 123,
                "y": 127
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9782151e-bf8c-4626-a650-185b957894a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 207,
                "y": 102
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "335acc7f-d272-4c43-a580-777c69032eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 44,
                "y": 127
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "18d31248-42a9-4d79-9ad8-a4266b3acf1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 196,
                "y": 102
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "35f12d8d-2186-4256-88b3-9d9cd159e88a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 71,
                "y": 102
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7b099dea-c5ed-4ada-88bf-17a7691be9cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "05172067-3c10-4611-862b-28f3aa6da519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 102
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "da00be59-fe97-47d3-b724-e96bdb9c0004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 102
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "c170e25a-e0e2-412f-b759-9ec9fd3587c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 102
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "96b8f6f5-96b7-4ff5-99f6-d1cf5a7a32fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 46,
                "y": 102
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4803f68a-b594-4d10-9621-6e34947ba8bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 58,
                "y": 102
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "974e5d1c-686a-48c6-9e33-651d6228f630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 63,
                "y": 102
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "287a764b-b8ba-4ac3-8070-8f3d567dfb0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 84,
                "y": 102
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bb7b7e8e-7244-481d-b135-3a4ceee63f5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 174,
                "y": 102
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d4f32b5e-9357-4e1d-876f-efed1a954d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 94,
                "y": 102
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "225e41e6-1e49-447f-8559-a9a134f6ca47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 104,
                "y": 102
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2b0f31c8-c752-4fae-9b38-4fc106cf6f63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 113,
                "y": 102
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b24ca3d6-54b3-42a3-882b-9b7346a3fd1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 126,
                "y": 102
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0f3e4049-d76d-4841-815a-c3227d5dc742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 139,
                "y": 102
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2d80d314-650d-4216-bbf7-69e1ca90d2fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 150,
                "y": 102
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b9a0fed6-84f9-4ea5-a5c9-2e56066e0d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 102
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8eff8067-5aec-40c1-abc5-c422315f0792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 185,
                "y": 102
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fa842b42-a809-461f-a2f3-d77e61fff519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 145,
                "y": 127
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fce607e6-6228-41a6-9e5a-23208543a331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 155,
                "y": 127
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d67b6114-4cd6-4798-be0c-8dc27a0d83bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 167,
                "y": 127
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e730410a-7038-4e7d-b1e4-b701cfb21084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 145,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "47185fef-895b-4568-b212-5ede43f6d3c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 156,
                "y": 152
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "136be957-6976-4ce7-af10-8d0b107c29af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 166,
                "y": 152
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "6e61d14b-e657-4dc4-a72e-351b25509e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 178,
                "y": 152
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e5bf37a8-47fe-4e28-a2ef-b2e0919b65ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 188,
                "y": 152
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "01047660-856a-4d30-a77d-e9d9e16691dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 201,
                "y": 152
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9412dad1-83f2-489f-bb6f-86f59bc149dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 212,
                "y": 152
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f1c83924-ba46-47bc-8e9b-477a3cfd5ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 225,
                "y": 152
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fcf0daf1-45e6-48cf-a152-17e43fb4d220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 236,
                "y": 152
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0315ddac-045b-4af4-a969-c45783dee239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "599dd636-9572-457c-b49c-fd633471ea12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 14,
                "y": 177
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "148fb759-4d2d-4143-9f93-6fdfd27dc887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 25,
                "y": 177
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1d6430c7-b841-4fee-9a99-520a090c20b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 38,
                "y": 177
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "27f77270-9bd7-430d-88a6-a9a9833f8034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 49,
                "y": 177
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e764074c-0674-45c4-9696-55d538f6057d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 62,
                "y": 177
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "42d3f6b6-0278-4a08-9fb2-3e984f877292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 75,
                "y": 177
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2178a980-1f31-48c6-840b-f6b1643123e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 177
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "62161dbc-fb77-4df9-ac27-f73cecee4907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 152
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5770d5ed-a76a-4c15-8afe-8019772bcd66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 23,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 126,
                "y": 152
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b8c8b150-bf49-498d-b7fe-f51438495cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 115,
                "y": 152
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "915cf1a8-9965-490f-a9f3-971c5ee838f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "980f5ab7-b994-45e1-a3e0-c48fa4caf63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 178,
                "y": 127
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c14693fb-5ea5-40ea-96d2-bbdc3225e4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 189,
                "y": 127
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d88380f9-16dd-407c-9211-68d0bab3f82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 202,
                "y": 127
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4f47138d-8b31-4b06-a042-e276c059bd0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 211,
                "y": 127
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f920a9c6-0f22-4fa4-873a-2a52f37170ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 222,
                "y": 127
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "88e78362-17a4-4918-840d-179e17608231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 233,
                "y": 127
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e981744d-42fc-4802-8f43-a19353c49b33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 127
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7d53ccf9-5d4a-4d4c-b39e-9e66f1bfb70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 10,
                "y": 152
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a6db5f91-b48c-47ed-94ea-feee01f53f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 102,
                "y": 152
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a2fd6ecb-f8b0-48e8-9f11-b290e8c3c1ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 21,
                "y": 152
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "49f2f154-d4a3-41c4-aa8b-0f03a639c099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 34,
                "y": 152
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b3a48e75-f48d-49df-ad08-0e8efe1491b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 45,
                "y": 152
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ca20fc5e-4232-427a-b3de-c498a1075844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 56,
                "y": 152
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fb88a0cf-9a59-405c-afae-682c3debe2b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 66,
                "y": 152
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "487ded94-2347-4cb9-a999-b7845817ccd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 78,
                "y": 152
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "49009637-08d3-44f7-b473-e664b268aa82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 89,
                "y": 152
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3189d87b-11df-4457-9914-84816c35ee23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 240,
                "y": 77
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7ece3e94-49ec-4abf-944d-efdd0503d3f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 101,
                "y": 177
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d7996fd4-26ec-46d0-9043-89f501ad8121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 229,
                "y": 77
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2ccefee3-73dc-45d3-b11c-c93b9ca9f0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 206,
                "y": 77
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6082283e-e0bd-4ab9-9b40-b19e246fa897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 13,
                "y": 27
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d7a4f1a1-4249-42a2-a052-da1c8c97e054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 25,
                "y": 27
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d86e78b2-7ed3-41de-b80e-28897230c41c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 27
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3449bd81-1a04-472e-aa37-48b1dc85022c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 48,
                "y": 27
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0ce79ec1-6d71-4eb7-9d40-38a1183209c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 59,
                "y": 27
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e9f2cae1-e880-46dc-a567-6e145c680a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 72,
                "y": 27
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f2987bc4-5ef7-45d8-bc3a-00b3efa367a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 85,
                "y": 27
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a25d4f33-cd38-47e5-b804-5b4df6cb2d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 108,
                "y": 27
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "59085697-20b5-467e-8ec6-cbbe87aa152a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 196,
                "y": 27
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "aa6387ba-4cc1-470f-ad62-4cec67e6d74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 121,
                "y": 27
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "19a8fc28-dab9-4dbc-91ff-bbef9b5c4948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 23,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 131,
                "y": 27
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "72c2c73e-ae0d-4cc2-b0c3-8f8c5c9eaf66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 136,
                "y": 27
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "65219035-b3c1-428f-93ce-788ff73ff54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 146,
                "y": 27
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "9fc4c9e5-76b6-4ea5-81d3-262744ce6ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 159,
                "y": 27
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "6452ca66-720f-4e0f-a707-42d2000dae09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 171,
                "y": 27
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "1d1bedfb-fdc6-40bc-825a-cdd983512107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 184,
                "y": 27
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "e95a5527-0fdf-4370-bdb1-f54e342ef2e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 27
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "c7649359-9f3e-45ee-80b5-ee6e27d93b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 98,
                "y": 27
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "d2e331d1-7e33-42ab-bd09-c5d72bb906e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "bdb31667-547e-4423-8f08-9a21dcf3b959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "39ae4c80-564c-4a14-8104-d90b06460f85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "b1bab217-660f-4ba5-b8aa-4215a0bea7fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "42be87f3-cf05-407a-8df0-c572b883ea23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "e911593c-3274-4d44-b736-5a3687edc3f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "23f7eddb-0e2c-4f97-b045-01f7b074af6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "f773423a-7795-4331-a531-cd9c003f8f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "c9f42c40-1dc4-4ca5-9c3b-1e4f3a4a2cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "12953223-8a3f-448a-b3d9-9c0f2373a5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "ee5e1373-1a11-48ef-bb46-06f397b2b12c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "68ef4469-2bfa-429d-93cd-4348fcc3188d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "ece79c8c-59b8-47cc-b368-b1a4e5de6447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "ed788eb8-bf43-42ae-bc63-a799696486ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "245822c0-901d-41b5-bdda-878890f6f556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "ca099ee5-d131-4144-9229-b0801c4fa249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "d12a64e5-9d66-4604-9399-83ba77c7737f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "7a789099-b1b7-44e2-8c22-d9ced0bf14ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "0e18c318-fb69-489d-81ee-b129a909144e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "41b8ce82-2b02-49d0-a2e7-d48d0c142ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 207,
                "y": 27
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "ce0d5b28-6944-4c74-a10a-8e6be6e7cef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 219,
                "y": 27
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "96df3231-fd52-4e37-8ed7-03a7bb3f6248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 232,
                "y": 27
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "283e0a0f-b4e5-4d09-b849-297b4f114699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 77
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "2dcf8a1c-ccbb-4414-905e-4a76859e7cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 77
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "c72fda76-95e4-45cf-a7e0-4883b65c40e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 28,
                "y": 77
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "b1730dec-6e3b-4d26-9c4c-4d846e7074c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 40,
                "y": 77
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "d97388c7-42df-46bc-a47b-9ad457b527f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 52,
                "y": 77
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "e0d90559-f232-4725-9225-aa1967210462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 65,
                "y": 77
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "4570a163-b6db-4272-961d-35ad3c6d52b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 77,
                "y": 77
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "cc0e6c1a-c025-487e-aad5-398db09b1f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 88,
                "y": 77
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "b101b09d-4751-47ed-a6dc-ba4dd94af49b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 100,
                "y": 77
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "2314e0e1-8e05-4e0a-ac99-39d4ac4e8b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 23,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 111,
                "y": 77
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "2d9c1c29-c761-4bf3-b702-a532e4615e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 121,
                "y": 77
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "4b5b34e3-683c-4a03-80cd-89df8d8b0561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 134,
                "y": 77
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "93600208-a0fe-4d79-a0fe-6c3ac0851f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 23,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 145,
                "y": 77
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "bcfa2bb2-3a60-444d-8763-8f8873e70804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 160,
                "y": 77
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "738b9592-2e5e-4b58-bd30-e40056f3ff95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 171,
                "y": 77
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "e868b590-8e04-411a-9dde-c63f8c8521b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 182,
                "y": 77
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "73ec9ec3-1a1f-468d-b0d4-4c9ad2f9c551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 77
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "2f3fc4c7-1909-4f54-8cd9-6f9120b8d0f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 234,
                "y": 52
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "2a9dc935-2f93-4709-8eee-896d5aac6c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 221,
                "y": 52
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "a156abff-20de-453c-879d-950617bfcd4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 210,
                "y": 52
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "6af09223-00ae-4100-af94-7f75a2ea84da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 87,
                "y": 52
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "adeda52e-26af-48a0-9025-29782a6f5374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "3c76a3be-7194-49df-bc8f-75c7374bc200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 52
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "0f1f25e5-d94c-4d5b-9593-adaa0452edff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 52
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "a1793f26-3987-4973-b87e-af569b89db88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 35,
                "y": 52
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "e03f3b6a-f4b5-4838-8e1e-734bc22f5a91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 48,
                "y": 52
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "7c2df050-a6c1-47d5-b3f0-cc387b8d62c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 61,
                "y": 52
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "36f1d410-517e-4c45-90f0-56b4a95c9723",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 74,
                "y": 52
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "b2da6a53-4559-4bbf-a0b3-b926c1cbb208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 100,
                "y": 52
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "109b08fa-17ec-4dac-a010-eec2d41b3b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 52
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "f4f397d7-43af-4f78-a560-45a7b8a42bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 112,
                "y": 52
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "857d9ef6-2baf-43b7-a7b2-3997592da58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 52
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "1983bb39-c909-4f52-924b-410b5871096d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 138,
                "y": 52
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "52f4b5f5-5649-4017-b208-2a52691f4669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 151,
                "y": 52
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "c7c9081b-17a9-4556-b240-60b27ec1fce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 164,
                "y": 52
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "24c3d78b-ae43-4c3c-890c-4397d120110d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 23,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 175,
                "y": 52
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "a7d978c5-3799-4a76-9a5c-3610affcaed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 186,
                "y": 52
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "2b6b0392-f919-45f3-85e9-4606eb3d0ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 217,
                "y": 77
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "e98330ec-1f15-46c5-9ef3-2500fabc0c8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 23,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 114,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}