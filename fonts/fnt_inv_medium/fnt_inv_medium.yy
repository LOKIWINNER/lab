{
    "id": "856bd2fd-204c-4234-92eb-a9eab2899cbb",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_medium",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5007c511-0979-4b8c-b4ae-94d7f3ce8997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "272b3392-a628-4ad1-9996-0b5b0ae65c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 124,
                "y": 106
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c6d8539a-e948-44bd-9b42-be8cd944dd8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 128,
                "y": 106
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6bbfd1a6-2e71-4054-a2b4-dbb94dcccaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 135,
                "y": 106
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "556ac61c-41b3-4b3f-8213-b6a9fe457572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 151,
                "y": 106
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a83a6bf-5be6-4a33-a5e4-454d790298de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 162,
                "y": 106
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9574768b-dcc8-427b-867e-6880f940a335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 176,
                "y": 106
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ec850adb-3b93-40d2-b9aa-873d18bd7a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 2,
                "shift": 7,
                "w": 2,
                "x": 188,
                "y": 106
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "6cdc7102-1b1a-46a1-a4b9-7c1588967226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 202,
                "y": 106
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "dbb7ded9-1143-4e5c-a6b6-65cd776acd6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 13,
                "y": 132
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f3f40a4a-6a40-406c-b8c9-4fbb539f003d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 209,
                "y": 106
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "85ef980a-ea9c-49d8-944d-eb1584ee1b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 219,
                "y": 106
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f869af14-4b3e-4286-af70-0047602efb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 229,
                "y": 106
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5b78145a-31f1-4e29-bcdd-6b497d257a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 233,
                "y": 106
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "105ade43-0f1d-45cd-b6ec-7d05db0a6ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 240,
                "y": 106
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9e37662b-0cbf-44de-af2d-6b62b34db4ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 244,
                "y": 106
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "40dcf5c3-7c63-459b-a03b-2ef0691388e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0f67eec7-d01b-4c86-a332-86a5b6704da4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 106
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b97dc1cd-4a77-4b16-a3b7-e0ae1dbe2398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 192,
                "y": 106
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f5e3fe9e-d94e-497b-a710-e3b650e45ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 106
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bbe7b490-6292-4ecc-afca-75fbf392d395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fe899e06-86d5-4f39-b5e0-e3c80c5aeb06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 182,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fa8d54dc-d423-4730-9ca9-1f4ac4f856d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 193,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ea7693b7-61ce-466e-baab-a7c69fefb9b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 203,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "04e27223-ed15-43e6-9bff-236a429c898d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 214,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a16dd124-877c-4b16-a0b6-551fc42f1f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 224,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f5f6909b-4535-48cc-9c48-b598bca273e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 2,
                "shift": 5,
                "w": 1,
                "x": 235,
                "y": 80
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5d4edb42-08ea-4933-8b39-4a4ca3ed156d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 238,
                "y": 80
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0904c4a4-af69-49d8-8fa6-895b4a92d82e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e655b7da-ebe8-4c5f-b841-24b38fb09bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 87,
                "y": 106
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9b703c23-a00b-443c-b1a7-379aadbe9dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 9,
                "y": 106
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1d5b2bfb-c390-4aa8-9301-951ca9ee531f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 17,
                "y": 106
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fc3c9479-86b7-4817-95aa-719c94b7dec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 27,
                "y": 106
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fc5d954d-0204-4fa3-9335-afb41b079028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 43,
                "y": 106
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f2c09d4f-6810-4083-9737-8f48f580cbc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 55,
                "y": 106
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cf2bb0c9-1b91-4c95-a03f-9ce00ab2e951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 65,
                "y": 106
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "df0d3be3-318f-47a7-812d-f5c7d8d19ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 76,
                "y": 106
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "3214e4b1-c8ce-44b5-82e7-9083f010b777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 95,
                "y": 106
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4b740dc1-8dc3-4737-9c13-c112de9aeb2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 20,
                "y": 132
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "652de101-c2c8-4020-b849-c9676dde602e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 31,
                "y": 132
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a14d7e12-3a7e-4829-b007-764601d64692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 43,
                "y": 132
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c6ce08af-16c1-49cf-a26a-34a5c351f953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 236,
                "y": 132
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "366dd765-9073-4232-9757-8c76339ed6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "96724c5f-9ccf-4db1-a4b2-c40b3ffa3f0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e5221a0a-cec1-4145-a0f3-bc34bd4fc2bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 24,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "091e1821-306e-47d7-a0d9-50cd513c0ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 34,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "397ecf15-60ab-429f-90d8-2f652ed06c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 49,
                "y": 158
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "63af0fd1-afd3-46be-920d-c35ac91c0527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 63,
                "y": 158
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e24f1361-b7f3-4d8f-8cb5-ae8efca19bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 158
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f3cc61cd-6d84-4cc9-bb13-99f41d45c0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 86,
                "y": 158
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf4f36cb-03b6-4b81-ba8a-e7782fd5d967",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 102,
                "y": 158
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a1c7f5d2-c029-4264-b854-c41fae7ec2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 113,
                "y": 158
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "6f7ceb7e-4582-4447-b87f-d7069a05623a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 125,
                "y": 158
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f5d990d1-f83a-41bf-88fa-97cd13b2b14b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 138,
                "y": 158
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b01d5eff-bf7e-43d6-ab57-1fc117aca3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 151,
                "y": 158
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "015e4a41-5df4-4a92-a7a8-59dfb05f092d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 163,
                "y": 158
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "48769947-8a64-4d62-a2a2-55b8959d2c35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 181,
                "y": 158
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "87a61de4-21f0-4568-a641-3cf455805fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 158
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b1637515-914a-41c2-9580-ff8eb214c2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 224,
                "y": 132
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "137a3ae8-c629-45ac-8e26-1a158045ae1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 217,
                "y": 132
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3f2b7819-34f1-498e-a142-355fdc09355f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 208,
                "y": 132
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "fe3b0b7a-740b-4d1a-84b4-b5d4c78efdd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 122,
                "y": 132
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d245d69f-0935-4d45-8697-6053a43cdd4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 56,
                "y": 132
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5257bed4-7f6a-4355-b38e-7d25c45ee2a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 64,
                "y": 132
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b7f5521a-2c5d-45e3-bc4c-7e6bd623cd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 77,
                "y": 132
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5089d49a-9c72-422d-adff-3056db3b9087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 83,
                "y": 132
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dc709249-72e0-4a67-bd16-6ecceacdf286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 93,
                "y": 132
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5f3497eb-3668-4a41-8d04-ca6bc175fb44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 103,
                "y": 132
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f37b52ac-1dde-4028-8a38-801027cd37f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 132
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "db17da16-c44e-4a1b-81be-43a06c37fe6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 132
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5dcfcea2-b44b-4624-b074-b6c80ac80e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 199,
                "y": 132
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "190ce544-0091-43da-a8b0-e9ad6495e490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 132
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bccbd8d2-3511-4e58-adea-748a783af28c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 149,
                "y": 132
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "466dc18e-fd16-49bf-a979-b918ab619291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 159,
                "y": 132
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5aba96f9-d4ce-4eca-8254-1a65d5b65cbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 164,
                "y": 132
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "37d10b2a-03a9-4429-8ed6-3e04fb18db08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 171,
                "y": 132
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "20a2958e-e482-4049-ab80-21eabcd493c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 181,
                "y": 132
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0e97626f-cb59-4777-8f41-935bb106d37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 185,
                "y": 132
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "154bc831-6de0-4159-af76-75267a0b966f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 173,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "894de6a1-29b0-484b-8ba6-1b31d87af797",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 206,
                "y": 158
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b1607f70-71ac-48f8-836c-3f06f56aa36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 164,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bb18dda0-4172-4de3-b867-91c398212aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 145,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4e5d9d89-2407-48d4-b2ba-0c5434ca495f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 28
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a0f8a937-0038-4f2a-a572-732e1b0f2616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c5280acc-8245-4d19-b2eb-68d417f8256d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 31,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0efff293-8492-41fe-ae6a-d21c634824bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 40,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "58182012-5822-4c42-9470-789c13a8aff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 49,
                "y": 28
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "020e3dcb-60b8-4c83-a4fb-388859a38171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 58,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "52b32bd6-7bf6-4521-8b48-24e7bdce4dad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 70,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f942d413-47c6-4a27-9428-eb423f979ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9ae6e215-29ef-4520-8f41-904b167eca92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 164,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "9f618ed5-55c7-4811-abcb-55a95d9cc869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 102,
                "y": 28
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "1088088a-fa2b-486b-87a2-42ab33456759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 3,
                "shift": 7,
                "w": 1,
                "x": 110,
                "y": 28
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d0de12dc-5dde-4df5-b99c-a2ca143d46d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 113,
                "y": 28
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e97291a9-955f-48ba-a070-8a47fb90aeb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 121,
                "y": 28
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "2099b6f9-ae0a-469a-ba7d-d83512c4e0c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 131,
                "y": 28
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "7f0d92ed-3aac-4bd1-aba1-069e74a53f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 142,
                "y": 28
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "742559cd-a909-412e-9f48-366b11380823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 154,
                "y": 28
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "c4ea9e4e-684d-4b8b-ba02-d7b7bc065c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 24,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "dc4b3dc3-3297-46aa-88ba-14c6197e381c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 24,
                "offset": 2,
                "shift": 10,
                "w": 8,
                "x": 81,
                "y": 28
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "a1afbe4d-b38b-42e8-aebb-939401278c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "e3a96ebb-82e5-4785-bfcd-8090eaed28d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "2a636006-3241-4d51-b6d0-c90aab641c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "f0ce3c15-0813-44fb-9489-62d8381ba2d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "fedb34a2-1464-4d73-b8cc-81ac91ea5f76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "dd71b86c-90c7-4dc5-b62d-3c374e904597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "c8061194-7642-4b8a-ac17-6bb32d11e1ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "8b751180-4477-4f8d-85c1-9acfc87caf90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "6dc0ec14-e5b6-48be-b82d-9bacd8281a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "5af114e4-0707-45c0-a1fe-87294020915c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "22ea296d-ee09-4260-aed3-2d6b78ab7177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "a58eb7c6-8fb8-432d-9431-7006672a7075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "8f5cc53a-88da-4847-9b16-caf88e78dccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "f684d970-21fa-474a-a4a5-65b2d5045162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "6d3ef388-60a8-4ca0-9d93-355dba114705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "52216001-2215-420d-80c1-16a8e6aa3dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "c8f6dd67-3a40-4686-80c9-71ceeaa279fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "5d4eab5b-9689-442a-ae0f-10e723600f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "a09c4b19-c55d-4ac6-8d9f-8961cb4bc6a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "49eba81e-8a01-4034-95c6-517930df5c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 174,
                "y": 28
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "5330589c-a475-4423-a71f-152cecb4b3db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 184,
                "y": 28
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "f4caeeca-e62b-4ff3-8931-ef6067d20856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 24,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 199,
                "y": 28
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "eead1ae2-f27c-4ecf-b651-9cf4169cb552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 192,
                "y": 54
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "a655bf67-8f2d-4db8-a612-a070604499db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 24,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 206,
                "y": 54
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "fc75d024-aa83-435d-a200-9232c159c582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 225,
                "y": 54
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "8fc75738-ab48-43e9-89d7-2b8037cb05d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 235,
                "y": 54
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "2b46ddfc-9c95-4fe0-a200-fcb1c672a86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 24,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "ef8eac80-96a2-4813-83ef-89b39cf7cde5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 22,
                "y": 80
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "a106352f-2851-4b4e-ad27-ca54c72864c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 34,
                "y": 80
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "a4acfd5f-5348-44ac-8653-75682948fbf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 80
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "2a3a6101-65b9-49c0-9cf4-c1cd2f4da605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 54,
                "y": 80
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "b09f0b8c-f81d-4eba-93e1-6e0a7c4b8b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 63,
                "y": 80
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "905f5b7e-2178-41b9-99aa-fd81ed6af0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 80
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "51bbfeab-7b01-41ae-81f2-435a66758efe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 84,
                "y": 80
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "a65fe501-ee34-4662-a679-8848e8ff6942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 24,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 94,
                "y": 80
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "3d9a7259-9659-4976-bef0-0b28fbeaf880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 24,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 80
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "b962f94b-a59b-4f6b-b7d9-956b4599acc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 114,
                "y": 80
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "f1b0d08a-a904-4a4f-b441-7a45043dbe89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 125,
                "y": 80
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "1f7131d2-eccf-4287-978e-94d077011eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 136,
                "y": 80
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "c8d88946-0372-41bf-b362-d922ec8582ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 180,
                "y": 54
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "35239cf2-3378-4e70-984c-9100457130e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 166,
                "y": 54
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "8f9e3648-ab78-4b59-a214-a2cdcaa62b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 156,
                "y": 54
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "44fa9cbc-c8c2-48fd-bd92-44331c95b3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 38,
                "y": 54
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "7e83da97-744c-4020-a77c-f7f773754cf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 24,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 216,
                "y": 28
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "d0c43b2d-3400-4d20-9502-a1035492d1bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 226,
                "y": 28
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "4e067e6e-f95e-417c-b485-4a6b81c1547c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 235,
                "y": 28
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "e28661f0-3a20-42fa-8b12-c415b357fba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 244,
                "y": 28
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "8315b774-fd29-42aa-9e3f-4f4c547206d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "9f60f143-665b-4dbd-b646-7ec9071fd8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 13,
                "y": 54
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "b4fd08bc-af8a-4279-bc13-cf54594d7222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 24,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 27,
                "y": 54
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "6e92b4c5-849c-4424-85e1-00f88e76fdcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 24,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 49,
                "y": 54
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "8c12cd78-144d-4aee-83e1-6e74dbf338f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 147,
                "y": 54
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "b3ba5e6b-3dd6-4ef1-9cea-d011e08eb5fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 60,
                "y": 54
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "2d5ef4e5-a8cf-4810-a258-a2b41d0e8a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 73,
                "y": 54
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "8c2762d6-f673-4dd9-8758-390c94623c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 24,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 87,
                "y": 54
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "640981de-2789-4151-8ae4-16a3af50f16a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 24,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 99,
                "y": 54
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "d2846634-1dae-44a3-b636-4e6fd16a441a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 24,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 113,
                "y": 54
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "df777f1e-b4f9-40b2-976f-f9c0f2d4acff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "c3bba83c-aff0-4d3e-8fd9-c60c6093c7c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 24,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 131,
                "y": 54
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "bef8cf25-2c55-4fd4-bfe2-034295819634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 154,
                "y": 80
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "b76461a1-520e-4d2c-9f47-54a9ce028b96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 24,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 215,
                "y": 158
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 13,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}