{
    "id": "b76a3629-9b1c-49a4-9a8c-bae3863f5270",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_big",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "13165eae-c236-466b-90bb-44d65e68b4db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "988710e2-b709-448d-957d-ac8b5f8d9d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 148,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ba3a6058-842b-4e6d-b4b6-0a7da218ba3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 152,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "34802a52-738d-4790-aecb-2d7bcc00f305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 161,
                "y": 119
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "80f6248c-0c4d-4140-adca-1113e6b87b5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 185,
                "y": 119
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a9926b5-1bc9-4a7b-b765-f2363a348887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 202,
                "y": 119
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6c1eafb8-0679-4285-97ef-77755b0027bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 223,
                "y": 119
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "370ab825-3169-4618-ae43-05d41f5c54b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 241,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2ea5a7da-4e40-496a-94ba-faed8ba5bd48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 260,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5053e705-2b98-492e-952a-a68f74a5a119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 349,
                "y": 119
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1024cf30-4302-4e81-9b1d-d14893ca1d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 269,
                "y": 119
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bd2e4365-413c-4511-a307-2c930d74f2a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 283,
                "y": 119
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "63fe17d4-d953-4517-b346-1484b4551d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 3,
                "shift": 7,
                "w": 4,
                "x": 296,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ac83bf99-66db-475c-a4f4-7d659a720107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 302,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4b56dd35-b42e-4247-a0d6-22a2fd72867d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 313,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0b76deca-0b3d-4299-b8c2-d5f92f7bd9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 318,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b7bf3f5f-5e9a-422f-83eb-b58b26fe6624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 332,
                "y": 119
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "028e3970-db92-4ca2-9c78-69d0cb0c6053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 137,
                "y": 119
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ee58f4dc-386f-42b8-99b7-3402f176f8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 245,
                "y": 119
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "10b8fa19-20b5-4f80-a1e1-873e71d71271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 123,
                "y": 119
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e452d267-9594-4245-a89c-4256159bd588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 450,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "add4e04d-8bf0-4c3c-b1a7-0b7b01ab7d0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 360,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "182d1f1d-172c-452a-b4a9-79431792d343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 375,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ed7f1710-291d-4f79-8bf9-b4805ed730bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 391,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "33470b11-bb5f-4005-9b64-97c7406a4d94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 408,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f2d7166e-e47b-4714-9756-ea3acedf57b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 423,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "90680d68-21f8-4e27-afc8-6d58c71864d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 439,
                "y": 80
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b2fdd624-e1b7-46bb-80c4-6bec1f4eaa39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 444,
                "y": 80
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dfbf0cea-ab9f-4ee8-85df-5cea20766d7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 467,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "8bcd15bb-7d78-402a-9995-17364d9ae592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 94,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5e5ab74c-6239-41cc-a0bd-508995042649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 477,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9ba6ebed-5a57-49bf-a73c-022b9cdba7ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 488,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "53f399ac-0817-4f14-ba8f-802bdc55d75b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2c429518-45cb-45df-818a-a139c7ddfcdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 26,
                "y": 119
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3600a5f4-b28e-4d8d-8892-6d018a1897eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 44,
                "y": 119
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "29a248a0-a691-4cf4-8f63-8f2dac3d1383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 59,
                "y": 119
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bafed535-16da-437b-86fb-3d8eaf39910f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 119
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5fac3fe9-7a26-4e5a-bb45-9d8d84c20777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 107,
                "y": 119
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f64d5201-477b-4ffd-8074-dea8ba3a136f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 358,
                "y": 119
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0a2cfcc0-7ef5-4d80-b4d5-6f0d4c213889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 374,
                "y": 119
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a9139a92-797e-4ccf-89b4-cbef6032e5a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 393,
                "y": 119
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7082bda3-e99c-4d63-817a-0fbfa90522c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 164,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c3196a8e-40f5-43a5-ae0a-985ac1de40fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 179,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3cb40b7e-6a87-4bc4-b9a0-97aa7b13092d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 197,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "210b6dd9-f073-44a0-8505-c5eed2a83f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "071b94e7-49a0-406a-bb3e-56af926a6bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 227,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "457d3595-4720-4f5a-a4db-292ccd21eb24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 251,
                "y": 158
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ab6f5f97-b31f-42cd-8aae-21da6b618454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 271,
                "y": 158
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ec8f6f3a-90bf-4afd-9d14-f479c5f6f205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 291,
                "y": 158
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "959e145f-8c49-4bab-8262-cbe9291a100b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 305,
                "y": 158
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bd48946c-1926-4f04-9b5a-34afba1bf4e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 329,
                "y": 158
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "986de94b-d5ac-4976-a713-dacbb4d19567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 345,
                "y": 158
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2c1a875b-6467-4e6c-8376-85bf770bf852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 362,
                "y": 158
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4b325ddc-5f56-4537-98cb-762c780ef7e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 381,
                "y": 158
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7372b66e-2f23-4ee7-8ca7-fd337d3d73c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 399,
                "y": 158
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b481f9ba-7d6e-426c-a944-ca1d0c400e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 2,
                "shift": 28,
                "w": 26,
                "x": 416,
                "y": 158
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "407b8ce2-4ae2-4558-be9e-9604dbba8a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 444,
                "y": 158
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7b7a0959-708b-419b-ab0f-bc083be8aadb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 464,
                "y": 158
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2d31aca0-1f1d-4ff8-b7d5-8c54306ad963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 145,
                "y": 158
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2c15788f-d8a2-4272-9c21-9dc86aa9934d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 136,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "37e225d4-3384-423e-9584-890d20b85c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 123,
                "y": 158
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7b528b24-cdf8-4cd8-b955-a668086ed139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a199ba16-2f8f-4680-85a8-b726194fd868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 412,
                "y": 119
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "582dd9d1-d202-4ba7-bfcd-e7c3edfce71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 424,
                "y": 119
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9dbd493d-5842-40c9-b25a-7a78dae0d0d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 6,
                "x": 443,
                "y": 119
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f0c50dec-24ee-44bc-a322-617f8560a1f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 451,
                "y": 119
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5e04ed8d-a3a6-4241-b36b-40689dedac63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 465,
                "y": 119
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8183f0f0-21d3-4c02-8077-7be40ab6303a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 480,
                "y": 119
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "58b684f5-3f0e-4ead-b679-fb6515a4b334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 494,
                "y": 119
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6693afab-1a31-497a-b16c-56804ecd3b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 11,
                "y": 158
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "47f6e347-d7c9-49ab-b0a6-502dfa99cc2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 110,
                "y": 158
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a324d126-fd49-4b54-b890-1a171ba33d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 26,
                "y": 158
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a0f03687-ae65-4d39-b2a0-762ba8063061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 40,
                "y": 158
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bfcb19db-5ab0-4b1f-a9bd-8321db16d620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 54,
                "y": 158
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d96b0986-0050-4387-b67b-a4ed27ad9358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 60,
                "y": 158
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "88b49254-700e-4b04-a750-27a767990138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 71,
                "y": 158
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d9d1e4ca-40d8-408e-87e9-42c069454ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 85,
                "y": 158
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b40e1a92-6928-40e7-b7d6-63ceddc60f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 90,
                "y": 158
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "90238661-ff22-4aea-acc1-406fc2bdeec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 347,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "047079c2-a7c4-418e-9066-a8b551330650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 482,
                "y": 158
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "63c194c2-40af-42ec-83da-c413f197c1a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 334,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7caefe59-624c-4240-9ce6-85eaa11214e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 307,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "096d1987-d9fc-4b09-b22c-ee76663a53fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7798b9fd-12bd-4c1b-8026-ea8362387603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 402,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d99a7df9-c20a-4236-89cc-271b6335393c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ab447cda-d2be-4178-9f3f-75ac51e8a0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9286d48e-a71b-4ca6-8922-708d412cd463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9d4713f0-6de4-4e2d-816b-21cabb8ce8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "81d4b307-cf9c-4209-9cf9-f7a9df6ec13a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2b9e2ea1-2033-43d0-b0d1-caf190fb3473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8381ad56-2831-440c-8ff0-7a72d2f031cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 109,
                "y": 41
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5a883015-6016-4b7f-9f4c-8a907eba93fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 18,
                "y": 41
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c12b2ce8-0b6e-4a4a-a0fc-24247904fabb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 5,
                "shift": 11,
                "w": 2,
                "x": 29,
                "y": 41
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2b28af64-1567-40b1-931c-b756aa2afd3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 41
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a2e517b4-9754-4d5b-8f47-11ea72572533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 44,
                "y": 41
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "fbfcba16-3a2e-4463-9d5d-06e0492998bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 60,
                "y": 41
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "d57efec9-27af-49a4-8abb-2da43e3022a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 76,
                "y": 41
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "03b8c78f-99b7-4d2e-8fa1-6b64a3e54e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 94,
                "y": 41
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "8ccfb81c-3036-4de6-9a5b-8b6bef6c11df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "98f2862e-41b9-41e2-83b2-532616df1f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "e49c24ed-adf7-4cbe-bb16-50c9a522a743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "5b05621c-0bcb-40e7-831b-971fc18ff783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "bb5fa0df-6b88-4724-a0f7-10da796a5b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "d8f4283e-bf8d-4ec6-9603-d39c5dbdc1d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "b57cd5ae-af02-4e0b-8918-0aa5c0165d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "52d3dc35-285b-4594-966a-c77a12bb6483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "89c85fa5-e214-4ae3-a5fc-8c728b07acd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "2f6287be-9de8-4681-84eb-537976efb16a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "1a4a7e1b-9b17-4498-b96a-51c203e3177f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "a69ed87b-53b7-4c2c-81c9-9763edd38637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "4e3309be-02bf-457d-9a73-605bb04a91fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "8141e50f-e474-4dd2-af8d-89d34b774115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "2105c562-f917-4b82-9538-e822529e3ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "ec3f157a-c51c-4296-9e29-81ac9aa1f19e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "068e2542-405f-4bcd-a3f8-fb608b556504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "c3fb21e2-0231-4172-a7f9-6693c70699b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "afe8df35-7bd4-464e-bb15-0c2cb1aaa04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "49340981-43f9-4bbd-8500-dc2984c1aaae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "c6d1aa0e-918e-4fb2-baf2-6cdeabbb10f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "3b3697af-46a0-489b-888e-5a472d2ef589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 123,
                "y": 41
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "cea39c59-2cb4-4100-b02d-048c941d64eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 139,
                "y": 41
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "2c380130-9765-4ac9-8149-81db400cca65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 37,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 163,
                "y": 41
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "c99170bc-1d5b-410b-8d70-7f836c6fd9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 20,
                "y": 80
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "011f657f-23c2-43ba-a657-3618e1cb6758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 37,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 40,
                "y": 80
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "505907ef-e94c-4a5e-8ce2-14791da1fafc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "07f16f63-c139-4103-be7c-c9b93e0e4eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 83,
                "y": 80
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "8f68e42c-43cb-4ca8-92ac-eee0ee3f9225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 37,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 100,
                "y": 80
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "e6fbec7f-6718-410f-8119-c9f1781af3f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 80
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "b8e35299-fb7e-4cec-a455-532575483d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 147,
                "y": 80
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "2ae6c700-5789-4e53-b5eb-0d516d55dc42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 161,
                "y": 80
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "3dff0862-4386-4c2f-9872-071211cfd2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 176,
                "y": 80
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "28ca33d8-cbc6-4036-aed7-d792fda7875c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 189,
                "y": 80
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "4e792419-ac47-461e-a668-9b830da54071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 201,
                "y": 80
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "06f1a336-e147-4e4e-87d7-80bdef665113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 218,
                "y": 80
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "ccf7c673-c4bc-4dbe-a949-a61950079960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 233,
                "y": 80
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "17eed99e-5f54-4f8f-b27b-b7e3546b1272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 251,
                "y": 80
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "8990ec3e-2497-4a53-b830-d4004f84d963",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 262,
                "y": 80
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "fb9faf0a-f0bd-44ac-9dc6-15b80b46f595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 278,
                "y": 80
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "706a440b-4792-4d6b-99a0-f0ab1411e819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 294,
                "y": 80
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "e8b5ce14-e2aa-4a47-b612-efe5cd809540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "7ebb3d04-5f01-4324-9318-2cde171360ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 41
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "19bedbfc-d5a9-4a3d-a300-3e001c719b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 475,
                "y": 41
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "96915932-37c0-48c0-9e80-0a983ef8f7ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 298,
                "y": 41
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "f06f5603-a4a2-40a5-8475-dafcd0513633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 189,
                "y": 41
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "11a7d972-9e0d-4006-a4ea-7ea2610988ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 204,
                "y": 41
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "697de418-4767-45db-9116-6a50ab3dd7ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 217,
                "y": 41
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "3a7e32cf-4a04-4597-a27a-1aaead22c614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 231,
                "y": 41
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "10538a6a-8f09-42d7-a45e-81100f3e93be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 244,
                "y": 41
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "5dbc9e77-df6b-4b96-94d8-05fd2327ffad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 260,
                "y": 41
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "da82bb79-1b21-4f11-9a8c-87565cb5dd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 282,
                "y": 41
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "1a4dec8b-c617-47ab-ada8-332e407f7210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 314,
                "y": 41
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "bdf45c52-3529-4cd5-b9e1-1b59cd449a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 461,
                "y": 41
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "b1186d7b-4129-4fd4-ba59-bbd2cf8d441d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 330,
                "y": 41
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "4c5c5eae-a3b6-45b0-a7b4-1fdeea3760d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 350,
                "y": 41
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "665f1bcb-8376-4727-9224-43083cfa65e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 372,
                "y": 41
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "ffb516e9-6f0c-4d6b-9af8-e79d01499f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 390,
                "y": 41
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "b4e1da6b-19a0-4d6e-ae13-1e1d6e04f2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 411,
                "y": 41
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "0958a7b7-0366-4c72-b21f-f724e99f64c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 423,
                "y": 41
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "b43d44de-0a4d-44f7-8029-8ad052ba44b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 437,
                "y": 41
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "29b4ea3b-38b6-4650-be99-951409596ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 320,
                "y": 80
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "96567ced-7fa9-4457-8317-11c07fdb8d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 496,
                "y": 158
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}