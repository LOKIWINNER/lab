{
    "id": "856bd2fd-204c-4234-92eb-a9eab2899cbb",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_inv_medium",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "fb1d72bf-ce8d-4a98-b3d4-f78d100bbe80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d0ebb327-db30-4e5c-9930-8412a3461c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 148,
                "y": 119
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "30ab3ad9-5846-4426-8f55-737dddbae600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 152,
                "y": 119
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1ed20806-ab90-4b1e-ac1d-f8a92c3d4a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 161,
                "y": 119
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7d10b894-deaa-4c45-a11f-9fd3c607c5e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 185,
                "y": 119
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9fec8652-3f4e-4790-ad49-c2ff1b1c0fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 202,
                "y": 119
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fa238f59-1adb-422d-8a31-b548153917a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 223,
                "y": 119
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ab2cb57c-1d42-4042-9955-52857561c17d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 241,
                "y": 119
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d6b558e3-7859-4354-8a4a-867fd9419770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 260,
                "y": 119
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b0ecb897-8c72-437d-9e27-5a1b0e797677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 349,
                "y": 119
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3b6af4d4-50db-4276-8cb6-13092fdb6c02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 269,
                "y": 119
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2a28f075-ab12-4a7b-ae30-acf4bac5115e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 283,
                "y": 119
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9f8c384a-6cc6-4d46-a1b9-4459bd4ccaa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 3,
                "shift": 7,
                "w": 4,
                "x": 296,
                "y": 119
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8ee3c337-55d6-4b78-a1c5-83b7c6467481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 302,
                "y": 119
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a37a8a89-afb8-4712-85ee-a0cb2ceb149b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 313,
                "y": 119
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "08a9d123-a533-4580-b811-88130043a30f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 318,
                "y": 119
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "22683085-4492-40dd-9cc9-dd8875118cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 332,
                "y": 119
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6c86386f-5b32-49f9-833e-3839c34819e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 137,
                "y": 119
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1a2535ce-9b94-450a-a9d1-92751cc5e042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 245,
                "y": 119
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b6da5cc2-feb6-44e1-857a-bc0b31688dac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 123,
                "y": 119
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0798e170-6ed7-48ab-9916-2eb4e8991266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 450,
                "y": 80
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "afb5d56c-eb26-4df4-965b-a1e2b756004c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 360,
                "y": 80
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "54cf0727-bc40-4d8c-bbe8-1472f09acbc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 375,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5f8f6ff6-4dc7-41bc-b1b4-50e86f7b1452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 391,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3a490419-6235-41e6-82f6-3efb9db22533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 408,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "312c61a1-e144-41b9-97a0-477dade1b221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 423,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a45d7585-5b2b-44ab-8707-02cf17cf4cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 439,
                "y": 80
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "16ff8241-2e0e-4519-bda2-79b3fe218059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 444,
                "y": 80
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4a6744e1-9058-4437-8b29-bb6bc0ffd218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 467,
                "y": 80
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cf371425-2a2e-46e3-99ae-6345cfb43458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 94,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f8c45c94-1ae0-4d43-be53-5e812a46553e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 477,
                "y": 80
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "705b8f17-1045-4e6a-ae93-3fd19d4ca9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 488,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2c115270-25b8-4f71-8240-586d4ba0b47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d2c0c895-36a2-4182-a7e1-bb072b12aceb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 26,
                "y": 119
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2d150c29-9b09-4dd1-a6de-593c463d0446",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 44,
                "y": 119
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fc4e3cf4-20eb-400e-9ab4-9026fd4bbf89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 59,
                "y": 119
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "da24b95f-ac66-44c6-84a2-d10816e4467a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 76,
                "y": 119
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "23c4dcf2-e5cd-4f66-841c-0bf6ff70f5b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 107,
                "y": 119
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "33a29ebc-ac1d-4026-8e76-a09a7e734a6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 358,
                "y": 119
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "17075921-d269-41ab-a4dd-f2fee8a45fae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 374,
                "y": 119
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e2277f7e-188b-4af1-88fd-fed5f4cf61ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 393,
                "y": 119
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d9505553-69ba-449d-a880-631c8df5cfad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 164,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "35b6dd29-3ce7-471d-bcd2-69307707fae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 179,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bde41f91-e021-47d2-8d0b-fae0d70fac26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 197,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b11d1e16-a3ab-413c-848b-6a6f23ea198c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "16d4d312-3974-4870-b2a7-239c59071a65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 227,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "58607b3a-3e09-4914-8164-e27575e99f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 251,
                "y": 158
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b8345e5c-4b60-422a-8228-c4af4f3cb681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 271,
                "y": 158
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "42468e1a-a277-482c-bdc5-53f852ade617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 291,
                "y": 158
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b152cd8c-81ce-4ec6-9959-e149b018e815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 305,
                "y": 158
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "68dd329d-070c-4686-b3a8-109427cd9bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 329,
                "y": 158
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e344136d-5714-4871-96ff-70d314e014d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 345,
                "y": 158
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cbbcc4c8-4ebf-4991-b4c3-ac9cb8ed23ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 362,
                "y": 158
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "de5e8e93-3971-46e6-b0be-aae3fd8329ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 381,
                "y": 158
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "474f9765-b1bc-4b54-b907-75723f850d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 399,
                "y": 158
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fb0d4bbc-6ac6-4c2c-a6d4-0ca2efa11c22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 2,
                "shift": 28,
                "w": 26,
                "x": 416,
                "y": 158
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "483ec895-eba6-4a29-9746-60803280e08c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 444,
                "y": 158
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f5f93ab4-9f39-42a8-94de-a930e3a3a77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 464,
                "y": 158
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a079b11f-f50d-449b-ac56-40971b0cca8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 145,
                "y": 158
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7cef1f60-3d72-4654-bd08-eca6af85155c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 136,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "460f37a1-68e2-4238-b672-82275ca39ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 123,
                "y": 158
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1956cb35-35af-4cac-aaf4-716b45635a8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4ac6e1ff-aa21-4fb7-a006-3300d14c3dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 412,
                "y": 119
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "97c4f372-f09d-493c-b56c-aca843816796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 424,
                "y": 119
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "30262f6d-f88a-4d18-ab1f-9ea086e69678",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 6,
                "x": 443,
                "y": 119
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d20ae5ca-5303-45e0-aa66-ea81a7e7bc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 451,
                "y": 119
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9609e285-dad6-4bf2-94f0-d01ff700a1d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 465,
                "y": 119
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "05314151-f596-4e73-b7aa-8c96dfee1345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 480,
                "y": 119
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e3607fed-9752-4d33-bbb6-82d65e72a2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 494,
                "y": 119
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d08021f8-9add-4d28-b6d1-91160499b15b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 11,
                "y": 158
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8580bdbe-35af-4ba9-b4b6-bef3b4ec3476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 110,
                "y": 158
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d3cf606f-31ce-4efe-abf0-085222538a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 26,
                "y": 158
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "904d9d82-fe12-475e-9160-be0b0b029fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 40,
                "y": 158
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e1033638-59f7-4125-ade8-e7b3f6bf2fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 54,
                "y": 158
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7997fc4d-67a4-454f-9dbb-6c8bea0e642c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 60,
                "y": 158
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b724b769-9b66-41ad-9a07-29c0d3f33057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 71,
                "y": 158
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d4a6e082-4a92-4817-bbbb-cf25bb11ea2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 85,
                "y": 158
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "212443a2-d439-4550-99ee-7314a34829dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 90,
                "y": 158
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "57216710-6f08-45a1-95ec-656200e2d260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 347,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e56fe195-0771-4f1c-9949-d19f8b916df0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 482,
                "y": 158
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2613da0d-9be0-4af4-8e25-c2e12d3e4cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 334,
                "y": 80
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "34b88bfc-0e02-4802-8f00-a22cef57e5ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 307,
                "y": 80
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "3d575211-ede6-4896-b3e0-4a7d8ddde392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b16f74f9-89a2-4265-9b46-66aa705f37f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 402,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2d999253-fc09-42ef-981f-a14a963620e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 415,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "86a43031-3973-4ecb-a6be-93a9e541b4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 428,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "84713288-f67a-4ce1-8537-3530beab977d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2e8f9b8e-f985-4bfe-8db8-6baad28e4ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "996ae8a2-2989-4bcf-9075-f2bdf72abf4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 475,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3ea45418-0c11-4fac-adcc-e80ba1ccd459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "601a2ba4-7871-444a-9a58-892e26f6401f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 109,
                "y": 41
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "02e0b8df-469a-4ab9-8852-14a3246e2a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 18,
                "y": 41
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b9e8e48d-0aba-4341-b688-bd6efa83e914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 5,
                "shift": 11,
                "w": 2,
                "x": 29,
                "y": 41
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "907ccaa8-3d38-45db-b5c8-136fedfef821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 33,
                "y": 41
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b065cf50-5a2a-41f9-84be-956d95108e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 44,
                "y": 41
            }
        },
        {
            "Key": 1025,
            "Value": {
                "id": "ca434395-1b43-4cae-9f55-2da48be9d762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1025,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 60,
                "y": 41
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "71b5b54a-461b-47db-acdf-07bd407b0657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 76,
                "y": 41
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "fe349056-70b8-4522-8541-2c3e25c09896",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 94,
                "y": 41
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "f1485a26-8b1b-4b59-98fa-4ff0a43b9ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "46b28f3c-6291-47d5-8d12-251a0fc4bde1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 491,
                "y": 2
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "15cdc8f1-f1e2-4e66-a7f3-3c21a1288b03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "40d949a9-560a-465a-bf4e-1aadfe278c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "04aa4dcd-6ff5-4fad-afc7-3c6e5c0030ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "1d2b3840-d5a1-448f-981b-a98271e51572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "b4f52f16-4c90-42c1-baa8-a9c859811ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "e9d13c67-2416-4210-8260-299b7b4d405f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "9668f156-7465-439d-9a37-0dc8969f6a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "fd946926-76f7-4a65-98ed-42e82725a958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "8832b895-e5cf-44f8-8bce-971f6729024c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "995a8057-f60b-4101-bfe0-45941b24c6dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "1f7ba038-e2cb-4ea1-b931-39d4bb16bf09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "a7df5fd4-49f9-43ab-b724-d97a620f8532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "a5e1371d-6b46-4815-a20c-cc0243cc3cbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "b58f16a4-6f06-4c5d-b1d2-3ac12af3ea6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "a13a3869-6558-45e2-ab89-23ab5f968389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "285338bf-aa36-4caa-a0c1-f7a21f6f33d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 258,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "6df20111-4c89-4090-b165-9e117deafef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "f77f6c7e-2dc5-40d8-8f64-da6bb05c06ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "958bde1d-1f53-473d-8b8f-0460edb1eca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 334,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "a7e76cae-252b-4c6d-9e87-514e71694604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 123,
                "y": 41
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "d70844a7-581a-40f2-9131-51534cc1ca71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 139,
                "y": 41
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "64dbce5d-a5fd-4d8a-998c-7f21996943d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 37,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 163,
                "y": 41
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "dd8376dc-cddc-4e21-b292-1372e300a76b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 20,
                "y": 80
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "997fa64f-3ff5-4e8c-b4af-ac73593da852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 37,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 40,
                "y": 80
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "8bc2075e-c49c-451f-a3c1-72a35f491e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "0647c9da-2aa1-46f9-835e-cb15166645be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 83,
                "y": 80
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "c9026047-4c67-4588-bc61-658ccb145eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 37,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 100,
                "y": 80
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "e7de40c8-8c62-47b0-a15e-2cf1d05611e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 80
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "0d6b14ce-ebb0-42ea-b8d8-39e7df684131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 147,
                "y": 80
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "36adf16f-ee69-48e5-ba4f-37486308b68d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 161,
                "y": 80
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "9f6fc724-ab02-4cc1-a2d2-3009f8487596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 176,
                "y": 80
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "e7e5d48a-3f3a-4acd-a692-6c8dcf1b6902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 189,
                "y": 80
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "4d0cf8aa-d612-4d03-bc23-4babe989ef07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 201,
                "y": 80
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "5256faa2-d8bd-47c3-b3a8-456e6bdde229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 218,
                "y": 80
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "caa99fce-8730-4230-a021-6299e54c31ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 233,
                "y": 80
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "651a3bed-a0b7-4b5e-9faa-4bc4af2b4fb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 251,
                "y": 80
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "0042be5e-9237-4c00-8580-76b38dd7e908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 262,
                "y": 80
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "f7656e13-de07-441a-af29-e3f1f3580e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 278,
                "y": 80
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "62ca2337-28f4-4f75-b9e5-d103357ad0e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 294,
                "y": 80
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "f39a7b38-fd14-4565-a6ae-45dc5c953da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "ddf0054b-b74c-4e53-b6eb-3a039b1cb242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 41
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "42256e6c-1824-4f53-9ffd-417f4a52150b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 475,
                "y": 41
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "a1373faa-a852-4261-bb39-d98dcdde4701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 298,
                "y": 41
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "b2027a28-93b8-451b-9caa-535f2bddeaf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 37,
                "offset": 3,
                "shift": 17,
                "w": 13,
                "x": 189,
                "y": 41
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "8728472a-7e96-4868-9423-aadb8e70187d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 204,
                "y": 41
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "231bd592-2136-4aba-970d-e8ddb7271926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 217,
                "y": 41
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "4d4911a3-1640-4c73-b4e4-3db16ca14a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 231,
                "y": 41
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "be6ab68b-16a0-426a-8b0a-3173f8c515b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 244,
                "y": 41
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "3bebe7ea-e8a7-45ee-ba6f-ae44c3180a45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 37,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 260,
                "y": 41
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "4d63eae6-0007-4811-9184-3f2a4b684bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 282,
                "y": 41
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "841f010c-7f0b-4a9e-a29d-82be4c83d6b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 314,
                "y": 41
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "a19e341f-65ce-4b1c-8e74-b648700af017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 461,
                "y": 41
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "7a2cf66c-d66e-4a8e-a9a4-7a05bf264ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 330,
                "y": 41
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "2c5a5f04-5814-436e-ad70-1a94f154441c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 350,
                "y": 41
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "93b07882-1e4f-4595-abc5-6025d0934581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 372,
                "y": 41
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "fa03f892-a252-4911-b529-99843e15d365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 390,
                "y": 41
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "69b63a39-3de7-4898-b8eb-571f96fdbeae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 411,
                "y": 41
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "4bd6ea34-1fc5-4fb8-b71a-a30c01014772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 423,
                "y": 41
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "17657b71-c643-41cd-bb09-bebd2f12fd6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 437,
                "y": 41
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "54243d81-1d9e-4f6e-8697-6593e7fd589d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 37,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 320,
                "y": 80
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "c940af59-c39a-4164-8b0f-4fbbfb38ffe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 496,
                "y": 158
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1025,
            "y": 1025
        },
        {
            "x": 1040,
            "y": 1103
        },
        {
            "x": 1105,
            "y": 1105
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}