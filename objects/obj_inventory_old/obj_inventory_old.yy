{
    "id": "b376c76d-b3f5-483f-860e-4dd71317987a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory_old",
    "eventList": [
        {
            "id": "1dfe45d5-65bb-4ad6-91fc-99975411cf65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b376c76d-b3f5-483f-860e-4dd71317987a"
        },
        {
            "id": "07a743a1-b765-4b74-84b3-1263d36a38bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b376c76d-b3f5-483f-860e-4dd71317987a"
        },
        {
            "id": "50381e97-6ad6-48f7-b112-6b5eda930310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "b376c76d-b3f5-483f-860e-4dd71317987a"
        },
        {
            "id": "1fc833cc-89a7-474e-be42-94234d710491",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b376c76d-b3f5-483f-860e-4dd71317987a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}