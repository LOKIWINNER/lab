
/*
// Проверяем, существует ли сетка
if(ds_exists(ds_depthgrid, ds_type_grid)){

	// Сделать сетку ds_depth локальной и доступной для наших экземпляров (все дочерние элементы родителя глубины)
	var depthgrid = ds_depthgrid;
	
	// получить количество экземпляров (количество детей)
	var instNum = instance_number(obj_parent_depth);
	
	// изменить размер сетки на количество экземпляров / детей
	ds_grid_resize(depthgrid, 2, instNum);
	
	// объявляем локальную переменную, мы будем увеличивать каждый цикл при добавлении дочерних элементов в сетку
	var yy = 0;
	
	// добавьте все экземпляры / дочерние элементы в сетку и соответствующее им значение y
	with(obj_parent_depth){
		depthgrid[# 0,yy] = id;
		depthgrid[# 1,yy] = y;	

		yy++;
	}
	
	// сортируем сетку в порядке возрастания (самый низкий Y будет наверху)
	ds_grid_sort(depthgrid, 1, true);
	
	// используйте повтор для цикла через сетку, начиная с высоты = 0, рисуя экземпляр,
	// и затем увеличивая yy для следующего цикла, чтобы нарисовать следующий экземпляр в сетке
	yy = 0; repeat(instNum){
		var instanceID = depthgrid[# 0, yy];

			with(instanceID) {
				
				event_perform(ev_draw,0);
								
				
				//if (!variable_instance_exists(instanceID, "fly")){
				//	if (variable_instance_exists(instanceID, "xp")){
				//		var spr_width = sprite_get_width(sprite_index);
				//		var spr_height = sprite_get_height(sprite_index);
				//		draw_healthbar(x - spr_width / 2, y - spr_height + 10, x + spr_width / 2, y - spr_height, xp, c_black, c_red, c_lime, 0, true, true);
				//	}
				//	draw_self();
				//} else {
				//	draw_sprite_ext(sprite_index, image_index, round(x), round(y), image_xscale * 0.6, image_yscale * 0.6, image_angle, c_black, 0.8);
				//	draw_sprite_ext(sprite_index, image_index, x + fix_x, y + fix_y, 1, 1, facing * 90, c_white, 1);
				//}
			}
		yy++;
	}
	
	// очистить все ячейки сетки до 0
	//ds_grid_clear(ds_depthgrid, 0);
}






 
