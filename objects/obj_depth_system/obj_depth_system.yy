{
    "id": "f3fb99d2-9527-436b-8e1f-47f3a71cbafe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_depth_system",
    "eventList": [
        {
            "id": "5c441c62-3185-40e7-befd-6c890000c512",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3fb99d2-9527-436b-8e1f-47f3a71cbafe"
        },
        {
            "id": "58312409-8a10-4d50-ae52-b19066068a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f3fb99d2-9527-436b-8e1f-47f3a71cbafe"
        },
        {
            "id": "89b0239a-1f58-4f72-a92c-71e72ace5895",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f3fb99d2-9527-436b-8e1f-47f3a71cbafe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}