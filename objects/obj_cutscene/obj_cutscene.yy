{
    "id": "86d7bc93-f0b0-4099-8c8e-2241479dbbd3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cutscene",
    "eventList": [
        {
            "id": "3a9682c0-6c39-439f-87f7-bb9c71a59f7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86d7bc93-f0b0-4099-8c8e-2241479dbbd3"
        },
        {
            "id": "020f712c-4ef7-449b-b62b-f416cc5bbf17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "86d7bc93-f0b0-4099-8c8e-2241479dbbd3"
        },
        {
            "id": "a0155453-a345-4422-a19c-90deda640b58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "86d7bc93-f0b0-4099-8c8e-2241479dbbd3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}