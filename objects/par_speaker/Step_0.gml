var _hero = playerobject;
if !instance_exists(_hero) exit;
var w = _hero.bbox_right - _hero.bbox_left + sprite_get_width(spr_collision_mask)/3;
var h = _hero.bbox_bottom - _hero.bbox_top + sprite_get_height(spr_collision_mask)/2;
if (point_in_rectangle(x, y, _hero.x-w, _hero.y-h, _hero.x+w, _hero.y+h)){
	_hero.draw_E = true;
	
	if(myTextbox != noone){ 
		if(!instance_exists(myTextbox)){ myTextbox = noone; exit; }
	}
	//if I haven't already created my textbox, make one:
	else if(keyboard_check_pressed(interact_key)){
		if(instance_exists(obj_textbox)){ exit; }	//exit if a textbox already exists
		event_user(0);								//if you need variables to update for text
			
		//Hand over variables
		create_dialogue(myText, mySpeaker, myEffects, myTextSpeed, myTypes, myNextLine, myScripts, myTextCol, myEmotion, myEmote);
	}
} else {	//if player moves outside of detection radius
	if(myTextbox != noone){
		with(myTextbox) instance_destroy();
		myTextbox = noone;
	}
}
