/// @description Insert description here


#region keyboard buttons						<- Вы можете добавить или удалить ключи здесь
// клавиши, используемые для отображения консоли
displaykey[0] = 223		// ` Qwerty
displaykey[1] = 220		// § Qwertz/Apple
displaykey[2] = 112		// F1
displaykey[2] = 192		// `
// ключи, используемые для скрытия консоли
closekey[0] = 223		// ` Qwerty
closekey[1] = 220		// § Qwertz/Apple
closekey[2] = 112		// F1
closekey[3] = 27		// Esc
closekey[3] = 192		// `


open_me_key_count =				array_length_1d(displaykey)
close_me_key_count =			array_length_1d(closekey)
#endregion
#region settings
prediction_count_max =			11				// при вводе, сколько прогнозов вы хотите в раскрывающемся списке
history_count_max =				20				// сколько истории вы хотите сохранить
history_file_name =				"console.txt"	// что вы хотите, чтобы файл сохранения был вызван 
view_history_datapoints =		500				// Когда вы рисуете график, сколько точек нужно нарисовать (посмотрите, собираетесь ли вы нарисовать 100 графиков, и вы собираетесь решить эту проблему, уменьшив это число).
#endregion
#region colours/style							<- Вы можете изменить цвета здесь
history_background_col =		make_colour_rgb(0,0,0)
history_background_alpha =		0.8
history_text_col =				make_colour_rgb(173,173,173)
history_text_col_info =			make_colour_rgb(144,113,80)
history_text_col_focus =		make_colour_rgb(255,182,104)
history_text_col_hover =		make_colour_rgb(255,132,0)
history_text_alpha =			1
history_text_shadow =			0.2 // 0=off, 1=full on, decimal=amount of opacity 
history_border_col =			make_colour_rgb(255,132,0)
history_border_alpha =			1
main_background_col =			make_colour_rgb(60,60,60)
main_background_alpha =			0.8
main_text1_col =				make_colour_rgb(255,255,255) // это первая часть разобранного текста
main_text2_col =				make_colour_rgb(160,160,160) // это аргументы, которые не в фокусе
main_text3_col =				make_colour_rgb(255,132,0) // это цвет, который вы вводите, чтобы сформировать слово (и выбранное слово)
main_text_alpha =				1
main_text_shadow =				0.3 // 0=off, 1=full on, decimal=amount of opacity 
main_text_bad_col =				make_colour_rgb(241,186,186) // цвет используется, если консоль не может разобрать код
main_border_col =				make_colour_rgb(209,209,209)
main_border_alpha =				1
main_mouse_selected_col =		make_colour_rgb(255,132,0)
predict_background_col =		make_colour_rgb(0,0,0)
predict_background_alpha =		0.3
predict_text_col =				make_colour_rgb(255,255,255)
predict_text_alpha =			1
predict_text_shadow =			0.7 // 0=off, 1=full on, decimal=amount of opacity 
predict_border_col =			make_colour_rgb(0,0,0)
predict_border_alpha =			0.7
focus_background_col =			make_colour_rgb(60,60,60)
focus_background_alpha =		1
focus_text_col =				make_colour_rgb(255,132,0)
focus_arrow_colour =			make_colour_rgb(255,132,0)
view_background_col =			make_colour_rgb(60,60,60)
view_background_alpha =			0.8
view_border_col =				make_colour_rgb(250,250,250)
view_border_alpha =				1
view_text_title_col =			make_colour_rgb(255,255,255)
view_text_value_col =			make_colour_rgb(255,132,0)
view_text_name_col =			make_colour_rgb(255,132,0)
view_data_bar_col =				make_colour_rgb(255,132,0)
view_data_bar_alpha =			0
view_data_top_col =				make_colour_rgb(255,132,0)
view_data_top_alpha =			1
view_intro_highlight_col =		make_colour_rgb(255,255,255)
#endregion
#region heights/widths/sizes
pos_top =						-1		// где это начинается на экране
pos_height =					200		// какой высоты область истории (текстовое поле начинается после этого)
pos_margin_sides =				24		// как далеко от сторон должна быть коробка
pos_inner_padding_sides =		20		// сколько места должно быть между текстом и стороной окна
pos_inner_padding_bottom =		25		// это то, сколько места находится между нижней частью текстовой области и нижней текстовой строкой
main_area_height =				40		// высота текстовой области
main_area_inner_padding_sides = 50		// как далеко от левой стороны текст начинается в основной области
history_line_height =			24
predict_line_height =			22



view_left_margin =				25
view_left_padding =				90
view_bottom_padding =			15
view_bottom_margin =			display_get_gui_height() - view_bottom_padding - view_left_margin;
view_data_height =				30
view_data_width =				display_get_gui_width() - view_left_padding*2 - view_left_margin*2
view_each_height =				view_data_height*1.4 + view_bottom_padding
#endregion
#region animations 
display_frames =				27
display_frame =					0
display_direction =				0
display_open_speed =			6
display_close_speed =			8
predict_grow_speed =			74 // in px
predict_shrink_speed =			53 // in px

dt = delta_time / 100000
#endregion
#region font
font_for_main = font_console
#endregion
#region only 1 of this object allowed 
if (instance_number(obj_console) < 1) {
	instance_destroy()
}
#endregion
#region предварительные размеры кэша (не редактируйте их)
history_x1 = pos_margin_sides
history_y1 = pos_top
history_x2 = display_get_gui_width()-pos_margin_sides
history_y2 = pos_top+pos_height
history_text_x = history_x1+pos_inner_padding_sides
history_text_y = history_y2-pos_inner_padding_bottom
	
main_x1 = history_x1
main_y1 = history_y2 + 1
main_x2 = history_x2
main_y2 = main_y1 + main_area_height

predict_x1 = main_x1
predict_y1 = 0 // worked out each frame
predict_x2 = main_x2
predict_y2 = history_y2 // worked out each frame
predict_y2_draw = history_y2

text_start_x = main_x1+main_area_inner_padding_sides
text_end_x = text_start_x
text_y = main_y1+floor((main_y2-main_y1)/2)

view_data_left_x = view_left_margin + view_left_padding
view_data_right_x = view_data_left_x + view_data_width + view_left_padding
#endregion
#region Инициализация материалов
predict_count =					0		// сколько прогнозов в списке
text =							""		// THE TEXT
text_changed =					true	// изменил ли текст этот кадр?
text_char_length[0] =			0		// насколько широк каждый символ
text_char_running_length[0] =	0		// сколько пикселей от этого символа до самого левого символа
cursor_at =						0		// где фокус находится в тексте
cursor_at_stage =				-1		// -1 is very right most, all stages start from 0 upwards 
cursor_at_stage_last =			-1		// если человек меняет стадию, ясно, на чем он сосредоточен
stage_count =					0		// количество ступеней
stage_text_length[0] =			0		// сколько символов на этом этапе текста
stage_text_running_length[0] =	0		// сколько символов от конца этого этапа до начала текста
good_first_stage =				false
command_to_run =				-1
focus =							0		// Это вертикальный выбор, 0 = текстовые области, <= - 1 прогноз,> = 1 история
temp_post_message =				""		// если вы поместите здесь какой-нибудь текст, а затем запустите user_event_1, он отправит его в историю
history_file_path =				working_directory + history_file_name
view_destroyed_frames =			200		// если вы отслеживаете переменную, и она уничтожается, держите всплывающее окно на экране так долго
view_intro_highlight_frames =	10		// когда вы появляетесь один, дайте ему новый цвет для этого множества кадров
mouse_selection_start =			0		// когда мышь используется для выделения текста, начальный символ
mouse_selection_end =			0		// когда мышь используется для выделения текста, начальный символ
mouse_selection_temp =			-1		// Используется для удержания позиции, на которой была нажата мышь
mouse_x_last =					device_mouse_x_to_gui(0) // Я использую это, чтобы увидеть, если мышь переместилась
mouse_y_last =					device_mouse_y_to_gui(0)
history_mouse_hover =			-1		// если мышь находится над одной из вещей истории

console_version = "0.2.1"
#endregion
#region cursor 
flash_frames = 45
flash_frame = 0
flash_frames_half = floor(flash_frames/2)
#endregion
#region holding down delete/backspace 
holding_delete_frames =			0
holding_backspace_frames =		0
holding_frame_to_activate =		15		// сколько кадров вы должны держать перед тем, как они будут репрессированы
holding_frame_delete_speed =	4		// сколько кадров между удалениями при удержании
#endregion
#region delimiters 
var i = 0
delimiters[i++] = " "
delimiters[i++] = "."
delimiters[i++] = ","
delimiters[i++] = "("
delimiters[i++] = ")"
delimiters[i++] = "="
delimiter_count = i++
#endregion
#region text break down
text_part[0] = ""
#endregion
#region history 
history_text[0] = ""
history_text_type[0] = 0 // 1=command, 2=info
history_text_count = 0

	#region load from save file 
	var lastline = "" // не загружайте последнюю строку дважды
	var thisline = ""
	
	if (file_exists(history_file_path)) {
		var i, save_file;
		save_file = file_text_open_read(history_file_path);
		
		for (i = 0; i < history_count_max; ++i) {
			if(!file_text_eoln(save_file)) {
				
				thisline = file_text_read_string(save_file); // получить эту строку из сохраненного файла
				
				if (thisline != lastline) {		// не загружайте одну и ту же строку дважды
				
					history_text[i] = thisline
					history_text_type[i] = 1	// 1=command, 2=info
					history_text_count++
					lastline = thisline
					
				}
				
				file_text_readln(save_file);	// следующая строка вниз
				
			} else {
				break;	
			}
		}

		file_text_close(save_file);
	}
	#endregion
#endregion
#region viewing data 
view_active_count =					0
view_text[0] =						"No Data"
view_title[0] =						"No Data"
view_obj[0] =						-1
view_min[0] =						9999999
view_max[0] =						9999999
view_last_data[0] =					0
view_type[0] =						0 // 0=int, 1=str, 2=array, 3=destroyed
view_destroyed_frame[0] =			view_destroyed_frames // если вы отслеживаете переменную, и она уничтожается, держите всплывающее окно на экране так долго
view_recalculate[0] =				false
view_graph_pixels[0,0] =			0
view_graph_data[0,0] =				0
view_next_insert[0] =				0
view_intro_highlight_frame[0] =		view_intro_highlight_frames
#endregion
#region top level commands
var i = 0
enum command_1 {
	create,
	set,
	destroy,
	view,
	script,
	game,
	console, 
	
	size
}

commands[command_1.create]	= "create"
commands[command_1.set]		= "set" // edit also works 
commands[command_1.destroy] = "destroy"
commands[command_1.view]	= "view"
commands[command_1.script]	= "script"
commands[command_1.game]	= "game"
commands[command_1.console] = "console"

commands_visible = array_create(command_1.size,false) // если мы покажем эту команду
commands_focused = array_create(command_1.size,false) // эта команда имеет фокус
#endregion
#region second level commands 

	predictions[0] = ""
	predictions_visible[0] = 0		// если мы покажем эту команду 
	predictions_focused[0] = 0		// эта команда имеет фокус
	command_to_prediction[0] = 0	// это справочная таблица для перехода между полным списком прогнозов и меньшим списком команд
	
	second_level_commands[0] = 0
	
	#region create
		//get all objects 
		var i = 0
		var still_finding_objects = true
		do {
			if (object_exists(i)) {
				second_level[command_1.create,i]	= object_get_name(i)
				second_level[command_1.set,i]		= second_level[command_1.create,i] // naughty of me to put this here as it breaks the layout but it is less code 
				second_level[command_1.destroy,i]	= second_level[command_1.create,i] // naughty of me to put this here as it breaks the layout but it is less code 
				second_level[command_1.view,i]		= second_level[command_1.create,i] // naughty of me to put this here as it breaks the layout but it is less code 
				i++
			} else {
				still_finding_objects = false
			}
		} until (!still_finding_objects);
	#endregion
	#region set
		// naughty me put this in the create one because it is less code 
	#endregion
	#region destroy
		// naughty me put this in the create one because it is less code 
	#endregion
	#region view
		// naughty me put this in the create one because it is less code 
	#endregion
	#region script
		
		//get all scripts 
		var i = 0
		var still_finding_objects = true
		do {
			if (script_exists(i)) {
				second_level[command_1.script,i] = script_get_name(i)
				i++
			} else {
				still_finding_objects = false
			}
		} until (!still_finding_objects);
	#endregion
	#region game
		var i = 0
		second_level[command_1.game,i++] = "speed_set"
		second_level[command_1.game,i++] = "resource_count"
		second_level[command_1.game,i++] = "instance_count"
		second_level[command_1.game,i++] = "restart"
		second_level[command_1.game,i++] = "fullscreen_toggle"
		second_level[command_1.game,i++] = "room_restart"
		second_level[command_1.game,i++] = "room_goto"
		second_level[command_1.game,i++] = "debug_overlay"
		second_level[command_1.game,i++] = "texture_debug"
		second_level[command_1.game,i++] = "debug_event"
	#endregion
	#region console
		var i = 0
		second_level[command_1.console,i++] = "clear_screen"
		second_level[command_1.console,i++] = "delete_history"
		second_level[command_1.console,i++] = "version"
		second_level[command_1.console,i++] = "check_for_updates"
		second_level[command_1.console,i++] = "about"
	#endregion
#endregion
#region third level commands
third_level[0] = ""
#endregion
