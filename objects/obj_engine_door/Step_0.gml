/// Run Script to Create sys_transition
if (place_meeting(x, y, obj_hero) && obj_hero.door) {
    room_goto_transition(targetRoom, kind, 60, sys_transition);
    image_speed = 0.12;

	if (xx != -1) { sys_transition.xx = xx; }
    if (yy != -1) { sys_transition.yy = yy; }


}
