/// Draw which stripes it uses over the door
draw_set_font(fnt_engine);
draw_self();

var str_version;

if (kind == "transition.stripe_hor") { str_version = "Horizontal Lines"; } 
else { str_version = "Vertical Lines"; }

draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_color(c_white);

draw_text(x, y - 60, str_version);