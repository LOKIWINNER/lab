{
    "id": "8d9cbed0-817e-44cc-b2f5-2690e91fe867",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_engine_door",
    "eventList": [
        {
            "id": "76e717f4-070a-4bb4-aa22-e33c7b6495f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d9cbed0-817e-44cc-b2f5-2690e91fe867"
        },
        {
            "id": "13507213-ca7a-4e42-b2af-194194d069fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d9cbed0-817e-44cc-b2f5-2690e91fe867"
        },
        {
            "id": "44c26f7f-4496-486c-82b4-25688cb51757",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "8d9cbed0-817e-44cc-b2f5-2690e91fe867"
        },
        {
            "id": "655e2626-86cc-477d-8e22-383e1c81d131",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8d9cbed0-817e-44cc-b2f5-2690e91fe867"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d708c672-f164-479e-a513-52b1f901cee5",
    "visible": true
}