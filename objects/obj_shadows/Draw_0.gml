/// @description 

if(!surface_exists(shadowSurface)){
	shadowSurface = surface_create(__view_get(e__VW.WView,0), __view_get(e__VW.HView,0));
}



var viewX = camera_get_view_x(view_camera[0]);
var viewY = camera_get_view_y(view_camera[0]);

surface_set_target(shadowSurface);
draw_clear_alpha($635C74,0);

var sx = skewX;
var sy = shadowHeight;

//var sx = (room_width/4) - mouse_x;
//var sy = (room_height/4) - mouse_y;

gpu_set_fog(true, $635C74, 0, 1);
with(obj_parent_depth){
	if (!variable_instance_exists(id, "shadow_use")) shadow_use = false;
	if (shadow_use){
		sy = sprite_height / 6;
		if (!variable_instance_exists(id, "shadow_scale_up")) shadow_scale_up = 4;
		if (!variable_instance_exists(id, "shadow_scale_down")) shadow_scale_down = 6;
		if (!variable_instance_exists(id, "shadow_xx")) shadow_xx = 0;
		if (!variable_instance_exists(id, "shadow_yy")) shadow_yy = 0;

sx = 0;
sy = sprite_height;

var ww = sprite_get_bbox_left(sprite_index); 
var hhh = sprite_get_bbox_top(sprite_index);
var www = sprite_get_bbox_right(sprite_index);
var hh = sprite_get_bbox_bottom(sprite_index);




var i = www - ww; 
var r = hhh - hh;

var k = sprite_get_xoffset(sprite_index);
var l = sprite_get_yoffset(sprite_index);


	draw_sprite_pos(sprite_index, image_index, 
		x - k + i - sprite_width / 2 - viewX + sx,
		y - l + r - viewY - sy, 
		x - k + i + sprite_width / 2 - viewX + sx, 
		y - l + r - viewY - sy, 
		x - k + i + sprite_width / 2 - viewX, 
		y - l + r - viewY, 
		x - k + i - sprite_width / 2 - viewX, 
		y - l + r - viewY, 
		1);
		
	/*	
	draw_sprite_pos(sprite_index, image_index, 
		x - k - r - (sprite_width / shadow_scale_up) - viewX + sx,
		y - l + i - viewY - sy, 
		x - k - r + (sprite_width / shadow_scale_up) - viewX + sx, 
		y - l + i - viewY - sy, 
		x - k - r + (sprite_width / shadow_scale_down) - viewX + shadow_xx, 
		y - l + i - viewY + shadow_yy, 
		x - k - r - (sprite_width / shadow_scale_down) - viewX + shadow_xx, 
		y - l + i - viewY + shadow_yy, 
		1);
	*/
		
	}
}
gpu_set_fog(false, c_white, 0, 0);

surface_reset_target();

draw_set_alpha(0.5);
draw_surface(shadowSurface, viewX, viewY);
draw_set_alpha(1);



