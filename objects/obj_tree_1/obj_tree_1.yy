{
    "id": "c7983194-5244-42f4-955a-ad4030f88e0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tree_1",
    "eventList": [
        {
            "id": "1decaca4-023b-44f4-ae59-0542fbdc8fb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7983194-5244-42f4-955a-ad4030f88e0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c898b248-3949-4201-b7f0-d0b383d11fda",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "64755610-3590-475f-82fa-9163b3b90962",
    "visible": true
}