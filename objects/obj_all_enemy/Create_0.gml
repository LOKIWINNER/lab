//Перечисления
// hitByAttack					= ds_list_create();
event_inherited();
enum ENEMY_STATE		
{						
    normal					= EnemyState_Normal,
	walk					= EnemyState_Walk,
    attack					= EnemyState_Attack,
	alert					= EnemyState_Alert,
	hit						= EnemyState_Hit,
	dead					= EnemyState_Dead,
	
}
enum ENEMY_ACTION		
{						
    stand,				
    walk,				
    attack,				
    alert
}


// Начальные переменные игрока
state               = ENEMY_STATE.normal;     
action              = ENEMY_ACTION.stand;     
facing              = round(random(3));  
counter				= 0;
_x_input = 0;
_y_input = 0;

attack = false;


var_direction = 0;

enemy_list = 
[
obj_enemy_knight_1,
obj_enemy_knight_2,
obj_enemy_knight_3,
]

height = 32;

//z var
z				= 0;		
z_floor_		= 0;		
z_speed_		= 3;		
z_grav_			= 0;		
z_jump_			= false;	
z_jump_speed_	= 7
height			= 16;