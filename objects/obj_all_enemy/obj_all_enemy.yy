{
    "id": "782de373-6e97-4168-992b-48a470810795",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_all_enemy",
    "eventList": [
        {
            "id": "0ee0beda-4326-4c87-8767-e5d579757488",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "782de373-6e97-4168-992b-48a470810795"
        },
        {
            "id": "f1c18753-353a-43ca-8702-45f73903c3a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "782de373-6e97-4168-992b-48a470810795"
        },
        {
            "id": "f94c245e-8946-48db-82d7-46dc1f630539",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "782de373-6e97-4168-992b-48a470810795"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fb6b4689-815a-4f7f-8c34-eed94394907e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}