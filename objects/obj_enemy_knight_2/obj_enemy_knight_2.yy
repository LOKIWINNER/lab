{
    "id": "47c0deb3-49a6-4adc-8a01-45915a3e01f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_knight_2",
    "eventList": [
        {
            "id": "8a12a6e1-7d79-4506-a0b3-91564d7f7b78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        },
        {
            "id": "74a20eb0-61ce-4a81-b2c6-781de7d15168",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        },
        {
            "id": "c2de6d09-24fd-4794-98c4-e4c0dfd03bd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        },
        {
            "id": "360037c8-298c-4358-8425-2b22b3adf294",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        },
        {
            "id": "f240b531-2ffc-4844-a067-db50eee06c4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        },
        {
            "id": "ce1b50b8-4c75-450a-91c0-e73855ce605e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        },
        {
            "id": "e45883ef-2637-4fd2-96b8-0d40326a25c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "47c0deb3-49a6-4adc-8a01-45915a3e01f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "782de373-6e97-4168-992b-48a470810795",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
    "visible": true
}