{
    "id": "cc6c8eb8-dd94-4487-b1b7-e29001b975fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box_32",
    "eventList": [
        {
            "id": "23617173-e234-4ef3-bcb6-3b2fa13f99c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc6c8eb8-dd94-4487-b1b7-e29001b975fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42c12e3c-461c-4920-afe8-f3ccfa393856",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d28870d-a5a1-42b1-ae3e-aa9bea268524",
    "visible": true
}