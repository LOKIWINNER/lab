{
    "id": "2946f069-5961-4668-89ea-a628446b1e5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GrassArea",
    "eventList": [
        {
            "id": "0a1fdad6-060e-40b8-8dab-568432372b16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2946f069-5961-4668-89ea-a628446b1e5b"
        },
        {
            "id": "cf980a8f-4283-4c0a-8618-8c4881bf65e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2946f069-5961-4668-89ea-a628446b1e5b"
        },
        {
            "id": "55590086-1a39-436b-90b4-98d7903f5c04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2946f069-5961-4668-89ea-a628446b1e5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77b56da0-9e60-4e85-a88d-2516e13f01ee",
    "visible": true
}