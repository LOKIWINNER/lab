{
    "id": "ca5711c7-d6e5-46b6-9113-01b03d89f2d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_day_and_night",
    "eventList": [
        {
            "id": "6f6bd15d-e50f-4c04-856c-6dd8720ce70b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca5711c7-d6e5-46b6-9113-01b03d89f2d3"
        },
        {
            "id": "287bea4d-5c57-40c9-b6c4-f999a042118f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca5711c7-d6e5-46b6-9113-01b03d89f2d3"
        },
        {
            "id": "7719b1c8-3979-4d8f-b9f2-c5b8fe731505",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "ca5711c7-d6e5-46b6-9113-01b03d89f2d3"
        },
        {
            "id": "d26709c2-2b0f-4891-9baa-4020ef277ab6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "ca5711c7-d6e5-46b6-9113-01b03d89f2d3"
        },
        {
            "id": "7f8edf63-a2a2-42e3-ac3e-3f33160a81a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ca5711c7-d6e5-46b6-9113-01b03d89f2d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}