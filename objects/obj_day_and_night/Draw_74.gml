// DRAW:
//-----------------------------------------------------------------------------
shader_set(shader);
	shader_set_uniform_f_array(u_col, color_mix);
	shader_set_uniform_f_array(u_con_sat_brt, con_sat_brt_mix);
	if surface_exists(application_surface)
		draw_surface_ext(application_surface, 0, 0, scale_x, scale_y, 0, c_white, 1); // Нужно! scale сделать глобальной переменной
shader_reset();


draw_text(20, 60, alpha);
