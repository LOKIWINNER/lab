
// get key times:

//if (automation_type == 1) {
//	value += automation_spd * automation_sign;
//	if (value >= 1) || (value <= 0) automation_sign = -automation_sign;
//} else if (automation_type == 2) {
//	value += automation_spd;
//	if (value >= 1) value -= 1;
//}
//value = clamp(value, 0, 1);

world_time += 0.0001;
if (world_time >= 1) world_time -= 1;
world_time = clamp(world_time, 0, 1);
var time		= world_time;

key_previous	= min(floor(time * number_of_key_times), number_of_key_times -1);
key_next		= (key_previous + 1) mod number_of_key_times;

// get lerp amount:
var lerp_amt	= (time - key_previous/number_of_key_times) * number_of_key_times;

// mix colors:
color_mix		=  [lerp(color[key_previous,0], color[key_next,0], lerp_amt),
					lerp(color[key_previous,1], color[key_next,1], lerp_amt),
					lerp(color[key_previous,2], color[key_next,2], lerp_amt)];
					
con_sat_brt_mix	=  [lerp(con_sat_brt[key_previous,0], con_sat_brt[key_next,0], lerp_amt),
					lerp(con_sat_brt[key_previous,1], con_sat_brt[key_next,1], lerp_amt),
					lerp(con_sat_brt[key_previous,2], con_sat_brt[key_next,2], lerp_amt),
					lerp(con_sat_brt[key_previous,3], con_sat_brt[key_next,3], lerp_amt),
					lerp(con_sat_brt[key_previous,4], con_sat_brt[key_next,4], lerp_amt)];

// reflection alpha:
//			  sin((2 * time			+ phase) * pi) * scale - shift
alpha	= clamp(cos((2 * time) * 3.14) * 1.6 - 0.1, 0, 1);

// set slider caption:
var hour		= string_format(floor(time * 24), 0, 0);
hour			= (string_length(hour) == 1) ? "0" + hour : hour;
var minute		= string_format(time * 24 * 60 mod 60, 0, 0);
minute			= (string_length(minute) == 1) ? "0" + minute : minute;
world_time_caption = hour + ":" + minute;

//-----------------------------------------------------------------------------
