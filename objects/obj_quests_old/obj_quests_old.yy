{
    "id": "d6c888b0-948c-43a9-a6fb-6ba04c715a79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quests_old",
    "eventList": [
        {
            "id": "20a3e256-dcce-4644-a833-fe4d757a6730",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6c888b0-948c-43a9-a6fb-6ba04c715a79"
        },
        {
            "id": "adc3ae77-9bee-450f-9c49-f358cdf59b61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d6c888b0-948c-43a9-a6fb-6ba04c715a79"
        },
        {
            "id": "6cc91e38-3c28-40a5-b1ec-b2f55ff78d9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d6c888b0-948c-43a9-a6fb-6ba04c715a79"
        },
        {
            "id": "5f00c924-7cf8-42a3-af35-7231f7e3c238",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d6c888b0-948c-43a9-a6fb-6ba04c715a79"
        },
        {
            "id": "783c6536-bc58-4440-8446-c937f75875bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d6c888b0-948c-43a9-a6fb-6ba04c715a79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
    "visible": true
}