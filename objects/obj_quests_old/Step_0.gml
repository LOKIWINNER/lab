/* 
var grid = ds_quests;
var _draw_E = draw_E;
var i = 0; repeat(ds_quests_number){
	switch(i){
		case quests.paint:
		#region Задача по перекрашиванию
			with(obj_hero){
				switch(grid[# 1, i]){
					case -1: break;
					case 0: 
							var inst = instance_nearest(x, y, obj_tree_all);
							if (inst != noone){
								var w = inst.bbox_right - inst.bbox_left + sprite_get_width(spr_collision_mask)/3;
								var h = inst.bbox_bottom - inst.bbox_top + sprite_get_height(spr_collision_mask)/2;
								if (point_in_rectangle(x, y, inst.x-w, inst.y-h, inst.x+w, inst.y+h)){
									_draw_E = true;
									myRedTree = inst;
									if keyboard_check_pressed(ord("E")){
										with(inst){
											image_blend = c_red;
											grid[# 1, i] +=1;
										}
									}
								} else {
									_draw_E = false;		
								}
							}

					break;
					case 1: 
				
							var inst = instance_nearest(x, y, obj_tree_all);
							if (inst != noone){
								var w = inst.bbox_right - inst.bbox_left + sprite_get_width(spr_collision_mask)/3;
								var h = inst.bbox_bottom - inst.bbox_top + sprite_get_height(spr_collision_mask)/2;
								
								if (inst != myRedTree && point_in_rectangle(x, y, inst.x-w, inst.y-h, inst.x+w, inst.y+h)){
									_draw_E = true;
									if keyboard_check_pressed(ord("E")){
										with(inst){
											image_blend = c_blue;
											grid[# 1, i] +=1;
										}
									}
								} else {
									_draw_E = false;		
								}
							}
				
					break;
					case 2: 
				
							var inst = instance_nearest(x, y, obj_knight_sun);
							if (inst != noone){
								var w = inst.bbox_right - inst.bbox_left + sprite_get_width(spr_collision_mask)/3;
								var h = inst.bbox_bottom - inst.bbox_top + sprite_get_height(spr_collision_mask)/2;
								
								if (inst != noone && point_in_rectangle(x, y, inst.x-w, inst.y-h, inst.x+w, inst.y+h)){
									_draw_E = true;
									if keyboard_check_pressed(ord("E")){
										with(inst){
											change_variable(obj_knight_sun, "choice_variable", "done");
											grid[# 1, i] +=1;
										}
									}
								} else {
									_draw_E = false;		
								}
							}
				
					break;
				}
			}
		#endregion
		break;
		
		
	}
	draw_E = _draw_E;
	i++;
}