draw_set_valign(fa_top);
draw_set_halign(fa_left);

if (instance_exists(obj_hero)){
	draw_healthbar(10, 10, 100, 20, obj_hero.hp, c_black, c_maroon, c_red, 0, true, true);
	draw_healthbar(10, 25, 100, 35, obj_hero.sp, c_black, c_yellow, c_green, 0, true, true);
	draw_healthbar(10, 40, 100, 50, obj_hero.mp, c_black, c_maroon, c_blue, 0, true, true);
}

draw_set_font(fnt_GUI);


