{
    "id": "512d7908-b071-4b2e-8bd4-fd89934a89b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_control",
    "eventList": [
        {
            "id": "ef3ea1e9-55de-4b5b-90d0-cf90be7aeacf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "512d7908-b071-4b2e-8bd4-fd89934a89b7"
        },
        {
            "id": "deed7077-2414-4aa4-b980-ef5354d8cde4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "512d7908-b071-4b2e-8bd4-fd89934a89b7"
        },
        {
            "id": "a61419d3-cad7-488b-89e9-afcdfb3e065c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "512d7908-b071-4b2e-8bd4-fd89934a89b7"
        },
        {
            "id": "1cd7c77b-4bfb-4597-8741-0ee7655a1ad2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "512d7908-b071-4b2e-8bd4-fd89934a89b7"
        },
        {
            "id": "30911ea8-dbab-45c2-90f8-73cb805e4d47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "512d7908-b071-4b2e-8bd4-fd89934a89b7"
        },
        {
            "id": "a4026d35-30de-4108-83d3-37532821b87f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "512d7908-b071-4b2e-8bd4-fd89934a89b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}