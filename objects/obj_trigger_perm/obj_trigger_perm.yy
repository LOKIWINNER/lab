{
    "id": "3c62d647-8320-434e-b8bd-a72164266778",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trigger_perm",
    "eventList": [
        {
            "id": "b676cf2e-5a9f-40d7-8034-f6466cb2b048",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c62d647-8320-434e-b8bd-a72164266778"
        },
        {
            "id": "973b9843-7212-4a23-8a92-349b5a7de7e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3c62d647-8320-434e-b8bd-a72164266778"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ac4952a7-0700-48d1-8ce8-956d71503cb4",
    "visible": true
}