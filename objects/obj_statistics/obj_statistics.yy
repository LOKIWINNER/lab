{
    "id": "29e09956-3bbf-478c-9e88-732fdf7a95cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_statistics",
    "eventList": [
        {
            "id": "edbaa22a-70ff-4cc5-b3aa-f12166f8535a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29e09956-3bbf-478c-9e88-732fdf7a95cd"
        },
        {
            "id": "f853e777-e73e-43f6-84d2-ed1385838098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29e09956-3bbf-478c-9e88-732fdf7a95cd"
        },
        {
            "id": "85239753-79eb-42da-84cf-8e509b7ed67e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "29e09956-3bbf-478c-9e88-732fdf7a95cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}