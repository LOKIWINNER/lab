 
draw_set_font(fnt_engine);

draw_text(bbox_right + 10, bbox_top + (bbox_bottom-bbox_top)/2 - 5, string(bbox_right));
draw_text(bbox_left - 30, bbox_top + (bbox_bottom-bbox_top)/2 - 5, string(bbox_left));
draw_text(bbox_left + (bbox_right - bbox_left)/2 - 10, bbox_top - 15, string(bbox_top));
draw_text(bbox_left + (bbox_right - bbox_left)/2 - 10, bbox_bottom + 5, string(bbox_bottom));

draw_self();