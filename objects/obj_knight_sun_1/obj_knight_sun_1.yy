{
    "id": "b8e1bbe0-ceb5-43b8-8a1d-9a0175a1d670",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_knight_sun_1",
    "eventList": [
        {
            "id": "8af99617-b37c-4145-bc64-5437f542f754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8e1bbe0-ceb5-43b8-8a1d-9a0175a1d670"
        },
        {
            "id": "a00ea196-e924-4c6b-9e69-8aa4428eb8da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8e1bbe0-ceb5-43b8-8a1d-9a0175a1d670"
        },
        {
            "id": "abda284d-efa5-45e4-a85c-8d02a89d36f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b8e1bbe0-ceb5-43b8-8a1d-9a0175a1d670"
        },
        {
            "id": "110643d2-e379-4d89-a820-64f286236cf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b8e1bbe0-ceb5-43b8-8a1d-9a0175a1d670"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4aab45b5-e4a8-4f3d-b6b1-61c0f18605ef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
    "visible": true
}