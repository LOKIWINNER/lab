//---You can update variables here!---//
reset_dialogue_defaults();
var _hero = playerobject;
var _self = id;

switch(choice_variable){
	case -1:
	#region First Dialogue
		//Line 0
		var i = 0;
		myText[i]		= "Привет!";
		mySpeaker[i]	= _self;
		myEmotion[i]	= 1; // эмоция возле изображения
		myEmote[i]		= 1; // эмоция в облачке
		//myScripts[i]	= [create_instance_layer, _self.x, _self.y,"Text",obj_emote];
		
		
		//Line 1
		i++;
		myText[i]		= "Хочешь задание?";
		mySpeaker[i]	= _self;
		myEmote[i]		= 11; // эмоция в облачке
		
		//Line 2
		i++;
		myText[i]		= ["Да", "Нет"];
		myTypes[i]		= 1;
		mySpeaker[i]	= _hero;
		//myEmote[i]		= 11; // эмоция в облачке
		//myScripts[i]	= [[create_instance_layer, _self.x, _self.y,"Text",obj_emote], [create_instance_layer, _self.x, _self.y + 40,"Instances",obj_knight_sun]];
		myNextLine[i]	= [0,0];
		
		//Line 3
		i++;
		myText[i]		= "Да какая разница! Я просто дам его тебе!";
		mySpeaker[i]	= _self;
		//myScripts[i]	= [create_instance_layer, _hero.x, _hero.y,"Text",obj_emote];
		myEmote[i]		= 12; // эмоция в облачке
		
		//Line 4
		i++;
		myText[i]		= "Видишь те деревья? Тебе нжно их перекрасить!";
		myEffects[i]	= [11,1, 18,0];
		mySpeaker[i]	= _self;
		myTextCol[i]	= [11, c_lime, 18, c_white];

		//Line 5
		i++;
		myText[i]		= "Покрась два дерева в красный и синий!";
		myEmotion[i]	= 1; // эмоция возле изображения
		myEmote[i]		= 3; // эмоция в облачке
		mySpeaker[i]	= _self;
		myTextCol[i]	= [22, c_red, 29, c_white, 32, c_blue, 37, c_white];

		//Line 6
		i++;
		myText[i]		= "Удачи! Буть осторожен со стражей!";
		//myTextSpeed[i]	= [1,0.5, 10,0.1];
		myEmotion[i]	= 1; // эмоция возле изображения
		myEmote[i]		= 4; // эмоция в облачке
		mySpeaker[i]	= _self;
		myTextCol[i]	= [1, c_red, 6, c_white];

		//Line 7
		i++;
		myText[i]		= ["Но у меня нет краски!", "Хорошо, я всё сделаю."];
		myTypes[i]		= 1;
		myNextLine[i]	= [8,10];
		//myScripts[i]	= [[change_variable, _self, "choice_variable", "blue"], [change_variable, _self, "choice_variable", "green"]];
		mySpeaker[i]	= _hero;

		//Line 8
		i++;
		myText[i]		= "Да это же игра =) краска у тебя всегда с собой.";
		myEmotion[i]	= 1; // эмоция возле изображения
		myEmote[i]		= 1; // эмоция в облачке
		//myNextLine[i]	= -1;
		myNextLine[i]	= 10;
		mySpeaker[i]	= _self;

		//Line 9
		i++;
		myText[i]		= "Ну вот и ладненько.";
		//myTextSpeed[i]	= [1,1, 6,0.3, 10,1];
		myEmotion[i]	= 2; // эмоция возле изображения
		myEmote[i]		= 9; // эмоция в облачке
		mySpeaker[i]	= _self;
		
		//Line 10
		i++;
		myText[i]		= "Как всё покрасишь приходи ко мне.";
		//myTextSpeed[i]	= [1,1, 6,0.3, 10,1];
		myEmotion[i]	= 2; // эмоция возле изображения
		myEmote[i]		= 9; // эмоция в облачке
		mySpeaker[i]	= _self;
		myScripts[i]	= [change_variable, _self, "choice_variable", "waiting"];
		
		#endregion
	break;
	
	case "waiting":
	#region Waiting
		var i = 0;
		//Line 0
		myText[i]		= "Ну и как дела? Всё сделал?";
		myTextSpeed[i]	= [1, 0.3];
		myEmote[i]		= 11;
		mySpeaker[i]	= _self;
		
		//Line 1
		i++;
		myText[i]		= "Нет. Я забыл что нужно было сделать.";
		myTextSpeed[i]	= [1, 0.3];
		myEmote[i]		= 12;
		mySpeaker[i]	= _hero;
		
		//Line 2
		i++;
		myText[i]		= "Покрась два дерева в красный и синий! Я думал это у меня с памятью плохо.";
		mySpeaker[i]	= _self;
		myTextCol[i]	= [22, c_red, 29, c_white, 32, c_blue, 37, c_white];
		
		//Line 3
		i++;
		myText[i]		= "И помни. Чем больше краски, тем больше и ответственности.";
		mySpeaker[i]	= _self;
		
		//uncommenting this will make the first conversation begin again
		//choice_variable	= -1;
	#endregion
	
	break;
	
	case "done":
	#region Done
		var i = 0;
		//Line 0
		myText[i]		= "Ну и как дела? Всё сделал?";
		myEmote[i]		= 11;
		mySpeaker[i]	= _self;
		
		//Line 1
		i++;
		myText[i]		= "Иначе меня бы здесь не было!";
		myEmote[i]		= 12;
		mySpeaker[i]	= _hero;
		myScripts[i]	= [change_variable, _self, "choice_variable", "nope"];
		
		//Line 2
		i++;
		myText[i]		= "Молодец! и ты даже не запачкался. Какую награду ты для себя хочешь?";
		myEmote[i]		= 1;
		mySpeaker[i]	= _self;
		myScripts[i]	= [change_variable, _self, "choice_variable", "nope"];
		
		//Line 3
		i++;
		myText[i]		= ["Больше опыта", "Легендарный меч", "Мне ничего не нужно"];
		myTypes[i]		= 1;
		mySpeaker[i]	= _hero;
		myNextLine[i]	= [4,4,5];
		
		//Line 4
		i++;
		myText[i]		= "Прости, но у меня нет того что ты просишь.";
		mySpeaker[i]	= _self;
		
		//Line 5
		i++;
		myText[i]		= "Спасибо что помог.";
		mySpeaker[i]	= _self;
		
		//uncommenting this will make the first conversation begin again
		//choice_variable	= -1;
	#endregion
	
	break;
	
	case "nope":
	#region Nope
		var i = 0;
		//Line 0
		myText[i]		= "Прости, но у меня нет заданий для тебя.";
		myTextSpeed[i]	= [1,1, 10,0.5];
		myEmotion[i]	= 2;
		mySpeaker[i]	= _self;
		myTextCol[i]	= [19,c_aqua, 23,c_white];
		//myScripts[i]	= [[change_variable, _self, "choice_variable", "blue"], [change_variable, _self, "choice_variable", "green"]];
		
		//uncommenting this will make the first conversation begin again
		//choice_variable	= -1;
	#endregion
	break;
}