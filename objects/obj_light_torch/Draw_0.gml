

if(!surface_exists(nightCircleSurf)){
	nightCircleSurf = surface_create(__view_get(e__VW.WView,0), __view_get(e__VW.HView,0));
}

var viewX = camera_get_view_x(view_camera[0]);
var viewY = camera_get_view_y(view_camera[0]);

surface_set_target(nightCircleSurf);
draw_clear(c_black);


with(obj_torch){
	gpu_set_blendmode(bm_src_color);
	draw_sprite_ext(spr_torch_light,0,x-viewX,y + spr_torch/4 -viewY,2.5 * glowSize,2 * glowSize,0,c_white,1);
	gpu_set_blendmode(bm_normal);
}


surface_reset_target();

draw_surface_ext(nightCircleSurf, viewX, viewY,1,1,0,c_white,alpha);
