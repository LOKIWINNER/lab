{
    "id": "62170c24-d2da-4fd3-b833-9d006f6e1418",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_torch",
    "eventList": [
        {
            "id": "902966ed-349e-43ca-b7ce-dfb1d1ac2356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62170c24-d2da-4fd3-b833-9d006f6e1418"
        },
        {
            "id": "44efff3b-d5bb-4e74-b2cb-409a3f71ef57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "62170c24-d2da-4fd3-b833-9d006f6e1418"
        },
        {
            "id": "f6d3b7fb-03df-48fc-ad78-ea6c3d2cb5b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "62170c24-d2da-4fd3-b833-9d006f6e1418"
        },
        {
            "id": "ebf40ab5-e5b8-4e8f-ae4b-7d66a8a16768",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "62170c24-d2da-4fd3-b833-9d006f6e1418"
        },
        {
            "id": "77421f86-cae1-404b-9702-b3b3a414d13b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62170c24-d2da-4fd3-b833-9d006f6e1418"
        },
        {
            "id": "e71491cf-4e8d-4f1f-8e24-61d86940ba0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "62170c24-d2da-4fd3-b833-9d006f6e1418"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}