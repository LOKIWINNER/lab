///update depth
depth = -y;

//pushable blocks
//when player is on same z and can push block
if (obj_hero.z = z-16) and (canpush = true)
{
//push right
if (place_meeting(x-1,y,obj_hero))
{
	if (keyboard_check(ord("D")))
	{
			if (place_empty(x+1,y,obj_box_par))
			{
				x += 1;
				alarm[0] = weight;
				canpush = false;
			}
	}
}
//push up
if (place_meeting(x,y+1,obj_hero))
{
	if (keyboard_check(ord("W")))
	{
			if (place_empty(x,y-1,obj_box_par))
			{
				y -= 1;
				alarm[0] = weight;
				canpush = false;
			}
	}
}
//push left
if (place_meeting(x+1,y,obj_hero))
{
	if (keyboard_check(ord("A")))
	{
			if (place_empty(x-1,y,obj_box_par))
			{
				x -= 1;
				alarm[0] = weight;
				canpush = false;
			}
	}
}
//push down
if (place_meeting(x,y-1,obj_hero))
{
	if (keyboard_check(ord("S")))
	{
			if (place_empty(x,y+1,obj_box_par))
			{
				y += 1;
				alarm[0] = weight;
				canpush = false;
			}
	}
}
}