{
    "id": "a714b032-2d5c-42e9-b1d8-5c68d7689bdf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box_push",
    "eventList": [
        {
            "id": "701697f1-1a50-4f3c-b74e-3e44e86d3835",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a714b032-2d5c-42e9-b1d8-5c68d7689bdf"
        },
        {
            "id": "5c1291ff-db94-4c6f-a734-58865434d66c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a714b032-2d5c-42e9-b1d8-5c68d7689bdf"
        },
        {
            "id": "f3f83daf-fda4-4ad7-bca8-5eca7a29d694",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a714b032-2d5c-42e9-b1d8-5c68d7689bdf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42c12e3c-461c-4920-afe8-f3ccfa393856",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6ed6990-c3a0-4ef1-9d2f-d17b1364f1e8",
    "visible": true
}