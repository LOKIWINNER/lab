{
    "id": "0e6850de-69b4-4ffd-96c1-57a17959d776",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory",
    "eventList": [
        {
            "id": "a5fd812e-9fb6-47e7-8feb-c31a70b045c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e6850de-69b4-4ffd-96c1-57a17959d776"
        },
        {
            "id": "75c00e63-eb98-4a81-8261-065be63f5356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0e6850de-69b4-4ffd-96c1-57a17959d776"
        },
        {
            "id": "288f3ab3-d002-41cc-a7d0-03987d6c8df8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0e6850de-69b4-4ffd-96c1-57a17959d776"
        },
        {
            "id": "a4b8b0de-61a4-4cc7-8187-6dc3342979dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e6850de-69b4-4ffd-96c1-57a17959d776"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}