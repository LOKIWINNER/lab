if (!show_inventory) || (global.pause) exit;


draw_set_valign(fa_top);
draw_set_halign(fa_left);

// Рисуем сам фон (left и top нужно для смещения начальной точки)
draw_sprite_part_ext(
	spr_inv_UI, 3, 0, 0, inv_UI_width, inv_UI_height,
	inv_UI_x, inv_UI_y, scale, scale, c_white, 1
); // Лучше если здесь будет фон без слотов


var info_grid = ds_player_info;


draw_set_font(fnt_inv_medium);
var c = c_white;

// Рисуем наименование персанажа
//draw_text_color(info_x, info_y, info_grid[# 0, 3] + ": " + info_grid[# 1, 3], c, c, c, c, 1);


// Рисуем количество денег (в данном случае золото, серебро, медь)
draw_set_font(fnt_inv_small);
var width_str = 0;
var yy = 0; repeat(3){
	var str = ds_player_info[# 0, yy] + " " + string(info_grid[# 1, yy]);
	draw_text_color(info_x + width_str, info_y,  str, c, c, c, c, 1);	
	width_str += string_width(str)  + 10;
	yy++;
}




// Inventory
var ii, ix, iy, xx, yy, sx, sy, iitem, inv_grid;
ii = 0; ix = 0; iy = 0; inv_grid = ds_inventory
repeat (inv_slots){
	// x, y location for slot
	xx = slots_x + ((cell_size + x_buffer) * ix * scale);
	yy = slots_y + ((cell_size + y_buffer) * iy * scale);
	
	// Item
	iitem = inv_grid[# 0, ii];
	sx = (iitem mod spr_inv_items_columns) * cell_size;
	sy = (iitem div spr_inv_items_columns) * cell_size;
	
	// Draw slot and item
	
	
	//draw_sprite_part_ext(
	//	spr_inv_UI, 2, sl_x, sl_y, cell_size, cell_size,
	//	xx, yy, scale, scale, c_white, 1
	//); // Рисуем слоты если на фоне их нет
	
	
	
	switch(ii){
		case selected_slot:
			if (iitem > 0) draw_sprite_part_ext(
				spr_inv_items, 0, sx, sy, cell_size, cell_size,
				xx, yy, scale, scale, c_white, 1
			);
			gpu_set_blendmode(bm_add);
			draw_sprite_part_ext(
				spr_inv_UI, 2, sl_x, sl_y, cell_size, cell_size,
				xx, yy, scale, scale, c_white, 0.3
			);
			gpu_set_blendmode(bm_normal);
		break;
		
		case pickup_slot:
			if (iitem > 0) draw_sprite_part_ext(
				spr_inv_items, 0, sx, sy, cell_size, cell_size,
				xx, yy, scale, scale, c_white, 0.2
			);
		break;
		
		default:
			if (iitem > 0) draw_sprite_part_ext(
				spr_inv_items, 0, sx, sy, cell_size, cell_size,
				xx, yy, scale, scale, c_white, 1
			);
		break;
	}
	
	//draw_sprite_part_ext(
	//	spr_item_focus, 0, 0, 0, sprite_get_width(spr_item_focus), sprite_get_height(spr_item_focus),
	//	xx, yy, scale, scale, c_white, 0.9
	//); // Рисуем стекло над предметами
	
	
	// Draw item number
	c = c_white;
	if (iitem > 0) {
		var number = inv_grid[# 1, ii];
		draw_text_color(xx + cell_size - string_width(number), yy + cell_size - string_height(number), string(number), c, c, c, c, 1);
	}
	
	// Increment
	ii++;
	ix = ii mod inv_slots_width;
	iy = ii div inv_slots_width;


}

// Draw item description
var info_grid = ds_items_info, description = "";
iitem = inv_grid[# 0, selected_slot];

if (iitem > 0){
	draw_set_font(fnt_inv_small);
	description = info_grid[# 0, iitem] + ". "	+ info_grid[# 1, iitem];
	c = c_red;
	draw_text_ext_color(desc_x, desc_y, description, string_height("M"), inv_UI_height * scale - (x_buffer * 2), c,c,c,c, 1);
}


if (pickup_slot != -1){
	// Item
	iitem = inv_grid[# 0, pickup_slot];
	sx = (iitem mod spr_inv_items_columns) * cell_size;
	sy = (iitem div spr_inv_items_columns) * cell_size;
	
	draw_sprite_part_ext(
		spr_inv_items, 0, sx, sy, cell_size, cell_size,
		mousex, mousey, scale, scale, c_white, 1
	);
	
	var inum = inv_grid[# 1, pickup_slot];
	draw_text_color(mousex + (cell_size * scale * 0.5), mousey, string(inum), c, c, c, c, 1);
	
}