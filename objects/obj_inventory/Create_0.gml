depth					= -1;
scale					= 1;
show_inventory			= false;


inv_slots				= 48; // Количество открытых слотов в инвентаре
inv_slots_width			= 6;
inv_slots_height		= 8;

selected_slot			= 0;
pickup_slot				= -1;
m_slotx					= 0;
m_sloty					= 0;


x_buffer				= 6; // Расстояние между слотами ро x
y_buffer				= 4; // Расстояние между слотами ро y

gui_width				= global.view_width;
gui_height				= global.view_height;

cell_size				= 32; // Размер слота (слот должен быть квадратным)

inv_UI_width			= 274;
inv_UI_height			= 424;


spr_inv_UI				= spr_inventory_UI_new;
spr_item_focus			= spr_inventory_UI_new_glass_items;
spr_inv_items			= spr_inventory_items;


spr_inv_items_columns	= sprite_get_width(spr_inv_items)/cell_size;
spr_inv_items_rows		= sprite_get_height(spr_inv_items)/cell_size;


inv_UI_x				= (gui_width * 0.5) - (inv_UI_width * 0.5 * scale); // определяем где будел левый верхний угол по x
inv_UI_y				= (gui_height * 0.5) - (inv_UI_height * 0.5 * scale); // определяем где будел левый верхний угол по y


info_x					= inv_UI_x + (27 * scale); // определяем где будет начинаться описание по x
info_y					= inv_UI_y + (355 * scale); // определяем где будет начинаться описание по y

sl_x					= 27; // координата до левого верхнего угла первого слота по x
sl_y					= 66; // координата до левого верхнего угла первого слота по y

slots_x					= inv_UI_x + (sl_x * scale); //info_x; // Определяем где будет левый верхний угол первого слота по x
slots_y					= inv_UI_y + (sl_y * scale); //inv_UI_y + (40 * scale); // Определяем где будет левый верхний угол первого слота по y


desc_x					=  info_x;
desc_y					=  info_y + (20 * scale);


// Player Info
// 0 = Gold
// 1 = Silver
// 2 = Cooper
// 3 = Name

ds_player_info = ds_grid_create(2, 4);
ds_player_info[# 0, 0]		= "Gold";
ds_player_info[# 0, 1]		= "Silver";
ds_player_info[# 0, 2]		= "Cooper";
ds_player_info[# 0, 3]		= "Name";

ds_player_info[# 1, 0]		= irandom_range(0, 99);
ds_player_info[# 1, 1]		= irandom_range(0, 99);;
ds_player_info[# 1, 2]		= irandom_range(0, 99);;
ds_player_info[# 1, 3]		= "Player";

//===================================================================
// Inventory
// 0 = Item
// 1 = Number
ds_inventory		= ds_grid_create(2, inv_slots);

enum item{
	none			= 0,
	tomato			= 1,
	potato			= 2,
	carron			= 3,
	artichoke		= 4,
	chilli			= 5,
	gourd			= 6,
	corn			= 7,
	wood			= 8,
	stone			= 9,
	bucket			= 10,
	chair			= 11,
	picture			= 12,
	axe				= 13,
	potion			= 14,
	starfish		= 15,
	mushroom		= 16,
	height			= 17,
	
}


#region Create item info grid
ds_items_info = ds_grid_create(2, item.height);

//---- Item names
var z = 0, i = 0;
ds_items_info[# z, i++] = "Nothing. 1";
ds_items_info[# z, i++] = "Nothing. 2";
ds_items_info[# z, i++] = "Nothing. 3";
ds_items_info[# z, i++] = "Nothing. 4";
ds_items_info[# z, i++] = "Nothing. 5";
ds_items_info[# z, i++] = "Nothing. 6";
ds_items_info[# z, i++] = "Nothing. 7";
ds_items_info[# z, i++] = "Nothing. 8";
ds_items_info[# z, i++] = "Nothing. 9";
ds_items_info[# z, i++] = "Nothing. 10";
ds_items_info[# z, i++] = "Nothing. 11";
ds_items_info[# z, i++] = "Nothing. 12";
ds_items_info[# z, i++] = "Nothing. 13";
ds_items_info[# z, i++] = "Nothing. 14";
ds_items_info[# z, i++] = "Nothing. 15";
ds_items_info[# z, i++] = "Nothing. 16";
ds_items_info[# z, i++] = "Grib";


//---- Items descriptions

z = 1; i = 0;
ds_items_info[# z, i++] = "Empty1";
ds_items_info[# z, i++] = "Empty2";
ds_items_info[# z, i++] = "Empty3";
ds_items_info[# z, i++] = "Empty4";
ds_items_info[# z, i++] = "Empty5";
ds_items_info[# z, i++] = "Empty6";
ds_items_info[# z, i++] = "Empty7";
ds_items_info[# z, i++] = "Empty8";
ds_items_info[# z, i++] = "Empty9";
ds_items_info[# z, i++] = "Empty10";
ds_items_info[# z, i++] = "Empty11";
ds_items_info[# z, i++] = "Empty12";
ds_items_info[# z, i++] = "Empty13";
ds_items_info[# z, i++] = "Empty14";
ds_items_info[# z, i++] = "Empty15";
ds_items_info[# z, i++] = "Empty16";
ds_items_info[# z, i++] = "Vojmojno siedoben";


#endregion


var yy = 0;
repeat(inv_slots*0.5){
ds_inventory[# 0, yy] = irandom_range(1, item.height-1);
ds_inventory[# 1, yy] = irandom_range(1, item.height-1);
	yy++;
}
