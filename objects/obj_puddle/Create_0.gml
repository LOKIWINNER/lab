if (layer_exists(layer_get_id("Tiles_Water")) || layer_exists(layer_get_id("Refl_Layer"))){
	refl_start("Refl_Layer", true);				//Начинает отражение в слое «Refl_layer» с включенной камерой.
	refl_water_layer("Tiles_Water", false);	//Устанавливает слой "Tiles_Water", который содержит только водяные плитки, с отключенным обнаружением воды.
	//refl_water_object(obj_puddle);				//Добавляет obj_puddle как водный объект.
	
	if instance_exists(obj_barrel) { refl_add_object(obj_barrel, 0); }
	if instance_exists(obj_hero) { refl_add_object(obj_hero, 0); }
	if instance_exists(obj_tree_all) { refl_add_object(obj_tree_all, 0); }
	if instance_exists(obj_all_enemy) { refl_add_object(obj_all_enemy, 0); }
	if instance_exists(obj_all_NPC) { refl_add_object(obj_all_NPC, 0); }
}