{
    "id": "bee66545-c0a1-434c-aaad-e399c3d3912f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_puddle",
    "eventList": [
        {
            "id": "2b3a88f6-4c04-4666-9616-cd884f212983",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bee66545-c0a1-434c-aaad-e399c3d3912f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3e7bb6f7-6eee-4dfd-be78-d802fd05c38b",
    "visible": false
}