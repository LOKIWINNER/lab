/// Draw Transitions
switch (kind) {
    // Horizontal Stripes
    case "transition.stripe_hor":
        // Sets the color. Can be changed
        draw_set_color(c_black);
        draw_set_alpha(0.9);
		
        // Draws the stripes according to the already defined array variables
        for (var i = stripes; i > 0; i--) {
            draw_rectangle(stripe_one[i], length[i], stripe_one[i] + width, length[i] - height, false);
            draw_rectangle(stripe_two[i], length[i], stripe_two[i] - width, length[i] - height, false);
        }
    break;
    // Vertical Stripes
    case "transition.stripe_ver":
        // Sets the color. Can be changed
        draw_set_color(c_black);
		draw_set_alpha(0.9);
    
        // Draws the stripes according to the already defined array variables
        for (var i = stripes; i > 0; i--) {
            draw_rectangle(length[i], stripe_one[i], length[i] - height, stripe_one[i] + width, false);
            draw_rectangle(length[i], stripe_two[i], length[i] - height, stripe_two[i] - width, false);
        }
    break;
}