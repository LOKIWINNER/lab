{
    "id": "4a33746d-4408-48a7-9c00-00b5e1e16657",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_transition",
    "eventList": [
        {
            "id": "5f3247a5-e692-412c-a47a-e348b5c41e9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a33746d-4408-48a7-9c00-00b5e1e16657"
        },
        {
            "id": "f2351c59-c73e-43be-af0d-87c8a4ef3dce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4a33746d-4408-48a7-9c00-00b5e1e16657"
        },
        {
            "id": "e6dfbef1-4b2f-4b5a-abcc-66a5a1ece32a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "4a33746d-4408-48a7-9c00-00b5e1e16657"
        },
        {
            "id": "294ef0f9-47a4-4011-bfed-d1e6a037176d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4a33746d-4408-48a7-9c00-00b5e1e16657"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}