/// Stripe Effect Transition
stripes = 100;											// Total amount of stripes
anim = false;											// Used to change the time if needed
height = __view_get(e__VW.HView, 0) / stripes;					// Height of each stripe
width = __view_get(e__VW.WView, 0) + (__view_get(e__VW.WView, 0) / 10);	// The width of each stripe
timer = -width;											// Timer will work as the slowest stripe
roomChange = false;										// True when half time has passed
maxTimer = __view_get(e__VW.WView, 0);							// How long the timer will last

// Define all necessary arrays
stripe_one[stripes] = 0;                    // X position for stripe on left side
stripe_two[stripes] = 0;                    // X position for stripe on right side
length[stripes] = 0;                        // Y position for each stripe
stripe_spd[stripes] = 0;                    // Random speed for each stripe

// Alarm is used to change the time to the new one and start transition
alarm[0] = 1;
xx = -1;
yy = -1;