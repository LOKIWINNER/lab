/// Stripe Effect - Finish array and start animation
// Get easy references for all port sizes
x = view_xport[0];
y = view_yport[0];
var stripe_w = __view_get(e__VW.WView, 0);//window_get_width(); 
var stripe_h = __view_get(e__VW.HView, 0);;//window_get_height();

switch (kind) {
    case "transition.stripe_hor":
        maxTimer = window_get_width();
    
        for (var i = stripes; i > 0; i--) {
            stripe_one[i] = x - width;
            stripe_two[i] = (x + stripe_w) + width;
            length[i] = i * (stripe_h / stripes);
            stripe_spd[i] = random_range(width / time, width / time + (time / 8));
        }
    break;
    case "transition.stripe_ver":  
        width = stripe_h + (stripe_h / 5);
        height = stripe_w / stripes;
        timer = y - width;
        maxTimer = window_get_height();
       
        for (var i = stripes; i > 0; i--) {
            stripe_one[i] = y - width;
            stripe_two[i] = (y + stripe_h) + width;
            length[i] = i * (stripe_w / stripes);
            stripe_spd[i] = random_range(width / time, width / time + (time / 8));
        }
    break;
}

// Animation starts after this is true
anim = true;