/// Stripe Effect - Move the stripes
if (anim) {
    // Moves each stripe with their individual speed. Same speed for left and right stripe
    for (var i = stripes; i > 0; i--) {  
        stripe_one[i] += stripe_spd[i];
        stripe_two[i] -= stripe_spd[i];
    }
    
    // Increase timer
    timer += width / time;
        
    // Check if the transition is halfways done
    if (timer + width > maxTimer / 2 && !roomChange) { 
        room_goto(targetRoom); 
		roomChange = true;
		
        if (xx != -1) { 
			obj_hero.x = xx;
			obj_hero_camera.x = xx;	
		}
        if (yy != -1) { 
			obj_hero.y = yy; 
			obj_hero_camera.y = yy;
		}
    }
        
    // Destroy instance if transition is done
    if (timer > maxTimer) { instance_destroy(); }
}