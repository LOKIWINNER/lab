{
    "id": "af0a1a4e-9300-4e80-883c-e5dec4aa5eaf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_engine_teleport",
    "eventList": [
        {
            "id": "3dcba436-d966-466c-9751-c7f67e3a4d78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af0a1a4e-9300-4e80-883c-e5dec4aa5eaf"
        },
        {
            "id": "0b3a5396-77f1-457d-9747-17789f293142",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "af0a1a4e-9300-4e80-883c-e5dec4aa5eaf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d708c672-f164-479e-a513-52b1f901cee5",
    "visible": true
}