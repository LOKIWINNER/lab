{
    "id": "42c12e3c-461c-4920-afe8-f3ccfa393856",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box_par",
    "eventList": [
        {
            "id": "cd89af5e-195d-4f98-8e3a-cd4c4dc88169",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42c12e3c-461c-4920-afe8-f3ccfa393856"
        },
        {
            "id": "9cae3670-576a-4ed2-8698-b26c320d627d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "42c12e3c-461c-4920-afe8-f3ccfa393856"
        },
        {
            "id": "bfc0e236-d01c-4be1-a419-4dba4f1a4a5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42c12e3c-461c-4920-afe8-f3ccfa393856"
        },
        {
            "id": "c0c13509-4ad3-4429-8c03-45addbfca60c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "42c12e3c-461c-4920-afe8-f3ccfa393856"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "33a0636d-b223-4772-9c63-ab61c2a1fbbd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}