event_inherited();
zx = 255;

hp = 100;
sp = 100;
mp = 100;

pause_run	= false;
god_mode	= false; // можем ходить сквозь стены



//Перечисления
enum PLAYER_STATE											// Каждому состоянию присваивается индекс соответствующего скрипта.
{															// Это означает, что для выполнения правильного состояния мы можем использовать
    normal					= PlayerState_Normal,           // script_execute (state), а не оператор switch.
    attack_one				= PlayerState_Attack_One,
	attack_two				= PlayerState_Attack_Two,
	attack_three			= PlayerState_Attack_Three,
	magic_cast				= PlayerState_MagicCast,
	hit						= PlayerState_Hit,
	dead					= PlayerState_Dead,
	
}
enum PLAYER_ACTION											// Это используется просто как способ сообщить, что игрок
{															// делает, чтобы не добавлять анимацию, звук или эффекты
    stand,													// код в сценарии состояния. Это держит все в порядке.
    run,													// Это также позволило бы еще немного гибкости, если вам нужно
    attack_one,												// знаем, что делает игрок.
    attack_two,
	attack_three,
	jump,
	run_shield,
	stand_shield,
	magic_cast
}


// Объекты
object_solid				= obj_solid;                  


//Клавиши
input_left					= false;
input_right					= false;
input_up					= false;
input_down					= false;
input_jump					= false;
input_jump_hold				= false;
input_attack				= false;
input_attack_hold			= false;
input_shield				= false;
input_magic_cast			= false;
input_run					= false;


// Начальные переменные игрока
state						= PLAYER_STATE.normal;      //These variables are used to keep track of the player's
action						= PLAYER_ACTION.stand;      //basic control statistics.
facing						= ceil(random(3));          //The facing variable determines whether the player is facing
attack						= false;

door						= true;		// Можно проходить в двери
run							= true;		// Бег включён
shield						= false;	// Щит убран

// Настройки передвижение

x_speed_					= 0;
y_speed_					= 0;
max_speed_					= 4;
acceleration_				= .1;
run_acceleration_max_		= 2; // начальное значение


//z var
z				= 0;		
z_floor_		= 0;		
z_speed_		= 0;		
z_grav_			= .2;		
z_jump_			= false;	
z_jump_speed_	= 3;
height			= 30;


pos = "";
coll = 0;

// Добавление камеры
instance_create_layer(x, y, "Instances", obj_hero_camera);







draw_E = false;




reset_dialogue_defaults();
myPortrait			= spr_portrait_player;
myVoice				= snd_voice2;
myFont				= fnt_dialogue;
myName				= "Hero";

myPortraitTalk		= spr_portrait_examplechar_mouth;
myPortraitTalk_x	= 26;
myPortraitTalk_y	= 44;
myPortraitIdle		= spr_portrait_examplechar_idle;




//boom_x = 0;
//boom_y = 0;