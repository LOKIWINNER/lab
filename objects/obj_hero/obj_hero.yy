{
    "id": "ea5de280-3734-410f-9c48-54e6996d25e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hero",
    "eventList": [
        {
            "id": "51a1ff05-5607-4a32-930a-bf380bd8f965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        },
        {
            "id": "90da9aee-bde7-4d96-83c9-2643ab845938",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        },
        {
            "id": "f9d929ad-d51a-43a7-8111-d043576b947d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        },
        {
            "id": "78505fbd-50bf-48cb-8444-74a72d93a5c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        },
        {
            "id": "d7979477-ef7b-4c29-bf23-10fdc59749f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 5,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        },
        {
            "id": "4265c406-6018-4947-90df-04d99d53ef32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        },
        {
            "id": "51daeaa1-5243-484e-aaa3-9ae088a05335",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 8,
            "m_owner": "ea5de280-3734-410f-9c48-54e6996d25e4"
        }
    ],
    "maskSpriteId": "cbb72197-0f22-41f8-b592-bea3b485c4b9",
    "overriddenProperties": null,
    "parentObjectId": "fb6b4689-815a-4f7f-8c34-eed94394907e",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d2b6ac1b-f900-45c3-a3cb-0265ee72ac27",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "070be4ab-d26e-4d26-a490-de74c1d0b4b5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "4839afb9-96e0-4013-bb1c-511c0f955fa7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "686a303c-f0dc-4983-aa87-367124901316",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "cbd2cf38-8a08-4225-ab4e-7a2040bab47a",
    "visible": true
}