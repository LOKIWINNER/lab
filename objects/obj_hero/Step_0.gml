
if (instance_exists(sys_transition) || instance_exists(sys_transition_2)) {
	pause_run	= true;
} else {
	pause_run	= false;
}


if ((global.pause) || (pause_run) || (instance_exists(obj_cutscene))){ // || G.d3d_mode){
	clear_all();
	exit;
}
if(instance_exists(obj_textbox)){ exit; }
door		= true;


//Get User Input
input_left			= keyboard_check_direct(global.key_left);                   //This is where you can change your character
input_right			= keyboard_check_direct(global.key_right);                  //controls for your game.
input_up			= keyboard_check_direct(global.key_up);
input_down			= keyboard_check_direct(global.key_down);
input_jump			= keyboard_check_pressed(global.key_jump);
input_jump_hold		= keyboard_check(global.key_jump);
input_magic_cast	= keyboard_check_direct(global.key_magic_cast);
input_attack		= mouse_check_button_pressed(global.key_attack);
input_attack_hold	= mouse_check_button(global.key_attack);
input_run			= keyboard_check_direct(global.key_run);
input_shield		= mouse_check_button(global.key_shield);



if run{
	_x_input = input_right - input_left;
	_y_input = input_down - input_up;
} else {
	_x_input = 0;
	_y_input = 0;
}


x_speed_ += _x_input * acceleration_;
y_speed_ += _y_input * acceleration_;


script_execute(state);





if (sp != 100 && _x_input == 0 && _y_input == 0){
	sp += .5;	
} else {
	sp += .1;	
}
sp = clamp(sp, 0, 100);


if (_y_input > 0) {facing = 3;} else if (_y_input < 0) {facing = 1;}
if (_x_input > 0) {facing = 0;} else if (_x_input < 0) {facing = 2;}

if (instance_exists(self)){
	Check_collision_for_run(god_mode);
}


//Sprite and Animation
image_speed     = 60 / room_speed;
switch(action)
{
    case PLAYER_ACTION.stand:
		switch(facing)                                              
		{
			case 0: sprite_index    = spr_hero_right_idle;		image_xscale	=	1;		/*image_index		= 0;*/	break;
			case 1: sprite_index    = spr_hero_up_idle;			image_xscale	=	1;		/*image_index		= 1;*/	break;
			case 2: sprite_index    = spr_hero_right_idle;		image_xscale	=	-1;		/*image_index		= 2;*/	break;
			case 3: sprite_index    = spr_hero_down_idle;		image_xscale	=	1;		/*image_index		= 3;*/	break;
		}
	break;
	
	case PLAYER_ACTION.stand_shield:
		switch(facing)                                              
		{
			case 0: sprite_index    = spr_hero_right_shield_idle;		image_xscale	=	1;		break;
			case 1: sprite_index    = spr_hero_up_shield_idle;			image_xscale	=	1;		break;
			case 2: sprite_index    = spr_hero_right_shield_idle;		image_xscale	=	-1;		break;
			case 3: sprite_index    = spr_hero_down_shield_idle;		image_xscale	=	1;		break;
		}
	break;
	
	
    
    case PLAYER_ACTION.run:
		switch(facing)                                              
		{
			case 0: 
			sprite_index	=	spr_hero_right_walk;
			image_xscale	=	1;
			break;
			
			case 1: 
			sprite_index	=	spr_hero_up_walk;
			image_xscale	=	1;
			break;
			
			case 2: 
			sprite_index	=	spr_hero_right_walk;
			image_xscale	=	-1;
			break;
			
			case 3: 
			sprite_index	=	spr_hero_down_walk;		
			image_xscale	=	1;
			break;
		}
	
	var image_speed_fix_x = 0.1, image_speed_fix_y = 0.1;
	if (_x_input != 0) image_speed_fix_x = lerp(abs(x_speed_), 1, .1);	
	else if (_y_input != 0) image_speed_fix_y = lerp(abs(y_speed_), 1, .1);
	image_speed     = 60 / room_speed + image_speed_fix_x * image_speed_fix_y;

	break;
	
	
	
	
	
	case PLAYER_ACTION.run_shield:
		switch(facing)                                              
		{
			case 0: 
			sprite_index	=	spr_hero_right_shield_walk;
			image_xscale	=	1;
			break;
			
			case 1: 
			sprite_index	=	spr_hero_up_shield_walk;
			image_xscale	=	1;
			break;
			
			case 2: 
			sprite_index	=	spr_hero_right_shield_walk;
			image_xscale	=	-1;
			break;
			
			case 3: 
			sprite_index	=	spr_hero_down_shield_walk;		
			image_xscale	=	1;
			break;
		}
	break;
		
	
	case PLAYER_ACTION.jump:
		image_speed     = 0;
        switch(facing)                                              
		{
			case 0: 
			sprite_index	=	spr_hero_right_jump;
			image_xscale	=	1;
			if (z_speed_ > 0){image_index = 1;} else {image_index = 0;}		
			
			break;
			case 1: 
			sprite_index	=	spr_hero_up_jump;
			if (z_speed_ > 0){image_index = 1;} else {image_index = 0;}		
			
			break;
			case 2: 
			sprite_index	=	spr_hero_right_jump;
			image_xscale	=	-1;
			if (z_speed_ > 0){image_index = 1;} else {image_index = 0;}	
			
			break;
			case 3: 
			sprite_index	=	spr_hero_down_jump;
			if (z_speed_ > 0){image_index = 1;} else {image_index = 0;}			
			
			break;
		}
	image_speed     = 50 / room_speed;
	break;
	
	case PLAYER_ACTION.attack_one:
        switch(facing)                                              
		{
			case 0: 
			sprite_index	=	spr_hero_right_attack_one;
			image_xscale	=	1;
			
			break;
			case 1: 
			sprite_index	=	spr_hero_up_attack_one;			
			
			break;
			case 2: 
			sprite_index	=	spr_hero_right_attack_one;
			image_xscale	=	-1;
			
			break;
			case 3: 
			sprite_index	=	spr_hero_down_attack_one;		
			
			break;
		}
	image_speed     = 50 / room_speed;
	break;
	
	case PLAYER_ACTION.attack_two:
        switch(facing)                                              
		{
			case 0: 
			sprite_index	=	spr_hero_right_attack_two;
			image_xscale	=	1;
			
			break;
			case 1: 
			sprite_index	=	spr_hero_up_attack_two;			
			
			break;
			case 2: 
			sprite_index	=	spr_hero_right_attack_two;
			image_xscale	=	-1;
			
			break;
			case 3: 
			sprite_index	=	spr_hero_down_attack_two;		
			
			break;
		}
	image_speed     = 50 / room_speed;
	break;
	
	
	
	case PLAYER_ACTION.attack_three:
        switch(facing)                                              
		{
			case 0: 
			sprite_index	=	spr_hero_right_attack_three;
			image_xscale	=	1;
			
			break;
			case 1: 
			sprite_index	=	spr_hero_up_attack_three;			
			
			break;
			case 2: 
			sprite_index	=	spr_hero_right_attack_three;
			image_xscale	=	-1;
			
			break;
			case 3: 
			sprite_index	=	spr_hero_down_attack_three;		
			
			break;
		}
	image_speed     = 50 / room_speed;
	break;
	
}







