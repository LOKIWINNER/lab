{
    "id": "762631cd-d91f-4c54-9d52-a93c29df0000",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_knight_1",
    "eventList": [
        {
            "id": "d70f155a-c355-4bc4-a687-4c731566c766",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        },
        {
            "id": "230f3b28-2be5-43f7-81b1-fe5020f53960",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        },
        {
            "id": "288a3771-4844-4141-9aaf-c69dc04eefea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        },
        {
            "id": "cc7ad86c-526c-4907-b865-86cb01db5d62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        },
        {
            "id": "1b21e72d-04fb-4570-8fac-cccad82918dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        },
        {
            "id": "0aade39f-3753-436e-b90b-9f6cef6e07d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        },
        {
            "id": "4f202d6d-5605-4490-b50e-34cd657a5f5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "762631cd-d91f-4c54-9d52-a93c29df0000"
        }
    ],
    "maskSpriteId": "cbb72197-0f22-41f8-b592-bea3b485c4b9",
    "overriddenProperties": null,
    "parentObjectId": "782de373-6e97-4168-992b-48a470810795",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
    "visible": true
}