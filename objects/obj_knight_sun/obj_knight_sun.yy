{
    "id": "0dea97ce-2fd3-466b-8e13-dbdcb21c6ff3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_knight_sun",
    "eventList": [
        {
            "id": "364d9ffc-02ba-4486-833f-3717f20e6ecb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0dea97ce-2fd3-466b-8e13-dbdcb21c6ff3"
        },
        {
            "id": "ae26924a-8bc3-417e-b8d3-0c7b107e2622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0dea97ce-2fd3-466b-8e13-dbdcb21c6ff3"
        },
        {
            "id": "7ce2761e-c9ad-447c-b4d3-4fa2b71a3137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0dea97ce-2fd3-466b-8e13-dbdcb21c6ff3"
        },
        {
            "id": "91223a06-7c56-4fad-ad23-72a8011a6e19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0dea97ce-2fd3-466b-8e13-dbdcb21c6ff3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4aab45b5-e4a8-4f3d-b6b1-61c0f18605ef",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "3f6c2c68-017f-4374-8eed-ba7d235bda9b",
    "visible": true
}