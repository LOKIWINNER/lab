if (drop_move){
	x = lerp(x, goal_x, 0.1);
	y = lerp(y, goal_y, 0.1);
	if (abs(x - goal_x) < 1 and abs(y - goal_y) < 1){
		drop_move = false;	
	}
} else {
	//if (!pick_item) exit;
	//	pick_up_move = true;	

	if (!pick_up_move) exit;
	
	var px = obj_hero.x;
	var py = obj_hero.y;
	//var r = 32;
	//if (point_in_rectangle(px, py, x-r*2, y-r*1.5, x+r*2, y+r*1.5)){
		
		// Are we on top of the player
		r = 4;
		if (!point_in_rectangle(px, py, x-r*2, y-r*1.5, x+r*2, y+r*1.5)){
			// move toward the player for pickup
			x = lerp(x, px, 0.2);
			y = lerp(y, py, 0.2);
		} else { 
			// pickup item
			var in = item_num;
			
			with(obj_inventory){
				
				var ds_inv = ds_inventory;
				var picked_up = false;
				
				// chek if item exists in inventory alredy
				var yy = 0; repeat (inv_slots){
					if (ds_inv[# 0, yy] == in){
						ds_inv[# 1, yy] += 1;
						picked_up = true;
						break;
					} else {
						yy+=1;	
					}
				}
				
				// otherwise, add item to an empty slot if there is one
				if (!picked_up){
					yy = 0; repeat (inv_slots){
						if (ds_inv[# 0, yy] == item.none){
							ds_inv[# 0, yy] = in;
							ds_inv[# 1, yy] += 1;
							picked_up = true;
							break;
						} else {
							yy+=1;	
						}
					}	
				}
			}
			// Destroy item if picked_up
			if (picked_up){
				//pick_item = false;
				pick_up_move = false;
				name = obj_inventory.ds_items_info[# 0, in];
				instance_destroy();
				show_debug_message("Picked up an item.")
			}
		}
	//}
}