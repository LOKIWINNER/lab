{
    "id": "cd54db8a-1b23-4175-9f3b-341c8080c7ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trigger",
    "eventList": [
        {
            "id": "f8a48178-4077-4172-ae15-757939492bbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd54db8a-1b23-4175-9f3b-341c8080c7ea"
        },
        {
            "id": "cb8c16dd-772a-415b-9fe9-9c3cc2420ea0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd54db8a-1b23-4175-9f3b-341c8080c7ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "014ad170-2112-438d-979e-0f3ca22c81a4",
    "visible": true
}