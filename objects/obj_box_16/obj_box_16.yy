{
    "id": "862b432e-8fb1-4c2d-aac8-6ad8e75484d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box_16",
    "eventList": [
        {
            "id": "c14d96c2-8263-4d6e-b715-11fa17e614aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "862b432e-8fb1-4c2d-aac8-6ad8e75484d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42c12e3c-461c-4920-afe8-f3ccfa393856",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "df15591f-f68a-44b7-ab33-666a8bb8e667",
    "visible": true
}