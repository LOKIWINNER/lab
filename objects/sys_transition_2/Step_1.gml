/// Transition
step++;

if (step > time) { instance_destroy(); }

if (!surface_exists(surf_start) || !surface_exists(surf_end)) {
    if (room != targetRoom) { room_goto(targetRoom); }
    instance_destroy();
}