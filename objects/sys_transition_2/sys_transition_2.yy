{
    "id": "228143ee-318b-4787-9811-b6704261f82a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys_transition_2",
    "eventList": [
        {
            "id": "e410a1d3-82c4-4038-91a8-2f1ec5eb1a43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "228143ee-318b-4787-9811-b6704261f82a"
        },
        {
            "id": "edfb8741-58da-453e-be0c-766638e07ed7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "228143ee-318b-4787-9811-b6704261f82a"
        },
        {
            "id": "edfa19fe-4469-4a74-a2e8-816b4160d1d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "228143ee-318b-4787-9811-b6704261f82a"
        },
        {
            "id": "59c2e677-b15b-413d-8e6a-76fdcf40534b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "228143ee-318b-4787-9811-b6704261f82a"
        },
        {
            "id": "ca21bfd9-d747-4c0c-a162-af02d4b59626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "228143ee-318b-4787-9811-b6704261f82a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}