{
    "id": "4aab45b5-e4a8-4f3d-b6b1-61c0f18605ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_all_NPC",
    "eventList": [
        {
            "id": "bf43608d-ce84-410d-8d79-272de3a7d2b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4aab45b5-e4a8-4f3d-b6b1-61c0f18605ef"
        },
        {
            "id": "67fdb5bf-2de3-4a7d-b1e6-65657d5d22ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4aab45b5-e4a8-4f3d-b6b1-61c0f18605ef"
        },
        {
            "id": "0fe5e151-e708-446b-b42b-2a46218a8f34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4aab45b5-e4a8-4f3d-b6b1-61c0f18605ef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bacfa89f-a1ce-4276-93c3-7cba4cb8ddb1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}