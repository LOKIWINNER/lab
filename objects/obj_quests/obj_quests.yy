{
    "id": "44ad21a4-299f-47d5-9b1a-922b47fdd355",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quests",
    "eventList": [
        {
            "id": "5c3aeea4-e3d4-4502-9f67-51f2abfc7d34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44ad21a4-299f-47d5-9b1a-922b47fdd355"
        },
        {
            "id": "c79705c0-551a-4345-bfed-d4af9fb551d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44ad21a4-299f-47d5-9b1a-922b47fdd355"
        },
        {
            "id": "d01a8016-bbae-4675-a21b-6b7b6480abcb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "44ad21a4-299f-47d5-9b1a-922b47fdd355"
        },
        {
            "id": "3b1b5a27-e7a0-4fbe-893d-a706f51f7035",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "44ad21a4-299f-47d5-9b1a-922b47fdd355"
        },
        {
            "id": "855fde1c-ab78-4f75-8471-ead0927556ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "44ad21a4-299f-47d5-9b1a-922b47fdd355"
        },
        {
            "id": "21a420f1-4a41-427b-8893-183742a52a88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "44ad21a4-299f-47d5-9b1a-922b47fdd355"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2654bfe5-157c-43ab-be0f-0ac80775f1ed",
    "visible": true
}