
// Это позволяло выводить квесты поверх паузы
/*


draw_set_valign(fa_top);
draw_set_halign(fa_left);

var grid = ds_quests;
var ds_quests_number = ds_grid_width(grid);

var str = "";
var i = 0; repeat(ds_quests_number){
	var stage = grid[# i, 1]; // Переменная хранит статус квеста
	
	if (!global.pause) {
		var vis = (stage == 1); // Если нет паузы, то выводим только те цены, что имеют статус 1
	} else {
		var vis = (stage != -1); // Если пауза, то выводим все цены
	}
	
	if (vis){
		if (i != 0) str += "\n ------------------------ \n";
		switch (grid[# i, 2]) {
			case 0:
			#region // quest.kill
				str += string_upper(grid[# i, 4]) + "\n";
				str += "Тебе нужно убить ";
				var _grid	= grid[# i, 5];
				var _grid2	= grid[# i, 6];
				var _grid3	= grid[# i, 7];
				var _ds_map = obj_statistics.ds_map_enemy;
				var _num = array_length_1d(_grid);
				if (is_array(_grid) || _num > 1){
					var kill_now = ds_map_find_value(_ds_map, _grid[0]) - _grid3[0];
					str += object_get_name(_grid[@ 0]) + " (" + string(kill_now) + "/" + string(_grid2[@ 0]) + ")";
					
					var ii = 1; repeat(_num-1){
						var kill_now = ds_map_find_value(_ds_map, _grid[ii]) - _grid3[ii];
						str += " и " + object_get_name(_grid[@ ii]) + " (" + string(kill_now) + "/" + string(_grid2[@ ii]) + ")";
						
						ii++;
					}
				
				} else {
					var kill_now = ds_map_find_value(_ds_map, _grid) - _grid3;
					str += object_get_name(_grid) + " (" + string(kill_now) + "/" + string(_grid2) + ")";
					
				}
				
			#endregion
			break;
			
			case 1:
			#region // quest.picking
				str += string_upper(grid[# i, 4]) + "\n";
				str += "Тебе нужно собрать ";
				var _grid	= grid[# i, 5];
				var _grid2	= grid[# i, 6];
				var _num = array_length_1d(_grid);

				if (is_array(_grid) || _num > 1){
					
					var ii = 0; repeat(_num){
						str += string(_grid[@ ii]) + " - ";
						ii++;
					}
				} else {
					str += string(_grid) + " - ";
				}

			#endregion
			break;
			
			case 2:
			#region // quest.escort
				str += string_upper(grid[# i, 4]) + "\n";
				str += "Тебе нужно сопроводить ";
				var _name = grid[# i, 3].myName;
				var _grid = grid[# i, 5];
				var _grid2 = grid[# i, 6];
				str += _name + ". ";
				str += "Сопроводить до " + _grid2[@ 1].myName;
				
			#endregion
			break;
		}
	}
	i++;
	
	
}
draw_set_font(fnt_engine);
draw_text_transformed(10, 10, str, .5, .5, 0);

*/