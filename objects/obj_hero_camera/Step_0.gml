if keyboard_check_released(ord("L")){
	G.d3d_mode = !G.d3d_mode;
	cam = cam_change(G.d3d_mode);
}

if G.d3d_mode {

	// === Basic Cam Movement === //
	dir-=(display_mouse_get_x()-display_get_width()/2)/10;
	pitch=clamp(pitch-(display_mouse_get_y()-display_get_height()/2)/10, -80, 80);
	display_mouse_set(display_get_width()/2, display_get_height()/2);
	
	var _s = spd,
		_xi = keyboard_check(vk_right)-keyboard_check(vk_left),
		_yi = keyboard_check(vk_down)-keyboard_check(vk_up),
		_zi = (mouse_wheel_down()-mouse_wheel_up())*2;
	x+=_xi*dsin(dir)*_s-_yi*dcos(dir)*_s;
	y+=_xi*dcos(dir)*_s+_yi*dsin(dir)*_s;
	z+=_zi*_s;

} else {

	if not instance_exists(target_) exit;
	x = lerp(x, target_.x, .1);
	y = lerp(y, target_.y-8, .1);
	x = round_n(x, scale_);
	y = round_n(y, scale_);
	
	if (room_get_name(room) != "room_Main"){
		x = clamp(x, width_/2 + CELL_WIDTH, room_width-width_/2 - CELL_WIDTH);
		y = clamp(y, height_/2 + CELL_HEIGHT, room_height-height_/2 - CELL_HEIGHT);
	} else {
		x = clamp(x, width_/2, room_width-width_/2);
		y = clamp(y, height_/2, room_height-height_/2);
	}
	camera_set_view_pos(view_camera[0], x-width_/2, y-height_/2);

}