{
    "id": "72c8c7bb-cbf7-4d21-86a5-b1eb3aa65107",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_engine",
    "eventList": [
        {
            "id": "b618865f-8f53-40b5-a48d-d5bea5245e6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "72c8c7bb-cbf7-4d21-86a5-b1eb3aa65107"
        },
        {
            "id": "91f4bc5e-f172-4f4e-bafa-da4828618024",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "72c8c7bb-cbf7-4d21-86a5-b1eb3aa65107"
        },
        {
            "id": "003c3ee8-bc6c-4801-b809-b47b61a0dd0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "72c8c7bb-cbf7-4d21-86a5-b1eb3aa65107"
        },
        {
            "id": "365b5cf7-7b03-404d-84ca-c87ac6541e6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "72c8c7bb-cbf7-4d21-86a5-b1eb3aa65107"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}