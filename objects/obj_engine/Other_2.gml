
// Инициализация объекта двигателя

SL_engine_ini_begin(); // Начинается инициализация системы

sl_sunshadows_active = false; // Тень Активной солнечный рендеринга
sl_ambientshadows_active = false; // Активная тень окружающей среды рендеринга

sl_timespeed = (1/60)/(room_speed/9); // Прогресс Speed Clock
sl_maxexposure = 0.3; // Максимальный коэффициент насыщения лампы

// Создание окружающей среды карты
//SL_sprite_set_ambient(spr_player);
SL_sprite_set_ambient(obj_solid);
SL_sprite_set_ambient(spr_barrel);

// Перечень объектов проектирования глобальной тени
//SL_global_cast_obj(2,obj_player,-1,1,1);
SL_global_cast_obj(1,obj_solid,-1,1,1);
SL_global_cast_obj(0,obj_barrel,-1,1,1);

// Список световых объектов
SL_add_light(obj_light);
SL_add_light(obj_player);

SL_ToD_default(); // Инициализация ToD
SL_set_time(15); // время инициализации

SL_engine_ini_end(); // Завершает инициализацию системы