{
    "id": "a09ab07d-be35-45a4-81bb-3b1744efeccf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_torch",
    "eventList": [
        {
            "id": "f418aa90-dce2-48f2-9ff8-f52fdf2d58bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a09ab07d-be35-45a4-81bb-3b1744efeccf"
        },
        {
            "id": "f859db51-0ad9-4cb8-9220-7de60fa1cae5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a09ab07d-be35-45a4-81bb-3b1744efeccf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f221d0b6-cc0a-45ac-b33c-c86258e33dc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed2e5efb-cf00-4f6c-acf9-647d00e593ce",
    "visible": true
}