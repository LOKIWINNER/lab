
gpu_set_blendmode_ext(bm_dest_alpha, bm_inv_dest_alpha);
gpu_set_alphatestenable(true);

//Draw Player Silhouette


with(obj_parent_silhouette){
	switch(object_index){
		case obj_hero: 
			gpu_set_fog(true, merge_color(c_black, c_maroon, .5), 0, 1); 
			if (self.z > 0) {
				draw_sprite_ext(sprite_index, 2, round(x), round(y), image_xscale, image_yscale, image_angle, c_white, 1);
			}
			draw_sprite_ext(sprite_index, image_index, round(x), round(y) - z, image_xscale, image_yscale, image_angle, c_white, 1);
		break;
		
		case obj_enemy_knight_1: gpu_set_fog(true, c_black, 0, 1); 
		draw_self();
		break;
		
		default: gpu_set_fog(true, c_black, 0, 1); 
		draw_self();
		break;
	}

}

with(obj_box_par){
	gpu_set_fog(true, c_black, 0, 1); 
		draw_self();
}


gpu_set_fog(false, c_white, 0, 0);

gpu_set_alphatestenable(false);
gpu_set_blendmode(bm_normal);