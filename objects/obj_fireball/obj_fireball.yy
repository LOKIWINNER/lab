{
    "id": "089a0d48-81d6-4310-b759-7a8c376c1080",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fireball",
    "eventList": [
        {
            "id": "d04d5ef3-80da-4bd9-ab87-669949ebf199",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "089a0d48-81d6-4310-b759-7a8c376c1080"
        },
        {
            "id": "380e7111-a55c-4054-82a1-19a01066f97b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "089a0d48-81d6-4310-b759-7a8c376c1080"
        },
        {
            "id": "34475185-1359-466b-9e36-5c9d3c347fa6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "089a0d48-81d6-4310-b759-7a8c376c1080"
        },
        {
            "id": "9506505f-859f-48bc-83bd-5d5c7a3387e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "762631cd-d91f-4c54-9d52-a93c29df0000",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "089a0d48-81d6-4310-b759-7a8c376c1080"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f221d0b6-cc0a-45ac-b33c-c86258e33dc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2d1998c-a148-4d0b-88d0-75eb668ff0b8",
    "visible": true
}