{
    "id": "8395c6ec-14ec-4d6c-8710-8e0b9f37ba6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_maze_generator",
    "eventList": [
        {
            "id": "4510af54-ed3f-498a-8929-472ebb65acc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8395c6ec-14ec-4d6c-8710-8e0b9f37ba6b"
        },
        {
            "id": "648c317c-3c15-4c08-8ef1-64d6bedf3f6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8395c6ec-14ec-4d6c-8710-8e0b9f37ba6b"
        },
        {
            "id": "a4a2d137-23e6-4331-b5ca-ad72c7d7e770",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "8395c6ec-14ec-4d6c-8710-8e0b9f37ba6b"
        },
        {
            "id": "48fe377d-6931-4da1-aba3-de956679a688",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "8395c6ec-14ec-4d6c-8710-8e0b9f37ba6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}