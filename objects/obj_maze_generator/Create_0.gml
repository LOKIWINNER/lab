randomize();

shadow_surface_		= noone;
var wall			= 1;

dungeon_grid_col	= 31;					// Обязательно чтобы параметры были нечётными
dungeon_grid_row	= 21;					// Обязательно чтобы параметры были нечётными
dungeon_fix_w		= 3;					// Обязательно чтобы параметры были нечётными
dungeon_fix_h		= dungeon_fix_w;		// Обязательно чтобы параметры были нечётными


world_width			= dungeon_grid_col * dungeon_fix_w * CELL_WIDTH;
world_height		= dungeon_grid_row * dungeon_fix_h * CELL_HEIGHT;

dungeon				= ds_grid_create(dungeon_grid_col,dungeon_grid_row);	// Желательно чтобы параметры были нечётными
dungeon_new			= ds_grid_create(ds_grid_width(dungeon)*dungeon_fix_w,ds_grid_height(dungeon)*dungeon_fix_h);


if (room_get_name(room) = "room_Main") exit;

room_width			= world_width;			//ширина спрайта без отступов
room_height			= world_height;			//высота спрайта без отступов



ds_grid_clear(dungeon,0);
ds_grid_clear(dungeon_new,0);
dungeon_carve(dungeon,0,wall,1,1);


var _wall_map_id	= layer_tilemap_get_id("WallTiles");
var dungeon_grid	= dungeon_new;
var ww				= ds_grid_width(dungeon_grid);
var hh				= ds_grid_height(dungeon_grid);


for(i = 0; i < ww;i++){
    for(j = 0; j < hh;j++){
		if(dungeon_grid[#i,j] == wall){
	
			var qone			= (i < ww-1) && (dungeon_grid[#i+1,j]		!= wall);		// право
			var qtwo			= (j > 0) && (dungeon_grid[#i,j-1]			!= wall);		// верх
			var qthree			= (i > 0) && (dungeon_grid[#i-1,j]			!= wall);		// лево
			var qfour			= (j < hh-1) && (dungeon_grid[#i,j+1]		!= wall);		// низ
			
			var www				= (i < ww-1) * (i > 0);
			var hhh				= (j < hh-1) * (j > 0);
			
			var _tile_index		= qone+2*qtwo+4*qthree+8*qfour + 1;
			
			
			tilemap_set(_wall_map_id, _tile_index, i,j);
			if (www && hhh){
				if (!dungeon_grid[# i-1,j-1] && !qone && !qtwo && !qthree && !qfour){		// лево-верх
					tilemap_set(_wall_map_id, 19, i,j);										
				}																			
				if (!dungeon_grid[# i+1,j-1] && !qone && !qtwo && !qthree && !qfour){		//право-верх
					tilemap_set(_wall_map_id, 20, i,j);										
				}																			
				if (!dungeon_grid[# i-1,j+1] && !qone && !qtwo && !qthree && !qfour){		//право-низ
					tilemap_set(_wall_map_id, 17, i,j);										
				}																			
				if (!dungeon_grid[# i+1,j+1] && !qone && !qtwo && !qthree && !qfour){		//лево-низ
					tilemap_set(_wall_map_id, 18, i,j);
				}
			}
		}

    }
}
			


