	
var hitByAttackNow = ds_list_create();
var hits = instance_place_list(x, y, obj, hitByAttackNow, false);
var _obj = obj;
var _damage = damage;
var _inst = inst;


if (hits > 0){
	for (var i = 0; i < hits; i++){
		// Если этот экземпляр еще не был поражен этой атакой
		var hitID = hitByAttackNow[| i];
		if (ds_list_find_index(hitByAttack, hitID) == -1){
			
			ds_list_add(hitByAttack,hitID);
			with (hitID){
					EnemyHit(_obj, _damage, _inst);
				
			}
		}
	}
}
ds_list_destroy(hitByAttackNow);