{
    "id": "2843c9f0-c417-4b33-9bce-f8d8948fc00b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hero_attack",
    "eventList": [
        {
            "id": "3856c0d6-b0b4-4bab-af03-24e263180649",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2843c9f0-c417-4b33-9bce-f8d8948fc00b"
        },
        {
            "id": "9f62f867-06ef-4fe9-a13a-93b30e51a47a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2843c9f0-c417-4b33-9bce-f8d8948fc00b"
        },
        {
            "id": "35c6c0b9-9d1e-4449-909d-97f5ab4edf21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "2843c9f0-c417-4b33-9bce-f8d8948fc00b"
        },
        {
            "id": "63fca44a-7b46-4d36-bc08-de4f1f7492b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2843c9f0-c417-4b33-9bce-f8d8948fc00b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f52e5662-c6fb-451a-b947-af5578c54cb8",
    "visible": true
}