{
    "id": "466912c9-f15b-4fb7-bb86-8879e24ab47d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light",
    "eventList": [
        {
            "id": "d0350704-d94e-4819-8e6b-6af3ff2c0bb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "466912c9-f15b-4fb7-bb86-8879e24ab47d"
        },
        {
            "id": "f3a5c0c6-81cb-4197-9f38-331f6c780e92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "466912c9-f15b-4fb7-bb86-8879e24ab47d"
        },
        {
            "id": "af1526b1-7d15-4bcd-a7cc-f2523e9974c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "466912c9-f15b-4fb7-bb86-8879e24ab47d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dfa2b25f-af7f-4bf7-8096-68cae28e35aa",
    "visible": true
}