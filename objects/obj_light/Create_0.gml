
SL_light_ini_begin(); // Начинает инициализацию объекта света

sl_light_texture = spr_light; // Текстура света
sl_light_xscale = 1; // Факторы растяжения х и у света
sl_light_yscale = 1;
sl_light_shadowsharpness = 0.9; // Фактор тени твердости

// Определяют объекты на свет «окклюдер»
SL_light_cast_obj(obj_barrel,-1);
SL_light_cast_obj(obj_crate,-1);
SL_light_cast_obj(obj_player,-1);

SL_light_ini_end(); // Завершает инициализацию объекта света