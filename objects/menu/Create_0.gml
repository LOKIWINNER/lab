global.pause		= false;
global.view_width	= camera_get_view_width(view_camera[0]);
global.view_height	= camera_get_view_height(view_camera[0]);

global.key_revent			= ord("X");
global.key_enter			= vk_enter;
global.key_left				= ord("A");
global.key_right			= ord("D");
global.key_up				= ord("W");
global.key_down				= ord("S");
global.key_jump				= vk_space;
global.key_magic_cast		= ord("F");
global.key_attack			= mb_left;
global.key_shield			= mb_right;
global.key_run				= vk_shift;

display_set_gui_size(global.view_width, global.view_height);


enum menu_page {
	main,
	settings,
	audio,
	difficulty,
	graphics,
	controls,
	height
}


enum menu_element_type {
	script_runner,
	page_transfer,
	slider,
	shift,
	toggle,
	input
}

//CREATE MENU PAGES

ds_menu_main = create_menu_page(
	["RESUME",		menu_element_type.script_runner,	resume_game],
	["SETINGS",		menu_element_type.page_transfer,	menu_page.settings],
	["EXIT",		menu_element_type.script_runner,	exit_game]
);


ds_menu_settings = create_menu_page(
	["AUDIO",		menu_element_type.page_transfer,	menu_page.audio],
	["DIFFICULTY",	menu_element_type.page_transfer,	menu_page.difficulty],
	["GRAPHICS",	menu_element_type.page_transfer,	menu_page.graphics],
	["CONTROLS",	menu_element_type.page_transfer,	menu_page.controls],
	["BACK",		menu_element_type.page_transfer,	menu_page.main],
);


ds_menu_audio = create_menu_page(
	["MASTER",		menu_element_type.slider,			change_volume,			0.5,	[0, 1]],
	["SOUNDS",		menu_element_type.slider,			change_volume,			0.5,	[0, 1]],
	["MUSIC",		menu_element_type.slider,			change_volume,			0.5,	[0, 1]],
	["BACK",		menu_element_type.page_transfer,	menu_page.settings],
);

ds_menu_difficulty = create_menu_page(
	["ENEMIES",		menu_element_type.shift,			change_difficulty,		0,		["HARMLESS", "NORMAL", "TERRIBLE"]],
	["ALLIES",		menu_element_type.shift,			change_difficulty,		0,		["DIM-WITTED", "NORMAL", "HELPFUL"]],
	["BACK",		menu_element_type.page_transfer,	menu_page.settings],
);


ds_menu_graphics = create_menu_page(
	["RESOLUTION",		menu_element_type.shift,			change_resolution,		0,		["1920 x 1080", "1680 x 1050", "1600 x 1024", "1600 x 900", "1400 x 990", "1366 x 768", "1280 x 1024", "1280 x 800", "1280 x 720", "1024 x 768", "640 x 480"]],
	["WINDOW MODE",		menu_element_type.toggle,			change_window_mode,		0,		["FULLSCREEN", "WINDOWED"]],
	["BACK",			menu_element_type.page_transfer,	menu_page.settings],
);


ds_menu_controls = create_menu_page(
	["UP",			menu_element_type.input,				"key_up",						ord("W")],
	["LEFT",		menu_element_type.input,				"key_left",						ord("A")],
	["RIGHT",		menu_element_type.input,				"key_right",					ord("D")],
	["DOWN",		menu_element_type.input,				"key_down",						ord("S")],
	["ATTACK",		menu_element_type.input,				"key_attack",					mb_left],
	["RUN",			menu_element_type.input,				"key_run",						vk_shift],
	["MAGIC",		menu_element_type.input,				"key_jump",						vk_space],
	["MAGIC",		menu_element_type.input,				"key_magic_cast",				ord("E")],
	["BACK",		menu_element_type.page_transfer,		menu_page.settings],
);

page = 0;
menu_pages = [ds_menu_main, ds_menu_settings, ds_menu_audio, ds_menu_difficulty, ds_menu_graphics, ds_menu_controls];

var i = 0, array_len = array_length_1d(menu_pages);
repeat(array_len){
	menu_option[i] = 0; 
	i++;
}

inputing = false;








