{
    "id": "bd46f2ec-d52e-46c5-8492-fcb66d788c6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menu",
    "eventList": [
        {
            "id": "f9c717d0-1435-4fae-af48-a74d010bc155",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd46f2ec-d52e-46c5-8492-fcb66d788c6a"
        },
        {
            "id": "abf16ecb-8dc2-4888-ae49-fdc9384a32d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "bd46f2ec-d52e-46c5-8492-fcb66d788c6a"
        },
        {
            "id": "75f2c250-2d82-414a-b23d-ad5444185e1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bd46f2ec-d52e-46c5-8492-fcb66d788c6a"
        },
        {
            "id": "d4d7fa7b-dd8c-4fb3-9248-90d5795f8c31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd46f2ec-d52e-46c5-8492-fcb66d788c6a"
        },
        {
            "id": "4612c6f7-9804-4a7e-86b1-43dfaff07a8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 8,
            "m_owner": "bd46f2ec-d52e-46c5-8492-fcb66d788c6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}