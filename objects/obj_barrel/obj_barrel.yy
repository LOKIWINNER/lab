{
    "id": "aa71d1fc-185b-4de5-b92a-22398b659fa2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_barrel",
    "eventList": [
        {
            "id": "921e8290-4d25-4e58-9c92-ad9f14424e02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa71d1fc-185b-4de5-b92a-22398b659fa2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f221d0b6-cc0a-45ac-b33c-c86258e33dc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "561c82d9-7485-416d-85df-4c3f32cc8100",
    "visible": true
}