{
    "id": "b6055fe6-6c5b-4e6a-b718-f0ce218a7740",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box_8",
    "eventList": [
        {
            "id": "5d907a4a-f98b-47a5-90ed-910487c433cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6055fe6-6c5b-4e6a-b718-f0ce218a7740"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42c12e3c-461c-4920-afe8-f3ccfa393856",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f175b7e5-f425-4655-a72a-9dddb144ec31",
    "visible": true
}