/// @description 
//------YOU MAY TWEAK THESE VARIABLES----------------------
//Water waves
//Вам нужно будет настроить эти переменные методом проб и ошибок, чтобы найти лучшие настройки.
//это подходит вам по вкусу.
global.wave_speed = 1; //Скорость волновой анимации
global.wave_size = 1; //Размер волн по горизонтали
global.wave_frequency = 1; //Частота; Сколько волн создано за один раз

global.vertical_wave_size = 2; //Размер вертикальных волн
global.vertical_wave_frequency = 5; //Частота вертикальных волн

//Blur
global.water_blur = false; //Стоит ли размыть отражения
global.water_blur_power = 4; //Мощность размытия, если размытие включено

//Water other
global.water_blend = c_white; //Смешайте цвет для отражений
							 //Измените на c_white, если вы не хотите смешивать
//--------END----------------------------------------------
#region internal
global.refl_insts = ds_list_create();
global.refl_offsets = ds_list_create();

global.water_insts = ds_list_create();
global.water_tile = -1;

global.camera_used = false;
global.water_detection = false;

delayed_create = true;


//shader
uni_uvs = shader_get_uniform(sh_refl, "uvs");
uni_time = shader_get_uniform(sh_refl, "time");
uni_texel = shader_get_uniform(sh_refl, "texel");

uni_slow = shader_get_uniform(sh_refl, "slow");
uni_amount = shader_get_uniform(sh_refl, "amount");
uni_waves = shader_get_uniform(sh_refl, "waves");
uni_y_amount = shader_get_uniform(sh_refl, "y_amount");
uni_y_waves = shader_get_uniform(sh_refl, "y_waves");

uni_offset = shader_get_uniform(sh_refl, "offset");

uni_detect = shader_get_uniform(sh_refl, "detect");
uni_blur = shader_get_uniform(sh_refl, "blur");
uni_blur_power = shader_get_uniform(sh_refl, "blur_radius");
//uni_dest_surf = shader_get_uniform(sh_refl, "dest_surf");
//uni_dest_pos = shader_get_uniform(sh_refl, "dest_pos");
//uni_spr_size = shader_get_uniform(sh_refl, "spr_size");
//uni_surf_size = shader_get_uniform(sh_refl, "surf_size");
#endregion