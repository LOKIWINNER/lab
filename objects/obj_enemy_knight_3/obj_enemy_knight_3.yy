{
    "id": "5c9378ea-0323-464a-bd55-56e0c9a0d100",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_knight_3",
    "eventList": [
        {
            "id": "ad7703d0-6c95-4dd6-9e01-4141535c7656",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        },
        {
            "id": "7c54b0dd-f28c-4717-960d-87ccd487df43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        },
        {
            "id": "c336533c-039a-4542-90dd-9980f0199ed3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        },
        {
            "id": "113609b8-b24a-49b5-b0f8-ce4d658d36e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        },
        {
            "id": "7de53cf0-4e35-464b-bef4-2cb1e0367229",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        },
        {
            "id": "7a8fb2e5-9b31-43e3-a63a-312f760f5686",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        },
        {
            "id": "bebd61d1-c5a7-4ea1-8bec-95031979b8ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "5c9378ea-0323-464a-bd55-56e0c9a0d100"
        }
    ],
    "maskSpriteId": "cbb72197-0f22-41f8-b592-bea3b485c4b9",
    "overriddenProperties": null,
    "parentObjectId": "782de373-6e97-4168-992b-48a470810795",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "69be2ce2-60c0-44fe-819c-87c935e0c325",
    "visible": true
}