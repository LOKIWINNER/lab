randomize();
scr_multiclick_init(2);


__mouse_x = device_mouse_x_to_gui(0);
__mouse_y = device_mouse_y_to_gui(0);

scale = 0.5;

fnt_inventory	= fnt_inv_medium;
fnt_cell		= fnt_inv_small;
fnt_subtitle	= fnt_inv_small

// Создаём Master List (вмещает весь инвентарь)
masterInvList = ds_list_create();

// Создаём инвентарь
inv1 = scr_init_inventory("Inventory 1", true);
// Создаём табы в нём
inv1_tab1 = scr_inventory_tab_init(inv1, "Backpack",	50, 250, 9, 10, 32, 5, 99, 1, true);
inv1_tab2 = scr_inventory_tab_init(inv1, "Coins",		430, 250, 1, 4, 32, 5, 10, 1, true);
inv1_tab3 = scr_inventory_tab_init(inv1, "Ammo",		478, 250, 1, 4, 32, 5, 999, 1, true);
inv1_tab4 = scr_inventory_tab_init(inv1, "Social",		910, 250, 1, 3, 32, 5, 1, 3, true);
inv1_tab5 = scr_inventory_tab_init(inv1, "Equip",		960, 250, 1, 8, 32, 5, 1, 3, true);

//Устанавливаем отображения у табов и скролл (если нужно)
scr_set_tab_view(inv1, "Backpack", 0, 0, 9, 5);
scr_set_tab_scrollbar(inv1, "Backpack", true, "UD-RIGHT", 0, scr_get_tab_view_pixel_height(inv1, "Backpack"), 0);





// Создаём второй инвентарь
inv2		= scr_init_inventory("Pickup", true);
// Создаём табы в нём
inv2_tab1	= scr_inventory_tab_init(inv2, "Test Items", 50, 700, 10, 1, 32, 5, 999, 1, true);

//Устанавливаем отображения у табов и скролл (если нужно)
scr_set_tab_view(inv2, "Test Items", 0, 0, 5, 1);
scr_set_tab_scrollbar(inv2, "Test Items", true, "LR-BOTTOM", 0, scr_get_tab_view_pixel_width(inv2, "Test Items"), 0);

// Добавляем предметы во второй инвентарь (нужно переделать на один предмет содержащий всю инфу о предметах)
scr_set_cell_object(scr_get_cell_at_grid_pos(inv2, "Test Items", 0, 0), obj_test1);
scr_set_cell_object(scr_get_cell_at_grid_pos(inv2, "Test Items", 1, 0), obj_test2);
scr_set_cell_object(scr_get_cell_at_grid_pos(inv2, "Test Items", 2, 0), obj_test3);

scr_set_cell_quantity(scr_get_cell_at_grid_pos(inv2, "Test Items", 0, 0), 199, 1);
scr_set_cell_quantity(scr_get_cell_at_grid_pos(inv2, "Test Items", 1, 0), 299, 1);
scr_set_cell_quantity(scr_get_cell_at_grid_pos(inv2, "Test Items", 2, 0), 399, 1);


// Other Variables
cI			= -1;
cT			= -1;
cC			= -1;
lc			= -1;
cVXS		= -1;
cVYS		= -1;
cVXE		= -1;
cVYE		= -1;
cTW			= -1;
cTH			= -1;
drawTooltip = true;

// Hooked Tab
hook[0] = -1;	// Hooked Inventory
hook[1] = -1;	// Hooked Tab
hook[2] = -1;	// Cancle move XY
hook[3] = -1;	// Cancel move XY

//Scrollbar
cTSP	= -1;
cTSPMax = -1;
cTSPMin = -1;

// Selection Variables
selectedItem		= -1;
selectedQuantity	= -1;
selectType			= -1;
prevCell			= -1;