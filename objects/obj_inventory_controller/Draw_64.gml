draw_set_font(fnt_inventory);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_color(c_black);


draw_text(500, 100, string(__mouse_x) + " - " + string(__mouse_y));

// Hbcetv bydtynfhm
scr_inventory_draw(inv1);
scr_inventory_draw(inv2);

// Нарисуйте выбранные элементы
if !(selectedItem == -1) {
	var sprite = object_get_sprite(selectedItem);
	draw_sprite(sprite, 0, __mouse_x, __mouse_y);
	draw_set_valign(fa_bottom);
	draw_text(__mouse_x - (sprite_get_width(sprite) / 2), __mouse_y + (sprite_get_width(sprite) / 2), selectedQuantity);
}


// Описание предмета
if (scr_get_cell_object(cC) != -1) {
	var tooltipStr = "Это примерное описание. У меня есть предложение найти, какой объект хранится в инвентаре, и основывать свое описание на этом."
	if (hook[0] == -1) scr_tooltip(cC, tooltipStr);
}